﻿<%@ Control Language="c#" CodeBehind="Monogram.ascx.cs" Inherits="netpoint.mckleinControls.Monogram.Monogram" %>

<asp:Panel ID="removePanel" runat="server">
    Monogram: 
    <asp:Label ID="monogramText" runat="server" />
    <br/>
    Position:
    <asp:DropDownList id="monogramPositionList"
                    AutoPostBack="True"
                    OnSelectedIndexChanged="Position_Change"
                    runat="server"/>
    <br/>
    <asp:Button ID="removeMonogram" runat="server" Text="Remove Monogram [-$30]" OnCommand="RemoveMonogram_Click" />
</asp:Panel>
<asp:Panel ID="addPanel" runat="server">
    <asp:TextBox ID="monogramTextInput" MaxLength="3" Columns="4" runat="server"/>
    <asp:Button ID="addMonogram" runat="server" Text="Add Monogram [+$30]" OnCommand="AddMonogram_Click" />
</asp:Panel>
