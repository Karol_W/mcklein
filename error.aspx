<%@ Page language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.error" Codebehind="error.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<br />
	<table cellpadding="2" cellspacing="1" border="0" width="100%">
		<tr>
			<td class="npwarning"><asp:Literal id="ltlError" runat="server" Text="An Internal Error has occurred."></asp:Literal></td>
		</tr>
	</table>
	<br />
	<table cellpadding="2" cellspacing="1" border="0" width="100%" id="tblError" runat="server">
		<tr>
			<td style="width:20%;" class="nplabel"><asp:Literal id="ltlVersionLabel" runat="server" Text="Version"></asp:Literal></td>
			<td class="npbody"><asp:Literal id="ltlVersion" runat="server"></asp:Literal></td>
		</tr>
		<tr>
			<td style="width:20%;" class="nplabel"><asp:Literal id="ltlMessageLabel" runat="server" Text="Message"></asp:Literal></td>
			<td class="npbody"><asp:Literal id="ltlMessage" runat="server"></asp:Literal></td>
		</tr>
		<tr>
			<td style="width:20%;" class="nplabel"><asp:Literal id="ltlSourceLabel" runat="server" Text="Source"></asp:Literal></td>
			<td class="npbody"><asp:Literal id="ltlSource" runat="server"></asp:Literal></td>
		</tr>
		<tr>
			<td style="width:20%;" class="nplabel"><asp:Literal id="ltlStackLabel" runat="server" Text="Stack"></asp:Literal></td>
			<td class="npbody"><asp:Literal id="ltlStack" runat="server"></asp:Literal></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="1" border="0" width="100%" id="tblOptions" runat="server">
		<tr>
			<td class="npbody"></td>
		</tr>
	</table>
	<asp:Literal id="errPricing" runat="server" Text="There is no default pricelist.  <a href='admin/commerce/price.aspx' >A default pricelist must be defined.</a>" Visible="False"></asp:Literal>
</asp:Content>
