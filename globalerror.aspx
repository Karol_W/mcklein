<%@ Page Language="c#" MasterPageFile="~/masters/default.master" Inherits="netpoint.globalerror" Codebehind="globalerror.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <br />
    <table cellpadding="2" cellspacing="1" border="0" width="100%">
        <tr>
            <td class="npwarning"><asp:Literal ID="ltlError" runat="server" Text="Whoops. Something broke."></asp:Literal></td>
        </tr>
    </table>
    <br />
    <table cellpadding="2" cellspacing="1" border="0" width="100%" id="tblError" runat="server">
        <tr>
            <td style="width:20%;" class="nplabel"><asp:Literal ID="ltlMessageLabel" runat="server" Text="Message"></asp:Literal></td>
            <td class="npbody"><asp:Literal ID="ltlMessage" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td style="width:20%;" class="nplabel"><asp:Literal ID="ltlSourceLabel" runat="server" Text="Source"></asp:Literal></td>
            <td class="npbody"><asp:Literal ID="ltlSource" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td style="width:20%;" class="nplabel"><asp:Literal ID="ltlStackLabel" runat="server" Text="Stack"></asp:Literal></td>
            <td class="npbody"><asp:Literal ID="ltlStack" runat="server"></asp:Literal></td>
        </tr>
    </table>
    <table cellpadding="2" cellspacing="1" border="0" width="100%" id="tblOptions" runat="server">
        <tr>
            <td class="npbody"><asp:Literal ID="ltlApology" runat="server" Text="Our technical people have been notified.<br>We're sorry for the inconvenience."></asp:Literal></td>
        </tr>
    </table>
</asp:Content>
