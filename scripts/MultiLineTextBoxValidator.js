function isMaxLength(ctl,maxLength,event) {
    var keyCode = (event.which)?event.which:event.keyCode;
    if ((keyCode != 8) && (keyCode != 46)) {
        if (arguments[3] == true && ctl.value.length > maxLength) {
            alert('Input is limited to ' + maxLength + ' characters. There are ' + ctl.value.length + ' characters present.');                
            return false;            
        }
        else if (ctl.value.length >= maxLength) {                
                return false;            
        }       
    }
}
