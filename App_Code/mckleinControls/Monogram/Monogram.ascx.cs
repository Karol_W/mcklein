﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using netpoint.api.commerce;
using McKlein_Website.mckleinAPI.Monogram;

namespace netpoint.mckleinControls.Monogram
{
    public class Monogram : System.Web.UI.UserControl
    {
        private int _orderDetailId;
        public int OrderDetailID {
            get {
                return _orderDetailId;
            }
            set {
                _orderDetailId = value;
                UpdateMonogram();
            }
        }

        protected global::System.Web.UI.WebControls.TextBox monogramTextInput;
        protected global::System.Web.UI.WebControls.Label monogramText;
        protected global::System.Web.UI.WebControls.DropDownList monogramPositionList;
        
        protected global::System.Web.UI.WebControls.Button addMonogram;
        protected global::System.Web.UI.WebControls.Button removeMonogram;
        
        protected global::System.Web.UI.WebControls.Panel addPanel;
        protected global::System.Web.UI.WebControls.Panel removePanel;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private OrderDetailMonogramUdf monogramUdf;
        
        private void UpdateMonogram() {
            monogramUdf = new OrderDetailMonogramUdf(_orderDetailId);
            monogramText.Text = monogramUdf.MonogramText;
            monogramTextInput.Text = monogramUdf.MonogramText;
            addMonogram.CommandArgument = _orderDetailId.ToString();
            removeMonogram.CommandArgument = _orderDetailId.ToString();
            
            foreach (MonogramPosition r in Enum.GetValues(typeof(MonogramPosition)))
            {
                ListItem item = new ListItem(
                    Regex.Replace(Enum.GetName(typeof(MonogramPosition), r), "(\\B[A-Z])", " $1"),
                    _orderDetailId + "," + r.ToString());
                monogramPositionList.Items.Add(item);
            }
            monogramPositionList.SelectedValue = _orderDetailId + "," + monogramUdf.Position.ToString();

            if(monogramUdf.MonogramText.Length > 0) {
                removePanel.Visible = true;
                addPanel.Visible = false;
            } else {
                removePanel.Visible = false;
                addPanel.Visible = true;
            }
        }

        protected void AddMonogram_Click(Object sender, CommandEventArgs e) {
            int.TryParse(e.CommandArgument.ToString(), out _orderDetailId);
            monogramUdf = new OrderDetailMonogramUdf(_orderDetailId);
            var orderDetail = new NPOrderDetail(_orderDetailId);
            if(monogramUdf.MonogramText.Length == 0) {
                orderDetail.DiscountPer -= 30;
                orderDetail.Save();
            }

            
            monogramUdf.MonogramText = monogramTextInput.Text;
            monogramUdf.Save();
            monogramText.Text = monogramUdf.MonogramText;

            Response.Redirect(HttpContext.Current.Request.Path);
        }

        protected void RemoveMonogram_Click(Object sender, CommandEventArgs e) {
            int.TryParse(e.CommandArgument.ToString(), out _orderDetailId);
            monogramUdf = new OrderDetailMonogramUdf(_orderDetailId);
            var orderDetail = new NPOrderDetail(_orderDetailId);
            if(monogramUdf.MonogramText.Length > 0) {
                orderDetail.DiscountPer += 30;
                orderDetail.Save();
            }

            
            monogramUdf.MonogramText = "";
            monogramUdf.Save();
            monogramText.Text = monogramUdf.MonogramText;

            Response.Redirect(HttpContext.Current.Request.Path);
        }

        protected void Position_Change(Object sender, EventArgs e) {
            string[] values = monogramPositionList.SelectedValue.Split(',');
            int.TryParse(values[0], out _orderDetailId);
            monogramUdf = new OrderDetailMonogramUdf(_orderDetailId);
            MonogramPosition monogramPosition;
            Enum.TryParse(values[1], out monogramPosition);
            monogramUdf.Position = monogramPosition;
            monogramUdf.Save();
        }
    }
}