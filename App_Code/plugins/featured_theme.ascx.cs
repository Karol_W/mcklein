﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;
using netpoint.api.catalog;

namespace netpoint.plugins
{
    public partial class featured_theme : System.Web.UI.UserControl
    {
        public NPBasePage bp;
        public string virtURL = "";
        public string assetsURL = "";
        public int DisplayNumber = 6;

        protected void Page_Load(object sender, EventArgs e)
        {
            bp = (NPBasePage)Page;

            FeaturedRepeater.DataSource = bp.Catalog.PartsListFeatured(DisplayNumber, bp.CatalogCode, bp.AccountID, bp.PriceList);
            FeaturedRepeater.DataBind();

            virtURL = bp.VirtualPath;
            assetsURL = bp.AssetsPath;
        }

        protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                NPPart part = (NPPart)e.Item.DataItem;
                NPManufacturer manf = new NPManufacturer(part.ManufacturerCode, bp.Encoding);

                //Show the image for the item. If there is a thumbnail specified use it instead of generating one
                HyperLink PartImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
                PartImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
                if (part.StaticThumbNail.Length > 0)
                {
                    PartImageLink.ImageUrl = bp.ProductsPath + part.StaticThumbNail;
                }
                else
                {
                    PartImageLink.ImageUrl = bp.ProductThumbImage + part.ThumbNail;
                }
                //Show the description when hovering over the image
                PartImageLink.ToolTip = part.Description;

                //Item number link
                HyperLink PartNoLink = ((HyperLink)e.Item.FindControl("PartNoLink"));
                PartNoLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
                PartNoLink.Text = part.PartName;

                //Add to Cart button
                HyperLink AddToCart = ((HyperLink)e.Item.FindControl("AddToCart"));
                AddToCart.NavigateUrl = "~/commerce/cart.aspx?AddPartNo=" + part.PartNo;
                AddToCart.ImageUrl = bp.ThemesPath + "assets/img/buy-btn.png";

                //More Info button
                HyperLink MoreInfo = ((HyperLink)e.Item.FindControl("MoreInfo"));
                MoreInfo.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
                MoreInfo.ImageUrl = bp.ThemesPath + "assets/img/more-info-btn.png";
                MoreInfo.ToolTip = "Learn More...";

                //Show the item description
                //((Label)e.Item.FindControl("PartDescription")).Text = part.Description;

                //Show the price
                ((netpoint.catalog.controls.PartPriceDisplay)e.Item.FindControl("SpecialPrice")).Part = part;
            }
        }
    }
}