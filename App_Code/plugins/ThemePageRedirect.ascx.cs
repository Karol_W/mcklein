﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;
using netpoint.api.catalog;

namespace netpoint.plugins
{
    public partial class ThemePageRedirect : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            int lastSlash = Page.Request.Url.LocalPath.LastIndexOf("/");
            string pageName = Page.Request.Url.LocalPath.Substring(lastSlash, Page.Request.Url.LocalPath.Length - lastSlash);

            if (System.IO.File.Exists(this.MapPath(bp.ThemesPath) + pageName))
            {
                if (!bp.AppRelativeVirtualPath.Contains(bp.ServerID))
                {
                    Server.Transfer("~/assets/common/themes/" + bp.ServerID + pageName + Page.Request.Url.Query);
                }
            }
        }
    }
}