﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;

namespace netpoint.plugins
{
    public partial class stringeditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NPBasePage bp = (NPBasePage)this.Page;
            btnStringEditor.Visible = bp.PermissionController.UserInRole("superuser");
            this.Visible = bp.PermissionController.UserInRole("superuser");
        }

        protected void btnStringEditor_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (Session["stringeditor"] == null)
            {
                Session["stringeditor"] = true;
            }
            else
            {
                Session["stringeditor"] = !(bool)Session["stringeditor"];
            }

            Response.Redirect(Request.Url.AbsoluteUri);
        }
    }
}