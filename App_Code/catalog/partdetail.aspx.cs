using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.classes;
using netpoint.api.commerce;
using netpoint.catalog.controls;
using netpoint.api;
using netpoint.api.common;
using netpoint.api.data;
using netpoint.api.utility;
using netpoint.common.controls;
using System.Collections.Generic;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class partdetail : netpoint.classes.NPBasePage
    {
        protected global::System.Web.UI.WebControls.Label sysMSRP; // need it to edit Label with ID sysMSRP.
        protected global::System.Web.UI.WebControls.Label sysSeries; // need it to edit Label with ID sysSeries.
        protected global::System.Web.UI.WebControls.Label sysVideo; // need it to edit Label with ID sysVideo.
        protected global::System.Web.UI.WebControls.Label sysAdvanceFeatures; // need it to edit Label with ID sysAdvanceFeatures.
        protected global::System.Web.UI.WebControls.Label sysDesignerN;
        private NPPart _p;
        private NPBasePage _bp;
        private bool firstPrice = false;
        public bool SortAsc
        {
            get
            {
                if (ViewState["PLSortAsc"] == null)
                {
                    ViewState.Add("PLSortAsc", true);
                }
                return (bool)ViewState["PLSortAsc"];
            }
            set
            {
                if (ViewState["PLSortAsc"] != null)
                {
                    ViewState.Remove("PLSortAsc");
                }
                ViewState.Add("PLSortAsc", value);
            }
        }

        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            if (Request.QueryString["PartNo"] != null)
            {                
                _p = new NPPart(Request.QueryString["PartNo"], CatalogCode);
                if (_p == null || !_p.Initialized)
                {
                    //item may be from another theme/catalog, check to see if it's in one of the configured ones from webconfig
                    ArrayList themes = GetAssociatedThemes(Request.QueryString["PartNo"]);
                    string themeList = "";
                    if (ConfigurationManager.AppSettings["ThemeList"] != null)
                    {
                        themeList = ConfigurationManager.AppSettings["ThemeList"].ToString().ToLower();

                        foreach (string theme in themes)
                        {
                            if (themeList.Contains(theme.ToLower()))
                            {
                                //found the theme in the supported list so redirect here
                                string url = Page.Request.Url.ToString();
                                if (url.ToLower().Contains("serverid="))
                                {
                                    //original url has serverid so replace in existing string
                                    string origServerId = Request.QueryString["ServerID"].ToString().ToLower();
                                    url = url.ToLower().Replace("serverid=" + origServerId, "ServerID=" + theme);
                                    Response.Redirect(url , true);
                                }
                                else
                                {
                                    //original url does not have serverid so add to existing string
                                    Response.Redirect(url + "&ServerID=" + theme, true);
                                }
                            }
                        }
                    }                    
                }

                string _globalDisplayOutOfStockFlag = "Y";
                if (!Convert.ToBoolean(NPConnection.GetConfigDB("Inventory", "DisplayOutOfStockFlag")))
                {
                    _globalDisplayOutOfStockFlag = "N";
                }

                if (_p != null
                    && _p.Initialized)
                {
                    if (_p.Available
                        && _p.AvailableDate <= DateTime.Today
                        && (_p.ExpirationDate > DateTime.Today || _p.ExpirationDate == DateTime.MinValue || _p.ExpirationDate == null)
                        && _p.ProdLinePartNo == ""
                        && !_p.PartNo.StartsWith("~")
                        && ((_p.InheritInventorySettings && (_globalDisplayOutOfStockFlag == "Y" || _p.InStock))
                        || (!_p.InheritInventorySettings && (_p.DisplayOutOfStock || _p.InStock)))
                        )
                    {
                        if (_p.TemplateFile != null && _p.TemplateFile != "")
                        {
                            NPBasePage bp = (NPBasePage)Page;
                            string filePath = bp.ThemesPath + "catalog/masters/" + _p.TemplateFile;
                            FileInfo thememaster = new FileInfo(Server.MapPath(filePath));
                            if (thememaster.Exists)
                            {
                                this.Page.MasterPageFile = filePath;
                            }
                            else
                            {
                                filePath = "~/assets/catalog/masters/" + _p.TemplateFile;
                                thememaster = new FileInfo(Server.MapPath(filePath));
                                if (thememaster.Exists)
                                {
                                    this.Page.MasterPageFile = filePath;
                                }
                            }
                        }
                    }
                    //else allow product line masters to load.
                }
                else
                {
                    base.Exit();
                }
            }
            else
            {
                base.Exit();
            }
            //base.OnPreInit(e);
        }
        protected void Repeater_Content_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                System.Web.UI.WebControls.Image btnImg = (System.Web.UI.WebControls.Image)e.Item.FindControl("ColorSwatch");

                string strColor = DataBinder.Eval(e.Item.DataItem, "Color").ToString();
                string colorName = DataBinder.Eval(e.Item.DataItem, "ColorName").ToString();
                btnImg.ImageUrl = "~/assets/catalog/Swatches/" + strColor;
                btnImg.ToolTip = colorName;
                btnImg.ID = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();



            }

        }

        protected void Repeater_PartPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl partPrice = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("spanPartPrice");
                System.Web.UI.HtmlControls.HtmlGenericControl partOrigPrice = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("spanPartOrigPrice");
                System.Web.UI.HtmlControls.HtmlGenericControl priceDisplay = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("spanPriceDisplay");
      


                //  System.Web.UI.WebControls.Literal partPrice = (System.Web.UI.WebControls.Literal)e.Item.FindControl("partPrice");
                if (!firstPrice) //First Time through
                {
                    firstPrice = true;
                    partPrice.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " mc-part-price";
                    partOrigPrice.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " mc-part-origprice";
                }
                else
                {
                    if(priceDisplay != null) {
                        //priceDisplay.Attributes["style"] = "display:none";                    
                    }
                    partPrice.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " mc-part-price";
                    partPrice.Attributes["style"] = "display:none";
                    partOrigPrice.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " mc-part-origprice";
                    partOrigPrice.Attributes["style"] = "display:none";
                }


                NPPart part = new NPPart(DataBinder.Eval(e.Item.DataItem, "PartNo").ToString());
                decimal value = NPPartPricingMaster.Price(DataBinder.Eval(e.Item.DataItem, "PartNo").ToString(), _bp.AccountID, _bp.PriceList, 1, DateTime.Now, part.ConnectionString);
                partPrice.InnerHtml = value.ToString("C");
                partPrice.ID = "spanPartPrice_" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();

                decimal origprice = part.OriginalPrice;
                //IF STATEMENT determines whether the original price is to be displayed or not. "Value" will effectively be the sales price and the actual price of the item.
                if (origprice > value)
                {

                    if (_bp.TheTheme.AddTaxToPrice)
                    {
                        decimal origValue = origprice + NPTax.GetPartTax(part, origprice, _bp.AccountID, _bp.TaxDestination);
                        partOrigPrice.InnerHtml = "" + origValue.ToString("C");
                        partOrigPrice.ID = "spanPartOrigPrice_" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();

                    }
                    else
                    {
                        partOrigPrice.InnerHtml = "  " + origprice.ToString("C");
                        partOrigPrice.ID = "spanPartOrigPrice_" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();
                    }
                }
                else
                {
                    partOrigPrice.Visible = false;
                }




            }

        }
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (_p != null && _p.Initialized)
            {
                _bp = (NPBasePage)Page;
                BindHeader(_p);
                npppblock.Part = _p;
                nppcblock.Part = _p;

                //	Product Line
                if (_p.ProdLinePartNo.Length > 0)
                {
                    Response.Redirect("~/catalog/partdetail.aspx?PartNo=" + _p.ProdLinePartNo, true);
                }
                if (_p.InProdLine)
                {
                    BindProdLine(_p);
                }
                //	Variant Config
                if (_p.AggregateType == AggregatePartType.Variant)
                {
                    partConfig.Part = _p;
                }

                //	Part Options								
                BindOptions(_p);

                //  Part Kit
                kpblock.ParentPart = _p;

                //mark the view
                NPStats.View(UserID, SessionID, "part", _p.PartNo, ConnectionString);
                var table = new DataTable();
                table.Columns.Add("Color", typeof(String));
                table.Columns.Add("PartNo", typeof(String));
                table.Columns.Add("ColorName", typeof(String));
                if (_p.InProdLine)
                {
                    // HtmlAnchor AddToCart = (HtmlAnchor)Parent.FindControl("mcAddToCart");
                    ArrayList parts = NPPart.GetProductLine(_p.PartNo, _bp.AccountID, null, NPConnection.GblConnString, _bp.AppCulture);
                    int count = 0;
                    List<String> upcs = new List<String>();
                    foreach (NPPart part in parts)
                    {
                        NPPartUDF partProdLineUDF = new NPPartUDF(part.PartNo, "U_ColorSwatch");
                        NPPartUDF partProdLineColorUDF = new NPPartUDF(part.PartNo, "U_maincolor");
                        if (partProdLineUDF.Initialized && partProdLineUDF.Value != null)
                        {
                            string color = partProdLineUDF.Value;
                            table.Rows.Add(new object[] { color, part.PartNo, partProdLineColorUDF.Value });
                            part.Fetch();
                            upcs.Add(part.PartCode + " (" + partProdLineColorUDF.Value + ")");
                        }
                        if (count == 0)
                        {
                            mcAddToCart.HRef = "~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(part.PartNo);
                            count++;
                        }


                    }
                    sysPartUPC.Text = string.Join(", ", upcs.ToArray());
                }
                Repeater_PartPrice.DataSource = table;
                Repeater_PartPrice.DataBind();

                Repeater_Content.DataSource = table;
                Repeater_Content.DataBind();
            }
            else
            {
                base.Exit();
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (_p != null && _p.Initialized)
            {
                if (_p.PageTitle != "")
                {
                    Page.Title = _p.PageTitle;
                }
                else
                {
                    Page.Title = _p.PartName + " - " + _p.PartNo + (Page.Title != null ? (" | " + Page.Title) : "");
                }

            }
        }

        public string Keywords
        {
            get
            {
                return _p.Keywords;
            }
        }

        public string ClientID
        {
            get
            {
                return "test";
            }
        }

        public string Description
        {
            get
            {
                return _p.MetaDescription;
            }
        }

        private void BindHeader(NPPart p)
        {
            NPBasePage bp = (NPBasePage)Page;
            sysPartNo.Text = p.PartNo;
            sysPartName.Text = p.PartName;
            sysPartCode.Text = p.PartCode;
            sysManufacturerPartNo.Text = p.ManufacturerPartNo;
            sysManufacturerName.Text = p.ManufacturerName;
            NPPartUDF partProdLineUDF = new NPPartUDF(p.PartNo, "U_tagline");
            NPPartUDF msrpUDF = new NPPartUDF(p.PartNo, "U_msrp");
            NPPartUDF seriesUDF = new NPPartUDF(p.PartNo, "U_collection");
            NPPartUDF videoUDF = new NPPartUDF(p.PartNo, "U_videos");
            NPPartUDF advanceFeaturesUDF = new NPPartUDF(p.PartNo, "U_advancefeatures");
            
            NPPartUDF designerNtsUDF = new NPPartUDF(p.PartNo, "U_DesNote");

            if (videoUDF.Initialized && videoUDF.Value != null)
            {
                sysVideo.Text = videoUDF.Value;
            }
            if (seriesUDF.Initialized && seriesUDF.Value != null)
            {
                sysSeries.Text = seriesUDF.Value;
            }
            if (msrpUDF.Initialized && msrpUDF.Value != null)
            {
                sysMSRP.Text = msrpUDF.Value;
            }
            if (advanceFeaturesUDF.Initialized && advanceFeaturesUDF.Value != null)
            {
                sysAdvanceFeatures.Text = advanceFeaturesUDF.Value;
            }

            if (designerNtsUDF.Initialized && designerNtsUDF.Value != null)
            {
                sysDesignerN.Text = designerNtsUDF.Value;
            }

            if (partProdLineUDF.Initialized && partProdLineUDF.Value != null)
            {
                sysPartTagline.Text = partProdLineUDF.Value;
            }
            if (!p.PartType.Equals("") && p.PartType.Equals(" "))
            {
                sysPartType.Text = "(" + p.PartType + ")";
            }
            if (_p.DisplayOption.Description)
            {
                sysPartDescription.Text = p.Description;
            }

            lnkEdit.Visible = false;
            lnkEdit.NavigateUrl = "~/admin/catalog/Part.aspx?partno=" + Server.UrlEncode(p.PartNo) + "&backpage=~/catalog/partdetail.aspx?partno=" + Server.UrlEncode(p.PartNo);
            if (bp.PermissionController.UserInRole("superuser") || bp.PermissionController.UserInRole("partsmanager"))
            {
                lnkEdit.ImageUrl = bp.ResolveImage("icons", "edit.gif");
                lnkEdit.Visible = true;
            }
        }

        private void BindOptions(NPPart p)
        {
            nppablock.Visible = p.DisplayOption.Attributes;
            if (nppablock.Visible)
            {
                nppablock.PartNo = p.PartNo;
            }

            nppmblock.Visible = p.DisplayOption.Media;
            if (nppmblock.Visible)
            {
                nppmblock.Part = p;
            }

            nppnblock.Visible = p.DisplayOption.Notes;
            if (nppnblock.Visible)
            {
                nppnblock.Part = p;
            }

            npprblock.Visible = p.DisplayOption.Remarks;
            if (npprblock.Visible)
            {
                npprblock.Part = p;
            }

            npmanfblock.Visible = p.DisplayOption.Manufacturer;
            if (npmanfblock.Visible)
            {
                npmanfblock.MNFCode = p.ManufacturerCode;
                npmanfblock.Encoding = ((NPBasePage)this.Page).Encoding;
            }

            if (!p.DisplayOption.Media && !p.DisplayOption.Manufacturer && !p.DisplayOption.Dimensions)
            {
                //tblLayout.Rows[0].Cells[0].Width=new Unit(0);
                nppmblock.Visible = false;
                nppsblock.Visible = false;
                npmanfblock.Visible = false;
            }

            if (p.DisplayOption.QuantityDDL || p.DisplayOption.QuantityTB)
            {
                QuantityAddBlock.Visible = true;
            }
            nppss.Part = p;
            nppublock.Part = p;
        }

        private void BindProdLine(NPPart p)
        {
            npppblock.Visible = false;

            if (pap != null && p.ProdLineDisplayOption.AttributePicker)
            {
                pap.Part = p;
                pap.Visible = true;
            }
            if (prodLine != null && p.ProdLineDisplayOption.List)
            {
                if (!p.ProdLineDisplayOption.AttributePicker)
                {
                    prodLine.PartNo = p.PartNo;
                }
                prodLine.Visible = true;
            }
            if (npProdLineMatrix != null && p.ProdLineDisplayOption.AttributeMatrix)
            {
                npProdLineMatrix.PartNo = p.PartNo;
                npProdLineMatrix.Visible = true;
            }
            if (ProdLineDropBlock != null && p.ProdLineDisplayOption.DropDownList)
            {
                ProdLineDropBlock.PartNo = p.PartNo;
                ProdLineDropBlock.Visible = true;
            }
            if (ProdLineImageBlock != null && p.ProdLineDisplayOption.ImagePicker)
            {
                ProdLineImageBlock.PartNo = p.PartNo;
                ProdLineImageBlock.Visible = true;
            }
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args.GetType().Name == "RecalcPriceEventArgs")
            {
                npppblock.VariantPrice = ((RecalcPriceEventArgs)args).ConfiguredPrice;
            }

            if (source.GetType().BaseType.Name == "PartAttributePicker")
            {
                if (args.GetType().Name == "NPEventArgs")
                {
                    NPEventArgs ea = (NPEventArgs)args;
                    if (ea.FromEvent == "Page_Load")
                    {
                        NPPart p = (NPPart)ea.Data;

                        if (prodLine != null && p.ProdLineDisplayOption.List)
                        {
                            if (p.ProdLineDisplayOption.AttributePicker)
                            {
                                SortProductLineList();
                            }
                        }
                    }
                }
            }

            if (source.GetType().BaseType.Name == "ProdLineBlock")
            {
                if (args.GetType().Name == "GridViewSortEventArgs")
                {
                    SortProductLineList();
                }
            }

            return base.OnBubbleEvent(source, args);
        }

        private void SortProductLineList()
        {
            NPBasePage bp = (NPBasePage)Page;
            SortAsc = !SortAsc;
            ArrayList p = pap.Parts;
            if (!pap.Visible)
            {
                p = NPPart.GetProductLine(prodLine.PartNo, bp.AccountID, null, NPConnection.GblConnString, bp.AppCulture);
            }
            if (!SortAsc)
            {
                p.Reverse();
            }
            prodLine.DataSource = p;
        }

        private ArrayList GetAssociatedThemes(string _partNo)
        {
            ArrayList associatedThemes = new ArrayList(25);
            DataParameters dp = new DataParameters(1);
            dp.Add(NPDataType.NPString, "@partno", _partNo);
            string SQL = "SELECT ServerID FROM PartsCategory PC " +
                        "INNER JOIN CatalogsCategory CC ON PC.CategoryID = CC.CategoryID " + 
                        "INNER JOIN Catalogs C ON CC.CatalogID = C.CatalogID " + 
                        "INNER JOIN NPThemes T ON T.CatalogCode = C.CatalogCode " +
                        "WHERE PartNo = @partno " +
                        "ORDER BY ServerID";
            using (IDataReader TheReader = DataFunctions.ExecuteReader(CommandType.Text, SQL, dp.Parameters, ConnectionString))
            {
                while (TheReader.Read())
                {
                    associatedThemes.Add(Convert.ToString(TheReader["ServerID"]));
                }
            }
            return associatedThemes;
        }
    }
}
