namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.classes;


    /// <summary>
    ///		Summary description for PartAttributesBlock.
    /// </summary>
    public partial class PartAttributesBlock : System.Web.UI.UserControl
    {
        public NPCatalog npcatalog;
        public string PartNo = "";
        public string LabelWidth = "35%";
        public string LabelCssClass = "nplabel";
        public string TableCssClass = "npbody";
        public string HeaderCssClass = "npheader";
        public string AttributesCssClass = "npbody";
        public int TableBorderWidth = 0;
        public string TableBorderColor = "silver";
        private NPBasePage bp;

        public Unit BorderWidth
        {
            set { AttributesPanel.BorderWidth = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)Page;
            npcatalog = bp.Catalog;
            createTables();
            AttributesPanel.Visible = Convert.ToBoolean(sysStartExpanded.Text);
            clpGeneral.ImageUrl = bp.ResolveImage("icons", "collapse.gif");
            expGeneral.ImageUrl = bp.ResolveImage("icons", "expand.gif");
        }

        private void createGrid()
        {
            DataTable palist = npcatalog.PartAttributes(PartNo);
            foreach (DataRow padr in palist.Rows)
            {
                DataTable dt = new DataTable();
                DataRow dr;
                DataColumn dcLabel = new DataColumn("Label", typeof(string));
                DataColumn dcValue = new DataColumn("Value", typeof(string));
                dt.Columns.Add(dcLabel);
                dt.Columns.Add(dcValue);
                int dimtotal = (int)padr["DimensionsTotal"];
                for (int i = 1; i <= dimtotal; i++)
                {
                    if (padr["Dimension" + i].ToString() != "")
                    {
                        dr = dt.NewRow();
                        dr[0] = padr["DimLabel" + i].ToString();
                        dr[1] = padr["Dimension" + i].ToString();
                        dt.Rows.Add(dr);
                    }
                }

                Label lbl = new Label();
                lbl.Text = padr["AttributeName"].ToString();
                lbl.CssClass = "npbody";
                AttributesPanel.Controls.Add(lbl);

                GridView gv = new GridView();
                gv.CssClass = "npbody";
                gv.Width = Unit.Parse("100%");
                gv.ShowHeader = false;
                gv.DataSource = dt;
                gv.DataBind();
                AttributesPanel.Controls.Add(gv);
            }
        }

        private void createTables()
        {
            DataTable palist = npcatalog.PartAttributes(PartNo, bp.CatalogCode);

            if (palist.Rows.Count == 0)
            {
                AttributesPanel.Visible = false;
                MainPanel.Visible = false;
            }
            else
            {
                foreach (DataRow padr in palist.Rows)
                {
                    HtmlTable ht = new HtmlTable();
                    ht.Width = "100%";
                    ht.Border = TableBorderWidth;
                    ht.CellSpacing = 0;
                    ht.CellPadding = 2;
                    ht.BorderColor = TableBorderColor;
                    ht.Attributes.Add("class", TableCssClass);
                    HtmlTableRow headerrow = new HtmlTableRow();
                    HtmlTableCell header = new HtmlTableCell();
                    header.InnerText = padr["AttributeName"].ToString();
                    header.ColSpan = 2;
                    header.Attributes.Add("class", HeaderCssClass);
                    headerrow.Cells.Add(header);
                    ht.Rows.Add(headerrow);

                    for (int i = 1; i < 10; i++)
                    {
                        if (padr["Dimension" + i].ToString().Trim() != "" &&
                            padr["DisplayOptions"].ToString().Substring(i - 1, 1) == "Y")
                        {
                            HtmlTableRow hr = new HtmlTableRow();
                            HtmlTableCell label = new HtmlTableCell();
                            HtmlTableCell dimension = new HtmlTableCell();
                            label.InnerText = padr["DimLabel" + i].ToString();
                            label.Width = LabelWidth;
                            label.VAlign = "top";
                            label.Attributes.Add("class", LabelCssClass);
                            dimension.InnerText = padr["Dimension" + i].ToString();
                            dimension.VAlign = "top";
                            dimension.Attributes.Add("class", AttributesCssClass);
                            hr.Cells.Add(label);
                            hr.Cells.Add(dimension);
                            ht.Rows.Add(hr);
                        }
                    }
                    AttributesPanel.Controls.Add(ht);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clpGeneral.Click += new System.Web.UI.ImageClickEventHandler(this.clpGeneral_Click);
            this.expGeneral.Click += new System.Web.UI.ImageClickEventHandler(this.expGeneral_Click);

        }
        #endregion

        private void clpGeneral_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            clpGeneral.Visible = false;
            expGeneral.Visible = true;
            AttributesPanel.Visible = false;
        }

        private void expGeneral_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            clpGeneral.Visible = true;
            expGeneral.Visible = false;
            AttributesPanel.Visible = true;
        }
    }
}
