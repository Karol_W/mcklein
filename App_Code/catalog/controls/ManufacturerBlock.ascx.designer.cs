//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace netpoint.catalog.controls
{

    public partial class ManufacturerBlock
    {
        protected System.Web.UI.WebControls.Panel MNFPanel;
        protected System.Web.UI.WebControls.HyperLink sysMNFLogo;
        protected System.Web.UI.WebControls.Label sysMNFName;
        protected System.Web.UI.WebControls.Label sysMNFDescription;
        protected System.Web.UI.WebControls.HyperLink sysMNFWebsite;
    }
}
