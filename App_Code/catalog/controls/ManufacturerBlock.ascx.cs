namespace netpoint.catalog.controls
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api;
    using netpoint.api.commerce;
    using netpoint.classes;
    using netpoint.api.catalog;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class ManufacturerBlock : System.Web.UI.UserControl
    {
        private string _mnfCode;
        private string _encoding;
        private int _mnfID;

        public Unit BorderWidth
        {
            set { MNFPanel.BorderWidth = value; }
        }

        public string MNFCode
        {
            get { return _mnfCode; }
            set { _mnfCode = value; }
        }

        public string Encoding
        {
            get { return _encoding; }
            set { _encoding = value; }
        }

        public int MNFID
        {
            get { return _mnfID; }
            set { _mnfID = value; }
        }

        public bool ShowDescription
        {
            set { sysMNFDescription.Visible = value; }
        }

        public bool ShowLogo
        {
            set { sysMNFLogo.Visible = value; }
        }

        public bool ShowWebsite
        {
            set { sysMNFWebsite.Visible = value; }
        }

        public bool ShowName
        {
            set { sysMNFName.Visible = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)this.Page;
            if (!Page.IsPostBack)
            {
                if (_mnfCode != null && _mnfCode != "-1" && _mnfCode != "")
                {
                    NPManufacturer m = new NPManufacturer(this._mnfCode, bp.Encoding);
                    if (!m.Initialized)
                    {
                        this.Visible = false;
                    }
                    else
                    {
                        sysMNFName.Text = m.MNFName;
                        sysMNFDescription.Text = m.MNFDescription;
                        sysMNFLogo.ImageUrl = bp.AssetsPath + "catalog/manufacturers/" + m.MNFLogo;
                        sysMNFLogo.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + _mnfCode + "&Keywords=" + m.MNFName;
                        sysMNFLogo.Visible = System.IO.File.Exists(Server.MapPath(sysMNFLogo.ImageUrl));
                        sysMNFWebsite.Text = m.MNFWebsite;
                        sysMNFWebsite.NavigateUrl = m.MNFWebsite;
                    }
                }
                else if (_mnfID > 0)
                {
                    NPManufacturer m = new NPManufacturer(_mnfID);
                    if (!m.Initialized)
                    {
                        this.Visible = false;
                    }
                    else
                    {
                        _mnfCode = m.MNFCode;
                        sysMNFName.Text = m.MNFName;
                        sysMNFDescription.Text = m.MNFDescription;
                        sysMNFLogo.ImageUrl = bp.AssetsPath + "catalog/manufacturers/" + m.MNFLogo;
                        sysMNFLogo.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + _mnfCode + "&Keywords=" + m.MNFName;
                        sysMNFLogo.Visible = System.IO.File.Exists(Server.MapPath(sysMNFLogo.ImageUrl));
                        sysMNFWebsite.Text = m.MNFWebsite;
                        sysMNFWebsite.NavigateUrl = m.MNFWebsite;
                    }
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
