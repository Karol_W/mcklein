namespace netpoint.catalog.controls
{
    using System;
    using System.Configuration;
    using System.Collections;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api;
    using netpoint.api.catalog;
    using netpoint.api.commerce;
    using netpoint.api.common;
    using netpoint.classes;
    using netpoint.common.controls;
    using netpoint.api.account;
    using netpoint.api.utility;
    using netpoint.api.data;


    /// <summary>
    ///     Summary description for CategoriesBlock.
    /// </summary>
    public partial class PartsListBlock : System.Web.UI.UserControl
    {

        #region "Members and Properties"


        public int CategoryID = 0;
        public int DisplayNumber = 5;
        public string ListType = ""; //C=Category,F=Featured,D=Discounted,N=New,S=Search,P=PreviouslyPurchased;
        public string DisplayType = ""; //L=ListGrid,I=IteratedItems,M=MachineList,Q=Qty List,D=PartDetailList
        public string MNFCode = "";
        public string Keywords = "";
        public string CategoryCode = null;

        private bool _lShowFullImage = false;
        private bool _iShowFullImage = false;
        private bool _bound = false;
        private bool _showAddToWishListButton = true;
        private bool _enabled = true;        

        public bool LShowFullImage
        {
            get { return _lShowFullImage; }
            set { _lShowFullImage = value; }
        }

        public bool IShowFullImage
        {
            get { return _iShowFullImage; }
            set { _iShowFullImage = value; }
        }
        public bool CompareVisible
        {
            get { return PartsListGrid.FindColumn("colCompare").Visible; }
            set { PartsListGrid.FindColumn("colCompare").Visible = value; }
        }
        public bool ImagesVisible
        {
            get { return PartsListGrid.FindColumn("colImage").Visible; }
            set { PartsListGrid.FindColumn("colImage").Visible = value; }
        }
        //2=partno
        public bool DescriptionVisible
        {
            get { return PartsListGrid.FindColumn("ItemName").Visible; }
            set { PartsListGrid.FindColumn("ItemName").Visible = value; }
        }
        public bool TypeVisible
        {
            get { return PartsListGrid.FindColumn("colType").Visible; }
            set { PartsListGrid.FindColumn("colType").Visible = value; }
        }
        public bool UnitVisible
        {
            get { return PartsListGrid.FindColumn("colUnit").Visible; }
            set { PartsListGrid.FindColumn("colUnit").Visible = value; }
        }
        //6=price
        public bool TaxVisible
        {
            get { return PartsListGrid.FindColumn("colTax").Visible; }
            set { PartsListGrid.FindColumn("colTax").Visible = value; }
        }
        public bool AddVisible
        {
            get { return PartsListGrid.FindColumn("colAdd").Visible; }
            set { PartsListGrid.FindColumn("colAdd").Visible = value; }
        }
        public bool QtyVisible
        {
            get { return PartsListGrid.FindColumn("colQty").Visible; }
            set { PartsListGrid.FindColumn("colQty").Visible = value; }
        }

        public Unit BorderWidth
        {
            set
            {
                PartsListGrid.BorderWidth = value;
                PartsListRepeater.BorderWidth = value;
            }
        }

        public bool ShowStyleRepeater
        {
            set
            {
                PartsListRepeater.Visible = value;
                PartsListGrid.Visible = !value;
            }
        }

        public int RepeatColumns
        {
            set { PartsListRepeater.RepeatColumns = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool ShowNoPartsMessage
        {
            get { return sysNoData.Visible; }
            set { sysNoData.Visible = value; }
        }

        private NPBasePage _bp;

        #endregion


       
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!Enabled)
            {
                this.Visible = false;
                return;
            }

            _bp = (NPBasePage)Page;
            _showAddToWishListButton = Convert.ToBoolean(NPConnection.GetConfigDB("Commerce", "WishList", bool.TrueString, _bp.ConnectionString));

            npmblock.CatalogID = _bp.Catalog.CatalogID;

            if (Request.QueryString["ListType"] != null)
            {
                ListType = Request.QueryString["ListType"];
            }
            if (Request.QueryString["DisplayType"] != null)
            {
                DisplayType = Request.QueryString["DisplayType"];
            }
            if (Request.QueryString["MNFCode"] != null)
            {
                MNFCode = Request.QueryString["MNFCode"];
            }
            if (Request.QueryString["CategoryCode"] != null)
            {
                CategoryCode = Request.QueryString["CategoryCode"];
            }
            if (Request.QueryString["Keywords"] != null)
            {
                Keywords = Request.QueryString["Keywords"];
            }

            if (!this.IsPostBack)
            {
                errNoPartsInCategory.Text = "<br><b>" + errNoPartsInCategory.Text + "</b><br><br>" +
                    hdnAdminVerify.Text + "<br>" +
                    hdnHasParts.Text + "<br>" +
                    hdnPartsUnavailable.Text + "<br>" +
                    hdnPartsExpired.Text + "<br>" +
                    hdnNoCategory.Text;

                btnSubmit.ImageUrl = _bp.ResolveImage("buttons", "buy-btn-xsmall.png");

                if (!_bound)
                {
                    BindGrid();
                    _bound = true;
                }
            }
            //protected global::System.Web.UI.WebControls.Label Repeater_Content; // need it to edit Label with ID sysSeries.
           // Repeater_Content.DataSource = table;
           // Repeater_Content.DataBind();
        }

        /// <summary>
        /// Fill a DropDownList with child parts for a given master part. Used with DisplayType "D"
        /// </summary>
        /// <param name="ddlChildParts">A reference to a DropDownList.</param>
        /// <param name="part">A reference to an NPPart object that is a master part.</param>
        private void FillChildPartsDropDownList(DropDownList ddlChildParts, string masterPartNumber)
        {
            string selectedValue = "";
            bool showPartNumber = false;
            bool showPartName = true;
            bool showPartType = true;
            bool showPartPrice = true;
            bool showPartInventory = false;
            string inStock = "0";
            string backOrdered = "0";
            string priceDesc = "";
            string taxDesc = hdnTax.Text + ": ";

            if (_bp.TheTheme.AddTaxToPrice)
            {
                priceDesc = hdnPriceInclTax.Text + ": ";
            }
            else if (_bp.TheTheme.ShowItemizedTax)
            {
                priceDesc = hdnPriceExclTax.Text + ": ";
            }

            NPListBinder.PopulateProductLine(
                ddlChildParts
                , selectedValue
                , masterPartNumber
                , _bp.AccountID
                , showPartNumber
                , showPartName
                , showPartType
                , showPartPrice
                , showPartInventory
                , _bp.TheTheme.AddTaxToPrice
                , _bp.TheTheme.ShowItemizedTax
                , inStock
                , backOrdered
                , priceDesc
                , taxDesc
                , _bp.PriceList
                , _bp.SessionCurrency
                , _bp.TaxDestination);
        }

        protected void BindGrid()
        {
            ArrayList dataSource = new ArrayList();

            // Create a data source based on the ListType.
            switch (ListType)
            {
                case "C":
                    if (DisplayType.Equals("M"))
                    {
                        dataSource = _bp.Catalog.PartsListMachine(CategoryID, npmblock.MachineCode, _bp.AccountID, _bp.PriceList);
                    }
                    else
                    {
                        dataSource = _bp.Catalog.CategoryParts(CategoryID, _bp.AccountID, _bp.PriceList, _bp.Encoding);
                    }
                    break;
                case "F":
                    dataSource = _bp.Catalog.PartsListFeatured(DisplayNumber, _bp.CatalogCode, _bp.AccountID, _bp.PriceList);
                    break;
                case "D":
                    dataSource = _bp.Catalog.PartsListDiscounted(DisplayNumber, _bp.CatalogCode, _bp.AccountID, _bp.PriceList);
                    break;
                case "N":
                    dataSource = _bp.Catalog.PartsListNew(DisplayNumber, _bp.CatalogCode, _bp.AccountID, _bp.PriceList);
                    break;
                case "S":
                    dataSource = NPIndexedSearch.PartSearch(Keywords, _bp.Catalog.CatalogID, CategoryCode, MNFCode, npmblock.MachineCode, _bp.ConnectionString, _bp.AccountID, 1, DateTime.Now, _bp.PriceList, true, null);
                    break;
                case "P":
                    NPAccount _acct = new NPAccount(_bp.AccountID);
                    dataSource = _acct.GetPreviouslyPurchasedItems(_bp.Catalog.CatalogID, DateTime.Now.AddYears(-100));
                    break;
                default:
                    dataSource = _bp.Catalog.CategoryParts(CategoryID, _bp.AccountID, _bp.PriceList, _bp.Encoding);
                    break;
            }

            // Enable UI controls based on the DisplayType.
            switch (DisplayType)
            {
                case "L":
                    PartsListGrid.Visible = true;
                    PartsListRepeater.Visible = false;
                    PartDetailListRepeater.Visible = false;
                    break;
                case "Q":
                    PartsListGrid.Visible = true;
                    PartsListRepeater.Visible = false;
                    PartDetailListRepeater.Visible = false;
                    AddVisible = false;
                    QtyVisible = true;
                    btnSubmit.Visible = true;
                    break;
                case "I":
                    PartsListGrid.Visible = false;
                    PartsListRepeater.Visible = true;
                    PartDetailListRepeater.Visible = false;
                    PartsListRepeater.DataSource = dataSource;
                    PartsListRepeater.DataBind();
                    break;
                case "M":
                    npmblock.Visible = true;
                    PartsListGrid.Visible = true;
                    PartsListRepeater.Visible = false;
                    PartDetailListRepeater.Visible = false;
                    PartsListGrid.DataSource = dataSource;
                    PartsListGrid.DataBind();
                    break;
                case "D":
                    PartsListGrid.Visible = false;
                    PartsListRepeater.Visible = false;
                    PartDetailListRepeater.Visible = true;
                    PartDetailListRepeater.DataSource = dataSource;
                    PartDetailListRepeater.DataBind();
                    btnSubmit.Visible = false;
                    break;
                default:
                    // If view only don't let them add parts to the cart
                    AddVisible = !((NPBasePage)this.Page).Catalog.ViewOnly;
                    QtyVisible = false;
                    btnSubmit.Visible = false;
                    break;
            }

            if (PartsListGrid.Visible)
            {
                TaxVisible = _bp.TheTheme.ShowItemizedTax;

                if (_bp.TheTheme.ShowItemizedTax || _bp.TheTheme.AddTaxToPrice)
                {
                    PartsListGrid.FindColumn("colPrice").HeaderText =
                        _bp.TheTheme.AddTaxToPrice ? hdnPriceInclTax.Text : hdnPriceExclTax.Text;
                }

                PartsListGrid.DataSource = dataSource;
                PartsListGrid.DataBind();
            }

            //  Load error message unless machine selector
            if (!DisplayType.Equals("M"))
            {
                if (PartsListGrid.Visible && dataSource.Count == 0)
                {
                    if (!ListType.Equals("P"))
                    {
                        sysNoData.Text = errNoPartsInCategory.Text;
                        PartsListGrid.Visible = false;
                    }                 
                    btnSubmit.Visible = false;
                }
                else if (dataSource.Count == 0)
                {
                    sysNoData.Text = errNoPartsInCategory.Text;
                }
            }

            // If view only don't let them add parts to the cart
            //check if option is visible first and only use catalog option if it is. This allows previous logic to function normally 
            // to decide which controls to normally display and this final validation check would override that
            if (AddVisible)
            {
                AddVisible = !((NPBasePage)this.Page).Catalog.ViewOnly;
            }
            if (QtyVisible)
            {
                QtyVisible = !((NPBasePage)this.Page).Catalog.ViewOnly;
            }
            if (btnSubmit.Visible)
            {
                btnSubmit.Visible = !((NPBasePage)this.Page).Catalog.ViewOnly;
            }
        }

        #region event handlers
        // Displays parts for Category Types of "ItemList" "QuantityList", "Sub Category" and "Machine Selector"
        protected void PartsListGrid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {

            NPPart part = (NPPart)e.Row.DataItem;
            HyperLink lnkPartNo = (HyperLink)e.Row.FindControl("lnkPartNo");
            TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
            HyperLink lnkViewMoreDetail = (HyperLink)e.Row.FindControl("lnkViewMoreDetail");
            HyperLink lnkViewMoreDetailQty = (HyperLink)e.Row.FindControl("lnkViewMoreDetailQty");
            
            if (part != null)
            {
                HyperLink lnk = (HyperLink)e.Row.FindControl("lnkImage");
                if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
                {
                    lnk.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
                }
                else
                {
                    lnk.NavigateUrl += Server.UrlEncode(part.PartNo);
                }                
                if (_lShowFullImage)
                {
                    lnk.ImageUrl = _bp.AssetsPath + "catalog/parts/" + part.ThumbNail;
                }
                else if (part.StaticThumbNail.Length > 0)
                {
                    lnk.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
                }
                else
                {
                    lnk.ImageUrl = _bp.ProductThumbImage + part.ThumbNail;
                }

                lnk = (HyperLink)e.Row.FindControl("lnkAddToCart");
                
                if (QtyVisible)
                {
                    lnk.Visible = false;
                }
                else if (part.InProdLine || part.AggregateType == AggregatePartType.Variant)
                {
                    lnk.Visible = false;
                    //lnk.ImageUrl = _bp.ResolveImage("icons", "detail.gif");
                    
                    //if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
                    //{
                    //    lnk.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
                    //}
                    //else
                    //{
                    //    lnk.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + Server.UrlEncode(part.PartNo);
                    //}                    
                    //if (part.InProdLine)
                    //{
                    //    lnk.ToolTip = hdnViewDetails.Text;
                    //}
                    //else
                    //{
                    //    lnk.ToolTip = hdnCustomize.Text;
                    //}
                }
                else
                {
                    lnk.ImageUrl = _bp.ResolveImage("buttons", "buy-btn-small.png");
                    lnk.NavigateUrl += Server.UrlEncode(part.PartNo);
                }
                PartPriceDisplay ppdPartPrice = (PartPriceDisplay)e.Row.FindControl("ppdPartPrice");
                //only show volume discounts on quantity display
                ppdPartPrice.ShowVolDiscounts = DisplayType == "Q";
                
                PriceDisplay prcTax = (PriceDisplay)e.Row.FindControl("prcTax");
                if (ppdPartPrice != null && prcTax != null)
                {
                    prcTax.Price = ppdPartPrice.Tax;
                }

                lnkViewMoreDetail.ImageUrl = _bp.ResolveImage("icons", "detail.gif");
                lnkViewMoreDetailQty.ImageUrl = _bp.ResolveImage("icons", "detail.gif");
                lnkViewMoreDetail.NavigateUrl += Server.UrlEncode(part.PartNo);
                lnkViewMoreDetailQty.NavigateUrl += Server.UrlEncode(part.PartNo);
                if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
                {
                    lnkViewMoreDetail.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
                    lnkViewMoreDetailQty.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
                }

                // variants can not be added in quantity since there is no possibility of correctly synching them to B1
                if (txtQty != null && !part.InProdLine && part.AggregateType != AggregatePartType.Variant)
                {
                    txtQty.Visible = part.AggregateType != AggregatePartType.Variant;
                }
                else if (part.InProdLine || part.AggregateType == AggregatePartType.Variant)
                {
                    txtQty.Visible = false;

                    lnkViewMoreDetail.Visible = true;
                    lnkViewMoreDetailQty.Visible = true;
                }

                // If backorders are not allowed, hide the buy button
                if (!CanBeOrdered(part))
                {
                    lnk.Visible = false;
                    txtQty.Visible = false;
                    lnkViewMoreDetail.Visible = true;
                    lnkViewMoreDetailQty.Visible = true;
                }
            }

            if (lnkPartNo != null)
            {
                if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
                {
                    lnkPartNo.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
                }
                else
                {
                    lnkPartNo.NavigateUrl = "~/catalog/partdetail.aspx?partno=" + Server.UrlEncode(part.PartNo);
                }
            }
        }


        // Displays parts list for Category Type of "Picture Browser"
        //protected global::System.Web.UI.WebControls.Label partNumber;

        protected void PartsListRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            NPPart part = (NPPart)e.Item.DataItem;
            //partNumber.Text = part.PartNo;
            HyperLink partImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
            if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
            {
                partImageLink.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
            }
            else
            {
                partImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + Server.UrlEncode(part.PartNo);
            }

            if (_iShowFullImage)
            {
                partImageLink.ImageUrl = _bp.AssetsPath + "catalog/parts/" + part.ThumbNail;
            }
            else if (part.StaticThumbNail.Length > 0)
            {
                partImageLink.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
            }
            else
            {
                partImageLink.ImageUrl = _bp.ProductPreviewImage + part.ThumbNail;
            }

            partImageLink = ((HyperLink)e.Item.FindControl("lnkPartName"));
            partImageLink.Text = part.PartName;
            if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
            {
                partImageLink.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
            }
            else
            {
                partImageLink.NavigateUrl += part.PartNo;
            }
        }

        
        protected void PartDetailListRepeater_ItemCommand(object source, DataListCommandEventArgs e)
        {
            DropDownList ddlChildParts = (DropDownList)e.Item.FindControl("ddlChildParts");

            switch (e.CommandName)
            {
                case "AddToCart":
                    Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(ddlChildParts.SelectedValue));
                    break;
                case "ViewDetails":
                   Response.Redirect("~/catalog/partdetail.aspx?partno=" + Server.UrlEncode(ddlChildParts.SelectedValue));
                    break;
                case "AddToWishList":
                    Response.Redirect("~/common/accounts/savedlists.aspx?addpartno=" + Server.UrlEncode(ddlChildParts.SelectedValue));
                    break;
            }
        }

        // Displays part list for Category Type "Item Detail List"
        protected void PartDetailListRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            NPPart part = (NPPart)e.Item.DataItem;

            HyperLink partImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
            if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
            {
                partImageLink.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
            }
            else
            {
                partImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + Server.UrlEncode(part.PartNo);
            }

            if (_iShowFullImage)
            {
                partImageLink.ImageUrl = _bp.AssetsPath + "catalog/parts/" + part.ThumbNail;
            }
            else if (part.StaticThumbNail.Length > 0)
            {
                partImageLink.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
            }
            else
            {
                partImageLink.ImageUrl = _bp.ProductPreviewImage + part.ThumbNail;
            }

            partImageLink = ((HyperLink)e.Item.FindControl("lnkPartName"));
            partImageLink.Text = part.PartName;
            if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
            {
                partImageLink.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
            }
            else
            {
                partImageLink.NavigateUrl += part.PartNo;
            }

            Literal ltlDescription = (Literal)e.Item.FindControl("ltlDescription");
            Literal ltlPartCode = (Literal)e.Item.FindControl("ltlPartCode");
            DropDownList ddlChildParts = (DropDownList)e.Item.FindControl("ddlChildParts");
            Panel pnlOnSale = (Panel)e.Item.FindControl("pnlOnSale");
            System.Web.UI.WebControls.Image imgOnSale = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgOnSale");
            Panel pnlNotOnSale = (Panel)e.Item.FindControl("pnlNotOnSale");
            PriceDisplay prcOriginalPrice = (PriceDisplay)e.Item.FindControl("prcOriginalPrice");
            PartPriceDisplay ppdSalePrice = (PartPriceDisplay)e.Item.FindControl("ppdSalePrice");

            ltlDescription.Text = part.Description;
            ltlPartCode.Text = part.PartCode;

            // If this part number is on sale.
            if (ppdSalePrice.Price.HasValue && part.OriginalPrice > ppdSalePrice.Price.Value)
            {
                pnlOnSale.Visible = true;
                imgOnSale.ImageUrl = _bp.ResolveImage("images", "onsale.gif");
                if (_bp.TheTheme.AddTaxToPrice)
                {
                    prcOriginalPrice.Price = part.OriginalPrice + NPTax.GetPartTax(part, part.OriginalPrice, _bp.AccountID, _bp.TaxDestination);
                }
                else
                {
                    prcOriginalPrice.Price = part.OriginalPrice;
                }
            }
            else
            {
                pnlNotOnSale.Visible = true;
            }

            btnSubmit.Visible = true;

            ImageButton ibtnAddToCart = (ImageButton)e.Item.FindControl("ibtnAddToCart");
            ImageButton ibtnAddToWishList = (ImageButton)e.Item.FindControl("ibtnAddToWishList");
            HyperLink ilnkViewMoreDetail = (HyperLink)e.Item.FindControl("ilnkViewMoreDetail");

          

           
           
           





            ilnkViewMoreDetail.ImageUrl = _bp.ResolveImage("icons", "detail.gif");
            ilnkViewMoreDetail.NavigateUrl += Server.UrlEncode(part.PartNo);
            if (part.FriendlyUrl != "" && _bp.Config.SEO.EnableUrlRewrite)
            {
                ilnkViewMoreDetail.NavigateUrl = "~/" + Server.UrlEncode(part.FriendlyUrl.ToString());
            }

             // Part is a master part so render a DropDownList containing the child parts.
            if (part.InProdLine)
            {
                FillChildPartsDropDownList(ddlChildParts, part.PartNo);
                ddlChildParts.Visible = true;
                
              ibtnAddToCart.Visible = true;
            ibtnAddToCart.ImageUrl = _bp.ResolveImage("buttons", "buy-btn-small.png");

                ibtnAddToWishList.Visible = true;
            ibtnAddToWishList.ImageUrl = _bp.ResolveImage("buttons", "wishlist.jpg");
           
            }

            // Part is a variant, so do not add directly to cart, instead go to partdetail page to select options
            else if (part.AggregateType == AggregatePartType.Variant)
            {
                ibtnAddToCart.Visible = false;
                ddlChildParts.Visible = false;
                ibtnAddToWishList.Visible = false;
                ilnkViewMoreDetail.Visible = true;
            }
            else
            {

                ibtnAddToCart.PostBackUrl = "~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(part.PartNo);
                ibtnAddToWishList.PostBackUrl = "~/common/accounts/savedlists.aspx?addpartno=" + Server.UrlEncode(part.PartNo);

                ibtnAddToWishList.Visible = _showAddToWishListButton;
                ibtnAddToWishList.ImageUrl = _bp.ResolveImage("buttons", "wishlist.jpg");

                ddlChildParts.Visible = false;
            }

            // If backorders are not allowed, hide the buy button
            if (!CanBeOrdered(part))
            {
                ibtnAddToCart.Visible = true;

                ilnkViewMoreDetail.Visible = true;
            }
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            CommandEventArgs cea = (CommandEventArgs)args;
            if (cea != null)
            {
                if (cea.CommandName.Equals("MachineCode"))
                {
                    if (npmblock.Visible)
                    {
                        BindGrid();
                    }
                }
            }
            return true;
        }

        protected void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //int qty = 0;
            //TextBox txt = null;
            //NPOrder order = new NPOrder(bp.UserID, bp.SessionID);
            //if(!order.Initialized)
            //{
            //    order.OrderCurrency = NPConnection.GetConfigDB("NetPoint","BaseCurrency");
            //    order.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID);
            //}
            bool success = false;
            //foreach(GridViewRow gvr in PartsListGrid.Rows)
            //{
            //    txt = (TextBox)gvr.FindControl("txtQty");
            //    if(txt != null)
            //    {
            //        qty = 0;
            //        try
            //        {
            //            qty = Convert.ToInt32(txt.Text);
            //        }
            //        catch
            //        {
            //        }
            //        if(qty > 0)
            //        {
            //            order.AddPart(txt.ToolTip, qty, bp.Catalog.CatalogCode, "", bp.PriceList);
            //            success = true;
            //        }
            //    }
            //}

            success = AddPartsToOrder();
            if (success)
            {
                Response.Redirect("~/commerce/cart.aspx", true);
            }
        }

        protected void PartsListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Add code to add the quantity parts to the order if paging.
            AddPartsToOrder();
            PartsListGrid.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        #endregion

        private bool AddPartsToOrder()
        {
            int qty = 0;
            TextBox txt = null;
            NPOrder order = new NPOrder(_bp.UserID, _bp.SessionID);
            if (!order.Initialized)
            {
                order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                order.CreateDefaultOrder(_bp.UserID, _bp.AccountID, _bp.SessionID);
            }
            bool success = false;
            foreach (GridViewRow gvr in PartsListGrid.Rows)
            {
                txt = (TextBox)gvr.FindControl("txtQty");
                if (txt != null)
                {
                    qty = 0;
                    try
                    {
                        qty = Convert.ToInt32(txt.Text);
                    }
                    catch
                    {
                    }
                    if (qty > 0)
                    {
                        order.AddPart(txt.ToolTip, qty, _bp.Catalog.CatalogCode, "", _bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                        success = true;
                    }
                }
            }
            return success;
        }     

        protected void PartsListGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TextBox qty = (TextBox)PartsListGrid.Rows[e.RowIndex].FindControl("txtQty");
            HyperLink part = (HyperLink)PartsListGrid.Rows[e.RowIndex].FindControl("lnkPartNo");

            if (qty != null && part != null)
            {
                Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + part.Text + "&quantity=" + qty.Text);
            }
        }


        private bool CanBeOrdered(NPPart part)
        {
            bool canBeOrdered = false;

            part.Fetch();

            // Allow ordering if the part is in stock,
            // OR if the part uses global settings, and thoses settings allow backorders with or without warning
            // OR if the part uses its own settings, and those settings allow backorders with or without warning
            if (part.InStock
                || (!part.InStock && part.InheritInventorySettings
                    && (NPConnection.GetConfigDB("Inventory", "BackorderAction").ToUpper().Equals("Y")
                        || NPConnection.GetConfigDB("Inventory", "BackorderAction").ToUpper().Equals("W")))
                || (!part.InStock && !part.InheritInventorySettings && (part.BackorderAction.Equals("Y") || part.BackorderAction.Equals("W")))
                )
            {
                canBeOrdered = true;
            }

            return canBeOrdered;
        }
    }
}
