namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;

    /// <summary>
    ///		Summary description for PartAttributesBlock.
    /// </summary>
    public partial class PartNotesBlock : System.Web.UI.UserControl
    {

        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        public Unit BorderWidth
        {
            set { NotesPanel.BorderWidth = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (_part != null)
            {
                foreach (NPPartNote pn in _part.Notes)
                {
                    Label lbl = new Label();
                    lbl.Text = pn.PartNotes.Replace(System.Environment.NewLine, System.Environment.NewLine + "<br>") + "<br><br>";
                    lbl.CssClass = "npbody";
                    NotesPanel.Controls.Add(lbl);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
