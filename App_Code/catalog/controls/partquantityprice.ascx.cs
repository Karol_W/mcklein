﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;
using netpoint.common.controls;

namespace netpoint.catalog.controls
{
    public partial class partquantityprice : NPBaseControl
    {
        private string _partNo;
        private NPPart _Part;
        private bool? _showTax;
        private bool? _addTax;
        decimal _tax;


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Part != null && Part.DisplayOption.VolumeDiscount)
            {
                ArrayList volPricing = NPPartPricing.GetVolumeDiscounts(_partNo, BasePage.AccountID, BasePage.PriceList, DateTime.Now, BasePage.ConnectionString); ;

                if (volPricing != null && volPricing.Count > 0)
                {
                    rptQtyPrice.DataSource = volPricing;
                    rptQtyPrice.DataBind();
                }
                else
                {
                    this.Visible = false;
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        protected void PriceBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                NPPartPricing prcObj = (NPPartPricing)e.Item.DataItem;

                Literal qty = (Literal)e.Item.FindControl("ltlQty");
                qty.Text = prcObj.MinQuantity.ToString();

                PriceDisplay prcPrice = (PriceDisplay)e.Item.FindControl("prcPrice");
                decimal price = prcObj.CalculatePrice(prcObj.BasePricing.PriceFactorValue);

                if (AddTaxToPrice)
                {
                    price += GetTax(price);
                }
                prcPrice.Price = price;
            }
        }
        
        public string PartNo
        {
            get { return _partNo; }
            set 
            { 
                _partNo = value;
                if (_Part == null || (_Part != null && _Part.PartNo != _partNo))
                {
                    _Part = new NPPart(_partNo);
                }            
            }
        }

        public NPPart Part
        {
            get 
            {
                if (_Part == null)
                {
                    _Part = new NPPart(_partNo);
                }
                return _Part; 
            }
            set
            {
                _Part = (NPPart)value;
                _partNo = _Part.PartNo;
            }
            
        }

        public bool ShowItemizedTax
        {
            get { return _showTax ?? BasePage.TheTheme.ShowItemizedTax; }
            set { _showTax = value; }
        }

        public bool AddTaxToPrice
        {
            get { return _addTax ?? BasePage.TheTheme.AddTaxToPrice; }
            set { _addTax = value; }
        }

        public decimal GetTax(decimal _price)
        {            
            _tax = NPTax.GetPartTax(Part, _price, BasePage.AccountID, BasePage.TaxDestination);
            return _tax;            
        }
    }
}