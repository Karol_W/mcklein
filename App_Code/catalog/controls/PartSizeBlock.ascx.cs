namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for PartAttributesBlock.
    /// </summary>
    public partial class PartSizeBlock : System.Web.UI.UserControl
    {
        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        public Unit BorderWidth
        {
            set { pnlMain.BorderWidth = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (_part == null)
            {
                pnlMain.Visible = false;
            }
            else
            {
                //dimensions========================
                if (_part.SizeUnitAbbr == null)
                {
                    pnlSize.Visible = false;
                }
                else
                {
                    sysDepth.Text = _part.Depth.ToString();
                    sysDepthUnits.Text = _part.SizeUnitAbbr;
                    sysWidth.Text = _part.Width.ToString();
                    sysWidthUnits.Text = _part.SizeUnitAbbr;
                    sysHeight.Text = _part.Height.ToString();
                    sysHeightUnits.Text = _part.SizeUnitAbbr;
                }
                //weight======================
                if (_part.WeightUnitAbbr == null)
                {
                    pnlWeightAbbr.Visible = false;
                }
                else
                {
                    sysWeight.Text = _part.Weight.ToString("0.##");
                    sysWeightUnits.Text = _part.WeightUnitAbbr;
                }
                //volume===================
                if (_part.VolumeUnitAbbr == null)
                {
                    pnlVolume.Visible = false;
                }
                else
                {
                    sysVolume.Text = _part.Volume.ToString();
                    sysVolumeUnits.Text = _part.VolumeUnitAbbr;
                }
                //area======================
                if (_part.AreaUnitAbbr == null)
                {
                    pnlArea.Visible = false;
                }
                else
                {
                    sysAreaQty.Text = _part.Area.ToString();
                    sysAreaUnit.Text = _part.AreaUnitAbbr;
                }
                if (_part.SizeUnitAbbr == null && _part.WeightUnitAbbr == null && _part.VolumeUnitAbbr == null && _part.AreaUnitAbbr == null)
                {
                    pnlMain.Visible = false;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
