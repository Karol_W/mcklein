namespace netpoint.catalog.controls
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using netpoint.api;
    using netpoint.api.catalog;
    using netpoint.api.commerce;
    using netpoint.api.data;
    using netpoint.api.utility;

    /// <summary>
    ///		Summary description for ProdLineMatrix.
    /// </summary>
    public partial class ProdLineMatrix : System.Web.UI.UserControl
    {
        private string _partNo;
        private NPBasePage _bp;

        public string PartNo
        {
            get { return _partNo; }
            set { _partNo = value; }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //this.Visible = false;
            tblMatrix.Visible = Convert.ToBoolean(sysStartExpanded.Text);
            if (!this.Visible) { return; }
            if (_partNo == null)
            {
                _partNo = Request.QueryString["PartNo"];
                if (_partNo == null)
                {
                    //DIE!!!
                }
            }
            _bp = (NPBasePage)this.Page;
            ArrayList products = NPPart.GetProductLine(_partNo, _bp.AccountID, _bp.PriceList, _bp.ConnectionString, _bp.AppCulture);

            if (products.Count <= 0)
            {
                this.Visible = false;
            }
            else
            {
                if (!IsPostBack)
                {
                    NPPart p = (NPPart)products[0];
                    ArrayList attributes = p.GetAttributeObjects();
                    DataTable listSource = NPPartAttribute.AttributesAndDimensions(attributes, _bp.ConnectionString);
                    if (listSource.Rows.Count >= 2)
                    {
                        BindList(ddlXAttribute, listSource);
                        BindList(ddlYAttribute, listSource);
                        ddlXAttribute.SelectedIndex = 1;
                    }
                    else
                    {
                        this.Visible = false;
                    }
                }
                else if (ddlXAttribute.SelectedIndex == ddlYAttribute.SelectedIndex)
                {
                    if (ddlXAttribute.Items.Count >= 2)
                    {
                        if (ddlXAttribute.SelectedIndex.ToString() != hdnXValue.Value)
                        {
                            ddlYAttribute.SelectedIndex = ddlXAttribute.SelectedIndex > 0 ? ddlXAttribute.SelectedIndex - 1 : ddlXAttribute.SelectedIndex + 1;
                        }
                        else
                        {
                            ddlXAttribute.SelectedIndex = ddlYAttribute.SelectedIndex > 0 ? ddlYAttribute.SelectedIndex - 1 : ddlYAttribute.SelectedIndex + 1;
                        }
                    }
                }
                hdnYValue.Value = ddlYAttribute.SelectedIndex.ToString();
                hdnXValue.Value = ddlXAttribute.SelectedIndex.ToString();
                BindMatrix(products);
            }

            clpGeneral.ImageUrl = _bp.ResolveImage("icons", "collapse.gif");
            expGeneral.ImageUrl = _bp.ResolveImage("icons", "expand.gif");
            btnAdd.ImageUrl = _bp.ResolveImage("buttons", "addtocart.gif");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnAdd.Click += new System.Web.UI.ImageClickEventHandler(btnAdd_Click);
        }
        #endregion

        private void BindMatrix(ArrayList products)
        {
            theMatrix.Rows.Clear();

            if (hdnXValue.Value.ToString() != "-1" && hdnXValue.Value.ToString() != "-1")
            {
                string X = ddlXAttribute.Items[Convert.ToInt32(hdnXValue.Value)].Value;
                string Y = ddlYAttribute.Items[Convert.ToInt32(hdnYValue.Value)].Value;
                string[] XArray = X.Split(',');
                string[] YArray = Y.Split(',');
                string xDimension = "Dimension" + XArray[1];
                string yDimension = "Dimension" + YArray[1];

                DataSet mySet = NPPart.GetProdLineAttributes(_partNo, X, Y, _bp.ConnectionString);
                DataTable XList = mySet.Tables[0];
                DataTable YList = mySet.Tables[1];
                DataTable partsAndValues = mySet.Tables[2];
                HtmlTableRow headerRow = new HtmlTableRow();
                HtmlTableCell leftCell = new HtmlTableCell();

                headerRow.Attributes["class"] = "npheader";
                headerRow.Cells.Add(leftCell);
                ArrayList values = new ArrayList();
                bool addIt;
                foreach (DataRow Yrow in YList.Rows)
                {
                    addIt = true;
                    if (values.Count > 0)
                    {
                        foreach (string s in values)
                        {
                            if (s == Yrow[yDimension].ToString())
                            {
                                addIt = false;
                            }
                        }
                    }
                    if (addIt)
                    {
                        HtmlTableCell headerCell = new HtmlTableCell();
                        headerCell.Attributes["align"] = "center";
                        headerCell.InnerText = Yrow[yDimension].ToString();
                        headerRow.Cells.Add(headerCell);
                        values.Add(Yrow[yDimension].ToString());
                    }
                }
                values.Clear();
                theMatrix.Rows.Add(headerRow);
                foreach (DataRow Xrow in XList.Rows)
                {
                    addIt = true;
                    if (values.Count > 0)
                    {
                        foreach (string s in values)
                        {
                            if (s == Xrow[xDimension].ToString())
                            {
                                addIt = false;
                            }
                        }
                    }
                    if (addIt)
                    {
                        HtmlTableRow innerRow = new HtmlTableRow();
                        HtmlTableCell marginCell = new HtmlTableCell();
                        marginCell.Attributes["class"] = "npheader";
                        marginCell.InnerText = Xrow[xDimension].ToString();
                        innerRow.Cells.Add(marginCell);
                        ArrayList usedvalues = new ArrayList();
                        bool used;
                        foreach (DataRow Yrow in YList.Rows)
                        {
                            used = false;
                            if (usedvalues.Count > 0)
                            {
                                foreach (string z in usedvalues)
                                {
                                    if (z == Yrow[yDimension].ToString())
                                    {
                                        used = true;
                                    }
                                }
                            }
                            if (!used)
                            {
                                HtmlTableCell innerCell = new HtmlTableCell();
                                DataRow[] yrows = partsAndValues.Select(yDimension + " = '" + Yrow[yDimension].ToString().Replace("'", "") + "'");
                                if (yrows.Length > 0)
                                {
                                    DataRow[] xrows = partsAndValues.Select(xDimension + " = '" + Xrow[xDimension].ToString().Replace("'", "") + "'");
                                    bool exists = false;
                                    string partNo = "";
                                    foreach (DataRow partY in yrows)
                                    {
                                        foreach (DataRow partX in xrows)
                                        {
                                            if (partY["PartNo"].ToString() == partX["PartNo"].ToString())
                                            {
                                                exists = true;
                                                partNo = partX["partNo"].ToString();
                                            }
                                        }
                                    }
                                    if (exists)
                                    {
                                        innerCell = CreateMatrixCell(true, partNo);
                                    }
                                }
                                if (innerCell.Controls.Count < 1)
                                {
                                    innerCell = CreateMatrixCell(false, "");
                                }
                                innerRow.Cells.Add(innerCell);
                                usedvalues.Add(Yrow[yDimension].ToString());
                            }
                        }
                        theMatrix.Rows.Add(innerRow);
                        values.Add(Xrow[xDimension].ToString());
                    }
                }
            }
        }

        private HtmlTableCell CreateMatrixCell(bool text, string cellID)
        {
            HtmlTableCell tc = new HtmlTableCell();
            tc.Attributes["align"] = "center";
            if (text)
            {
                TextBox box = new TextBox();
                box.ID = cellID;
                box.Width = 40;
                box.Text = "0";
                box.Style["text-align"] = "center";
                tc.Controls.Add(box);
            }
            else
            {
                tc.InnerText = "N/A";
            }
            return tc;
        }

        private DataTable CreateList(NPPartAttribute pa, ArrayList al)
        {
            return NPPartAttribute.AttributesAndDimensions(al, _bp.ConnectionString);
        }

        private void BindList(System.Web.UI.WebControls.ListControl ddl, DataTable attributes)
        {
            ddl.DataSource = attributes;
            ddl.DataValueField = "DimensionIndex";
            ddl.DataTextField = "AttributeName";
            ddl.DataBind();
        }

        protected void clpGeneral_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            clpGeneral.Visible = false;
            expGeneral.Visible = true;
            tblMatrix.Visible = false;
        }

        protected void expGeneral_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            clpGeneral.Visible = true;
            expGeneral.Visible = false;
            tblMatrix.Visible = true;
        }

        protected void btnAdd_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Hashtable ht = new Hashtable();
            foreach (HtmlTableRow row in theMatrix.Rows)
            {
                foreach (HtmlTableCell cell in row.Cells)
                {
                    foreach (System.Web.UI.Control control in cell.Controls)
                    {
                        if (control.GetType() == typeof(System.Web.UI.WebControls.TextBox))
                        {
                            TextBox box = (TextBox)control;
                            int quantity = 0;
                            try
                            {
                                quantity = Convert.ToInt32(box.Text);
                            }
                            catch
                            {

                            }
                            if (quantity > 0)
                            {
                                string[] boxID = box.ID.Split('_');
                                ht.Add(boxID[boxID.Length - 1], quantity);
                            }
                        }
                    }
                }
            }
            bool success = false;
            NPOrder order = new NPOrder(_bp.UserID, _bp.SessionID);
            if (!order.Initialized)
            {
                order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                order.CreateDefaultOrder(_bp.UserID, _bp.AccountID, _bp.SessionID);
            }
            foreach (object key in ht.Keys)
            {
                order.AddPart(key.ToString(), (int)ht[key], _bp.Catalog.CatalogCode, "", _bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                success = true;
            }
            if (success)
            {
                Response.Redirect("~/commerce/cart.aspx", true);
            }
        }
    }
}

