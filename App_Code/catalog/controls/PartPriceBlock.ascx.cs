namespace netpoint.catalog.controls
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.api.commerce;
    using netpoint.classes;
    using System.Resources;
    using netpoint.api.account;
    using netpoint.api;
    using netpoint.api.utility;
    using netpoint.api.common;
    using netpoint.api.data;
    using System.Web.UI;

    /// <summary>
    ///		Summary description for PartAttributesBlock.
    /// </summary>
    public partial class PartPriceBlock : System.Web.UI.UserControl
    {

        private NPPart _part;

        public Unit BorderWidth
        {
            set { PricePanel.BorderWidth = value; }
        }

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        public decimal VariantPrice
        {
            get { return Convert.ToDecimal(this.ViewState["variantprice"]); }
            set
            {
                this.ViewState["variantprice"] = value;
                sysBasePrice.Price = value;
            }
        }
       
        protected void Page_Load(object sender, System.EventArgs e)
        {

            NPBasePage bp = (NPBasePage)Page;
          

            sysBasePrice.Part = _part;
            decimal origprice = _part.OriginalPrice;
            if (origprice != 0)
            {
                pnlOriginalPrice.Visible = true;
                ltlOriginalPrice.Text = hdnOriginally.Text;
                if (bp.TheTheme.AddTaxToPrice)
                {
                    sysOriginalPrice.Price = origprice + NPTax.GetPartTax(_part, origprice, bp.AccountID, bp.TaxDestination);
                }
                else
                {
                    sysOriginalPrice.Price = origprice;
                }
            }

            string _backOrderAction = _part.BackorderAction;
            if (_part.InheritInventorySettings)
            {
                _backOrderAction = NPConnection.GetConfigDB("Inventory", "BackorderAction");
            }

            if (_backOrderAction == "W" || _backOrderAction == "Y")
            {                
                if (_part.InStock)
                {
                    ltlInStock.Visible = true;
                }
                else if (_backOrderAction == "W")
                {
                    ltlNotInStock.Visible = true;
                }
            }

            if (bp.PermissionController.UserInRole("superuser") ||
                bp.PermissionController.UserInRole("inventoryadmin") ||
                bp.PermissionController.UserInRole("inventoryuser"))
            {
                int onHand = _part.GetInventoryCount(bp.AccountID);
                sysInventoryCount.Text = hdnCount.Text + " " + onHand.ToString();
                sysInventoryCount.Visible = true;
            }

            sysUnits.Text = _part.SalesUnitText;
            if (_part.SalesQty > 1)
            {
                sysUnitQuantity.Text = _part.SalesQty.ToString();
                lblUnitSeperator.Visible = true;
            }
            if (_part.ReleaseDate != DateTime.MinValue && _part.ReleaseDate > DateTime.Now)
            {
                sysReleaseDate.Text = _part.ReleaseDate.ToString("yyyy-MMM-dd");
                sysReleaseDate.Visible = true;
                ltlReleaseDate.Visible = true;
            }

            AddButton.ImageUrl = bp.ResolveImage("buttons", "buy-btn-small.png");
            AddButton.Visible = !bp.Catalog.ViewOnly;
            if (AddButton.Visible)
            {
                //check backorder option to see if add button should be displayed
                if (_backOrderAction == "N" && !_part.InStock)
                {
                    ltlNotAvailable.Visible = true;
                    AddButton.Visible = false;
                }
            }
            
            imgWishList.Visible = Convert.ToBoolean(NPConnection.GetConfigDB("Commerce", "WishList", bool.TrueString, bp.ConnectionString));

            if (bp.TheTheme.V7Compatible)
            {
                imgWishList.ImageUrl = bp.ResolveImage("buttons", "addtowishlist.gif");
            }
            else
            {
                imgWishList.ImageUrl = bp.ResolveImage("buttons", "save-btn-small.png");
            }
            
            pnlOrderLineNotes.Visible = _part.DisplayOption.OrderNotes;
            if (_part.DisplayOption.QuantityDDL || _part.DisplayOption.QuantityTB)
            {
                AddButton.Visible = false;
                pnlOrderLineNotes.Visible = false;
            }

            AddButton.Visible = false;
            imgWishList.Visible = false;
        }

        public void AddToCart(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (_part.AggregateType == AggregatePartType.Variant && Request.QueryString["ordermasterid"] == null)
            {
                Response.Redirect("~/commerce/cart.aspx?AddVariantNo=" + Server.UrlEncode(_part.PartNo));
            }
            else if (Request.QueryString["ordermasterid"] != null)
            {
                //	if we have an ordermasterid, we got here from the cart and are reconfiguring a variant
                Response.Redirect("~/commerce/cart.aspx");
            }
            else
            {
                if (tbNote.Text != "")
                {
                    Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(_part.PartNo) + "&notes=" + Server.UrlEncode(tbNote.Text));
                }
                else
                {
                    Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(_part.PartNo));
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddButton.Click += new System.Web.UI.ImageClickEventHandler(this.AddButton_Click);
        }

        #endregion

        private void AddButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

        }

        protected void AddToWishlist(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (_part.AggregateType == AggregatePartType.Variant)
            {
                Response.Redirect("~/common/accounts/savedlists.aspx?AddPartNo=" + Server.UrlEncode(_part.PartNo) + "&variant=Y");
            }
            else
            {
                Response.Redirect("~/common/accounts/savedlists.aspx?AddPartNo=" + Server.UrlEncode(_part.PartNo));
            }
        }
    }
}
