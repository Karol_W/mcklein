namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.classes;
    using netpoint.api;
    using netpoint.api.data;
    using netpoint.common.controls;

    /// <summary>
    ///		PartCrossBlock - this displays a part's cross sell information, which can be parts or categories.
    /// </summary>
    public partial class PartCrossBlock : System.Web.UI.UserControl
    {
        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            int thumbnailSize = bp.TheTheme.ProductThumbSize > 0 ? bp.TheTheme.ProductThumbSize : 50;
            string CategoriesImagePath = bp.VirtualPath + "common/controls/thumbnail.ashx?size=" + thumbnailSize + "&image=" + bp.AssetsPath + "catalog/categories/";
            dlCrossRepeater.DataSource = _part.GetAvailableCrossObjects(bp.ProductThumbImage, CategoriesImagePath);
            dlCrossRepeater.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dlCrossRepeater.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlCrossRepeater_ItemDataBound);

        }
        #endregion

        private void dlCrossRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            NPBasePage bp = (NPBasePage)this.Page;
            NPPartCross nppc = (NPPartCross)e.Item.DataItem;
            string thelink = "";
            HyperLink lnkImage = (HyperLink)e.Item.FindControl("lnkImage");
            HyperLink lnkValue = (HyperLink)e.Item.FindControl("lnkValue");
            HyperLink lnkName = (HyperLink)e.Item.FindControl("lnkName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            HyperLink lnkAddToCart = (HyperLink)e.Item.FindControl("lnkAddToCart");
            PartPriceDisplay ppdPrice = (PartPriceDisplay)e.Item.FindControl("ppdPrice");
            Label lblRefDesc = (Label)e.Item.FindControl("lblRefDesc");


            if (lnkAddToCart != null)
            {
                lnkAddToCart.ImageUrl = bp.ResolveImage("icons", "addtocart.gif");
            }
            if (lnkImage != null)
            {
                if (nppc.CrossType == "P")
                {
                    thelink = "~/catalog/partdetail.aspx?PartNo=" + Server.UrlEncode(nppc.CrossValue);
                    if (nppc.ShowPrice)
                    {
                        ppdPrice.PartNo = nppc.CrossValue;
                        ppdPrice.Visible = true;
                        lnkAddToCart.NavigateUrl = "~/commerce/cart.aspx?AddPartNo=" + Server.UrlEncode(nppc.CrossValue);
                        NPPart p = new NPPart(nppc.CrossValue);
                        p.Fetch();
                        if (p.Initialized)
                        {
                            if (p.InProdLine)
                            {
                                lnkAddToCart.Visible = false;
                            }
                            else
                            {
                                lnkAddToCart.Visible = true;
                            }
                        }
                        else
                        {
                            lnkAddToCart.Visible = false;
                        }
                    }
                }
                else if (nppc.CrossType == "C")
                {
                    thelink = "~/catalog/partlist.aspx?CategoryID=" + Server.UrlEncode(nppc.CrossValue);
                    lnkAddToCart.Visible = false;
                }
                if (nppc.ShowImage)
                {
                    lnkImage.NavigateUrl = thelink;
                    lnkImage.ImageUrl = nppc.RefImage;
                    lnkImage.Visible = true;
                }
                if (nppc.ShowPartNo)
                {
                    lnkValue.NavigateUrl = thelink;
                    lnkValue.Text = nppc.CrossValue;
                    lnkValue.Visible = true;
                }
                if (nppc.ShowName)
                {
                    lnkName.NavigateUrl = thelink;
                    lnkName.Text = nppc.RefName;
                    lnkName.Visible = true;
                }
                if (nppc.ShowDescription) // Show the item or category's description only if checked.
                {
                    lblRefDesc.Text = nppc.RefDescription;
                    lblRefDesc.Visible = true;
                }
                //Always show the configured cross-sell description
                lblDescription.Text = nppc.Description;
                lblDescription.Visible = true;
            }
        }
    }
}
