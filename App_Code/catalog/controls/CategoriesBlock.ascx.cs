namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class CategoriesBlock : System.Web.UI.UserControl
    {

        #region "Members and Properties"

        public string assetsURL;
        public string virtURL;
        public int ParentID = 0;
        public bool ShowName = false;
        public bool ShowImage = true;
        public bool ShowDescription = false;
        private bool _enabled = true;

        public int CatsPerCol
        {
            set { categoryrepeater.RepeatColumns = value; }
        }

        public RepeatDirection CatsRepeatDirection
        {
            set { categoryrepeater.RepeatDirection = value; }
        }

        public Unit BorderWidth
        {
            set { categoryrepeater.BorderWidth = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        private NPBasePage bp;

        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!Enabled)
            {
                this.Visible = false;
                return;
            }

            bp = (NPBasePage)Page;

            assetsURL = bp.AssetsPath + "catalog/categories/";

            categoryrepeater.DataSource = (ParentID == 0 ? bp.Catalog.GetChildCategories(true) : new NPCatalogCategory(ParentID).GetChildCategories(true));
            categoryrepeater.DataBind();
        }


        public void DetermineRowVars(object sender, DataListItemEventArgs e)
        {
            NPCatalogCategory cat = (NPCatalogCategory)e.Item.DataItem;
            cat.Fetch();
            if (!ShowName)
            {
                e.Item.FindControl("CategoryName").Visible = false;
            }
            if (!ShowImage)
            {
                e.Item.FindControl("CategoryImage").Visible = false;
            }
            if (!ShowDescription)
            {
                e.Item.FindControl("CategoryDescription").Visible = false;
            }
            //overrides
            if (cat.ImageFile == null || cat.ImageFile.Length == 0)
            {
                e.Item.FindControl("CategoryImage").Visible = false;
                e.Item.FindControl("CategoryName").Visible = true;
            }
            if (cat.CategoryName == null || cat.CategoryName.Length == 0)
            {
                e.Item.FindControl("CategoryName").Visible = false;
            }
            if (cat.CategoryName == null || cat.CategoryName.Length == 0)
            {
                e.Item.FindControl("CategoryDescription").Visible = false;
            }
            //links
            if (cat.FriendlyUrl != "" && bp.Config.SEO.EnableUrlRewrite)
            {
                ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/" + cat.FriendlyUrl.ToString();
                ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/" + cat.FriendlyUrl.ToString();
            }
            else
            {
                if (bp is partlist)
                {
                    //if categories block is on partslist page, all category links should go to partslist
                    ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
                    ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
                }
                else
                {
                    if (cat.HasParts)
                    {
                        ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
                        ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
                    }
                    else{
                        ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/catalog/categories.aspx?ParentID=" + cat.CategoryID.ToString();
                        ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/catalog/categories.aspx?ParentID=" + cat.CategoryID.ToString();
                    }                    
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
