namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.api.utility;
    using netpoint.classes;
    using System.Web.UI;
    using System.Collections;
    using netpoint.api.data;

    /// <summary>
    ///		Summary description for PartMediaBlock.
    /// </summary>
    public partial class PartMediaBlock : System.Web.UI.UserControl
    {

        private NPPart _part;
        public bool ShowAlternateLinks = true;
        private bool firstDiv = false;
        private NPBasePage _bp;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        public Unit BorderWidth
        {
            set { MediaPanel.BorderWidth = value; }
        }

        protected void Repeater_Content_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl partDetail = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("partDetailImageContainer");
                

                if (!firstDiv) //First Time through
                {
                    firstDiv = true;             
                    partDetail.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " partDetailImageContainer";
                }
                else
                {                 
                        
                    partDetail.Attributes["class"] = DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + " partDetailImageContainer";
                    partDetail.Attributes["style"] = "display:none";
                }
                //Coursell Images
                System.Web.UI.WebControls.Image MainImage = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image1");
                System.Web.UI.WebControls.Panel MainImage_panel = (System.Web.UI.WebControls.Panel)e.Item.FindControl("Image1_panel");
                System.Web.UI.WebControls.Image Image1s = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image1s");
                System.Web.UI.WebControls.Panel Image1s_panel = (System.Web.UI.WebControls.Panel)e.Item.FindControl("Image1s_panel");
                System.Web.UI.WebControls.Image Image1s1 = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image1s1");
                System.Web.UI.WebControls.Panel Image1s1_panel = (System.Web.UI.WebControls.Panel)e.Item.FindControl("Image1s1_panel");
                System.Web.UI.WebControls.Image Image1s2 = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image1s2");
                System.Web.UI.WebControls.Panel Image1s2_panel = (System.Web.UI.WebControls.Panel)e.Item.FindControl("Image1s2_panel");
                System.Web.UI.WebControls.Image Image1s3 = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image1s3");
                System.Web.UI.WebControls.Panel Image1s3_panel = (System.Web.UI.WebControls.Panel)e.Item.FindControl("Image1s3_panel");

                MainImage.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s.jpg";
                if (File.Exists(Server.MapPath(MainImage.ImageUrl)))
                {
                    MainImage.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s.jpg";
                } else
                {
                    MainImage.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1.jpg";
                }
              //  if (!File.Exists(Server.MapPath(MainImage.ImageUrl)))
              //      {
              //          MainImage.Visible = false;
              //          MainImage_panel.Visible = false;
              //      }
                

                Image1s.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1.jpg";
                if (!File.Exists(Server.MapPath(Image1s.ImageUrl)))
                {
                    Image1s.Visible = false;
                    Image1s_panel.Visible = false;
                }
                Image1s.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1.jpg";
                Image1s1.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1a.jpg";
                if (!File.Exists(Server.MapPath(Image1s1.ImageUrl)))
                {
                    Image1s1.Visible = false;
                    Image1s1_panel.Visible = false;
                }
                Image1s1.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1a.jpg";
                Image1s2.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s1.jpg";
                if (!File.Exists(Server.MapPath(Image1s2.ImageUrl)))
                {
                    Image1s2.Visible = false;
                    Image1s2_panel.Visible = false;
                }
                Image1s2.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s1.jpg";
                Image1s3.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s2.jpg";
                if (!File.Exists(Server.MapPath(Image1s3.ImageUrl)))
                {
                    Image1s3.Visible = false;
                    Image1s3_panel.Visible = false;
                }
                Image1s3.ImageUrl = "~/common/controls/thumbnail.ashx?size=700&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s2.jpg";




                //   Image1s1.ImageUrl = "~/assets/catalog/parts/Promo_Code_Images/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-1s1.jpg";
                //  if(File.Exists(Server.MapPath(Image1s1.ImageUrl))){
                //      Image1s1_panel.Attributes["class"] = " item active";   
                //   }
                //   else {
                MainImage_panel.Attributes["class"] = " item active";   
              //  }

                //if 1s1 exists set it as active

























                //Part Images
                System.Web.UI.WebControls.Image SideImage1 = (System.Web.UI.WebControls.Image)e.Item.FindControl("SideImage1");
                System.Web.UI.WebControls.Image SideImage2 = (System.Web.UI.WebControls.Image)e.Item.FindControl("SideImage2");
                System.Web.UI.WebControls.Image ModelImage = (System.Web.UI.WebControls.Image)e.Item.FindControl("ModelImage");
                System.Web.UI.WebControls.Image SideImage3 = (System.Web.UI.WebControls.Image)e.Item.FindControl("SideImage3");

             
                SideImage1.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-2.jpg";
                if (!File.Exists(Server.MapPath(SideImage1.ImageUrl)))
                {
                    SideImage1.Visible = false;
                }
                SideImage1.ImageUrl = "~/common/controls/thumbnail.ashx?size=350&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-2.jpg";
                SideImage2.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-3.jpg";
                if (!File.Exists(Server.MapPath(SideImage2.ImageUrl)))
                {
                    SideImage2.Visible = false;
                }
                SideImage2.ImageUrl = "~/common/controls/thumbnail.ashx?size=350&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-3.jpg";
                ModelImage.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-7.jpg";
                if (!File.Exists(Server.MapPath(ModelImage.ImageUrl)))
                {
                    ModelImage.Visible = false;
                }
                ModelImage.ImageUrl = "~/common/controls/thumbnail.ashx?size=350&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-7.jpg";
                SideImage3.ImageUrl = "~/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-9.jpg";
                if (!File.Exists(Server.MapPath(SideImage3.ImageUrl)))
                {
                    SideImage3.Visible = false;
                }
                SideImage3.ImageUrl = "~/common/controls/thumbnail.ashx?size=350&image=/assets/catalog/parts/" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "-9.jpg";

                System.Web.UI.HtmlControls.HtmlAnchor  leftCarousel = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("leftCarousel");
                System.Web.UI.HtmlControls.HtmlAnchor rightCarousel = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("rightCarousel");


                System.Web.UI.HtmlControls.HtmlGenericControl carrousel = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("carrousel");
             

                //leftCarousel.HRef = "#carrousel" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();
                //rightCarousel.HRef = "#carrousel" + DataBinder.Eval(e.Item.DataItem, "PartNo").ToString();
                leftCarousel.HRef = "#" + carrousel.ClientID;
                rightCarousel.HRef = "#" + carrousel.ClientID;



                //Get all images that have that partNo and create a div to contain them all for pop up. 
                System.Web.UI.HtmlControls.HtmlGenericControl allPartImages = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("allPartImages");
                System.Web.UI.HtmlControls.HtmlGenericControl sliderthumbs = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("sliderthumbs");
                //sliderthumbs.Attributes["style"] = "display:none";
                sliderthumbs.Attributes["class"] = "partDetailImageContainer col-md-12 hidden-sm hidden-xs";

                //allPartImages.Attributes["style"] = "display:none";
                //allPartImages.Attributes["class"] = "partDetailImageContainer";

                System.Web.UI.HtmlControls.HtmlGenericControl allPartImagesCarousel = (System.Web.UI.HtmlControls.HtmlGenericControl)allPartImages.FindControl("allPartImagesCarrousel");
                System.Web.UI.HtmlControls.HtmlGenericControl myFullCarousel = (System.Web.UI.HtmlControls.HtmlGenericControl)allPartImages.FindControl("myFullCarousel");
                System.Web.UI.HtmlControls.HtmlAnchor leftFullCarousel = (System.Web.UI.HtmlControls.HtmlAnchor)allPartImages.FindControl("leftFullCarousel");
                System.Web.UI.HtmlControls.HtmlAnchor rightFullCarousel = (System.Web.UI.HtmlControls.HtmlAnchor)allPartImages.FindControl("rightFullCarousel");
                leftFullCarousel.HRef = "#" + myFullCarousel.ClientID;
                rightFullCarousel.HRef = "#" + myFullCarousel.ClientID;


                string[] listoFFiles = GetFiles(Server.MapPath("~/assets/catalog/parts/"), DataBinder.Eval(e.Item.DataItem, "PartNo").ToString() + "*", SearchOption.TopDirectoryOnly);
                int count = 0;
                foreach (string file in listoFFiles)
                {
                    System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
                    System.Web.UI.HtmlControls.HtmlGenericControl NewDiv = new System.Web.UI.HtmlControls.HtmlGenericControl();
                    System.Web.UI.HtmlControls.HtmlAnchor aTag = new System.Web.UI.HtmlControls.HtmlAnchor();

                   // NewDiv.ID = count.ToString();
                    NewDiv.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    if(!Path.GetFileName(file).Contains("R.jpg"))
					{


                    aTag.ID = "carousel-selector-" + count;
                    aTag.ClientIDMode = System.Web.UI.ClientIDMode.Static;

                    img.ImageUrl = "~/assets/catalog/parts/" +  Path.GetFileName(file);
                    img.Attributes["class"] = "img-responsive";
                    img.ClientIDMode = System.Web.UI.ClientIDMode.Static;

                    if (count == 0)
                    {
                        NewDiv.Attributes["class"] = "item active";
                        aTag.Attributes["class"] = "selected";
                    }
                    else
                    {
                        NewDiv.Attributes["class"] = "item";
                    }
                    aTag.InnerHtml = "<img class='img-responsive' src='../assets/catalog/parts/" +  Path.GetFileName(file) + "'/>";
                   // aTag.Controls.Add(img);
                    sliderthumbs.Controls.Add(aTag);

                    NewDiv.Attributes["data-slide-number"] = count.ToString();
                    allPartImagesCarousel.Controls.Add(NewDiv);
                    NewDiv.Controls.Add(img);
                    //allPartImages.
                    count++;
					}

               }
            }

        }

        private static string[] GetFiles(string path, string searchPattern, SearchOption searchOption)
        {
            return System.IO.Directory.GetFiles(path, searchPattern, searchOption);
        }
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (_part != null)
            {
                _bp = (NPBasePage)Page;

                Page.ClientScript.RegisterClientScriptBlock(Page.ClientScript.GetType(), "partswap", GetScript());

                if (!Page.IsPostBack)
                {

                    if (_part.ThumbNail.Length > 0)
                    {
                        imgPart.ImageUrl = _bp.ProductPreviewImage + _part.ThumbNail;
                        //imgPart.Visible = File.Exists(Server.MapPath(_bp.AssetsPath + "catalog/parts/" + _part.ThumbNail));
                        imgPart.Visible = false;
                        sysMMImageLink.Attributes.Add("onClick", "popPartImage('" + _bp.ProductsPath + _part.ThumbNail + "');return false;");
                    }
                    else if (_part.StaticThumbNail.Length > 0)
                    {
                        imgPart.ImageUrl = _bp.ProductsPath + _part.StaticThumbNail;
                       // imgPart.Visible = File.Exists(Server.MapPath(imgPart.ImageUrl));
                        imgPart.Visible = false;
                        sysMMImageLink.Attributes.Add("onClick", "popPartImage('" + _bp.ProductsPath + _part.StaticThumbNail + "');return false;");                        
                    }
                    else
                    {
                        imgPart.Visible = false;
                    }
                    imgPart.AlternateText = "Click to view full size of image of " + _part.PartName;
                    imgPart.ToolTip = "Click to view full size image of " + _part.PartName;

                    HtmlTable ht = new HtmlTable();
                    foreach (NPPartMedia pm in _part.Media)
                    {
                        HyperLink hl = new HyperLink();
                        hl.Text = pm.MediaDescription;
                        if (StringFunctions.Left(pm.MediaLink, 7) == "http://")
                        {
                            hl.NavigateUrl = pm.MediaLink;
                            hl.Target = "_blank";
                        }
                        else
                        {
                            hl.NavigateUrl = "javascript:void();";
                            hl.Attributes.Add("onClick", "popPartImage('" + _bp.ProductsPath + pm.MediaLink + "');return false;");
                        }
                        HtmlTableRow htr = new HtmlTableRow();
                        HtmlTableCell htc = new HtmlTableCell();
                        htc.Controls.Add(hl);
                        htr.Cells.Add(htc);
                        ht.Rows.Add(htr);
                    }
                    AddlMediaPanel.Controls.Add(ht);

                    var table = new DataTable();
                    table.Columns.Add("Color", typeof(String));
                    table.Columns.Add("PartNo", typeof(String));
                    if (_part.InProdLine)
                    {
                        ArrayList parts = NPPart.GetProductLine(_part.PartNo, _bp.AccountID, null, NPConnection.GblConnString, _bp.AppCulture);
                        //foreach (NPPart part in parts)
                        //{
                        //    NPPartUDF partProdLineUDF = new NPPartUDF(part.PartNo, "U_ColorSwatch");
                        //    if (partProdLineUDF.Initialized && partProdLineUDF.Value != null)
                        //    {
                        //        string color = partProdLineUDF.Value;
                        //        table.Rows.Add(new object[] { color, _part.PartNo });
                        //    }
                        //}
                        Repeater_Content.DataSource = parts;
                        Repeater_Content.DataBind();

                    }


                 
                    //System.Web.UI.HtmlControls.HtmlGenericControl createDiv =
                    //new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    //createDiv.ID = _part.PartNo;
                    //createDiv.Attributes["class"] = "myCssClass";
                    //createDiv.Controls.Add(new System.Web.UI.WebControls.Image() { ID = "MainImage", CssClass = "partDetailImages",  Width = 105, Height = 100, ImageUrl = "~/assets/catalog/parts/" + _part.PartNo + "-1.jpg" });
                    //createDiv.Controls.Add(new System.Web.UI.WebControls.Image() { ID = "SideImage1", CssClass = "partDetailImages", Width = 105, Height = 100, ImageUrl = "~/assets/catalog/parts/" + _part.PartNo + "-2.jpg" });
                    //createDiv.Controls.Add(new System.Web.UI.WebControls.Image() { ID = "SideImage2", CssClass = "partDetailImages", Width = 105, Height = 100, ImageUrl = "~/assets/catalog/parts/" + _part.PartNo + "-3.jpg" });
                    //createDiv.Controls.Add(new System.Web.UI.WebControls.Image() { ID = "ModelImage", CssClass = "partDetailImages", Width = 105, Height = 100, ImageUrl = "~/assets/catalog/parts/" + _part.PartNo + "-7.jpg" });
                    //MediaPanel.Controls.Add(createDiv);
                }
            }
            else
            {
                MediaPanel.Visible = false;
            }
        }

        private string GetScript()
        {
            System.Drawing.Image img = null;
            if (File.Exists(Server.MapPath(_bp.ProductsPath + _part.ThumbNail)))
            {
                try
                {
                    img = System.Drawing.Image.FromFile(Server.MapPath(_bp.ProductsPath + _part.ThumbNail));
                }
                catch { //do nothing 
                }
            }
            return "<script language=\"javascript\">\n" +
                "var theWidth = " + (img == null ? "0" : (img.Width + 30).ToString()) + ";\n" +
                "var theHeight = " + (img == null ? "0" : (img.Height + 35).ToString()) + ";\n" +
                "var thePath = '" + _bp.ProductsPath + _part.ThumbNail + "';\n" +
                "function swapPartImage(theSource, imgWidth, imgHeight){\n" +
                "\tdocument.getElementById('" + imgPart.ClientID + "').src = '" + _bp.ProductPreviewImage + "' + theSource;\n" +
                "\tthePath = '" + _bp.ProductsPath + "' + theSource;\n" +
                "\ttheWidth = imgWidth;\n" +
                "\ttheHeight = imgHeight;\n" +
                "}\n" +
                "function popPartImage(thePath){\n" +
                "\tif(theWidth > 0 && theHeight > 0){\n" +
                "\tif(theWidth > 450){\n" +
                "\t\ttheWidth = 450;\n" +
                "\t}\n" +
                "\tif(theHeight > 450){\n" +
                "\t\ttheHeight= 450;\n" +
                "\t}\n" +
                "\t\tvar theWin = window.open(thePath, 'popWin', 'resizable=1,scrollbars=1,toolbar=0,status=0,menubar=0,width=' + theWidth + ',height=' + theHeight);\n" +
                "\t\ttheWin.focus();\n" +
                "\t}\n" +
                "}\n" +
                "</script>\n";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
