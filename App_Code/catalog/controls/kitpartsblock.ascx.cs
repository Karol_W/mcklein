using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;
using netpoint.api.account;
using netpoint.api;
using netpoint.api.utility;
using netpoint.api.data;
using netpoint.common.controls;

namespace netpoint.catalog.controls
{

    public partial class KitPartsBlock : System.Web.UI.UserControl
    {

        private NPPart _parentPart;

        public NPPart ParentPart
        {
            get { return _parentPart; }
            set { _parentPart = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_parentPart != null)
            {
                BindGridView();
                gvKitParts.FindColumn("colPrice").Visible = _parentPart.AggregateType == AggregatePartType.SalesKit &&
                    NPConnection.GetConfigDB("NetPoint", "SalesBOMisAssembly", ((NPBasePage)Page).ConnectionString) == "false";
            }

            NPBasePage _bp = (NPBasePage)Page;
            clpGeneral.ImageUrl = _bp.ResolveImage("icons", "collapse.gif"); //clpGeneral.ImageUrl="~/assets/common/icons/collapse.gif"
            expGeneral.ImageUrl = _bp.ResolveImage("icons", "expand.gif"); //expGeneral.ImageUrl="~/assets/common/icons/expand.gif"
        }

        private void BindGridView()
        {
            NPBasePage bp = (NPBasePage)this.Page;

            gvKitParts.FindColumn("colTax").Visible = bp.TheTheme.ShowItemizedTax;

            if (bp.TheTheme.ShowItemizedTax || bp.TheTheme.AddTaxToPrice)
            {
                gvKitParts.FindColumn("colPrice").HeaderText = bp.TheTheme.AddTaxToPrice ? hdnPriceInclTax.Text : hdnPriceExclTax.Text;
            }

            DataSearchParameters dsp = new DataSearchParameters();
            dsp.Add(new DataSearchParameter("ParentPartNo", DataFilterType.Equal, _parentPart.PartNo));
            ArrayList sourceList = NPPartKit.Search(dsp, "", "ORDER BY ChildPartNo", 0);
            if (sourceList.Count > 0)
            {
                gvKitParts.DataSource = sourceList;
                gvKitParts.DataBind();
            }
            else
            {
                this.Visible = false;
            }
        }

        protected void gvKitParts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            Literal ltl = (Literal)e.Row.FindControl("ltlDescription");
            Literal ltlSeparator = (Literal)e.Row.FindControl("ltlSeparator");
            Literal ltlPartName = (Literal)e.Row.FindControl("ltlPartName");
            HyperLink lnk = (HyperLink)e.Row.FindControl("lnkImage");
            HyperLink lnkPartNo = (HyperLink)e.Row.FindControl("lnkPartNo");
            PartPriceDisplay ppdPrice = (PartPriceDisplay)e.Row.FindControl("ppdPartPrice");
            PriceDisplay prcTax = (PriceDisplay)e.Row.FindControl("prcTax");
            NPPartKit kit = (NPPartKit)e.Row.DataItem;

            if (kit == null) { return; }

            NPPart p = new NPPart(kit.ChildPart.PartNo);

            if (lnk != null)
            {
                lnk.NavigateUrl = "../partdetail.aspx?partno=" + Server.UrlEncode(kit.ChildPart.PartNo);
            }
            if (lnkPartNo != null)
            {
                lnkPartNo.NavigateUrl = "../partdetail.aspx?partno=" + Server.UrlEncode(kit.ChildPart.PartNo);
            }
            if (ltl != null)
            {
                string desc = p.Description;
                if (desc.Length > 95)
                {
                    desc = desc.Remove(95);
                    ltl.Text = desc + "...";
                }
                else
                {
                    ltl.Text = desc;
                    if (desc.Length == 0 && ltlSeparator != null)
                    {
                        ltlSeparator.Visible = false;
                    }
                }
            }

            if (ltlPartName != null)
            {
                ltlPartName.Text = p.PartName;
            }
            if (ppdPrice != null && prcTax != null)
            {
                prcTax.Price = ppdPrice.Tax;
            }
        }

        protected void clpGeneral_Click(object sender, ImageClickEventArgs e)
        {
            gvKitParts.Visible = false;
            clpGeneral.Visible = false;
            expGeneral.Visible = true;
        }

        protected void expGeneral_Click(object sender, ImageClickEventArgs e)
        {
            gvKitParts.Visible = true;
            clpGeneral.Visible = true;
            expGeneral.Visible = false;
        }
    }
}