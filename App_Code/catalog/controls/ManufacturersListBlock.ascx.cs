using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;

namespace netpoint.catalog.controls
{

    /// <summary>
    ///		Summary description for ManufacturersListBlock.
    /// </summary>
    public partial class ManufacturersListBlock : NPBaseControl
    {

        private bool _showCategories = false;

        public Unit BorderWidth
        {
            set { MNFListRepeater.BorderWidth = value; }
        }

        public int RepeatColumns
        {
            set { MNFListRepeater.RepeatColumns = value; }
        }

        public bool ShowCategories
        {
            get { return _showCategories; }
            set { _showCategories = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                MNFListRepeater.DataSource = BasePage.Catalog.Manufacturers;
                MNFListRepeater.DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MNFListRepeater.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.MNFListRepeater_ItemDataBound);

        }
        #endregion

        private void MNFListRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            ManufacturerBlock i = (ManufacturerBlock)e.Item.FindControl("npmanfblock");
            GridView CategoriesList = (GridView)e.Item.FindControl("grid");
            if (i != null)
            {
                string mnfcode = ((NPCatalogManufacturer)e.Item.DataItem).ManufacturerCode;
                int mnfid = ((NPCatalogManufacturer)e.Item.DataItem).ManufacturerID;
                i.MNFID = mnfid;
                i.Visible = true;
                if (_showCategories)
                {
                    CategoriesList.RowDataBound += new GridViewRowEventHandler(this.category_RowDataBound);
                    CategoriesList.DataSource = NPManufacturer.GetCategoriesPerManufacturer(mnfcode, BasePage.Catalog.CatalogID);
                    CategoriesList.DataBind();
                    CategoriesList.Visible = true;
                }
            }
        }

        protected void category_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink lnk = (HyperLink)e.Row.FindControl("CategoryLink");
            if (lnk != null)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                lnk.Text = drv["CategoryName"].ToString();
                lnk.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&MNFCode=" + drv["MNFCode"].ToString() + "&CategoryCode=" + drv["CategoryCode"].ToString();
            }
        }
    }
}
