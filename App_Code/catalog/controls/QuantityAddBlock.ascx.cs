namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using netpoint.api.catalog;
    using netpoint.api.commerce;
    using netpoint.api.common;
    using netpoint.api;
    using netpoint.api.data;
    using System.Web.UI;
    using System.Collections.Generic;

    /// <summary>
    ///		Summary description for QuantityAddBlock.
    /// </summary>
    public partial class QuantityAddBlock : System.Web.UI.UserControl
    {
        private string _partno = "";

        public string PartNo
        {
            set { _partno = value; }
            get { return _partno; }
        }
   


        protected void Page_Load(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            
            btnAddToCart.ImageUrl = bp.ResolveImage("icons", "addtocart.gif");
            if (_partno == "")
            {
                if (Request["PartNo"] != null)
                {
                    _partno = Request["PartNo"].ToString();
                    NPPart p = new NPPart(_partno);
                

                    //NPPartUDF partUDF = new NPPartUDF(_partno, "U_ColorSwatch");
                    //if (partUDF.Initialized && partUDF.Value != null)
                    //{
                    //    string[] colors = partUDF.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    //    var table = new DataTable();
                    //    table.Columns.Add("Color", typeof(String));
                    //    for (int i = 0; i < colors.Length; i++)
                    //    {
                    //        table.Rows.Add(new object[] { colors[i] });
                    //    }

                    //    Repeater_Content.DataSource = table;
                    //    Repeater_Content.DataBind();


                    //}

                    string _backOrderAction = p.BackorderAction;
                    if (p.InheritInventorySettings)
                    {
                        _backOrderAction = NPConnection.GetConfigDB("Inventory", "BackorderAction");
                    }
                    //check backorder option to see if add button should be displayed
                    if (_backOrderAction == "N" && !p.InStock)
                    {
                        btnAddToCart.Visible = false;
                    }

                    ddlQty.Visible = p.DisplayOption.QuantityDDL;
                    nbQuantity.Visible = p.DisplayOption.QuantityTB;
                    pnlOrderLineNotes.Visible = p.DisplayOption.OrderNotes;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddToCart.Click += new System.Web.UI.ImageClickEventHandler(this.btnAddToCart_Click);

        }
        #endregion

        private void btnAddToCart_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            NPOrder o = new NPOrder(bp.UserID, bp.SessionID);

            if (!o.Initialized)
            {
                o.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                if (!o.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID))
                {
                    throw new NPError("Failed to create default order for account [" + bp.AccountID + "], user [" + bp.UserID + "], Session [" + bp.SessionID + "]");
                }
            }

            if (ddlQty.Visible)
            {
                o.AddPart(_partno, Convert.ToInt32(ddlQty.SelectedValue), bp.Catalog.CatalogCode, "", bp.PriceList, tbNote.Text, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
            }
            else
            {
                o.AddPart(_partno, Convert.ToInt32(nbQuantity.Text), bp.Catalog.CatalogCode, "", bp.PriceList, tbNote.Text, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
            }
            Response.Redirect("~/commerce/cart.aspx");
        }
    }
}
