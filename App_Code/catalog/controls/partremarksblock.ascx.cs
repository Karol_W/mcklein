﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;

namespace netpoint.catalog.controls
{
    public partial class partremarksblock : System.Web.UI.UserControl
    {
        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        public Unit BorderWidth
        {
            set { RemarksPanel.BorderWidth = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && _part != null)
            {
                lblRemarks.Text = _part.Remarks.Replace(System.Environment.NewLine, System.Environment.NewLine + "<br>");                
            }         
        }
    }
}