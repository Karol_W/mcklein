namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using netpoint.api;
    using netpoint.api.catalog;
    using System.Collections;
    using netpoint.api.commerce;
    using netpoint.api.data;
    using netpoint.common.controls;

    /// <summary>
    ///		Summary description for ProdLineBlock.
    /// </summary>
    public partial class ProdLineBlock : System.Web.UI.UserControl
    {
        private string _partNo;
        private NPPart _part;
        private ArrayList _datasource;
        private NPBasePage _bp;

        public string PartNo
        {
            get { return _partNo; }
            set { _partNo = value; }
        }

        public ArrayList DataSource
        {
            get { return _datasource; }
            set
            {
                _datasource = value;
                _bp = (NPBasePage)this.Page;
                if (_datasource.Count > 0)
                {
                    gridProdLine.DataSource = _datasource;
                    gridProdLine.DataBind();
                }
            }
        }

        public bool ShowThumbnail
        {
            get { return gridProdLine.FindColumn("colImage").Visible; }
            set { gridProdLine.FindColumn("colImage").Visible = value; }
        }

        public bool ShowPartNo
        {
            get { return gridProdLine.FindColumn("ItemNo").Visible; }
            set { gridProdLine.FindColumn("ItemNo").Visible = value; }
        }

        public bool ShowPartType
        {
            get { return gridProdLine.FindColumn("ItemType").Visible; }
            set { gridProdLine.FindColumn("ItemType").Visible = value; }
        }

        public bool ShowPrice
        {
            get { return gridProdLine.FindColumn("colPrice").Visible; }
            set { gridProdLine.FindColumn("colPrice").Visible = value; }
        }

        public bool ShowInventory
        {
            get { return gridProdLine.FindColumn("colInventory").Visible; }
            set { gridProdLine.FindColumn("colInventory").Visible = value; }
        }

        public bool ShowAddToCart
        {
            get { return gridProdLine.FindColumn("colAdd").Visible; }
            set { gridProdLine.FindColumn("colAdd").Visible = value; }
        }

        /// <summary>
        /// Indicates whether quantity column is displayed
        /// </summary>
        public bool ShowQuantity
        {
            get { return gridProdLine.FindColumn("colQty").Visible; }
            set
            {
                gridProdLine.FindColumn("colQty").Visible = value;
                ShowAddToCart = !value;
                btnSubmit.Visible = value;
            }
        }

        public bool ShowTax
        {
            get { return gridProdLine.FindColumn("colTax").Visible; }
            set { gridProdLine.FindColumn("colTax").Visible = value; }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            if (_partNo != "")
            {
                _part = new NPPart(_partNo);
                gridProdLine.FindColumn("colNotes").Visible = _part.DisplayOption.OrderNotes;
            }
            if (!this.IsPostBack)
            {
                btnSubmit.ImageUrl = _bp.ResolveImage("icons", "addtocart.gif");
                ShowTax = _bp.TheTheme.ShowItemizedTax;

                if (_bp.TheTheme.ShowItemizedTax || _bp.TheTheme.AddTaxToPrice)
                {
                    gridProdLine.FindColumn("colPrice").HeaderText = _bp.TheTheme.AddTaxToPrice ? hdnPriceInclTax.Text : hdnPriceExclTax.Text;
                }

                BindGrid();
            }
            if (!_bp.PermissionController.IsSuperUser)
            {
                gridProdLine.FindColumn("colEdit").Visible = false;
            }
        }

        private void BindGrid()
        {
            if (_partNo != null && _partNo.Length > 0 && _datasource == null)
            {
                DataSource = NPPart.GetProductLine(_partNo, _bp.AccountID, null, NPConnection.GblConnString, _bp.AppCulture);
            }
        }

        protected void gridProdLine_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            NPBasePage bp = (NPBasePage)this.Page;
            NPPart part = (NPPart)e.Row.DataItem;
            HyperLink link = (HyperLink)e.Row.FindControl("lnkThumbnail");
            HyperLink lnk = (HyperLink)e.Row.FindControl("lnkAddToCart");
            ImageButton btn = (ImageButton)e.Row.FindControl("btnAddToCart");
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            PartPriceDisplay ppdPrice = (PartPriceDisplay)e.Row.FindControl("ppdPrice");
            PriceDisplay prcTax = (PriceDisplay)e.Row.FindControl("prcTax");

            if (part != null)
            {
                part.Fetch();
            }

            if (link != null)
            {
                System.Drawing.Image img = null;
                if (File.Exists(Server.MapPath(_bp.ProductsPath + part.ThumbNail)))
                {
                    img = System.Drawing.Image.FromFile(Server.MapPath(_bp.ProductsPath + part.ThumbNail));
                }
                link.Attributes.Add("onClick", "swapPartImage('" + part.ThumbNail + "', " + (img == null ? "0" : (img.Width + 30).ToString()) + "," + (img == null ? "0" : (img.Height + 35).ToString()) + ");");
                if (part.StaticThumbNail.Length > 0)
                {
                    link.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
                }
                else
                {
                    link.ImageUrl = _bp.ProductThumbImage + part.ThumbNail;
                }
                if (_bp.PermissionController.IsSuperUser)
                {
                    lnkEdit.Visible = true;
                    lnkEdit.NavigateUrl = "~/admin/catalog/Part.aspx?partno=" + Server.UrlEncode(part.PartNo) + "&backpage=~/catalog/partdetail.aspx?partno=" + Server.UrlEncode(part.PartNo);
                    lnkEdit.ImageUrl = _bp.ResolveImage("icons", "edit.gif");
                }
                else
                {
                    lnkEdit.Visible = false;
                }
            }

            //	inventory=============
            Literal inStock = (Literal)e.Row.FindControl("ltlInStock");
            Literal ltlNotInStock = (Literal)e.Row.FindControl("ltlNotInStock");

            if (inStock != null && ltlNotInStock != null)
            {
                part.GetInventoryCount(bp.AccountID);
                inStock.Text = hdnInStock.Text;
                ltlNotInStock.Text = hdnNotInStock.Text;
                inStock.Visible = part.Inventory > 0;
                ltlNotInStock.Visible = part.Inventory <= 0;
            }

            if (btn != null)
            {
                if (this._datasource != null)
                {
                    btn.Visible = false;
                }
                else
                {
                    btn.Visible = true;
                    btn.ImageUrl = _bp.ResolveImage("icons", "addtocart.gif");
                    btn.ToolTip = hdnAddToCart.Text;
                }

                if ((!part.InStock && part.InheritInventorySettings
                        && NPConnection.GetConfigDB("Inventory", "BackorderAction").ToUpper().Equals("N"))
                    || (!part.InStock && !part.InheritInventorySettings && part.BackorderAction.Equals("N")))
                {
                    btn.Visible = false;
                }
            }
            if (lnk != null)
            {
                if (this._datasource != null)
                {
                    //	OK--this is cheesy
                    //	this check is because attributepicker needs a hyperlink so it's bubble event doesn't keep
                    //	reloading this control and preventing the redirect to cart
                    lnk.Visible = true;
                    lnk.ImageUrl = _bp.ResolveImage("icons", "addtocart.gif");
                    lnk.NavigateUrl = "~/commerce/cart.aspx?AddPartNo=" + part.PartNo.ToString();
                    lnk.ToolTip = hdnAddToCart.Text;
                }
                else
                {
                    lnk.Visible = false;
                }
                // if part's not in stock, and using global settings, and global backorders aren't allowed
                // OR part's not in stock, and using part-specific settings, and part backorders aren't allowed
                if ((!part.InStock && part.InheritInventorySettings
                        && NPConnection.GetConfigDB("Inventory", "BackorderAction").ToUpper().Equals("N"))
                    || (!part.InStock && !part.InheritInventorySettings && part.BackorderAction.Equals("N")))
                {
                    lnk.Visible = false;
                }
            }
            if (ppdPrice != null && prcTax != null)
            {
                prcTax.Price = ppdPrice.Tax;
            }
        }

        protected void gridProdLine_RowCommand(object source, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            if (e.CommandName == "addtocart")
            {
                GridViewRow row = (GridViewRow)((System.Web.UI.Control)e.CommandSource).NamingContainer;
                TextBox tbNote = (TextBox)row.FindControl("tbNote");
                if (tbNote != null && tbNote.Text != "")
                {
                    Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + e.CommandArgument.ToString() + "&notes=" + Server.UrlEncode(tbNote.Text));
                }
                else
                {
                    Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + e.CommandArgument.ToString());
                }
            }
        }

        protected void gridProdLine_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Add code to add the quantity parts to the order if paging.
            AddPartsToOrder();
            gridProdLine.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void gridProdLine_Sorting(object sender, GridViewSortEventArgs e)
        {
            RaiseBubbleEvent(this, e);
        }

        protected void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (AddPartsToOrder())
            {
                Response.Redirect("~/commerce/cart.aspx", true);
            }
            else
            {
                BindGrid();
            }
        }

        private bool AddPartsToOrder()
        {
            int qty = 0;
            TextBox txt = null;
            string partNo = "";
            NPOrder order = new NPOrder(_bp.UserID, _bp.SessionID);
            if (!order.Initialized)
            {
                order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                order.CreateDefaultOrder(_bp.UserID, _bp.AccountID, _bp.SessionID);
            }
            bool success = false;
            foreach (GridViewRow gvr in gridProdLine.Rows)
            {
                txt = (TextBox)gvr.FindControl("txtQuantity");
                partNo = gridProdLine.FindCell(gvr, "ItemNo").Text;
                if (txt != null)
                {
                    qty = 0;
                    try
                    {
                        qty = Convert.ToInt32(txt.Text);
                    }
                    catch { }
                    if (qty > 0)
                    {
                        order.AddPart(partNo, qty, _bp.Catalog.CatalogCode, "", _bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                        success = true;
                    }
                }
            }

            return success;
        }
    }
}

