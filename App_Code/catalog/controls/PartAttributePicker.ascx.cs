namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Text.RegularExpressions;
    using netpoint.api.catalog;
    using System.Collections;
    using netpoint.classes;

    /// <summary>
    ///		Sets a collection of dropdowns with available attributes for a specified product line or category
    /// </summary>
    public partial class PartAttributePicker : System.Web.UI.UserControl
    {
        #region "Properties and Members"
        private NPPart _part;
        private netpoint.api.catalog.NPCatalogCategory _category;

        /// <summary>
        /// Productline whose attributes will be displayed
        /// </summary>
        public NPPart Part { get { return _part; } set { _part = value; } }

        /// <summary>Category whose attributes will be displayed</summary>
        public NPCatalogCategory Category { get { return _category; } set { _category = value; } }

        /// <summary>Array of Parts whose attributes match the currently selected attributes</summary>
        public ArrayList Parts
        {
            get
            {
                int i;
                NPPart p;
                ArrayList theList = new ArrayList();
                string[] theParts = filter().Split(',');

                for (i = 0; i < theParts.Length; i++)
                {
                    if (theParts[i].Trim() != "")
                    {
                        p = new NPPart(theParts[i].Trim('\''));
                        if (p.PartNo != null && p.PartNo != "")
                        {
                            theList.Add(p);
                        }
                    }
                }

                return theList;
            }
        }
        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (_part == null)
            {
                pnlMain.Visible = false;
            }
            else
            {
                NPBasePage bp = (NPBasePage)this.Page;
                DropDownList ddl;
                NPPartAttribute pa;
                ArrayList theList = new ArrayList();
                string[] s = GetDDLText();

                if (bp.TheTheme.V7Compatible)
                {
                    btnReset.ImageUrl = bp.ResolveImage("buttons", "reset.gif");
                }
                else
                {
                    btnReset.ImageUrl = bp.ResolveImage("buttons", "reset-btn-small.png");
                }

                if (this._category != null && this._category.CategoryID != 0)
                {
                    //theList = NPPartAttribute.AttributeSearch(
                }
                else if (_part != null && _part.PartNo != null)
                {
                    theList = NPPartAttribute.AttributeSearch(_part.PartNo, bp.Catalog.CatalogID, bp.ConnectionString);
                }
                else
                {
                    sysError.Text = hdnNoParts.Text;
                    return;
                }

                for (int i = 0; i < 10; i++)
                {
                    if (i < theList.Count)
                    {
                        pa = (NPPartAttribute)theList[i];
                        ddl = (DropDownList)FindControl("ddl" + i.ToString());
                        ((Label)this.FindControl("sysDim" + i.ToString())).Visible = true;
                        ddl.Visible = true;

                        ((Label)this.FindControl("sysDim" + i.ToString())).Text = pa.AttributeName;
                        NPPartAttribute.BindAttributes(ddl, _part.PartNo, pa.AttributeCode, bp.Catalog.CatalogID, filter(), bp.ConnectionString);

                        // since an attribute can be listed twice for the same part
                        // we need to make sure we are showing the right one 
                        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(s[i]));

                        //	search returned only invisible attributes
                        if (ddl.Items.Count == 1)
                        {
                            ddl.Visible = false;
                            ((Label)FindControl("sysDim" + i.ToString())).Visible = false;
                        }
                        NPEventArgs ea = new NPEventArgs("Page_Load", _part);
                        this.RaiseBubbleEvent(this, ea);
                    }
                    else
                    {
                        ((Label)this.FindControl("sysDim" + i.ToString())).Visible = false;
                        ((DropDownList)this.FindControl("ddl" + i.ToString())).Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// returns the union of the part nos of all dropdowns
        /// </summary>
        /// <returns></returns>
        private string filter()
        {
            int i;
            int ii;
            int iii;
            DropDownList ddl;
            string theParts = "";
            string[] part;
            string[] partCompare = null;
            string comma = "";

            //	exclude all parts that are not in all dropdowns
            for (i = 0; i < 10; i++)
            {
                ddl = (DropDownList)this.FindControl("ddl" + i.ToString());
                if (ddl != null &&
                    ddl.SelectedValue != "")
                {
                    part = theParts.Split(',');
                    partCompare = ddl.SelectedValue.Split(',');

                    //	reset parts and keep only those in both sets
                    theParts = "";
                    comma = "";

                    for (ii = 0; ii <= part.GetUpperBound(0); ii++)
                    {
                        for (iii = 0; iii <= partCompare.GetUpperBound(0); iii++)
                        {
                            if (part[ii] == partCompare[iii])
                            {
                                theParts += comma + "'" + part[ii].Trim('\'') + "'";
                                comma = ",";
                            }
                        }
                    }
                }
            }

            if (theParts == "" && partCompare != null)
            {
                //  if there is only one selected value, there was nothing to filter
                //  use the values from the first dropdown
                //  The last value is the placeholder value--dump it
                for (i = 0; i < partCompare.GetUpperBound(0); i++)
                {
                    theParts += comma + partCompare[i];
                    comma = ",";
                }
            }

            return theParts;
        }

        /// <summary>
        /// Gets the current text of the ddl's for setting selected index
        /// </summary>
        /// <returns></returns>
        private string[] GetDDLText()
        {
            string[] s = new string[10] { "", "", "", "", "", "", "", "", "", "" };
            DropDownList ddl;

            for (int i = 0; i < 10; i++)
            {
                ddl = (DropDownList)this.FindControl("ddl" + i.ToString());
                if (ddl.SelectedIndex != -1)
                {
                    s[i] = ddl.SelectedItem.Text;
                }
            }

            return s;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReset.Click += new System.Web.UI.ImageClickEventHandler(this.btnReset_Click);

        }
        #endregion

        private void btnReset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("~/catalog/partdetail.aspx?PartNo=" + _part.PartNo);
        }

    }
}
