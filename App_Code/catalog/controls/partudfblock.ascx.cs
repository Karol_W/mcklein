﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;
using netpoint.api.catalog; 

namespace netpoint.catalog.controls
{
    public partial class partudfblock : System.Web.UI.UserControl
    {
        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            Dictionary<string, NPPartUDF> fields = _part.UserFields;
            List<NPPartUDF> fieldList = new List<NPPartUDF>();
            NPPartUDF udf;
            //Select desired UDFs (in order)
            if(fields.TryGetValue("U_material", out udf))
            {
                fieldList.Add(udf);
            }
            if(fields.TryGetValue("U_p_size_in_inches", out udf))
            {
                fieldList.Add(udf);
            }
            if(fields.TryGetValue("U_product_wgt", out udf))
            {
                fieldList.Add(udf);
            }
            if(fields.TryGetValue("U_size_no_frame", out udf))
            {
                fieldList.Add(udf);
            }
            if(fields.TryGetValue("U_wheelsize_wgt", out udf))
            {
                fieldList.Add(udf);
            }
            
            if(fields.TryGetValue("U_laptop_com_size", out udf))
            {
                fieldList.Add(udf);
            }

            dlUDFRepeater.DataSource = fieldList;
            dlUDFRepeater.DataBind();
        }

        private void dlUDFRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            NPBasePage bp = (NPBasePage)this.Page;
            NPPartUDF part = (NPPartUDF)e.Item.DataItem;

            
            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblValue = (Label)e.Item.FindControl("lblValue");


            if (lblName != null)
            {
                lblName.Text = string.Format("<h3>{0}</h3>", part.Description.ToUpper());
            }

            if (lblValue != null)
            {
                lblValue.Text = part.Value;
                string[] lbs = { "U_product_wgt", "U_wheelsize_wgt" };
                if (Array.Exists(lbs, o => o == part.Id))
                {
                    lblValue.Text += " (lbs)";
                }
            }
            if (part.Value.Length == 0)
            {
                lblName.Visible = false;
                lblValue.Visible = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dlUDFRepeater.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlUDFRepeater_ItemDataBound);

        }
        #endregion
    }
}