using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using netpoint.classes;
using netpoint.api;
using netpoint.api.catalog;
using netpoint.api.commerce;
using System.ComponentModel;

namespace netpoint.catalog.controls
{
    public enum DescriptionVisibility
    {
        Automatic,
        Always,
        Never
    }

    public partial class PartPriceDisplay : NPBaseControl
    {
        decimal? _price, _tax;
        string _partNo;
        bool? _showTax, _addTax;
        int? _showPriceDescription;
        NPPart _part;
        bool _showVolDiscounts = false;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            _partNo = ViewState["part"] as string;
            _showTax = ViewState["showTax"] as bool?;
            _addTax = ViewState["addTax"] as bool?;
            _showPriceDescription = ViewState["showPriceDesc"] as int?;
        }

        protected override object SaveViewState()
        {
            if (_partNo != null)
            {
                ViewState["part"] = _partNo;
            }
            if (_showTax != null)
            {
                ViewState["showTax"] = _showTax;
            }
            if (_addTax != null)
            {
                ViewState["addTax"] = _addTax;
            }
            if (_showPriceDescription != null)
            {
                ViewState["showPriceDesc"] = _showPriceDescription;
            }

            return base.SaveViewState();
        }

        public string PartNo
        {
            get { return _partNo; }
            set
            {
                if (_partNo == value)
                    return;

                _partNo = value;
                _part = null;
            }
        }

        public NPPart Part
        {
            get
            {
                if (_part == null)
                {
                    if (_partNo != null)
                        _part = new NPPart(_partNo, BasePage.Connection);
                }

                return _part;
            }
            set
            {
                if (_part == value)
                    return;

                _part = value;
                _partNo = value == null || !value.Initialized ? null : value.PartNo;
                _price = null;
                _tax = null;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true)]
        public Style Style { get { return lblAll.ControlStyle; } }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true)]
        public Style PriceStyle { get { return lblPrice.ControlStyle; } }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true)]
        public Style TaxStyle { get { return lblTax.ControlStyle; } }

        public string PriceDescription { get { return ltlPrice.Text; } set { ltlPrice.Text = value; } }
        public string PriceIncludingTaxDescription { get { return ltlPriceInclTax.Text; } set { ltlPriceInclTax.Text = value; } }
        public string PriceExcludingTaxDescription { get { return ltlPriceExclTax.Text; } set { ltlPriceExclTax.Text = value; } }

        public decimal? Price
        {
            get
            {
                if (_price == null && Part != null)
                {
                    if (_part.AggregateType == AggregatePartType.SalesKit ||
                        _part.AggregateType == AggregatePartType.AssemblyKit)
                    {
                        _price = NPPartKit.KitPrice(Part.PartNo, BasePage.AccountID, BasePage.PriceList, 1, DateTime.Now, Part.AggregateType, BasePage.ConnectionString);
                    }
                    else
                    {
                        _price = NPPartPricingMaster.Price(Part.PartNo, BasePage.AccountID, BasePage.PriceList, 1, DateTime.Now, Part.ConnectionString);
                    }
                }

                return _price;
            }
            set
            {
                _price = value;
                _tax = null;
            }
        }

        public decimal? Tax
        {
            get
            {
                if (_tax == null && Part != null)
                {
                    _tax = NPTax.GetPartTax(Part, Price ?? 0, BasePage.AccountID, BasePage.TaxDestination);
                }

                return _tax;
            }
        }

        public bool ShowVolDiscounts
        {
            get { return _showVolDiscounts; }
            set { _showVolDiscounts = value; }
        }

        public bool ShowItemizedTax
        {
            get { return _showTax ?? BasePage.TheTheme.ShowItemizedTax; }
            set { _showTax = value; }
        }

        public bool AddTaxToPrice
        {
            get { return _addTax ?? BasePage.TheTheme.AddTaxToPrice; }
            set { _addTax = value; }
        }

        public DescriptionVisibility ShowPriceDescription
        {
            get { return (DescriptionVisibility)(_showPriceDescription ?? (int)DescriptionVisibility.Automatic); }
            set { _showPriceDescription = (int)value; }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // clear the IDs of the controls, to avoid rendering the extremely long client ID tags
            lblAll.ID = null;
            lblPrice.ID = null;
            lblTax.ID = null;

            base.Render(writer);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Price == null)
            {
                prcPrice = null;
            }
            else
            {
                decimal price = Price.Value;

                if (AddTaxToPrice)
                {
                    price += Tax ?? 0;
                }
                prcPrice.Price = price;
            }

            bool nowrap = false;

            if (ShowItemizedTax)
            {
                pnlTax.Visible = true;
                prcTax.Price = Tax;
                ltlTax.Text += " ";
                nowrap = true;
            }

            Literal ltlPriceDesc = null;

            if (ShowPriceDescription == DescriptionVisibility.Always || ShowPriceDescription == DescriptionVisibility.Automatic)
            {
                if (AddTaxToPrice)
                {
                    ltlPriceDesc = ltlPriceInclTax;
                }
                else if (ShowItemizedTax)
                {
                    ltlPriceDesc = ltlPriceExclTax;
                }
            }
            if (ltlPriceDesc == null && ShowPriceDescription == DescriptionVisibility.Always)
            {
                ltlPriceDesc = ltlPrice;
            }

            if (ltlPriceDesc != null)
            {
                ltlPriceDesc.Visible = true;
                ltlPriceDesc.Text += " ";
                nowrap = true;
            }

            if (nowrap)
            {
                lblAll.Style[HtmlTextWriterStyle.WhiteSpace] = "nowrap";
            }

            if (ShowVolDiscounts)
            {
                qtyPriceDisplay.Visible = true;
                qtyPriceDisplay.Part = Part;
            }
            else
            {
                qtyPriceDisplay.Visible = false;
            }
        }
    }
}