//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace netpoint.catalog.controls
{

    public partial class ProdLineMatrix
    {
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnXValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnYValue;
        protected System.Web.UI.WebControls.ImageButton clpGeneral;
        protected System.Web.UI.WebControls.ImageButton expGeneral;
        protected System.Web.UI.WebControls.Label lblDetailed;
        protected System.Web.UI.WebControls.Panel tblMatrix;
        protected System.Web.UI.WebControls.DropDownList ddlXAttribute;
        protected System.Web.UI.WebControls.DropDownList ddlYAttribute;
        protected System.Web.UI.HtmlControls.HtmlTable theMatrix;
        protected System.Web.UI.WebControls.ImageButton btnAdd;
        protected System.Web.UI.WebControls.Panel AttributesPanel;
        protected System.Web.UI.WebControls.Label sysStartExpanded;
    }
}
