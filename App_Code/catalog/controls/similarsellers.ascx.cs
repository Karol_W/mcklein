using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;

namespace netpoint.catalog.controls
{
    public partial class similarsellers : NPBaseControl
    {
        private NPPart _part;

        public NPPart Part
        {
            get { return _part; }
            set { _part = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            dlSellers.ItemDataBound += OnDataBind;
            dlSellers.DataSource = OrderReports.SimilarSellers(_part.PartNo, 5, BasePage.CatalogCode, Connection);
            dlSellers.DataBind();
            if (dlSellers.Items.Count == 0)
            {
                lblPeopleWhoBought.Visible = false;
            }
        }

        protected void OnDataBind(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = (DataRowView)(e.Item.DataItem);
                string partno = drv.Row["PartNo"].ToString();
                HyperLink part = e.Item.FindControl("lnkSimilar") as HyperLink;
                if (part != null)
                {
                    part.NavigateUrl = string.Format(part.NavigateUrl, Server.UrlEncode(partno));
                    part.Text = partno;
                }
            }
        }
    }
}