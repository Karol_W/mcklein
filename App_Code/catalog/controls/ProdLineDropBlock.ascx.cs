namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using netpoint.api;
    using netpoint.api.catalog;
    using netpoint.api.utility;
    using netpoint.api.common;
    using netpoint.api.data;

    /// <summary>
    ///		Summary description for ProdLineBlock.
    /// </summary>
    public partial class ProdLineDropBlock : System.Web.UI.UserControl
    {
        private string _partNo;
        private NPBasePage _bp;
        private bool _showThumbnail = false;
        private bool _showPartNo = false;
        private bool _showPrice = true;
        private bool _showPartType = true;
        private bool _showInventory = false;
        private bool _showAddToCart = true;
        private bool _showPartName = false;

        public string PartNo
        {
            get { return _partNo; }
            set { _partNo = value; }
        }

        public bool ShowPartName
        {
            get { return _showPartName; }
            set { _showPartName = value; }
        }

        public bool ShowThumbnail
        {
            get { return _showThumbnail; }
            set { _showThumbnail = value; }
        }

        public bool ShowPartNo
        {
            get { return _showPartNo; }
            set { _showPartNo = value; }
        }

        public bool ShowPartType
        {
            get { return _showPartType; }
            set { _showPartNo = value; }
        }

        public bool ShowPrice
        {
            get { return _showPrice; }
            set { _showPrice = value; }
        }

        public bool ShowInventory
        {
            get { return _showInventory; }
            set { _showInventory = value; }
        }

        public bool ShowAddToCart
        {
            get { return _showAddToCart; }
            set { _showAddToCart = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            Page.ClientScript.RegisterClientScriptBlock(Page.ClientScript.GetType(), "dropswap", GetScript());
            if (!this.IsPostBack)
            {
                if (PartNo != null && PartNo != "")
                {
                    string inStock = "";
                    string backOrdered = "";
                    string priceDesc = "";
                    string taxDesc = "";
                    inStock = hdnInStock.Text;
                    backOrdered = hdnNotInStock.Text;
                    taxDesc = hdnTax.Text + " ";

                    if (_bp.TheTheme.AddTaxToPrice)
                    {
                        priceDesc = hdnPriceInclTax.Text + " ";
                    }
                    else if (_bp.TheTheme.ShowItemizedTax)
                    {
                        priceDesc = hdnPriceExclTax.Text + " ";
                    }

                    NPListBinder.PopulateProductLine(
                        this.ddlProdLine
                        , ""
                        , this.PartNo
                        , _bp.AccountID
                        , _showPartNo
                        , _showPartName
                        , _showPartType
                        , _showPrice
                        , _showInventory
                        , _bp.TheTheme.AddTaxToPrice
                        , _bp.TheTheme.ShowItemizedTax
                        , inStock
                        , backOrdered
                        , priceDesc
                        , taxDesc
                        , _bp.PriceList
                        , _bp.SessionCurrency
                        , _bp.TaxDestination);

                    NPPart _part = new NPPart(PartNo);
                    _part.Fetch();
                    pnlOrderLineNotes.Visible = _part.DisplayOption.OrderNotes;
                    string _globalDisplayOutOfStockFlag = "Y";
                    if (!Convert.ToBoolean((NPConnection.GetConfigDB("Inventory", "DisplayOutOfStockFlag"))))
                    {
                        _globalDisplayOutOfStockFlag = "N";
                    }
                }
            }

            if (this._showAddToCart)
            {
                btnAddToCart.ImageUrl = _bp.ResolveImage("icons", "addtocart.gif");
                btnAddToCart.Visible = true;
            }
            else
            {
                btnAddToCart.Visible = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddToCart.Click += new System.Web.UI.ImageClickEventHandler(this.btnAddToCart_Click);

        }
        #endregion


        private void Inv()
        {
            //inventory=============
            /*
            Literal inStock = (Literal)e.Item.FindControl("ltlInStock");
            Literal backOrder = (Literal)e.Item.FindControl("ltlBackOrdered");
            if(inStock != null && backOrder != null){
                inStock.Visible = part.Inventory > 0;
                backOrder.Visible = part.Inventory <= 0;
            }
            */
        }

        private void btnAddToCart_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (tbNote.Text != "")
            {
                Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + ddlProdLine.SelectedValue + "&notes=" + Server.UrlEncode(tbNote.Text));
            }
            else
            {
                Response.Redirect("~/commerce/cart.aspx?AddPartNo=" + ddlProdLine.SelectedValue);
            }
        }

        private string GetScript()
        {
            return "<script language=\"javascript\">\n" +
                "function swapDrop(theVal){\n" +
                "\tvar obj=document.getElementById('" + ddlProdLine.ClientID + "');\n" +
                "\tfor(var i=0;i<obj.options.length;i++){\n" +
                "\t\tif(obj.options[i].value==theVal){\n" +
                "\t\t\tobj.options[i].selected = true;\n" +
                "\t\t}\n" +
                "\t}\n" +
                "}\n" +
                "</script>\n";
        }
    }
}
