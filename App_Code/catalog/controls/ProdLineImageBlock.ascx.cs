namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using netpoint.api;
    using netpoint.api.catalog;
    using netpoint.api.data;

    /// <summary>
    ///		Summary description for ProdLineBlock.
    /// </summary>
    public partial class ProdLineImageBlock : System.Web.UI.UserControl
    {
        private string _partNo;
        private bool _swapimage = false;
        private bool _swapdropdown = false;
        private NPBasePage _bp;

        public string PartNo
        {
            get { return _partNo; }
            set { _partNo = value; }
        }

        public bool SwapImage
        {
            get { return _swapimage; }
            set { _swapimage = value; }
        }

        public bool SwapDropDown
        {
            get { return _swapdropdown; }
            set { _swapdropdown = value; }
        }


        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            if (!this.IsPostBack)
            {
                if (_partNo != null && _partNo != "")
                {
                    rptImages.DataSource = NPPart.GetProductLine(_partNo, _bp.AccountID, _bp.PriceList, NPConnection.GblConnString, _bp.AppCulture);
                    rptImages.DataBind();
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptImages.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.rptImages_ItemDataBound);

        }
        #endregion

        private void rptImages_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            NPPart part = (NPPart)e.Item.DataItem;
            HyperLink link = (HyperLink)e.Item.FindControl("lnkThumbnail");
            if (link != null)
            {
                link.ToolTip = part.PartType;
                System.Drawing.Image img = null;
                if (File.Exists(Server.MapPath(_bp.ProductsPath + part.ThumbNail)))
                {
                    img = System.Drawing.Image.FromFile(Server.MapPath(_bp.ProductsPath + part.ThumbNail));
                }
                string action = "";
                if (_swapdropdown)
                {
                    action += "swapDrop('" + part.PartNo + "');";
                }
                if (_swapimage)
                {
                    action += "swapPartImage('" + part.ThumbNail + "', " + (img == null ? "0" : (img.Width + 30).ToString()) + "," + (img == null ? "0" : (img.Height + 35).ToString()) + ");";
                }
                link.Attributes.Add("onClick", action);
                if (part.StaticThumbNail.Length > 0)
                {
                    link.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
                }
                else
                {
                    link.ImageUrl = _bp.ProductThumbImage + part.ThumbNail;
                }
            }
        }
    }
}
