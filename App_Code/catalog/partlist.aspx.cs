using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.catalog;
using netpoint.api.common;
using netpoint.api.data;
using netpoint.api.utility;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class partlist : netpoint.classes.NPBasePage
    {
        private NPCatalogCategory cat;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            int categoryid = Request.QueryString["CategoryID"] == null ? 0 : Convert.ToInt32(Request.QueryString["CategoryID"]);
            cat = new NPCatalogCategory(categoryid);
            NPTheme _theme = ((netpoint.classes.NPBasePage)Page).TheTheme;
            
            //check to see if this category belongs to the current catalog, if not redirect to correct catalog if possible
            ArrayList themes = GetAssociatedThemes(categoryid, _theme.ServerID);
            //if themes above has 1 record and it matches the current theme don't enter here, stay on current theme
            if (themes.Count > 1 || themes[0].ToString().ToLower() != _theme.ServerID.ToLower())
            {                
                string themeList = "";
                if (ConfigurationManager.AppSettings["ThemeList"] != null)
                {
                    themeList = ConfigurationManager.AppSettings["ThemeList"].ToString().ToLower();

                    foreach (string theme in themes)
                    {
                        if (themeList.Contains(theme.ToLower()))
                        {
                            //found the theme in the supported list so redirect here
                            string url = Page.Request.Url.ToString();
                            if (url.ToLower().Contains("serverid="))
                            {
                                //original url has serverid so replace in existing string
                                string origServerId = Request.QueryString["ServerID"].ToString().ToLower();
                                url = url.ToLower().Replace("serverid=" + origServerId, "ServerID=" + theme);
                                Response.Redirect(url, true);
                            }
                            else
                            {
                                //original url does not have serverid so add to existing string
                                Response.Redirect(url + "&ServerID=" + theme, true);
                            }
                        }
                    }
                }
            }

            if (cat.HasSubCategory)
            {                
                npcblock.ParentID = categoryid;
                npplblock.ShowNoPartsMessage = false;
            }
            else
            {
                npcblock.Enabled = false;
            }

            if (cat.HasParts || !cat.HasSubCategory)
            {                
                npplblock.CategoryID = categoryid;
                npplblock.DisplayType = cat.ListType;
                npplblock.ListType = "C";                
            }
            else
            {
                npplblock.Enabled = false;                
            }

            NPStats.View(UserID, SessionID, "category", categoryid.ToString(), ConnectionString);
            sysCategoryDescription.Text = WebFunctions.ReplaceTags(cat.Description, _theme.HomeUrl, VirtualPath, AssetsPath, Page, UserID, AccountID, ServerID);
        }
        
        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (cat != null && cat.Initialized)
            {
                if (cat.PageTitle != "")
                {
                    Page.Title = cat.PageTitle;
                }
                else
                {
                    Page.Title = cat.CategoryName + (Page.Title != null ? (" | " + Page.Title) : "");
                }                
            }
        }

        private ArrayList GetAssociatedThemes(int _categoryId, string currServerId)
        {
            ArrayList associatedThemes = new ArrayList(25);
            DataParameters dp = new DataParameters(1);
            dp.Add(NPDataType.NPInt, "@categoryId", _categoryId);
            string SQL = "SELECT ServerID FROM CatalogsCategory CC " +                        
                        "INNER JOIN Catalogs C ON CC.CatalogID = C.CatalogID " +
                        "INNER JOIN NPThemes T ON T.CatalogCode = C.CatalogCode " +
                        "WHERE CC.CategoryID = @categoryId " +
                        "ORDER BY ServerID";
            using (IDataReader TheReader = DataFunctions.ExecuteReader(CommandType.Text, SQL, dp.Parameters, ConnectionString))
            {
                while (TheReader.Read())
                {
                    string theme = Convert.ToString(TheReader["ServerID"]);
                    if (theme.ToLower() == currServerId.ToLower())
                    {
                        //just return this theme since it's the one we are on now. No need to loop over rest of them
                        associatedThemes.Clear();
                        associatedThemes.Add(Convert.ToString(TheReader["ServerID"]));
                        break;
                    }
                    associatedThemes.Add(Convert.ToString(TheReader["ServerID"]));
                }
            }
            return associatedThemes;
        }

        public string Keywords
        {
            get
            {
                return cat.MetaKeywords;
            }
        }

        public string Description
        {
            get
            {
                return cat.MetaDescription;
            }
        }

    }
}
