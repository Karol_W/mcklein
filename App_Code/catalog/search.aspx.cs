using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.catalog;
using netpoint.api.common;
using netpoint.classes;
using System.Resources;
using netpoint.common.controls;
using netpoint.catalog.controls;
using netpoint.api.commerce;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class search : netpoint.classes.NPBasePage
    {

        private const string REGEX = @"[\w\-\.]{2,}";
        private bool _bind = true;
        private NPBasePage _bp;
        private string _keywords;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            _bp = (NPBasePage)Page;

            mmy.CatalogID = _bp.Catalog.CatalogID;

            ContentPlaceHolder cph = (ContentPlaceHolder)this.Master.FindControl("mainslot");
            if(cph!=null){
                Label li = (Label)cph.FindControl("totalResults");
                if(li!=null){
                    li.Text="TEST";
                }
            }

            if (!this.IsPostBack)
            {
                _bind = false;
                BindDropDowns();
                BindGrid();


                

                if (Request.QueryString["keywords"] != null)
                {
                    _bind = true;
                    Keywords.Text = Request.QueryString["keywords"].ToString();
                    btnSearch_Click1(this, null);
                }
            }

            //AdvancedPanel.Visible = !SearchType.SelectedValue.Equals("basic");
            ltlSearchIn.Visible = SearchType.SelectedValue.Equals("advanced");
            ddlCategory.Visible = SearchType.SelectedValue.Equals("advanced");
            if(SearchType.SelectedValue.Equals("basic")){
                ddlCategory.SelectedIndex =0;
            }
        }
        private void totalNumDisplay() {
            ContentPlaceHolder cph = (ContentPlaceHolder)this.Master.FindControl("mainslot");
            Label li = (Label)cph.FindControl("totalResults");
            li.Text="hello test";
            Regex exp = new Regex(REGEX, RegexOptions.IgnoreCase);
            MatchCollection mc = exp.Matches(Keywords.Text);
            
        }
        private void BindDropDowns()
        {
            //ddlCategory.DataSource = _bp.Catalog.GetChildCategories(true);
            ddlCategory.DataSource = _bp.Catalog.GetAllNestedChildCategories(true);
            ddlCategory.DataValueField = "CategoryCode";
            ddlCategory.DataTextField = "CategoryName";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("- " + hdnAllCategories.Text + " -", "0"));

            MNFCode.DataSource = _bp.Catalog.Manufacturers;
            MNFCode.DataBind();
            MNFCode.Items.Insert(0, new ListItem("- " + hdnAllManufacturers.Text + " -", ""));
        }

        private void BindGrid()
        {   
            
            if (_bind)
            {

                Regex exp = new Regex(REGEX, RegexOptions.IgnoreCase);
                MatchCollection mc = exp.Matches(Keywords.Text);
                if (mc.Count > 0)
                {   

                    _keywords = "";
                    bool firstToken = true;
                    foreach (Match m in mc)
                    {
                        string lc = m.Value.ToLower();
                        if (_keywords.IndexOf(lc) == -1)
                        {
                            _keywords += (firstToken ? "" : " ") + lc;
                        }
                        firstToken = false;
                    }
                }

                Keywords.Text = _keywords;
                PartsListGrid.DataSource = NPIndexedSearch.PartSearch(
                    _keywords,
                    Catalog.CatalogID,
                    ddlCategory.SelectedValue,
                    MNFCode.SelectedValue,
                    mmy.MachineCode,
                    ConnectionString,
                    AccountID,
                    1,
                    DateTime.Now,
                    null,                   
                    true,
                    null);
                PartsListGrid.DataBind();
            }
        }

        protected void PartsListGrid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            NPPart part = (NPPart)e.Row.DataItem;
            HyperLink link = (HyperLink)e.Row.FindControl("lnkAddPart");

            if (link != null)
            {
                if (part.InProdLine || part.AggregateType == AggregatePartType.Variant)
                {
                    link.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + HttpUtility.UrlEncode(part.PartNo);
                    link.ImageUrl = _bp.ResolveImage("icons", "detail.gif"); // link.ImageUrl="~/assets/common/icons/detail.gif"
                    link.ToolTip = "Product Details";
                }
                else
                {
                    link.NavigateUrl += HttpUtility.UrlEncode(part.PartNo);
                    link.ImageUrl = _bp.ResolveImage("icons", "addtocart.gif");
                }

                if (this.Cache["stringmanager"] != null)
                {
                    link.ToolTip = hdnAddToCart.Text;
                }
            }

            HyperLink thumb = (HyperLink)e.Row.FindControl("lnkThumb");
            if (thumb != null)
            {
                if (part.StaticThumbNail.Length > 0)
                {
                    thumb.ImageUrl = _bp.ProductsPath + part.StaticThumbNail;
                }
                else
                {
                    thumb.ImageUrl = _bp.ProductThumbImage + part.ThumbNail;
                }
                thumb.NavigateUrl = string.Format(thumb.NavigateUrl, HttpUtility.UrlEncode(part.PartNo));
            }

            HyperLink linkDetails = (HyperLink)e.Row.FindControl("lnkPartNo");
            if (linkDetails != null)
            {
                linkDetails.Text = part.PartNo;
                linkDetails.NavigateUrl = string.Format(linkDetails.NavigateUrl, HttpUtility.UrlEncode(part.PartNo));
            }

            PriceDisplay prc = (PriceDisplay)e.Row.FindControl("prcPrice");
            if (prc != null)
            {
                //http://drnick:8282/browse/JECOMM-80 show price with tax in search.
                prc.Price = part.Price(AccountID, PriceList, 1, DateTime.Now);
                if (_bp.TheTheme.AddTaxToPrice)
                {
                    prc.Price += NPTax.GetPartTax(part, prc.Price.Value, _bp.AccountID, _bp.TaxDestination);
                }   
            }

            partquantityprice qtyPriceDisplay = (partquantityprice)e.Row.FindControl("qtyPriceDisplay");
            if (qtyPriceDisplay != null)
            {
                qtyPriceDisplay.Part = part;
            }

            //highlight keywords==========================
            if (_keywords != null)
            {
                Literal ltlPartName = (Literal)e.Row.FindControl("ltlPartNo");
                Literal ltlPartDescription = (Literal)e.Row.FindControl("ltlPartDescription");

                if (ltlPartName != null && ltlPartDescription != null)
                {

                    string partname = part.PartName;
                    string description = part.Description;                    

                    Regex exp = new Regex(REGEX, RegexOptions.IgnoreCase);
                    MatchCollection mc = exp.Matches(_keywords);
                    string matchWordRegEx = "";
                    foreach (Match m in mc)
                    {
                        //build a regular expression search string to replace with highlights
                        //only special character accepted is period so escape it
                        if (matchWordRegEx == "")
                        {
                            matchWordRegEx = "\\b" + m.Value.Replace(".", "\\.");
                        }
                        else
                        {
                            matchWordRegEx += "|\\b" + m.Value.Replace(".", "\\.");
                        }                                               
                    }

                    //replace generated regular expression with highlight span and backreference so case on matched text remains the same
                    description = Regex.Replace(description, "(" + matchWordRegEx + ")", "<span class=nphighlight>$1</span>", RegexOptions.IgnoreCase);
                    partname = Regex.Replace(partname, "(" + matchWordRegEx + ")", "<span class=nphighlight>$1</span>", RegexOptions.IgnoreCase);

                    ltlPartName.Text = partname;
                    ltlPartDescription.Text = description;
                }
            }
        }

        protected void btnSearch_Click1(object sender, EventArgs e)
        {
            PartsListGrid.PageIndex = 0;
            BindGrid();
            if (Keywords.Text.Length == 0)
            {
                sysError.Text = hdnEnterKeyword.Text;
            }
            sysError.Visible = PartsListGrid.Rows.Count == 0;
        }

        protected void Keywords_TextChanged(object sender, System.EventArgs e)
        {
            PartsListGrid.PageIndex = 0;
            BindGrid();
        }

        protected void PartsListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PartsListGrid.PageIndex = e.NewPageIndex;
            BindGrid();
        }
    }
}
