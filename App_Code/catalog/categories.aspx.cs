using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.catalog;
using netpoint.api.utility;
using netpoint.api.common;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class categories : netpoint.classes.NPBasePage
    {
        private NPCatalogCategory cat;
        private NPTheme _theme;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                npcblock.ParentID = Convert.ToInt32(Request.QueryString["ParentID"]);

                _theme = new NPTheme(base.ServerID, Connection);
                cat = new NPCatalogCategory(npcblock.ParentID);
                string catDescription = cat.Description;
                string subCatDescription = WebFunctions.ReplaceTags(catDescription, _theme.HomeUrl, VirtualPath, AssetsPath, Page, UserID, AccountID, ServerID);
                sysCategoryDescription.Text = subCatDescription;
                Page.DataBind();
            }
            catch
            {
                npcblock.ParentID = 0;
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (cat != null && cat.Initialized)
            {
                if (cat.PageTitle != "")
                {
                    Page.Title = cat.PageTitle;
                }
                else
                {
                    Page.Title = cat.CategoryName + (Page.Title != null ? (" | " + Page.Title) : "");
                }
            }
        }

        public string Keywords
        {
            get
            {
                return cat.MetaKeywords;
            }
        }

        public string Description
        {
            get
            {
                return cat.MetaDescription;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
