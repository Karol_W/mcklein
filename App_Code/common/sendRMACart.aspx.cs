﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.common;
using netpoint.api.catalog;
using netpoint.api.utility;
using netpoint.api.support;
using netpoint.classes;
using System.Resources;
using System.Collections.Generic;
using netpoint.api;
using System.IO;
using netpoint.api.data;

using McKlein.ServiceCall;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Mail;
using netpoint.api.commerce;
using netpoint.common.controls;

namespace netpoint.common
{
    public partial class sendRMACart : NPBasePage
    {

        private NPBasePage _bp;
        private NPOrder _order;

        protected global::System.Web.UI.WebControls.Panel contactForm;

        protected global::System.Web.UI.WebControls.TextBox eml_RMANumber;
        protected global::System.Web.UI.WebControls.TextBox customer;
        protected global::System.Web.UI.WebControls.DropDownList discount;
        protected global::System.Web.UI.WebControls.DropDownList shipping;
        protected global::System.Web.UI.WebControls.HyperLink orderLink;
        protected global::System.Web.UI.WebControls.TextBox message;

        protected global::System.Web.UI.WebControls.Label totalPrice;
        protected global::System.Web.UI.WebControls.Panel messageBlock;

        protected global::System.Web.UI.WebControls.Literal sysEmailFrom;
        protected global::System.Web.UI.WebControls.Literal sysEmailAddress;
        protected global::System.Web.UI.WebControls.Literal sysEmailSubject;
        protected global::System.Web.UI.WebControls.Literal sysEmailTemplate;
        protected global::System.Web.UI.WebControls.Literal sysEmailBody;
        protected global::System.Web.UI.WebControls.Literal orderNo;
        protected global::System.Web.UI.WebControls.Label sysSentMessage;
        protected global::System.Web.UI.WebControls.Button btnSendmail;

        protected global::System.Web.UI.WebControls.GridView gridProducts;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            _order = new NPOrder(UserID, SessionID);
            if (!_order.Initialized)
            {
                _order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                if (!_order.CreateDefaultOrder(UserID, AccountID, SessionID))
                {
                    Response.Write("Catastrophic Failure.");
                    Response.End();
                }
            }

            else
            {
                _order.ServerID = ServerID;
                _order.Save(false);
            }

            gridProducts.DataKeyNames = new string[1] { "PartNo" };
            gridProducts.DataSource = new NPCatalogCategory("Repairs", Connection).GetPartDataTable(null);
            gridProducts.DataBind();

            if (!IsPostBack)
            {
                eml_RMANumber.Text = Request.QueryString.Get("rmaNo");
                eml_RMANumber.Enabled = false;
            }

            if (!IsPostBack)
            {
                customer.Text = Request.QueryString.Get("customer");
                customer.Enabled = false;
            }



        }

        protected void ItemNumberValidate(object source, ServerValidateEventArgs args)
        {
            NPPart part = new NPPart(args.Value);

            args.IsValid = part.PartExists();
        }
        protected void btnSendmail_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                NPUser user = ((NPUser)new NPAccount(customer.Text).Users[0]);

                string customerEmail = user.Email;
                string csEmail = sysEmailAddress.Text;
                
                String customerSubject = "McKlein Company Repair Request RMA #" + eml_RMANumber.Text;
                String customerMessage = Uri.UnescapeDataString(this.message.Text);
                String sendMessage = customerMessage;
                if (sendMessage.Length != 0 && customerEmail != "")
                {
                    NPCommunicationLog mail = new NPCommunicationLog();
                    mail.UserID = UserID;
                    mail.AccountID = AccountID;
                    mail.LogType = "email";
                    mail.TimeStamp = DateTime.Now;
                    mail.Incoming = false;
                    mail.TemplateCode = "general";

                    mail.EmailFrom = csEmail;
                    mail.Email = customerEmail;
                    mail.Subject = customerSubject;
                    mail.Message = sendMessage.ToString();

                    mail.Save();
                    sysSentMessage.Visible = true;
                }

            }


        }
        private string ProcessFields()
        {
            string msg = "";

            //get send button id to build other dynamic ids
            string sendId = btnSendmail.UniqueID;
            //strip off last control
            sendId = sendId.Substring(0, sendId.LastIndexOf("$"));
            //now append starting id for our email fields
            sendId = sendId + "$eml_";
            int startLen = sendId.Length;
            string inputName;
            Label inputLabel;
            foreach (string key in _bp.Request.Form.Keys)
            {
                if (key.StartsWith(sendId))
                {
                    inputName = key.Substring(startLen);
                    inputLabel = (Label)FindControl(key.Replace("$eml_", "$lbl_"));
                    if (inputLabel != null)
                    {
                        inputName = inputLabel.Text;
                    }
                    msg = msg + inputName + " " + _bp.Request.Form[key].ToString() + "\r\n\r\n";
                }
            }

            return msg;
        }

        protected void processOrder_Click(object sender, EventArgs e)
        {
            int i;

            DataTable parts = (DataTable)gridProducts.DataSource;
            NPOrder order;
            NPUser user = (NPUser)new NPAccount(customer.Text).Users[0];
            order = new NPOrder();
            order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
            order.CreateDefaultOrder(user.UserID, customer.Text, "tmp_" + customer.Text);
            //order.AccountID = customer.Text;
            //order.CustomerKey = "tmp_" + customer.Text;
            //order.UserID = ((NPUser)new NPAccount(customer.Text).Users[0]).UserID;
            order.CartTypeEnum = CartType.Cart;

            order.Notes = "Service Call Order\r\nRMA No: " + eml_RMANumber.Text;
            order.Save();
            order.RemoveAllDetail();
            
            for (i = 0; i < gridProducts.Rows.Count; i++)
            {
                string partno = (string)parts.Rows[i]["PartNo"];
                string id = gridProducts.Rows[i].FindControl("nbQuantity").UniqueID;
                string qty = _bp.Request.Form[id].ToString();
                ((TextBox)gridProducts.Rows[i].FindControl("nbQuantity")).Text = qty;
                if (int.Parse(qty) > 0)
                {
                    order.AddPart(partno, int.Parse(qty), "001", "Repairs", NPPartPricingMaster.DefaultPricelist(Connection), "I");
                }
            }

            IDictionary<string, string> shippingRates = new Dictionary<string, string>();
            shippingRates["6"] = "4c46NuTV";
            shippingRates["10"] = "6mGtpcqi";
            shippingRates["15"] = "G9gBJuEP";
            shippingRates["0"] = "hZRMmqBb";


            string ship = shippingRates[_bp.Request.Form[shipping.UniqueID].ToString()];
            if (ship != null)
            {
                NPOrderDiscount od = new NPOrderDiscount(ship, ConnectionString);
                NPOrderOrderDiscount ooDiscount = new NPOrderOrderDiscount(order.OrderID, od.DiscountID, Connection);
                
                ooDiscount.Save();
                order.GetPresentedCoupons().Add(ooDiscount);
            }

            IDictionary<string, string> coupons = new Dictionary<string, string>();
            coupons["0"] = null;
            coupons["10"] = "TCTDabLh";
            coupons["20"] = "RKaMA4f5";
            coupons["30"] = "JVcMMz5V";
            coupons["40"] = "Fiq8EdeE";
            coupons["50"] = "mtFPcLTH";
            coupons["60"] = "P8LVhoqh";
            coupons["70"] = "6SmgpQ5f";
            coupons["80"] = "zvMC5Z6u";
            coupons["90"] = "3QJwkGXh";
            coupons["100"] = "DxyqfAjr";

            string coupon = coupons[_bp.Request.Form[discount.UniqueID].ToString()];
            if (coupon != null)
            {
                NPOrderDiscount od = new NPOrderDiscount(coupon, ConnectionString);
                NPOrderOrderDiscount ooDiscount = new NPOrderOrderDiscount(order.OrderID, od.DiscountID, Connection);
                ooDiscount.Save();
                order.GetPresentedCoupons().Add(ooDiscount);
            }
            order.Save();


            orderNo.Text = order.OrderID.ToString();
            totalPrice.Text = (order.SubTotalWithoutLineDiscounts * (1 - decimal.Parse(_bp.Request.Form[discount.UniqueID]) / 100) + decimal.Parse(_bp.Request.Form[shipping.UniqueID].ToString())).ToString("C") + " + taxes";
            orderLink.NavigateUrl = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
(Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + Page.ResolveUrl("~/commerce/repair.aspx?orderId=" + order.OrderID.ToString());
            orderLink.Text = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
(Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + Page.ResolveUrl("~/commerce/repair.aspx?orderId=" + order.OrderID.ToString());
            messageBlock.Visible = true;
            message.Text = Uri.EscapeDataString(String.Format(
                sysEmailBody.Text,
                user.GetFormattedName(NameFormat.FullName),
                eml_RMANumber.Text,
                totalPrice.Text,
                orderLink.NavigateUrl
            ));

        }

    }
}
