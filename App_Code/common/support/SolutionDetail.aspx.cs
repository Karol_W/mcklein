using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.classes;
using netpoint.api.support;

namespace netpoint.common.support
{
    public partial class SolutionDetail : NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["SolutionID"] != null)
            {
                int solutionid = Convert.ToInt32(Request["SolutionID"]);
                NPSupportSolution ss = new NPSupportSolution(solutionid);
                solutionview.NPControlObject = ss;
            }
        }
    }
}
