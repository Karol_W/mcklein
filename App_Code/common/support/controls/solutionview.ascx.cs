using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.support;
using netpoint.classes;

namespace netpoint.common.support.controls
{
    public partial class solutionview : System.Web.UI.UserControl, INPControl
    {
        private NPSupportSolution _solution;

        #region INPControl Members

        public object NPControlObject
        {
            get
            {
                return _solution;
            }
            set
            {
                _solution = (NPSupportSolution)value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_solution != null && _solution.SolutionID != 0)
            {
                sysSolutionName.Text = _solution.SolutionName;
                if (_solution.Symptoms != "")
                {
                    pnlSymptoms.Visible = true;
                    sysSymptoms.Text = _solution.Symptoms;
                }
                if (_solution.Cause != "")
                {
                    pnlCause.Visible = true;
                    sysCause.Text = _solution.Cause;
                }
                if (_solution.MoreInfo != "")
                {
                    pnlMoreInfo.Visible = true;
                    sysMoreInfo.Text = _solution.MoreInfo;
                }
                if (_solution.Resolution != "")
                {
                    pnlResolution.Visible = true;
                    sysResolution.Text = _solution.Resolution;
                }
            }
        }

        public bool Save()
        {
            return false;
        }

    }
}