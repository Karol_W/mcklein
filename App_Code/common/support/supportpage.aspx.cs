using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.support;
using netpoint.classes;
using System.Text;
using netpoint.api.utility;
using System.Resources;
using netpoint.api.common;
using netpoint.api.data;

namespace netpoint.common.support
{
    /// <summary>
    /// Summary description for supportpage.
    /// </summary>
    public partial class supportpage : NPBasePage
    {
        private int _sectionID = 0;
        private int _projectID = 0;
        private string _ticketType = "";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CheckPageCode();
                lblUserID.Text = this.UserID;
                lblAccountID.Text = this.AccountID;
                NPUser npu = new NPUser(this.UserID);
                txtUserEmailAddress.Text = npu.Email;
                if (Request.QueryString["sectionid"] != null && Request.QueryString["sectionid"].Length != 0)
                {
                    _sectionID = int.Parse(Request.QueryString["sectionid"]);
                }
                if (Request.QueryString["projectid"] != null && Request.QueryString["projectid"].Length != 0)
                {
                    _projectID = int.Parse(Request.QueryString["projectid"]);
                }
                if (Request.QueryString["tickettype"] != null && Request.QueryString["tickettype"].Length != 0)
                {
                    _ticketType = Request.QueryString["tickettype"];
                }
                BindForm();
                btnSubmit.ImageUrl = this.ResolveImage("buttons", "submit.gif");
                Image1.ImageUrl = this.ResolveImage("icons", "warning.gif");
                Image2.ImageUrl = this.ResolveImage("icons", "warning.gif");
            }
        }

        private void BindForm()
        {
            ltlPhone.Visible = false;
            ltlEmail.Text = hdnEnterTicket.Text;
            gvSupport.Visible = false;

            if (_sectionID != 0)
            {
                NPSupportSection ss = new NPSupportSection(_sectionID);
                sysSectionID.Text = Convert.ToString(_sectionID);
                sysSectionName.Text = ss.SectionName;
                ddlSectionID.Items.Add(new ListItem(ss.SectionName, Convert.ToString(_sectionID)));
                ddlSectionID.Visible = false;
                //
                this.ViewState["finishURL"] = ss.FinishURL;
                rsp.DataBind(ss.PageCode);
            }
            else
            {
                NPListBinder.PopulateSupportSection(ddlSectionID, this.Encoding, "", false);
            }
            if (_ticketType != "")
            {
                string ticketname = NPCode.getCodeName(_ticketType, "tickettype");
                sysTicketType.Text = _ticketType;
                sysTicketTypeName.Text = ticketname;
                ddlTicketType.Items.Add(new ListItem(ticketname, _ticketType));
                ddlTicketType.Visible = false;
            }
            else
            {
                NPListBinder.PopulateNPCode(ddlTicketType, "tickettype", this.Encoding, "", true);
            }
            if (_projectID != 0)
            {
                NPSupportProject sp = new NPSupportProject(_projectID, ConnectionString);
                sysProjectID.Text = Convert.ToString(_projectID);
                sysProjectName.Text = sp.ProjectName;
                ddlProjectID.Items.Add(new ListItem(sp.ProjectName, Convert.ToString(_projectID)));
                ddlProjectID.Visible = false;
            }
            else
            {
                NPListBinder.PopulateProjects(ddlProjectID, "", true, Connection);
            }
        }


        public string CheckPageCode()
        {
            string PageCode = "";
            try
            {
                PageCode = Request.QueryString["PageCode"];
            }
            catch { }
            return PageCode;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmit_Click);

        }
        #endregion

        private void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

            if (Page.IsValid)
            {
                if (NPValidator.IsEmail(txtUserEmailAddress.Text))
                {
                    NPSupportTicket st = new NPSupportTicket(base.Connection);
                    st.Subject = txtSubject.Text;
                    st.UserEmailAddress = txtUserEmailAddress.Text;
                    st.AccountID = this.AccountID;
                    st.SubmitIPAddress = Request.UserHostAddress;
                    st.SubmitUserID = this.UserID;
                    st.ServerID = this.ServerID;
                    st.PageFrom = Request.RawUrl;
                    st.OriginatorCode = "web";
                    st.ProjectID = int.Parse(ddlProjectID.SelectedValue);
                    if (ddlSectionID.Visible)
                    {
                        if (ddlSectionID.SelectedValue != "")
                        {
                            st.SectionID = int.Parse(ddlSectionID.SelectedValue);
                        }
                    }
                    else
                    {
                        st.SectionID = Convert.ToInt32(Request.QueryString["sectionid"].ToString());
                    }
                    st.TicketType = ddlTicketType.SelectedValue;

                    bool sendConfirmation = Convert.ToBoolean(NPConnection.GetConfigDB("Messaging", "TicketConfirmation") ?? bool.FalseString);
                    NPSupportTicket.Save(st, txtMessage.Text, this.UserID, false, sendConfirmation);

                    NPSupportSection ss = new NPSupportSection(st.SectionID, base.Connection);

                    if (ss.FinishURL != null && ss.FinishURL != "")
                    {
                        Response.Redirect(ss.FinishURL);
                    }
                    else
                    {
                        Response.Redirect("~/default.aspx");
                    }

                }
                else
                {
                    ltlInvalidEmail.Visible = true;
                    imgEmail.Visible = true;
                }
            }
        }
    }
}
