using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.classes;
using netpoint.api.support;
using netpoint.api.common;

namespace netpoint.common.support
{
    public partial class search : NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["keywords"] != null && !IsPostBack)
            {
                txtSearch.Text = Request["keywords"];
                btnSearch_Click(sender, null);
            }
            btnSearch.ImageUrl = ResolveImage("icons", "search.gif");
        }

        protected void gvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvResults.PageIndex = e.NewPageIndex;
            gvResults.DataBind();
        }

        protected void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            gvResults.DataSource = NPIndexedSearch.SolutionSearch(txtSearch.Text, "", 0, true, ConnectionString);
            gvResults.DataBind();
        }
    }
}
