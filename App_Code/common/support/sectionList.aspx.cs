using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.support;
using netpoint.classes;

namespace netpoint.common.support
{
    /// <summary>
    /// Summary description for sectionList.
    /// </summary>
    public partial class sectionList : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.ViewState["id"] = (Request.QueryString["sectionid"] == null || Request.QueryString["sectionid"] == "") ? 0 : int.Parse(Request.QueryString["sectionid"].ToString());
            this.ViewState["type"] = (Request.QueryString["type"] == null || Request.QueryString["type"] == "") ? "SUPPORT" : Request.QueryString["type"].ToString().ToUpper();

            if (!this.IsPostBack)
            {
                BuildPage(int.Parse(this.ViewState["id"].ToString()), this.ViewState["type"].ToString());
            }
        }

        private void BuildPage(int sectionID, string type)
        {

            switch (type)
            {
                case "FAQ":
                    rpt.DataSource = NPSupportSection.FetchSectionList(sectionID, true, false, false, ConnectionString);
                    rpt.DataBind();
                    if (rpt.Items.Count == 0)
                        Response.Redirect("faq.aspx?sectionid=" + sectionID.ToString());
                    break;
                case "KB":
                    rpt.DataSource = NPSupportSection.FetchSectionList(sectionID, false, true, false, ConnectionString);
                    rpt.DataBind();
                    if (rpt.Items.Count == 0)
                        Response.Redirect("kbs.aspx?sectionid=" + sectionID.ToString());
                    break;
                default:
                    //rpt.DataSource = NPSupportSection.FetchSectionList(sectionID, false, base.ConnectionString);
                    rpt.DataBind();
                    break;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpt.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_ItemDataBound);
            this.rpt.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rpt_ItemCommand);

        }
        #endregion

        private void rpt_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            ImageButton btn = (ImageButton)e.Item.FindControl("btnImage");

            if (btn != null)
            {
                if (((NPSupportSection)e.Item.DataItem).ResourcePage.ImageUrl == null || ((NPSupportSection)e.Item.DataItem).ResourcePage.ImageUrl == "")
                {
                    btn.ImageUrl = base.ResolveImage("icons", "detail.gif");
                }
                else
                {
                    btn.ImageUrl = ((NPSupportSection)e.Item.DataItem).ResourcePage.ImageUrl;
                }
            }
        }

        private void rpt_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            NPSupportSection ss = new NPSupportSection(int.Parse(e.CommandArgument.ToString()));

            if (e.CommandName == "go")
            {
                if (!ss.HasChildren)
                {
                    switch (this.ViewState["type"].ToString().ToUpper())
                    {
                        case "FAQ":
                            Response.Redirect("faq.aspx?sectionid=" + ss.SectionID.ToString());
                            break;
                        case "KB":
                            Response.Redirect("kbs.aspx?sectionid=" + ss.SectionID.ToString());
                            break;
                        case "SUPPORT":
                            Response.Redirect("supportpage.aspx?sectionid=" + ss.SectionID.ToString());
                            break;
                        default:
                            Response.Redirect("sectionList.aspx?sectionid=" + e.CommandArgument.ToString());
                            break;
                    }
                }
                else
                {
                    Response.Redirect("sectionList.aspx?sectionid=" + e.CommandArgument.ToString() + "&type=" + this.ViewState["type"].ToString());
                }
            }
        }
    }
}
