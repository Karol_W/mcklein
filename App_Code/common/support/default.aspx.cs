using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.classes;
using netpoint.api.common;
using netpoint.api.support;

namespace netpoint.common.support
{
    public partial class landingpage : NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string solutiontype = "";
            if (Request["type"] != null)
            {
                solutiontype = Request["type"];
                sysCodeType.Text = "[" + NPCode.getCodeName(solutiontype, "solutiontype", Encoding, ConnectionString) + "]";
            }
            gvResults.DataSource = NPSupportSolution.FeaturedArticles(0, Request["type"], true, ConnectionString);
            gvResults.DataBind();

            gvTypes.DataSource = NPCode.getCodeList("solutiontype", Encoding);
            gvTypes.DataBind();

            btnSearch.ImageUrl = ResolveImage("icons", "search.gif");
        }

        protected void gvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvResults.PageIndex = e.NewPageIndex;
            gvResults.DataBind();
        }

        protected void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("search.aspx?keywords=" + Server.UrlEncode(txtSearch.Text));
        }
    }
}
