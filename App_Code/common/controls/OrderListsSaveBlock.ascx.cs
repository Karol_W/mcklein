using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.classes;

namespace netpoint.common.controls
{

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class OrderListsSaveBlock : System.Web.UI.UserControl
    {

        private NPBasePage bp;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)Page;
            NPListBinder.BindSavedOrderLists(ListsDropDown, bp.UserID, "");
            if (ListsDropDown.Items.Count == 0 || (ListsDropDown.Items.Count == 1 && ListsDropDown.SelectedItem.Text == ""))
            {
                divUpdate.Visible = false;
                divNew.Visible = true;
                lnkUpdate.Visible = false;
            }

            SaveToList.ImageUrl = bp.ResolveImage("buttons", "update.gif");
            SaveNewList.ImageUrl = bp.ResolveImage("buttons", "add.gif");
            lnkNew.Text = ltlNewText.Text;
            lnkUpdate.Text = ltlUpdateText.Text;
        }

        public void SaveTo(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            NPOrder o = new NPOrder(bp.UserID, bp.SessionID);
            RemoveDiscounts(o);
            o.ReassignDetail(Convert.ToInt32(ListsDropDown.SelectedValue), bp.PriceList);
            Response.Redirect("~/common/accounts/savedlists.aspx", true);
        }

        public void SaveNew(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            bool err = NewListName.Text.Length == 0 || NewListName.Text.Length > 45;
            imgWarning.Visible = NewListName.Text.Length == 0;
            ltlTooLong.Visible = NewListName.Text.Length > 45;

            if (!err)
            {
                NPOrder o = null;
                if (ListsDropDown.Items.Count == 1 && ListsDropDown.SelectedItem.Text == "")
                {
                    o = new NPOrder(Convert.ToInt32(ListsDropDown.SelectedValue));
                }
                else
                {
                    o = new NPOrder(bp.AccountID, netpoint.api.CartType.Cart);
                    o.CartType = "L";
                }
                o.CartName = NewListName.Text;
                RemoveDiscounts(o);
                Response.Redirect("~/common/accounts/savedlists.aspx", true);
            }
        }

        private void RemoveDiscounts(NPOrder o)
        {
            foreach (NPOrderDetail od in o.OrderDetail)
            {
                if (od.AggregateType == netpoint.api.AggregatePartType.Discount)
                {
                    od.Delete();
                }
            }
            o.Calculate();
            o.Save();
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveToList.Click += new System.Web.UI.ImageClickEventHandler(this.SaveToList_Click);

        }
        #endregion

        private void SaveToList_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

        }

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            divUpdate.Visible = false;
            divNew.Visible = true;
        }

        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            divUpdate.Visible = true;
            divNew.Visible = false;
        }
    }
}
