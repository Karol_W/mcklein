namespace netpoint.common.controls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using System.Collections;
    using netpoint.api.utility;
    using netpoint.classes;
    using netpoint.api;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class UserListBlock : System.Web.UI.UserControl
    {

        public NPAccount Npaccount;
        public string AccountID = "";
        private NPBasePage bp;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)this.Page;
            if (!this.IsPostBack)
            {
                BindData();
                btnNew.ImageUrl = bp.ResolveImage("buttons", "createaccount.gif");
            }
        }

        private void BindData()
        {
            Npaccount = new NPAccount(bp.AccountID);
            ArrayList theUsers = Npaccount.Users;

            if (bp.UserID == Npaccount.MainContactID)
            {
                btnNew.Visible = true;                
            }

            grid.DataSource = theUsers;
            grid.DataBind();
        }

        private void BindDetail(string userID)
        {
            NPUser npu = new NPUser(userID);
            btnSave.CommandArgument = userID;
            if (userID == "")
            {
                txtUserID.Visible = true;
                sysUserID.Visible = false;
            }
            else
            {
                txtUserID.Visible = false;
                sysUserID.Visible = true;
            }
            txtUserID.Text = npu.UserID;
            sysUserID.Text = npu.UserID;
            txtTitle.Text = npu.Title;
            txtFirstName.Text = npu.FirstName;
            txtMiddleName.Text = npu.MiddleName;
            txtLastName.Text = npu.LastName;
            txtSuffix.Text = npu.Suffix;
            txtEmail.Text = npu.Email;
            sysActive.Checked = npu.ActiveFlag;
            sysAdded.Text = npu.AddDate.ToLongDateString();
            sysLastVisit.Text = npu.LastVisitDate.ToLongDateString();
            nbApprovalLimit.Text = npu.ApprovalLimit.ToString("0.00");

            pnlDetail.Visible = true;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }


        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave.Click += new System.Web.UI.ImageClickEventHandler(this.btnSave_Click);
            this.btnNew.Click += new System.Web.UI.ImageClickEventHandler(this.btnNew_Click);

        }
        #endregion

        private void btnNew_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            BindDetail("");
        }

        private void btnSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            sysError.Text = "";
            if (Page.IsValid)
            {                
                NPUser u = new NPUser(btnSave.CommandArgument);
                u.UserID = txtUserID.Text;
                if (btnSave.CommandArgument == "" && u.UserExists())
                {
                    //if command arguement is empty, then it a new user. Check to make sure that userid doesn't already exist
                    sysError.Text = hdnDuplicateUsername.Text;
                    return;  
                }
                u.Email = txtEmail.Text;
                u.ActiveFlag = sysActive.Checked;
                u.Title = txtTitle.Text;
                u.FirstName = txtFirstName.Text;
                u.MiddleName = txtMiddleName.Text;
                u.LastName = txtLastName.Text;
                u.Suffix = txtSuffix.Text;
                if (!u.Initialized)
                {
                    u.SetPasswordRandom();
                }
                u.AccountID = bp.AccountID;
                try
                {
                    u.ApprovalLimit = Convert.ToDecimal(nbApprovalLimit.Text);
                }
                catch
                {
                    sysError.Text = errMustBeNumber.Text;
                    return;
                }
                List<NPError> Errors = new List<NPError>();
                u.Valid(Errors);
                if (Errors.Count == 0)
                {
                    u.Save();
                    BindData();
                    pnlDetail.Visible = false;
                    NPErrorLog.LogError(new Exception("A new user was created [" + u.UserID + "]"), bp.UserID, "", bp.ConnectionString);
                }
                else
                {
                    switch (Errors[0].ErrorType)
                    {
                        case NPErrorType.USER_LoginNameExists:
                        case NPErrorType.USER_AlreadyExists:
                            sysError.Text = hdnDuplicateUsername.Text;                            
                            break;                        
                        case NPErrorType.USER_EmailExists:
                            sysError.Text = hdnDuplicateEmailText.Text; 
                            break;
                        case NPErrorType.USER_LoginNameIsReqired:
                            sysError.Text = hdnUsernameRequired.Text;                            
                            break;
                        default:
                            sysError.Text = Errors[0].Message;
                            break;
                    }
                }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            ImageButton btn0 = (ImageButton)e.Row.FindControl("delete");
            ImageButton btn1 = (ImageButton)e.Row.FindControl("edit");
            ImageButton btn2 = (ImageButton)e.Row.FindControl("save");
            ImageButton btn3 = (ImageButton)e.Row.FindControl("cancel");
            ImageButton btn4 = (ImageButton)e.Row.FindControl("btnEdit");
            NPUser user = (NPUser)e.Row.DataItem;

            if (user != null)
            {
                if (user.ActiveFlag)
                {
                    e.Row.FindControl("imgActiveFlag").Visible = true;
                }
                if (user.UserID == Npaccount.MainContactID)
                {
                    grid.Columns[5].Visible = true;
                }
            }

            if (btn0 != null)
            {
                btn0.ImageUrl = bp.ResolveImage("buttons", "remove.gif");
                btn0.ToolTip = hdnDelete.Text;
                if (bp.UserID != Npaccount.MainContactID) { btn0.Visible = false; }
            }
            if (btn1 != null)
            {
                btn1.ImageUrl = bp.ResolveImage("buttons", "editinfo.gif");
                if (bp.UserID != Npaccount.MainContactID) { btn1.Visible = false; }
            }
            if (btn2 != null)
            {
                btn2.ImageUrl = bp.ResolveImage("buttons", "saveinfo.gif");
                btn2.ToolTip = hdnSave.Text;
                if (bp.UserID != Npaccount.MainContactID) { btn2.Visible = false; }
            }
            if (btn3 != null)
            {
                btn3.ImageUrl = bp.ResolveImage("icons", "cancel.gif");
                if (bp.UserID != Npaccount.MainContactID) { btn3.Visible = false; }
            }
            if (btn4 != null)
            {
                if (bp.UserID != Npaccount.MainContactID) { btn4.Visible = false; }
            }
        }

        protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DataKey data = grid.DataKeys[e.NewEditIndex];
            BindDetail(data["UserID"].ToString());
        }
    }
}
