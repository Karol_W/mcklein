namespace netpoint.common.controls
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api;
    using netpoint.api.common;
    using netpoint.api.account;
    using netpoint.api.utility;
    using netpoint.api.prospecting;
    using netpoint.classes;
    using NetPoint.WebControls;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class UserSubscriptionsBlock : System.Web.UI.UserControl
    {

        private string _userID = "";
        private string _email = "";

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            btnRemove.Attributes.Add("onClick", "return confirm('" + hdnRemoveMessage.Text + "');");
            if (Request["email"] != null)
            {
                _email = Request["email"].ToString();
            }
            if (!Page.IsPostBack)
            {
                BindData();
                btnRemove.ImageUrl = ((NPBasePage)Page).ResolveImage("buttons", "remove.gif");
            }
        }

        private void BindData()
        {
            ArrayList subscriptions = NPContactSubscriber.FetchUserSubscriptions(0, _userID, _email, true, ((NPBasePage)Page).ConnectionString);
            UserSubscriptionsGrid.DataSource = subscriptions;
            UserSubscriptionsGrid.DataBind();
            if (subscriptions.Count == 0)
            {
                btnRemove.Visible = false;
            }
            else
            {
                btnRemove.Visible = true;
            }
                        
        }

        protected void grid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            Literal sysListName = (Literal)e.Row.FindControl("sysListName");

            if (sysListName != null)
            {
                NPContactSubscriber s = (NPContactSubscriber)e.Row.DataItem;
                sysListName.Text = NPContactList.GetListName(s.ListID);
            }
        }

        protected void btnRemove_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            foreach (GridViewRow gvr in UserSubscriptionsGrid.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvr.FindControl("chkSelect");
                if (chkSelect.Checked)
                {
                    int csid = Convert.ToInt32(UserSubscriptionsGrid.DataKeys[gvr.DataItemIndex].Value);
                    NPContactSubscriber s = new NPContactSubscriber(csid);
                    s.ContactSubscribeType = SubscribeType.UnSubscribed;
                    s.SubscribeDate = DateTime.Now;
                    s.SubmitIPAddress = Request.UserHostAddress;
                    s.Save();
                }
            }
            BindData();
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is System.Web.UI.WebControls.CommandEventArgs)
            {
                CommandEventArgs cea = (CommandEventArgs)args;
                if (cea.CommandName.Equals("subscribed"))
                {
                    BindData();
                }
            }
            return base.OnBubbleEvent(source, args);
        }
    }
}
