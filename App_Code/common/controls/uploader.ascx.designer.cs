//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace netpoint.common.controls
{

    public partial class uploader
    {
        protected System.Web.UI.HtmlControls.HtmlInputFile FindFile;
        protected System.Web.UI.WebControls.ListBox FilesListBox;
        protected System.Web.UI.WebControls.Button AddFile;
        protected System.Web.UI.WebControls.Button RemvFile;
        protected System.Web.UI.HtmlControls.HtmlInputSubmit Upload;
        protected System.Web.UI.WebControls.Label MessageLabel;
        protected System.Web.UI.WebControls.TextBox uploadpath;
        protected System.Web.UI.WebControls.Literal errNoFile;
        protected System.Web.UI.WebControls.Literal errSaving;
        protected System.Web.UI.WebControls.Literal hdnFilesUploaded;
    }
}
