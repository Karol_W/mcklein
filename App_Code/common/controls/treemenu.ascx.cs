namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;
    using NetPoint.WebControls;
    using ComponentArt.Web.UI;

    /// <summary>
    ///		Summary description for transmenu.
    /// </summary>
    public partial class treemenu : System.Web.UI.UserControl
    {

        private DataSet _DataSource;
        private string _DataMember = "DataTable";
        private string _DataRelation = "SelfReferencing";
        public string LinkBase = "";

        public DataSet DataSource
        {
            set { _DataSource = value; }
            get { return _DataSource; }
        }
        public string DataMember
        {
            set { _DataMember = value; }
            get { return _DataMember; }
        }
        public string DataRelation
        {
            set { _DataRelation = value; }
            get { return _DataRelation; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
        }

        public override void DataBind()
        {
            base.DataBind();
            foreach (DataRow row in _DataSource.Tables[_DataMember].Rows)
            {
                if (row["ChildColumn"] == null || row["ChildColumn"].ToString() == "" || row["ChildColumn"].ToString() == "0")
                {
                    TreeViewNode topNode = new TreeViewNode();
                    topNode.Text = row["DisplayText"].ToString();
                    topNode.Expanded = true;
                    if (row["DisplayType"].ToString() == "L")
                    {
                        topNode.NavigateUrl = LinkBase + row["DisplayValue"].ToString();
                    }
                    tvList.Nodes.Add(topNode);
                    if (row.GetChildRows(_DataRelation).Length > 0)
                    {
                        BuildChildRow(topNode, row);
                    }
                }
            }
        }

        private void BuildChildRow(TreeViewNode parentNode, DataRow row)
        {
            foreach (DataRow rowChild in row.GetChildRows(_DataRelation))
            {
                TreeViewNode childNode = new TreeViewNode();
                childNode.Text = rowChild["DisplayText"].ToString();
                childNode.Expanded = true;
                if (rowChild["DisplayType"].ToString() == "L")
                {
                    childNode.NavigateUrl = LinkBase + rowChild["DisplayValue"].ToString();
                }
                parentNode.Nodes.Add(childNode);
                if (rowChild.GetChildRows(_DataRelation).Length > 0)
                {
                    BuildChildRow(childNode, rowChild);
                }
            }
        }

    }
}
