namespace netpoint.common.controls
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class OrderPOBlock : System.Web.UI.UserControl
    {
        public string UserID = "";

        protected void Page_Load(object sender, System.EventArgs e)
        {

            NPAccount npaccount = new NPAccount(UserID);
            if (npaccount.Initialized)
            {
                Email.Text = npaccount.Email;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
