namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Collections;
    using System.Text;
    using System.Resources;
    using netpoint.api.account;
    using netpoint.classes;
    using netpoint.providers;

    /// <summary>
    ///		Summary description for login.
    /// </summary>
    public partial class login : System.Web.UI.UserControl
    {

        protected void loginMain_LoggedIn(object sender, EventArgs e)
        {
            // IM 1804554 2008
            // login changed from userid to login name.  This will fail if the upgrade script fails to set loginname to userid
            ((NPBasePage)Page).Login(loginMain.UserName, loginMain.Password, loginMain.RememberMeSet);
        }

        protected void loginMain_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NPBasePage bp = (NPBasePage)Page;
                loginMain.LoginButtonImageUrl = bp.ResolveImage("buttons", "logon.gif");
                ImageButton LoginButton = (ImageButton)loginMain.FindControl("LoginButton");
                if (LoginButton != null)
                {
                    LoginButton.ImageUrl = bp.ResolveImage("buttons", "logon.gif");
                }
                NPMembershipProvider provider = new NPMembershipProvider();
                loginMain.FindControl("lnkForgotPassword").Visible = provider.EnablePasswordRetrieval;
                loginMain.FailureText = hdnLoginFaiureText.Text;
            }
            else
            {
                NPBasePage bp = (NPBasePage)Page;

                NPUser uFailed = new NPUser(((TextBox)loginMain.FindControl("UserName")).Text, new netpoint.api.data.NPConnection(bp.ConnectionString));
                uFailed.Fetch();

                if (uFailed.Initialized
                    && uFailed.PasswordTries >= bp.Config.NPMembershipProvider.MaxInvalidPasswordAttempts
                    && bp.Config.NPMembershipProvider.MaxInvalidPasswordAttempts > 0
                    && bp.Config.NPMembershipProvider.EnableNotifyLockout)
                {
                    loginMain.FailureText = hdnLoginLockoutText.Text;
                }
                else
                {
                    loginMain.FailureText = hdnLoginFaiureText.Text;
                }
            }
        }
    }
}
