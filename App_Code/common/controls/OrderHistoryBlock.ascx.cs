namespace netpoint.common.controls
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.catalog;
    using netpoint.api.commerce;
    using netpoint.api.account;
    using netpoint.classes;
    using System.Resources;
    using netpoint.api;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class OrderHistoryBlock : System.Web.UI.UserControl
    {

        #region "Members and Properties"

        private NPBasePage _bp;
        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        public Unit BorderWidth
        {
            set { OrderHistoryListGrid.BorderWidth = value; }
        }

        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;

            if (!this.IsPostBack)
            {
                btnCancel.ImageUrl = _bp.ResolveImage("icons", "cancel.gif");
                btnOrderFromHistory.ImageUrl = _bp.ResolveImage("buttons", "addtocart.gif");
                btnSubmit.ImageUrl = _bp.ResolveImage("buttons", "submit.gif");
                dtStartDate.SelectedDate = DateTime.Today.AddDays(-30);
                dtEndDate.SelectedDate = DateTime.Today;
                BindGrid();
            }
        }

        private void BindGrid()
        {
            DataSet ds = new DataSet();
            pnlDetail.Visible = false;
            pnlGrid.Visible = true;
            OrderHistoryListGrid.Visible = true;
            ltlNoRecords.Visible = false;
            NPUser u = new NPUser(_userID);
            NPAccount a = new NPAccount(u.AccountID);

            if (_userID != a.MainContactID)
            {
                OrderHistoryListGrid.Columns[9].Visible = false;
            }
            DateTime startDate = new DateTime(dtStartDate.SelectedDate.Year, dtStartDate.SelectedDate.Month, dtStartDate.SelectedDate.Day);
            DateTime endDate = new DateTime(dtEndDate.SelectedDate.Year, dtEndDate.SelectedDate.Month, dtEndDate.SelectedDate.Day, 23, 59, 59);
            ds = OrderReports.GetOrders(_userID, u.AccountID, "O", startDate, endDate, _bp.ConnectionString);
            OrderHistoryListGrid.DataSource = ds;
            OrderHistoryListGrid.DataMember = "DataTable";
            OrderHistoryListGrid.DataBind();

            if (ds.Tables[0].Rows.Count == 0)
            {
                OrderHistoryListGrid.Visible = false;
                ltlNoRecords.Visible = true;
            }
        }

        #region "BindDetail"

        private void BindDetail(int orderid)
        {

            ViewState["activeorder"] = orderid;

            pnlDetail.Visible = true;
            btnOrderFromHistory.Visible = !((NPBasePage)this.Page).Catalog.ViewOnly;
            pnlGrid.Visible = false;

            NPOrder o = new NPOrder(orderid);

            sysDocNum.Text = o.OrderID.ToString();
            switch (o.CartType)
            {
                case "O":
                    sysDocumentType.Text = hdnOrder.Text;
                    break;
                case "I":
                    sysDocumentType.Text = hdnInvoice.Text;
                    break;
                case "Q":
                    sysDocumentType.Text = hdnQuote.Text;
                    break;
            }
            sysPurchaseDate.Text = o.PurchaseDate.ToString("yyyy-MMM-dd");
            if (o.Account.AccountType.Equals("B"))
            {
                if (o.RequestedShipDate != DateTime.MinValue)
                {
                    sysRequestedShipDate.Text = o.RequestedShipDate.ToString("yyyy-MMM-dd");
                }
                else
                {
                    ltlRequestedShipDate.Visible = false;
                    sysRequestedShipDate.Visible = false;
                }
                if (o.PONumber.Length > 0)
                {
                    sysPONum.Text = o.PONumber;
                }
                else
                {
                    ltlPONumber.Visible = false;
                    sysPONum.Visible = false;
                }
            }
            else
            {
                ltlRequestedShipDate.Visible = false;
                sysRequestedShipDate.Visible = false;
                ltlPONumber.Visible = false;
                sysPONum.Visible = false;
            }
            if (o.SynchID.Length > 0 && o.SynchID != "0")
            {
                sysConfirmationNum.Text = o.SynchID;
                sysConfirmationNum.Visible = true;
                ltlConfirmationNum.Visible = true;
            }
            else
            {
                sysConfirmationNum.Visible = false;
                ltlConfirmationNum.Visible = false;
            }

            sysBillTo.Text = o.BillingAddress.Replace("\r\n", "<br>").Replace("\r", "<br>").Replace("\n", "<br>");
            sysShipTo.Text = o.ShippingAddress.Replace("\r\n", "<br>").Replace("\r", "<br>").Replace("\n", "<br>");

            gridDetail.DataSource = o.OrderDetail;
            gridDetail.DataBind();

            sysVolumeTotal.Text = o.VolumeTotal.ToString("0.00");
            sysWeightTotal.Text = o.WeightTotal.ToString("0.00");

            expenses.LoadExpenses(o);

            gridPayments.DataSource = o.Payments;
            gridPayments.DataBind();

            gridShipments.DataSource = o.Shipments;
            gridShipments.DataBind();

        }

        #endregion

        protected void OrderHistoryListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            OrderHistoryListGrid.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void OrderHistoryListGrid_RowCommand(object source, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("detail"))
            {
                BindDetail(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void btnCancel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            pnlDetail.Visible = false;
            pnlGrid.Visible = true;
        }

        protected void btnOrderFromHistory_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            NPOrder.CreateOrderFromHistory(Convert.ToInt32(ViewState["activeorder"]), _bp.UserID, _bp.AccountID, _bp.SessionID, "C", _bp.PriceList);
            Response.Redirect("~/commerce/cart.aspx", true);
        }

        protected void btnFind_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            OrderHistoryListGrid.PageIndex = 0;
            BindGrid(); 
        }


        protected void OrderHistoryListGrid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            Literal ltlShippingAddressName = (Literal)e.Row.FindControl("ltlShippingAddressName");
            Literal ltlDocumentType = (Literal)e.Row.FindControl("ltlDocumentType");
            Literal ltlStatus = (Literal)e.Row.FindControl("ltlShipStatus");

            if (ltlShippingAddressName != null)
            {
                int index = drv["ShippingAddress"].ToString().IndexOf("\n");
                ltlShippingAddressName.Text = (index > -1 ? drv["ShippingAddress"].ToString().Substring(0, index) : drv["ShippingAddress"].ToString());
                //
                switch (drv["CartType"].ToString())
                {
                    case "O":
                        ltlDocumentType.Text = hdnOrder.Text;
                        break;
                    case "I":
                        ltlDocumentType.Text = hdnInvoice.Text;
                        break;
                    case "Q":
                        ltlDocumentType.Text = hdnQuote.Text;
                        break;
                }
            }
            if (ltlStatus != null)
            {
                if (drv["ShippingStatus"] != null)
                {
                    if (drv["ShippingStatus"].ToString() == "Shipped")
                    {
                        ltlStatus.Text = ((Literal)FindControl("hdn" + drv["ShippingStatus"].ToString())).Text + " " + Convert.ToDateTime(drv["ActualShipDate"]).ToString("yyyy-MMM-dd");
                    }
                    else if (
                        drv["ShippingStatus"].ToString() == "Pending" ||
                        drv["ShippingStatus"].ToString() == "Picked" ||
                        drv["ShippingStatus"].ToString() == "Partial")
                    {
                        ltlStatus.Text = ((Literal)FindControl("hdn" + drv["ShippingStatus"].ToString())).Text;
                    }
                    else if (
                        drv["ShippingStatus"].ToString() == "Closed")
                    {
                        ltlStatus.Text = ((Literal)FindControl("hdn" + drv["ShippingStatus"].ToString())).Text;
                    }
                    else if (
                        drv["ShippingStatus"].ToString() == "Cancelled")
                    {
                        ltlStatus.Text = ((Literal)FindControl("hdn" + drv["ShippingStatus"].ToString())).Text;
                    }
                }
            }
        }

        protected void gridDetail_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            NPOrderDetail od = (NPOrderDetail)e.Row.DataItem;
            if (od != null)
            {
                Label lblQuantity = (Label)e.Row.FindControl("lblQuantity");
                Label lblPartNo = (Label)e.Row.FindControl("lblPartNo");
                Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                netpoint.commerce.controls.PartPrice partPrice = (netpoint.commerce.controls.PartPrice)e.Row.FindControl("partPrice");
                HyperLink lnkDownloads = (HyperLink)e.Row.FindControl("lnkDownloads");

                partPrice.LoadPrice(od);
                lblDescription.Text = od.DescriptorPartName;// +"<br>" + od.PartDescription;
                lblPartNo.Text = od.PartNo;
                lblQuantity.Text = od.Quantity.ToString();

                if (od.PartType == "variantmaster")
                {
                    foreach (NPOrderDetail vd in od.VariantDetail)
                    {
                        lblDescription.Text += "<br>&nbsp;&nbsp;&nbsp;&nbsp;" + vd.PartName;
                    }
                }
                if (od.AggregateType == AggregatePartType.AssemblyKit)
                {
                    foreach (NPOrderDetail pd in od.KitDetail)
                    {
                        lblDescription.Text += "<br>&nbsp;&nbsp;&nbsp;&nbsp;" + pd.Quantity.ToString() + "&nbsp;-&nbsp;" + pd.PartName;
                    }
                }

                NPPart p = new NPPart(od.PartNo);
                if (p.Downloads.Count > 0)
                {
                    lnkDownloads.Visible = true;
                    lnkDownloads.NavigateUrl = "~/common/accounts/orderdownloads.aspx?PartNo=" + od.PartNo;
                }
            }
        }

        protected void gridPayments_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            NPOrderPayment op = (NPOrderPayment)e.Row.DataItem;
            if (op != null)
            {
                e.Row.Cells[2].Text = op.ExpirationMonth + "/" + op.ExpirationYear;
            }
        }
    }
}
