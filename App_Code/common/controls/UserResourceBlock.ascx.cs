namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.api.common;
    using System.Resources;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class UserResourceBlock : System.Web.UI.UserControl
    {
        NPAccount npaccount = new NPAccount();
        string _resourceID = "";
        string _userID = "";
        int _urid;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (this.ViewState["urid"] != null)
                    LoadControl();

                SaveButton.ImageUrl = ((NPBasePage)this.Page).ResolveImage("buttons", "update.gif");
            }
        }

        public string ResourceID
        {
            get { return _resourceID; }
            set
            {
                _resourceID = value;
                this.ViewState["resourceid"] = value;
            }
        }

        public string UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                this.ViewState["userid"] = value;
            }
        }

        public int UserResourceID
        {
            get { return _urid; }
            set
            {
                _urid = value;
                this.ViewState["urid"] = value;
            }
        }

        public void LoadControl()
        {
            LoadLabels();
            LoadData();
            DisableLines();
        }

        public void LoadLabels()
        {
            NPResource r = new NPResource(this._resourceID);
            if (r.Initialized)
            {
                sysd1label.Text = r.Dimension1;
                sysd2label.Text = r.Dimension2;
                sysd3label.Text = r.Dimension3;
                sysd4label.Text = r.Dimension4;
                sysd5label.Text = r.Dimension5;
                sysd6label.Text = r.Dimension6;
                sysd7label.Text = r.Dimension7;
                sysd8label.Text = r.Dimension8;
                sysd9label.Text = r.Dimension9;
                syst1label.Text = r.CommentsDimension;
                sysp1label.Text = r.PictureDimension;
                sysc1label.Text = r.CodeDimension1;
                sysu1label.Text = r.URLDimension1;
                ddlc1.DataSource = NPCode.getCodeList(r.CodeType1);
                ddlc1.DataBind();
                sysc2label.Text = r.CodeDimension2;
                ddlc2.DataSource = NPCode.getCodeList(r.CodeType2);
                ddlc2.DataBind();
            }
        }

        public void LoadData()
        {
            NPUserResource ur = new NPUserResource(int.Parse(this.ViewState["urid"].ToString()));

            txtd1.Text = ur.Dimension1;
            txtd2.Text = ur.Dimension2;
            txtd3.Text = ur.Dimension3;
            txtd4.Text = ur.Dimension4;
            txtd5.Text = ur.Dimension5;
            txtd6.Text = ur.Dimension6;
            txtd7.Text = ur.Dimension7;
            txtd8.Text = ur.Dimension8;
            txtd9.Text = ur.Dimension9;
            txtp1.Text = ur.PictureDimension;
            txtt1.Text = ur.TextDimension;
            txtu1.Text = ur.URLDimension;
            syscommunityvisible.Checked = ur.CommunityVisible;
            ddlc1.SelectedIndex = ddlc1.Items.IndexOf(ddlc1.Items.FindByValue(ur.CodeDimension1));
            ddlc2.SelectedIndex = ddlc2.Items.IndexOf(ddlc2.Items.FindByValue(ur.CodeDimension2));
            CurrentResourceID.Text = _resourceID;
        }

        public void DisableLines()
        {
            if (syst1label.Text == "") { syst1label.Visible = false; txtt1.Visible = false; } else { syst1label.Visible = true; txtt1.Visible = true; }
            if (sysu1label.Text == "") { sysu1label.Visible = false; txtu1.Visible = false; } else { sysu1label.Visible = true; txtu1.Visible = true; }
            if (sysp1label.Text == "") { sysp1label.Visible = false; txtp1.Visible = false; } else { sysp1label.Visible = true; txtp1.Visible = true; }
            if (sysc1label.Text == "") { sysc1label.Visible = false; ddlc1.Visible = false; } else { sysc1label.Visible = true; ddlc1.Visible = true; }
            if (sysc2label.Text == "") { sysc2label.Visible = false; ddlc2.Visible = false; } else { sysc2label.Visible = true; ddlc2.Visible = true; }
            if (sysd1label.Text == "") { sysd1label.Visible = false; txtd1.Visible = false; } else { sysd1label.Visible = true; txtd1.Visible = true; }
            if (sysd2label.Text == "") { sysd2label.Visible = false; txtd2.Visible = false; } else { sysd2label.Visible = true; txtd2.Visible = true; }
            if (sysd3label.Text == "") { sysd3label.Visible = false; txtd3.Visible = false; } else { sysd3label.Visible = true; txtd3.Visible = true; }
            if (sysd4label.Text == "") { sysd4label.Visible = false; txtd4.Visible = false; } else { sysd4label.Visible = true; txtd4.Visible = true; }
            if (sysd5label.Text == "") { sysd5label.Visible = false; txtd5.Visible = false; } else { sysd5label.Visible = true; txtd5.Visible = true; }
            if (sysd6label.Text == "") { sysd6label.Visible = false; txtd6.Visible = false; } else { sysd6label.Visible = true; txtd6.Visible = true; }
            if (sysd7label.Text == "") { sysd7label.Visible = false; txtd7.Visible = false; } else { sysd7label.Visible = true; txtd7.Visible = true; }
            if (sysd8label.Text == "") { sysd8label.Visible = false; txtd8.Visible = false; } else { sysd8label.Visible = true; txtd8.Visible = true; }
            if (sysd9label.Text == "") { sysd9label.Visible = false; txtd9.Visible = false; } else { sysd9label.Visible = true; txtd9.Visible = true; }
        }

        public void UpdateData(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //hardcode finish here with object
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveButton.Click += new System.Web.UI.ImageClickEventHandler(this.SaveButton_Click);

        }
        #endregion

        private void SaveButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            NPUserResource ur = new NPUserResource(int.Parse(this.ViewState["urid"].ToString()));
            NPResource npr = new NPResource(this.ViewState["resourceid"].ToString());

            //	check to make sure required fields are populated
            sysError.Text = "";

            if (ur.URID < 1)
            {
                ur.UserID = this.ViewState["userid"].ToString();
                ur.ResourceID = this.ViewState["resourceid"].ToString();
            }
            ur.Dimension1 = txtd1.Text;
            if (ur.Dimension1 == "" && npr.IsRequired[0] == 'Y')
            {
                sysError.Text = npr.Dimension1 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension2 = txtd2.Text;
            if (ur.Dimension2 == "" && npr.IsRequired[1] == 'Y')
            {
                sysError.Text = npr.Dimension2 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension3 = txtd3.Text;
            if (ur.Dimension3 == "" && npr.IsRequired[2] == 'Y')
            {
                sysError.Text = npr.Dimension3 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension4 = txtd4.Text;
            if (ur.Dimension4 == "" && npr.IsRequired[3] == 'Y')
            {
                sysError.Text = npr.Dimension4 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension5 = txtd5.Text;
            if (ur.Dimension5 == "" && npr.IsRequired[4] == 'Y')
            {
                sysError.Text = npr.Dimension5 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension6 = txtd6.Text;
            if (ur.Dimension6 == "" && npr.IsRequired[5] == 'Y')
            {
                sysError.Text = npr.Dimension6 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension7 = txtd7.Text;
            if (ur.Dimension7 == "" && npr.IsRequired[6] == 'Y')
            {
                sysError.Text = npr.Dimension7 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension8 = txtd8.Text;
            if (ur.Dimension8 == "" && npr.IsRequired[7] == 'Y')
            {
                sysError.Text = npr.Dimension8 + " " + hdnIsRequired.Text;
                return;
            }
            ur.Dimension9 = txtd9.Text;
            if (ur.Dimension9 == "" && npr.IsRequired[8] == 'Y')
            {
                sysError.Text = npr.Dimension9 + " " + hdnIsRequired.Text;
                return;
            }
            ur.PictureDimension = txtp1.Text;
            ur.TextDimension = txtt1.Text;
            ur.URLDimension = txtu1.Text;
            ur.CommunityVisible = syscommunityvisible.Checked;
            ur.CodeDimension1 = ddlc1.SelectedValue;
            ur.CodeDimension2 = ddlc2.SelectedValue;

            ur.Save();

            LoadControl();

            CommandEventArgs cea = new CommandEventArgs("John the Blob", "");
            this.RaiseBubbleEvent(this, cea);
        }
    }
}
