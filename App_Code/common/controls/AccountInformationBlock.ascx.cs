namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.api.commerce;
    using netpoint.api.common;
    using netpoint.classes;
    using System.Resources;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class AccountInformationBlock : System.Web.UI.UserControl
    {

        private NPBasePage _bp;
        private NPAccount _account;

        public NPAccount Account
        {
            get { return _account; }
            set { _account = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

            _bp = (NPBasePage)Page;

            if (!Page.IsPostBack)
            {
                AccountName.Text = _account.AccountName;
                Phone1.Text = _account.Phone1;
                Phone2.Text = _account.Phone2;
                PhoneFax.Text = _account.PhoneFax;
                PhoneMobile.Text = _account.PhoneMobile;
                EmailAddress.Text = _account.Email;
                Website.Text = _account.WebSite;
                TaxID.Text = _account.TaxID;
                tbxTaxID.Text = _account.TaxID;
                if (_bp.UserID == _account.MainContactID)
                {
                    //main contact logged in so make tax id editable
                    tbxTaxID.Visible = true;
                    TaxID.Visible = false;
                }
                else
                {
                    tbxTaxID.Visible = false;
                    TaxID.Visible = true;
                }
                PurchaseLimit.Price = _account.PurchaseLimit;
                if (_account.PurchaseLimit <= 0)
                {
                    PurchaseLimit.Visible = false;
                    PurchaseLimitLabel.Visible = false;
                }
                if (_account.TaxExempt)
                {
                    TaxExempt.Visible = true; ;
                }
                NPListBinder.PopulateAddressList(DefaultBillAddressID, _account.DefaultBillAddressID.ToString(), true, _account.AccountID, false);
                NPListBinder.PopulateAddressList(DefaultShipAddressID, _account.DefaultShipAddressID.ToString(), true, _account.AccountID, true);
                //http://drnick:8282/browse/JECOMM-425
                NPListBinder.PopulatePaymentList(DefaultBillOptionID, _account.DefaultBillOptionID.ToString(), true, false, _bp.Encoding, _account.IndustryCode);
                NPListBinder.PopulateRateList(DefaultShipOptionID, "S", _account.DefaultShipOptionID.ToString(), true, false, true, -1, -1, _bp.UserID);

                SaveButton.ImageUrl = _bp.ResolveImage("buttons", "update.gif");
            }
        }

        public void SaveAccount(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SaveMessage.Visible = false;
            _account.AccountName = AccountName.Text;
            _account.Phone1 = Phone1.Text;
            _account.Phone2 = Phone2.Text;
            _account.PhoneFax = PhoneFax.Text;
            _account.PhoneMobile = PhoneMobile.Text;
            _account.Email = EmailAddress.Text;
            _account.WebSite = Website.Text;
            _account.DefaultBillAddressID = Convert.ToInt32(DefaultBillAddressID.SelectedValue);
            _account.DefaultBillOptionID = Convert.ToInt32(DefaultBillOptionID.SelectedValue);
            _account.DefaultShipAddressID = Convert.ToInt32(DefaultShipAddressID.SelectedValue);
            _account.DefaultShipOptionID = Convert.ToInt32(DefaultShipOptionID.SelectedValue);
            if (_bp.UserID == _account.MainContactID)
            {
                _account.TaxID = tbxTaxID.Text;
            }
            _account.Save();
            SaveMessage.Visible = true;
        }
    }
}
