namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.api;
    using netpoint.api.data;
    using netpoint.api.catalog;
    using netpoint.api.framework;
    using netpoint.classes;
    using System.Collections;
    using System.Resources;

    /// <summary>
    ///		Summary description for RegisteredParts.
    /// </summary>
    public partial class RegisteredParts : System.Web.UI.UserControl
    {
        private bool _showConractButton;
        private int _buttonCount = 0;

        public bool ShowContractButton
        {
            get { return this._showConractButton; }
            set { this._showConractButton = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ltlHeader.Text += " " + ((NPBasePage)this.Page).AccountID;
                BindData();
            }
        }

        private void BindData()
        {
            gvParts.Columns[4].Visible = this._showConractButton;
            NPQuery q = new NPQuery(typeof(NPUserAccountPart), ((NPBasePage)Page).Connection);
            q.AddJoin(typeof(NPPart), "PartNo", "PartNo", JoinType.INNER);
            q[0].ColumnNames = new string[] { "UAPID", "SupportContractID", "DatePurchased", "DateRegistered", "UserID"};
            q[0].AddWhere(Comparison.Equal, "AccountID", ((NPBasePage)Page).AccountID);
            q[1].ColumnNames = new string[] { "PartNo", "PartName" };
            gvParts.DataSource = q.ExecuteDataSet();
            gvParts.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void grid_PreRender(object sender, System.EventArgs e)
        {
            //	don't show button column if there are no visible buttons
            if (this._buttonCount == 0)
            {
                gvParts.Columns[4].Visible = false;
            }
        }

        protected void gvParts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            NPUserAccountPart part = new NPUserAccountPart(Convert.ToInt32(e.CommandArgument));

            if (e.CommandName == "contract")
            {
                Response.Redirect("accountsupport.aspx?contractid=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "remove")
            {
                part.Delete();
                BindData();
            }
        }

        protected void gvParts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton img = (ImageButton)e.Row.FindControl("btnSupport");
            ImageButton img2 = (ImageButton)e.Row.FindControl("imgDelete");

            if (img != null)
            {
                img.ImageUrl = ((NPBasePage)Page).ResolveImage("icons", "detail.gif");
                if (Convert.ToInt32(((DataRowView)e.Row.DataItem)["SupportContractID"]) > 0)
                {
                    img.Visible = true;
                    _buttonCount++;
                }
                else
                {
                    img.Visible = false;
                }
            }
            if (img2 != null)
            {
                img2.Attributes.Add("onClick", "return confirm('" + ltlDeleteText.Text + "');");
            }
        }

        protected void gvParts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvParts.PageIndex = e.NewPageIndex;
            BindData();
        }
    }
}
