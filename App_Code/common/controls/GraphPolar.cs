using System;
using System.Web;
using System.Data;
using System.Drawing;
using netpoint.api;
using netpoint.api.reports;
using ChartDirector;
using netpoint.api.common;
using netpoint.api.data;

namespace netpoint.common.controls
{
    /// <summary>
    /// Summary description for GraphPolar.
    /// </summary>
    public class GraphPolar : System.Web.IHttpHandler
    {
        #region IHttpHandler Members

        public void ProcessRequest(System.Web.HttpContext context)
        {
            BuildGraph(context, int.Parse(context.Request.QueryString["graphid"]));
        }

        public bool IsReusable { get { return false; } }

        #endregion

        private void BuildGraph(HttpContext context, int reportGraphID)
        {
            ReportGraph g = new ReportGraph(reportGraphID);
            System.Drawing.Image img;
            int i = 0;
            int ii;

            try
            {
                //	run the query
                DataTable theTable;
                if (g.Query.Query == null || g.Query.Query == "")
                {
                    throw (new NPError("No query has been set for this graph.", NPErrorType.RPG_NoQuery));
                }
                else
                {
                    theTable = DataFunctions.GetDataTable(g.Query.Query, NPConnection.GblConnString);
                }

                double[][] data = new double[theTable.Columns.Count - 1][];
                string[] label = new string[theTable.Rows.Count];
                for (i = 0; i < data.Length; i++)
                {
                    data[i] = new double[theTable.Rows.Count];
                }
                string[] legend = new string[data.Length];

                //	First column is assumed to be labels
                //	each column after is assumed to be data layer
                i = 0;
                foreach (DataRow dr in theTable.Rows)
                {
                    label[i] = dr.ItemArray[0].ToString();

                    for (ii = 0; ii < data.Length; ii++)
                    {
                        data[ii][i] = Convert.ToDouble(dr.ItemArray[ii + 1]);
                    }

                    i++;
                }

                //Create a PieChart object of size 360 x 300 pixels
                PieChart c = new PieChart(g.Width, g.Height);

                //c.setPieSize();



                //	title
                c.addTitle(g.Title);


                //	legend
                if (g.Legend)
                {
                    c.addLegend(g.LegendX, g.LegendY, g.LegendVertical);
                }

                //	wallpaper
                if (g.Wallpaper != "")
                {
                    c.setBgImage(context.Server.MapPath("~/assets/common/images/" + g.Wallpaper));
                }
                //  background color
                c.setBackground(Convert.ToInt32(g.BackgroundColor, 16),
                    Convert.ToInt32(g.BackgroundEdgeColor, 16), g.BackgroundDepth);


                //	output the chart
                img = c.makeImage();

                img.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                //	include tool tip for the chart
                //viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                //	"title='{xLabel}: US${value}K'");
            }
            catch (System.Exception ex)
            {
                ReportGraph.sendError(context, ex.Message);
            }

        }
    }
}
