using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using netpoint.api.common;
using netpoint.api.prospecting;

namespace netpoint.common.controls
{

    public class Link : IHttpHandler
    {

        private HttpContext _context;
        private int subscriberid = 0;
        private int campaignid = 0;
        private string link = "";

        public void ProcessRequest(HttpContext context)
        {
            _context = context;
            getRequestVars();
            if (subscriberid > 0)
            {
                NPContactSubscriber cs = new NPContactSubscriber(subscriberid);
                NPCommunicationLog cl = new NPCommunicationLog();
                cl.CampaignID = campaignid;
                cl.DirectionFlag = "I";
                cl.Email = cs.Email;
                cl.LogStatus = "C";
                cl.LogType = "link";
                cl.ProspectID = cs.ProspectID;
                cl.UserID = cs.UserID;
                cl.Message = _context.Request.UserHostAddress;
                cl.Subject = link;
                cl.TimeStamp = DateTime.Now;
                cl.Save();
            }
            _context.Response.Redirect(link);
        }

        public bool IsReusable
        {
            get { return true; }
        }

        private void getRequestVars()
        {
            try
            {
                subscriberid = Convert.ToInt32(_context.Request.QueryString["subscriberid"]);
                campaignid = Convert.ToInt32(_context.Request.QueryString["campaignid"]);
                link = _context.Request.QueryString["link"].ToString();
            }
            catch (Exception E)
            {
                string devnull = E.ToString();
            }
        }

    }
}