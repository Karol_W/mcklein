namespace netpoint.common.controls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api;
    using netpoint.api.account;
    using netpoint.classes;
    using System.Resources;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class UserProfileBlock : System.Web.UI.UserControl
    {

        NPBasePage _bp;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            _bp = (NPBasePage)Page;

            if (!Page.IsPostBack)
            {
                NPUser user = new NPUser(_bp.UserID);
                txtTitle.Text = user.Title;
                txtFirstName.Text = user.FirstName;
                txtMiddleName.Text = user.MiddleName;
                txtLastName.Text = user.LastName;
                txtSuffix.Text = user.Suffix;
                Email.Text = user.Email;
                txtDayPhone.Text = user.DayPhone;
                txtEveningPhone.Text = user.EveningPhone;
                ProfileNotes.Text = user.ProfileNotes;
                Points.Text = user.Points.ToString();
                AddDate.Text = user.AddDate.ToShortDateString();
                txtLoginName.Text = user.LoginName;
                if (user.ProfilePicture != null && user.ProfilePicture.Length > 0)
                {
                    ProfilePicture.ImageUrl = user.ProfilePicture;
                }
                else
                {
                    ProfilePicture.Visible = false;
                }

                UpdateButton.ImageUrl = _bp.ResolveImage("buttons", "update.gif");
            }
        }

        public void UpdateData(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            NPUser user = new NPUser(_bp.UserID);

            if (user.LoginName != txtLoginName.Text.Trim() && txtLoginName.Text.Trim() != "")
            {
                user.LoginName = txtLoginName.Text.Trim();
                List<NPError> errors = new List<NPError>();
                user.Valid(errors);
                if (errors.Count != 0)
                {
                    DisplayErrors(errors);
                    return;
                }

                user.Save();
            }

            if (txtTitle.Text.Length > txtTitle.MaxLength)
            {
                sysMessage.Text = errTitleExceedsMaximumLength.Text;
                sysMessage.Visible = true;
            }
            else
            {
                user.Title = txtTitle.Text;
                user.FirstName = txtFirstName.Text;
                user.MiddleName = txtMiddleName.Text;
                user.LastName = txtLastName.Text;
                user.Suffix = txtSuffix.Text;
                user.Email = Email.Text;
                user.DayPhone = txtDayPhone.Text;
                user.EveningPhone = txtEveningPhone.Text;
                user.ProfileNotes = ProfileNotes.Text;
                user.ProfilePicture = ProfilePicture.ImageUrl;
                user.Save();
                sysMessage.Text = ltlSaveMessage.Text;
                sysMessage.Visible = true;
            }
        }

        private void DisplayErrors(List<NPError> theErrors)
        {
            string separator = "<br />";
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.USER_LoginNameExists:
                        sysMessage.Text += hdnLoginNameMustBeUnique.Text + separator;
                        break;                    
                    default:
                        throw npe;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
