using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.common;
using netpoint.classes;

namespace netpoint.common.controls
{

    public partial class SearchSetListPanel : NPBasePage
    {

        private string _searchType;

        protected void Page_Load(object sender, EventArgs e)
        {
            _searchType = Server.UrlDecode(Request["searchtype"]);
            if (!Page.IsPostBack)
            {
                BindDDL();
            }
        }

        public void WriteScript()
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "script", "window.opener.location=window.opener.location;window.close();", true);
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            if (tbSaveName.Text != "")
            {
                NPSearchSet ss = new NPSearchSet(UserID, "C", _searchType, ConnectionString);
                ss.ObjectType = _searchType.ToString();
                ss.SetName = tbSaveName.Text;
                ss.SetType = "L";
                ss.Save();
                WriteScript();
                BindDDL();
                tbSaveName.Text = "";
            }
        }

        private void BindDDL()
        {
            ddlRemoveSet.DataSource = NPSearchSet.GetSets(UserID, "L", _searchType, ConnectionString);
            ddlRemoveSet.DataValueField = "SearchSetID";
            ddlRemoveSet.DataTextField = "SetName";
            ddlRemoveSet.DataBind();
        }

        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
            NPSearchSet ss = new NPSearchSet(Convert.ToInt32(ddlRemoveSet.SelectedValue), ConnectionString);
            ss.Clear();
            ss.Delete();
            WriteScript();
            BindDDL();
        }
    }
}
