namespace netpoint.common.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.common;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for ResourcePage.
    /// </summary>
    public partial class ResourcePage : System.Web.UI.UserControl
    {

        public NPResourcesPages _resourcepage;

        public NPResourcesPages ResourcesPage
        {
            get { return _resourcepage; }
            set { _resourcepage = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        public void DataBind(string pageCode)
        {
            if (pageCode == null)
            {
                sysImage.Visible = false;
                sysPageBlurb.Visible = false;
                sysSource.Visible = false;
            }
            else
            {
                _resourcepage = new NPResourcesPages(pageCode);

                if (_resourcepage.ImageUrl != "")
                    sysImage.ImageUrl = _resourcepage.ImageUrl;
                else
                    sysImage.Visible = false;

                if (_resourcepage.PageBlurb == "")
                    sysPageBlurb.Visible = false;
                else
                    sysPageBlurb.Text = _resourcepage.PageBlurb;

                if (_resourcepage.PageSourceCode == "")
                    sysSource.Visible = false;
                else
                    sysSource.Text = _resourcepage.PageSourceCode;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
