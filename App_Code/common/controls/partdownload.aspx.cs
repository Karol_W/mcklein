using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using System.IO;

namespace netpoint.common.controls
{
    public partial class partdownload : netpoint.classes.NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserID != "")
            {
                if (Request["pdid"] != null)
                {
                    NPPartDownload pd = new NPPartDownload(Convert.ToInt32(Request["pdid"]), ConnectionString);
                    if (NPPartDownload.CanDownload(pd.DownloadID, UserID, ConnectionString))
                    {
                        string filename = Server.MapPath("~/assets/" + pd.Location);
                        FileInfo file = new FileInfo(filename);
                        Response.ClearContent();
                        Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", file.Name));
                        Response.AddHeader("Content-Length", file.Length.ToString());
                        Response.ContentType = "application/octet-stream";
                        Response.TransmitFile(filename);
                        Response.End();
                    }
                    else
                    {
                        Response.Write(errNotAuthorized.Text);
                    }
                }
                else
                {
                    Response.Write(errNOID.Text);
                }
            }
            else
            {
                Response.Write(errNotLoggedID.Text);
            }
        }
    }
}
