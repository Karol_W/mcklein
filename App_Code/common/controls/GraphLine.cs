using System;
using System.Web;
using System.Data;
using System.Drawing;
using netpoint.api;
using netpoint.api.reports;
using ChartDirector;
using System.Drawing.Imaging;
using netpoint.api.common;
using netpoint.api.data;

namespace netpoint.common.controls
{
    /// <summary>
    /// Summary description for GraphLine.
    /// </summary>
    public class GraphLine : System.Web.IHttpHandler
    {
        public GraphLine() { }

        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context)
        {
            BuildGraph(context, int.Parse(context.Request.QueryString["graphid"]));
        }

        public bool IsReusable { get { return true; } }

        #endregion

        private void BuildGraph(HttpContext context, int reportGraphID)
        {
            ReportGraph g = new ReportGraph(reportGraphID);
            netpoint.api.reports.Report rpt = new netpoint.api.reports.Report(g.ReportCode, NPConnection.GblConnString);
            DataParameters dp = new DataParameters(rpt.FinalParameters.Count);
            System.Drawing.Image img;
            int i = 0;
            int ii;

            try
            {
                DataTable theTable;
                //	get parameters from querystring
                foreach (netpoint.api.reports.ReportParameter rp in rpt.FinalParameters)
                {
                    if (context.Request.QueryString[rp.VariableName] != null)
                    {
                        rp.SetValue(context.Request.QueryString[rp.VariableName]);
                    }
                }
                //	add the params 
                foreach (ReportParameter rp in rpt.FinalParameters)
                {
                    dp.Add(rp.DataType, rp.VariableName, rp.Value);
                }
                //	run the query
                if (g.Query == null || g.Query.Query == "")
                {
                    throw (new NPError("No query has been set for this graph.", NPErrorType.RPG_NoQuery));
                }
                else
                {
                    theTable = DataFunctions.GetDataTable(g.Query.Query, dp.Parameters, NPConnection.GblConnString);
                }

                double[][] data = new double[theTable.Columns.Count - 1][];
                string[] label = new string[theTable.Rows.Count];
                for (i = 0; i < data.Length; i++)
                {
                    data[i] = new double[theTable.Rows.Count];
                }
                string[] legend = new string[data.Length];

                //	First column is assumed to be labels
                //	each column after is assumed to be data layer
                i = 0;
                foreach (DataRow dr in theTable.Rows)
                {
                    label[i] = dr.ItemArray[0].ToString();

                    for (ii = 0; ii < data.Length; ii++)
                    {
                        data[ii][i] = Convert.ToDouble(dr.ItemArray[ii + 1]);
                    }

                    i++;
                }

                XYChart c = new XYChart(g.Width, g.Height);

                //	legend
                if (g.Legend)
                {
                    c.addLegend(g.LegendX, g.LegendY, g.LegendVertical);
                }

                //	plot area
                if (g.Width - 50 > 0 && g.Height - 50 > 0)
                {
                    c.setPlotArea(Convert.ToInt32(g.Property("plotareax").PropertyValue),
                        Convert.ToInt32(g.Property("plotareay").PropertyValue),
                        Convert.ToInt32(g.Property("plotareawidth").PropertyValue),
                        Convert.ToInt32(g.Property("plotareaheight").PropertyValue));
                }
                else
                {
                    c.setPlotArea(0, 0, 50, 50);
                }

                //	wallpaper
                if (g.Wallpaper != "")
                {
                    c.setBgImage(context.Server.MapPath("~/assets/common/images/" + g.Wallpaper));
                }
                //  background color
                c.setBackground(Convert.ToInt32(g.BackgroundColor, 16),
                    Convert.ToInt32(g.BackgroundEdgeColor, 16), g.BackgroundDepth);

                //	layers
                i = 0;
                Layer[] layer = new Layer[g.Layers.Count];
                foreach (ReportGraphLayer gl in g.Layers)
                {
                    if (gl.ReportGraphType.LayerCode == "line")
                    {
                        layer[i] = c.addLineLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "bar")
                    {
                        layer[i] = c.addBarLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "area")
                    {
                        layer[i] = c.addAreaLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "boxwhisker")
                    {
                        //layer[i] = c.addBoxWhiskerLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "candlestick")
                    {
                        //layer[i] = c.addCandleStickLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "hloc")
                    {
                        //layer[i] = c.addHLOCLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "scatter")
                    {
                        //layer[i] = c.addScatterLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName, gl.Depth);
                    }
                    else if (gl.ReportGraphType.LayerCode == "spline")
                    {
                        layer[i] = c.addSplineLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName);
                    }
                    else if (gl.ReportGraphType.LayerCode == "stepline")
                    {
                        layer[i] = c.addStepLineLayer(data[i], Convert.ToInt32(gl.Color, 16), gl.LegendName);
                    }
                    else if (gl.ReportGraphType.LayerCode == "trend")
                    {
                        //layer[i] = c.addTrendLayer(
                    }

                    if (gl.Label)
                    {
                        layer[i].getDataSet(0).setDataLabelStyle(gl.LabelFont, gl.LabelSize, Convert.ToInt32(gl.LabelColor, 16), gl.LabelAngle);
                    }
                    if (TranslateSymbol(gl.Symbol) != Chart.NoSymbol &&
                        gl.Symbol != "")
                    {
                        layer[i].getDataSet(0).setDataSymbol(TranslateSymbol(gl.Symbol), gl.SymbolSize, Convert.ToInt32(gl.SymbolFillColor, 16), Convert.ToInt32(gl.SymbolEdgeColor, 16), gl.SymbolLineWidth);
                    }

                    if (gl.LineWidth > 0)
                    {
                        layer[i].setLineWidth(gl.LineWidth);
                    }

                    i++;
                }

                //	X-Axis
                if (g.Property("xaxis").PropertyValue != null &&
                    g.Property("xaxis").PropertyValue != "")
                {
                    ReportGraphAxis xAxis = new ReportGraphAxis(Convert.ToInt32(g.Property("xaxis").PropertyValue));

                    c.xAxis().setTitle(xAxis.Title, xAxis.TitleFont, xAxis.TitleFontSize, Convert.ToInt32(xAxis.TitleFontColor, 16));

                    if (xAxis.UpperLimit > -1 && xAxis.LowerLimit > -1)
                    {
                        c.xAxis().setLinearScale(xAxis.LowerLimit, xAxis.UpperLimit, xAxis.MajorTicInterval, xAxis.MajorTicInterval);
                    }

                    if (xAxis.TitleAlignment != "")
                    {
                        c.xAxis().setTitlePos(TranslateAlignment(xAxis.TitleAlignment), xAxis.TitleGap);
                    }

                }
                c.xAxis().setLabels(label);


                //	Y-Axis
                if (g.Property("yaxis").PropertyValue != null &&
                    g.Property("yaxis").PropertyValue != "")
                {
                    ReportGraphAxis yAxis = new ReportGraphAxis(Convert.ToInt32(g.Property("yaxis").PropertyValue));

                    c.yAxis().setTitle(yAxis.Title, yAxis.TitleFont, yAxis.TitleFontSize, Convert.ToInt32(yAxis.TitleFontColor, 16));

                    if (yAxis.LowerLimit > -1 && yAxis.UpperLimit > -1)
                    {
                        c.yAxis().setLinearScale(yAxis.LowerLimit, yAxis.UpperLimit, yAxis.MajorTicInterval, yAxis.MinorTicInterval);
                    }

                    if (yAxis.TitleAlignment != "")
                    {
                        c.yAxis().setTitlePos(TranslateAlignment(yAxis.TitleAlignment), yAxis.TitleGap);
                    }
                }

                //	title
                c.addTitle(g.Title, g.TitleFont, g.TitleFontSize, Convert.ToInt32(g.TitleColor, 16),
                    Convert.ToInt32(g.TitleBgColor, 16), Convert.ToInt32(g.TitleBgEdgeColor, 16));


                //	output the chart
                img = c.makeImage();

                img.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                //	include tool tip for the chart
                //viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                //	"title='{xLabel}: US${value}K'");
            }
            catch (System.Exception ex)
            {
                ReportGraph.sendError(context, ex.Message);
            }

        }



        private int TranslateSymbol(string symbol)
        {
            switch (symbol)
            {
                case "circle": return Chart.CircleSymbol;
                case "cross2": return Chart.Cross2Symbol;
                case "cross": return Chart.CrossSymbol;
                case "diamond": return Chart.DiamondSymbol;
                case "invertedtriangle": return Chart.InvertedTriangleSymbol;
                case "lefttriangle": return Chart.LeftTriangleSymbol;
                case "righttriangle": return Chart.RightTriangleSymbol;
                case "square": return Chart.SquareSymbol;
                case "triangle": return Chart.TriangleSymbol;
                default: return Chart.NoSymbol;
            }
        }

        private int TranslateAlignment(string align)
        {
            switch (align)
            {
                case "bottomcenter": return Chart.BottomCenter;
                case "bottomleft": return Chart.BottomLeft;
                case "bottomright": return Chart.BottomRight;
                case "left": return Chart.Left;
                case "right": return Chart.Right;
                case "topcenter": return Chart.TopCenter;
                case "topleft": return Chart.TopLeft;
                case "topright": return Chart.TopRight;
                default: return Chart.BottomCenter;
            }
        }
    }
}
