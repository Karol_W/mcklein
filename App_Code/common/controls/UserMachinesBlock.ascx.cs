namespace netpoint.common.controls
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.classes;
    using System.Resources;

    /// <summary>
    ///	Summary description for CategoriesBlock.
    /// </summary>
    public partial class UserMachinesBlock : System.Web.UI.UserControl
    {

        NPBasePage bp;
        private NPUser _user;

        public NPUser User
        {
            get { return _user; }
            set { _user = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)Page;
            machine.CatalogID = bp.Catalog.CatalogID;
            if (!Page.IsPostBack && _user != null)
            {
                BindGrid();

                btnBack.ImageUrl = bp.ResolveImage("icons", "cancel.gif");
                btnSave.ImageUrl = bp.ResolveImage("buttons", "saveinfo.gif");
                btnAdd.ImageUrl = bp.ResolveImage("buttons", "addinfo.gif");
            }
        }

        private void BindGrid()
        {
            grid.DataSource = _user.Machines;
            grid.DataBind();
        }

        private void BindDetail(int umid)
        {
            NPUserMachine um = new NPUserMachine(umid);
            txtMachineName.Text = um.MachineName;
            if (umid != 0)
            {
                machine.PreFill(um.MachineID);
            }
            else
            {
                machine.ResetMachines(this, new System.Web.UI.ImageClickEventArgs(1, 1));
            }
            calPurchaseDate.SelectedDate = um.PurchaseDate;
            txtNotes.Text = um.MachineNotes;
            ViewState["umid"] = umid;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            grid.Visible = grid.Rows.Count > 0;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack.Click += new System.Web.UI.ImageClickEventHandler(this.btnBack_Click);
            this.btnSave.Click += new System.Web.UI.ImageClickEventHandler(this.btnSave_Click);
            this.btnAdd.Click += new System.Web.UI.ImageClickEventHandler(this.btnAdd_Click);

        }
        #endregion

        private void btnBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            pnlDetail.Visible = false;
        }

        private void btnAdd_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            BindDetail(0);
            pnlDetail.Visible = true;
        }

        private void btnSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            imgError.Visible = false;
            lblMachineRequired.Visible = false;
            if (machine.MachineID > 0)
            {
                NPUserMachine um = new NPUserMachine(Convert.ToInt32(ViewState["umid"]));
                _user = new NPUser(((NPBasePage)Page).UserID);

                um.UserID = _user.UserID; ;
                um.MachineName = txtMachineName.Text;
                um.MachineID = machine.MachineID;
                um.MachineImage = "";
                um.PurchaseDate = calPurchaseDate.SelectedDate;
                um.MachineNotes = txtNotes.Text;
                um.Save();
                BindGrid();
            }
            else
            {
                imgError.Visible = true;
                lblMachineRequired.Visible = true;
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton edit = (ImageButton)e.Row.FindControl("btnEdit");
            ImageButton delete = (ImageButton)e.Row.FindControl("btnDelete");
            if (edit != null && delete != null)
            {
                NPUserMachine um = (NPUserMachine)e.Row.DataItem;
                if (um.Locked)
                {
                    edit.Visible = false;
                    delete.Visible = false;
                }

                edit.ImageUrl = bp.ResolveImage("buttons", "edit.gif");
                delete.ImageUrl = bp.ResolveImage("buttons", "remove.gif");
            }
        }

        protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DataKey data = grid.DataKeys[e.NewEditIndex];
            BindDetail(Convert.ToInt32(data["UMID"]));
            pnlDetail.Visible = true;
        }

        protected void grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            DataKey data = grid.DataKeys[e.RowIndex];
            NPUserMachine um = new NPUserMachine(Convert.ToInt32(data["UMID"]));
            um.Delete();
            Response.Redirect("~/common/user/equipment.aspx", true);
        }
    }
}
