using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.data;
using netpoint.api.catalog;
using netpoint.api.common;
using netpoint.api.commerce;
using netpoint.classes;

namespace netpoint.common.controls
{

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class OrderListsBlock : System.Web.UI.UserControl
    {
        private NPBasePage bp;

        public Unit BorderWidth
        {
            set
            {
                gridSavedLists.BorderWidth = value;
                gridDetail.BorderWidth = value;
            }
        }

        private bool AddListVisible
        {
            get { return gridSavedLists.FindColumn("colAdd").Visible; }
            set { gridSavedLists.FindColumn("colAdd").Visible = value; }
        }

        private bool AddDetailVisible
        {
            get { return gridDetail.FindColumn("colAdd").Visible; }
            set { gridDetail.FindColumn("colAdd").Visible = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)this.Page;
            if (!Page.IsPostBack)
            {
                gridDetail.Visible = false;
                gridSavedLists.Visible = true;
                AddListVisible = !((NPBasePage)this.Page).Catalog.ViewOnly;
                if (Request.QueryString["AddPartNo"] != null)
                {
                    AddPart(Request.QueryString["AddPartNo"]);
                }
                BindLists();
            }

        }

        private void BindLists()
        {
            gridSavedLists.DataSource = OrderReports.GetSavedLists(bp.UserID, bp.ConnectionString);
            gridSavedLists.DataBind();
        }

        private void BindDetail(int orderid)
        {
            gridDetail.Visible = true;
            gridSavedLists.Visible = false;
            AddDetailVisible = !((NPBasePage)this.Page).Catalog.ViewOnly;
            NPOrder o = new NPOrder(orderid);
            o.Calculate(); // Force a refresh of pricing
            gridDetail.DataSource = o.OrderDetail;
            gridDetail.DataBind();
        }

        private void AddPart(string partNo)
        {
            if (bp.UserID == "")
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=savedlists~AddPartNo=" + Server.UrlEncode(partNo), true);
            }
            else
            {
                ArrayList orders = OrderReports.GetSavedLists(bp.UserID, bp.ConnectionString);
                DateTime theDate = DateTime.MinValue;
                NPOrder theOrder = new NPOrder(bp.Connection);
                NPWebConfig config = new NPWebConfig("Commerce", "local", "local", "WishListName");
                if (orders.Count > 0)
                {
                    foreach (NPOrder order in orders)
                    {
                        if (order.CartName.Equals(config.Value) || order.CartType == "L")
                        {
                            theOrder = order;
                            theOrder.SuppressExtendedProperties = false;
                            theOrder.ReloadOrderDetail();
                        }
                    }
                }
                if (theOrder.AccountID == null)
                {
                    if (config.Value == null || config.Value == "")
                    {
                        theOrder.CartName = "Wish List";
                    }
                    else
                    {
                        theOrder.CartName = config.Value;
                    }
                    theOrder.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                    theOrder.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID, CartType.List);
                }
                if (Request.QueryString["variant"] == null)
                {
                    theOrder.AddPart(partNo, 1, bp.Catalog.CatalogCode, "", bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                }
                else
                {
                    NPVariant.AddVariantToCart(partNo, bp.AccountID, bp.SessionID, theOrder.OrderID, bp.PriceList, bp.ConnectionString);
                }
                BindDetail(theOrder.OrderID);
            }
        }

        protected void gridSavedLists_RowCommand(object source, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            NPOrder o;
            switch (e.CommandName)
            {
                case "email":
                    Response.Redirect("emailtofriend.aspx?orderid=" + e.CommandArgument.ToString(), true);
                    break;
                case "remove":
                    o = new NPOrder(Convert.ToInt32(e.CommandArgument));
                    o.Delete();
                    BindLists();
                    break;
                case "addtocart":
                    NPOrder cart = new NPOrder(bp.UserID, bp.SessionID);
                    if (!cart.Initialized)
                    {
                        cart.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                        cart.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID);
                    }
                    o = new NPOrder(Convert.ToInt32(e.CommandArgument));
                    o.ReassignDetail(cart.OrderID, bp.PriceList);
                    Response.Redirect("~/commerce/cart.aspx", true);
                    break;
                case "showdetail":
                    BindDetail(Convert.ToInt32(e.CommandArgument));
                    if (e.CommandSource is LinkButton)
                    {
                        this.RaiseBubbleEvent(gridSavedLists, new NPEventArgs("gridSavedLists_ItemCommand", ((LinkButton)e.CommandSource).Text));
                    }
                    break;
            }
        }

        protected void gridDetail_RowCommand(object source, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            NPOrder o;
            NPOrderDetail od;
            if (e.CommandName == "remove")
            {
                od = new NPOrderDetail(Convert.ToInt32(e.CommandArgument));
                od.Delete();
                o = new NPOrder(od.OrderID);
                o.Calculate();
                BindDetail(od.OrderID);
                if (gridDetail.Rows.Count == 0)
                {
                    //	we deleted all of the detail, dump the order
                    o.Delete();
                    Response.Redirect("~/common/accounts/savedlists.aspx");
                }
            }
            if (e.CommandName == "addtocart")
            {
                od = new NPOrderDetail(Convert.ToInt32(e.CommandArgument));
                int listID = od.OrderID;
                od.MoveToCart(bp.SessionID, bp.PriceList);
                o = new NPOrder(listID);
                o.Calculate();
                Response.Redirect("~/commerce/cart.aspx", true);
            }
        }

        protected void gridDetail_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            HyperLink lnk = (HyperLink)e.Row.FindControl("lnkPartImage");
            ImageButton btnDeleteDetail = (ImageButton)e.Row.FindControl("btnDeleteDetail");
            ImageButton btnAddDetailToCart = (ImageButton)e.Row.FindControl("btnAddDetailToCart");
            netpoint.commerce.controls.PartDescription partDesc = (netpoint.commerce.controls.PartDescription)e.Row.FindControl("partDesc");
            NPOrderDetail od = (NPOrderDetail)e.Row.DataItem;

            if (od != null)
            {
                if (lnk != null && od.AggregateType != AggregatePartType.Discount)
                {
                    NPPart p = new NPPart(od.PartNo);
                    lnk.ImageUrl = bp.ProductThumbImage + p.ThumbNail;
                    lnk.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + p.PartNo;
                }
                if ((od.AggregateType == AggregatePartType.SalesKit && od.Part.AggregateType != AggregatePartType.SalesKit)
                           || od.AggregateType == AggregatePartType.Discount)
                {
                    btnAddDetailToCart.Visible = false;
                    btnDeleteDetail.Visible = false;
                }
                if (partDesc != null)
                {
                    partDesc.LoadDescription(od, bp.ConnectionString);
                }
            }
        }

        protected void gridSavedLists_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            ImageButton btnDeleteList = (ImageButton)e.Row.FindControl("btnDeleteList");
            ImageButton btnEmailList = (ImageButton)e.Row.FindControl("btnEmailList");
            ImageButton btnAddListToCart = (ImageButton)e.Row.FindControl("btnAddListToCart");
            NPOrder o = (NPOrder)e.Row.DataItem;

            if (btnDeleteList != null)
            {
                btnDeleteList.ImageUrl = ((NPBasePage)this.Page).ResolveImage("icons", "delete.gif");
            }
            if (btnEmailList != null)
            {
                btnEmailList.ImageUrl = ((NPBasePage)this.Page).ResolveImage("buttons", "sendtofriend.gif");
            }
            if (btnAddListToCart != null)
            {
                btnAddListToCart.ImageUrl = ((NPBasePage)this.Page).ResolveImage("icons", "addtocart.gif");
            }
        }

        protected void gridSavedLists_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridSavedLists.PageIndex = e.NewPageIndex;
            gridSavedLists.DataSource = OrderReports.GetSavedLists(bp.UserID, bp.ConnectionString);
            gridSavedLists.DataBind();
        }

    }
}
