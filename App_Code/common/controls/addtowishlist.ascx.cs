using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;
using netpoint.api.account;
using netpoint.api;
using netpoint.api.data;
using netpoint.api.utility;

namespace netpoint.common.controls
{
    public partial class AddToWishList : System.Web.UI.UserControl
    {

        public string PartNo
        {
            set { ViewState["PartNo"] = value; }
        }

        public System.Web.UI.WebControls.ListControl ListControl
        {
            get { return _ddl; }
            set { _ddl = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void WishButton_Click(object sender, EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            string partNo = ViewState["PartNo"].ToString();
            if (partNo == null)
            {
                if (_ddl != null)
                {
                    partNo = _ddl.SelectedValue;
                }
                else
                {

                }
            }
            if (bp.UserID == "")
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=partdetail~PartNo=" + Server.UrlEncode(partNo));
            }
            else
            {
                ArrayList orders = OrderReports.GetSavedLists(bp.UserID, bp.ConnectionString);
                DateTime theDate = DateTime.MinValue;
                NPOrder theOrder = null;
                if (orders.Count > 0)
                {
                    foreach (NPOrder order in orders)
                    {
                        if (theDate == DateTime.MinValue || order.CreateDate > theDate)
                        {
                            theDate = order.CreateDate;
                            theOrder = order;
                        }
                    }
                }
                else
                {
                    theOrder = new NPOrder(bp.UserID, bp.SessionID);
                    theOrder.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                    theOrder.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID, CartType.List);
                }
                theOrder.AddPart(partNo, 1, bp.Catalog.CatalogCode, "", bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                if (orders.Count <= 0)
                {
                    Response.Redirect("~/common/accounts/savecart.aspx");
                }
                Response.Redirect("~/common/accounts/savedlists.aspx");
            }
        }
    }
}