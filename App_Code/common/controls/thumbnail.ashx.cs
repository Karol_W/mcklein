using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace netpoint.common.controls
{

    public class ThumbnailHandler : IHttpHandler
    {

        private HttpContext _context;
        private string _imagefile = "";
        private string _thumbnailsize = "50";

        public void ProcessRequest(HttpContext context)
        {
            _context = context;
            getRequestVars();
            if (_imagefile == "")
            {
                sendError();
            }
            else
            {
                if (File.Exists(_context.Server.MapPath(_imagefile)))
                {
                    sendFile();
                }
                else
                {
                    sendError();
                }
            };
        }


        public bool IsReusable
        {
            get { return true; }
        }

        private void getRequestVars()
        {
            try
            {
                _imagefile = _context.Request.QueryString["image"].ToString();
                _thumbnailsize = _context.Request.QueryString["size"].ToString();
            }
            catch (Exception E)
            {
                string devnull = E.ToString();
            }
        }

        private Size newthumbSize(int currentwidth, int currentheight, int newsize)
        {
            // Calculate the Size of the new image
            int thewidth = 0;
            int theheight = 0;

            if (currentwidth > newsize || currentheight > newsize)
            {
                if (currentheight > currentwidth)
                {
                    //portrait
                    theheight = newsize;
                    thewidth = Convert.ToInt32(((double)newsize / (double)currentheight) * (double)currentwidth);
                }
                else
                {
                    //landscape
                    theheight = Convert.ToInt32(((double)newsize / (double)currentwidth) * (double)currentheight);
                    thewidth = newsize;
                }
                if (thewidth > 0 && theheight > 0)
                {
                    Size newSize = new Size(thewidth, theheight);
                    return newSize;
                }
                else
                {
                    Size defaultSize = new Size(1, 1);
                    return defaultSize;
                }
            }
            else
            {
                Size oldSize = new Size(currentwidth, currentheight);
                return oldSize;
            }
        }

        private void sendFile()
        {
            try
            {
                //create new image and bitmap objects. Load the image file and put into a resized bitmap.
                System.Drawing.Image g = System.Drawing.Image.FromFile(_context.Server.MapPath(_imagefile));
                ImageFormat thisformat = g.RawFormat;
                int its = Convert.ToInt32(_thumbnailsize);
                Size thumbSize = newthumbSize(g.Width, g.Height, its);
                //Response.Write(thumbSize.Width.ToString());
                Bitmap imgOutput = new Bitmap(g);
                //set the contenttype
                if (thisformat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                {
                    _context.Response.ContentType = "image/gif";
                }
                else if (thisformat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                {
                    _context.Response.ContentType = "image/png";
                }
                else
                {
                    _context.Response.ContentType = "image/jpeg";
                }
                if (g.Width == thumbSize.Width && g.Height == thumbSize.Height)
                {
                    imgOutput = new Bitmap(_context.Server.MapPath(_imagefile));
                }
                else
                {
                    imgOutput = new Bitmap(thumbSize.Width, thumbSize.Height);
                    Graphics.FromImage(imgOutput).DrawImage(g, 0, 0, thumbSize.Width, thumbSize.Height);
                }
                //send the resized image to the viewer
                imgOutput.Save(_context.Response.OutputStream, thisformat); //output to the user
                //tidy up
                g.Dispose();
                imgOutput.Dispose();
            }
            catch (Exception ex)
            {
                _context.Response.Write(ex.Message);
                //sendError();
            }
        }


        private void sendError()
        {
            //if no height, width, src then output "error"
            int its = Convert.ToInt32(_thumbnailsize);
            Bitmap imgOutput = new Bitmap(1, 1, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(imgOutput); //create a new graphic object from the above bmp
            g.Clear(Color.White); //blank the image
            //g.DrawString("no\nimage", new Font("verdana",9,FontStyle.Bold),SystemBrushes.WindowText, new PointF(2,2));
            //set the contenttype
            _context.Response.ContentType = "image/gif";
            //send the resized image to the viewer
            imgOutput.Save(_context.Response.OutputStream, ImageFormat.Gif); //output to the user
            //tidy up
            g.Dispose();
            imgOutput.Dispose();
        }



    }
}