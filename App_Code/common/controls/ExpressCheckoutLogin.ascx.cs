﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using netpoint.classes;
using netpoint.providers;
using System.Web.Security;
using netpoint.api.account;

namespace netpoint.common.controls
{
    /// <summary>
    ///		Summary description for express checkout login.
    /// </summary>
    public partial class ExpressCheckoutLogin : System.Web.UI.UserControl
    {
        private NPBasePage _bp;

        protected void Page_Load(object sender, EventArgs e)
        {
            this._bp = (NPBasePage)Page;

            if (!IsPostBack)
            {
                btnAutoCreateAccount.ImageUrl = _bp.ResolveImage("buttons", "expresscheckout.gif");
                txtEmail.Attributes.Add("onKeyPress", "checkEnter(event, '" + btnAutoCreateAccount.UniqueID + "');");
            }
        }

        protected void ExpressCheckout_Click(object sender, CommandEventArgs e)
        {
            if (ValidEmailAddress())
            {
                CreateExpressCheckoutUser();
            }
            else
            {
                imgEmailWarning.Visible = true;
                pnlError.Visible = true;
            }
        }

        private void CreateExpressCheckoutUser()
        {
            NPMembershipProvider newProvider = new NPMembershipProvider();
            MembershipCreateStatus providerStatus;
            MembershipUser memUser = newProvider.CreateUser(NPMembershipProvider.ExpressCheckoutUser, "", "", "", "", true, null, out providerStatus);

            if (providerStatus == MembershipCreateStatus.Success)
            {
                NPUser newUser = new NPUser(memUser.UserName);
                if (newUser.Initialized)
                {
                    newUser.AddDate = DateTime.Now;
                    newUser.LastPasswordChangeDate = DateTime.Now;
                    newUser.ExpressCheckoutUser = true;
                    newUser.Email = txtEmail.Text;
                    newUser.Save();
                    newUser.ResolveCarts(this._bp.SessionID);
                    this._bp.Login(newUser.UserID, newUser.AccountID.Substring(newUser.AccountID.Length - 8), false);
                }
            }
        }

        protected bool ValidEmailAddress()
        {
            bool valid = false;

            if (txtEmail.Text.Length > 0 && txtEmail.Text.Length < 255)
            {
                if (txtEmail.Text.Contains("@"))
                {
                    valid = true;
                }
            }
            
            return valid;
        }
    }
}