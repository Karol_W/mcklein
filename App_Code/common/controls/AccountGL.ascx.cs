namespace netpoint.common.controls
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.classes;
    using System.Resources;
    using netpoint.api.books;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class AccountGL : System.Web.UI.UserControl
    {

        private NPBasePage _bp;
        private string _accountID;

        public string AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            DataTable dt = GLTransaction.Search(_accountID, "PostDate", "", DateTime.Now.AddDays(-365), DateTime.Now, _bp.ConnectionString, true);
            grid.DataSource = dt.DataSet;
            grid.DataMember = "DataTable";
            grid.DataBind();
        }

        protected void grid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            Literal ltlPostDate = (Literal)e.Row.FindControl("ltlPostDate");
            if (ltlPostDate != null)
            {
                Literal ltlDocumentType = (Literal)e.Row.FindControl("ltlDocumentType");
                DataRowView drv = (DataRowView)e.Row.DataItem;
                ltlPostDate.Text = Convert.ToDateTime(drv["PostDate"]).ToString("yyyy-MMM-dd");
                switch (drv["DocumentType"].ToString())
                {
                    case "I":
                        ltlDocumentType.Text = hdnInvoice.Text;
                        break;
                    case "P":
                        ltlDocumentType.Text = hdnPayment.Text;
                        break;
                    case "C":
                        ltlDocumentType.Text = hdnCredit.Text;
                        break;
                }
                if (drv["Reference"].ToString() != "")
                {
                    ltlDocumentType.Text += " - " + drv["Reference"].ToString();
                }
            }
        }

        protected void grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grid.PageIndex = e.NewPageIndex;
            BindGrid();
        }

    }
}
