namespace netpoint.common.controls
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for gallery.
    /// </summary>
    public partial class gallery : System.Web.UI.UserControl
    {


        private NPBasePage bp;

        #region properties
        public string imageDirectory
        {
            set
            {
                imagepath.Text = value;
            }
            get
            {
                return imagepath.Text;
            }
        }

        public string thumbnailSize
        {
            set
            {
                imagesize.Text = value;
            }
            get
            {
                return imagesize.Text;
            }
        }

        public string imagesPerColumn
        {
            set
            {
                imagerepeater.RepeatColumns = Convert.ToInt32(value);
            }
            get
            {
                return imagerepeater.RepeatColumns.ToString();
            }
        }

        public string borderWidth
        {
            set
            {
                imagerepeater.BorderWidth = Convert.ToInt32(value);
            }
            get
            {
                return imagerepeater.BorderWidth.ToString();
            }
        }
        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            pageuploader.uploadDirectory = Server.MapPath(imageDirectory);
            loadBrowseTable();
            bp = (NPBasePage)this.Page;
        }


        public void loadBrowseTable()
        {

            DataTable dtPictures = new DataTable();
            DataColumn dcPicturesImage = new DataColumn("Image", typeof(string));
            DataColumn dcPicturesFileName = new DataColumn("FileName", typeof(string));
            DataColumn dcPicturesPath = new DataColumn("Path", typeof(string));
            DataColumn dcPicturesSize = new DataColumn("Size", typeof(string));
            DataColumn dcPicturesDate = new DataColumn("Date", typeof(string));
            DataRow drPictures;
            dtPictures.Columns.Add(dcPicturesImage);
            dtPictures.Columns.Add(dcPicturesFileName);
            dtPictures.Columns.Add(dcPicturesPath);
            dtPictures.Columns.Add(dcPicturesSize);
            dtPictures.Columns.Add(dcPicturesDate);

            System.IO.FileInfo file = null;
            string[] files = null;
            string filename = "";
            files = System.IO.Directory.GetFiles(Server.MapPath(imageDirectory), "*.*");

            for (int i = 0; i <= files.Length - 1; i++)
            {
                //Create a new FileInfo object for this filename
                file = new System.IO.FileInfo(files[i]);
                filename = file.Name.ToString();
                if (filename.EndsWith(".jpg") || filename.EndsWith(".gif"))
                {
                    drPictures = dtPictures.NewRow();
                    drPictures[0] = bp.VirtualPath + "/common/controls/thumbnail.aspx?size=" + thumbnailSize + "&image=" + Server.UrlPathEncode(imageDirectory + "/" + file.Name);
                    drPictures[1] = file.Name;
                    drPictures[2] = Server.UrlPathEncode(imageDirectory + "/" + file.Name);
                    drPictures[3] = (file.Length / 1000) + "kb";
                    drPictures[4] = file.LastWriteTime.ToShortDateString();
                    dtPictures.Rows.Add(drPictures);
                }
            }
            DataView dvPictures = new DataView(dtPictures);
            imagerepeater.DataSource = dvPictures;
            imagerepeater.DataBind();
        }

        public void previewImage(object sender, CommandEventArgs e)
        {
            string previewfile = e.CommandArgument.ToString();
            ImagePreview.ImageUrl = bp.VirtualPath + "/common/controls/thumbnail.aspx?size=300&image=" + Server.UrlPathEncode(previewfile);
            System.IO.FileInfo file = null;
            file = new System.IO.FileInfo(Server.MapPath(previewfile));
            previewName.Text = file.Name.ToString();
            previewSize.Text = Convert.ToInt32(file.Length / 1000).ToString() + "kb";
            previewDate.Text = file.LastWriteTime.ToShortDateString();

            System.Drawing.Image g = System.Drawing.Image.FromFile(Server.MapPath(previewfile));
            previewHeight.Text = g.Height.ToString() + " pixels ";
            previewWidth.Text = g.Width.ToString() + " pixels ";
            previewResolution.Text = g.HorizontalResolution.ToString() + " dpi";
            g.Dispose();

            deleteLink.CommandArgument = previewfile;
            fullsizeLink.NavigateUrl = previewfile;
            showPreview(sender, e);
        }

        public void deleteImage(object sender, CommandEventArgs e)
        {
            ImagePreview.Visible = false;
            ImagePreview.ImageUrl = "";
            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath(e.CommandArgument.ToString()));
            file.Delete();
            loadBrowseTable();
            showBrowse(sender, e);
        }

        public void showBrowse(object sender, System.EventArgs e)
        {
            tabPreview.Visible = false;
            tabUpload.Visible = true;
            tabBrowse.Visible = true;

            PanelBrowse.Visible = true;
            PanelPreview.Visible = false;
            PanelUpload.Visible = false;
        }

        public void showPreview(object sender, System.EventArgs e)
        {
            tabPreview.Visible = true;
            tabUpload.Visible = true;
            tabBrowse.Visible = true;

            PanelBrowse.Visible = false;
            PanelPreview.Visible = true;
            PanelUpload.Visible = false;
        }

        public void showUpload(object sender, System.EventArgs e)
        {
            tabPreview.Visible = false;
            tabUpload.Visible = true;
            tabBrowse.Visible = true;

            PanelBrowse.Visible = false;
            PanelPreview.Visible = false;
            PanelUpload.Visible = true;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
