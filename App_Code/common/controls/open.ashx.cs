using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using netpoint.api.common;
using netpoint.api.prospecting;

namespace netpoint.common.controls
{

    public class Open : IHttpHandler
    {

        private HttpContext _context;
        private int subscriberid = 0;
        private int campaignid = 0;

        public void ProcessRequest(HttpContext context)
        {
            _context = context;
            getRequestVars();
            if (subscriberid > 0)
            {
                NPContactSubscriber cs = new NPContactSubscriber(subscriberid);
                NPCommunicationLog cl = new NPCommunicationLog();
                cl.CampaignID = campaignid;
                cl.DirectionFlag = "I";
                cl.Email = cs.Email;
                cl.LogStatus = "C";
                cl.LogType = "open";
                cl.ProspectID = cs.ProspectID;
                cl.UserID = cs.UserID;
                cl.Message = _context.Request.UserHostAddress;
                cl.Subject = _context.Request.Browser.Platform;
                cl.TimeStamp = DateTime.Now;
                cl.Save();
            }
            sendHolder();
        }

        public bool IsReusable
        {
            get { return true; }
        }

        private void getRequestVars()
        {
            try
            {
                subscriberid = Convert.ToInt32(_context.Request.QueryString["subscriberid"]);
                campaignid = Convert.ToInt32(_context.Request.QueryString["campaignid"]);
            }
            catch (Exception E)
            {
                string devnull = E.ToString();
            }
        }

        private void sendHolder()
        {
            Bitmap imgOutput = new Bitmap(1, 1, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(imgOutput); //create a new graphic object from the above bmp
            g.Clear(Color.Transparent);
            _context.Response.ContentType = "image/gif";
            imgOutput.Save(_context.Response.OutputStream, ImageFormat.Gif); //output to the user
            //tidy up
            g.Dispose();
            imgOutput.Dispose();
        }

    }
}