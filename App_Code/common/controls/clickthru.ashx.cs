using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using netpoint.api.common;

namespace netpoint.common.controls
{

    public class clickthru : IHttpHandler
    {

        private HttpContext _context;
        private int _adid = 0;

        public void ProcessRequest(HttpContext context)
        {
            _context = context;
            getRequestVars();
            if (_adid > 0)
            {
                NPResourcesAds ad = new NPResourcesAds(_adid);
                ad.clicks = ad.clicks + 1;
                ad.Save();
                if (ad.clickurl != "")
                {
                    _context.Response.Redirect(ad.clickurl);
                }
            }
            else
            {
                _context.Response.Write("error: adid not found");
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }

        private void getRequestVars()
        {
            try
            {
                _adid = Convert.ToInt32(_context.Request.QueryString["adid"]);
            }
            catch (Exception E)
            {
                string devnull = E.ToString();
            }
        }
    }
}