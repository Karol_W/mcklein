using System;
using System.Web;
using System.Data;
using System.Drawing;
using netpoint.api;
using netpoint.api.reports;
using ChartDirector;
using netpoint.api.common;
using netpoint.api.data;

namespace netpoint.common.controls
{
    /// <summary>
    /// .
    /// </summary>
    public class GraphPie : System.Web.IHttpHandler
    {

        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context)
        {
            BuildGraph(context, int.Parse(context.Request.QueryString["graphid"]));
        }

        public bool IsReusable { get { return false; } }

        #endregion

        private void BuildGraph(HttpContext context, int reportGraphID)
        {
            ReportGraph g = new ReportGraph(reportGraphID);
            netpoint.api.reports.Report rpt = new netpoint.api.reports.Report(g.ReportCode, NPConnection.GblConnString);
            DataParameters dp = new DataParameters(rpt.FinalParameters.Count);
            System.Drawing.Image img;
            int i = 0;

            try
            {
                //	get parameters from querystring
                foreach (netpoint.api.reports.ReportParameter rp in rpt.FinalParameters)
                {
                    if (context.Request.QueryString[rp.VariableName] != null)
                    {
                        rp.SetValue(context.Request.QueryString[rp.VariableName]);
                    }
                }
                //	add the params 
                foreach (ReportParameter rp in rpt.FinalParameters)
                {
                    dp.Add(rp.DataType, rp.VariableName, rp.Value);
                }
                //	run the query
                DataTable theTable;
                if (g.Query == null || g.Query.Query == "")
                {
                    throw (new NPError("No query has been set for this graph.", NPErrorType.RPG_NoQuery));
                }
                else
                {
                    theTable = DataFunctions.GetDataTable(g.Query.Query, dp.Parameters, NPConnection.GblConnString);
                }

                double[] data = new double[theTable.Rows.Count];
                string[] label = new string[theTable.Rows.Count];

                //	First column is assumed to be labels
                //	each column after is assumed to be data layer
                i = 0;
                foreach (DataRow dr in theTable.Rows)
                {
                    label[i] = dr.ItemArray[0].ToString();

                    data[i] = Convert.ToDouble(dr.ItemArray[1]);

                    i++;
                }

                //	Create a PieChart object of size 360 x 300 pixels
                PieChart c = new PieChart(g.Width, g.Height);


                //	legend
                if (g.Legend)
                {
                    c.addLegend(g.LegendX, g.LegendY, g.LegendVertical);
                }

                //	wallpaper
                if (g.Wallpaper != "")
                {
                    c.setBgImage(context.Server.MapPath("~/assets/common/images/" + g.Wallpaper));
                }

                //  size/location
                c.setPieSize(Convert.ToInt32(g.Property("centerx").PropertyValue), Convert.ToInt32(g.Property("centery").PropertyValue), Convert.ToInt32(g.Property("radius").PropertyValue));

                // data
                c.setData(data, label);

                c.setStartAngle(Convert.ToInt32(g.Property("startangle").PropertyValue));

                // 3D, each slice can have it's own depth
                // we expect a comma delimited string
                // if a value is not set, the first value will be used
                char[] delimit = new char[1];
                delimit[0] = ',';
                string[] d = g.Property("depth").PropertyValue.Replace(" ", "").Split(delimit, theTable.Rows.Count);
                double[] depth = new double[theTable.Rows.Count];
                for (i = 0; i < theTable.Rows.Count; i++)
                {
                    if (i > d.Length - 1)
                    {
                        depth[i] = Convert.ToDouble(d[0]);
                    }
                    else if (d[i] == null || d[i].Trim() == "")
                    {
                        depth[i] = Convert.ToDouble(d[0]);
                    }
                    else
                    {
                        depth[i] = Convert.ToDouble(d[i]);
                    }
                }
                c.set3D(depth, Convert.ToInt32(g.Property("angle").PropertyValue));

                if (g.Property("sidelabel").PropertyValue == "Y")
                {
                    c.setLabelLayout(Chart.SideLayout);
                }
                else
                {
                    c.setLabelPos(Convert.ToInt32(g.Property("labelposition").PropertyValue));
                }

                if (Convert.ToInt32(g.Property("explode").PropertyValue) > -1)
                {
                    c.setExplode(Convert.ToInt32(g.Property("explode").PropertyValue), Convert.ToInt32(g.Property("explodedistance").PropertyValue));
                }

                if (g.Property("transparent").PropertyValue == "Y")
                {
                    c.setColors(Chart.transparentPalette);
                }

                //  label style
                if (g.Property("labelbackground").PropertyValue == "Y")
                {
                    int bkgColor = Convert.ToInt32(g.Property("labelbkgcolor").PropertyValue, 16);
                    int bkgEdgeColor = Convert.ToInt32(g.Property("labelbkgedgecolor").PropertyValue, 16);

                    bkgColor = bkgColor == 0 ? Chart.SameAsMainColor : bkgColor;
                    bkgEdgeColor = bkgEdgeColor == 0 ? Chart.SameAsMainColor : bkgEdgeColor;

                    c.setLabelStyle().setBackground(bkgColor, bkgEdgeColor, Convert.ToInt32(g.Property("labelbkgbevel").PropertyValue));
                }
                c.setLabelStyle(g.Property("labelfont").PropertyValue, Convert.ToDouble(g.Property("labelsize").PropertyValue), Convert.ToInt32(g.Property("labelcolor").PropertyValue, 16));

                //  background color
                c.setBackground(Convert.ToInt32(g.BackgroundColor, 16),
                    Convert.ToInt32(g.BackgroundEdgeColor, 16), g.BackgroundDepth);

                //	title
                c.addTitle(g.Title, g.TitleFont, g.TitleFontSize, Convert.ToInt32(g.TitleColor, 16),
                    Convert.ToInt32(g.TitleBgColor, 16), Convert.ToInt32(g.TitleBgEdgeColor, 16));

                //	output the chart
                img = c.makeImage();

                img.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                //	include tool tip for the chart
                //  viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                //	"title='{xLabel}: US${value}K'");
            }
            catch (System.Exception ex)
            {
                ReportGraph.sendError(context, ex.Message);
            }
        }
    }
}
