using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.encryption;
using netpoint.classes;
using netpoint.providers;

namespace netpoint.common.user
{

    public partial class changepassword : NPBasePage
    {
        override protected void OnInit(EventArgs e)
        {
            IgnorePasswordExpired = true;
            base.OnInit(e);

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
            Response.Cache.SetMaxAge(TimeSpan.FromMilliseconds(0));
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(System.Web.HttpCacheRevalidation.AllCaches);
            Response.Cache.SetLastModified(DateTime.Now);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.UserID == null || this.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }

            if (this.TheSession.PasswordExpired)
            {
                //display error message explaining why user is here
                sysPasswordExpired.Visible = true;
            }

            if (!this.IsPostBack)
            {
                NPUser u = new NPUser(this.UserID);
                if (!u.Initialized)
                {
                    Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
                }

                NPMembershipProvider provider = new NPMembershipProvider(u.LoginName);                
                if (!provider.RequiresQuestionAndAnswer)
                {
                    ltlQuestion.Visible = false;
                    txtQuestion.Visible = false;
                    rfvSecurityQuestion.Enabled = false;

                    ltlAnswer.Visible = false;
                    txtAnswer.Visible = false;
                    rfvSecurityAnswer.Enabled = false;
                }

                btnSubmit.ImageUrl = ((NPBasePage)Page).ResolveImage("buttons", "update.gif");
            }
        }

        protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            NPUser u = new NPUser(UserID);
            NPMembershipProvider provider = new NPMembershipProvider(u.LoginName);

            if (!Encryption.VerifyHash(txtOldPassword.Text, "SHA256", u.Password))
            {
                sysMessage.Text = ltlOldPassInvalid.Text;
                return;
            }

            if (!rfvNewPassword.IsValid && 
                !rfvConfirmNewPassword.IsValid && 
                !rfvSecurityQuestion.IsValid && 
                !rfvSecurityAnswer.IsValid)
            {
                sysMessage.Text = ltlFillPasswordOrQuestion.Text;
                return;
            }

            if (provider.RequiresQuestionAndAnswer)
            {
                if (rfvSecurityQuestion.IsValid && rfvSecurityAnswer.IsValid)
                {
                    if (!provider.ChangePasswordQuestionAndAnswer(this.UserID, txtOldPassword.Text, txtQuestion.Text, txtAnswer.Text))
                    {
                        DisplayErrors(provider.Errors, provider);
                        return;
                    }
                }
                else if (rfvSecurityQuestion.IsValid || rfvSecurityAnswer.IsValid)
                {
                    sysMessage.Text = ltlSecurityQuestionAnswer.Text;
                    return;
                }
            }            

            if (rfvNewPassword.IsValid && rfvConfirmNewPassword.IsValid)
            {
                if (!txtNewPassword.Text.Equals(txtConfirmNewPassword.Text))
                {
                    sysMessage.Text = ltlPasswordMatch.Text;
                    return;
                }
                
                int loopCount = 1;
                foreach (string pass in u.UsedPasswords)
                {
                    //only look at a the number of last passwords in config setting in case there are more in the db
                    if (loopCount++ > this.Config.NPMembershipProvider.PasswordHistoryCount) break;
                    if (Encryption.VerifyHash(txtNewPassword.Text, "SHA256", pass))
                    {
                        sysMessage.Text = String.Format(ltlPasswordUsed.Text, this.Config.NPMembershipProvider.PasswordHistoryCount.ToString());
                        return;
                    }
                }
                if (!provider.ChangePassword(u.UserID, txtOldPassword.Text, txtNewPassword.Text))
                {
                    DisplayErrors(provider.Errors, provider);
                    return;
                }
                else
                {
                    //password changed, reset password expiration
                    TheSession.PasswordExpired = false;
                }
            }
            else if (rfvNewPassword.IsValid || rfvConfirmNewPassword.IsValid)
            {
                sysMessage.Text = ltlPasswordMatch.Text;
                return;
            }

            Response.Redirect("~/common/accounts/myaccount.aspx", true);
        }

        private void DisplayErrors(List<NPError> theErrors, NPMembershipProvider provider)
        {
            string separator = "<br />";
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.USER_LoginNameExists:
                        sysMessage.Text += hdnLoginNameMustBeUnique.Text + separator;
                        break;
                    case NPErrorType.USER_LoginFailedIncorrectPassword:
                        sysMessage.Text += ltlOldPassInvalid.Text + separator;
                        break;
                    case NPErrorType.USER_PasswordNotMeetingMinLength:
                        sysMessage.Text += String.Format(ltlPasswordLength.Text, provider.MinRequiredPasswordLength) + separator;
                        break;
                    case NPErrorType.USER_PasswordNotMeetingMinNonAlphanumericCharacters:
                        sysMessage.Text += String.Format(ltlPasswordNonAlphanumericCharacters.Text, provider.MinRequiredNonAlphanumericCharacters) + separator;
                        break;
                    case NPErrorType.USER_PasswordNotMeetingRegExp:
                        sysMessage.Text += ltlPasswordRegularExpression.Text + separator;
                        break;
                    case NPErrorType.USER_EmailExists:
                        sysMessage.Text += ltlEmailExists.Text + separator;
                        break;
                    default:
                        throw npe;
                }
            }
        }
    }
}
