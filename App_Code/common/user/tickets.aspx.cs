using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.support;
using netpoint.api.framework;

namespace netpoint.common.user
{
    /// <summary>
    /// Summary description for equipment.
    /// </summary>
    public partial class tickets : netpoint.classes.NPBasePage
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            if (!IsPostBack)
            {
                lblUserID.Text = UserID;
                BindGrid();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void BindGrid()
        {
            NPQuery q = new NPQuery(typeof(NPSupportTicket), Connection);
            q[0].AddWhere(Comparison.Equal, "SubmitUserID", UserID);
            if (ViewState["sortex"] != null)
            {
                q.AddOrderBy(0, Convert.ToString(ViewState["sortex"]), (OrderDirection)Enum.Parse(typeof(OrderDirection), Convert.ToString(ViewState["sortdir"])));
            }
            TicketsListGrid.DataSource = q.ExecuteObjects();
            TicketsListGrid.DataBind();
        }

        public void TicketsListGrid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            Literal ltlSubject = (Literal)e.Row.FindControl("ltlSubject");
            Literal ltlDate = (Literal)e.Row.FindControl("ltlDate");
            NPSupportTicket st = (NPSupportTicket)e.Row.DataItem;
            if (ltlSubject != null)
            {
                ltlSubject.Text = st.Subject;
                ltlDate.Text = st.SubmitDate.ToString("yyyy-MMM-dd");
            }
        }

        protected void TicketsListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TicketsListGrid.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void TicketsListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["sortex"] != null && e.SortExpression.Equals(Convert.ToString(ViewState["sortex"])))
            {
                ViewState["sortdir"] = Convert.ToString(ViewState["sortdir"]).Equals("ASC") ? "DESC" : "ASC";
            }
            else
            {
                ViewState["sortex"] = e.SortExpression;
                ViewState["sortdir"] = "ASC";
            }
            BindGrid();
        }
    }
}
