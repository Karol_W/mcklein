using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api;
using netpoint.api.common;
using netpoint.classes;
using netpoint.api.prospecting;

namespace netpoint.common.user
{
    /// <summary>
    /// Summary description for salesrep.
    /// </summary>
    public partial class salesrep : NPBasePage
    {
        protected System.Web.UI.WebControls.Literal ltlSettingsFor;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (base.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            if (!this.IsPostBack)
            {
                lblUserID.Text = UserID;

                NPAccount a = new NPAccount(AccountID);
                NPUser u = new NPUser(a.SalesManager);

                ltlRep.Text = "<b>" + u.GetFormattedName(NameFormat.FullName) + "</b><br>" +
                    u.JobTitle + ((u.JobTitle == "" || u.JobTitle == null) ? "" : "<br>") +
                    u.DayPhone;             

                if (u.ProfilePicture == null || u.ProfilePicture == "")
                {
                    imgRep.Visible = false;
                }
                else
                {
                    imgRep.ImageUrl = u.ProfilePicture;
                }               

                if (u.Initialized && u.Email != null && netpoint.api.utility.NPValidator.IsEmail(u.Email))
                {
                    lnkEmail.NavigateUrl = "mailto:" + u.Email;
                    lnkEmail.Visible = true;
                }
            }
        }       
    }
}
