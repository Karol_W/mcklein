using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.common;
using netpoint.api.utility;
using netpoint.classes;
using System.Resources;

namespace netpoint.common
{
    /// <summary>
    /// Summary description for pagedetail.
    /// </summary>
    public partial class pagedetail : NPBasePage
    {

        private NPTheme _theme;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            _theme = new NPTheme(base.ServerID, Connection);

            if (Request.QueryString["pagecode"] != null)
            {

                NPResourcesPages nrp = new NPResourcesPages(Request.QueryString["pagecode"]);

                if (nrp.PageID > 0)
                {
                    string sourcecode = string.Empty;
                    if (_theme.HomeUrl != "")
                    {
                        sourcecode = WebFunctions.ReplaceTags(nrp.PageSourceCode, _theme.HomeUrl, VirtualPath, AssetsPath, Page, UserID, AccountID, ServerID);
                    }
                    else
                    {
                        sourcecode = WebFunctions.ReplaceTags(nrp.PageSourceCode, Page.Request.Url.Host, VirtualPath, AssetsPath, Page, UserID, AccountID, ServerID);
                    }
                    Regex s_regexPlugin = new Regex(@"\{\{plugin:(.+?)\}\}", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                    Control grp = null;
                    int lastEnd = 0;

                    foreach (Match m in s_regexPlugin.Matches(sourcecode))
                    {
                        if (grp == null)
                            grp = new Control();

                        if (lastEnd < m.Index)
                            grp.Controls.Add(new LiteralControl(sourcecode.Substring(lastEnd, m.Index - lastEnd)));

                        grp.Controls.Add(GetPlugin(m.Groups[1].Value.Trim()));

                        lastEnd = m.Index + m.Length;
                    }
                    if (grp != null)
                    {
                        if (lastEnd < sourcecode.Length)
                            grp.Controls.Add(new LiteralControl(sourcecode.Substring(lastEnd)));

                        sysPageText.Controls.Add(grp);
                    }
                    else
                    {
                        sysPageText.Controls.Add(new LiteralControl(sourcecode));
                    }
                    
                    sysDateModifiedLabel.Text = nrp.DateModified.ToString("dd-MMM-yyyy HH:mm");
                    sysViewsLabel.Text = nrp.Views.ToString();
                    sysUserIDLabel.Text = nrp.UserID;
                    NPResourcesPages.PageView(Request.QueryString["pagecode"]);
                    NPStats.View(this.UserID, this.SessionID, "page", Request.QueryString["pagecode"], this.ConnectionString);
                    lnkEdit.NavigateUrl = "~/admin/common/pages/pageeditor.aspx?PageID=" + nrp.PageID;
                    lnkEdit.ImageUrl = ResolveImage("icons", "edit.gif");
                    if (UserID == nrp.UserID)
                    {
                        PageAdminPanel.Visible = true;
                    }
                }
                else
                {                    
                    sysPageText.Controls.Add(new LiteralControl(errMissingPage.Text + " " + Server.HtmlEncode(Request.QueryString["pagecode"])));
                }

            }
            if (PermissionController.UserInRole("superuser") || PermissionController.UserInRole("contentadmin"))
            {
                PageAdminPanel.Visible = true;
            }

            Page.DataBind();
        }

        private Control GetPlugin(string pluginName)
        {
            try
            {
                return Page.LoadControl("~/plugins/" + pluginName + ".ascx");
            }
            catch (Exception e)
            {
                if (Page.Trace.IsEnabled)
                {
                    Page.Trace.Write("PageDetail.aspx", "Failed to load plugin '" + pluginName + "'", e);
                }
                return new LiteralControl(pluginName);
            }
        }

        public string Description
        {
            get
            {
                NPResourcesPages nrp = new NPResourcesPages(Request.QueryString["pagecode"]);

                if (nrp.PageID > 0)
                {
                    if (nrp.PageBlurb != null && nrp.PageBlurb.Length > 0)
                    {
                        return nrp.PageBlurb;
                    }
                }
                return string.Empty;
            }
        }

        public string Keywords
        {
            get
            {
                NPResourcesPages nrp = new NPResourcesPages(Request.QueryString["pagecode"]);

                if (nrp.Keywords != null && nrp.Keywords.Length > 0)
                {
                    return nrp.Keywords;
                }
                return string.Empty;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            ((NPBasePage)Page).IgnoreLoginRequired = Convert.ToBoolean(sysIgnoreLoginRequired.Text);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

    }
}
