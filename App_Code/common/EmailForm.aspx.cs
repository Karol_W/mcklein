﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.common;
using netpoint.api.catalog;
using netpoint.api.utility;
using netpoint.api.support;
using netpoint.classes;
using System.Resources;
using System.Collections.Generic;
using netpoint.api;
using System.IO;
using netpoint.api.data;

using McKlein.ServiceCall;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Mail;

namespace netpoint.common
{
    public partial class EmailForm : NPBasePage
    {

        private NPBasePage _bp;

        protected global::System.Web.UI.WebControls.Panel requiredLogin;
        protected global::System.Web.UI.WebControls.Panel contactForm;
        protected global::System.Web.UI.WebControls.HyperLink loginLink;

        protected global::System.Web.UI.WebControls.Literal sysEmailFrom;
        protected global::System.Web.UI.WebControls.Literal sysEmailBody;
        protected global::System.Web.UI.WebControls.Literal sysEmailAddress;
        protected global::System.Web.UI.WebControls.Literal sysEmailSubject;
        protected global::System.Web.UI.WebControls.Literal sysEmailTemplate;
        protected global::System.Web.UI.WebControls.Label sysSentMessage;
        protected global::System.Web.UI.WebControls.Button btnSendmail;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            
            if (_bp.UserID != "")
            {
                requiredLogin.Visible = false;
            }
            else
            {
                contactForm.Visible = false;
                loginLink.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=~" + HttpContext.Current.Request.Url.AbsolutePath;
            }

        }
        protected void ItemNumberValidate(object source, ServerValidateEventArgs args)
        {
            NPPart part = new NPPart(args.Value);

            args.IsValid = part.PartExists();
        }
        protected void btnSendmail_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                NPSupportWorkOrder newTicket = new NPSupportWorkOrder();
                NPUser user = new NPUser(_bp.UserID);
                //newTicket.SupportWorkOrderID
                newTicket.WorkName = "Repair Request";
                newTicket.Description = this.ProcessFields();
                newTicket.AccountID = user.AccountID;
                newTicket.ContactUserID = user.UserID;
                newTicket.Status = "Open";
                newTicket.Priority = "low";
                newTicket.TypeA = "Web";
                newTicket.TypeB = NPCode.getCodeIDByValue("Defective", "worktypeb", Connection);
                newTicket.TypeC = _bp.Request.Form["ctl00$mainslot$eml_category2"].ToString(); 
                newTicket.AssignUserID = "manager";
                newTicket.CreateDate = DateTime.Now;
                
                newTicket.PartNo = _bp.Request.Form["ctl00$mainslot$eml_ItemNumber"].ToString();
                //newTicket.SerialNumber
                //newTicket.Part
                List<NPError> errors = new List<NPError>();
                bool valid = newTicket.Valid(errors);
                if (!valid)
                {
                    foreach (NPError error in errors)
                    {
                        Response.Write(error.ToString());
                        Response.Write("<br/>");
                    }
                }
                else
                {
                    newTicket.Save();
                    object result_tmp = DataFunctions.ExecuteScalar("SELECT TOP 1 RmaNo FROM SupportWorkOrderRMA ORDER BY RmaNO DESC", _bp.ConnectionString);
                    int result = (result_tmp == null) ? 10001 : Math.Max((int) result_tmp, 10001);
                    ServiceCallRMA rma = new ServiceCallRMA(newTicket.SupportWorkOrderID);
                    rma.RMANo = result + 1;
                    rma.Save();
                    ServiceCallImages images = new ServiceCallImages(newTicket.SupportWorkOrderID);
                    JArray array = JArray.Parse(_bp.Request.Form["ctl00$mainslot$UploadedFiles"].ToString());
                    int i = 0;
                    foreach(JObject obj in array.Children()){
                        i++;
                        JEnumerable<JProperty> props = obj.Children<JProperty>();
                        if (i == 1)
                        {
                            foreach(JProperty val in props)
                            {
                                if(val.Name == "url")
                                {
                                    images.Image_1_Url = val.Value.ToString();
                                } else if (val.Name == "name")
                                {
                                    images.Image_1 = val.Value.ToString();
                                }
                            }
                        } else if (i == 2)
                        {
                            foreach (JProperty val in props)
                            {
                                if (val.Name == "url")
                                {
                                    images.Image_2_Url = val.Value.ToString();
                                }
                                else if (val.Name == "name")
                                {
                                    images.Image_2 = val.Value.ToString();
                                }
                            }
                        } else if (i == 3)
                        {
                            foreach (JProperty val in props)
                            {
                                if (val.Name == "url")
                                {
                                    images.Image_3_Url = val.Value.ToString();
                                }
                                else if (val.Name == "name")
                                {
                                    images.Image_3 = val.Value.ToString();
                                }
                            }
                        }
                    }
                    images.Save();
                    
                    string customerEmail = user.Email;
                    string csEmail = sysEmailAddress.Text;
                    string subject = sysEmailSubject.Text + " " + rma.RMANo;

                    StringBuilder message = new StringBuilder();
                    message.AppendFormat("First Name: {0}<br/><br/>", user.FirstName);
                    message.AppendFormat("Last Name: {0}<br/><br/>", user.LastName);
                    message.AppendFormat("Email: {0}<br/><br/>", user.Email);
                    message.AppendFormat("RMA Number: {0}<br/><br/>", rma.RMANo);
                    message.Append(ProcessFields().Replace("\r\n", "<br/>"));
                    string url = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
(Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + Page.ResolveUrl(String.Format("~/common/sendRMACart.aspx?rmaNo={0}&customer={1}", rma.RMANo, user.AccountID));
                    message.AppendFormat("Send Link: <a href=\"{0}\">{0}</a><br/><br/>", url);

                    if (message.Length != 0 && csEmail != "")
                    {
                        NPCommunicationLog mail = new NPCommunicationLog();
                        mail.UserID = UserID;
                        mail.AccountID = AccountID;
                        mail.LogType = "email";
                        mail.TimeStamp = DateTime.Now;
                        mail.Incoming = false;
                        mail.TemplateCode = "general";

                        mail.EmailFrom = customerEmail;
                        mail.Email = csEmail;
                        mail.Subject = subject;
                        mail.Message = message.ToString();

                        mail.Save();

                        sysSentMessage.Visible = true;
                    }
                    String customerSubject = "McKlein Company Repair Request RMA #" + rma.RMANo;
                    String customerMessage = sysEmailBody.Text;
                    String sendMessage = String.Format(customerMessage, user.GetFormattedName(NameFormat.FullName), rma.RMANo, csEmail);
                    if (sendMessage.Length != 0 && customerEmail != "")
                    {
                        NPCommunicationLog mail = new NPCommunicationLog();
                        mail.UserID = UserID;
                        mail.AccountID = AccountID;
                        mail.LogType = "email";
                        mail.TimeStamp = DateTime.Now;
                        mail.Incoming = false;
                        mail.TemplateCode = "general";

                        mail.EmailFrom = csEmail;
                        mail.Email = customerEmail;
                        mail.Subject = customerSubject;
                        mail.Message = sendMessage.ToString();

                        mail.Save();

                        sysSentMessage.Visible = true;
                    }

                }

            }
        }
        private string ProcessFields()
        {
            string msg = "";

            //get send button id to build other dynamic ids
            string sendId = btnSendmail.UniqueID;
            //strip off last control
            sendId = sendId.Substring(0, sendId.LastIndexOf("$"));
            //now append starting id for our email fields
            sendId = sendId + "$eml_";
            int startLen = sendId.Length;
            string inputName;
            Label inputLabel;
            foreach (string key in _bp.Request.Form.Keys)
            {
                if (key.StartsWith(sendId))
                {
                    inputName = key.Substring(startLen);
                    inputLabel = (Label)FindControl(key.Replace("$eml_", "$lbl_"));
                    if (inputLabel != null)
                    {
                        inputName = inputLabel.Text;
                    }
                    msg = msg + inputName + " " + _bp.Request.Form[key].ToString() + "\r\n\r\n";
                }
            }

            return msg;
        }

    }
}
