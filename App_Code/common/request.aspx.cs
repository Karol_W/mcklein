using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.classes;

namespace netpoint.common
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class request : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            ((NPBasePage)Page).IgnoreLoginRequired = Convert.ToBoolean(sysIgnoreLoginRequired.Text);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
