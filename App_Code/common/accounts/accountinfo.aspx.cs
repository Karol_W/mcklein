using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.classes;
using netpoint.common.controls;
using System.Resources;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for login.
    /// </summary>
    public partial class accountinfo : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (this.AccountID == null || this.AccountID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            NPAccount a = new NPAccount(this.AccountID);
            sysAccountID.Text = a.AccountName;
            acctInfo.Account = a;
        }
    }
}
