﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;

namespace netpoint.common.accounts
{
    public partial class purchaseditems : NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            lblUserID.Text = UserID;
        }
    }
}
