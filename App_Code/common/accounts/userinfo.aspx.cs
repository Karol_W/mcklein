using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.common;
using netpoint.classes;
using netpoint.api.account;
using netpoint.api;
using netpoint.api.data;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for login.
    /// </summary>
    public partial class userinfo : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NPResource resource = null;
                if (Request.QueryString["ResourceID"] != null)
                {
                    resource = new NPResource(Request.QueryString["ResourceID"]);
                }
                else
                {
                    resource = new NPResource();
                }
                PageView.Text = resource.Name;
                lblUserID.Text = UserID;
                npuserresource.UserID = UserID;
                npuserresource.ResourceID = resource.ResourceID;
                npuserresource.UserResourceID = 0;
                LoadData(resource.ResourceID);
            }
        }

        private void LoadData(string urid)
        {

            NPResource r = new NPResource(urid);

            //	dimension 1
            gridView.Columns[0].HeaderText = r.Dimension1;
            gridView.Columns[0].Visible = !r.IsURL[0].Equals('Y');
            gridView.Columns[1].Visible = r.IsURL[0].Equals('Y');
            //	dimension 2
            gridView.Columns[2].HeaderText = r.Dimension2;
            gridView.Columns[3].HeaderText = r.Dimension2;
            gridView.Columns[2].Visible = !r.IsURL[1].Equals('Y');
            gridView.Columns[3].Visible = r.IsURL[1].Equals('Y');
            if (r.Dimension2 == null || r.Dimension2 == "")
            {
                gridView.Columns[2].Visible = false;
                gridView.Columns[3].Visible = false;
            }
            //	dimension 3
            gridView.Columns[4].HeaderText = r.Dimension3;
            gridView.Columns[5].HeaderText = r.Dimension3;
            gridView.Columns[4].Visible = !r.IsURL[2].Equals('Y');
            gridView.Columns[5].Visible = r.IsURL[2].Equals('Y');
            if (r.Dimension3 == null || r.Dimension3 == "")
            {
                gridView.Columns[4].Visible = false;
                gridView.Columns[5].Visible = false;
            }

            DataSearchParameters dsp = new DataSearchParameters();
            dsp.Add(new DataSearchParameter("UserID", DataFilterType.Equal, this.UserID));
            dsp.Add(new DataSearchParameter("ResourceID", DataFilterType.Equal, urid));
            ArrayList theList = NPUserResource.Search(dsp, "", "");
            gridView.DataSource = theList;
            gridView.DataBind();

            btnAdd.Visible = true;
            npuserresource.Visible = true;
            if (!r.AllowMultiple && theList.Count > 0)
            {
                btnAdd.Visible = false;
                npuserresource.Visible = false;
            }
        }

        private void LoadDetail(int urid)
        {
            npuserresource.Visible = true;
            npuserresource.UserID = this.UserID;
            npuserresource.ResourceID = Request.QueryString["ResourceID"];
            npuserresource.UserResourceID = urid;
            npuserresource.LoadControl();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (((CommandEventArgs)args).CommandName == "John the Blob")
            {
                LoadData(Request.QueryString["ResourceID"]);
            }
            return base.OnBubbleEvent(source, args);
        }

        protected void btnAdd_Click(object sender, ImageClickEventArgs e)
        {
            LoadDetail(0);
        }

        protected void gridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton btn0 = (ImageButton)e.Row.FindControl("btnDelete");
            ImageButton btn2 = (ImageButton)e.Row.FindControl("btnEdit");

            if (btn0 != null)
            {
                btn0.ImageUrl = this.ResolveImage("icons", "delete.gif");
            }
            if (btn2 != null)
            {
                btn2.ImageUrl = this.ResolveImage("icons", "edit.gif");
            }
        }

        protected void gridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("del"))
            {
                NPUserResource ur = new NPUserResource(Convert.ToInt32(e.CommandArgument), this.Connection);
                ur.Delete();
                LoadData(Request.QueryString["ResourceID"]);
            }
            else if (e.CommandName.Equals("edi"))
            {
                LoadDetail(Convert.ToInt32(e.CommandArgument));
            }
        }
    }
}
