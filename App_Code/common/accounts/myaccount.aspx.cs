using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.common;
using netpoint.classes;
using netpoint.api.account;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for login.
    /// </summary>
    public partial class myaccount : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                imgLogout.ImageUrl = ResolveImage("buttons", "logoff.gif");
            }

            if (UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl));
            }
            else
            {
                NPUser u = new NPUser(UserID);
                lblUserID.Text = u.LoginName;
                
                ResourcesUserRepeater.DataSource = NPResource.ResourceList("U", false); //-s i think this is right... changed it in the api upgrade
                ResourcesUserRepeater.DataBind();

                pnlAdmin.Visible = PermissionController.GetPermission("control", "netpoint.common.accounts.myaccount.pnlAdmin", PermissionType.Read);
                //EventsPanel.Visible = bp.PermissionController.GetPermission("control", "eventspanel", PermissionType.Read);

                NPAccount a = new NPAccount(u.AccountID);
                if (!a.AccountHasSpecialPricing())
                {
                    pnlSpecialPricing.Visible = false;
                }
            }

        }

        public void Logout(object sender, CommandEventArgs e)
        {            
            base.Logout();
            Response.Redirect("~/common/accounts/login.aspx");
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            

        }
        #endregion
    }
}
