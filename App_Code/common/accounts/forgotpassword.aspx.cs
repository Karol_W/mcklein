using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.common;
using netpoint.api.encryption;
using netpoint.api.utility;
using netpoint.classes;
using netpoint.providers;

namespace netpoint.common.accounts
{

    public partial class forgotpassword : NPBasePage
    {
        public string locate;
        public bool RequiresQA
        {
            get { return ViewState["RequiresQA"] == null ? false : Convert.ToBoolean(ViewState["RequiresQA"]); }
            set { ViewState["RequiresQA"] = value; }
        }        

        override protected void OnInit(EventArgs e)
        {
            IgnoreLoginRequired = true;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
            locate = HttpContext.Current.Request.Url.AbsoluteUri;
            if(locate.Contains("siamod")){
                locate="Siamod";
            }
            else if(locate.Contains("parinda")){
                locate="Parinda";
            }
            else {
                locate="mcklein";
            }
            Control mycontrol = FindControl("ctl00_mainslot_btnSubmit");
            
            if (!IsPostBack)
            {
                RequiresQA = NPMembershipProvider.GetRequiresQuestionAndAnswer();
            }
        }

        protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            //pass connection so constructor get user by loginname instead of userid
            NPUser u = new NPUser(txtUserName.Text, Connection);
            if (!u.Initialized)
            {
                trErr.Visible = true;
                return;
            }

            if (RequiresQA && !trQA.Visible)
            {
                txtUserName.Enabled = false;
                if (u.PasswordQuestion == null || u.PasswordQuestion.Length == 0)
                {
                    trErr.Visible = true;
                    btnSubmit.Visible = false;
                    return;
                }
                ltlQuestion.Text = u.PasswordQuestion;
                trQA.Visible = true;
                return;
            }

            if ((!RequiresQA || (RequiresQA && Encryption.VerifyHash(txtAnswer.Text, "SHA256", u.PasswordAnswer)) && NPValidator.IsEmail(u.Email)))
            {                                
                string newPassword = StringFunctions.CreateRandomPassword();
                u.SetPassword(newPassword);
                u.LastPasswordChangeDate = DateTime.MinValue;
                u.LastChangeDate = DateTime.Now;
                u.Save(); 

                NPCommunicationLog cl = new NPCommunicationLog();
                cl.Subject = "Forgot Password";
                cl.Email = u.Email;
                cl.Incoming = false;
                cl.LogStatus = "C";
                cl.LogType = "email";
                cl.TimeStamp = DateTime.Now;
                cl.AccountID = u.AccountID;
                cl.UserID = u.UserID;
                cl.OwnerID = u.UserID;
                cl.TemplateCode = "forgotpassword";
                cl.MergeObjectType = "user";
                cl.MergeObjectID = u.UserID;
                cl.ServerID = ServerID;
                cl.Save();

                //encrypt password into message field
                Encryption enc = new Encryption(cl.LogID.ToString());
                cl.Message = enc.Encrypt(newPassword);
                cl.LogStatus = "O";
                cl.Save();

                tblRetrieve.Visible = false;
                lblConfirm.Visible = true;
            }
            else
            {
                trErr.Visible = true;
            }
        }
    }
}
