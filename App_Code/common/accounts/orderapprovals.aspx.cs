using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.commerce;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for orderhistory.
    /// </summary>
    public partial class orderapprovals : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            EnableMultiCurrency = false;

            if (UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            lblAccountID.Text = AccountID;
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void BindGrid()
        {
            DataSet ds = new DataSet();
            NPUser u = new NPUser(UserID);
            NPAccount a = new NPAccount(u.AccountID);
            if (base.UserID != a.MainContactID)
            {
                OrdersListGrid.Columns[9].Visible = false;
            }
            ds = OrderReports.GetOrders(UserID, u.AccountID, "H", ConnectionString);
            OrdersListGrid.DataSource = ds;
            OrdersListGrid.DataMember = "DataTable";
            OrdersListGrid.DataBind();
        }

        protected void OrdersListGrid_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            Literal ltlShippingAddressName = (Literal)e.Row.FindControl("ltlShippingAddressName");
            Literal ltlStatus = (Literal)e.Row.FindControl("ltlShipStatus");

            if (ltlShippingAddressName != null)
            {
                int index = drv["ShippingAddress"].ToString().IndexOf("\n");
                ltlShippingAddressName.Text = (index > -1 ? drv["ShippingAddress"].ToString().Substring(0, index) : drv["ShippingAddress"].ToString());
            }
            if (ltlStatus != null)
            {
                if (drv["ShippingStatus"] != null)
                {
                    if (drv["ShippingStatus"].ToString() == "Shipped")
                    {
                        string controlname = "hdn" + drv["ShippingStatus"].ToString();
                        ltlStatus.Text = ((Literal)Master.FindControl("hiddenslot").FindControl(controlname)).Text;
                    }
                    else if (
                        drv["ShippingStatus"].ToString() == "Pending" ||
                        drv["ShippingStatus"].ToString() == "Picked" ||
                        drv["ShippingStatus"].ToString() == "Partial")
                    {
                        string controlname = "hdn" + drv["ShippingStatus"].ToString();
                        ltlStatus.Text = ((Literal)Master.FindControl("hiddenslot").FindControl(controlname)).Text;
                    }
                }
            }
        }
    }
}
