using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.classes;
using netpoint.api.data;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for settheme.
    /// </summary>
    public partial class settheme : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            string SQL = "SELECT ServerID FROM Users WHERE UserID = '" + UserID + "'";
            object result = DataFunctions.ExecuteScalar(SQL, ConnectionString);
            if (result != null && Convert.ToString(result).Length > 0)
            {
                Response.Redirect("myaccount.aspx?serverid=" + Convert.ToString(result), true);
            }
            else
            {
                Response.Redirect("myaccount.aspx", true);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
