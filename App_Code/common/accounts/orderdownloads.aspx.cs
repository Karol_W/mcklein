using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for orderhistory.
    /// </summary>
    public partial class orderdownloads : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (base.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            lblUserID.Text = UserID;
            string partno = Request["PartNo"];
            dlDownloads.DataSource = NPPartDownload.GetAuthorizedDownloads(UserID, partno, ConnectionString);
            dlDownloads.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void dlDownloads_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            NPPartDownload pd = (NPPartDownload)e.Item.DataItem;
            if (pd != null)
            {
                Label lblFileDescription = (Label)e.Item.FindControl("lblFileDescription");
                Label lblVersion = (Label)e.Item.FindControl("lblVersion");
                Label lblFileSize = (Label)e.Item.FindControl("lblFileSize");
                HyperLink lnkFileName = (HyperLink)e.Item.FindControl("lnkFileName");
                HyperLink lnkFileImage = (HyperLink)e.Item.FindControl("lnkFileImage");

                lblFileDescription.Text = pd.Description;
                lnkFileName.Text = pd.DownloadName;
                lnkFileName.NavigateUrl = "~/common/controls/partdownload.aspx?pdid=" + pd.DownloadID;
                lnkFileImage.NavigateUrl = "~/common/controls/partdownload.aspx?pdid=" + pd.DownloadID;
                lblVersion.Text = pd.Version;
                try
                {
                    FileInfo fi = new FileInfo(Server.MapPath("~/assets/" + pd.Location));
                    lblFileSize.Text = (fi.Length / 1024) + " KB";
                }
                catch (System.IO.IOException)
                {
                    lblFileSize.Text = "IO Error";
                }
            }
        }
    }
}
