using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.support;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for accountsupport.
    /// </summary>
    public partial class accountsupport : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            int contractID = 0;

            if (!this.IsPostBack)
            {
                lnkBack.ImageUrl = ResolveImage("icons", "cancel.gif");
                try
                {
                    if (Request.QueryString["contractid"] != null && Request.QueryString["contractID"].ToString() != "")
                    {
                        contractID = int.Parse(Request.QueryString["contractid"].ToString());
                        lnkBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
                    }
                }
                catch
                {
                    lnkBack.NavigateUrl = "accountinfo.aspx?type=regparts";
                }

                BindData(contractID);
            }
        }

        private void BindData(int contractID)
        {
            NPSupportContract sc = new NPSupportContract(contractID);

            ltlContractCode.Text = sc.ContractCode;
            ltlStartDate.Text = sc.StartDate.ToShortDateString();
            ltlEndDate.Text = sc.EndDate.ToShortDateString();
            ltlDescription.Text = sc.Description;

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
