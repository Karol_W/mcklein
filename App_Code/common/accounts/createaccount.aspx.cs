using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using netpoint.api.account;
using netpoint.classes;
using netpoint.providers;
using netpoint.api.common;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for login.
    /// </summary>
    public partial class createaccount : NPBasePage
    {
        NPUser u;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            base.OnInit(e);

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
            Response.Cache.SetMaxAge(TimeSpan.FromMilliseconds(0));
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(System.Web.HttpCacheRevalidation.AllCaches);
            Response.Cache.SetLastModified(DateTime.Now);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CreateUserWizard1.InvalidPasswordErrorMessage = this.hdnPasswordFaiureText.Text;
                CreateUserWizard1.DuplicateEmailErrorMessage = this.hdnDuplicateEmailText.Text;
                CreateUserWizard1.UnknownErrorMessage = this.hdnInvalidUsernameText.Text;
            }
            base.OnLoad(e);
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            string test = CreateUserWizard1.Answer;
        }
        #endregion

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            u = new NPUser(CreateUserWizard1.UserName, Connection);
            CreateUserWizardStep createStep = (CreateUserWizardStep)CreateUserWizard1.WizardSteps[0];
            u.Title = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtTitle")).Text;
            u.FirstName = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtFirstName")).Text;
            u.MiddleName = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtMiddleName")).Text;
            u.LastName = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtLastName")).Text;
            u.Suffix = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtSuffix")).Text;
            u.AddDate = DateTime.Now;
            u.LastPasswordChangeDate = DateTime.Now;
            u.Save();

            NPAccount acct = new NPAccount(u.AccountID);
            acct.TaxID = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtTaxId")).Text;
            string compName = ((TextBox)createStep.ContentTemplateContainer.FindControl("txtCompanyName")).Text;
            if (compName != "")
            {
                acct.AccountName = compName;
            }
            acct.Save();
            SendNewAccountMessage();
            string currPassword = ((TextBox)createStep.ContentTemplateContainer.FindControl("Password")).Text;
            this.Login(u.LoginName, currPassword, ((CheckBox)createStep.ContentTemplateContainer.FindControl("RememberMe")).Checked);
        }

        private void SendNewAccountMessage()
        {
            if (u != null && u.LoginName != string.Empty)
            {
                NPCommunicationLog cl = new NPCommunicationLog();
                cl.Subject = "User Created";
                cl.Email = u.Email;
                cl.Incoming = false;
                cl.LogStatus = "O";
                cl.LogType = "email";
                cl.TimeStamp = DateTime.Now;
                cl.AccountID = u.AccountID;
                cl.UserID = u.UserID;
                cl.OwnerID = u.UserID;
                cl.TemplateCode = "newuser";
                cl.MergeObjectType = "user";
                cl.MergeObjectID = u.UserID;
                cl.Save();
            }
        }
    }
}
