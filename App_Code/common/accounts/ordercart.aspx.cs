using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.prospecting;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.api.account;
using netpoint.api;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for orderhistory.
    /// </summary>
    public partial class ordercart : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            EnableMultiCurrency = false;

            if (base.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            if (Request["orderid"] != null)
            {
                Session["orderid"] = Request["orderid"];
            }
            if (Session["orderid"] != null)
            {
                int orderid = Convert.ToInt32(Session["orderid"]);
                NPOrder _order = new NPOrder(orderid);
                npcdlblock.Order = _order;
                lblOrderID.Text = orderid.ToString();

                expenses.LoadExpenses(_order);

                if (UserID != _order.Account.MainContactID)
                {
                    pnlApproverTools.Visible = false;
                }
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgApprove.Click += new System.Web.UI.ImageClickEventHandler(this.imgApprove_Click);
            this.imgReject.Click += new System.Web.UI.ImageClickEventHandler(this.imgReject_Click);

        }
        #endregion

        protected void lnkApprove_Click(object sender, System.EventArgs e)
        {
            int orderid = Convert.ToInt32(Session["orderid"]);
            NPOrder _order = new NPOrder(orderid);
            _order.CheckOut(Request.UserHostAddress, ServerID);
            //
            NPUser owner = new NPUser(_order.UserID);
            NPUser approver = new NPUser(UserID);
            NPCommunicationLog cl = new NPCommunicationLog();
            cl.Email = owner.Email;
            cl.EmailFrom = approver.Email;
            cl.Incoming = false;
            cl.LogStatus = "O";
            cl.LogType = "email";
            cl.TimeStamp = DateTime.Now;
            cl.AccountID = _order.AccountID;
            cl.UserID = _order.UserID;
            cl.OwnerID = _order.UserID;
            cl.TemplateCode = "orderapproved";
            cl.MergeObjectType = "order";
            cl.MergeObjectID = _order.OrderID.ToString();
            cl.Message += txtMessage.Text;
            cl.Save();
            //
            Response.Redirect("orderapprovals.aspx");
        }

        protected void lnkReject_Click(object sender, System.EventArgs e)
        {
            int orderid = Convert.ToInt32(Session["orderid"]);
            NPOrder _order = new NPOrder(orderid);
            _order.CartTypeEnum = CartType.Cancelled;
            _order.Save();
            //
            NPUser owner = new NPUser(_order.UserID);
            NPUser approver = new NPUser(UserID);
            NPCommunicationLog cl = new NPCommunicationLog();
            cl.Email = owner.Email;
            cl.EmailFrom = approver.Email;
            cl.Incoming = false;
            cl.LogStatus = "O";
            cl.LogType = "email";
            cl.TimeStamp = DateTime.Now;
            cl.AccountID = _order.AccountID;
            cl.UserID = _order.UserID;
            cl.OwnerID = _order.UserID;
            cl.TemplateCode = "orderrejected";
            cl.MergeObjectType = "order";
            cl.MergeObjectID = _order.OrderID.ToString();
            cl.Message += txtMessage.Text;
            cl.Save();
            //
            Response.Redirect("orderapprovals.aspx");
        }

        private void imgApprove_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            lnkApprove_Click(sender, e);
        }

        private void imgReject_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            lnkReject_Click(sender, e);
        }
    }
}
