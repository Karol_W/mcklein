using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.classes;
using netpoint.api.utility;
using netpoint.api.prospecting;
using System.Text;
using System.Configuration;
using System.Resources;
using netpoint.common.controls;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for EmailToFriend.
    /// </summary>
    public partial class EmailToFriend : NPBasePage
    {


        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (!this.IsPostBack)
            {
                BindData(int.Parse(Request.QueryString["orderid"].ToString()));
            }
        }

        private void BindData(int orderID)
        {

            gvEmails.DataSource = OrderReports.CartDetail(orderID, ProductThumbImage, ConnectionString);
            gvEmails.DataBind();

            NPUser npu = new NPUser(UserID);
            ltlEmailFrom.Text = npu.Email;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGo.Click += new System.Web.UI.ImageClickEventHandler(this.btnGo_Click);

        }
        #endregion

        private void btnGo_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (NPValidator.IsEmail(txtEmail.Text))
            {
                NPCommunicationLog cl = new NPCommunicationLog();
                cl.Subject = txtSubject.Text;
                cl.Email = txtEmail.Text;
                cl.EmailFrom = ltlEmailFrom.Text;
                cl.Message = StyleSheetLink;
                if (txtMessage.Text != "")
                    cl.Message += txtMessage.Text + "<br><br><br>";
                GridView gv = CreateNeGridView();
                Page.Controls.Add(gv);
                cl.Message += ControlToHTML(gv);
                Page.Controls.Remove(gv);
                cl.Incoming = false;
                cl.LogStatus = "O";
                cl.LogType = "email";
                cl.TimeStamp = DateTime.Now;
                cl.AccountID = AccountID;
                cl.UserID = UserID;
                cl.OwnerID = UserID;
                cl.TemplateCode = "emailtofriend";
                cl.Save();
                Response.Redirect("savedlists.aspx");
            }
            else
            {
                Response.Write("<script>alert('" + errInvalidEmail.Text + "');</script>");
                return;
            }
        }

        protected void gvEmails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            System.Web.UI.WebControls.Image i = (System.Web.UI.WebControls.Image)e.Row.FindControl("img");
            DataRowView drv = (DataRowView)e.Row.DataItem;
            Literal ltl = (Literal)e.Row.FindControl("ltlDesc");
            PriceDisplay prcTotal = (PriceDisplay)e.Row.FindControl("prcTotal");

            if (i != null)
            {
                i.ImageUrl = "http://" + this.Request.Url.Host + drv["PartThumb"].ToString();
            }
            if (ltl != null)
            {
                HyperLink lnk = (System.Web.UI.WebControls.HyperLink)e.Row.FindControl("lnkPart");
                lnk.Text = drv.Row.ItemArray[6].ToString();
                lnk.NavigateUrl = "http://" + this.Request.Url.Host + VirtualPath +
                    "catalog/partdetail.aspx?PartNo=" + drv.Row.ItemArray[6].ToString();
            }
            if (prcTotal != null)
            {
                decimal quantity = Convert.ToDecimal(drv["Quantity"]);
                decimal purchasePrice = Convert.ToDecimal(drv["PurchasePrice"]);

                prcTotal.Price = quantity == 0 ? purchasePrice : purchasePrice * quantity;
            }
        }

        private const string dynamicGridViewID = "tmp_DynamicGridView";

        private GridView CreateNeGridView()
        {
            GridView gv = new GridView();
            gv.ID = dynamicGridViewID;
            foreach (DataControlField dcf in gvEmails.Columns)
            {
                gv.Columns.Add(dcf);
            }
            gv.AutoGenerateColumns = false;
            gv.Width = new Unit("100%");
            gv.CssClass = gvEmails.CssClass;
            gv.HeaderStyle.HorizontalAlign = gvEmails.HeaderStyle.HorizontalAlign;
            gv.HeaderStyle.CssClass = gvEmails.HeaderStyle.CssClass;
            gv.CellPadding = gvEmails.CellPadding;
            gv.RowDataBound += gvEmails_RowDataBound;
            gv.DataSource = OrderReports.CartDetail(int.Parse(Request.QueryString["orderid"].ToString()), ProductThumbImage, ConnectionString);
            gv.DataBind();
            return gv;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //To avoid exception when rendering the gridview control out of regular page lifecycle
             if (control.ID == dynamicGridViewID)
                 return;
             base.VerifyRenderingInServerForm(control);
        }

    }
}
