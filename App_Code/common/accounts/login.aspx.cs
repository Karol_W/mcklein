using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using netpoint.classes;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for login.
    /// </summary>
    public partial class login : netpoint.classes.NPBasePage
    {

        override protected void OnInit(EventArgs e)
        {
            IgnorePasswordExpired = true;
            base.OnInit(e);

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
            Response.Cache.SetMaxAge(TimeSpan.FromMilliseconds(0));
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(System.Web.HttpCacheRevalidation.AllCaches);
            Response.Cache.SetLastModified(DateTime.Now);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
			CreateAccountButton.ImageUrl = base.ResolveImage("buttons", "createaccount.gif");
			
            if (Request.QueryString["ReturnTo"] != null)
            {
                if (Request.QueryString["ReturnTo"].Equals("checkout"))
                {
                    sysMessage.Text = "<p>" + errLoginIn.Text + "</p>";
                    
                    if (this.TheTheme.ExpressCheckout)
                    {
                        pnlExpressCheckout.Visible = true;
                    }
                }
                else if (Request.QueryString["inactivityTimeout"] != null)
                {
                    sysMessage.Text = errTimeoutExpired.Text;
                }
                CreateAccountButton.NavigateUrl = "~/common/accounts/createaccount.aspx?ReturnTo=" + Request.QueryString["ReturnTo"];
            }
            if (DisableUserSelfCreation)
            {
                pnlCreateAccount.Visible = false;
            }
        }
    }
}
