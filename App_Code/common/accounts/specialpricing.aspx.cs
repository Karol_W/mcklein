﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using netpoint.classes;

namespace netpoint.common.accounts
{
    public partial class specialpricing : NPBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.UserID.Length == 0)
            {
                Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl), true);
            }
            lblUserID.Text = UserID;

            if (!IsPostBack)
            {
                specialPriceTree.PriceListCode = this.AccountID;
                specialPriceTree.TreeType = PriceTreeType.SpecialPrice;
            }
        }
    }
}
