using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.classes;
using netpoint.api.account;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for accountaddress.
    /// </summary>
    public partial class accountaddress : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                lblUserID.Text = UserID;
                if (base.UserID.Length == 0)
                {
                    Response.Redirect("~/common/accounts/login.aspx?ReturnTo=~" + Server.UrlEncode(Request.RawUrl));
                }
            }
            AccountAddresses.Account = new NPAccount(AccountID);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {

            if (args.GetType().Name == "NPEventArgs")
            {
                NPEventArgs eva = (NPEventArgs)args;
                if (((NPEventArgs)args).FromEvent == "AddressBlock_Edit")
                {
                    lnkMyAddresses.Visible = true;
                    sysGT.Visible = true;
                    lblMyAddress.Text = hdnEditAddress.Text;
                }
                else if (((NPEventArgs)args).FromEvent == "AddressBlock_Add")
                {
                    lnkMyAddresses.Visible = true;
                    sysGT.Visible = true;
                    lblMyAddress.Text = hdnAddAddress.Text;
                }
                else if (((NPEventArgs)args).FromEvent == "AddressBlock_Save")
                {
                    if (!DiplayErrors(eva.Data))
                    {
                        Response.Redirect("accountaddress.aspx");
                    }
                }
            }
            return base.OnBubbleEvent(source, args);
        }

        private bool DiplayErrors(object theErrs)
        {
            sysError.Text = "";
            List<NPError> theErrors = new List<NPError>();
            if (theErrors is List<NPError>)
            {
                theErrors = (List<NPError>)theErrs;
                foreach (NPError npe in theErrors)
                {
                    switch (npe.ErrorType)
                    {
                        case NPErrorType.ADDR_DuplicateNameShipping:
                            sysError.Text += hdnDuplicationAddressNameShipping.Text + "<br/>";
                            break;
                        case NPErrorType.ADDR_DuplicateNameBilling:
                            sysError.Text += hdnDuplicationAddressNameBilling.Text + "<br/>";
                            break;
                        case NPErrorType.ADDR_AddressNameIsRequired:
                            sysError.Text += hdnAddressNameRequired.Text + "<br/>";
                            break;
                        case NPErrorType.ADDR_CityIsRequired:
                            sysError.Text += hdnCityIsRequired.Text + "<br/>";
                            break;
                        case NPErrorType.ADDR_PostalCodeIsRequired:
                            sysError.Text += hdnPostalCodeRequired.Text + "<br/>";
                            break;
                        case NPErrorType.ADDR_Street1IsRequired:
                            sysError.Text += hdnStreet1Required.Text + "<br/>";
                            break;
                        default:
                            throw npe;
                    }
                }
            }

            return theErrors.Count > 0;
        }
    }
}
