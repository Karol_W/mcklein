using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.common;
using netpoint.api.account;
using netpoint.api;
using netpoint.api.support;

namespace netpoint.common.accounts
{
    /// <summary>
    /// Summary description for registerpart.
    /// </summary>
    public partial class registerpart : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            btnSubmit.ImageUrl = ResolveImage("buttons", "addinfo.gif");
            if (!this.IsPostBack)
            {
                sysHeader.Text = ltlProdReg.Text;
                try
                {
                    lnkBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
                    lnkBack2.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
                }
                catch
                {
                    lnkBack.NavigateUrl = "accountinfo.aspx?type=regparts";
                    lnkBack2.NavigateUrl = "accountinfo.aspx?type=regparts";
                }

                calPurchaseDate.SelectedDate = DateTime.Now;
                NPListBinder.BindManufacturers(ddlMNFCode, "", Catalog.CatalogID, Connection);
                NPListBinder.PopulateNPCode(ddlLocationPurchased, "purchaselocation", "", false);
                NPListBinder.BindPartsByManufacturer(ddlPart, ddlMNFCode.SelectedValue.ToString(), "", false);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmit_Click);

        }
        #endregion

        protected void ddlMNFCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            NPListBinder.BindPartsByManufacturer(ddlPart, ddlMNFCode.SelectedValue.ToString(), "", false);
        }

        private void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (calPurchaseDate.SelectedDate.Equals(DateTime.MinValue))
            {
                imgPurchaseDateWarning.Visible = true;
                return;
            }
            else
            {
                imgPurchaseDateWarning.Visible = false;
            }
            NPUserAccountPart ap = new NPUserAccountPart();
            ArrayList theContracts = new ArrayList();

            sysError.Text = "";

            ap.AccountID = AccountID;
            ap.DateRegistered = DateTime.Now;
            ap.PartNo = ddlPart.SelectedValue;
            ap.UserID = UserID;
            ap.RegistrationNumber = txtRegistrationNumber.Text;
            ap.DatePurchased = calPurchaseDate.SelectedDate;

            ap.LocationPurchased = ddlLocationPurchased.SelectedValue;
            if (txtPurchasePrice.Text == "")
            {
                txtPurchasePrice.Text = "0.00";
                ap.PurchasePrice = Convert.ToDecimal(txtPurchasePrice.Text);
            }
            else
            {
                try
                {
                    ap.PurchasePrice = Convert.ToDecimal(txtPurchasePrice.Text);
                }
                catch
                {
                    sysError.Text = errMustBeNumber.Text;
                    txtPurchasePrice.Text = "0";
                    return;
                }
            }

            //	if there is only one contract for this part, set the contract id
            theContracts = ap.FindSupportContracts(ContractSearchType.All);
            if (theContracts.Count == 1)
            {
                ap.SupportContractID = ((NPSupportContract)theContracts[0]).SupportContractID;
            }
            ap.Save();

            pnl.Visible = false;
            pnlThankYou.Visible = true;
        }
    }
}
