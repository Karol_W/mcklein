﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using netpoint.api.commerce;

namespace netpoint.catalog
{
    public partial class confirm : netpoint.classes.NPBasePageCommerce
    {
        public void checkCoupons(int OrderNo)
        {
            NPOrder order = new NPOrder(OrderNo);
            List<NPOrderOrderDiscount> orderDiscounts = order.GetPresentedCoupons();
            foreach (NPOrderOrderDiscount orderOrderdiscount in orderDiscounts)
            {
                NPOrderDiscount discount = new NPOrderDiscount(orderOrderdiscount.DiscountID);
                if (discount.DiscountCode.StartsWith("V"))
                {
                    discount.EndDate = DateTime.Now;
                    discount.Save();
                }
            }
        }
    }
}