using System.Web.SessionState;
using System.Web;
using System.Text.RegularExpressions;

public class MySessionIDManager: SessionIDManager, ISessionIDManager
{   
    void ISessionIDManager.SaveSessionID( HttpContext context, string id, out bool redirected, out bool cookieAdded )
    {
        base.SaveSessionID( context, id, out redirected, out cookieAdded );

        if (cookieAdded) {
            var name = "ASP.NET_SessionId";
            var cookie = context.Response.Cookies[ name ];
            var regex = new Regex("[^\\.]+\\.[^\\.]+$");
            cookie.Domain = regex.Match(context.Request.Url.Host).Value;
        }
    }
}