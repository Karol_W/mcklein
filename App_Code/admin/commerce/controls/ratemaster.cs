﻿// Decompiled with JetBrains decompiler
// Type: netpoint.admin.commerce.controls.ratemaster
// Assembly: netpoint, Version=8.2.0.7369, Culture=neutral, PublicKeyToken=d935c97cd97f6431
// MVID: AC664620-8146-4552-904D-2293A06475CE
// Assembly location: \\McKlein-FS1\UserProfiles$\MInternJB\My Documents\Visual Studio 2015\Projects\ServiceCallPlugin\ServiceCallPlugin\libs\netpoint.dll

using netpoint.admin.common.controls;
using netpoint.api.common;
using netpoint.api.fulfillment;
using netpoint.classes;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mcklein.admin.commerce.controls
{
    public class ratemaster : netpoint.admin.commerce.controls.ratemaster
    {

        protected TextBox dscCode;

        protected new void Page_Load(object sender, EventArgs e)
        {
            NPBasePage npBasePage = (NPBasePage)this.Page;
            if (!this.IsPostBack)
            {
                NPListBinder.PopulateNPCode((ListControl)this.ddlProviderType, "rateprovider", npBasePage.Encoding, "", true);
                NPListBinder.PopulateNPCode((ListControl)this.ddlCheckField, "ratefield", npBasePage.Encoding, "", false);
                NPListBinder.PopulateNPCode((ListControl)this.ddlChargeType, "ratechargetype", npBasePage.Encoding, "", false);
                NPListBinder.PopulateCountryList((ListControl)this.ddlCountry, "");
                if (((NPRate)this.NPControlObject).CheckField == "PT" || ((NPRate)this.NPControlObject).CheckField == "CT")
                {
                    foreach (ListItem listItem in this.ddlChargeType.Items)
                    {
                        if (listItem.Value == "F")
                        {
                            this.ddlChargeType.Items.Remove(listItem);
                            break;
                        }
                    }
                    this.realTimeShipping.Visible = false;
                }
                this.ddlWeight.DataSource = (object)NPUnit.GetUnits("weight", npBasePage.Encoding, npBasePage.ConnectionString);
                this.ddlWeight.DataValueField = "UnitCode";
                this.ddlWeight.DataTextField = "UnitName";
                this.ddlWeight.DataBind();
                this.ddlWeight.Items.Insert(0, new ListItem("", " "));
                if (this.realTimeShipping.Visible)
                {
                    DataTable dataTable = NPWebConfig.ConfigTypes("NetPoint.ShippingProvider", "", npBasePage.Connection);
                    ArrayList arrayList = new ArrayList();
                    foreach (DataRow dataRow in (InternalDataCollectionBase)dataTable.Rows)
                    {
                        foreach (NPWebConfig npWebConfig in NPWebConfig.GetConfigs(dataRow["App"].ToString(), npBasePage.Connection))
                        {
                            if (npWebConfig.Value == string.Empty && !arrayList.Contains((object)dataRow))
                                arrayList.Add((object)dataRow);
                        }
                    }
                    foreach (DataRow row in arrayList)
                        dataTable.Rows.Remove(row);
                    this.ddlRealTimeProviders.DataSource = (object)dataTable;
                    this.ddlRealTimeProviders.DataTextField = "AppName";
                    this.ddlRealTimeProviders.DataValueField = "App";
                    this.ddlRealTimeProviders.DataBind();
                    this.ddlRealTimeProviders.Items.Insert(0, new ListItem("None", ""));
                    if (dataTable.Rows.Count == 0)
                    {
                        this.NoRealTimeProviders.Text = "Real time rate provider details incomplete. <a href=" + (this.Request.ApplicationPath == "/" ? "" : this.Request.ApplicationPath) + "/admin/common/webconfig/shipgatewayconfig.aspx>Click here to configure</a>";
                        this.NoRealTimeProviders.Visible = true;
                    }
                }
                this.BindData();
            }
            else
            {
                if (!(this.ddlCheckField.SelectedValue == "PT" || this.ddlCheckField.SelectedValue == "CT"))
                    return;
                NPListBinder.PopulateNPCode((ListControl)this.ddlChargeType, "ratechargetype", npBasePage.Encoding, "", false);
                foreach (ListItem listItem in this.ddlChargeType.Items)
                {
                    if (listItem.Value == "F")
                    {
                        this.ddlChargeType.Items.Remove(listItem);
                        break;
                    }
                }
            }
        }

        private void BindData()
        {
            this.txtRateName.Text = ((NPRate)this.NPControlObject).RateName;
            this.txtBlurb.Text = ((NPRate)this.NPControlObject).Blurb;
            this.ddlCheckField.SelectedIndex = this.ddlCheckField.Items.IndexOf(this.ddlCheckField.Items.FindByValue(((NPRate)this.NPControlObject).CheckField));
            this.ddlChargeType.SelectedIndex = this.ddlChargeType.Items.IndexOf(this.ddlChargeType.Items.FindByValue(((NPRate)this.NPControlObject).ChargeType));
            this.BindBasis(((NPRate)this.NPControlObject).UnitCode);
            this.ddlProviderType.SelectedIndex = this.ddlProviderType.Items.IndexOf(this.ddlProviderType.Items.FindByValue(((NPRate)this.NPControlObject).ProviderType));
            this.ddlRealTimeProviders.SelectedIndex = this.ddlRealTimeProviders.Items.IndexOf(this.ddlRealTimeProviders.Items.FindByValue(((NPRate)this.NPControlObject).RealTimeProviderType));
            this.txtProviderCode.Text = ((NPRate)this.NPControlObject).ProviderCode;
            if (this.ddlRealTimeProviders.SelectedIndex > 0 && ((NPRate)this.NPControlObject).RateType != "H")
            {
                NPBasePage npBasePage = (NPBasePage)this.Page;
                this.ddlRealTimeProviderQuotes.Enabled = true;
                NPListBinder.PopulateNPCode((ListControl)this.ddlRealTimeProviderQuotes, ((NPRate)this.NPControlObject).RealTimeProviderType, npBasePage.Encoding, "", false);
                this.ddlRealTimeProviderQuotes.SelectedIndex = this.ddlRealTimeProviderQuotes.Items.IndexOf(this.ddlRealTimeProviderQuotes.Items.FindByValue(((NPRate)this.NPControlObject).RealTimeProviderCode));
            }
            this.ddlCountry.SelectedIndex = this.ddlCountry.Items.IndexOf(this.ddlCountry.Items.FindByValue(((NPRate)this.NPControlObject).CountryAbbr));
            NPListBinder.PopulateStateList((ListControl)this.ddlState, "", this.ddlCountry.SelectedValue);
            this.ddlState.SelectedIndex = this.ddlState.Items.IndexOf(this.ddlState.Items.FindByValue(((NPRate)this.NPControlObject).StateAbbr));
            this.txtSortCode.Text = ((NPRate)this.NPControlObject).SortCode.ToString();
            this.chkNotifyLaterFlag.Checked = ((NPRate)this.NPControlObject).NotifyLater;
            this.chkRoleRestricted.Checked = ((NPRate)this.NPControlObject).RoleRestricted;
            if (((NPRate)this.NPControlObject).RateType.Equals("ST"))
            {
                this.ddlCountry.Visible = true;
                this.ddlState.Visible = true;
                this.txtRateName.Visible = false;
                this.rfvRateName.Visible = false;
            }
            else if (((NPRate)this.NPControlObject).RateType.Equals("CT"))
            {
                this.ddlCountry.Visible = true;
                this.ddlState.Visible = false;
                this.txtRateName.Visible = false;
                this.rfvRateName.Visible = false;
            }
            else if (((NPRate)this.NPControlObject).RateType.Equals("LT"))
            {
                this.ddlCountry.Visible = true;
                this.ddlState.Visible = true;
                this.txtRateName.Visible = true;
                this.rfvRateName.Visible = true;
            }
            else
            {
                this.ddlCountry.Visible = false;
                this.ddlState.Visible = false;
                this.txtRateName.Visible = true;
                this.rfvRateName.Visible = true;
            }
        }

        protected new void ddlCheckField_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindBasis("");
            this.RaiseBubbleEvent((object)this, (EventArgs)new NPEventArgs("CheckField_Changed", (object)this.ddlCheckField.SelectedValue));
        }

        private void BindBasis(string basisType)
        {
            
            if (this.ddlCheckField.SelectedValue == "PT")
            {
                this.ddlWeight.Visible = false;
                this.ppkBasis.Visible = true;
                this.dscCode.Visible = false;
                this.ppkBasis.Text = basisType;
            }
            else if (this.ddlCheckField.SelectedValue == "CT")
            {
                this.ddlWeight.Visible = false;
                this.ppkBasis.Visible = false;
                this.dscCode.Visible = true;
                this.dscCode.Text = basisType;
            }
            else
            {
                this.ddlWeight.Visible = true;
                this.ppkBasis.Visible = false;
                this.dscCode.Visible = false;
                this.ddlWeight.SelectedIndex = this.ddlWeight.Items.IndexOf(this.ddlWeight.Items.FindByValue(basisType));
            }
        }
        private NPRate _rate;
        public new bool Save()
        {

            this._rate = ((NPRate)this.NPControlObject);
            string str = "";

            int num = 0;
            try
            {
                num = Convert.ToInt32(this.txtSortCode.Text);
            }
            catch
            {
                str = str + this.ltlSortCode.Text + " " + this.errMustBeNumber.Text + "<br>";
            }


            switch (this._rate.RateType)
            {
                case "ST":
                    this._rate.RateName = this.ddlState.SelectedValue + "-" + this.ddlCountry.SelectedValue;
                    break;
                case "CT":
                    this._rate.RateName = this.ddlCountry.SelectedValue;
                    break;
                case "LT":
                    this._rate.RateName = this.txtRateName.Text;
                    break;
                default:
                    this._rate.RateName = this.txtRateName.Text;
                    break;
            }
            this._rate.StateAbbr = this.ddlState.SelectedValue;
            this._rate.CountryAbbr = this.ddlCountry.SelectedValue;
            this._rate.Blurb = this.txtBlurb.Text;
            if (this._rate.RateType == null || this._rate.RateType == "")
                this._rate.RateType = this.Request.QueryString["type"].ToString();
            
            switch (this.ddlCheckField.SelectedValue)
            {
                case "PT":
                    this._rate.UnitCode = this.ppkBasis.Text;
                    break;
                case "CT":
                    this._rate.UnitCode = this.dscCode.Text;
                    break;
                default:
                    this._rate.UnitCode = this.ddlWeight.SelectedValue;
                    break;
            }
            this._rate.CheckField = this.ddlCheckField.SelectedValue;
            this._rate.ChargeType = this.ddlChargeType.SelectedValue;
            this._rate.SortCode = num;
            this._rate.NotifyLater = this.chkNotifyLaterFlag.Checked;
            this._rate.RoleRestricted = this.chkRoleRestricted.Checked;
            this._rate.ProviderType = this.ddlProviderType.SelectedValue;
            this._rate.ProviderCode = this.txtProviderCode.Text;
            this._rate.RealTimeProviderType = this.ddlRealTimeProviders.SelectedValue;
            this._rate.RealTimeProviderCode = this.ddlRealTimeProviderQuotes.SelectedValue;
            if (this._rate.RateName.Length == 0)
                str = str + this.ltlRateName.Text + " " + this.errIsRequired.Text + "<br>";
            if (str.Length > 0)
                this.sysError.Text += str;
            else
                this._rate.Save();
            return true;
        }
    }
}
