﻿// Decompiled with JetBrains decompiler
// Type: netpoint.admin.commerce.controls.ratemaster
// Assembly: netpoint, Version=8.2.0.7369, Culture=neutral, PublicKeyToken=d935c97cd97f6431
// MVID: AC664620-8146-4552-904D-2293A06475CE
// Assembly location: \\McKlein-FS1\UserProfiles$\MInternJB\My Documents\Visual Studio 2015\Projects\ServiceCallPlugin\ServiceCallPlugin\libs\netpoint.dll

using mcklein.admin.commerce.controls;
using netpoint.admin.common.controls;
using netpoint.api.common;
using netpoint.api.fulfillment;
using netpoint.classes;
using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mcklein.admin.commerce
{
    public class rate : netpoint.admin.commerce.rate
    {
        

        protected new void btnSave2_Click(object sender, ImageClickEventArgs e)
        {
            if (!((ratemaster)this.theMaster).Save())
                return;
            this.theDetail.Save();
            this.RedirectToParent("~/admin/commerce/rates.aspx?ratetype=" + this.Request.QueryString["type"]);
        }

        private void RedirectToParent(string defaultPath)
        {
            if (SiteMap.CurrentNode != null && SiteMap.CurrentNode.ParentNode != null)
            {
                string url1 = SiteMap.CurrentNode.ParentNode.Url;
                string[] strArray = url1.Split('?');
                if (strArray.Length > 1 && !string.IsNullOrEmpty(strArray[0]))
                {
                    string str = strArray[1];
                    string url2 = strArray[0];
                    if (str.IndexOf("Callback", StringComparison.CurrentCultureIgnoreCase) >= 0)
                        this.Response.Redirect(url2, true);
                    else
                        this.Response.Redirect(url1, true);
                }
                else
                    this.Response.Redirect(SiteMap.CurrentNode.ParentNode.Url);
            }
            else
                this.Response.Redirect(defaultPath);
        }
    }
}
