using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.classes;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class manual : netpoint.classes.NPBasePage
    {
        protected netpoint.catalog.controls.CategoriesBlock npcblock;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (((NPBasePage)this.Page).Catalog.ViewOnly)
            {
                Response.Redirect("~/default.aspx");
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
