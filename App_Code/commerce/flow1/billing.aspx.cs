﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.commerce;
using netpoint.api.account;
using netpoint.api.utility;
using netpoint.classes;
using NetPoint.CreditCards;
using System.Resources;

namespace netpoint.commerce.flow1
{
    /// <summary>
    /// Summary description for billing.
    /// </summary>
    public partial class billing : NPBasePageCommerce
    {

        private NPOrder _order;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _order = new NPOrder(UserID, SessionID);
            if (!_order.Initialized || _order.OrderDetail.Count == 0 || _order.UserID == null || _order.UserID == "")
            {
                Response.Redirect("cart.aspx");
            }

            paymentBlock.Order = _order;
            sysErrorMessage.Text = "";

            if (!this.IsPostBack)
            {
                AddAddress.ShowSave = false;
                pnlB2B.Visible = _order.Account.AccountType.Equals("B");
                BindForm();
                string nextPage;
                btnNext.ImageUrl = ResolveImage("buttons", "continue.gif");
                if ((nextPage = GetFlow("checkout")) != null)
                {
                    if (nextPage.Equals("checkout"))
                    {
                        btnNext.ImageUrl = ResolveImage("buttons", "placeyourorder.gif");
                    }
                }

                topimage.ImageUrl = ResolveImage("images", "checkout2.gif");
            }
            SetShippingBlockVisibility();

            lnkShowShippingImage.ImageUrl = ResolveImage("icons", "expand.gif");
            lnkHideShippingImage.ImageUrl = ResolveImage("icons", "collapse.gif");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterScript();
        }

        protected void SetShippingBlockVisibility()
        {
            string showBlockStyle;
            string hideBlockStyle;
            if (rblBillingAddress.SelectedIndex != -1)
            {
                showBlockStyle = "";
                hideBlockStyle = "none";
            }
            else
            {
                showBlockStyle = "none";
                hideBlockStyle = "";
            }
            rblShippingAddress.Style["display"] = hideBlockStyle;
            lnkShowShipping.Style["display"] = showBlockStyle;
            lnkShowShippingImage.Style["display"] = showBlockStyle;
            lnkHideShipping.Style["display"] = hideBlockStyle;
            lnkHideShippingImage.Style["display"] = hideBlockStyle;
        }

        private void RegisterScript()
        {
            string theScript =
            "\n<script type='text/javascript'> \n" +
            "function toggleShippingAddress()\n" +
            "{\n" +
            "   var shippingAddressList = document.getElementById('" + rblShippingAddress.ClientID + "');\n" +
            "   var lnkShowShipping = document.getElementById('" + lnkShowShipping.ClientID + "');\n" +
            "   var lnkShowShippingImage = document.getElementById('" + lnkShowShippingImage.ClientID + "');\n" +
            "   var lnkHideShipping = document.getElementById('" + lnkHideShipping.ClientID + "');\n" +
            "   var lnkHideShippingImage = document.getElementById('" + lnkHideShippingImage.ClientID + "');\n" +
            "   if(shippingAddressList.style.display == 'none')\n" +
            "   {\n" +
            "       shippingAddressList.style.display = 'block';\n" +
            "       lnkShowShipping.style.display = 'none';\n" +
            "       lnkShowShippingImage.style.display = 'none';\n" +
            "       lnkHideShipping.style.display = '';\n" +
            "       lnkHideShippingImage.style.display = '';\n" +
            "   }\n" +
            "   else\n" +
            "   {\n" +
            "       shippingAddressList.style.display = 'none';\n" +
            "       lnkShowShipping.style.display = '';\n" +
            "       lnkShowShippingImage.style.display = '';\n" +
            "       lnkHideShipping.style.display = 'none';\n" +
            "       lnkHideShippingImage.style.display = 'none';\n" +
            "   }\n" +
            "}\n" +
            "</script>\n";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "toggleShippingAddress", theScript);
        }

        private void BindAddress(ListControl lc)
        {
            if (lc == rblShippingAddress)
            {
                lc.DataSource = NPAddress.GetAddresses(_order.AccountID, true, false, "", ConnectionString);
                lc.DataValueField = "AddressID";
                lc.DataBind();
            }
            else
            {
                lc.DataSource = NPAddress.GetAddresses(_order.AccountID, false, true, true, hdnNewAddress.Text, ConnectionString);
                lc.DataValueField = "AddressID";
                lc.DataBind();
                lc.SelectedIndex = lc.Items.IndexOf(lc.Items.FindByValue(_order.BillingAddressID.ToString()));
                if (lc.SelectedIndex == -1 && lc.Items.Count > 0)
                {
                    lc.SelectedIndex = 0;
                }
            }
        }

        private void BindForm()
        {
            NPAccount account = new NPAccount(_order.AccountID);
            BindAddress(rblBillingAddress);
            rblBillingAddress_SelectedIndexChanged(this, null);
            BindAddress(rblShippingAddress);

            if (account.GetShippingAddresses().Count == 0)
            {
                lnkShowShippingImage.Visible = false;
                lnkShowShipping.Visible = false;
            }
            txtPO.Text = _order.PONumber;
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            CommandEventArgs cea = (CommandEventArgs)args;
            if (cea != null)
            {
                if (cea.CommandName.Equals("AddAddress"))
                {
                    _order.BillingAddressID = Convert.ToInt32(cea.CommandArgument);
                    _order.PONumber = txtPO.Text;
                    _order.Save();
                    Response.Redirect("billing.aspx", true);
                }
            }
            return true;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext.Click += new System.Web.UI.ImageClickEventHandler(this.btnNext_Click);

        }
        #endregion

        protected void rblBillingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (rblBillingAddress.SelectedIndex != -1)
            {
                rblShippingAddress.SelectedIndex = -1;
                ChangeAddress(rblBillingAddress);
            }
            SetShippingBlockVisibility();
        }

        protected void rblShippingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (rblShippingAddress.SelectedIndex != -1)
            {
                rblBillingAddress.SelectedIndex = -1;
                ChangeAddress(rblShippingAddress);
            }
            SetShippingBlockVisibility();

        }

        private void ChangeAddress(ListControl lc)
        {
            int addressID = Convert.ToInt32(lc.SelectedValue);
            NPAddress address = new NPAddress(addressID);
            if (address.ShipDestination)
            {
                address.AddressID = 0;
                address.ShipDestination = false;
            }
            AddAddress.LoadAddress(address);
            if (addressID == 0)
            {
                AddAddress.AccountID = _order.AccountID;
                AddAddress.UserID = _order.UserID;
            }
            _order.BillingAddressID = Convert.ToInt32(lc.SelectedValue);
            _order.PONumber = txtPO.Text;
            _order.Save();
        }

        protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            List<NPError> theErrors = new List<NPError>();
            if (AddAddress.Save(theErrors) > 0)
            {
                if (paymentBlock.SubmitPayment())
                {
                    paymentBlock.AddPayment();
                    //  need to calculate incase the payment method incurs a shipping charge
                    _order.Calculate();
                    ((NPOrderPayment)_order.Payments[0]).PaymentAmount = _order.GrandTotal;
                    ((NPOrderPayment)_order.Payments[0]).Save();
                    int newAdd = AddAddress.Save(theErrors);
                    if (newAdd > 0)
                    {
                        _order.BillingAddressID = newAdd;
                    }
                    _order.PONumber = txtPO.Text;
                    _order.Save();
                    if (!_order.PaymentTotal.ToString("c").Equals(_order.GrandTotal.ToString("c")))
                    {
                        theErrors.Add(new NPError("Total Payments must match the Total of the Order", NPErrorType.ORDR_PaymentNotTotal));
                    }
                    if (Page.IsValid && newAdd != 0 && !DisplayErrors(theErrors))
                    {
                        string nextPage;
                        if ((nextPage = GetFlow("checkout")) != null)
                        {
                            if (nextPage.Equals("checkout"))
                            {
                                this.order();
                            }
                            else
                            {
                                Response.Redirect(nextPage, true);
                            }
                        }
                        else
                        {
                            NPCCProvider.ExitPage(_order, "verify.aspx", "cart.aspx");
                        }
                    }
                }
            }
            else
            {
                DisplayErrors(theErrors);
            }
        }

        private bool DisplayErrors(List<NPError> theErrors)
        {
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.ADDR_DuplicateNameBilling:
                        sysErrorMessage.Text += errDuplicationAddressNameBilling.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_AddressNameIsRequired:
                        sysErrorMessage.Text += hdnAddressNameRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_CityIsRequired:
                        sysErrorMessage.Text += hdnCityIsRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_PostalCodeIsRequired:
                        sysErrorMessage.Text += hdnPostalCodeRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_Street1IsRequired:
                        sysErrorMessage.Text += hdnStreet1Required.Text + "<br/>";
                        break;
                    case NPErrorType.ORDR_PaymentNotTotal:
                        sysErrorMessage.Text += errTotalPayments.Text += "<br/>";
                        break;
                    default:
                        throw npe;

                }
            }
            return theErrors.Count > 0;
        }

        private void order()
        {
            NPAccount a = new NPAccount();
            a.AccountID = _order.AccountID;

            NPCCAuth npcca = new NPCCAuth();
            if (npcca.Authorize(_order))
            {
                _order.IPAddress = Request.UserHostAddress;
                _order.SentFlag = "N";
                _order.ServerID = base.ServerID;
                _order.Save();
                if (_order.CheckOut(Request.UserHostAddress, ServerID))
                {
                    Response.Redirect("confirm.aspx?OrderNo=" + _order.OrderID.ToString());
                }
                else
                {
                    Response.Write("Checkout Error");
                }
            }
            else
            {
                sysErrorMessage.Text += "<br>" + errCardNotAuthorized.Text + " (" + npcca.ErrorMessage + ")";
            }
        }
    }
}
