using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.classes;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.api.fulfillment;
using System.Resources;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary description for shipment.
    /// </summary>
    public partial class shipment : netpoint.classes.NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (Request.QueryString["shipmentid"] == null)
            {
                Response.Write(errShipmentNotFound.Text);
                Response.End();
            }

            NPShipment shipment = new NPShipment(Convert.ToInt32(Request.QueryString["shipmentid"]));
            if (!shipment.Initialized || !shipment.AccountID.Equals(this.AccountID))
            {
                Response.Write(errShipmentNotFound.Text);
                Response.End();
            }

            NPAddress address = new NPAddress(shipment.ShipAddressID);
            NPRate rate = new NPRate(shipment.ShipMethodID);

            sysShipmentID.Text = shipment.ShipmentID.ToString();
            sysShippingAddress.Text = address.AddressString.Replace("\n", "<br>");
            sysShipMethod.Text = rate.RateName + "<br>" + rate.Blurb;
            sysTrackingNumber.Text = shipment.TrackingNum;
            if (shipment.ActualShipDate == DateTime.MinValue && shipment.ActualDelivDate == DateTime.MinValue)
            {
                sysStatus.Text = hdnReadyToShip.Text;
            }
            else if (shipment.ActualShipDate != DateTime.MinValue && shipment.ActualDelivDate == DateTime.MinValue)
            {
                sysStatus.Text = hdnShipped.Text;
            }
            else if (shipment.ActualShipDate != DateTime.MinValue && shipment.ActualDelivDate != DateTime.MinValue)
            {
                sysStatus.Text = hdnDelivered.Text;
            }

            gvDetail.DataSource = shipment.ShipmentDetail;
            gvDetail.DataBind();

            sysRequestedShipDate.Text = (shipment.RequestedShipDate == DateTime.MinValue ? hdnNotSet.Text : shipment.RequestedShipDate.ToShortDateString());
            sysEstShipDate.Text = (shipment.EstShipDate == DateTime.MinValue ? hdnNotSet.Text : shipment.EstShipDate.ToShortDateString());
            sysActualShipDate.Text = (shipment.ActualShipDate == DateTime.MinValue ? hdnNotSet.Text : shipment.ActualShipDate.ToShortDateString());
            sysEstDelivDate.Text = (shipment.EstDelivDate == DateTime.MinValue ? hdnNotSet.Text : shipment.EstDelivDate.ToShortDateString());
            sysActualDelivDate.Text = (shipment.ActualDelivDate == DateTime.MinValue ? hdnNotSet.Text : shipment.ActualDelivDate.ToShortDateString());
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

    }
}
