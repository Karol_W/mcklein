using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.commerce;
using System.Collections.Generic;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for _default.
    /// </summary>
    public partial class confirm : netpoint.classes.NPBasePageCommerce
    {

        public int OrderNo;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            OrderNo = CheckOrderNo();
            OrderNoLabel.Text = OrderNo.ToString();
            checkCoupons(OrderNo);            
            sysGo.NavigateUrl = "invoice.aspx?OrderNo=" + OrderNo.ToString();
            lnkGoAgain.NavigateUrl = "invoice.aspx?OrderNo=" + OrderNo.ToString();
            sysGo.ImageUrl = base.ResolveImage("icons", "indicator.gif");
        }

        #region check incoming requests

        public int CheckOrderNo()
        {
            int orderno = 0;
            try
            {
                orderno = Convert.ToInt32(Request.QueryString["OrderNo"]);
            }
            catch { }
            return orderno;
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
