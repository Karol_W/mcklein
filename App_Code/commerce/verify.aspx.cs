using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.data;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.api.fulfillment;
using netpoint.api.prospecting;
using netpoint.classes;
using netpoint.api.account;
using System.Text;
using System.Resources;
using netpoint.common.controls;
using NetPoint.ShippingProvider;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary description for verify.
    /// </summary>
    public partial class verify : NPBasePageCommerce
    {

        private NPOrder _order;
        //exposed address variables to use in populate rate method call
        private NPAddress shipto;
        private NPAddress billto;

        protected void Page_Load(object sender, System.EventArgs e)
        {            
            _order = new NPOrder(UserID, SessionID);
            topimage.ImageUrl = this.ResolveImage("images", "checkout3.gif");
            btnOrder.ImageUrl = this.ResolveImage("buttons", "placeyourorder.gif");

            if (!_order.Initialized || _order.OrderDetail.Count == 0 || _order.UserID == null || _order.UserID == "")
            {
                Response.Redirect("cart.aspx");
            }

            //check if notes should be displayed
            
            if (TheTheme.DisplayCheckoutNotes)
            {
                pnlOrderNotes.Visible = true;
            }

            //check for payment error and display error message
            if (Request.QueryString["payerror"] != null)
            {
                sysErrorMessage.Text = errPaymentMismatch.Text;
            }

            //check for multiple taxes on a line and display error message
            try
            {
                _order.Valid(netpoint.api.OrderValidationType.OrderTaxes);
            }
            catch (Exception ex)
            {
                sysErrorMessage.Text = hdnIncorrectTax.Text;
                HideButtonPanel();
            }

            shipto = new NPAddress(_order.GetShipAddress());
            sysShippingAddress.Text = shipto.AddressHTML;

            billto = new NPAddress(_order.BillingAddressID);
            sysBillingAddress.Text = billto.AddressHTML;

            //Must test for callback as it causes a postback on changes
            if (!this.IsPostBack && !clbackTotals.CausedCallback)
            {
                //Call the shipping rate population method
                BindForm();
                NPCCProvider.EnterPage(_order, "cart.aspx");
            }

            CartList.Order = _order;

            //Add the javascript call to each radio button in the list. This allows the callback to be triggered
            foreach (ListItem Li in rblShipMethod.Items)
            {
                Li.Attributes.Add("onclick", "selectProduct('" + Li.Value + "');");
            }            

            //Commented out by Curtis as this was moved to the RefreshPaymentInformation method
            //gvPayments.DataSource = _order.Payments;
            //gvPayments.DataBind();
            //CALL METHOD TO REFRESH PAYMENTS, This is done in the call back handler so not needed on callbacks
            if (!IsPostBack && !clbackTotals.CausedCallback)
            {
                RefreshPaymentInformation();
                expenses.LoadExpenses(_order);

                decimal _balance = decimal.Round(Convert.ToDecimal(_order.GrandTotal), 2) - decimal.Round(Convert.ToDecimal(_order.PaymentTotal), 2);
                sysPaymentTotal.Price = _order.PaymentTotal;
                sysBalance.Price = _balance;
            }

            if (_order.Account.UseApprovals)
            {
                NPUser ou = new NPUser(_order.UserID);
                if (ou.ApprovalLimit != 0 && _order.GrandTotal > ou.ApprovalLimit)
                {
                    btnSubmitForApproval.Visible = true;
                    btnOrder.Visible = false;
                }
            }

            //Must test for callback as it causes a postback on changes
            if (!IsPostBack && !clbackTotals.CausedCallback) 
            {
                rowB2B.Visible = _order.Account.AccountType.Equals("B");
                bool fastCheckout = false;
                if (!String.IsNullOrEmpty(Request.QueryString["fastcheckout"]))
                {
                    fastCheckout = Convert.ToBoolean(Request.QueryString["fastcheckout"]);
                }
                //NO LONGER NEED TO CHECK THIS cellRequestedShipDate.Visible = fastCheckout;
                cellPONo.Visible = fastCheckout;
                //This was removed as the requested ship date is not set on the shipping page
                dtRequestedShipDate.SelectedDate = _order.RequestedShipDate;
                txtPO.Text = _order.PONumber;
                txtNotes.Text = _order.Notes;

                cellRequestedShipDate.Visible = _order.Account.AccountType.Equals("B");
                ltlRequestedShipDate.Visible = _order.Account.AccountType.Equals("B");
                dtRequestedShipDate.Visible = _order.Account.AccountType.Equals("B");

                //NOT SETTING THIS ANYMORE 
                //NPRate rate = new NPRate(_order.ShippingMethod);
                //sysShipMethod.Text = rate.RateName;
            }

            if (!IsPostBack)
            {
                imgRequestedShipDate.ImageUrl = this.ResolveImage("icons", "warning.gif");
                imgNotes.ImageUrl = this.ResolveImage("icons", "warning.gif");
                imgOutOfStock.ImageUrl = this.ResolveImage("icons", "warning.gif");
                imgUnavailableOutOfStock.ImageUrl = this.ResolveImage("icons", "outofstock.gif");
                imgUnavailable.ImageUrl = this.ResolveImage("icons", "availableno.gif");
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrder.Click += new System.Web.UI.ImageClickEventHandler(this.btnOrder_Click);
            this.btnSubmitForApproval.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmitForApproval_Click);
        }
        #endregion


        private void btnOrder_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (_order.PaymentTotal != _order.GrandTotal)
            {                
                Response.Redirect("verify.aspx?payerror=1");
                return;
            }

            if (!CheckAndSetRequestedShipDateAndPO(_order))
            {
                return;
            }

            Response.Redirect("paymentauthorize.aspx", true);
        }       

        protected void gvPayments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            NPOrderPayment p = (NPOrderPayment)e.Row.DataItem;
            if (p != null)
            {
                ((PriceDisplay)e.Row.FindControl("prcAmount")).Price = p.PaymentAmount;
            }
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            NPEventArgs npa = null;
            if (args.GetType().FullName == "netpoint.classes.NPEventArgs")
            {
                npa = (NPEventArgs)args;
            }

            if (npa != null)
            {
                switch (npa.FromEvent)
                {
                    case "EmptyCart":
                        Response.Redirect("cart.aspx");
                        break;
                    case "outofstock":
                        imgOutOfStock.Visible = true;
                        lblOutOfStock.Visible = true;
                        break;
                    case "unavailableoutofstock":
                        imgUnavailableOutOfStock.Visible = true;
                        lblUnavailableOutOfStock.Visible = true;
                        btnOrder.Visible = false;
                        btnOrder.Enabled = false;
                        btnOrder.Width = 0;
                        break;
                    case "unavailable":
                        imgUnavailable.Visible = true;
                        lblUnavailable.Visible = true;
                        btnOrder.Visible = false;
                        btnOrder.Enabled = false;
                        btnOrder.Width = 0;
                        break;
                }
            }
            return true;
        }

        private void btnSubmitForApproval_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            _order.IPAddress = Request.UserHostAddress;
            _order.SentFlag = "D";
            _order.ServerID = this.ServerID;
            _order.CartTypeEnum = CartType.WaitingApproval;
            _order.ClosedDate = DateTime.Now;
            if (!CheckAndSetRequestedShipDateAndPO(_order)) 
            {
                return;
            }
            _order.Save();
            //
            NPUser maincontact = new NPUser(_order.Account.MainContactID);
            NPCommunicationLog cl = new NPCommunicationLog();
            cl.Email = maincontact.Email;
            cl.Incoming = false;
            cl.LogStatus = "O";
            cl.LogType = "email";
            cl.TimeStamp = DateTime.Now;
            cl.AccountID = _order.AccountID;
            cl.UserID = _order.UserID;
            cl.OwnerID = _order.UserID;
            cl.TemplateCode = "submitforapproval";
            cl.MergeObjectType = "order";
            cl.MergeObjectID = _order.OrderID.ToString();
            cl.Save();
            //
            Response.Redirect("submit.aspx?OrderNo=" + _order.OrderID.ToString());
        }

        private bool CheckAndSetRequestedShipDateAndPO(NPOrder _order)
        {
            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
                return false;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }
            if (txtNotes.Text.Length > txtNotes.LimitInput)
            {
                imgNotes.Visible = true;
                return false;
            }
            else
            {
                imgNotes.Visible = false;
                _order.Notes = txtNotes.Text;
            }
            _order.PONumber = txtPO.Text;            
            _order.Save();
            return true;
        }

        #region SHIP METHOD CODE FROM SHIPPING.ASPX
        private void BindForm()
        {
            try
            {
                //Updated method call to included address parameters used for zone filtering
                NPListBinder.PopulateRateList(rblShipMethod, "S", _order.ShippingMethod.ToString(), false, true, true, _order.SubTotal, _order.WeightTotal, _order.WeightTotalUnitCode, UserID, true, shipto.CountryAbbr, shipto.StateAbbr, shipto.PostalCode);

                if (rblShipMethod.SelectedIndex == -1 && rblShipMethod.Items.Count > 0)
                {
                    rblShipMethod.SelectedIndex = 0;
                    _order.SetAllDetailShipMethod(Convert.ToInt32(rblShipMethod.SelectedValue));
                }
                if (rblShipMethod.Items.Count == 0)
                {
                    sysErrorMessage.Text = errNoShipping.Text + "<br>" + errConfigureShipping.Text;
                    //TODO is this correct to hide both these buttons
                    HideButtonPanel();
                }

                //Code called on the shipping aspx page
                int shipMethod = 0;
                int.TryParse(rblShipMethod.SelectedValue, out shipMethod);
                _order.ShippingMethod = shipMethod;
                List<Exception> excList = _order.Calculate();
                if (excList.Count > 0)
                {
                    //check first exception in list
                    if (excList[0] is ShippingProviderException)
                    {
                        lblRealError.Text = excList[0].Message;
                        if (excList[0].InnerException != null) lblRealError.Text += excList[0].InnerException.Message;
                        HideButtonPanel();
                        //Log the error so it can be tracked
                        NPErrorLog.LogError(excList[0], UserID, "verify.aspx", ConnectionString);
                        _order.ShippingTotal = 0;
                        _order.Save(false);
                    }
                    else
                    {
                        throw excList[0];
                    }
                }                 

                //Refresh the payment section 
                RefreshPaymentInformation();
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblRealError.Text = ex.Message;
                if (ex.InnerException != null) lblRealError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "verify.aspx", ConnectionString);             
            }
        }
        /// <summary>
        /// Triggered by the on click event of the radio button list. Dynamically refreshes the totals.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void clbackTotals_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            try
            {
                ShipMethodChanged(rblShipMethod, e.Parameter);
                //Reload the Expenses Section to update
                expenses.LoadExpenses(_order);
                //NOT SETTING THIS ANYMORE Update the Shipping Via Section
                //NPRate rate = new NPRate(_order.ShippingMethod);
                //sysShipMethod.Text = rate.RateName;

                RefreshPaymentInformation();

                decimal _balance = decimal.Round(Convert.ToDecimal(_order.GrandTotal), 2) - decimal.Round(Convert.ToDecimal(_order.PaymentTotal), 2);
                sysPaymentTotal.Price = _order.PaymentTotal;
                sysBalance.Price = _balance;

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblRealError.Text = ex.Message;
                if (ex.InnerException != null) lblRealError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "verify.aspx", ConnectionString);

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }

        }

        /// <summary>
        /// Refreshes the payment objects and reloads the grid with the updated totals.
        /// </summary>
        private void RefreshPaymentInformation()
        {
            //TODO Verify that we can only ever have one payment object
            foreach (NPOrderPayment p in _order.Payments)
            {
                //Make sure the payment matches the grand total
                if (_order.GrandTotal != p.PaymentAmount)
                {
                    p.PaymentAmount = _order.GrandTotal;
                    p.Save();
                }
            }
            //update the payment information
            gvPayments.DataSource = _order.Payments;
            gvPayments.DataBind();
            _order.ReloadPayments();    
        }

        //REPLACED BY THE clbackTotals_Callback METHOD protected void rblShipMethod_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    //ShipMethodChanged(rblShipMethod);
        //    ////Reload the Expenses Section to update
        //    //expenses.LoadExpenses(_order);
        //    ////Update the Shipping Via Section
        //    //NPRate rate = new NPRate(_order.ShippingMethod);
        //    //sysShipMethod.Text = rate.RateName;
        //}

        private void ShipMethodChanged(ListControl lc, string selectVal)
        {
            //_order.ShippingMethod = Convert.ToInt32(lc.SelectedValue);
            _order.ShippingMethod = Convert.ToInt32(selectVal);
            
            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }

            List<Exception> excList = _order.Calculate();
            if (excList.Count > 0)
            {
                //check first exception in list
                if (excList[0] is ShippingProviderException)
                {
                    lblRealError.Text = excList[0].Message;
                    if (excList[0].InnerException != null) lblRealError.Text += excList[0].InnerException.Message;
                    HideButtonPanel();
                    //Log the error so it can be tracked
                    NPErrorLog.LogError(excList[0], UserID, "verify.aspx", ConnectionString);
                    _order.ShippingTotal = 0;
                    _order.Save(false);
                }
                else
                {
                    throw excList[0];
                }
            }                            
        }

        private void HideButtonPanel()
        {
            pnlButton.Enabled = false;
            pnlButton.Width = 0;
            btnOrder.Width = 0;
            btnSubmitForApproval.Width = 0;
        }
        #endregion
    }
}
