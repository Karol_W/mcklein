using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.data;
using netpoint.api.commerce;
using netpoint.classes;
using NetPoint.WebControls;
using netpoint.api.catalog;
using NetPoint.ShippingProvider;

namespace netpoint.catalog
{
    /// <summary>
    /// Summary description for cart.
    /// </summary>
    public partial class cart : NPBasePage
    {

        private NPOrder _order;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            _order = new NPOrder(UserID, SessionID);
            if (!_order.Initialized)
            {
                _order.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                if (!_order.CreateDefaultOrder(UserID, AccountID, SessionID))
                {
                    Response.Write("Catastrophic Failure.");
                    Response.End();
                }
            }

            else
            {
                _order.ServerID = ServerID;
                _order.Save(false);
            }

            //validate taxes on order
            bool inValidTax = false;
            try
            {
                _order.Valid(netpoint.api.OrderValidationType.OrderTaxes);
            }
            catch (Exception ex)
            {
                lblIncorrectTax.Visible = true;
                inValidTax = true;
            }

            string nextPage;
            lnkViewListsImg.ImageUrl = ResolveImage("icons", "indicator.gif"); 
            lnkSaveCartImg.ImageUrl = ResolveImage("icons", "indicator.gif");
            lnkClearCartImg.ImageUrl = ResolveImage("icons", "indicator.gif");
            lnkCheckOut.ImageUrl = ResolveImage("buttons", "continuetocheckout.gif");
            
            if (UserID.Equals("") || AccountID.Equals(""))
            {
                lnkCheckOut.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=checkout";
                lnkCartSave.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=savecart";
                lnkSaveCartImg.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=savecart";
                lnkViewListsImg.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=savedlists";
                lnkViewLists.NavigateUrl = "~/common/accounts/login.aspx?ReturnTo=savedlists";
            }
            else if ((nextPage = GetFlow("checkout")) != null)
            {
                lnkCheckOut.NavigateUrl = nextPage;
            }

            //clear cart==================
            if (Request.QueryString["ClearCart"] != null && Request.QueryString["ClearCart"].Equals("true"))
            {
                _order.RemoveAllDetail();
                _order.Calculate();
            }
            // get the quantity if it has been passed in
            int quantityToAdd = 1;
            if (Request.QueryString["quantity"] != null)
            {
                int.TryParse(Request.QueryString["quantity"], out quantityToAdd);
            }

            //find the variant that exists for the passed part and place it on this order
            if (Request.QueryString["AddVariantNo"] != null)
            {
                NPVariant.AddVariantToCart(Request.QueryString["AddVariantNo"].ToString(), AccountID, SessionID, _order.OrderID, PriceList, ConnectionString);
                Response.Redirect(Request.FilePath, true);
            }
            //add part - if/else prevents two calculations on one request.
            if (Request.QueryString["AddPartNo"] != null)
            {
                if (Request.QueryString["notes"] != null)
                {
                    _order.AddPart(Request.QueryString["AddPartNo"], quantityToAdd, Catalog.CatalogCode, "", PriceList, Request.QueryString["notes"], NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                }
                else
                {
                    _order.AddPart(Request.QueryString["AddPartNo"], quantityToAdd, Catalog.CatalogCode, "", PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                }
                Response.Redirect(Request.FilePath, true);
            }
            else
            {
                List<Exception> excList = _order.Calculate();
                if (excList.Count > 0)
                {
                    //check first exception in list
                    if (excList[0] is ShippingProviderException)
                    {
                        _order.ShippingTotal = 0;
                        _order.Save(false);
                        lblShippingRemoved.Visible = true;
                    }
                    else
                    {
                        throw excList[0];
                    }
                }
            }

            lnkCheckOut.Visible = _order.OrderDetail.Count > 0;
            lnkCartSave.Visible = _order.OrderDetail.Count > 0;
            lnkSaveCartImg.Visible = _order.OrderDetail.Count > 0;
            lnkClearCartImg.Visible = _order.OrderDetail.Count > 0;
            lnkClearCart.Visible = _order.OrderDetail.Count > 0;

            SetTotals();

            if (_order.WeightUnit != null)
            {
                sysShipUnits.Text = _order.WeightTotal.ToString("0.00##");
                sysShipUnitName.Text = _order.WeightUnit.UnitName;
            }

            npcdlblock.Order = _order;

            if (!CommerceLicense || inValidTax)
            {
                lnkCheckOut.Visible = false;
            }

            imgOutOfStock.ImageUrl = ResolveImage("icons", "warning.gif");
            imgUnavailableOutOfStock.ImageUrl = ResolveImage("icons", "outofstock.gif");
            imgUnavailable.ImageUrl = ResolveImage("icons", "availableno.gif");
        }

        private void SetTotals()
        {
            expenses.LoadExpenses(_order);
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            NPEventArgs npa = null;
            if (args.GetType().FullName == "netpoint.classes.NPEventArgs")
            {
                npa = (NPEventArgs)args;
            }
            if (npa != null)
            {
                switch (npa.FromEvent)
                {
                    case "outofstock":
                        imgOutOfStock.Visible = true;
                        lblOutOfStock.Visible = true;
                        break;
                    case "unavailableoutofstock":
                        imgUnavailableOutOfStock.Visible = true;
                        lblUnavailableOutOfStock.Visible = true;
                        lnkCheckOut.Visible = false;
                        break;
                    case "unavailable":
                        imgUnavailable.Visible = true;
                        lblUnavailable.Visible = true;
                        lnkCheckOut.Visible = false;
                        break;
                    case "ApplyDiscount":
                        SetTotals();
                        break;
                }
            }
            return true;
        }
    }
}
