using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.api.encryption;
using netpoint.classes;
using netpoint.provider.payment;

namespace netpoint.commerce
{

    public partial class paymentauthorize : NPBasePageCommerce
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            NPOrder order = new NPOrder(UserID, SessionID);

            if (!order.Initialized || order.OrderDetail.Count == 0)
            {
                trNoOrder.Visible = true; 
                return;
            }

            if (!IsPostBack)
            {
                NPCCProvider.EnterPage(order, "cart.aspx");
            }

            order.Valid(OrderValidationType.OrderCheckout);
            //set payment cvv value
            NPOrderPayment orderPayment = (NPOrderPayment)order.Payments[0];

            Encryption enc = new Encryption(orderPayment.OrderPaymentID.ToString());
            List<PaymentError> errors = new List<PaymentError>();

            
            NPPaymentOption po = new NPPaymentOption(orderPayment.PaymentID);                

            if (TheSession.EncryptedSessionPaymentInformation[orderPayment.OrderPaymentID] != null)
            {
                try
                {
                    string[] vals = enc.Decrypt(TheSession.EncryptedSessionPaymentInformation[orderPayment.OrderPaymentID].ToString()).Split(',');
                    orderPayment.VerificationCode = vals[0].ToString();
                    orderPayment.AccountNumber = vals[1].ToString();
                    if (orderPayment.VerificationCode == "" || orderPayment.AccountNumber == "")
                    {
                        errors.Add(new PaymentError("Payment information expired", PayementErrorType.ExpiredPaymentInformation));
                        //remove payment from database
                        orderPayment.Delete(false);
                    }
                }
                catch
                {
                    errors.Add(new PaymentError("Payment information expired", PayementErrorType.ExpiredPaymentInformation));
                    //remove payment from database
                    orderPayment.Delete(false);
                }
            }
            else if (po.UseBankCardNumber)
            {
                errors.Add(new PaymentError("Payment information expired", PayementErrorType.ExpiredPaymentInformation));
                //remove payment from database
                orderPayment.Delete(false);
            }
            if (errors.Count > 0)
            {                
                ProcessErrors(errors);
            }
            else
            {
                NPCCProvider provider = new NPCCProvider();
                if (provider.Authorize(order)) 
                {
                    //log successful order
                    NPErrorLog.LogError(new Exception("Order " + order.OrderID.ToString() + " completed successfully."), UserID, "", ConfigurationManager.ConnectionStrings["connString"].ConnectionString);                
                    //remove payment infomation from session
                    TheSession.EncryptedSessionPaymentInformation[orderPayment.OrderPaymentID] = null;
                    if (order.CheckOut(Request.UserHostAddress, ServerID))
                    {
                        NPUser user = new NPUser(this.UserID);
                        if (user.ExpressCheckoutUser)
                        {
                            Response.Redirect("expressinvoice.aspx?OrderNo=" + order.OrderID.ToString());
                        }
                        else
                        {
                            Response.Redirect("confirm.aspx?OrderNo=" + order.OrderID.ToString(), true);
                        }
                    }
                }
                else
                {                    
                    TheSession.EncryptedSessionPaymentInformation[orderPayment.OrderPaymentID] = null;
                    //error from gateway so remove payment
                    orderPayment.Delete();
                }

                ProcessErrors(provider.Errors);

                //log un successful order
                NPErrorLog.LogError(new Exception("Order " + order.OrderID.ToString() + " failed. [" + sysAuthorize.Text + "]"), UserID, "", ConfigurationManager.ConnectionStrings["connString"].ConnectionString);                

            }
        }

        private void ProcessErrors(List<PaymentError> errors)
        {
            trAuthorize.Visible = true;

            sysAuthorize.Text += "<br>" + errCreditCardNotAuthorized.Text + " (";
            foreach (PaymentError pe in errors)
            {
                switch (pe.ErrorType)
                {
                    case PayementErrorType.OrderHasNoPayment:
                        sysAuthorize.Text += hdnOrderHasNoPayment.Text + "<br/>";
                        break;
                    case PayementErrorType.PaymentOptionHasNoProvider:
                        sysAuthorize.Text += hdnPaymentHasNoProvider.Text + "<br/>";
                        break;
                    case PayementErrorType.ProviderAssemblyMissing:
                        sysAuthorize.Text += hdnProviderDoesNotExist.Text + "<br/>";
                        break;
                    case PayementErrorType.ExpiredPaymentInformation:
                        sysAuthorize.Text += hdnExpiredPaymentInformation.Text + "<br/>";
                        break;
                    case PayementErrorType._errorFromPaymentProcessor:
                        sysAuthorize.Text += pe.Message + "<br/>"; // display the text of the error from the payment provider as it was sent to us
                        break;
                }
            }
            sysAuthorize.Text += ")";            
        }        
    }
}
