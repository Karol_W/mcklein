﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.api.commerce;
using netpoint.provider.payment;
using netpoint.classes;

namespace netpoint.commerce.flow3
{
    public partial class providerreturn : NPBasePageCommerce
    {
        private NPOrder _order;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _order = new NPOrder(UserID, SessionID);
                if (Request.QueryString["cancel"] != null)
                {
                    //user cancelled external payment so delete payment placeholder from database
                    foreach (NPOrderPayment op in _order.Payments)
                    {
                        op.Delete();
                    }
                }
                else
                {
                    NPCCProvider.EnterPage(_order, "~/commerce/cart.aspx");
                }
            }

            Response.Redirect("onepagecheckout.aspx", true);
        }
    }
}
