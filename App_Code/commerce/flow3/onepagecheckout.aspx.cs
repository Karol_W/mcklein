﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.api.data;
using netpoint.classes;
using NetPoint.ShippingProvider;

namespace netpoint.commerce.flow3
{
    public partial class onepagecheckout : NPBasePageCommerce
    {
        private NPOrder _order;

        protected void Page_Load(object sender, EventArgs e)
        {
            _order = new NPOrder(UserID, SessionID);
            if (!_order.Initialized || _order.OrderDetail.Count == 0 || _order.UserID == null || _order.UserID == "")
            {
                Response.Redirect("~/commerce/cart.aspx");
            }

            paymentBlock.Order = _order;
            sysErrorMessage.Text = "";

            //check for multiple taxes on a line and display error message
            try
            {
                _order.Valid(netpoint.api.OrderValidationType.OrderTaxes);
            }
            catch (Exception ex)
            {
                sysErrorMessage.Text = hdnIncorrectTax.Text;
                HideButtonPanel();
            }

            if (!IsPostBack)
            {
                btnOrder.ImageUrl = ResolveImage("buttons", "placeyourorder.gif");
                btnAddPayment.ImageUrl = ResolveImage("buttons", "addpayment.gif");
                btnPayPalOrder.ImageUrl = "https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif".ToLower();
                btnEditAddress.ImageUrl = ResolveImage("buttons", "editinfo.gif");
                btnSaveAddress.ImageUrl = ResolveImage("buttons", "save-btn-small.png");
                imgPhoneWarning.ImageUrl = ResolveImage("icons", "warning.png");
                imgRequestedShipDate.ImageUrl = ResolveImage("icons", "warning.png");
                imgOutOfStock.ImageUrl = ResolveImage("icons", "warning.png");
                imgUnavailableOutOfStock.ImageUrl = ResolveImage("icons", "warning.png");
                imgUnavailable.ImageUrl = ResolveImage("icons", "warning.png");
                imgNotes.ImageUrl = ResolveImage("icons", "warning.png");

                //check if notes should be displayed
                if (TheTheme.DisplayCheckoutNotes)
                {
                    pnlOrderNotes.Visible = true;
                }

                CartList.Order = _order;

                NPUser user = new NPUser(_order.UserID);
                txtDayPhone.Text = user.DayPhone;
                txtEveningPhone.Text = user.EveningPhone;
                sysDayPhone.Text = user.DayPhone;
                sysEveningPhone.Text = user.EveningPhone;

                rowB2B.Visible = _order.Account.AccountType.Equals("B");
                txtPO.Text = _order.PONumber;
                txtNotes.Text = _order.Notes;
            }

            if (_order.GetShipAddress() == 0 || _order.BillingAddressID == 0)
            {
                if (!this.IsPostBack)
                {
                    //don't have addresses associated with the order yet so display address entry controls
                    ToggleAddressDisplay(true);

                    SetAddressDisplay();
                }
            }
            else
            {
                if (!this.IsPostBack)
                {
                    //we have addresses so show addresses in readonly mode
                    ToggleAddressDisplay(false);

                    NPAddress shipto = new NPAddress(_order.GetShipAddress());
                    sysShippingAddress.Text = shipto.AddressHTML;

                    NPAddress billto = new NPAddress(_order.BillingAddressID);
                    sysBillingAddress.Text = billto.AddressHTML;

                    BindForm();

                    //set order property of payment block
                    paymentBlock.Order = _order;
                }

                //Add the javascript call to each radio button in the list. This allows the callback to be triggered
                foreach (ListItem Li in rblShipMethod.Items)
                {
                    Li.Attributes.Add("onclick", "selectProduct('" + Li.Value + "');");
                }

                if (!clbackTotals.CausedCallback)
                {
                    expenses.LoadExpenses(_order);
                }                
            }

            
        }

        protected void cbxUseUniqueShippingAddress_CheckedChanged(object sender, System.EventArgs e)
        {
            rowEditShipAddressHeader.Visible = cbxUseUniqueShippingAddress.Checked;
            rowEditShipAddress.Visible = cbxUseUniqueShippingAddress.Checked;

            if (cbxUseUniqueShippingAddress.Checked)
            {
                ddlShippingAddress_SelectedIndexChanged(sender, e);
            }
        }

        private void ToggleAddressDisplay(bool editMode)
        {
            if (editMode)
            {
                //show address blocks and hide shipping and payment fields
                tblEditAddress.Visible = true;
                //rowEditAddress.Visible = true;
                //rowSaveAddressButton.Visible = true;
                //rowB2B.Visible = false;     
                tblViewAddress.Visible = false;
                //rowViewAddress.Visible = false;
                //rowEditAddressButton.Visible = false;
                //pnlAddressDependantInfo.Visible = false;
                pnlAddressDependantInfo.Enabled = false;                
            }
            else
            {
                //show read-only addresses and shipping and payment fields
                tblEditAddress.Visible = false;
                //rowEditAddress.Visible = false;
                //rowSaveAddressButton.Visible = false;
                //rowB2B.Visible = true;                
                tblViewAddress.Visible = true;
                //rowViewAddress.Visible = true;
                //rowEditAddressButton.Visible = true;
                //pnlAddressDependantInfo.Visible = true;
                pnlAddressDependantInfo.Enabled = true;                
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //if selected payment is paypal, check if we have a completed payment, if so display buy button,
            //otherwsie show pay with paypal button
            if (tblViewAddress.Visible)
            {
                Panel pnlNewPayment = (Panel)paymentBlock.FindControl("pnlNew");
                if (pnlNewPayment.Visible)
                {
                    btnAddPayment.Visible = true;
                    btnPayPalOrder.Visible = false;
                    btnOrder.Visible = false;
                }
                else
                {
                    btnAddPayment.Visible = false;

                    btnPayPalOrder.Visible = false;
                    btnOrder.Visible = true;
                    if (_order.Payments.Count == 0 && paymentBlock.SelectedPaymentMethod == 10)
                    {
                        btnPayPalOrder.Visible = true;
                        btnOrder.Visible = false;
                    }
                }
            }
            else
            {
                btnAddPayment.Visible = false;

                //hide buttons in edit mode
                btnPayPalOrder.Visible = false;
                btnOrder.Visible = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrder.Click += new ImageClickEventHandler(this.btnOrder_Click);
            this.btnPayPalOrder.Click += new ImageClickEventHandler(this.btnOrder_Click);
            this.btnEditAddress.Click += new ImageClickEventHandler(btnEditAddress_Click);
            this.btnSaveAddress.Click += new ImageClickEventHandler(btnSaveAddress_Click);
        }

        #endregion

        void btnSaveAddress_Click(object sender, ImageClickEventArgs e)
        {
            Page.Validate();

            List<NPError> theErrors = new List<NPError>();

            //validate address info
            int _billAddId = BillingAddressBlock.Save(theErrors);
            if (_billAddId > 0)
            {
                _order.BillingAddressID = _billAddId;
            }

            //validate phone numbers
            bool validPhone = true;
            imgPhoneWarning.Visible = false;
            if (txtDayPhone.Text == "")
            {
                theErrors.Add(new NPError(hdnDayPhoneRequired.Text));
                imgPhoneWarning.Visible = true;
                validPhone = false;
            }
            if (validPhone)
            {
                NPUser user = new NPUser(_order.UserID);
                user.DayPhone = txtDayPhone.Text;
                user.EveningPhone = txtEveningPhone.Text;
                user.Save();

                sysDayPhone.Text = txtDayPhone.Text;
                sysEveningPhone.Text = txtEveningPhone.Text;
            }

            //check if we need to assign a shipping address automatically
            int _shipAddId = 0;

            if (!cbxUseUniqueShippingAddress.Checked)
            {
                //customer wants to use billing for shipping. Attempt to find an existing shipping address that matches or create a new one
                if (_billAddId > 0)
                {
                    //only look for or create a shipping address if billing address is valid                    

                    //make a copy of current billing address to use for our ship address
                    NPAddress shipAddress = new NPAddress(_billAddId);
                    shipAddress.AddressID = 0;
                    shipAddress.ShipDestination = true;

                    string shipAddressName = shipAddress.Name;
                    //address name could be "Name" or "Name(x)" for duplicates. Determine x so a new name can be generated if necessary
                    Regex regNameCount = new Regex("(?<name>.+)\\((?<index>\\d+)\\)$");
                    MatchCollection coll = regNameCount.Matches(shipAddressName);
                    if (coll.Count > 0 && coll[0].Groups["name"].Value != "")
                    {
                        //now name should just be the starting name without the (x)
                        shipAddressName = coll[0].Groups["name"].Value;
                    }

                    //loop over existing shipping addresses to find an existing one that will match the current billing address
                    bool addressFound = false;
                    int currentMaxDuplicateNameIndex = 0;
                    bool exactAddressNameFound = false;
                    List<NPAddress> allShipAddresses = _order.Account.GetShippingAddresses();

                    //first loop over all shipping addresses and see if there's an exact match including address name
                    foreach (NPAddress currShipAddress in allShipAddresses)
                    {
                        if (shipAddress.Equals(currShipAddress))
                        {
                            //exact match found
                            _shipAddId = currShipAddress.AddressID;
                            _order.SetAllDetailShipAddress(_shipAddId);
                            addressFound = true;
                            break;
                        }
                    }

                    //if no exact match, try finding a matching address details with a different name
                    if (!addressFound)
                    {
                        foreach (NPAddress currShipAddress in allShipAddresses)
                        {
                            //copy name to new address so we know that matches
                            shipAddress.Name = currShipAddress.Name;

                            if (shipAddress.Equals(currShipAddress))
                            {
                                //exact match found with exception of name
                                _shipAddId = currShipAddress.AddressID;
                                _order.SetAllDetailShipAddress(_shipAddId);
                                addressFound = true;
                                break;
                            }
                            else
                            {
                                //address doesn't match so now run logic to possibly handle duplicate names                               

                                //address name could be "Name" or "Name(x)" for duplicates. Determine x so a new name can be generated                            
                                string tmpShipAddressName = currShipAddress.Name;
                                coll = regNameCount.Matches(tmpShipAddressName);
                                if (coll.Count > 0 && coll[0].Groups["name"].Value != "")
                                {
                                    //now name should just be the starting name without the (x)
                                    tmpShipAddressName = coll[0].Groups["name"].Value;

                                    if (tmpShipAddressName == shipAddressName)
                                    {
                                        exactAddressNameFound = true;
                                        int idx = Int32.Parse(coll[0].Groups["index"].Value);
                                        if (idx > currentMaxDuplicateNameIndex)
                                        {
                                            //set current max index found to new index since it's the largest found yet
                                            currentMaxDuplicateNameIndex = idx;
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (!addressFound)
                    {
                        //didn't find an existing shipping address so save the new one
                        if (exactAddressNameFound)
                        {
                            //duplicate name exists so modify name to use maximum duplicate index
                            shipAddress.Name = shipAddressName + "(" + ++currentMaxDuplicateNameIndex + ")";
                        }
                        else
                        {
                            //set address name to same as billing
                            shipAddress.Name = shipAddressName;
                        }
                        shipAddress.Save();
                        if (shipAddress.AddressID > 0)
                        {
                            _shipAddId = shipAddress.AddressID;
                            _order.SetAllDetailShipAddress(_shipAddId);
                        }
                    }

                }
            }
            else
            {
                _shipAddId = ShippingAddressBlock.Save(theErrors);
                if (_shipAddId > 0)
                {
                    _order.SetAllDetailShipAddress(_shipAddId);
                }
            }

            if (!DisplayErrors(theErrors) && Page.IsValid && _billAddId > 0 && _shipAddId > 0)
            {

                //need to calculate for taxes, shipping costs, etc
                _order.Calculate();

                NPAddress shipto = new NPAddress(_order.GetShipAddress());
                sysShippingAddress.Text = shipto.AddressHTML;

                NPAddress billto = new NPAddress(_order.BillingAddressID);
                sysBillingAddress.Text = billto.AddressHTML;

                BindForm();

                //Add the javascript call to each radio button in the list. This allows the callback to be triggered
                foreach (ListItem Li in rblShipMethod.Items)
                {
                    Li.Attributes.Add("onclick", "selectProduct('" + Li.Value + "');");
                }

                expenses.LoadExpenses(_order);

                //set order property of payment block
                paymentBlock.Order = _order;

                ToggleAddressDisplay(false);
            }
        }

        void btnEditAddress_Click(object sender, ImageClickEventArgs e)
        {
            ToggleAddressDisplay(true);

            SetAddressDisplay();
        }
        

        protected void SetAddressDisplay()
        {            
            //only show address dropdown if customer has existing addresses
            ArrayList billAddresses = NPAddress.GetAddresses(_order.AccountID, false, true, true, hdnNewAddress.Text, ConnectionString);
            ddlBillingAddress.DataSource = billAddresses;
            ddlBillingAddress.DataValueField = "AddressID";
            ddlBillingAddress.DataBind();
            if (billAddresses.Count == 1)
            {                    
                ddlBillingAddress.Visible = false;
            }
            else
            {
                //set address display based on saved address for order
                int s = _order.BillingAddressID;
                if (s > 0)
                {
                    ddlBillingAddress.SelectedIndex = ddlBillingAddress.Items.IndexOf(ddlBillingAddress.Items.FindByValue(s.ToString()));
                }
                else if (ddlBillingAddress.SelectedIndex == -1 && ddlBillingAddress.Items.Count > 0)
                {
                    ddlBillingAddress.SelectedIndex = 1;
                }                
            }

            ddlBillingAddress_SelectedIndexChanged(this, null);

            //only show address dropdown if customer has existing addresses            
            ArrayList shipAddresses = NPAddress.GetAddresses(_order.AccountID, true, true, hdnNewAddress.Text, ConnectionString);
            ddlShippingAddress.DataSource = shipAddresses;
            ddlShippingAddress.DataValueField = "AddressID";
            ddlShippingAddress.DataBind();
            if (shipAddresses.Count == 1)
            {                    
                ddlShippingAddress.Visible = false;
            }
            else
            {
                int s = _order.GetShipAddress();
                if (s > 0)
                {
                    ddlShippingAddress.SelectedIndex = ddlShippingAddress.Items.IndexOf(ddlShippingAddress.Items.FindByValue(s.ToString()));
                }
                else if (ddlShippingAddress.SelectedIndex == -1 && ddlShippingAddress.Items.Count > 0)
                {
                    ddlShippingAddress.SelectedIndex = 1;
                }                
            }

            ddlShippingAddress_SelectedIndexChanged(this, null);
        }

        private void BindForm()
        {
            try
            {
                //Updated method call to included address parameters used for zone filtering
                NPAddress shipto = new NPAddress(_order.GetShipAddress(), base.Connection);
                NPListBinder.PopulateRateList(rblShipMethod, "S", _order.ShippingMethod.ToString(), false, true, true, _order.SubTotal, _order.WeightTotal, _order.WeightTotalUnitCode, UserID, true, shipto.CountryAbbr, shipto.StateAbbr, shipto.PostalCode);

                if (rblShipMethod.SelectedIndex == -1 && rblShipMethod.Items.Count > 0)
                {
                    rblShipMethod.SelectedIndex = 0;
                    _order.SetAllDetailShipMethod(Convert.ToInt32(rblShipMethod.SelectedValue));
                }
                if (rblShipMethod.Items.Count == 0)
                {
                    sysErrorMessage.Text = errNoShipping.Text + "<br>" + errConfigureShipping.Text;
                    //TODO is this correct to hide both these buttons
                    HideButtonPanel();
                }

                //Code called on the shipping aspx page
                int shipMethod = 0;
                int.TryParse(rblShipMethod.SelectedValue, out shipMethod);
                _order.ShippingMethod = shipMethod;
                List<Exception> excList = _order.Calculate();
                //only consider results if not a callback, if it is a callback it will be handled there
                if (!clbackTotals.IsCallback)
                {
                    if (excList.Count > 0)
                    {
                        //check first exception in list
                        if (excList[0] is ShippingProviderException)
                        {
                            lblShippingError.Text = excList[0].Message;
                            if (excList[0].InnerException != null) lblShippingError.Text += excList[0].InnerException.Message;
                            HideButtonPanel();
                            //Log the error so it can be tracked
                            NPErrorLog.LogError(excList[0], UserID, "flow3/onepagecheckout.aspx", ConnectionString);
                            _order.ShippingTotal = 0;
                            _order.Save(false);
                        }
                        else
                        {
                            throw excList[0];
                        }
                    }
                }
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblShippingError.Text = ex.Message;
                if (ex.InnerException != null) lblShippingError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "flow3/onepagecheckout.aspx", ConnectionString);
            }
        }

        protected void ddlBillingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int billaddid = 0;
            try
            {
                billaddid = Convert.ToInt32(ddlBillingAddress.SelectedValue);
            }
            catch { }

            BillingAddressBlock.AddressID = billaddid;
        }

        protected void ddlShippingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int shipaddid = 0;
            try
            {
                shipaddid = Convert.ToInt32(ddlShippingAddress.SelectedValue);
            }
            catch { }

            ShippingAddressBlock.AddressID = shipaddid;
        }

        /// <summary>
        /// Triggered by the on click event of the radio button list. Dynamically refreshes the totals.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void clbackTotals_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            try
            {
                ShipMethodChanged(rblShipMethod, e.Parameter);
                rblShipMethod.SelectedValue = e.Parameter;
                //Reload the Expenses Section to update
                expenses.LoadExpenses(_order);

                paymentBlock.BindAll();

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblShippingError.Text = ex.Message;
                if (ex.InnerException != null) lblShippingError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "flow3/onepagecheckout.aspx", ConnectionString);

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }

        }                

        private void ShipMethodChanged(ListControl lc, string selectVal)
        {
            //_order.ShippingMethod = Convert.ToInt32(lc.SelectedValue);
            _order.ShippingMethod = Convert.ToInt32(selectVal);

            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }

            List<Exception> excList = _order.Calculate();
            if (excList.Count > 0)
            {
                //check first exception in list
                if (excList[0] is ShippingProviderException)
                {
                    lblShippingError.Text = excList[0].Message;
                    if (excList[0].InnerException != null) lblShippingError.Text += excList[0].InnerException.Message;
                    HideButtonPanel();
                    //Log the error so it can be tracked
                    NPErrorLog.LogError(excList[0], UserID, "flow3/onepagecheckout.aspx", ConnectionString);
                    _order.ShippingTotal = 0;
                    _order.Save(false);
                }
                else
                {
                    throw excList[0];
                }
            }

            //reload payments so new order total can be applied if a payment already exists
            if (_order.Payments.Count > 0)
            {
                paymentBlock.AddPayment();
            }

        }

        private void HideButtonPanel()
        {
            btnOrder.Visible = false;
            btnOrder.Enabled = false;
            btnOrder.Width = 0;
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            NPEventArgs npa = null;
            if (args.GetType().FullName == "netpoint.classes.NPEventArgs")
            {
                npa = (NPEventArgs)args;
            }

            if (npa != null)
            {
                switch (npa.FromEvent)
                {
                    case "EmptyCart":
                        Response.Redirect("~/commerce/cart.aspx");
                        break;
                    case "outofstock":
                        imgOutOfStock.Visible = true;
                        lblOutOfStock.Visible = true;
                        break;
                    case "unavailableoutofstock":
                        imgUnavailableOutOfStock.Visible = true;
                        lblUnavailableOutOfStock.Visible = true;
                        HideButtonPanel();
                        break;
                    case "unavailable":
                        imgUnavailable.Visible = true;
                        lblUnavailable.Visible = true;
                        HideButtonPanel();
                        break;
                }
            }
            else if (args is CommandEventArgs)
            {
                CommandEventArgs cea = (CommandEventArgs)args;
                if (cea.CommandName == "balance")
                {
                    //came from payment block after adding or removing a payment
                    //reload expenses to handle payment charges
                    expenses.LoadExpenses(_order);
                }
            }
            return true;
        }

        public void AddPayment(object sender, CommandEventArgs e)
        {
            if (paymentBlock.SubmitPayment())
            {
                paymentBlock.AddPayment();

                paymentBlock.BindAll();
            }
        }

        private void btnOrder_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            List<NPError> theErrors = new List<NPError>();

            if (!CheckAndSetRequestedShipDateAndPO(_order))
            {
                return;
            }

            if (!_order.PaymentTotal.ToString("c").Equals(_order.GrandTotal.ToString("c")))
            {
                theErrors.Add(new NPError("Total Payments must match the Total of the Order", NPErrorType.ORDR_PaymentNotTotal));
            }

            if (Page.IsValid && !DisplayErrors(theErrors))
            {
                string nextPage;
                if ((nextPage = GetFlow("checkout")) != null)
                {
                    Response.Redirect(nextPage, true);
                }
                else
                {
                    NPCCProvider.ExitPage(_order, "~/commerce/paymentauthorize.aspx", "~/commerce/cart.aspx");
                }
            }
        }

        private bool CheckAndSetRequestedShipDateAndPO(NPOrder _order)
        {
            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
                return false;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }
            if (txtNotes.Text.Length > txtNotes.LimitInput)
            {
                imgNotes.Visible = true;
                return false;
            }
            else
            {
                imgNotes.Visible = false;
                _order.Notes = txtNotes.Text;
            }
            _order.PONumber = txtPO.Text;
            _order.Save();
            return true;
        }

        private bool DisplayErrors(List<NPError> theErrors)
        {
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.ORDR_PaymentNotTotal:
                        sysErrorMessage.Text += errTotalPayments.Text += "<br/>";
                        break;
                    case NPErrorType.ADDR_DuplicateNameBilling:
                        sysErrorMessage.Text += errDuplicationAddressNameBilling.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_DuplicateNameShipping:
                        sysErrorMessage.Text += errDuplicationAddressNameShipping.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_AddressNameIsRequired:
                        sysErrorMessage.Text += hdnAddressNameRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_CityIsRequired:
                        sysErrorMessage.Text += hdnCityIsRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_PostalCodeIsRequired:
                        sysErrorMessage.Text += hdnPostalCodeRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_Street1IsRequired:
                        sysErrorMessage.Text += hdnStreet1Required.Text + "<br/>";
                        break;
                    default:
                        sysErrorMessage.Text += npe.Message + "<br/>";
                        break;

                }
            }
            return theErrors.Count > 0;
        }
    }
}
