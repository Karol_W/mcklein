namespace netpoint.common.controls
{
    using System;
    using System.Configuration;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api;
    using netpoint.api.account;
    using netpoint.api.common;
    using netpoint.classes;
    using netpoint.api.data;
    using NetPoint.WebControls;
    using System.Resources;
    using netpoint.api.encryption;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class AddressBlockMobile : NPBaseControl
    {

        #region "Members and Properties"

        public bool ShowTextOnSet = false;
        private bool _adminMode = false;
        private bool _hideAddressType = false;
        //Add private member to hold UserID for attention text box control
        private string _userID;


        protected Label addressNameDuplicateMessage;
        protected Label addressNameEmptyMessage;

        public string UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                NPUser user = new NPUser(_userID);

                if (user.ExpressCheckoutUser == false)
                {
                    txtAttention.Text = _userID;
                }
            }
        }
        public Unit BorderWidth
        {
            set
            {
            }
        }

        public int AddressID
        {
            set
            {
                sysAddressID.Text = value.ToString();
                NPAddress a = new NPAddress(value);
                BindControl(a);
            }
            get
            {
                try
                {
                    return Convert.ToInt32(sysAddressID.Text);
                }
                catch
                {
                    return 0;
                }
            }
        }

        public bool ShipDestination
        {
            get { return sysShipDestination.Checked; }
            set
            {
                sysShipDestination.Checked = value;
                if (value)
                {
                    lblAddressType.Text = hdnShipping.Text;
                }
                else
                {
                    lblAddressType.Text = hdnBilling.Text;
                }
            }
        }

        public string AccountID
        {
            get { return ViewState["AccountID"] != null ? Convert.ToString(ViewState["AccountID"]) : null; }
            set { ViewState["AccountID"] = value; }
        }

        /// <summary>
        /// Controls visibility of save button
        /// </summary>
        public bool ShowSave
        {
            get { return btnSave.Visible; }
            set { btnSave.Visible = value; }
        }

        public bool AdminMode
        {
            get { return _adminMode; }
            set { _adminMode = value; }
        }

        public bool HideAddressType
        {
            get { return _hideAddressType; }
            set { _hideAddressType = value; }
        }

        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            imgAddressNameWarning.Visible = false;
            imgZipWarning.Visible = false;
            imgStreet1Warning.Visible = false;
            imgCityWarning.Visible = false;
            if (AdminMode)
            {
                btnSave.ImageUrl = BasePage.ResolveImage("icons", "save.gif");
            }
            else 
            {
                rowBlock.Visible = false;
                rowCounty.Visible = false;
                btnSave.ImageUrl = BasePage.ResolveImage("buttons", "saveinfo.gif");                
            }
            if (HideAddressType) 
            {
                rowAddressType.Visible = false;
            }

            //imgStreet1Warning.ImageUrl = BasePage.ResolveImage("icons", "warning.gif");
            //imgCityWarning.ImageUrl = BasePage.ResolveImage("icons", "warning.gif");
            //imgZipWarning.ImageUrl = BasePage.ResolveImage("icons", "warning.gif");
            //imgAddressNameWarning.ImageUrl = BasePage.ResolveImage("icons", "warning.gif");
        }

        public void SetEmptyDefault()
        {
            try
            {
                NPUser user = new NPUser(BasePage.UserID);
                txtAttention.Text = user.GetFormattedName(NameFormat.FullName);
                BindCountryList();
                NPListBinder.PopulateStateAbbrList(ddlState, "", ddlCountry.SelectedValue, new NPConnection(NPConnection.GblConnString), ShipDestination, !ShipDestination);
                rowState.Visible = (ddlState.Items.Count > 0);
            }
            catch (Exception E)
            {
                string devnull = E.Message;
            }
        }

        private void BindCountryList()
        {
            if (AdminMode)
            {
                //show all available countries in admin
                NPListBinder.PopulateCountryList(ddlCountry, BasePage.Encoding.Substring(BasePage.Encoding.Length - 2, 2).ToUpper(), false, false, BasePage.Connection);
            }
            else
            {
                if (ShipDestination)
                {
                    NPListBinder.PopulateCountryList(ddlCountry, BasePage.Encoding.Substring(BasePage.Encoding.Length - 2, 2).ToUpper(), true, false, BasePage.Connection);
                }
                else
                {
                    NPListBinder.PopulateCountryList(ddlCountry, BasePage.Encoding.Substring(BasePage.Encoding.Length - 2, 2).ToUpper(), false, true, BasePage.Connection);
                }
            }
            if (ddlCountry.Items.Count <= 0)
            {
                if (ShipDestination)
                {
                    sysError.Text = errShip.Text;
                }
                else
                {
                    sysError.Text = errBill.Text;
                }
            }
        }

        public void LoadAddress(NPAddress address)
        {
            BindControl(address);
        }

        private void BindControl(NPAddress a)
        {
            sysAddressHTML.Text = a.AddressHTML;
            ViewPanel.Visible = false;
            sysError.Text = "";
            if (ddlState.Items.Count == 0 || a.AddressID == 0)
            {
                SetEmptyDefault();
            }

            sysAddressID.Text = a.AddressID.ToString();
            txtAddressName.Text = a.Name;
            txtInCareOf.Text = a.InCareOf;
            txtStreet1.Text = a.Street1;
            txtStreet2.Text = a.Street2;
            txtBlock.Text = a.Block;
            txtCity.Text = a.City;
            txtPostalCode.Text = a.PostalCode;
            if (a.Initialized)
            {
                txtAttention.Text = a.Attention;
                if (a.CountryAbbr != ddlCountry.SelectedValue)
                {
                    NPListBinder.PopulateStateAbbrList(ddlState, "", a.CountryAbbr, new NPConnection(NPConnection.GblConnString), ShipDestination, !ShipDestination);
                }
                int _countryIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(a.CountryAbbr));
                
                if (_countryIndex == -1)
                {
                    //country not found in current list. Check that it is a valid country and if so add just that one
                    NPCountry newCtry = new NPCountry(a.CountryAbbr, BasePage.Encoding, BasePage.ConnectionString);
                    if (newCtry.Initialized)
                    {
                        bool added = false;
                        for (int i = 0; i < ddlCountry.Items.Count; i++)
                        {
                            if (ddlCountry.Items[i].Text.CompareTo(newCtry.CountryName) > 0)
                            {
                                ddlCountry.Items.Insert(i, new ListItem(newCtry.CountryName, a.CountryAbbr));
                                added = true;
                                break;
                            }
                        }
                        if (!added)
                        {
                            //add at end
                            ddlCountry.Items.Add(new ListItem(newCtry.CountryName, a.CountryAbbr));
                        }

                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(a.CountryAbbr));
                    }
                }
                else
                {
                    ddlCountry.SelectedIndex = _countryIndex;
                }
                ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(a.StateAbbr));
                rowState.Visible = (ddlState.Items.Count > 0);
                ShipDestination = a.ShipDestination;
            }
            txtCounty.Text = a.County;
            if (ShowTextOnSet && a.AddressID != 0)
            {
                EditPanel.Visible = false;
                ViewPanel.Visible = true;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave.Click += new System.Web.UI.ImageClickEventHandler(this.btnSave_Click);
        }
        #endregion

        protected void ddlCountry_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            NPListBinder.PopulateStateAbbrList(ddlState, "", ddlCountry.SelectedValue, new NPConnection(NPConnection.GblConnString), ShipDestination, !ShipDestination);
            rowState.Visible = (ddlState.Items.Count > 0);
        }

        private void btnSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            List<NPError> theErrors = new List<NPError>();
            Save(theErrors);
            NPEventArgs ea = new NPEventArgs("AddressBlock_Save", theErrors);
            this.RaiseBubbleEvent(this, ea);            
        }

        /// <summary>
        /// Saves data on the screen and returns the addressID or 0 if save failed.
        /// </summary>
        /// <returns>AddressID or 0 if save fails</returns>
        public int Save(List<NPError> theErrors)
        {            
            NPAddress a = new NPAddress(AddressID);
            if (!AddressChanged(a))
                return AddressID;
            a.Name = txtAddressName.Text;
            a.Attention = txtAttention.Text;
            a.InCareOf = txtInCareOf.Text;
            a.Street1 = txtStreet1.Text;
            a.Street2 = txtStreet2.Text;
            a.Block = txtBlock.Text;
            a.City = txtCity.Text;
            a.StateAbbr = ddlState.SelectedValue;
            a.PostalCode = txtPostalCode.Text;
            a.CountryAbbr = ddlCountry.SelectedValue;
            a.CountryName = ddlCountry.Items[ddlCountry.SelectedIndex].Text;
            a.County = txtCounty.Text;
            a.ShipDestination = sysShipDestination.Checked;
            if (AddressID == 0)
            {
                a.AccountID = AccountID != null ? AccountID : BasePage.AccountID;
            }
            
            sysError.Text = "";
            if (a.Valid(theErrors))
            {
                a.Save();
                BindControl(a);

                return a.AddressID;
            }
            ToggleErrorFlags(theErrors);
            return 0;
        }

        protected bool AddressChanged(NPAddress a)
        {
            if (a.AddressID == 0)
            {
                return true;
            }
            else
            {
                if (a.Name != txtAddressName.Text) return true;
                if (a.Attention != txtAttention.Text) return true;
                if (a.InCareOf != txtInCareOf.Text) return true;
                if (a.Street1 != txtStreet1.Text) return true;
                if (a.Street2 != txtStreet2.Text) return true;
                if (a.Block != txtBlock.Text) return true;
                if (a.City != txtCity.Text) return true;
                if (a.StateAbbr != ddlState.SelectedValue) return true;
                if (a.PostalCode != txtPostalCode.Text) return true;
                if (a.CountryAbbr != ddlCountry.SelectedValue) return true;
                if (a.CountryName != ddlCountry.Items[ddlCountry.SelectedIndex].Text) return true;
                if (a.County != txtCounty.Text) return true;
                if (a.ShipDestination != sysShipDestination.Checked) return true;
                if (a.AccountID != AccountID) return true;
                return false;
            }
        }

        private void ToggleErrorFlags(List<NPError> theErrors)
        {
            imgAddressNameWarning.Visible = false;
            imgAddressNameWarning.Visible = (txtAddressName.Text.Trim().Length == 0);
            imgStreet1Warning.Visible = (txtStreet1.Text.Trim().Length == 0);
            imgCityWarning.Visible = (txtCity.Text.Trim().Length == 0);
            imgZipWarning.Visible = (txtPostalCode.Text.Trim().Length == 0);          
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.ADDR_DuplicateNameShipping:
                    case NPErrorType.ADDR_DuplicateNameBilling:
                        imgAddressNameWarning.Visible = true;
                        addressNameDuplicateMessage.Visible = true;
                        addressNameEmptyMessage.Visible = false;
                        break;
                }
            }
        }
    }
}
