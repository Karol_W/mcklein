using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using netpoint.api.data;
using netpoint.api.commerce;
using netpoint.api.account;
using netpoint.api.common;
using netpoint.api.fulfillment;
using netpoint.api.catalog;
using System.Text.RegularExpressions;
using netpoint.api;
using netpoint.classes;
using netpoint.api.encryption;
using NetPoint.ShippingProvider;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary description for shipping.
    /// </summary>
    public partial class shipping : NPBasePageCommerce
    {

        private NPOrder _order;

        protected void Page_Load(object sender, System.EventArgs e)
        {            
            _order = new NPOrder(UserID, SessionID);
         
            if (!_order.Initialized)
            {
                Response.Write("Critical Error : No Order was found.");
                Response.End();
            }

            sysError.Text = "";
            if (!this.IsPostBack)
            {
                topimage.ImageUrl = ResolveImage("images", "checkout1.gif");
                lnkNext.ImageUrl = ResolveImage("buttons", "continue.gif");
                AddAddress.ShowSave = false;

                BindForm();
                NPUser user = new NPUser(_order.UserID);
                txtDayPhone.Text = user.DayPhone;
                txtEveningPhone.Text = user.EveningPhone;
                //ltlRequestedShipDate.Visible = _order.Account.AccountType.Equals("B");
                //dtRequestedShipDate.Visible = _order.Account.AccountType.Equals("B");                
            }
            else
            {
                NPUser user = new NPUser(_order.UserID);
                user.DayPhone = txtDayPhone.Text;
                user.EveningPhone = txtEveningPhone.Text;
                user.Save();
            }
        }

        private void BindAddress(ListControl lc)
        {
            ArrayList al = NPAddress.GetAddresses(_order.AccountID, true, true, hdnNewAddress.Text, ConnectionString);
            lc.DataSource = al;
            lc.DataValueField = "AddressID";
            lc.DataBind();
            int s = _order.GetShipAddress();
            if (s > 0)
            {
                lc.SelectedIndex = lc.Items.IndexOf(lc.Items.FindByValue(s.ToString()));
            }
            else if (lc.SelectedIndex == -1 && lc.Items.Count > 0)
            {
                lc.SelectedIndex = 1;
            }
        }

        private void BindForm()
        {
            NPAccount account = new NPAccount(_order.AccountID);            

            BindAddress(rblShippingAddress);
            rblShippingAddress_SelectedIndexChanged(this, null);
            BindAddress(ddlShippingAddress);
            ddlShippingAddress_SelectedIndexChanged(this, null);

            if (account.GetShippingAddresses().Count == 0)
            {
                ltlShippingAddressCheckBox.Visible = false;
                rblShippingAddress.Visible = false;
                ltlShippingAddressDropDown.Visible = false;
                ddlShippingAddress.Visible = false;
            }

            //NPListBinder.PopulateRateList(rblShipMethod, "S", _order.ShippingMethod.ToString(), false, true, true, _order.SubTotal, _order.WeightTotal, UserID);
            //if (rblShipMethod.SelectedIndex == -1 && rblShipMethod.Items.Count > 0)
            //{
            //    rblShipMethod.SelectedIndex = 0;
            //    _order.SetAllDetailShipMethod(Convert.ToInt32(rblShipMethod.SelectedValue));
            //}
            //if (rblShipMethod.Items.Count == 0)
            //{
            //    sysError.Text = errNoShipping.Text + "<br>" + errConfigureShipping.Text;
            //    lnkNext.Visible = false;
            //}

            //dtRequestedShipDate.SelectedDate = _order.RequestedShipDate;

            //int shipMethod = 0;
            //int.TryParse(rblShipMethod.SelectedValue, out shipMethod);
            //_order.ShippingMethod = shipMethod;

            try
            {
                _order.Calculate();
            }
            catch (ShippingProviderException ex)
            {
                //Skip shipping exceptions as shipping will be resolved later in the checkout process.
            }
        }        

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            CommandEventArgs cea = (CommandEventArgs)args;
            if (cea != null)
            {
                if (cea.CommandName.Equals("AddAddress"))
                {
                    if (this.IsPostBack)
                    {
                        _order.SetAllDetailShipAddress(Convert.ToInt32(cea.CommandArgument));
                        //MOVED TO VERIFY.ASPX CURTIS T
                        //if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
                        //{
                        //    imgRequestedShipDate.Visible = true;
                        //}
                        //else
                        //{
                        //    imgRequestedShipDate.Visible = false;
                        //    _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
                        //}
                        try
                        {
                            _order.Calculate();
                        }
                        catch (ShippingProviderException ex)
                        {
                            //Skip shipping exceptions as shipping will be resolved later in the checkout process.
                        }
                        Response.Redirect("shipping.aspx", true);
                    }
                }
            }
            return true;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkNext.Click += new System.Web.UI.ImageClickEventHandler(this.lnkNext_Click);

        }
        #endregion

        protected void rblShippingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            AddressChanged(rblShippingAddress);
        }

        private void AddressChanged(ListControl lc)
        {
            int shipaddid = 0;
            try
            {
                shipaddid = Convert.ToInt32(lc.SelectedValue);
            }
            catch { }
            _order.SetAllDetailShipAddress(shipaddid);
            AddAddress.AddressID = shipaddid;
            //MOVED TO VERIFY.ASPX CURTIS T
            //if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            //{
            //    imgRequestedShipDate.Visible = true;
            //}
            //else
            //{
            //    imgRequestedShipDate.Visible = false;
            //    _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            //}
            try
            {
                _order.Calculate();
            }
            catch (ShippingProviderException ex)
            {
                //Skip shipping exceptions as shipping will be resolved later in the checkout process.
            }
        }

        //MOVED TO VERIFY.ASPX CURTIS T
        //protected void rblShipMethod_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    ShipMethodChanged(rblShipMethod);
        //}

        //MOVED TO VERIFY.ASPX CURTIS T
        //private void ShipMethodChanged(ListControl lc)
        //{
        //    _order.ShippingMethod = Convert.ToInt32(lc.SelectedValue);
        
        //    if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
        //    {
        //        imgRequestedShipDate.Visible = true;
        //    }
        //    else
        //    {
        //        imgRequestedShipDate.Visible = false;
        //        _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
        //    }
            
        //    _order.Calculate();            
        //}

        private void lnkNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Page.Validate();
            imgPhoneWarning.Visible = false;
            bool noAddress = !rblShippingAddress.Visible;
            List<NPError> theErrors = new List<NPError>();
            int addID = AddAddress.Save(theErrors);

            if (addID > 0)
            {
                _order.SetAllDetailShipAddress(addID);
                //MOVED TO VERIFY.ASPX CURTIS T
                //if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
                //{
                //    imgRequestedShipDate.Visible = true;
                //}
                //else
                //{
                //    imgRequestedShipDate.Visible = false;
                //    _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
                //}
                try
                {
                    _order.Calculate();
                }
                catch (ShippingProviderException ex)
                {
                    //Skip shipping exceptions as shipping will be resolved later in the checkout process.
                }
            }

            //validate taxes on the order
            try
            {
                _order.Valid(netpoint.api.OrderValidationType.OrderTaxes);
            }
            catch (Exception ex)
            {
                if (ex is NPError)
                {
                    theErrors.Add((NPError)ex);
                }
            }

            if (!DisplayErrors(theErrors))
            {
                Save(addID);
            }
        }

        private bool DisplayErrors(List<NPError> theErrors)
        {
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.ADDR_DuplicateNameShipping:
                        sysError.Text += errDuplicationAddressNameShipping.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_AddressNameIsRequired:
                        sysError.Text += hdnAddressNameRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_CityIsRequired:
                        sysError.Text += hdnCityIsRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_PostalCodeIsRequired:
                        sysError.Text += hdnPostalCodeRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_Street1IsRequired:
                        sysError.Text += hdnStreet1Required.Text + "<br/>";
                        break;
                    case NPErrorType.ORDR_MoreThanOneTaxOnLineOrShipping:
                        sysError.Text += hdnIncorrectTax.Text + "<br/>";
                        break;
                    default:
                        throw npe;
                }
            }
            return theErrors.Count > 0;
        }

        private void Save(int addID)
        {
            if (txtDayPhone.Text == "")
            {
                imgPhoneWarning.Visible = true;
                return;
            }
            //MOVED TO VERIFY.ASPX CURTIS T
            //if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            //{
            //    imgRequestedShipDate.Visible = true;
            //    return;
            //}
            //else
            //{
            //    imgRequestedShipDate.Visible = false;
            //    _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            //}
            try
            {
                _order.Calculate();
            }
            catch (ShippingProviderException ex)
            {
                //Skip shipping exceptions as shipping will be resolved later in the checkout process.
            }
            
            if (Page.IsValid && addID != 0)
            {
                string nextPage;
                if ((nextPage = GetFlow("checkout")) != null)
                {
                    Response.Redirect(nextPage, true);
                }
                else
                {
                    Response.Redirect("billing.aspx", true);
                }
            }
        }        

        protected void ddlShippingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            AddressChanged(ddlShippingAddress);
        }
    }
}
