using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.data;
using netpoint.api.commerce;
using netpoint.classes;
using NetPoint.WebControls;
using netpoint.api.catalog;
using NetPoint.ShippingProvider;
using netpoint.api;

namespace netpoint.catalog
{
    public partial class repair : NPBasePage
    {

        private NPOrder _order;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _order = new NPOrder(UserID, SessionID, Connection);
            _order.RemoveAllDetail();
            NPOrder otherOrder = new NPOrder(int.Parse(Request.QueryString.Get("orderId")));
            int id = NPOrder.CreateOrderFromHistory(int.Parse(Request.QueryString.Get("orderId")), UserID, AccountID, SessionID, "C", NPPartPricingMaster.DefaultPricelist(Connection));
            _order = new NPOrder(id);
            foreach(NPOrderOrderDiscount disc in otherOrder.GetPresentedCoupons())
            {
                NPOrderOrderDiscount ooDiscount = new NPOrderOrderDiscount(_order.OrderID, disc.DiscountID, Connection);
                ooDiscount.Save();
                _order.GetPresentedCoupons().Add(ooDiscount);
            }
            _order.Save();
            
            Response.Redirect("~/commerce/cart.aspx");
            
        }
            
    }
}
