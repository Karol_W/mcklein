﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.classes;

namespace netpoint.commerce.flow2
{
    public partial class address : NPBasePageCommerce
    {
        private NPOrder _order;

        protected void Page_Load(object sender, EventArgs e)
        {
            _order = new NPOrder(UserID, SessionID);
            
            if (!_order.Initialized || _order.OrderDetail.Count == 0 || _order.UserID == null || _order.UserID == "")
            {
                Response.Redirect("~/commerce/cart.aspx");
            }

            sysError.Text = "";

            if (!this.IsPostBack)
            {            
                lnkNext.ImageUrl = ResolveImage("buttons", "continue.gif");
                imgPhoneWarning.ImageUrl = ResolveImage("icons", "warning.gif");

                NPUser user = new NPUser(_order.UserID);
                txtDayPhone.Text = user.DayPhone;
                txtEveningPhone.Text = user.EveningPhone;
            }            

            SetAddressDisplay();
        }

        protected void SetAddressDisplay()
        {
            if (!IsPostBack)
            {
                //only show address dropdown if customer has existing addresses
                ArrayList billAddresses = NPAddress.GetAddresses(_order.AccountID, false, true, true, hdnNewAddress.Text, ConnectionString);
                ddlBillingAddress.DataSource = billAddresses;
                ddlBillingAddress.DataValueField = "AddressID";
                ddlBillingAddress.DataBind();
                if (billAddresses.Count == 1)
                {
                    ltlBillingAddressDropDown.Visible = false;
                    ddlBillingAddress.Visible = false;
                }
                else
                {
                    //set address display based on saved address for order
                    int s = _order.BillingAddressID;
                    if (s > 0)
                    {
                        ddlBillingAddress.SelectedIndex = ddlBillingAddress.Items.IndexOf(ddlBillingAddress.Items.FindByValue(s.ToString()));
                    }
                    else if (ddlBillingAddress.SelectedIndex == -1 && ddlBillingAddress.Items.Count > 0)
                    {
                        ddlBillingAddress.SelectedIndex = 1;
                    }                    
                }

                ddlBillingAddress_SelectedIndexChanged(this, null);

                //only show address dropdown if customer has existing addresses            
                ArrayList shipAddresses = NPAddress.GetAddresses(_order.AccountID, true, true, hdnNewAddress.Text, ConnectionString);
                ddlShippingAddress.DataSource = shipAddresses;
                ddlShippingAddress.DataValueField = "AddressID";
                ddlShippingAddress.DataBind();
                if (shipAddresses.Count == 1)
                {
                    ltlShippingAddressDropDown.Visible = false;
                    ddlShippingAddress.Visible = false;
                }
                else
                {
                    int s = _order.GetShipAddress();
                    if (s > 0)
                    {
                        ddlShippingAddress.SelectedIndex = ddlShippingAddress.Items.IndexOf(ddlShippingAddress.Items.FindByValue(s.ToString()));
                    }
                    else if (ddlShippingAddress.SelectedIndex == -1 && ddlShippingAddress.Items.Count > 0)
                    {
                        ddlShippingAddress.SelectedIndex = 1;
                    }                    
                }

                ddlShippingAddress_SelectedIndexChanged(this, null);

                //only show shipping address display if customer clicked checkbox to use a different address
                cbxUseUniqueShippingAddress_CheckedChanged(null, null);
            }            
        }

        protected void ddlBillingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int billaddid = 0;
            try
            {
                billaddid = Convert.ToInt32(ddlBillingAddress.SelectedValue);
            }
            catch { }

            BillingAddressBlock.AddressID = billaddid;            
        }

        protected void ddlShippingAddress_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int shipaddid = 0;
            try
            {
                shipaddid = Convert.ToInt32(ddlShippingAddress.SelectedValue);
            }
            catch { }

            ShippingAddressBlock.AddressID = shipaddid;            
        }

        protected void cbxUseUniqueShippingAddress_CheckedChanged(object sender, System.EventArgs e)
        {
            pnlShippingAddress.Visible = cbxUseUniqueShippingAddress.Checked;
            if (pnlShippingAddress.Visible)
            {
                ddlShippingAddress_SelectedIndexChanged(sender, e);
            }
        }

        private void lnkNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Page.Validate();
            
            List<NPError> theErrors = new List<NPError>();

            //validate address info
            int _billAddId = BillingAddressBlock.Save(theErrors);
            if (_billAddId > 0)
            {
                _order.BillingAddressID = _billAddId;
            }

            int _shipAddId = 0;
            if (!cbxUseUniqueShippingAddress.Checked)
            {
                //customer wants to use billing for shipping. Attempt to find an existing shipping address that matches or create a new one
                if (_billAddId > 0)
                {
                    //only look for or create a shipping address if billing address is valid                    

                    //make a copy of current billing address to use for our ship address
                    NPAddress shipAddress = new NPAddress(_billAddId);
                    shipAddress.AddressID = 0;
                    shipAddress.ShipDestination = true;

                    string shipAddressName = shipAddress.Name;
                    //address name could be "Name" or "Name(x)" for duplicates. Determine x so a new name can be generated if necessary
                    Regex regNameCount = new Regex("(?<name>.+)\\((?<index>\\d+)\\)$");
                    MatchCollection coll = regNameCount.Matches(shipAddressName);
                    if (coll.Count > 0 && coll[0].Groups["name"].Value != "")
                    {
                        //now name should just be the starting name without the (x)
                        shipAddressName = coll[0].Groups["name"].Value;
                    }

                    //loop over existing shipping addresses to find an existing one that will match the current billing address
                    bool addressFound = false;
                    int currentMaxDuplicateNameIndex = 0;
                    bool exactAddressNameFound = false;
                    List<NPAddress> allShipAddresses = _order.Account.GetShippingAddresses();

                    //first loop over all shipping addresses and see if there's an exact match including address name
                    foreach (NPAddress currShipAddress in allShipAddresses)
                    {
                        if (shipAddress.Equals(currShipAddress))
                        {
                            //exact match found
                            _shipAddId = currShipAddress.AddressID;
                            _order.SetAllDetailShipAddress(_shipAddId);
                            addressFound = true;
                            break;
                        }
                    }

                    //if no exact match, try finding a matching address details with a different name
                    if (!addressFound)
                    {
                        foreach (NPAddress currShipAddress in allShipAddresses)
                        {
                            //copy name to new address so we know that matches
                            shipAddress.Name = currShipAddress.Name;

                            if (shipAddress.Equals(currShipAddress))
                            {
                                //exact match found with exception of name
                                _shipAddId = currShipAddress.AddressID;
                                _order.SetAllDetailShipAddress(_shipAddId);
                                addressFound = true;
                                break;
                            }
                            else
                            {
                                //address doesn't match so now run logic to possibly handle duplicate names                               

                                //address name could be "Name" or "Name(x)" for duplicates. Determine x so a new name can be generated                            
                                string tmpShipAddressName = currShipAddress.Name;
                                coll = regNameCount.Matches(tmpShipAddressName);
                                if (coll.Count > 0 && coll[0].Groups["name"].Value != "")
                                {
                                    //now name should just be the starting name without the (x)
                                    tmpShipAddressName = coll[0].Groups["name"].Value;

                                    if (tmpShipAddressName == shipAddressName)
                                    {
                                        exactAddressNameFound = true;
                                        int idx = Int32.Parse(coll[0].Groups["index"].Value);
                                        if (idx > currentMaxDuplicateNameIndex)
                                        {
                                            //set current max index found to new index since it's the largest found yet
                                            currentMaxDuplicateNameIndex = idx;
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (!addressFound)
                    {
                        //didn't find an existing shipping address so save the new one
                        if (exactAddressNameFound)
                        {
                            //duplicate name exists so modify name to use maximum duplicate index
                            shipAddress.Name = shipAddressName + "(" + ++currentMaxDuplicateNameIndex + ")";
                        }
                        else
                        {
                            //set address name to same as billing
                            shipAddress.Name = shipAddressName;
                        }
                        shipAddress.Save();
                        if (shipAddress.AddressID > 0)
                        {
                            _shipAddId = shipAddress.AddressID;
                            _order.SetAllDetailShipAddress(_shipAddId);
                        }
                    }

                }
            }
            else
            {
                _shipAddId = ShippingAddressBlock.Save(theErrors);
                if (_shipAddId > 0)
                {
                    _order.SetAllDetailShipAddress(_shipAddId);
                }
            }


            bool pageValid = Page.IsValid;

            if (!DisplayErrors(theErrors) && Page.IsValid && _billAddId > 0 && _shipAddId > 0)
            {
                //save entered phone numbers
                NPUser user = new NPUser(_order.UserID);
                user.DayPhone = txtDayPhone.Text;
                user.EveningPhone = txtEveningPhone.Text;
                user.Save();

                //need to calculate for taxes, shipping costs, etc
                _order.Calculate();

                if (txtDayPhone.Text == "")
                {
                    imgPhoneWarning.Visible = true;
                    return;
                }

                string nextPage;
                if ((nextPage = GetFlow("checkout")) != null)
                {
                    Response.Redirect(nextPage, true);
                }
                else
                {
                    Response.Redirect("verify.aspx", true);
                }
            }
            else
            {
                //unhandled error situation
            }
        }

        private bool DisplayErrors(List<NPError> theErrors)
        {
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {
                    case NPErrorType.ADDR_DuplicateNameBilling:
                        sysError.Text += errDuplicationAddressNameBilling.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_DuplicateNameShipping:
                        sysError.Text += errDuplicationAddressNameShipping.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_AddressNameIsRequired:
                        sysError.Text += hdnAddressNameRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_CityIsRequired:
                        sysError.Text += hdnCityIsRequired.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_PostalCodeIsRequired:
                        sysError.Text += hdnStreet1Required.Text + "<br/>";
                        break;
                    case NPErrorType.ADDR_Street1IsRequired:
                        sysError.Text += hdnStreet1Required.Text + "<br/>";
                        break;                    
                    default:
                        throw npe;

                }
            }
            return theErrors.Count > 0;
        }        

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkNext.Click += new System.Web.UI.ImageClickEventHandler(this.lnkNext_Click);

        }
        #endregion
    }
}
