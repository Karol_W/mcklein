﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.api;
using netpoint.api.account;
using netpoint.api.commerce;
using netpoint.api.common;
using netpoint.api.data;
using netpoint.classes;
using NetPoint.ShippingProvider;

namespace netpoint.commerce.flow2
{
    public partial class verify : NPBasePageCommerce
    {
        private NPOrder _order;

        protected void Page_Load(object sender, EventArgs e)
        {
            _order = new NPOrder(UserID, SessionID);
            if (!_order.Initialized || _order.OrderDetail.Count == 0 || _order.UserID == null || _order.UserID == "")
            {
                Response.Redirect("~/commerce/cart.aspx");
            }
            
            paymentBlock.Order = _order;
            sysErrorMessage.Text = "";
            lblShippingError.Text = "";

            //check for multiple taxes on a line and display error message
            try
            {
                _order.Valid(netpoint.api.OrderValidationType.OrderTaxes);
            }
            catch (Exception ex)
            {
                sysErrorMessage.Text = hdnIncorrectTax.Text;
                HideButtonPanel();
            }


            if (!this.IsPostBack)
            {
                btnOrder.ImageUrl = ResolveImage("buttons", "placeyourorder.gif");
                btnPayPalOrder.ImageUrl = "https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif".ToLower();
                imgRequestedShipDate.ImageUrl = ResolveImage("icons", "warning.gif");
                imgOutOfStock.ImageUrl = ResolveImage("icons", "warning.gif");
                imgUnavailableOutOfStock.ImageUrl = ResolveImage("icons", "outofstock.gif");
                imgUnavailable.ImageUrl = ResolveImage("icons", "availableno.gif");
                imgNotes.ImageUrl = ResolveImage("icons", "warning.gif");

                //check if notes should be displayed
                if (TheTheme.DisplayCheckoutNotes)
                {
                    pnlOrderNotes.Visible = true;
                }

                NPAddress shipto = new NPAddress(_order.GetShipAddress());
                sysShippingAddress.Text = shipto.AddressHTML;

                NPAddress billto = new NPAddress(_order.BillingAddressID);
                sysBillingAddress.Text = billto.AddressHTML;

                rowB2B.Visible = _order.Account.AccountType.Equals("B");
                txtPO.Text = _order.PONumber;
                txtNotes.Text = _order.Notes;

                CartList.Order = _order;

                BindForm();                

                //set order property of payment block
                paymentBlock.Order = _order;

                                
            }
            
            if (!clbackTotals.CausedCallback)
            {
                expenses.LoadExpenses(_order);
            }

            //Add the javascript call to each radio button in the list. This allows the callback to be triggered
            foreach (ListItem Li in rblShipMethod.Items)
            {
                Li.Attributes.Add("onclick", "selectProduct('" + Li.Value + "');");
            }  
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //if selected payment is paypal, check if we have a completed payment, if so display buy button,
            //otherwsie show pay with paypal button
            btnPayPalOrder.Visible = false;
            btnOrder.Visible = true;      
            if (_order.Payments.Count == 0 && paymentBlock.SelectedPaymentMethod == 10)
            {
                btnPayPalOrder.Visible = true;
                btnOrder.Visible = false;     
            }            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrder.Click += new ImageClickEventHandler(btnOrder_Click);
            this.btnPayPalOrder.Click += new ImageClickEventHandler(btnOrder_Click);
        }
        #endregion

        private void BindForm()
        {
            try
            {
                //Updated method call to included address parameters used for zone filtering
                NPAddress shipto = new NPAddress(_order.GetShipAddress(), base.Connection);
                NPListBinder.PopulateRateList(rblShipMethod, "S", _order.ShippingMethod.ToString(), false, true, true, _order.SubTotal, _order.WeightTotal, _order.WeightTotalUnitCode, UserID, true, shipto.CountryAbbr, shipto.StateAbbr, shipto.PostalCode);                 

                if (rblShipMethod.SelectedIndex == -1 && rblShipMethod.Items.Count > 0)
                {
                    rblShipMethod.SelectedIndex = 0;
                    _order.SetAllDetailShipMethod(Convert.ToInt32(rblShipMethod.SelectedValue));
                }
                if (rblShipMethod.Items.Count == 0)
                {
                    sysErrorMessage.Text = errNoShipping.Text + "<br>" + errConfigureShipping.Text;
                    //TODO is this correct to hide both these buttons
                    HideButtonPanel();
                }

                //Code called on the shipping aspx page
                int shipMethod = 0;
                int.TryParse(rblShipMethod.SelectedValue, out shipMethod);
                _order.ShippingMethod = shipMethod;
                List<Exception> excList = _order.Calculate();
                //only consider results if not a callback, if it is a callback it will be handled there
                if (!clbackTotals.IsCallback)
                {
                    if (excList.Count > 0)
                    {
                        //check first exception in list
                        if (excList[0] is ShippingProviderException)
                        {
                            lblShippingError.Text = excList[0].Message;
                            if (excList[0].InnerException != null) lblShippingError.Text += excList[0].InnerException.Message;
                            HideButtonPanel();
                            //Log the error so it can be tracked
                            NPErrorLog.LogError(excList[0], UserID, "flow2/verify.aspx", ConnectionString);
                            _order.ShippingTotal = 0;
                            _order.Save(false);
                        }
                        else
                        {
                            throw excList[0];
                        }
                    }
                }
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblShippingError.Text = ex.Message;
                if (ex.InnerException != null) lblShippingError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "flow2/verify.aspx", ConnectionString);
            }
        }

        /// <summary>
        /// Triggered by the on click event of the radio button list. Dynamically refreshes the totals.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void clbackTotals_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            try
            {
                ShipMethodChanged(rblShipMethod, e.Parameter);
                rblShipMethod.SelectedValue = e.Parameter;
                //Reload the Expenses Section to update
                expenses.LoadExpenses(_order);

                paymentBlock.BindAll();

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }
            catch (ShippingProviderException ex)
            {
                //TODO CONFIRM THIS IS THE RIGHT APPROACH FOR THE ERRORS
                lblShippingError.Text = ex.Message;
                if (ex.InnerException != null) lblShippingError.Text += ex.InnerException.Message;
                HideButtonPanel();
                //Log the error so it can be tracked
                NPErrorLog.LogError(ex, UserID, "verify.aspx", ConnectionString);

                //Flushes the HTML back to the browser
                PlaceHolder1.RenderControl(e.Output);
            }

        }        

        private void ShipMethodChanged(ListControl lc, string selectVal)
        {
            //_order.ShippingMethod = Convert.ToInt32(lc.SelectedValue);
            _order.ShippingMethod = Convert.ToInt32(selectVal);

            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }

            List<Exception> excList = _order.Calculate();
            if (excList.Count > 0)
            {
                //check first exception in list
                if (excList[0] is ShippingProviderException)
                {
                    lblShippingError.Text = excList[0].Message;
                    if (excList[0].InnerException != null) lblShippingError.Text += excList[0].InnerException.Message;
                    HideButtonPanel();
                    //Log the error so it can be tracked
                    NPErrorLog.LogError(excList[0], UserID, "flow2/verify.aspx", ConnectionString);
                    _order.ShippingTotal = 0;
                    _order.Save(false);
                }
                else
                {
                    throw excList[0];
                }
            } 

            //reload payments so new order total can be applied if a payment already exists
            if (_order.Payments.Count > 0)
            {
                paymentBlock.AddPayment();
            }

        }

        private void HideButtonPanel()
        {
            btnOrder.Visible = false;
            btnOrder.Enabled = false;
            btnOrder.Width = 0;
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            NPEventArgs npa = null;
            if (args.GetType().FullName == "netpoint.classes.NPEventArgs")
            {
                npa = (NPEventArgs)args;
            }

            if (npa != null)
            {
                switch (npa.FromEvent)
                {
                    case "EmptyCart":
                        Response.Redirect("~/commerce/cart.aspx");
                        break;
                    case "outofstock":
                        imgOutOfStock.Visible = true;
                        lblOutOfStock.Visible = true;
                        break;
                    case "unavailableoutofstock":
                        imgUnavailableOutOfStock.Visible = true;
                        lblUnavailableOutOfStock.Visible = true;
                        HideButtonPanel();
                        break;
                    case "unavailable":
                        imgUnavailable.Visible = true;
                        lblUnavailable.Visible = true;
                        HideButtonPanel();
                        break;
                }
            }
            return true;
        }

        private void btnOrder_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            List<NPError> theErrors = new List<NPError>();

            if (!CheckAndSetRequestedShipDateAndPO(_order))
            {
                return;
            }

            if (paymentBlock.SubmitPayment())
            {
                paymentBlock.AddPayment();

                //  need to calculate in case the payment method incurs a shipping charge
                _order.Calculate();

                ((NPOrderPayment)_order.Payments[0]).PaymentAmount = _order.GrandTotal;
                ((NPOrderPayment)_order.Payments[0]).Save();                

                if (!_order.PaymentTotal.ToString("c").Equals(_order.GrandTotal.ToString("c")))
                {
                    theErrors.Add(new NPError("Total Payments must match the Total of the Order", NPErrorType.ORDR_PaymentNotTotal));
                }

                if (Page.IsValid && !DisplayErrors(theErrors))
                {
                    string nextPage;
                    if ((nextPage = GetFlow("checkout")) != null)
                    {
                        Response.Redirect(nextPage, true);
                    }
                    else
                    {

                        NPCCProvider.ExitPage(_order, "~/commerce/paymentauthorize.aspx", "~/commerce/cart.aspx");
                        
                    }
                }
            }
        }

        private bool CheckAndSetRequestedShipDateAndPO(NPOrder _order)
        {
            if (dtRequestedShipDate.SelectedDate != DateTime.MinValue && dtRequestedShipDate.SelectedDate <= DateTime.Now)
            {
                imgRequestedShipDate.Visible = true;
                return false;
            }
            else
            {
                imgRequestedShipDate.Visible = false;
                _order.RequestedShipDate = dtRequestedShipDate.SelectedDate;
            }
            if (txtNotes.Text.Length > txtNotes.LimitInput)
            {
                imgNotes.Visible = true;
                return false;
            }
            else
            {
                imgNotes.Visible = false;
                _order.Notes = txtNotes.Text;
            }
            _order.PONumber = txtPO.Text;
            _order.Save();
            return true;
        }

        private bool DisplayErrors(List<NPError> theErrors)
        {
            foreach (NPError npe in theErrors)
            {
                switch (npe.ErrorType)
                {                    
                    case NPErrorType.ORDR_PaymentNotTotal:
                        sysErrorMessage.Text += errTotalPayments.Text += "<br/>";
                        break;
                    default:
                        throw npe;

                }
            }
            return theErrors.Count > 0;
        }
    }
}
