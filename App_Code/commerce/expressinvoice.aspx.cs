﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using netpoint.classes;
using netpoint.api.commerce;
using netpoint.api;
using netpoint.api.account;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary Description for Express Checkout Invoice.
    /// </summary>
    public partial class expressinvoice : NPBasePageCommerce
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["OrderNo"] == null)
            {
                Response.Write("Critical Error");
                Response.End();
            }
            else
            {
                NPBasePage bp = (NPBasePage)Page;
                NPOrder order = new NPOrder(Convert.ToInt32(Request.QueryString["OrderNo"]));
                if (CheckPermissions(order))
                {
                    order.SuppressExtendedProperties = false;
                    NPAddress shipping = new NPAddress(order.GetShipAddress());
                    NPAddress billing = new NPAddress(order.BillingAddressID);
                    NPAccount account = new NPAccount(order.AccountID);
                    account.SuppressExtendedProperties = true;
                    NPUser user = new NPUser(order.UserID);
                    user.SuppressExtendedProperties = true;

                    NPTemplate template = new NPTemplate("invoicetemplate", ServerID);
                    template.DefaultFormatProvider = bp.BaseCurrency.GetNumberFormatInfo(AppCulture.NumberFormat, true);

                    template.MergeText("StyleSheet", bp.StyleSheetLink);
                    template.MergeText("VirtualPath", bp.VirtualPath);
                    template.MergeText("AssetsPath", bp.AssetsPath);
                    template.MergeText("ThemesPath", bp.ThemesPath);
                    template.MergeText("ServerID", bp.ServerID);
                    template.MergeText("Host", this.Request.Url.Host);

                    template.MergeObject(order);
                    template.MergeObject(account);
                    template.MergeObject(user);
                    template.MergeSection("weight", order);
                    template.MergeSection("orderdetail", order.OrderDetail);
                    template.MergeSection("expenses", order.GetTotals(bp.BaseCurrency));
                    template.MergeSection("orderpayments", order.Payments);
                    template.MergeSection("billingaddress", billing);
                    template.MergeSection("shippingaddress", shipping);
                    template.MergeSection("shipments", order.Shipments);

                    string invoice = template.ToString().Replace("[" + order.UserID.Trim() + "]", " ");
                    invoice = invoice.Replace(order.UserID.Trim() + ": " + order.UserID.Trim(), " ");
                    sysPageText.Text = invoice;
                    EmailConfirmationText.Text = hdnEmailConfirmation.Text + user.Email;
                    lnkHome.NavigateUrl = "~/";
                    lnkHome.Text = hdnHomePage.Text;

                    this.TheSession.Logout();
                }
                else
                {
                    throw new NPError(hdnRestricted.Text, NPErrorType.__NetPointUI, NPErrorDisplayType.Nice);
                }
            }
        }

        protected bool CheckPermissions(NPOrder order)
        {
            if (UserID == order.UserID && order.UserID != "")
            {
                return true;
            }
            if (PermissionController.IsSuperUser)
            {
                return true;
            }
            NPUser currentUser = new NPUser(UserID);
            if (PermissionController.UserInRole("licenseduser") &&
                (order.AccountID == currentUser.AccountID || new NPAccount(currentUser.AccountID).AccountType == "I"))
            {
                return true;
            }
            return false;
        }
    }
}
