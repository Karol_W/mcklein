using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.classes;
using netpoint.api.commerce;
using netpoint.api.account;

namespace netpoint.commerce
{
    /// <summary>
    /// Summary description for setorderdefaults.
    /// </summary>
    public partial class setorderdefaults : NPBasePage
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            divDefaults.Visible = false;
            divUserProfile.Visible = false;
            sysErrorMessage.Text = "";
            NPOrder order = new NPOrder(UserID, SessionID);
            order.ReloadPayments();
            if (!order.Initialized)
            {
                DisplayError(errNoOrder.Text);
                return;
            }
            else
            {                
                bool errors = false;
                NPAccount account = new NPAccount(AccountID);
                if (order.GetShipAddress() == 0) 
                {
                    if (account.DefaultShipAddressID == 0)
                    {
                        DisplayError(errNoDefaultShipAddress.Text);
                        errors = true;
                    }
                    else 
                    {
                        NPAddress shippingAddress = new NPAddress(account.DefaultShipAddressID, this.Connection);
                        if (shippingAddress.Initialized)
                        {
                            order.SetAllDetailShipAddress(shippingAddress.AddressID);
                        }
                    }
                }
                if (order.ShippingMethod == 0)
                {
                    if (account.DefaultShipOptionID == 0)
                    {
                        DisplayError(errNoDefaultShipOption.Text);
                        errors = true;
                    }
                    else
                    {
                        order.ShippingMethod = account.DefaultShipOptionID;
                    }
                }
                if (order.BillingAddressID == 0)
                {
                    if (account.DefaultBillAddressID == 0)
                    {
                        DisplayError(errNoDefaultBillAddress.Text);
                        errors = true;
                    }
                    else
                    {
                        NPAddress billingAddress = new NPAddress(account.DefaultBillAddressID, this.Connection);
                        if (billingAddress.Initialized)
                        {
                            order.BillingAddressID = billingAddress.AddressID;
                        }
                    }
                }
                order.Calculate();
                if (order.Balance > 0)
                {
                    if (account.DefaultBillOptionID == 0)
                    {
                        DisplayError(errNoDefaultBillOption.Text);
                        errors = true;
                    }
                    else
                    {
                        NPPaymentOption po = new NPPaymentOption(account.DefaultBillOptionID);
                        if (!po.Initialized)
                        {
                            DisplayError(errNoPaymentOption.Text);
                            errors = true;
                        }
                        else if (po.HasRequirements || po.PaymentCode == "paypal") 
                        {
                            DisplayError(errPOHasRequirements.Text);
                            errors = true;
                        }
                        else
                        {
                            foreach (NPOrderPayment payment in order.Payments)
                            {
                                payment.Delete();
                            }
                            NPOrderPayment op = new NPOrderPayment();
                            op.UserID = order.UserID;
                            op.AccountID = order.AccountID;
                            op.OrderID = order.OrderID;
                            op.PaymentID = po.PaymentID;
                            op.PaymentName = po.PaymentName;
                            op.PaymentAmount = order.GrandTotal + Convert.ToDecimal(po.PaymentCharge);
                            op.BillingName = "";
                            op.Save();
                            order.Calculate();
                        }
                    }
                }
                if (errors) 
                {
                    divDefaults.Visible = true;
                }
                NPUser user = new NPUser(UserID);
                if (String.IsNullOrEmpty(user.DayPhone))
                {
                    DisplayError(errNoDayPhone.Text);
                    divUserProfile.Visible = true;
                    errors = true;
                }
                if (errors)
                {
                    return;
                }
                string nextPage;
                if ((nextPage = GetFlow("checkout")) != null)
                {
                    Response.Redirect(nextPage, true);
                }
                else
                {
                    Response.Redirect("verify.aspx?fastcheckout=true", true);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected override void DisplayError(string error)
        {
            string separator = "<br />";
            sysErrorMessage.Text += error + separator;
        }
    }
}
