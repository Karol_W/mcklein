namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Resources;
    using netpoint.api.commerce;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for CategoriesBlock.
    /// </summary>
    public partial class InvoiceTotalBlock : System.Web.UI.UserControl
    {
        public int OrderID = 0;
        private NPBasePage _bp;

        public Unit BorderWidth
        {
            set { TotalPanel.BorderWidth = value; }
        }

        public Unit Width
        {
            set { TotalPanel.Width = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            if (!Page.IsPostBack)
            {
                NPOrder o = new NPOrder(OrderID);
                if (o.Initialized)
                {
                    sysVolumeTotal.Text = hdnTotalVolume.Text + " " + o.VolumeTotal.ToString();
                    sysWeightTotal.Text = hdnTotalWeight.Text + " " + o.WeightTotal.ToString();

                    expenses.LoadExpenses(o);
                }
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
