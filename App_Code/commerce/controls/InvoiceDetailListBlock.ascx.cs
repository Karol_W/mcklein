namespace netpoint.catalog.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.commerce;
    using netpoint.classes;
    using System.Resources;
    using netpoint.common.controls;

    /// <summary>
    ///	Summary description for CategoriesBlock.
    /// </summary>
    public partial class InvoiceDetailListBlock : System.Web.UI.UserControl
    {

        public int OrderID = 0;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            NPBasePage bp = (NPBasePage)Page;
            if (Page.IsPostBack == false)
            {
                DataTable dt = OrderReports.CartDetail(OrderID, bp.ProductThumbImage, bp.ConnectionString);
                gvInvoiceDetail.DataSource = dt;
                gvInvoiceDetail.DataBind();
            }
        }

        #region properties

        public Unit BorderWidth
        {
            set
            {
                gvInvoiceDetail.BorderWidth = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        protected void gvInvoiceDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            PriceDisplay prc = (PriceDisplay)e.Row.FindControl("prcPrice");
            NPBasePage bp = (NPBasePage)this.Page;
            DataRowView drv = (DataRowView)e.Row.DataItem;

            if (prc != null)
            {
                prc.Price = Convert.ToDecimal(drv["PurchasePrice"]);
            }
        }
    }
}
