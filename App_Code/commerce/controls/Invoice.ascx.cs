namespace netpoint.commerce.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.commerce;
    using netpoint.api.account;
    using System.Collections;
    using System.Resources;


    /// <summary>
    ///		Summary description for Invoice.
    /// </summary>
    public partial class Invoice : System.Web.UI.UserControl
    {

        private int _orderNo;
        private NPOrder _order;

        public int OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _order = new NPOrder(_orderNo);

            if (!_order.Initialized)
                return;

            NPAccount a = new NPAccount(_order.AccountID);

            if (_order.BillingAddressID == 0)
            {
                npshiptoaddressblock.Visible = false;
                npbilltoaddressblock.Visible = false;
                ltlBillTo.Text = "<pre>" + _order.BillingAddress + "</pre>";
                ltlShipTo.Text = "<pre>" + _order.ShippingAddress + "</pre>";
            }
            else
            {
                npbilltoaddressblock.AddressID = _order.BillingAddressID;
                npshiptoaddressblock.AddressID = _order.GetShipAddress();
            }

            npidlblock.OrderID = _order.OrderID;
            npitblock.OrderID = _order.OrderID;

            OrderDate.Text = ((NPOrderDetail)_order.OrderDetail[0]).PurchaseDate.ToShortDateString();
            if (a.AccountType.Equals("B"))
            {
                if (_order.PONumber.Length > 0)
                {
                    ltlPONumber.Text += " " + _order.PONumber;
                    ltlPONumber.Visible = true;
                }
                if (_order.SynchID.Length > 0 && !_order.SynchID.Equals("0"))
                {
                    ltlConfirmationNumber.Text += " " + _order.SynchID;
                    ltlConfirmationNumber.Visible = true;
                }
            }

            BindPayment();
        }

        private void BindPayment()
        {
            gvInvoice.DataSource = _order.Payments;
            gvInvoice.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
