namespace netpoint.commerce.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using netpoint.api.account;
    using netpoint.classes;

    /// <summary>
    ///		Summary description for BrowseAsUser.
    /// </summary>
    public partial class BrowseAsUser : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (Session["UserID"] == null)
            {
                tblSelected.Visible = false;
                pnlSelect.Visible = true;
            }
            else
            {
                tblSelected.Visible = true;
                pnlSelect.Visible = false;
                ltlUserID.Text = Session["UserID"].ToString();
                ltlAccountID.Text = Session["AccountID"].ToString();
            }

            NPBasePage bp = (NPBasePage)Page;
            dot.ImageUrl = bp.ResolveImage("icons", "smallblockevents.gif");
            lnkSelectImg.ImageUrl = bp.ResolveImage("icons", "next.gif");
            btnRemove.ImageUrl = bp.ResolveImage("icons", "delete.gif");
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRemove.Click += new System.Web.UI.ImageClickEventHandler(this.btnRemove_Click);

        }
        #endregion

        private void btnRemove_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Session.Remove("UserID");
            Session.Remove("AccountID");
            string qs;
            if (Request.QueryString.ToString().Length > 0)
            {
                qs = "?" + Request.QueryString;
            }
            else
            {
                qs = "";
            }
            Response.Redirect(Request.FilePath + qs, true);
        }

    }
}
