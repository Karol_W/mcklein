using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;
using System.Resources;
using netpoint.api;
using netpoint.api.data;
using netpoint.api.utility;
using netpoint.commerce.controls;
using netpoint.common.controls;

namespace netpoint.catalog.controls
{
    /// <summary>
    ///	Summary description for CategoriesBlock.
    /// </summary>
    public partial class CartDetailListBlockMobile : NPBaseControl
    {
        #region "Members and Properties"

        private NPBasePage bp;
        protected NPOrder _order;
        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)this.Page;
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        public NPOrder Order
        {
            set
            {
                _order = value;
                if (pnl.Visible)
                {
                    discount.Order = _order;
                }
            }
            get { return _order; }
        }

        public void BindGrid()
        {
            gridOrderDetail.DataKeyNames = new string[1] { "OrderDetailID" };
            gridOrderDetail.DataSource = _order.OrderDetail;
            gridOrderDetail.DataBind();

            if (BasePage.IsMultiCurrency && gridOrderDetail.Columns.Count > 5 && gridOrderDetail.HeaderRow != null)
            {
                gridOrderDetail.Columns[5].Visible = true;
                gridOrderDetail.HeaderRow.Cells[4].ColumnSpan = 2;
                gridOrderDetail.HeaderRow.Cells[5].Visible = false;
            }

            discount.Visible = _order.OrderDetail.Count > 0;
        }
        protected void gridOrderDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
            System.Web.UI.WebControls.Image imgDelete = (System.Web.UI.WebControls.Image)e.Row.FindControl("imgDelete");
            ImageButton btnUpdateQty = (ImageButton)e.Row.FindControl("btnUpdateQty");
            PriceDisplay prcPrice = (PriceDisplay)e.Row.FindControl("prcPrice");
            Literal sysPackagingUnit = (Literal)e.Row.FindControl("sysPackagingUnit");
            Literal sysPackagingQty = (Literal)e.Row.FindControl("sysPackagingQty");
            Literal sysSalesUnit = (Literal)e.Row.FindControl("sysSalesUnit");
            Literal sysSalesQty = (Literal)e.Row.FindControl("sysSalesQty");
            HyperLink lnkThumb = (HyperLink)e.Row.FindControl("lnkThumb");
            System.Web.UI.WebControls.Image imgOutOfStock = (System.Web.UI.WebControls.Image)e.Row.FindControl("imgOutOfStock");
            System.Web.UI.WebControls.Image imgUnavailable = (System.Web.UI.WebControls.Image)e.Row.FindControl("imgUnavailable");
            System.Web.UI.WebControls.Image imgUnavailableOutOfStock = (System.Web.UI.WebControls.Image)e.Row.FindControl("imgUnavailableOutOfStock");
            Literal ltlOrderNotes = (Literal)e.Row.FindControl("ltlOrderNotes");
            TextBox txtQty = (TextBox)e.Row.FindControl("nbQuantity");
            Label lblQty = (Label)e.Row.FindControl("lblQuantity");
            netpoint.commerce.controls.PartDescription partDesc = (netpoint.commerce.controls.PartDescription)e.Row.FindControl("partDesc");
            netpoint.commerce.controls.PartPrice partPrice = (netpoint.commerce.controls.PartPrice)e.Row.FindControl("partPrice");
            netpoint.commerce.controls.PartPrice partPriceBC = (netpoint.commerce.controls.PartPrice)e.Row.FindControl("partPriceBC");

            if (lnkThumb != null)
            {
                NPOrderDetail od = (NPOrderDetail)e.Row.DataItem;
                //delete
                imgDelete.ImageUrl = bp.ResolveImage("icons", "remove.gif");
                if ((od.AggregateType == AggregatePartType.SalesKitChild) || od.AggregateType == AggregatePartType.Discount)
                {
                    btnDelete.Visible = false;
                }
                btnDelete.Visible = btnDelete.Visible && !DisableItemDelete;

                //quantity
                if (od.AggregateType == AggregatePartType.SalesKitChild || od.AggregateType == AggregatePartType.Discount)
                {
                    btnUpdateQty.Visible = false;
                    txtQty.Enabled = false;
                }
                btnUpdateQty.ImageUrl = bp.ResolveImage("icons", "calculate.gif");
                btnUpdateQty.ToolTip = hdnUpdateQuantity.Text;
                btnUpdateQty.Visible = btnUpdateQty.Visible && !DisableQuantityUpdate;
                txtQty.Visible = !DisableQuantityUpdate;
                lblQty.Visible = DisableQuantityUpdate;
                prcPrice.Price = od.AppliedUnitPrice;
                sysPackagingUnit.Text = od.Part.PackagingUnitText;
                if (od.Part.PackagingQty > 1)
                {
                    sysPackagingQty.Text = "[" + od.Part.PackagingQty.ToString(bp.AppCulture.NumberFormat) + "]";
                }
                sysSalesUnit.Text = od.Part.SalesUnitText;
                if (od.Part.SalesQty > 1)
                {
                    sysSalesQty.Text = "[" + od.Part.SalesQty.ToString(bp.AppCulture.NumberFormat) + "]";
                }

                if (od.AggregateType != AggregatePartType.Discount)
                {
                    //thumbnails
                    if (od.Part.StaticThumbNail != null && od.Part.StaticThumbNail.Length > 0)
                    {
                        lnkThumb.ImageUrl = bp.ProductsPath + od.Part.StaticThumbNail;
                    }
                    else
                    {
                        lnkThumb.ImageUrl = bp.ProductThumbImage + od.Part.ThumbNail;
                    }
                    if (od.AggregateType != AggregatePartType.Discount)
                    {
                        lnkThumb.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + Server.UrlEncode(od.Part.PartNo) + "&orderdetailid=" + od.OrderDetailID.ToString();
                    }
                }

                partDesc.LoadDescription(od, bp.ConnectionString);

                ltlOrderNotes.Text = od.Notes;

                //	extended price
                partPrice.LoadPrice(od);
                partPriceBC.LoadPrice(od);

                string _backOrderAction = od.Part.BackorderAction;
                if (od.Part.InheritInventorySettings)
                {
                    _backOrderAction = NPConnection.GetConfigDB("Inventory", "BackorderAction");
                }


                if (!od.Part.InStock)
                {
                    if (_backOrderAction == "W")
                    {
                        imgOutOfStock.Visible = true;
                        imgOutOfStock.ImageUrl = bp.ResolveImage("icons", "warning.gif");
                        this.RaiseBubbleEvent(this, new NPEventArgs("outofstock", null));
                    }
                    else if (_backOrderAction == "N")
                    {
                        imgUnavailableOutOfStock.Visible = true;
                        imgUnavailableOutOfStock.ImageUrl = bp.ResolveImage("icons", "outofstock.gif");
                        this.RaiseBubbleEvent(this, new NPEventArgs("unavailableoutofstock", null));
                    }
                }


                //	check for unavailable parts
                if (!od.Part.Available ||
                    DateTime.Now < od.Part.AvailableDate ||
                    (DateTime.Now > od.Part.ExpirationDate && !od.Part.ExpirationDate.Equals(DateTime.MinValue)))
                {
                    imgUnavailable.Visible = true;
                    imgUnavailable.ImageUrl = bp.ResolveImage("icons", "availableno.gif");
                    this.RaiseBubbleEvent(this, new NPEventArgs("unavailable", null));
                }
            }
        }

        protected void gridOrderDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this.Order == null)
            {
                this.Order = new NPOrder(bp.UserID, bp.SessionID);
                this.Order.Fetch();
            }
            sysErr.Text = "";
            imgQuantity.Visible = false;
            if (e.CommandName.Equals("updateqty"))
            {
                GridViewRow row = (GridViewRow)((System.Web.UI.Control)e.CommandSource).NamingContainer;

                NPOrderDetail od = new NPOrderDetail(Convert.ToInt32(e.CommandArgument));
                try
                {
                    int qty = Convert.ToInt32(((TextBox)row.FindControl("nbQuantity")).Text);
                    if (qty > 0)
                    {
                        if (od.Initialized)
                        {
                            //	if this is a variant or Sales Kit, update all of the children parts quantity as well
                            if (od.Part.AggregateType == AggregatePartType.Variant)
                            {
                                foreach (NPOrderDetail vd in od.VariantDetail)
                                {
                                    vd.Quantity = qty;
                                    vd.Save();
                                }
                            }
                            else if (od.Part.AggregateType == AggregatePartType.SalesKit)
                            {
                                NPPartKit.UpdateCartQty(od.OrderDetailID, od.PartNo, qty - od.Quantity, bp.ConnectionString);
                            }

                            od.Quantity = qty;
                            od.Save();
                            _order.ReloadOrderDetail();
                            _order.Calculate();
                            Response.Redirect(Request.FilePath, true);
                        }
                    }
                    else
                    {
                        if (od.Part.AggregateType == AggregatePartType.SalesKit)
                        {
                            NPPartKit.DeleteCartKit(od.OrderDetailID, bp.ConnectionString);
                        }
                        _order.DeleteOrderDetail(od);
                        _order.Calculate();
                        Response.Redirect(Request.FilePath, true);
                    }
                }
                catch (System.FormatException)
                {
                    imgQuantity.Visible = true;
                    imgQuantity.ImageUrl = bp.ResolveImage("icons", "warning.gif");
                    sysErr.Text = errQuantity.Text;
                }
            }
        }

        protected void gridOrderDetail_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.Order == null)
            {
                this.Order = new NPOrder(bp.UserID, bp.SessionID);
                this.Order.Fetch();
            }
            NPOrderDetail od = new NPOrderDetail(Convert.ToInt32(gridOrderDetail.DataKeys[e.RowIndex].Value));
            //check if part no is not empty to catch dbl clicks. If detail is already deleted just skip
            if (od.PartNo != "")
            {
                if (od.Part.AggregateType == AggregatePartType.SalesKit)
                {
                    NPPartKit.DeleteCartKit(od.OrderDetailID, bp.ConnectionString);
                }

                this.Order.DeleteOrderDetail(od);
                this.Order.Calculate();

                if (this.Order.OrderDetail.Count == 0)
                {
                    NPEventArgs ea = new NPEventArgs("EmptyCart", 0);
                    this.RaiseBubbleEvent(this, ea);
                }
            }
            Response.Redirect(Request.FilePath, true);
        }

        public bool DisableQuantityUpdate
        {
            get { return ViewState["disablequantityupdate"] == null ? false : true; }
            set
            {
                if (value)
                {
                    ViewState["disablequantityupdate"] = "";
                }
                else
                {
                    ViewState["disablequantityupdate"] = null;
                }
            }
        }

        public bool DisableItemDelete
        {
            get { return ViewState["disableitemdelete"] == null ? false : true; }
            set { ViewState["disableitemdelete"] = (value ? "" : null); }
        }
        # endregion

    }
}
