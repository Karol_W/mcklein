﻿// Decompiled with JetBrains decompiler
// Type: netpoint.commerce.controls.Discount
// Assembly: netpoint, Version=8.2.0.7369, Culture=neutral, PublicKeyToken=d935c97cd97f6431
// MVID: AC664620-8146-4552-904D-2293A06475CE
// Assembly location: \\McKlein-FS1\UserProfiles$\MInternJB\My Documents\Visual Studio 2015\Projects\ServiceCallPlugin\ServiceCallPlugin\libs\netpoint.dll

using netpoint.api;
using netpoint.api.commerce;
using netpoint.classes;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mcklein.commerce.controls
{
    public class Discount : UserControl
    {
        private bool _allowEdit = true;
        protected Literal ltlDescription;
        private NPBasePage _bp;
        private NPOrder _order;
        protected Literal ltlMessage;
        protected Literal ltlAppliedCoupons;
        protected TextBox txtDiscountCode;
        protected Button btnApplyDiscount;
        protected GridView gvAppliedCoupons;
        protected Literal sysDescription;
        protected Label sysError;
        protected Literal hdnBadCode;
        protected Literal hdnNoDiscountText;
        protected Literal hdnExpired;
        protected Literal hdnNotStarted;
        protected Literal hdnUserLimit;
        protected Literal hdnAccountLimit;
        protected Literal hdnUnauthorized;

        public NPOrder Order
        {
            get
            {
                return this._order;
            }
            set
            {
                this._order = value;
            }
        }

        public bool AllowEdit
        {
            get
            {
                return this._allowEdit;
            }
            set
            {
                this._allowEdit = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this._bp = (NPBasePage)this.Page;
            if (!this.IsPostBack && this._order != null)
                this.BindGrid();
            if (this.AllowEdit)
                return;
            this.ltlMessage.Visible = false;
            this.txtDiscountCode.Visible = false;
            this.btnApplyDiscount.Visible = false;
            this.gvAppliedCoupons.Columns[2].Visible = false;
        }

        private void BindGrid()
        {
            List<NPOrderOrderDiscount> presentedCoupons = this._order.GetPresentedCoupons();
            this.gvAppliedCoupons.DataSource = (object)presentedCoupons;
            this.gvAppliedCoupons.DataBind();
            this.ltlAppliedCoupons.Visible = presentedCoupons.Count != 0;
        }

        protected void btnApplyDiscount_Click(object sender, EventArgs e)
        {
            NPOrderDiscount npOrderDiscount = new NPOrderDiscount(this.txtDiscountCode.Text, this._bp.ConnectionString);
            int conditionID = 0;
            this.sysError.Text = "";
            if (this._order == null)
                throw new NPError("Discount can not be applied because order has not been initialized.", NPErrorType.__NetPointUI);
            try
            {
                conditionID = npOrderDiscount.QualifyDiscount(this._order);
            }
            catch (NPError ex)
            {
                switch (ex.ErrorType)
                {
                    case NPErrorType.DISC_PreceedsStartDate:
                        this.sysError.Text = this.hdnNotStarted.Text;
                        break;
                    case NPErrorType.DISC_Expired:
                        this.sysError.Text = this.hdnExpired.Text;
                        break;
                    case NPErrorType.DISC_CodeDoesNotExist:
                        this.sysError.Text = this.hdnBadCode.Text;
                        break;
                    case NPErrorType.DISC_AccountLimt:
                        this.sysError.Text = this.hdnAccountLimit.Text;
                        break;
                    case NPErrorType.DISC_UserLimit:
                        this.sysError.Text = this.hdnUserLimit.Text;
                        break;
                    case NPErrorType.DISC_Unauthorized:
                        this.sysError.Text = this.hdnUnauthorized.Text;
                        break;
                    default:
                        throw ex;
                }
            }
            if (conditionID > 0)
            {
                this.sysError.Text = new NPOrderDiscountCondition(conditionID, this._bp.ConnectionString).Description(this._bp.Encoding);
                if (!(this.sysError.Text == ""))
                    return;
                this.sysError.Text = this.hdnNoDiscountText.Text + " [" + this._bp.Encoding + "]";
            }
            else
            {
                if (!(this.sysError.Text == ""))
                    return;
                this.RaiseBubbleEvent((object)this, (EventArgs)new NPEventArgs("ApplyDiscount", (object)""));
                this.BindGrid();
            }
        }

        protected void gvAppliedCoupons_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "remove":
                    new NPOrderOrderDiscount(this._order.OrderID, Convert.ToInt32(e.CommandArgument), this._order.Connection).Delete();
                    this.BindGrid();
                    this.RaiseBubbleEvent((object)this, (EventArgs)new NPEventArgs("ApplyDiscount", (object)""));
                    break;
            }
        }
    }
}
