using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using netpoint.api.data;
using netpoint.api.catalog;
using netpoint.api.commerce;
using netpoint.classes;
using NetPoint.WebControls;

namespace netpoint.commerce.controls
{

    /// <summary>
    ///		Summary description for QuickOrderBlock.
    /// </summary>
    public partial class QuickOrderBlock : System.Web.UI.UserControl
    {

        private NPBasePage bp;
        private int _maxlines = 0;

        public int Columns
        {
            set { orderrepeater.RepeatColumns = value; }
        }

        public int TotalLines
        {
            set { _maxlines = value; }
        }

        public Unit BorderWidth
        {
            set { orderrepeater.BorderWidth = value; }
        }

        public string SubmitImageUrl
        {
            set { SubmitButton.ImageUrl = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            bp = (NPBasePage)Page;

            if (!Page.IsPostBack)
            {
                orderrepeater.DataSource = GetTable();
                orderrepeater.DataBind();

                SubmitButton.ImageUrl = bp.ResolveImage("buttons", "addtocart.gif");
            }
        }

        private DataTable GetTable()
        {
            DataTable dtTable = new DataTable();
            DataColumn dcPartNo = new DataColumn("Image", typeof(string));
            DataColumn dcQuantity = new DataColumn("FileName", typeof(string));
            DataRow drRow;
            dtTable.Columns.Add(dcPartNo);
            dtTable.Columns.Add(dcQuantity);
            for (int i = 1; i <= _maxlines; i++)
            {
                drRow = dtTable.NewRow();
                drRow[0] = "";
                drRow[1] = "";
                dtTable.Rows.Add(drRow);
            }
            return dtTable;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitButton.Click += new System.Web.UI.ImageClickEventHandler(this.SubmitButton_Click);

        }
        #endregion

        private void SubmitButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            bool addedpart = false;
            string partno;
            NPPart p;
            int qty;

            sysError.Text = "";
            for (int i = 0; i < orderrepeater.Items.Count; i++)
            {
                System.Web.UI.WebControls.Image imgWarning = (System.Web.UI.WebControls.Image)orderrepeater.Items[i].FindControl("imgWarning");
                imgWarning.ImageUrl = bp.ResolveImage("icons", "warning.gif");
                partno = ((PromptTextBox)orderrepeater.Items[i].FindControl("PartNo")).Value;
                qty = 1;
                try
                {
                    qty = Convert.ToInt32(((PromptTextBox)orderrepeater.Items[i].FindControl("Quantity")).Value);
                }
                catch { }
                if (partno.Length > 0)
                {
                    p = new NPPart(partno);
                    if (!p.Initialized || p.InProdLine || !p.Available || p.AvailableDate > DateTime.Now || (!p.ExpirationDate.Equals(DateTime.MinValue) && p.ExpirationDate < DateTime.Now))
                    {
                        imgWarning.Visible = true;
                        sysError.Text = errBadPart.Text;
                        return;
                    }
                    else if (qty <= 0)
                    {
                        imgWarning.Visible = true;
                        sysError.Text = errNegativeQuantity.Text;
                        return;
                    }
                    else
                    {
                        imgWarning.Visible = false;
                    }
                }
            }

            NPOrder o = new NPOrder(bp.UserID, bp.SessionID);
            if (!o.Initialized)
            {
                o.OrderCurrency = NPConnection.GetConfigDB("NetPoint", "BaseCurrency");
                if (!o.CreateDefaultOrder(bp.UserID, bp.AccountID, bp.SessionID))
                {
                    Response.Write("Catastrophic Failure.");
                    Response.End();
                }
            }

            for (int i = 0; i < orderrepeater.Items.Count; i++)
            {

                partno = ((PromptTextBox)orderrepeater.Items[i].FindControl("PartNo")).Value;
                if (partno.Length > 0)
                {

                    qty = 1;

                    try
                    {
                        qty = Convert.ToInt32(((PromptTextBox)orderrepeater.Items[i].FindControl("Quantity")).Value);
                    }
                    catch { }

                    o.AddPart(partno, qty, bp.Catalog.CatalogCode, "", bp.PriceList, NPConnection.GetConfigDB("Commerce", "CalculationUoM"));
                    addedpart = true;
                }
            }

            if (addedpart)
            {
                Response.Redirect("~/commerce/cart.aspx", true);
            }
        }
    }
}
