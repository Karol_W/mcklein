namespace netpoint.commerce.controls
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Collections;
    using netpoint.api.data;
    using netpoint.api.commerce;
    using netpoint.api.account;
    using netpoint.api.utility;
    using netpoint.classes;
    using System.Resources;
    using netpoint.api.catalog;
    using netpoint.common.controls;
    using netpoint.api.common;
    using netpoint.api.encryption;

    /// <summary>
    ///		Summary description for PaymentBlock.
    /// </summary>
    public partial class PaymentBlockMobile : System.Web.UI.UserControl
    {

        private NPOrder _order;
        private NPBasePage _bp;

        public NPOrder Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public int SelectedPaymentMethod
        {
            get
            {
                int ret = -1;
                Int32.TryParse(ddlPaymentMethod.SelectedValue, out ret);
                return ret;
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            _bp = (NPBasePage)Page;
            this.Visible = _order.Initialized;
            if (!this.IsPostBack)
            {
                if (_order.Initialized)
                {
                    BindAll();
                    BindCountryList();
                }
                for (int i = 0; i < 10; i++)
                {
                    ddlYear.Items.Add((DateTime.Now.Year + i).ToString());
                    ddlIssueYear.Items.Add((DateTime.Now.Year - i).ToString());
                }
                txtAmount2.Text = _order.Balance.ToString("0.00");
                imgVerification.ImageUrl = _bp.ResolveImage("images", "verifynumber.gif");
            }
        }

        public void BindAll()
        {
            _order.Calculate();
            BindPayments();
            BindTotals();            
            if (_order.Balance == 0 || _order.Payments.Count > 0)
            {
                pnlNew.Visible = false;
            }
            else
            {
                BindWallet();
                pnlNew.Visible = true;
            }
            CommandEventArgs cea = new CommandEventArgs("balance", (_order.Balance == 0));
            this.RaiseBubbleEvent(this, cea);
        }

        private void BindPayments()
        {
            if (_bp.IsMultiCurrency)
            {
                foreach (DataControlField dcf in grid.Columns)
                {
                    dcf.Visible = true;
                }
            }

            //check if payment requires cc or verification value and if so, it must be in session
            bool removepayment = false;
            if (_order.Payments.Count > 0)
            {
                NPPaymentOption po = new NPPaymentOption(((NPOrderPayment)_order.Payments[0]).PaymentID);
                if (po.UseBankCardNumber)
                {
                    if (_bp.TheSession.EncryptedSessionPaymentInformation[((NPOrderPayment)_order.Payments[0]).OrderPaymentID] != null)
                    {
                        Encryption enc = new Encryption(((NPOrderPayment)_order.Payments[0]).OrderPaymentID.ToString());
                        try
                        {
                            string[] vals = enc.Decrypt(_bp.TheSession.EncryptedSessionPaymentInformation[((NPOrderPayment)_order.Payments[0]).OrderPaymentID].ToString()).Split(',');
                            if (vals.Length != 2 || vals[0] == "" || vals[1] == "")
                            {
                                //empty values in session
                                removepayment = true;
                            }
                        }
                        catch
                        {
                            //problem with data in session
                            removepayment = true;
                        }
                    }
                    else
                    {
                        //nothing in session for this payment
                        removepayment = true;
                    }
                }
            }
            if (removepayment)
            {
                ((NPOrderPayment)_order.Payments[0]).Delete();
                _order.ReloadPayments();
            }

            grid.DataSource = _order.Payments;
            grid.DataBind();
            grid.Visible = _order.Payments.Count > 0;
            btnDelete.Visible = _order.Payments.Count > 0;
            if (_order.Payments.Count > 0)
            {
                ViewState["PaymentID"] = ((NPOrderPayment)_order.Payments[0]).PaymentID.ToString();
            }
        }

        private void BindCountryList()
        {
            NPListBinder.PopulateCountryList(ddlCountry, _bp.Encoding.Substring(_bp.Encoding.Length - 2, 2).ToUpper(), false, false, _bp.Connection);
            NPListBinder.PopulateStateAbbrList(ddlState, "", ddlCountry.SelectedValue, _bp.Connection, false, true);
            bool stateListVisible = (ddlState.Items.Count > 0) && ddlState.Visible;
            ltlState.Visible = stateListVisible;
            ddlState.Visible = stateListVisible;
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            NPListBinder.PopulateStateAbbrList(ddlState, "", ddlCountry.SelectedValue, _bp.Connection, false, true);
            bool stateListVisible = (ddlState.Items.Count > 0) && ddlState.Visible;
            ltlState.Visible = stateListVisible;
            ddlState.Visible = stateListVisible;
        }        

        private void BindWallet()
        {
            NPUser user = new NPUser(_order.UserID);
            ddlPaymentMethod.DataSource = user.GetEligiblePayments(_order.OrderID, _bp.Encoding);
            ddlPaymentMethod.DataValueField = "TheValue";
            ddlPaymentMethod.DataTextField = "TheText";
            ddlPaymentMethod.DataBind();
            if (ddlPaymentMethod.Items.Count > 0)
            {
                int paymentid = 0;
                if (_order.Payments.Count > 0)
                {
                    paymentid = ((NPOrderPayment)_order.Payments[0]).PaymentID;
                }
                else if (ViewState["PaymentID"] != null)
                {
                    int.TryParse(ViewState["PaymentID"].ToString(), out paymentid);
                }
                ddlPaymentMethod.SelectedIndex = ddlPaymentMethod.Items.IndexOf(ddlPaymentMethod.Items.FindByValue(paymentid.ToString()));
                GetAccountInfo(Convert.ToInt32(ddlPaymentMethod.SelectedValue));
            }
        }

        private void GetAccountInfo(int paymentid)
        {
            this.ClearDetail();
            NPPaymentOption po;
            if (paymentid > 0)
            {
                po = new NPPaymentOption(paymentid);
                ToggleAll(po);

                if (po.Message != null && po.Message.Length > 0)
                {
                    sysMessage.Text = po.Message;
                }
                pnlCharge.Visible = po.PaymentCharge > 0;
                prcCharge.Price = (decimal)po.PaymentCharge;
            }
            ViewState["PaymentID"] = paymentid;
        }

        private void BindTotals()
        {
            List<NPPair<string, decimal>> totals = new List<NPPair<string, decimal>>();

            totals.Add(new NPPair<string, decimal>(ltlOrderTotal.Text, _order.GrandTotal));
            //totals.Add(new NPPair<string, decimal>(ltlPay.Text, _order.PaymentTotal));

            if (_bp.IsMultiCurrency)
            {
                foreach (DataControlField dcf in summaryGrid.Columns)
                {
                    dcf.Visible = true;
                }
            }

            summaryGrid.DataSource = totals;
            summaryGrid.DataBind();
        }

        public bool SubmitPayment()
        {
            NPBasePage bp = (NPBasePage)this.Page;
            ClearDetail();

            int paymentid = Convert.ToInt32(ViewState["PaymentID"]);
            NPPaymentOption po = null;
            DateTime expirationDate = DateTime.MinValue;
            DateTime issueDate = DateTime.MinValue;

            if (pnlNew.Visible)
            {
                bool isValid = true;
                if (paymentid > 0)
                {                    
                    po = new NPPaymentOption(paymentid);                    
                    if (po.UseCardholder)
                    {
                        if (txtCardHolder.Text.Length == 0)
                        {
                            imgCardHolderWarning.Visible = true;
                            rowHolder.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                    if (po.UseAddress)
                    {
                        if (txtStreet.Text.Length == 0)
                        {
                            imgStreetWarning.Visible = true;
                            rowStreet.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                        if (txtCity.Text.Length == 0)
                        {
                            imgCityWarning.Visible = true;
                            rowCity.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                        if (txtPostalCode.Text.Length == 0)
                        {
                            imgPostalCodeWarning.Visible = true;
                            rowPostalCode.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                    if (po.UseBankCardNumber)
                    {
                        if (txtAccountNumber.Text.Length == 0 || !po.IsValidCardNumber(txtAccountNumber.Text, txtVerificationNumber.Text))
                        {
                            imgAccountNumberWarning.Visible = true;
                            rowAcctNum.CssClass += " has-error has-feedback";
                            imgVerificationWarning.Visible = true;
                            rowVerificationNum.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                    if (po.UseEmail && (txtEmail.Text.Length == 0 || !NPValidator.IsEmail(txtEmail.Text)))
                    {
                        imgEmailWarning.Visible = true;
                        rowEmail.CssClass += " has-error has-feedback";
                        isValid = false;
                    }
                    if (po.UsePhone && txtPhone.Text.Length == 0)
                    {
                        imgPhoneWarning.Visible = true;
                        rowPhone.CssClass += " has-error has-feedback";
                        isValid = false;
                    }
                    if (po.UseIssueDate)
                    {
                        issueDate = new DateTime(Convert.ToInt32(ddlIssueYear.SelectedValue), Convert.ToInt32(ddlIssueMonth.SelectedValue), 1);
                        if (issueDate > DateTime.Now)
                        {
                            imgIssueDateWarning.Visible = true;
                            rowIssueDate.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                    if (po.UseIssueNumber)
                    {
                        if (txtIssueNumber.Text.Length == 0)
                        {
                            imgIssueNumberWarning.Visible = true;
                            rowIssueNumber.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                    expirationDate = DateTime.MinValue;
                    if (po.UseExpiration)
                    {
                        expirationDate = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);
                        expirationDate = expirationDate.AddMonths(1).AddMinutes(-1);
                        if (expirationDate < DateTime.Now)
                        {
                            imgExpirationDateWarning.Visible = true;
                            rowExpDate.CssClass += " has-error has-feedback";
                            isValid = false;
                        }
                    }
                }
                else
                {
                    if (txtVerificationNumber.Text.Length == 0)
                    {                            
                        imgVerificationWarning.Visible = true;
                        rowVerificationNum.CssClass += " has-error has-feedback";
                        isValid = false;
                    }                    
                }
                if (!isValid) return false;
            }

            BindAll();
            return true;
        }

        public void AddPayment()
        {
            int paymentid = 0;
            NPPaymentOption po = null;
            if (int.TryParse(ViewState["PaymentID"].ToString(), out paymentid))
            {
                po = new NPPaymentOption(paymentid);
            }            

            NPOrderPayment op = null;
            // there should be only one payment
            foreach (NPOrderPayment p in _order.Payments)
            {
                op = p;
            }
            if (op == null)
            {
                op = new NPOrderPayment();
                op.UserID = _bp.UserID;
                op.AccountID = _bp.AccountID;
                op.OrderID = _order.OrderID;
                if (paymentid > 0)
                {
                    op.PaymentID = po.PaymentID;
                    op.PaymentName = po.PaymentName;
                    op.BillingName = txtCardHolder.Text;
                    op.AccountNumber = txtAccountNumber.Text;
                    op.ExpirationMonth = ddlMonth.SelectedValue;
                    op.ExpirationYear = ddlYear.SelectedValue;
                    if (po.UseAddress)
                    {
                        op.Street = txtStreet.Text;
                        op.City = txtCity.Text;
                        op.State = ddlState.SelectedValue;
                        op.Country = ddlCountry.SelectedValue;
                        op.Zip = txtPostalCode.Text;
                    }
                    if (po.UseIssueDate)
                    {
                        op.IssueMonth = ddlIssueMonth.SelectedValue;
                        op.IssueYear = ddlIssueYear.SelectedValue;
                    }
                    if (po.UseIssueNumber)
                    {
                        op.IssueNumber = txtIssueNumber.Text;
                    }
                    if (po.UseEmail)
                    {
                        op.Email = txtEmail.Text;
                    }
                    if (po.UsePhone)
                    {
                        op.Phone = txtPhone.Text;
                    }
                }                
            }
            op.PaymentAmount = _order.GrandTotal - _order.PaymentMethodTotal + po.PaymentCharge;
            op.Save();

            if (pnlNew.Visible)
            {
                if (txtVerificationNumber.Text != "" && txtAccountNumber.Text != "")
                {
                    //save payment info in session associated with payment id              
                    Encryption enc = new Encryption(op.OrderPaymentID.ToString());
                    _bp.TheSession.EncryptedSessionPaymentInformation[op.OrderPaymentID] = enc.Encrypt(txtVerificationNumber.Text + "," + txtAccountNumber.Text);
                }
            }

            _order.ReloadPayments();            
        }

        private void ClearDetail()
        {
            imgCardHolderWarning.Visible = false;
            imgStreetWarning.Visible = false;
            imgCityWarning.Visible = false;
            imgPostalCodeWarning.Visible = false;
            imgStateWarning.Visible = false;
            imgCountryWarning.Visible = false;
            imgEmailWarning.Visible = false;
            imgPhoneWarning.Visible = false;
            imgIssueDateWarning.Visible = false;
            imgIssueNumberWarning.Visible = false;
            imgAccountNumberWarning.Visible = false;
            imgVerificationWarning.Visible = false;
            imgExpirationDateWarning.Visible = false;
            imgAmount2Warning.Visible = false;
        }

        protected void ddlPaymentMethod_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            txtCardHolder.Text = "";
            txtAccountNumber.Text = "";
            txtVerificationNumber.Text = "";
            GetAccountInfo(Convert.ToInt32(ddlPaymentMethod.SelectedValue));
            BindTotals();
            txtAmount2.Text = _order.Balance.ToString("0.00");
        }

        #region "Toggle"               

        private void ToggleAll(NPPaymentOption po) 
        {
            ToggleCardHolder(po.UseCardholder);
            ToggleAddress(po.UseAddress);
            ToggleBankCardNumber(po.UseBankCardNumber);
            ToggleIssueDate(po.UseIssueDate);
            ToggleIssueNumber(po.UseIssueNumber);
            ToggleExpiration(po.UseExpiration);
            ToggleEmail(po.UseEmail);
            TogglePhone(po.UsePhone);
        }        

        private void ToggleCardHolder(bool show)
        {
            rowHolder.Visible = show;
            ltlHolder.Visible = show;
            txtCardHolder.Visible = show;
            ltlAcctNum.Visible = show;
            txtAccountNumber.Visible = show;
        }

        private void ToggleAddress(bool show)
        {
            rowCountry.Visible = show;
            rowStreet.Visible = show;
            rowCity.Visible = show;
            rowState.Visible = show;
            rowPostalCode.Visible = show;

            ltlStreet.Visible = show;
            ltlPostalCode.Visible = show;
            ltlCity.Visible = show;
            ltlState.Visible = show;
            ltlCountry.Visible = show;
            txtStreet.Visible = show;
            txtPostalCode.Visible = show;
            txtCity.Visible = show;
            ddlState.Visible = show;
            ddlCountry.Visible = show;
        }

        private void ToggleBankCardNumber(bool show)
        {
            rowAcctNum.Visible = show;
            rowVerificationNum.Visible = show;
            ltlAcctNum.Visible = show;
            txtAccountNumber.Visible = show;
            ltlVerificationNumber.Visible = show;
            txtVerificationNumber.Visible = show;
            imgVerification.Visible = show;
        }

        private void ToggleIssueDate(bool show)
        {
            rowIssueDate.Visible = show;
            ltlIssueDate.Visible = show;
            ddlIssueMonth.Visible = show;
            ddlIssueYear.Visible = show;
        }

        private void ToggleIssueNumber(bool show)
        {
            rowIssueNumber.Visible = show;
            ltlIssueNumber.Visible = show;
            txtIssueNumber.Visible = show;
        }

        private void ToggleExpiration(bool show)
        {
            rowExpDate.Visible = show;
            ltlExpirationDate.Visible = show;
            ddlMonth.Visible = show;
            ddlYear.Visible = show;
        }

        private void ToggleEmail(bool show)
        {
            rowEmail.Visible = show;
            ltlEmail.Visible = show;
            txtEmail.Visible = show;
        }

        private void TogglePhone(bool show)
        {
            rowPhone.Visible = show;
            ltlPhone.Visible = show;
            txtPhone.Visible = show;
        }

        #endregion

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton btn = (ImageButton)e.Row.FindControl("btnDelete");
            if (btn != null)
            {
                btn.ImageUrl = _bp.ResolveImage("buttons", "remove.gif");
            }
            //  deals with illegal post back error (row id is regenerated by .net, causing error if rebound on postback)
            e.Row.ID = e.Row.RowType.ToString() + "_" + e.Row.RowIndex.ToString();
        }

        protected void DeletePayment(object sender, EventArgs e)
        {
            foreach (NPOrderPayment op in _order.Payments)
            {
                op.Delete();
            }
            _order.ReloadPayments();
            BindAll();
            txtAmount2.Text = _order.Balance.ToString("0.00");
        }        
    }
}
