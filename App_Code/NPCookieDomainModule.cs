using System;
using System.Web;
using System.Text.RegularExpressions;
using netpoint.api.encryption;

public class NPCookieDomainModule : IHttpModule
{
   private HttpApplication httpApp;

   public void Init(HttpApplication httpApp)
   {
      this.httpApp = httpApp;
      httpApp.PreSendRequestHeaders += new EventHandler(OnSendHeaders);
      httpApp.PreRequestHandlerExecute += new EventHandler(OnReceiveHeaders);
   }

   void OnSendHeaders(object sender, EventArgs a)
   {
      HttpApplication application = (HttpApplication)sender;
      HttpContext context = application.Context;

      var prefix = "NPX";
      var cookies = context.Response.Cookies;
      var regex = new Regex("[^\\.]+\\.[^\\.]+$");
      
      foreach(String cookie in cookies) {
         if(cookie.StartsWith(prefix)) {
            cookies[cookie].Domain = regex.Match(context.Request.Url.Host).Value;
            if(cookie == "NPX.") {
               cookies[cookie].Value = RMEncryption.EncryptString(
                  RMEncryption.DecryptString(cookies[cookie].Value, context.Request.Url.Host),
                  cookies[cookie].Domain);
            }
         }
      }      
   }

   void OnReceiveHeaders(object sender, EventArgs a)
   {
      HttpApplication application = (HttpApplication)sender;
      HttpContext context = application.Context;

      var prefix = "NPX";
      var cookies = context.Request.Cookies;
      var regex = new Regex("[^\\.]+\\.[^\\.]+$");
      
      foreach(String cookie in cookies) {
         if(cookie.StartsWith(prefix)) {
            if(cookie == "NPX.") {
               cookies[cookie].Domain = regex.Match(context.Request.Url.Host).Value;
               cookies[cookie].Value = RMEncryption.EncryptString(
                  RMEncryption.DecryptString(cookies[cookie].Value, cookies[cookie].Domain),
                  context.Request.Url.Host);
            }
         }
      }      
   }

   public void Dispose()
   {}
}