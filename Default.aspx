<%@ Page language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.homepage" CodeBehind="default.aspx.cs" %>
<%@ Import Namespace="netpoint.api.common" %>
<%@ Import Namespace="netpoint.api.data" %>
<%@ Import Namespace="System.Net" %>
<script language="C#" runat="server">
protected void Page_Load(object sender, System.EventArgs e){
    ServicePointManager.SecurityProtocol = (SecurityProtocolType) 3072;
    var mckleinRegex = new Regex("^(www\\.)?mcklein[^.]*");
    var parindaRegex = new Regex("^(www\\.)?[^.]*parinda");
    var siamodRegex = new Regex("^(www\\.)?siamod");
    Uri referrer = Request.UrlReferrer;
    //if (referrer == null || !(referrer.Authority.ToLower().Contains("shopparinda.com") || referrer.Authority.ToLower().Contains("siamod.com") || referrer.Authority.ToLower().Contains("mckleinusa.com")))
    {
        //coming from outside, make sure the correct theme is being used and not cached

        //lookup default theme based on domain
        string serverid = Request.QueryString["serverid"];
        if(mckleinRegex.IsMatch(Request.ServerVariables["HTTP_HOST"])) {
            serverid = "mcklein";
        } else if(siamodRegex.IsMatch(Request.ServerVariables["HTTP_HOST"])) {
            serverid = "siamod";
        } else if(parindaRegex.IsMatch(Request.ServerVariables["HTTP_HOST"])) {
            serverid = "parinda";
        }
        NPTheme theme = NPTheme.ResolveTheme(serverid, "", Request.ServerVariables["HTTP_HOST"], new NPConnection(ConnectionString));
        //if themes don't match then clear session theme and redirect
        if (TheTheme.ServerID != theme.ServerID)
        {
            TheSession.ServerID = theme.ServerID;
            //TheSession.SetCookie();
            Response.Redirect(Request.Url.ToString(), true);
        }   
    }
    base.Page_Load(sender, e);
}
</script>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:PlaceHolder id="phMainSlot" runat="server" EnableViewState="False">
    
    </asp:PlaceHolder>
</asp:Content>

