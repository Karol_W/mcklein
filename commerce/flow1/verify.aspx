﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/commerce.master" AutoEventWireup="true" CodeBehind="verify.aspx.cs" Inherits="netpoint.commerce.flow1.verify" %>
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="CartListBlock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<%@ Register TagPrefix="np" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right white-bg">
		<center>
			<asp:Label ID="sysErrorMessage" runat="server" CssClass="npwarning"></asp:Label><br /><br />
			<asp:Image ID="topimage" runat="server" ImageUrl="~/assets/common/images/checkout3.gif"></asp:Image><br />
		</center>
		<table cellspacing="0" cellpadding="3" width="100%" border="0" class="review-address-wrapper">
			<tr>
				<td class="npsubheader"><asp:Literal ID="ltlShippingTo" runat="server" Text="Shipping To"></asp:Literal></td>
				<td class="npsubheader"><asp:Literal ID="ltlBillingTo" runat="server" Text="Billing To"></asp:Literal></td>
			   <td class="npsubheader"><asp:Literal ID="ltlShippingVia" runat="server" Text="Shipping Via"></asp:Literal></td>
			</tr>
			<tr>
				<td class="npbody" valign="top">
					<asp:HyperLink ID="syslinksa" runat="server" NavigateUrl="shipping.aspx">
						<asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal>
					</asp:HyperLink></td>
				<td class="npbody" valign="top">
					<asp:HyperLink ID="syslinkba" runat="server" NavigateUrl="billing.aspx">
						<asp:Literal runat="server" ID="sysBillingAddress"></asp:Literal>
					</asp:HyperLink></td>
			   <td class="npbody" valign="top">
					<asp:HyperLink ID="syslinksm" runat="server" NavigateUrl="shipping.aspx">
						<asp:Literal runat="server" ID="sysShipMethod"></asp:Literal>
					</asp:HyperLink></td>
			</tr>
			<tr id="rowB2B" runat="server">
				<td id="cellRequestedShipDate" runat="server" class="npbody" valign="top" visible="false">
					<asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Shipping Date"></asp:Literal><br />
					<np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
					<asp:Image ID="imgRequestedShipDate" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
				</td>
				<td id="cellPONo" runat="server" class="npbody" valign="top" colspan="2" visible="false">
					<asp:literal id="ltlPONo" runat="server" text="PO #"></asp:literal><br />
					<asp:TextBox id="txtPO" runat="server" width="175px"></asp:TextBox>
				</td>
			</tr>
		</table>
		<br />
		<np:CartListBlock ID="CartList" runat="server" ShowDiscount="true" AllowEditDiscount="false" DisableQuantityUpdate="true" DisableItemDelete="true"></np:CartListBlock>
		<br />
		<asp:Panel ID="pnlOrderNotes" runat="server" CssClass="npbody" Visible="false">
		<asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label><br />
		<np:MultiLineTextBox id="txtNotes" runat="server" LimitInput="150" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></np:MultiLineTextBox>
		<asp:Image ID="imgNotes" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image><br /><br />
		</asp:Panel>    
	 
		<table cellspacing="0" cellpadding="3" width="100%" border="0">
			<tr>
				<td class="npsubheader" style="width:50%;"><asp:Literal ID="ltlPayments" runat="server" Text="Payments"></asp:Literal></td>
				<td class="npsubheader" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td class="npbody" valign="top">
					<asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" Width="100%"
						GridLines="None" CellPadding="3" BorderStyle="None" ShowHeader="False" OnRowDataBound="gvPayments_RowDataBound">
						<RowStyle CssClass="npbody"></RowStyle>
						<HeaderStyle CssClass="npsubheader"></HeaderStyle>
						<Columns>
							<asp:HyperLinkField DataTextField="PaymentName" HeaderText="ltlCurrentPayments|Current Payments" NavigateUrl="billing.aspx"></asp:HyperLinkField>
							<asp:BoundField DataField="MaskedNumber" HeaderText="ltlAccount|Account"></asp:BoundField>
							<asp:TemplateField HeaderText="ltlAmount|Amount">
								<ItemTemplate>
									<np:PriceDisplay ID="prcAmount" runat="server" DisplayCurrency="Both" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
					<br />
					<asp:Image ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" runat="server"></asp:Image>
					<asp:Label ID="lblRealError" runat="server" CssClass="npwarning"></asp:Label><br /><br />           
					<asp:Label ID="lblOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="Some parts in your order are on back order"></asp:Label><br /><br />
					<asp:Image ID="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False" runat="server"></asp:Image>
					<asp:Label ID="lblUnavailableOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="This item is out of stock and cannot be backordered"></asp:Label><br /><br />
					<asp:Image runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
					<asp:Label ID="lblUnavailable" runat="server" Text="This part is no longer available; remove it from the cart before you check out"
						Visible="False" CssClass="npwarning"></asp:Label></td>
				<td class="npbody" valign="top" align="right">
					<table id="cart-page-total" cellspacing="0" cellpadding="2" border="0">
						<tr>
							<td class="npbody" align="right" colspan="2"><np:OrderExpenses ID="expenses" runat="server" /></td>
						</tr>
						<tr>
							<td class="npbody">
								<asp:Literal Visible="false" ID="ltlPay" runat="server" Text="Payments:"></asp:Literal>
								<np:PriceDisplay Visible="false" ID="sysPaymentTotal" DisplayCurrency="Both" runat="server" />
								<asp:Literal Visible="false" ID="ltlBal" runat="server" Text="Balance:"></asp:Literal>
								<np:PriceDisplay Visible="false" ID="sysBalance" DisplayCurrency="Both" runat="server" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="npbody" valign="top" align="right" colspan="2"><table class="npbody" width="100%" cellspacing="0" cellpadding="0"><tr>
					<td class="npbody" align="left">
						<np:MCWarning ID="mcWarning" runat="server" />
					</td>
					<td class="npbody" align="right">
						<asp:Panel ID="pnlButton" runat="server" >
							<asp:ImageButton ID="btnOrder" runat="server" ImageUrl="~/assets/common/buttons/placeyourorder.gif" ToolTip="Order" />
							<asp:ImageButton ID="btnSubmitForApproval" runat="server" ImageUrl="~/assets/common/buttons/submitforapproval.gif" Visible="false" ToolTip="Submit for Approval" />            
						</asp:Panel>
					</td>    
				</tr></table></td>
			</tr>
		</table>   
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errTotalPaymentsMustMatch" runat="server" Text="Total Payments must match the Total of the Order" Visible="False"></asp:Literal>
    <asp:Literal ID="errSelectSaveBillAddress" runat="server" Text="Please Select or Save a Bill To Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errSelectSaveShipAddress" runat="server" Text="Please Select or Save a Delivery Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNotAuthorized" runat="server" Text="Credit Card Not Authorized" Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNotCharged" runat="server" Text="Credit Card Not Charged" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnOrderHasNoPayment" runat="server" Text="The order does not have a payment."></asp:Literal>
    <asp:Literal ID="hdnPaymentHasNoProvider" runat="server" Text="The payment selected on the order has no provider."></asp:Literal>
    <asp:Literal ID="hdnProviderDoesNotExist" runat="server" Text="The provider selected on the payment does not exist."></asp:Literal>
    <asp:Literal ID="errPaymentMismatch" runat="server" Text="The payment total and order total did not match.  Please verify the information and place order again." Visible="False"></asp:Literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
</asp:Content>
