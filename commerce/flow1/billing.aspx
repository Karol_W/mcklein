﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/commerce.master" AutoEventWireup="true" CodeBehind="billing.aspx.cs" Inherits="netpoint.commerce.flow1.billing" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PaymentBlock" Src="~/commerce/controls/PaymentBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <div class="col-md-9 pull-right white-bg">
		<asp:label id="sysErrorMessage" runat="server" font-bold="true" ForeColor="red"></asp:label>
		<center>
			<asp:image runat="server" id="topimage" imageurl="~/assets/common/images/checkout2.gif"></asp:image><br />
		</center>
		<table id="tblAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
			<tbody>
				<tr>
					<td class="npsubheader" style="width:50%;"><asp:literal id="ltlAddBillingAddress" runat="server" text="Billing Address"></asp:literal></td>
					<td class="npsubheader" style="width:50%;"><asp:literal id="ltlBillingAddress" runat="server" text="Choose from your saved addresses"></asp:literal></td>
				</tr>
				<tr>
					<td valign="top">
						<np:AddressBlock ID="AddAddress" runat="server" ShowSave="false" ShipDestination="false" HideAddressType="true"></np:AddressBlock></td>
					<td valign="top">
						<asp:radiobuttonlist id="rblBillingAddress" runat="server" autopostback="True" width="100%"
							repeatcolumns="1" repeatdirection="Horizontal" cellspacing="0" cellpadding="2"
							cssclass="npbody" datatextfield="AddressHTML" onselectedindexchanged="rblBillingAddress_SelectedIndexChanged"></asp:radiobuttonlist>
						<asp:HyperLink ID="lnkShowShippingImage" runat="server" ImageUrl="~/assets/common/icons/expand.gif" 
							Text="Show Delivery Addresses" CssClass="npbody" NavigateUrl="javascript:toggleShippingAddress()"></asp:HyperLink>
						<asp:HyperLink ID="lnkShowShipping" runat="server" Text="Show Delivery Addresses" CssClass="npbody" 
							NavigateUrl="javascript:toggleShippingAddress()"></asp:HyperLink>
						<asp:HyperLink ID="lnkHideShippingImage" runat="server" ImageUrl="~/assets/common/icons/collapse.gif" 
							Text="Hide Delivery Addresses" CssClass="npbody" NavigateUrl="javascript:toggleShippingAddress()"></asp:HyperLink>
						<asp:HyperLink ID="lnkHideShipping" runat="server" Text="Hide Delivery Addresses" CssClass="npbody" 
							NavigateUrl="javascript:toggleShippingAddress()"></asp:HyperLink>                        
						<asp:radiobuttonlist id="rblShippingAddress" runat="server" autopostback="True" width="100%"
							repeatcolumns="1" repeatdirection="Horizontal" cellspacing="0" cellpadding="2"
							cssclass="npbody" datatextfield="AddressHTML" onselectedindexchanged="rblShippingAddress_SelectedIndexChanged"></asp:radiobuttonlist>   
					</td>
				</tr>
			</tbody>
		</table>
		<asp:panel id="pnlB2B" runat="server" visible="False">
			<br />
			<table cellspacing="0" cellpadding="2" width="100%" border="0">
				<tr>
					<td class="npsubheader"><asp:literal id="ltlPONo" runat="server" text="PO #"></asp:literal></td>
				</tr>
				<tr>
					<td class="npbody"><asp:TextBox id="txtPO" runat="server" width="175px"></asp:TextBox></td>
				</tr>
			</table>
		</asp:panel>
		<br />
		<np:PaymentBlock ID="paymentBlock" runat="server"></np:PaymentBlock>
		<br />
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="npbody">
					<np:MCWarning id="mcWarning" runat="server" />
				</td>
				<td align="right"><asp:imagebutton id="btnNext" runat="server" imageurl="~/assets/common/buttons/continue.gif" ToolTip="Next"></asp:imagebutton>
				</td>
			</tr>
		</table>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">    
    <asp:literal id="errNoOrder" runat="server" visible="False" text="Critical Error : No Order was found."></asp:literal>
    <asp:literal id="errTotalPayments" runat="server" visible="False" text="Total Payments must match the Total of the Order"></asp:literal>
    <asp:literal id="errSelectBillAddress" runat="server" visible="False" text="Please Select or Save a Bill To Address"></asp:literal>
    <asp:literal id="errSelectShipAddress" runat="server" visible="False" text="Please Select or Save a Ship To Address"></asp:literal>
    <asp:literal id="errCardNotAuthorized" runat="server" visible="False" text="Credit Card Not Authorized"></asp:literal>
    <asp:literal id="errCardNotCharged" runat="server" visible="False" text="Credit Card Not Charged"></asp:literal>
    <asp:literal id="hdnNewAddress" runat="server" text="New Address" visible="False"></asp:literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
</asp:Content>
