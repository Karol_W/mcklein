﻿This checkout flow combines the shipping method selection with the shipping address, payment options with billing address
and the verify page only displays readonly data to be confirmed

To enable this checkout flow, create at least there 3 webflow settings
Source Page: commerce/cart.aspx
Source Page: common/accounts/createaccount.aspx
Source Page: common/accounts/login.aspx

Next Page: commerce/flow1/shipping.aspx