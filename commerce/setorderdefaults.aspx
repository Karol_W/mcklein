<%@ Page Language="c#" MasterPageFile="~/masters/commerce.master" Inherits="netpoint.commerce.setorderdefaults" Codebehind="setorderdefaults.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:Label ID="sysErrorMessage" runat="server" font-bold="true" ForeColor="red"></asp:Label><br />
    <div id="divDefaults" runat="server" class="npbody"><asp:Literal ID="ltlDefaults" runat="server" Mode="PassThrough">The defaults can be set on the <a href="../common/accounts/accountinfo.aspx" class="npbody">Account Information</a> screen.</asp:Literal></div>
    <div id="divUserProfile" runat="server" class="npbody"><asp:Literal ID="ltlUserProfile" runat="server" Mode="passThrough">User's Dayphone Number can be set on the <a href="../common/user/profile.aspx">My Profile</a> screen.</asp:Literal></div><br />
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errNoOrder" runat="server" visible="False" text="No order found for the current user."></asp:Literal>
    <asp:Literal ID="errNoDefaultShipOption" runat="server" visible="False" text="Default Delivery Option is undefined."></asp:Literal>
    <asp:Literal ID="errNoDefaultShipAddress" runat="server" visible="False" text="Default Delivery Address is undefined."></asp:Literal>
    <asp:Literal ID="errNoDefaultBillOption" runat="server" visible="False" text="Default Billing Option is undefined."></asp:Literal>
    <asp:Literal ID="errNoDefaultBillAddress" runat="server" visible="False" text="Default Billing Address is undefined."></asp:Literal>
    <asp:Literal ID="errNoPaymentOption" runat="server" visible="False" text="Payment Terms not found."></asp:Literal>
    <asp:Literal ID="errPOHasRequirements" runat="server" visible="False" text="The Default Billing Option requires additional information, it cannot be used in the fast checkout process."></asp:Literal>
    <asp:Literal ID="errNoDayPhone" runat="server" visible="False" text="User's Dayphone Number is undefined."></asp:Literal>
</asp:Content>