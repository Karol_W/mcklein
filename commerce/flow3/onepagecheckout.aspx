﻿<%@ Page Language="C#" MasterPageFile="~/masters/commerce.master" CodeBehind="onepagecheckout.aspx.cs" Inherits="netpoint.commerce.flow3.onepagecheckout" EnableEventValidation="false" %>
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="CartListBlock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<%@ Register TagPrefix="np" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PaymentBlock" Src="~/commerce/controls/PaymentBlock.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<script type="text/javascript">
    function selectProduct(param) {
        clbackTotals.callback(param);
    }

    function clbackTotals_onload(sender, e) {
        var oldPrefix = sender.CallbackPrefix;
        var newPrefix = document.location.href;
        //alert('old: ' + oldPrefix + '\nnew: ' + newPrefix);
        sender.CallbackPrefix = newPrefix
    }
</script>


<div class="col-md-9 pull-right white-bg">
    <h1>Checkout</h1>
    <asp:label id="sysErrorMessage" runat="server" font-bold="true" ForeColor="red"></asp:label>
    <table id="tblEditAddress" runat="server" cellspacing="0" cellpadding="3" width="100%" border="0">
        <tr>
            <td class="npsubheader"><asp:Literal ID="ltlEditBillingTo" runat="server" Text="Billing To"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlBillingAddress" Style="max-height: 400px; max-width: 400px" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="400" OnSelectedIndexChanged="ddlBillingAddress_SelectedIndexChanged" /><br />
                <np:AddressBlock ID="BillingAddressBlock" runat="server" ShowSave="false" ShipDestination="false" HideAddressType="true"></np:AddressBlock>
            </td>
        </tr>
        <tr>
            <td class="npsubheader"><asp:Literal ID="ltlEditPhone" runat="server" Text="Telephone Numbers"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
			            <td class="npadminlabel"><asp:literal id="ltlDayPhone" Text="Day Phone " runat="server"></asp:literal></td>
			            <td class="npbody">
				            <asp:TextBox ID="txtDayPhone" runat="server" Width="175px"></asp:TextBox>
				            <asp:Image ID="imgPhoneWarning" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
				        </td>				
		            </tr>
		            <tr>
		                <td class="npadminlabel"><asp:Literal ID="ltlEveningPhone" runat="server" Text="Evening Phone "></asp:Literal></td>
		                <td class="npbody">
		                    <asp:TextBox ID="txtEveningPhone" runat="server" Width="175px"></asp:TextBox>
		                </td>
		            </tr>
		        </table>
            </td>
        </tr>
        <tr>
            <td>
            <asp:Literal ID="ltlDifferentShipAddress" runat="server" Text="Use a different shipping address"></asp:Literal>                
                <asp:CheckBox ID="cbxUseUniqueShippingAddress" runat="server" OnCheckedChanged="cbxUseUniqueShippingAddress_CheckedChanged" AutoPostBack="true" />
            </td>
        </tr>
        <tr id="rowEditShipAddressHeader" runat="server" visible="false">
            <td class="npsubheader"><asp:Literal ID="ltlEditShippingTo" runat="server" Text="Shipping To"></asp:Literal></td>     
        </tr>
        <tr id="rowEditShipAddress" runat="server" visible="false">
            <td>
                <asp:DropDownList ID="ddlShippingAddress" Style="max-height: 400px; max-width: 400px" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="400" OnSelectedIndexChanged="ddlShippingAddress_SelectedIndexChanged" /><br />       
                <np:AddressBlock ID="ShippingAddressBlock" runat="server" ShowSave="false" ShipDestination="true" HideAddressType="true"></np:AddressBlock>
            </td>  
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:ImageButton ID="btnSaveAddress" runat="server" ImageUrl="~/assets/common/buttons/save-btn.png" ToolTip="Save" />
            </td>
        </tr>
    </table>
    <table id="tblViewAddress" runat="server" visible="false" cellspacing="0" cellpadding="3" width="100%" border="0">
        <tr>
            <td class="npsubheader"><asp:Literal ID="ltlBillingTo" runat="server" Text="Billing To"></asp:Literal></td>
            <td class="npsubheader"><asp:Literal ID="ltlShippingTo" runat="server" Text="Shipping To"></asp:Literal></td>           
        </tr>        
        <tr>
            <td class="npbody" valign="top">                
                <asp:Literal runat="server" ID="sysBillingAddress"></asp:Literal><br /><br />  
                <asp:Literal runat="server" ID="sysDayPhone"></asp:Literal><br />
                <asp:Literal runat="server" ID="sysEveningPhone"></asp:Literal>                                         
            </td>
            <td class="npbody" valign="top">
                <asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal>                
            </td>           
        </tr> 
        <tr>
            <td colspan="2" align="right">
                <asp:ImageButton ID="btnEditAddress" runat="server" ImageUrl="~/assets/common/buttons/editinfo.gif" ToolTip="Edit" />
            </td>
        </tr>            
    </table>  
    <table>
        <tr id="rowB2B" runat="server">
            <td class="npbody" valign="top">
                <asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Shipping Date"></asp:Literal><br />
                <np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
                <asp:Image ID="imgRequestedShipDate" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
            </td>
            <td class="npbody" valign="top" colspan="2">
                <asp:literal id="ltlPONo" runat="server" text="PO #"></asp:literal><br />
                <asp:TextBox id="txtPO" runat="server" width="175px"></asp:TextBox>
            </td>
        </tr>      
    </table>  
	<br />	
    <np:CartListBlock ID="CartList" runat="server" ShowDiscount="true" AllowEditDiscount="false" DisableQuantityUpdate="true" DisableItemDelete="true"></np:CartListBlock>    
    <table cellspacing="0" cellpadding="3" width="100%" border="0">
        <tr>
            <td>
                <asp:Image ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" runat="server"></asp:Image>                
                <asp:Label ID="lblOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="Some parts in your order are on back order"></asp:Label>
                <asp:Image ID="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False" runat="server"></asp:Image>
                <asp:Label ID="lblUnavailableOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="This item is out of stock and cannot be backordered"></asp:Label>
                <asp:Image runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
                <asp:Label ID="lblUnavailable" runat="server" Text="This part is no longer available; remove it from the cart before you check out" Visible="False" CssClass="npwarning"></asp:Label>
            </td>
        </tr>
    </table>    
    <asp:Panel ID="pnlOrderNotes" runat="server" CssClass="npbody" Visible="false">
        <asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label><br />
        <np:MultiLineTextBox id="txtNotes" runat="server" LimitInput="150" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></np:MultiLineTextBox>
        <asp:Image ID="imgNotes" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image><br /><br />
    </asp:Panel>
	<br />
	<asp:Panel ID="pnlAddressDependantInfo" runat="server" Enabled="false">
	
	<ComponentArt:CallBack id="clbackTotals" CssClass="CallBack" runat="server" OnCallback="clbackTotals_Callback" >
	    <ClientEvents>
	        <Load EventHandler="clbackTotals_onload" />
	    </ClientEvents>
        <Content>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server">	
	        <table id="tblShipInfo" cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr>
                    <td class="npsubheader" style="width:50%;" colspan="2"><asp:Literal ID="ltlShippingProvider" runat="server" Text="Delivery Provider"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="npbody">
                        <asp:RadioButtonList ID="rblShipMethod" runat="server" AutoPostBack="False" CssClass="npbody"
                            CellPadding="2" CellSpacing="0">
                        </asp:RadioButtonList>              
                    </td>
                    <td align="right">                
                        <np:OrderExpenses ID="expenses" runat="server" />                                                                   
                    </td>
                </tr> 
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblShippingError" runat="server" CssClass="npwarning"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <np:PaymentBlock ID="paymentBlock" runat="server"></np:PaymentBlock>    
                    </td>            
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:ImageButton ID="btnAddPayment" runat="server" ImageUrl="~/assets/common/buttons/addpayment.gif" ToolTip="Add Payment" OnCommand="AddPayment" />        
                    </td>
                </tr>
            </table> 
            
            <table class="npbody" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="btnOrder" runat="server" ImageUrl="~/assets/common/buttons/placeyourorder.gif" ToolTip="Order" EnableViewState="false" />
                        <asp:ImageButton ID="btnPayPalOrder" runat="server" ImageUrl="https://www.paypal.com/en_us/i/btn/btn_xpresscheckout.gif" ToolTip="Go To PayPal" Visible="false" />
                    </td>
                </tr>
            </table>
            
            </asp:PlaceHolder>
        </Content>
        <LoadingPanelClientTemplate>
            <table width="200" height="86" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td>
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td style="font-size:10px;">Refreshing Rate Information...&nbsp;</td>
                  <td> <asp:Image runat="server" ID="Image1" ImageUrl="~/assets/common/icons/z_icon_anim.gif" width="16" height="16" border="0" Visible="true"/></td>
                </tr>
                </table>
                </td>
              </tr>
          </table>
        </LoadingPanelClientTemplate>
    </ComponentArt:CallBack>
    </asp:Panel>
    
</div>

</asp:Content>

<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
    <asp:Literal ID="errNoShipping" runat="server" Text="Freight rates are not available for this user." Visible="False"></asp:Literal>
    <asp:Literal ID="errConfigureShipping" runat="server" Text="Rates have not been defined for this order: the subtotal or weight of the order does not meet the cost structure of any defined rate." Visible="False"></asp:Literal>
    <asp:literal id="errNoOrder" runat="server" visible="False" text="Critical Error : No Order was found."></asp:literal>
    <asp:literal id="errTotalPayments" runat="server" visible="False" text="Total Payments must match the Total of the Order"></asp:literal>    
    <asp:literal id="errCardNotAuthorized" runat="server" visible="False" text="Credit Card Not Authorized"></asp:literal>
    <asp:literal id="errCardNotCharged" runat="server" visible="False" text="Credit Card Not Charged"></asp:literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
    <asp:Literal ID="hdnDayPhoneRequired" runat="server" Text="Day Phone can not be blank."></asp:Literal>
</asp:Content>
