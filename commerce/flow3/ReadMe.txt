﻿This checkout flow has a addresses, shipping and payment options on one page.

To enable this checkout flow, create at least there 3 webflow settings
Source Page: commerce/cart.aspx
Source Page: common/accounts/createaccount.aspx
Source Page: common/accounts/login.aspx

Next Page: commerce/flow3/onepagecheckout.aspx

If using PayPal, modify gateway settings:
Return Url : ~/commerce/flow3/providerreturn.aspx
Cancel URL: ~/commerce/flow3/providerreturn.aspx?cancel=1