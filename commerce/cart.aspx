<%@ Page Language="c#" MasterPageFile="~/masters/commercecart.master" Inherits="netpoint.catalog.cart"
    Codebehind="cart.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="cdlblock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="expense" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<asp:Content ContentPlaceHolderID="functionslot" runat="server" ID="function">
    <asp:hyperlink id="lnkViewListsImg" runat="server" navigateurl="~/common/accounts/savedlists.aspx" 
        ImageUrl="~/assets/common/icons/indicator.gif" ToolTip="View Lists"></asp:hyperlink>
    <asp:hyperlink id="lnkViewLists" runat="server" navigateurl="~/common/accounts/savedlists.aspx">View Saved Carts</asp:hyperlink>
    
    <asp:hyperlink id="lnkSaveCartImg" runat="server" navigateurl="~/common/accounts/savecart.aspx"
        ImageUrl="~/assets/common/icons/indicator.gif" ToolTip="Save Cart"></asp:hyperlink>
    <asp:hyperlink id="lnkCartSave" runat="server" navigateurl="~/common/accounts/savecart.aspx">Save Cart</asp:hyperlink>
    
    <asp:hyperlink id="lnkClearCartImg" runat="server" navigateurl="cart.aspx?ClearCart=true"
        ImageUrl="~/assets/common/icons/indicator.gif" ToolTip="Clear Cart"></asp:hyperlink>
    <asp:hyperlink id="lnkClearCart" runat="server" navigateurl="cart.aspx?ClearCart=true">Clear Cart</asp:hyperlink>
</asp:Content>

<asp:Content ContentPlaceHolderID="cartslot" runat="server" ID="cart">
    <np:cdlblock ID="npcdlblock" runat="server" ShowDiscount="True">
    </np:cdlblock>	
</asp:Content>

<asp:Content ContentPlaceHolderID="warningslot" runat="server" ID="warning">
    <np:MCWarning ID="mcWarning" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="checkoutslot" runat="server" ID="checkout">
     <asp:hyperlink id="lnkCheckOut" runat="server" navigateurl="shipping.aspx" 
        ImageUrl="~/assets/common/buttons/continuetocheckout.gif" ToolTip="Check Out"></asp:hyperlink>
</asp:Content>

<asp:Content ContentPlaceHolderID="unitsslot" runat="server" ID="units">
    <asp:literal id="sysShipUnits" runat="server"></asp:literal>
    
    <asp:literal id="sysShipUnitName" runat="server"></asp:literal>
    
    <asp:Image runat="server" id="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" />
    <asp:label id="lblOutOfStock" runat="server" text="Some items in your order are on back order"
        visible="False" cssclass="npwarning"></asp:label>
    
    <asp:Image runat="server" id="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False" />                
    <asp:label id="lblUnavailableOutOfStock" runat="server" text="This item is out of stock and cannot be backordered"
        visible="False" cssclass="npwarning"></asp:label>
    
    <asp:image runat="server" id="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
    
    <asp:label id="lblUnavailable" runat="server" text="This item is no longer available; remove it from the cart before you check out"
        visible="False" cssclass="npwarning"></asp:label>
    
    <asp:label id="lblIncorrectTax" runat="server" text="More than one tax applies to single or multiple items or shiping on this order and therefore cannot be processed."
        visible="False" cssclass="npwarning"></asp:label>
    
    <asp:label id="lblShippingRemoved" runat="server" text="The previously chosen ship method is not currently available."
        visible="False" cssclass="npwarning"></asp:label>
</asp:Content>

<asp:Content ContentPlaceHolderID="totalslot" runat="server" ID="total">
    <np:expense ID="expenses" runat="server" Visible="true" />           
</asp:Content>
