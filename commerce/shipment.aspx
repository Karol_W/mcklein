<%@ Page Language="c#" MasterPageFile="~/masters/default.master" Inherits="netpoint.commerce.shipment" Codebehind="shipment.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table id="Table1" cellspacing="0" cellpadding="3" width="800" border="0">
        <tr>
            <td></td>
            <td align="right"><strong>
                <asp:Literal ID="ltlShipmentNo" runat="server" Text="Shipment No"></asp:Literal></strong>
                <asp:Literal ID="sysShipmentID" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td></td>
            <td align="right"><strong>
                <asp:Literal ID="ltlStat" runat="server" Text="Status"></asp:Literal></strong>
                <asp:Literal ID="sysStatus" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong><asp:Literal ID="ltlShippingTo" runat="server" Text="Shipping To"></asp:Literal></strong></td>
            <td><strong><asp:Literal ID="ltlShippingVia" runat="server" Text="Shipping Via"></asp:Literal></strong></td>
        </tr>
        <tr>
            <td valign="top"><asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal></td>
            <td valign="top">
                <asp:Literal ID="sysShipMethod" runat="server"></asp:Literal>
                <br />
                <asp:Literal ID="sysTrackingNumber" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong><asp:Literal ID="ltlItemsShipped" runat="server" Text="Items Shipped"></asp:Literal></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvDetail" runat="server" CssClass="nptable" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="OrderID" HeaderText="colOrderNumber|Order No."></asp:BoundField>
                        <asp:BoundField DataField="PartNo" HeaderText="ItemNo|Item No."></asp:BoundField>
                        <asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name"></asp:BoundField>
                        <asp:BoundField DataField="Quantity" HeaderText="colQuantity|Quantity"></asp:BoundField>
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td align="right">
                <table cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td><b> <asp:Literal ID="ltlRequestShipDate" runat="server" Text="Request Ship Date"></asp:Literal></b></td>
                        <td align="right"><asp:Literal ID="sysRequestedShipDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><b><asp:Literal ID="ltlEstimatedShipDate" runat="server" Text="Estimated Ship Date"></asp:Literal></b></td>
                        <td align="right"><asp:Literal ID="sysEstShipDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><b><asp:Literal ID="ltlActShipDate" runat="server" Text="Actual Ship Date"></asp:Literal></b></td>
                        <td align="right"><asp:Literal ID="sysActualShipDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><b><asp:Literal ID="ltlEstimatedDeliveryDate" runat="server" Text="Estimated Delivery Date"></asp:Literal></b></td>
                        <td align="right"><asp:Literal ID="sysEstDelivDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><b><asp:Literal ID="ltlActualDeliveryDate" runat="server" Text="Actual Delivery Date"></asp:Literal></b></td>
                        <td align="right"><asp:Literal ID="sysActualDelivDate" runat="server"></asp:Literal></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errShipmentNotFound" runat="server" Text="Delivery not found." Visible="False"></asp:Literal>
    <asp:Literal ID="hdnReadyToShip" runat="server" Text="Ready to ship" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnShipped" runat="server" Text="Shipped" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDelivered" runat="server" Text="Delivered" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnNotSet" runat="server" Text="Not Set" Visible="False"></asp:Literal>
</asp:Content>
