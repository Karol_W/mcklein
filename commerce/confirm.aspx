<%@ Import Namespace="netpoint.api" %>

<%@ Page Language="c#" MasterPageFile="~/masters/commerce.master" Inherits="netpoint.catalog.confirm" Codebehind="confirm.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<script type="text/javascript"> 
        ga('create', 'UA-60694425-1', 'auto');
        ga('send', 'pageview')
        ga('require', 'ecommerce', 'ecommerce.js');   // Load the ecommerce plug-in.

        ga('ecommerce:addTransaction', {
          'id': '%%OrderID%%',                     
          'affiliation': '%%AccountName%%',   
          'revenue': '%%GrandTotal, {0:c}%%',              
          'shipping': '%%ShippingTotal, {0:c}%%',                 
          'tax': '%%TaxTotal, {0:c}%%'                 
        });

                        ga('ecommerce:addItem', {
                                  'id': '%%OrderID%%',                     
                                  'name': '%%PartName%% ',                
                                  'sku': '%%PartNo%%',                    
                                  'price': '%%PurchasePrice, {0:c}%%',                 
                                  'quantity': '%%Quantity%%'                   
                                });



            ga('ecommerce:send'); 
        </script>

<div class="col-md-9 pull-right white-bg">
    <table width="100%" cellpadding="3" cellspacing="0">
        <tbody>
            <tr>
                <td align="center" valign="middle" class="npheader">
                    <br />
                    <asp:literal id="ltlYourOrderHasBeenCompleted" runat="server" text="Your order has been completed."></asp:literal>
                    <br />
                    <asp:literal id="ltlYourOrderNumberIs" runat="server" text="Your Order Number is #"></asp:literal>
                    &nbsp;<asp:label id="OrderNoLabel" runat="server" />
                    <br />
                    <asp:hyperlink id="sysGo" runat="server" imageurl="~/assets/common/icons/indicator.gif"
                        target="_blank"></asp:hyperlink>
                    <asp:hyperlink id="lnkGoAgain" runat="server" text="View Printable Order" target="_blank"></asp:hyperlink>
                    <br />
                    <br />
                    </td>
            </tr>
        </tbody>
    </table>
</div>
</asp:Content>
