<%@ Page Language="c#" MasterPageFile="~/masters/commerce.master" Inherits="netpoint.commerce.manual" Codebehind="manual.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="qoblock" Src="controls/QuickOrderBlock.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right">
		<div class="cart-global-wrapper">
			<np:qoblock ID="npqoblock" runat="server" Columns="3" TotalLines="21" BorderWidth="0" />
		</div>
	</div>
</asp:Content>