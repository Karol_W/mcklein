<%@ Page Language="C#" MasterPageFile="~/masters/commerce.master" Inherits="netpoint.commerce.shipping" Codebehind="shipping.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<div class="col-md-9 pull-right white-bg">  
    <asp:Label ID="sysError" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
    <center>
        <asp:Image ID="topimage" runat="server" ImageUrl="~/assets/common/images/checkout1.gif"></asp:Image>
    </center>
         
    
    <table class="tblAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
        <tbody>
            <tr>
                <td colspan="2"><asp:Literal ID="ltlShippingAddressDropDown" runat="server" Text="Choose from your saved addresses" Visible="false"></asp:Literal></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlShippingAddress" Style="max-height: 400px; max-width: 400px"
                        runat="server" Visible="false" DataTextField="AddressDisplay" AutoPostBack="True"
                        Width="400" OnSelectedIndexChanged="ddlShippingAddress_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npsubheader"><asp:Literal ID="ltlAddShippingAddress" runat="server" Text="Delivery Address"></asp:Literal></td>
                <td class="npsubheader"><asp:Literal ID="ltlShippingAddressCheckBox" runat="server" Text="Choose from your saved addresses"></asp:Literal></td>
            </tr>
            <tr>
                <td valign="top"><np:AddressBlock ID="AddAddress" runat="server" ShipDestination="true" HideAddressType="true"></np:AddressBlock></td>
                <td valign="top">
                    <asp:RadioButtonList ID="rblShippingAddress" runat="server" DataTextField="AddressHTML"
                        AutoPostBack="True" Width="100%" CssClass="npbody" RepeatColumns="1" RepeatDirection="Horizontal"
                        CellPadding="2" CellSpacing="0" OnSelectedIndexChanged="rblShippingAddress_SelectedIndexChanged">
                    </asp:RadioButtonList></td>
            </tr>
        </tbody>
    </table>
   
   <div class="telephone-input-wrapper">
	   <h6><asp:Literal ID="ltlTelephoneNumbers" runat="server" Text="Telephone Numbers"></asp:Literal></h6>
	   
	   <p><asp:Literal ID="ltlDay" runat="server" Text="Day"></asp:Literal>
	   <asp:TextBox ID="txtDayPhone" runat="server" Width="175px"></asp:TextBox></p>
	   
	   <asp:Image ID="imgPhoneWarning" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
	   
	   <p><asp:Literal ID="ltlEvening" runat="server" Text="Evening"></asp:Literal>
	   <asp:TextBox ID="txtEveningPhone" runat="server" Width="175px"></asp:TextBox></p>
   </div>
   
   <asp:ImageButton class="continue-button" ID="lnkNext" runat="server" ImageUrl="~/assets/common/buttons/continue.gif"></asp:ImageButton>
                
   <%-- COMMENTED OUT BY CURTIS T AS THIS WAS MOVED TO THE VERIFY PAGE 
   <table id="tblShipInfo" cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npsubheader" style="width:50%;"><asp:Literal ID="ltlShippingProvider" runat="server" Text="Delivery Provider"></asp:Literal></td>
            <td class="npsubheader"><asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Shipping Date"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npbody">
                <asp:RadioButtonList ID="rblShipMethod" runat="server" AutoPostBack="True" CssClass="npbody"
                    CellPadding="2" CellSpacing="0" OnSelectedIndexChanged="rblShipMethod_SelectedIndexChanged">
                </asp:RadioButtonList>                
            </td>
            <td class="npbody" valign="top">
                <np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
                <asp:Image ID="imgRequestedShipDate" runat="server" ImageUrl="../assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
        </tr>
        <tr>
            <td class="npbody" align="right" colspan="2"></td>
        </tr>
        <tr>
            <td class="npbody" align="right" colspan="2">
                <asp:ImageButton ID="lnkNext" runat="server" ImageUrl="~/assets/common/buttons/continue.gif"></asp:ImageButton></td>
        </tr>
    </table>--%>
    
</div>   
    
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <%--asp:Literal ID="errNoShipping" runat="server" Text="Freight rates are not available for this user." Visible="False"></asp:Literal>
    <asp:Literal ID="errConfigureShipping" runat="server" Text="Rates have not been defined for this order: the subtotal or weight of the order does not meet the cost structure of any defined rate." Visible="False"></asp:Literal>--%>
    <asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
</asp:Content>
