<%@ Page Language="c#" MasterPageFile="~/masters/commerce.master" Inherits="netpoint.catalog.submit" Codebehind="submit.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <br />
    <table width="100%" cellpadding="3" cellspacing="0">
        <tbody>
            <tr>
                <td align="center" valign="middle" class="npheader">
                    <br />
                    <asp:Literal ID="ltlYourOrderHasBeenSubmitted" runat="server" Text="Your order has been submitted for approval."></asp:Literal>
                    <br />
                    <asp:Literal ID="ltlYourOrderNumberIs" runat="server" Text="Your Order Number is #"></asp:Literal>&nbsp;<asp:Label
                        ID="OrderNoLabel" runat="server" />
                    <br />
                    <asp:HyperLink ID="sysGo" runat="server" ImageUrl="~/assets/common/icons/indicator.gif"
                        Target="_blank"></asp:HyperLink>
                    <asp:HyperLink ID="lnkGoAgain" runat="server" Text="View Printable Order" Target="_blank"></asp:HyperLink>
                    <br />
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
