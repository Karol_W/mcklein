<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="paymentauthorize.aspx.cs" Inherits="netpoint.commerce.paymentauthorize" MasterPageFile="~/masters/commerce.master" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
  <table id="tblErrs" runat="server" width="100%" cellpadding="4" cellspacing="0" border="0">
    <tr>
        <td class="npsubheader">
            <asp:Literal ID="ltlHeader" runat="server" Text="We are unable to complete your order." />
        </td>
    </tr>
    <tr id="trNoOrder" runat="server" visible="false">
        <td class="npbody">
            <asp:Image ID="imgNoOrder" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
            <asp:Literal ID="ltlNoOrder" runat="server" Text="You haven't added any items to your cart." />&nbsp;
            <asp:HyperLink ID="lnkNoOrder" runat="server" NavigateUrl="~/default.aspx" Text="[Resolve]" />
        </td>
    </tr>
     <tr id="trPaymentAmount" runat="server" visible="false">
        <td class="npbody">
            <asp:Image ID="imgPaymentAmount" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
            <asp:Literal ID="ltlPaymentAmount" runat="server" Text="Your payment is incorrect or missing." />&nbsp;
            <asp:HyperLink ID="lnkPaymentAmount" runat="server" NavigateUrl="billing.aspx" Text="[Resolve]" />
        </td>
    </tr>
    <tr id="trShippingAddress" runat="server" visible="false">
        <td class="npbody">
            <asp:Image ID="imgShippingAddress" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
            <asp:Literal ID="ltlShippingAddress" runat="server" Text="Your shipping address is missing." />&nbsp;
            <asp:HyperLink ID="lnkShippingAddress" runat="server" NavigateUrl="shipping.aspx" Text="[Resolve]" />
        </td>
    </tr>
     <tr id="trBillingAddress" runat="server" visible="false">
        <td class="npbody">
            <asp:Image ID="imgBillingAddress" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
            <asp:Literal ID="ltlBillingAddress" runat="server" Text="Your billing address is missing." />&nbsp;
            <asp:HyperLink ID="lnkBillingAddress" runat="server" NavigateUrl="billing.aspx" Text="[Resolve]" />
        </td>
    </tr>
     <tr id="trAuthorize" runat="server" visible="false">
        <td class="npbody">
            <asp:Image ID="imgAuthorize" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
            <asp:Literal ID="sysAuthorize" runat="server" />&nbsp;
            <asp:HyperLink ID="lnkAuthorize" runat="server" NavigateUrl="billing.aspx" Text="[Resolve]" />
        </td>
    </tr>
  </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errCreditCardNotAuthorized" runat="server" Text="Credit Card Not Authorized" Visible="false"></asp:Literal>
    <asp:Literal ID="errNoQuoteItems" runat="server" Visible="False" Text="There are no items in the quote.  It can not be locked."></asp:Literal>    
    <asp:Literal ID="hdnOrderHasNoPayment" runat="server" Text="The order does not have a payment."></asp:Literal>
    <asp:Literal ID="hdnPaymentHasNoProvider" runat="server" Text="The payment selected on the order has no provider."></asp:Literal>
    <asp:Literal ID="hdnProviderDoesNotExist" runat="server" Text="The provider selected on the payment does not exist."></asp:Literal>
    <asp:Literal ID="hdnExpiredPaymentInformation" runat="server" Text="The payment information has expired and must be added again."></asp:Literal>
</asp:Content>

