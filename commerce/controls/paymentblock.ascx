<%@ Control Language="c#" Inherits="netpoint.commerce.controls.PaymentBlock" Codebehind="PaymentBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<asp:Panel ID="pnlNew" runat="server">
    <table id="tblNew" style="border-collapse: collapse" cellspacing="0" cellpadding="2" width="100%" class="npbody" border="0">
        <tr>
            <td class="npsubheader" colspan="3">
            <h6 class="cart-section-heading"><asp:Literal ID="ltlChoosePaymentMethod" runat="server" Text="Choose a Payment Method:"></asp:Literal></h6>
                <asp:DropDownList ID="ddlPaymentMethod" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="3"><asp:Literal ID="sysMessage" runat="server"></asp:Literal>
            <asp:Panel ID="pnlCharge" runat="server" Visible="false">
                <asp:Literal ID="hdnYouWillBeCharged" Text="Additional charge for this payment method:" runat="server"></asp:Literal>
                <np:PriceDisplay ID="prcCharge" runat="server" DisplayCurrency="Both" />
            </asp:Panel>
            </td>
        </tr>
        <tr id="rowHolder" runat="server">
            <td><asp:Literal ID="ltlHolder" runat="server" Text="Card Holder"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtCardHolder" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgCardHolderWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>
        <tr id="rowAcctNum" runat="server">
            <td><asp:Literal ID="ltlAcctNum" runat="server" Text="Account Number"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtAccountNumber" runat="server" Width="150px" AutoComplete="off"></asp:TextBox>
                <asp:ImageButton ID="imgAccountNumberWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
            <td valign="top" rowspan="4"><asp:Image ID="imgVerification" runat="server" ImageUrl="~/assets/common/images/verifynumber.gif" /></td>

        </tr>
        <tr id="rowVerificationNum" runat="server">
            <td><asp:Literal ID="ltlVerificationNumber" runat="server" Text="Verification Number"></asp:Literal></td>
            <td style="width:170;color:#cc0000;">
                <asp:TextBox ID="txtVerificationNumber" runat="server" Width="75px" MaxLength="6" AutoComplete="off"></asp:TextBox>&nbsp;
                <asp:Image ID="imgVerificationWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/images/verifypoint.gif" /></td>
        </tr>
        <tr id="rowCountry" runat="server">
            <td><asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
            <td colspan="2">
                <asp:dropdownlist id="ddlCountry" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCountry_SelectedIndexChanged"></asp:dropdownlist>
                <asp:ImageButton ID="imgCountryWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr id="rowStreet" runat="server">
            <td><asp:Literal ID="ltlStreet" runat="server" Text="Street"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtStreet" runat="server" Columns="45" MaxLength="150" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgStreetWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr id="rowCity" runat="server">
            <td><asp:Literal ID="ltlCity" runat="server" Text="City"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtCity" runat="server" Width="150px" Columns="25" MaxLength="150"></asp:TextBox>
                <asp:ImageButton ID="imgCityWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>      
        <tr id="rowState" runat="server">
            <td><asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
            <td colspan="2">
                <asp:dropdownlist id="ddlState" runat="server"></asp:dropdownlist>
                <asp:ImageButton ID="imgStateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr id="rowPostalCode" runat="server">
            <td><asp:Literal ID="ltlPostalCode" runat="server" Text="Postal Code"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtPostalCode" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgPostalCodeWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr id="rowEmail" runat="server">
            <td>
                <asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Visible="false"></asp:TextBox>
                <asp:ImageButton ID="imgEmailWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr id="rowPhone" runat="server">
            <td>
                <asp:Literal ID="ltlPhone" runat="server" Text="Phone"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Visible="false"></asp:TextBox>
                <asp:ImageButton ID="imgPhoneWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr id="rowIssueDate" runat="server"> 
            <td>
                <asp:Literal ID="ltlIssueDate" runat="server" Text="Issue Date"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlIssueMonth" runat="server">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>                
                <asp:DropDownList ID="ddlIssueYear" runat="server"></asp:DropDownList>
                <asp:Image ID="imgIssueDateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>
        <tr id="rowIssueNumber" runat="server">
            <td><asp:Literal ID="ltlIssueNumber" runat="server" Text="Issue Number"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtIssueNumber" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgIssueNumberWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />                
            </td>
        </tr>        
        <tr id="rowExpDate" runat="server">
            <td>
                <asp:Literal ID="ltlExpirationDate" runat="server" Text="Expiration Date"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlMonth" runat="server">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>                
                <asp:DropDownList ID="ddlYear" runat="server"></asp:DropDownList>
                <asp:Image ID="imgExpirationDateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>
        <tr id="rowAmount" runat="server" visible="false">
            <td><asp:Literal ID="ltlAmount" runat="server" Text="Amount" Visible="False" ></asp:Literal></td>
            <td><asp:TextBox ID="txtAmount2" runat="server" Width="100px" Visible="False" ></asp:TextBox>
                <asp:Image ID="imgAmount2Warning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>        
    </table>
</asp:Panel>
<br />
<table id="tblOrder" cellspacing="0" cellpadding="2" width="100%" border="0" class="npbody">    
    <tr>
        <td valign="top" align="right">
            <asp:GridView ID="summaryGrid" runat="server" CellSpacing="0" CellPadding="2" BorderWidth="0" CssClass="npbody" ShowHeader="false" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField ItemStyle-Font-Bold="true">
                        <ItemTemplate><%# Eval("First") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right">
                        <ItemTemplate><np:PriceDisplay ID="prcTotal" runat="Server" Price='<%# Eval("Second") %>' /></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>/</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate><np:PriceDisplay ID="prcBCTotal" runat="Server" Price='<%# Eval("Second") %>' DisplayCurrency="BaseCurrency" /></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr align="right">
        <td valign="top">
            <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="3" BorderStyle="None" ShowHeader="False" DataKeyNames="OrderPaymentID" OnRowDataBound="grid_RowDataBound">
                <RowStyle CssClass="npbody"></RowStyle>
                <HeaderStyle CssClass="npsubheader"></HeaderStyle>
                <Columns>
                    <asp:BoundField DataField="PaymentName" HeaderText="colCurrentPayments|Current Payments"></asp:BoundField>
                    <asp:BoundField DataField="MaskedNumber" HeaderText="Customer|Customer"></asp:BoundField>
                    <asp:TemplateField HeaderText="colAmount|Amount" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcAmount" runat="server" Price='<%# Eval("PaymentAmount") %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>/</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcBCAmount" runat="server" Price='<%# Eval("PaymentAmount") %>' DisplayCurrency="BaseCurrency" />
                        </ItemTemplate>
                    </asp:TemplateField>                    
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td  align="right">
            <asp:ImageButton ID="btnDelete" runat="server" OnClick="DeletePayment" ImageUrl="~/assets/common/buttons/remove.gif" />
        </td>
    </tr>
</table>

<asp:Literal ID="ltlOrderTotal" runat="server" Text="Order Total:" Visible="False" />
<asp:Literal ID="ltlPay" runat="server" Text="Payments:" Visible="false" />