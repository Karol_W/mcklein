<%@ Control Language="c#" Inherits="netpoint.commerce.controls.BrowseAsUser" Codebehind="BrowseAsUser.ascx.cs" %>
<table class="npbody" style="background-color:silver;" cellspacing="0" cellpadding="0" width="100%" border="1">
	<tr>
		<td class="npheader">
			<asp:Image id="dot" ImageUrl="~/assets/common/icons/smallblockevents.gif" Runat="server"></asp:Image>Browse 
			As
		</td>
	</tr>
	<tr>
		<td align="center">
            <asp:panel id="pnlSelect" HorizontalAlign="Center" CssClass="npbody" Width="100%" runat="server">
				<asp:HyperLink id="lnkSelect" runat="server" NavigateUrl="~/admin/common/accounts/BrowseAs.aspx">Select A User</asp:HyperLink>&nbsp;
				<asp:HyperLink id="lnkSelectImg" runat="server" ImageUrl="~/assets/common/icons/next.gif" NavigateUrl="~/admin/common/accounts/BrowseAs.aspx">HyperLink</asp:HyperLink>
            </asp:panel>
            <table class="npbody" border="1" style="background-color:silver;" cellpadding="0" cellspacing="0"
	            id="tblSelected" width="100%" runat="server">
	            <tr>
		            <td class="blockbodyleft">User:</td>
		            <td class="blockbodyleft"><asp:literal id="ltlUserID" runat="server"></asp:literal></td>
		            <td class="blockbodyleft"></td>
	            </tr>
	            <tr>
		            <td class="blockbodyleft">Account:</td>
		            <td class="blockbodyleft"><asp:literal id="ltlAccountID" runat="server"></asp:literal></td>
		            <td class="blockbodyleft"><asp:imagebutton id="btnRemove" runat="server" ImageUrl="~/assets/common/icons/delete.gif"></asp:imagebutton></td>
	            </tr>
            </table>
        </td>
    </tr>
</table>
