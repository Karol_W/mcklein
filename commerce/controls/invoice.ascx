<%@ Control Language="c#" Inherits="netpoint.commerce.controls.Invoice" Codebehind="Invoice.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="idlblock" Src="~/commerce/controls/InvoiceDetailListBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="itblock" Src="~/commerce/controls/InvoiceTotalBlock.ascx" %>
<center>
    <table class="npbody" cellspacing="0" cellpadding="3" width="600">
        <tr>
            <td align="left">
                <asp:Label ID="OrderDate" runat="server"></asp:Label></td>
            <td align="right">
                <asp:Label ID="lblOrderNo" runat="server"></asp:Label>
                <asp:Literal ID="ltlPONumber" runat="server" Text="<br>" Visible="False"></asp:Literal>
                <asp:Literal ID="ltlConfirmationNumber" runat="server" Visible="False" Text="<br>"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <b>
                    <asp:Literal ID="ltlBillToo" runat="server" Text="Bill To"></asp:Literal></b></td>
            <td>
                <b>
                    <asp:Literal ID="ltlShipToo" runat="server" Text="Ship To"></asp:Literal></b></td>
        </tr>
        <tr>
            <td valign="top">
                <np:AddressBlock ID="npbilltoaddressblock" runat="server" EnableViewState="False"></np:AddressBlock>
                <asp:Literal ID="ltlBillTo" runat="server"></asp:Literal></td>
            <td valign="top">
                <np:AddressBlock ID="npshiptoaddressblock" runat="server" EnableViewState="False"></np:AddressBlock>
                <asp:Literal ID="ltlShipTo" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td colspan="2">
                <np:idlblock ID="npidlblock" runat="server" BorderWidth="1" EnableViewState="False">
                </np:idlblock>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <np:itblock ID="npitblock" runat="server" BorderWidth="0" Width="100%" EnableViewState="False">
                </np:itblock>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>
                    <asp:Literal ID="ltlPaymentInformation" runat="server" Text="Payment Information:"></asp:Literal></b></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvInvoice" runat="server" CellPadding="1"
                     Width="100%" AutoGenerateColumns="False" EmptyDataText="No Records Found">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />  
                    <Columns>
                        <asp:BoundField DataField="BillingName" HeaderText="Customer Name"></asp:BoundField>
                        <asp:BoundField DataField="PaymentName" HeaderText="colPaymentMethod"></asp:BoundField>
                        <asp:BoundField DataField="MaskedNumber" HeaderText="Customer">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="PaymentAmount" HeaderText="colAmount">
                            <HeaderStyle HorizontalAlign="right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ExpirationDate" HeaderText="colExpiration" DataFormatString="{0:d}">
                            <HeaderStyle HorizontalAlign="right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</center>
