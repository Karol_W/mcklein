<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="multicurrencywarning.ascx.cs" Inherits="netpoint.commerce.controls.multicurrencywarning" EnableViewState="false" %>
<asp:Label ID="lblWarning" runat="server" Font-Bold="true" Font-Italic="true">
Note: amounts are also displayed in U.S. Dollars because orders placed with creditcards will be charged in U.S. Dollars.
</asp:Label>