<%@ Control Language="c#" Inherits="netpoint.catalog.controls.CartDetailListBlock" Codebehind="CartDetailListBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="Discount" Src="~/commerce/controls/Discount.ascx" %>
<%@ Register TagPrefix="np" TagName="PartDescription" src="~/commerce/controls/partdescription.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPrice" Src="~/commerce/controls/partprice.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="mck" TagName="Monogram" Src="~/mckleinControls/Monogram/monogram.ascx" %>

</script>
<asp:gridview id="gridOrderDetail" runat="server" cssclass="nptable" AutoGenerateColumns="False" Width="100%" GridLines="None"
     EmptyDataText="No Items in Your Shopping Cart" OnRowCommand="gridOrderDetail_RowCommand" OnRowDataBound="gridOrderDetail_RowDataBound" OnRowDeleting="gridOrderDetail_RowDeleting">
	<RowStyle CssClass="npbody" />
	<HeaderStyle CssClass="npsubheader"></HeaderStyle>
	<AlternatingRowStyle CssClass="npbodyalt" />
	<EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
	<Columns>
		<asp:templatefield HeaderText="colImage|Image">
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink id="lnkThumb" runat="server"></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield HeaderText="colDescription|Description">
			<ItemTemplate>
                <np:PartDescription ID="partDesc" runat="server" ShowPartDescription="false" ShowPartName="true" ShowPartNo="true" />
				<asp:Literal id="ltlPartType" runat="server"></asp:Literal>
				<asp:Literal id="ltlOrderNotes" runat="server"></asp:Literal>
				<mck:Monogram id="partMonogram" OrderDetailID='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>' runat="server"></mck:Monogram>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield>
			<ItemTemplate>
			    <asp:LinkButton ID="btnDelete" runat="server" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>'> 
			        <asp:Image ID="imgDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif" />
			    </asp:LinkButton>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield  HeaderText="colPrice|Price">
			<ItemTemplate>
				<asp:TextBox id="nbQuantity" runat="server" maxlength="9" columns="4" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>' />
				<asp:Label id="lblQuantity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>' Visible="false" />
				<asp:ImageButton id="btnUpdateQty" runat="server" ImageUrl="~/assets/common/icons/calculate.gif" CommandName="updateqty" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>' ToolTip="Update the quantity.">
				</asp:ImageButton>

				<np:PriceDisplay ID="prcPrice" Runat="server" />

				<asp:Literal ID="sysPackagingUnit" Runat="server" Visible="false" />
				<asp:Literal ID="sysPackagingQty" Runat="server" Visible="false" />
				<asp:Literal ID="sysSalesUnit" Runat="server" />
				<asp:Literal ID="sysSalesQty" Runat="server" />
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield HeaderText="colExtended|Extended">
			<ItemStyle HorizontalAlign="Right"></ItemStyle>
			<ItemTemplate>
				<np:PartPrice ID="partPrice" runat="server" ShowOriginalPrice="true" ShowOriginalPriceLabel="true" ShowDiscount="true" ShowDiscountLabel="true" ShowFinalPriceLabel="false"/>
			</ItemTemplate>
		</asp:templatefield>
		<asp:TemplateField Visible="false">
			<ItemStyle HorizontalAlign="Right" />
			<ItemTemplate>
				<np:PartPrice ID="partPriceBC" runat="server" ShowOriginalPrice="true" ShowOriginalPriceLabel="true" ShowDiscount="true" ShowDiscountLabel="true" ShowFinalPriceLabel="false" DisplayCurrency="BaseCurrency" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
			<ItemTemplate>
				<asp:Image Runat="server" ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" />
				<asp:Image Runat="server" ID="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False"></asp:Image>				
				<asp:Image Runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif"
					Visible="False"></asp:Image>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:gridview>
<asp:Image Runat="server" ID="imgQuantity" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
<asp:label id="sysErr" runat="server" cssclass="npwarning"></asp:label>
<asp:Panel ID="pnl" Runat="server">
	<np:Discount id="discount" runat="server"></np:Discount>
</asp:Panel>
<asp:literal id="hdnUpdateQuantity" Runat="server" Text="Update The Quantity" Visible="False"></asp:literal>
<asp:literal id="errQuantity" Runat="server" Text="Quantity must be a positive integer" Visible="False"></asp:literal>

