<%@ Control Language="c#" Inherits="netpoint.commerce.controls.QuickOrderBlock" Codebehind="QuickOrderBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<script language="c#" runat="server"></script>
<asp:Label id="sysError" CssClass="npwarning" runat="server"></asp:Label>
<h1>Enter the item code and quantity for each item, then click buy</h1>
<div id="quick-order-block">
    <asp:DataList id="orderrepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
	    RepeatDirection="Vertical" BorderWidth="0" BorderColor="black" RepeatColumns="0" RepeatLayout="Table"
	    Width="100%" CssClass="QOTable">
	    <ItemTemplate>
		    <table width="100%" cellspacing="0" cellpadding="0" border="0" id="tblPart" runat="server">
			    <tr>
				    <td><asp:Image ID="imgWarning" Runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />&nbsp;
					    <NP:PromptTextBox ID="PartNo" Prompt="Item Code" Runat="server" Columns="10" /></td>
				    <td><NP:PromptTextBox ID="Quantity" Prompt="Qty" Runat="server" Columns="3" /></td>
			    </tr>
		    </table>
	    </ItemTemplate>
    </asp:DataList>
    <asp:ImageButton ID="SubmitButton" Runat="server" />
</div>

<asp:Literal id="errBadPart" runat="server" Visible="False" Text="The item does not exist or is unavailable."></asp:Literal>
<asp:Literal id="errNegativeQuantity" runat="server" Visible="False" Text="Please enter a quantity greater than zero."></asp:Literal>
