<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.catalog.controls.InvoiceDetailListBlock" Codebehind="InvoiceDetailListBlock.ascx.cs" %>
<%@ Import Namespace="netpoint.api" %>
<asp:GridView ID="gvInvoiceDetail" runat="server" CellPadding="1" 
     Width="100%" AutoGenerateColumns="False" EmptyDataText="No Records Found" OnRowDataBound="gvInvoiceDetail_RowDataBound">
    <RowStyle CssClass="npbody" />
    <HeaderStyle CssClass="npheader"></HeaderStyle>
	<FooterStyle CssClass="CatalogFooter"></FooterStyle>
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />  
    <Columns>
        <asp:BoundField ItemStyle-Width="5%" DataField="Quantity" HeaderText="colQty"></asp:BoundField>
		<asp:BoundField ItemStyle-Width="10%" HeaderText="Item" DataField="ItemNo"></asp:BoundField>
		<asp:BoundField DataField="PartName" DataFormatString="<b>{0}</b>" HeaderText="ItemDescription"></asp:BoundField>
		<asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="right" HeaderText="colPrice">
		    <ItemTemplate>
		        <np:PriceDisplay ID="prcPrice" runat="server" />
		    </ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>
