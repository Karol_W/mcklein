<%@ Control Language="C#" AutoEventWireup="true" Inherits="netpoint.commerce.controls.PartPrice" Codebehind="partprice.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>


    <asp:Literal ID="hdnOrigianlly" runat="server" Text="Originally" Visible="false"></asp:Literal>
    <np:PriceDisplay ID="sysPrice" runat="server" />

    <asp:Literal ID="hdnDiscount" runat="server" Text="Discount" Visible="false"></asp:Literal>
    <np:PriceDisplay ID="sysDiscount" runat="server" />
    
    <asp:literal ID="hdnYourPrice" runat="server" Text="Your Price" Visible="false"></asp:literal>
    <np:PriceDisplay id="sysExtendedPrice" runat="server" />