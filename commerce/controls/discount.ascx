<%@ Control Language="c#" Inherits="mcklein.commerce.controls.Discount" Codebehind="Discount.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <!-- ID of <td> below was "ltlMessage" before -->
        <td class="npadminheader"><asp:Literal ID="ltlMessageAndPromo" Text="Enter any promotions or coupons:" runat="server"></asp:Literal></td>       
    </tr>
    <tr>
        <td valign="top" class="input-group_2">
            <asp:TextBox ID="txtDiscountCode" runat="server" CssClass="form-control"></asp:TextBox>
            <span class="input-group-btn">
                <asp:Button ID="btnApplyDiscount" runat="server" Text="Add"
                    OnClick="btnApplyDiscount_Click" ToolTip="Apply" CssClass="btn btn-default" /></td>
            </span>
    </tr>
    <tr>
        <td class="npadminheader"><asp:Literal ID="ltlAppliedCoupons" Text="Applied coupons:" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td valign="top">
            <asp:GridView ID="gvAppliedCoupons" runat="server" CssClass="nptable" AutoGenerateColumns="false"
                OnRowCommand="gvAppliedCoupons_RowCommand" ShowHeader="false" >
                <AlternatingRowStyle CssClass="npbodyalt" />
                <RowStyle CssClass="npbody" />
                <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                <FooterStyle CssClass="npbody" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Literal ID="ltlDiscountCode" runat="server" Text='<%# Eval("Discount.DiscountCode") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Right" />
                        <ItemTemplate>
                            <b><asp:Literal ID="ltlDiscountDescription" runat="server" Text='<%# Eval("Discount.Description") %>'></asp:Literal></b>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                                CommandName="remove" CommandArgument='<%# Eval("DiscountID") %>'>
                            </asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Literal ID="sysDescription" runat="server"></asp:Literal><br />
            <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label></td>
    </tr>
</table>
<asp:Literal ID="hdnBadCode" Text="The discount code entered is not valid" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="hdnNoDiscountText" runat="server" Text="No text was defined for this condition." Visible="False"></asp:Literal>
<asp:Literal ID="hdnExpired" runat="server" Text="This discount has expired." Visible="False"></asp:Literal>
<asp:Literal ID="hdnNotStarted" runat="server" Text="This discount has not currently available." Visible="False"></asp:Literal>
<asp:Literal ID="hdnUserLimit" runat="server" Text="The redemption limit has been met for this offer." Visible="False"></asp:Literal>
<asp:Literal ID="hdnAccountLimit" runat="server" Text="The redemption limit has been met for this offer on this account." Visible="False"></asp:Literal>
<asp:Literal ID="hdnUnauthorized" runat="server" Text="You are not authorized to use this discount." Visible="False"></asp:Literal>
