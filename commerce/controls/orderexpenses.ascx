<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="orderexpenses.ascx.cs" Inherits="netpoint.commerce.controls.orderexpenses" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>


<asp:GridView ID="sysgrid" runat="server" AutoGenerateColumns="False" CssClass="cart-global-table" 
    OnRowDataBound="grid_RowDataBound" ShowHeader="False" >
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Literal ID="ltlName" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Literal>
            </ItemTemplate>            
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle />
            <ItemTemplate>
                <np:PriceDisplay ID="sysAmount" Price='<%# Eval("Total") %>' ShowCurrencySymbol="true" DisplayCurrency="SessionCurrency" runat="server" />
            </ItemTemplate>            
             <FooterStyle HorizontalAlign="right" Font-Bold="true" />
        </asp:TemplateField>
        <asp:TemplateField Visible="false">
            <ItemStyle />
            <ItemTemplate>
                <np:PriceDisplay ID="sysBCAmount" Price='<%# Eval("Total") %>' ShowCurrencySymbol="true" DisplayCurrency="BaseCurrency" runat="server" />
            </ItemTemplate>            
             <FooterStyle />
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:Literal ID="hdnEstOrderTotal" runat="server" Text="Est. Order Total" Visible="False"></asp:Literal>
