<%@ Control Language="c#" Inherits="netpoint.catalog.controls.InvoiceTotalBlock" Codebehind="InvoiceTotalBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Import Namespace="netpoint.api" %>
<asp:Panel ID="TotalPanel" Runat="server" BorderColor="#EEEEEE">
	<table width="100%">
		<tr>
			<td valign="top" align="right">
				<table class="npbody">
					<tr>
						<td><asp:Label id="sysVolumeTotal" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td><asp:Label id="sysWeightTotal" Runat="server"></asp:Label></td>
					</tr>
				</table>
			</td>
			<td valign="top" align="right">
				<np:OrderExpenses ID="expenses" runat="server" />
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Literal id="hdnTotalVolume" runat="server" Text="Total Volume" Visible="False"></asp:Literal>
<asp:Literal id="hdnTotalWeight" runat="server" Text="Total Weight" Visible="False"></asp:Literal>
