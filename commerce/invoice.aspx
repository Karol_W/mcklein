<%@ Register TagPrefix="np" TagName="Invoice" Src="~/commerce/controls/invoice.ascx" %>
<%@ Page language="c#" Inherits="netpoint.catalog.invoice" CodeBehind="invoice.aspx.cs" %>
<%@ OutputCache Location="None" %>

<asp:literal id="hdnRestricted" runat="server" text="This data is restricted to the owner of the order." visible="false"></asp:literal>