﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/commerce.master" AutoEventWireup="true" CodeBehind="expressinvoice.aspx.cs" Inherits="netpoint.commerce.expressinvoice" %>
<%@ Register TagPrefix="np" TagName="Invoice" Src="~/commerce/controls/invoice.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" class="npbody">
        <tr>
            <td align="left" valign="top"><asp:Literal ID="sysPageText" runat="Server" /><br /></td>
        </tr>
        <tr>
            <td align="center" valign="top" class="nplabel"><asp:Literal ID="EmailConfirmationText" runat="Server" /><br /><br /><br /><br /></td>
        </tr>
        <tr>
        <td align="center" valign="top" class="npsubheader">
            <asp:hyperlink id="lnkHome" runat="server"></asp:hyperlink>
        </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="menu" ContentPlaceHolderID="menuslot" runat="server">
</asp:Content>
<asp:Content ID="hidden" ContentPlaceHolderID="hiddenslot" runat="server">
    <asp:literal id="hdnRestricted" runat="server" text="This data is restricted to the owner of the order." visible="false"></asp:literal>
    <asp:literal id="hdnEmailConfirmation" runat="server" text="An order confirmation has been sent to " visible="false"></asp:literal>
    <asp:literal id="hdnHomePage" runat="server" text="Return to HomePage " visible="false"></asp:literal>
</asp:Content>
