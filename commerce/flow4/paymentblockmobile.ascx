<%@ Control Language="c#" Inherits="netpoint.commerce.controls.PaymentBlockMobile" CodeBehind="PaymentBlockMobile.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>

<asp:Panel ID="pnlNew" runat="server" CssClass="form-horizontal">
    <asp:Panel ID="Panel1" runat="server" CssClass="form-group">
        <asp:Label ID="ltlChoosePaymentMethod" AssociatedControlID="ddlPaymentMethod" runat="server" Text="Choose a Payment Method:" CssClass="control-label col-sm-6"></asp:Label>
        <div class="col-sm-6">
            <asp:DropDownList ID="ddlPaymentMethod" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged" CssClass="form-control">
            </asp:DropDownList>
        </div>
    </asp:Panel>


    <asp:Literal ID="sysMessage" runat="server" Visible="false"></asp:Literal>
    <asp:Panel ID="pnlCharge" runat="server" Visible="false">

        <asp:Literal ID="hdnYouWillBeCharged" Text="Additional charge for this payment method:" runat="server"></asp:Literal>
        <np:PriceDisplay ID="prcCharge" runat="server" DisplayCurrency="Both" />
    </asp:Panel>


    <asp:Panel ID="rowHolder" runat="server" CssClass="form-group">
        <asp:Label ID="ltlHolder" AssociatedControlID="txtCardHolder" runat="server" Text="Card Holder" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtCardHolder" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgCardHolderWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowAcctNum" runat="server" CssClass="form-group">
        <asp:Label ID="ltlAcctNum" AssociatedControlID="txtAccountNumber" runat="server" Text="Account Number" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtAccountNumber" runat="server" AutoComplete="off" CssClass="form-control"></asp:TextBox>
            <span class="help-block">Leave out all spaces and dashes</span>
            <asp:Image ID="imgVerification" runat="server" Visible="False" />
            <asp:PlaceHolder ID="imgAccountNumberWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowVerificationNum" runat="server" CssClass="form-group">
        <asp:Label ID="ltlVerificationNumber" AssociatedControlID="txtVerificationNumber" runat="server" Text="Verification Number" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtVerificationNumber" runat="server" MaxLength="6" AutoComplete="off" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgVerificationWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowCountry" runat="server" CssClass="form-group">
        <asp:Label ID="ltlCountry" AssociatedControlID="ddlCountry" runat="server" Text="Country" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
            <asp:PlaceHolder ID="imgCountryWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowStreet" runat="server" CssClass="form-group">
        <asp:Label ID="ltlStreet" AssociatedControlID="txtStreet" runat="server" Text="Street" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtStreet" runat="server" Columns="45" MaxLength="150" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgStreetWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowCity" runat="server" CssClass="form-group">
        <asp:Label ID="ltlCity" AssociatedControlID="txtCity" runat="server" Text="City" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtCity" runat="server" Columns="25" MaxLength="150" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgCityWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowState" runat="server" CssClass="form-group">
        <asp:Label ID="ltlState" runat="server" AssociatedControlID="ddlState" Text="State" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control"></asp:DropDownList>
            <asp:PlaceHolder ID="imgStateWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowPostalCode" runat="server" CssClass="form-group">
        <asp:Label ID="ltlPostalCode" AssociatedControlID="txtPostalCode" runat="server" Text="Postal Code" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtPostalCode" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgPostalCodeWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowEmail" runat="server" CssClass="form-group">
        <asp:Label ID="ltlEmail" AssociatedControlID="txtEmail" runat="server" Text="Email" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtEmail" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgPhoneWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowPhone" runat="server" CssClass="form-group">
        <asp:Label ID="ltlPhone" AssociatedControlID="txtPhone" runat="server" Text="Phone" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtPhone" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgEmailWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowIssueDate" runat="server" CssClass="form-group form-inline">
        <asp:Label ID="ltlIssueDate" AssociatedControlID="ddlIssueMonth" runat="server" Text="Issue Date" CssClass="control-label col-sm-5"></asp:Label>

        <div class="col-sm-7">
            <asp:DropDownList ID="ddlIssueMonth" runat="server" CssClass="form-control">
                <asp:ListItem Value="01">01</asp:ListItem>
                <asp:ListItem Value="02">02</asp:ListItem>
                <asp:ListItem Value="03">03</asp:ListItem>
                <asp:ListItem Value="04">04</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="06">06</asp:ListItem>
                <asp:ListItem Value="07">07</asp:ListItem>
                <asp:ListItem Value="08">08</asp:ListItem>
                <asp:ListItem Value="09">09</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="11">11</asp:ListItem>
                <asp:ListItem Value="12">12</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlIssueYear" runat="server"></asp:DropDownList>

            <asp:PlaceHolder ID="imgIssueDateWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowIssueNumber" runat="server" CssClass="form-group">
        <asp:Label ID="ltlIssueNumber" AssociatedControlID="txtIssueNumber" runat="server" Text="Issue Number" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtIssueNumber" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgIssueNumberWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowExpDate" runat="server" CssClass="form-group form-inline">
        <asp:Label ID="ltlExpirationDate" AssociatedControlID="ddlMonth" runat="server" Text="Expiration Date" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control">
                <asp:ListItem Value="01">01</asp:ListItem>
                <asp:ListItem Value="02">02</asp:ListItem>
                <asp:ListItem Value="03">03</asp:ListItem>
                <asp:ListItem Value="04">04</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="06">06</asp:ListItem>
                <asp:ListItem Value="07">07</asp:ListItem>
                <asp:ListItem Value="08">08</asp:ListItem>
                <asp:ListItem Value="09">09</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="11">11</asp:ListItem>
                <asp:ListItem Value="12">12</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control"></asp:DropDownList>
            <asp:PlaceHolder ID="imgExpirationDateWarning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

    <asp:Panel ID="rowAmount" runat="server" Visible="false" CssClass="form-group">
        <asp:Label ID="ltlAmount" AssociatedControlID="txtAmount2" runat="server" Text="Amount" Visible="False" CssClass="control-label col-sm-5"></asp:Label>
        <div class="col-sm-7">
            <asp:TextBox ID="txtAmount2" runat="server" Width="100px" Visible="False" CssClass="form-control"></asp:TextBox>
            <asp:PlaceHolder ID="imgAmount2Warning" runat="server" Visible="False">
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>

</asp:Panel>


<asp:GridView ID="summaryGrid" runat="server" CellSpacing="0" CellPadding="2" BorderWidth="0" CssClass="npbody" ShowHeader="false" AutoGenerateColumns="false">
    <Columns>
        <asp:TemplateField ItemStyle-Font-Bold="true">
            <ItemTemplate><%# Eval("First") %></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcTotal" runat="Server" Price='<%# Eval("Second") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="center">
            <ItemTemplate>/</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <np:PriceDisplay ID="prcBCTotal" runat="Server" Price='<%# Eval("Second") %>' DisplayCurrency="BaseCurrency" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>


<asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="3" BorderStyle="None" ShowHeader="False" DataKeyNames="OrderPaymentID" OnRowDataBound="grid_RowDataBound">
    <RowStyle CssClass="npbody"></RowStyle>
    <HeaderStyle CssClass="npsubheader"></HeaderStyle>
    <Columns>
        <asp:BoundField DataField="PaymentName" HeaderText="colCurrentPayments|Current Payments"></asp:BoundField>
        <asp:BoundField DataField="MaskedNumber" HeaderText="Customer|Customer"></asp:BoundField>
        <asp:TemplateField HeaderText="colAmount|Amount" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcAmount" runat="server" Price='<%# Eval("PaymentAmount") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="center">
            <ItemTemplate>/</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="False" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <np:PriceDisplay ID="prcBCAmount" runat="server" Price='<%# Eval("PaymentAmount") %>' DisplayCurrency="BaseCurrency" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>


<asp:Button ID="btnDelete" runat="server" OnClick="DeletePayment" CssClass="btn btn-default" Text="Remove"/>



<asp:Literal ID="ltlOrderTotal" runat="server" Text="Order Total:" Visible="False" />
<asp:Literal ID="ltlPay" runat="server" Text="Payments:" Visible="false" />