<%@ Control Language="c#" Inherits="netpoint.common.controls.AddressBlockMobile" CodeBehind="addressblockmobile.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>

<asp:Panel ID="EditPanel" runat="server">
    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>

    <asp:Panel ID="rowAddressType" runat="server">
        <asp:Literal ID="ltlAddressType" Text="Address Type " runat="server"></asp:Literal>
        <asp:Label ID="lblAddressType" runat="server" Text="" />
    </asp:Panel>

    <ul class="form-horizontal">
        <li class="form-group">
            <asp:Label ID="ltlAttention" AssociatedControlID="txtAttention" runat="server" Text="Name " CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtAttention" runat="server" Columns="25" MaxLength="150" placeholder="Name" CssClass="form-control"></asp:TextBox>
            </div>
            <!-- <asp:Literal id="ltlAttentionExample" Runat="server" Text="i.e. John Doe"></asp:Literal> -->
        </li>

        <li class="form-group <%= imgStreet1Warning.Visible ? "has-error has-feedback" : "" %>">
            <asp:Label ID="ltlStreet1" AssociatedControlID="txtStreet1" Text="Street 1 " runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtStreet1" runat="server" Columns="45" MaxLength="150" placeholder="Street Name" CssClass="form-control"></asp:TextBox>
                <asp:PlaceHolder ID="imgStreet1Warning" runat="server" Visible="False">
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block">Street 1 cannot be blank</span>
                </asp:PlaceHolder>
            </div>
        </li>

        <li class="form-group">
            <asp:Label ID="ltlStreet2" AssociatedControlID="txtStreet2" Text="Street 2" runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtStreet2" runat="server" Columns="45" MaxLength="150" placeholder="Street Name" CssClass="form-control"></asp:TextBox>
            </div>
        </li>

        <asp:Panel ID="rowBlock" runat="server">
            <!-- <asp:literal id="ltlBlock" Text="Block " runat="server"></asp:literal> -->
            <asp:TextBox ID="txtBlock" runat="server" Columns="45" MaxLength="150" CssClass="form-control"></asp:TextBox>
        </asp:Panel>

        <li class="form-group <%= imgCityWarning.Visible ? "has-error has-feedback" : "" %>">
            <asp:Label ID="ltlCity" AssociatedControlID="txtCity" Text="City " runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtCity" runat="server" Columns="25" MaxLength="150" placeholder="City" CssClass="form-control"></asp:TextBox>
                <asp:PlaceHolder ID="imgCityWarning" runat="server" Visible="False">
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>                    
                    <span class="help-block">City cannot be blank</span>
                </asp:PlaceHolder>
            </div>
        </li>

        <li class="form-group">
            <asp:Label ID="ltlCountry" AssociatedControlID="ddlCountry" Text="Country " runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
            </div>
        </li>

        <asp:Panel ID="rowState" runat="server">
            <li class="form-group">
                <asp:Label ID="ltlState" AssociatedControlID="ddlState" Text="State " runat="server" CssClass="control-label col-sm-3"></asp:Label>
                <div class="col-sm-9">
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </li>
        </asp:Panel>

        <li class="form-group <%= imgZipWarning.Visible ? "has-error has-feedback" : "" %>">
            <asp:Label ID="ltlZIP" AssociatedControlID="txtPostalCode" Text="Zip " runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtPostalCode" runat="server" Columns="12" MaxLength="50" placeholder="ZIP" CssClass="form-control"></asp:TextBox>
                <asp:PlaceHolder ID="imgZipWarning" runat="server" Visible="False">
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block">Postal code is required</span>
                </asp:PlaceHolder>
            </div>
        </li>

        <asp:Panel ID="rowCounty" runat="server">
            <li class="form-group">
                <asp:Label ID="ltlCounty" AssociatedControlID="txtCounty" Text="County " runat="server" CssClass="control-label col-sm-3"></asp:Label>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtCounty" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                </div>
            </li>
        </asp:Panel>

        <li class="form-group">
            <asp:Label ID="ltlInCareOf" AssociatedControlID="txtInCareOf" runat="server" Text="In Care Of " CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtInCareOf" runat="server" Columns="25" MaxLength="150" placeholder="In Care Of" CssClass="form-control"></asp:TextBox>
            </div>
            <!-- <asp:Literal id="Literal1" Runat="server" Text="i.e. Company Name"></asp:Literal> -->
        </li>

        <li class="form-group <%= imgAddressNameWarning.Visible ? "has-error has-feedback" : "" %>">
            <asp:Label ID="ltlAddressName" AssociatedControlID="txtAddressName" Text="Address Label " runat="server" CssClass="control-label col-sm-3"></asp:Label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtAddressName" runat="server" Columns="25" MaxLength="50" placeholder="Address Label" CssClass="form-control"></asp:TextBox>
                <asp:PlaceHolder ID="imgAddressNameWarning" runat="server" Visible="False">
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span> 
                    <asp:Label id="addressNameEmptyMessage" CssClass="help-block" runat="server" Text="Address name cannot be blank"></asp:Label>
                    <asp:Label id="addressNameDuplicateMessage" CssClass="help-block" runat="server" Text="An address with this name already exists" Visible="false"></asp:Label>
                </asp:PlaceHolder>
            </div>
            <!-- <asp:Literal id="ltlAddressNameExample" Runat="server" Text="i.e. Work, Home"></asp:Literal> -->
        </li>

        <li class="form-group">
            <asp:CheckBox ID="sysShipDestination" runat="server" Checked="True" Visible="false"></asp:CheckBox>
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/buttons/saveinfo.gif" ToolTip="Save" />
        </li>

    </ul>

</asp:Panel>
<asp:Panel ID="ViewPanel" runat="server" Visible="False" CssClass="npbody">
    <asp:Literal ID="sysAddressHTML" runat="server"></asp:Literal>
</asp:Panel>
<asp:Literal ID="errVerifyAddress" runat="server" Text="Please verify that the address entered is correct." Visible="False"></asp:Literal>
<asp:Literal ID="hdnShipping" runat="server" Text="Delivery" Visible="False"></asp:Literal>
<asp:Literal ID="hdnBilling" runat="server" Text="Billing" Visible="False"></asp:Literal>
<asp:Literal ID="hdnMyAddress" runat="server" Text="My Address" Visible="False"></asp:Literal>
<asp:Literal ID="sysAddressID" runat="server" Text="0" Visible="False"></asp:Literal>
<asp:Literal ID="errShip" runat="server" Text="No countries have been made valid for shipping." Visible="False"></asp:Literal>
<asp:Literal ID="errBill" runat="server" Text="No countries have been made valid for billing." Visible="False"></asp:Literal>
