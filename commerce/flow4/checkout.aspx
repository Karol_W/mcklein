﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/commerce.master" CodeBehind="checkout.aspx.cs" Inherits="netpoint.commerce.flow4.checkout" %>

<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="CartListBlock" Src="CartDetailListBlockMobile.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<%@ Register TagPrefix="np" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PaymentBlock" Src="PaymentBlockMobile.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="AddressBlockMobile.ascx" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <script type="text/javascript">
        function selectProduct(param) {
            clbackTotals.callback(param);
        }

        function clbackTotals_onload(sender, e) {
            var oldPrefix = sender.CallbackPrefix;
            var newPrefix = document.location.href;
            //alert('old: ' + oldPrefix + '\nnew: ' + newPrefix);
            sender.CallbackPrefix = newPrefix
        }
    </script>

    <div>
        <div class="single-page-checkout">
             <h1 class="white-gloss-bg">Order Details</h1>
            <asp:Label ID="sysErrorMessage" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
            <div class="my-checkout-col-left">
            <asp:Panel ID="tblEditAddress" runat="server">
                <div id="cart-billing-address">
                    <h6 class="cart-section-headings">
                        <asp:Literal ID="ltlEditBillingTo" runat="server" Text="Billing To"></asp:Literal></h6>
                    <asp:DropDownList ID="ddlBillingAddress" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" OnSelectedIndexChanged="ddlBillingAddress_SelectedIndexChanged" CssClass="form-control" />
                    <np:AddressBlock ID="BillingAddressBlock" runat="server" ShowSave="false" ShipDestination="false" HideAddressType="true"></np:AddressBlock>
                    <ul class="form-horizontal">

                        <li class="checkbox">
                            <asp:Label ID="ltlDifferentShipAddress" AssociatedControlID="cbxUseUniqueShippingAddress" runat="server">
                                <asp:CheckBox ID="cbxUseUniqueShippingAddress" runat="server" OnCheckedChanged="cbxUseUniqueShippingAddress_CheckedChanged" AutoPostBack="true" />
                                Use a different shipping address
                            </asp:Label>
                        </li>
                    </ul>

                    <asp:Panel runat="server" ID="rowEditShipAddress" Visible="false">
                        <asp:Panel runat="server" ID="rowEditShipAddressHeader" Visible="false">
                            <h6 class="cart-section-heading">Shipping To</h6>

                        </asp:Panel>
                        <asp:DropDownList ID="ddlShippingAddress" Style="max-height: 400px; max-width: 400px" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="400" OnSelectedIndexChanged="ddlShippingAddress_SelectedIndexChanged" CssClass="form-control" />
                        <np:AddressBlock ID="ShippingAddressBlock" runat="server" ShowSave="false" ShipDestination="true" HideAddressType="true"></np:AddressBlock>
                    </asp:Panel>

                    <div class="customer-phone-numbers">
                        <h6 class="cart-section-headings">
                            <asp:Literal ID="ltlEditPhone" runat="server" Text="Telephone Numbers"></asp:Literal></h6>
                        <ul class="phone-number-inputs form-horizontal">
                            <li class="form-group <%= imgPhoneWarning.Visible ? "has-error has-feedback" : "" %>">
                                <asp:Label ID="ltlDayPhone" AssociatedControlID="txtDayPhone" Text="Day Phone " runat="server" CssClass="control-label col-sm-3"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtDayPhone" runat="server" placeholder="Primary Phone" CssClass="form-control"></asp:TextBox>
                                    <asp:PlaceHolder ID="imgPhoneWarning" runat="server" Visible="False">
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                        <span class="sr-only">(error)</span>
                                        <span class="help-block">Phone is required</span>
                                    </asp:PlaceHolder>
                                </div>
                            </li>
                            <li class="form-group">
                                <asp:Label ID="ltlEveningPhone" AssociatedControlID="txtEveningPhone" runat="server" Text="Secondary Phone " CssClass="control-label col-sm-3"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtEveningPhone" runat="server" placeholder="Second Phone" CssClass="form-control"></asp:TextBox>
                                </div>
                            </li>
                    </div>
                    <!-- /customer-phone-numbers -->

                    <ul class="form-horizontal">
                        <asp:Button ID="btnSaveAddress" runat="server" ToolTip="Save" EnableViewState="false" CssClass="btn btn-default" Text="Calculate Shipping" Width="13em" />
                        <span class="help-block"> <i class="fa fa-arrow-up" aria-hidden="true" style="padding-right:2%"></i>Must click the Save button to enter credit card information</span> 
                    </ul>
                </div>
            </asp:Panel>

            <asp:Panel ID="tblViewAddress" runat="server" Visible="false">
                <h6 class="cart-section-heading">
                    <asp:Literal ID="ltlShippingTo" runat="server" Text="Shipping To"></asp:Literal></h6>
                <ul>
                  <h6 class="cart-section-headings">Billing To</h6>
                    <li>
                        <asp:Literal runat="server" ID="sysBillingAddress"></asp:Literal></li>
                    <li>
                        <asp:Literal runat="server" ID="sysDayPhone"></asp:Literal></li>
                    <li>
                        <asp:Literal runat="server" ID="sysEveningPhone"></asp:Literal></li>
                    <h6 class="cart-section-headings">Shipping To</h6>
                    <li>
                        <asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal></li>
                </ul>
                <asp:Button ID="btnEditAddress" runat="server" ToolTip="Edit" EnableViewState="false" CssClass="btn btn-default btn-sm" Text="Edit" Width="5em"/>
            </asp:Panel>

            <asp:Panel ID="rowB2B" runat="server">
                <asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Shipping Date"></asp:Literal>
                <np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
                <asp:Image ID="imgRequestedShipDate" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
                <asp:Literal ID="ltlPONo" runat="server" Text="PO #"></asp:Literal>
                <asp:TextBox ID="txtPO" runat="server" Width="175px"></asp:TextBox>
            </asp:Panel>

            <asp:Panel ID="pnlOrderNotes" runat="server" CssClass="npbody" Visible="false">
                <asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label>
                <np:MultiLineTextBox id="txtNotes" runat="server" LimitInput="150" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></np:MultiLineTextBox>
                <asp:Image ID="imgNotes" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
            </asp:Panel>
            
            <div class="single-checkout-payment-area">
                <asp:Panel ID="pnlAddressDependantInfo" runat="server" Enabled="false">
                    <ComponentArt:CallBack ID="clbackTotals" CssClass="CallBack" runat="server" OnCallback="clbackTotals_Callback">
                        <ClientEvents>
                            <Load EventHandler="clbackTotals_onload" />
                        </ClientEvents>
                        <Content>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                                <h6 class="cart-section-headings">
                                    <asp:Literal ID="ltlShippingProvider" runat="server" Text="Delivery Provider"></asp:Literal></h6>

                                <asp:RadioButtonList ID="rblShipMethod" runat="server" AutoPostBack="False" CellPadding="2" CellSpacing="0" CssClass="radio">
                                </asp:RadioButtonList>

                                <np:OrderExpenses ID="expenses" runat="server" />
                                <asp:Label ID="lblShippingError" runat="server" CssClass="npwarning"></asp:Label>

                                <np:PaymentBlock ID="paymentBlock" runat="server"></np:PaymentBlock>

                                <asp:Button ID="btnAddPayment" runat="server" ToolTip="Add Payment" OnCommand="AddPayment" EnableViewState="false" CssClass="btn btn-default" Text="Add Payment" />

                                <cc1:GoogleReCaptcha ID="ctrlGoogleReCaptcha" runat="server" PublicKey="6LfcxBMUAAAAAHbA0ictefJz2EPtSfDEPTRzVbFr" PrivateKey="6LfcxBMUAAAAAKQKZ-xahgCLuzxqNolg-ctT1niO" />
                                
                                <asp:Button ID="btnOrder" runat="server" ToolTip="Order" EnableViewState="false" CssClass="btn btn-primary" Text="Buy" Width="10em"/>
                                <asp:ImageButton ID="btnPayPalOrder" runat="server" ImageUrl="https://www.paypal.com/en_us/i/btn/btn_xpresscheckout.gif" ToolTip="Go To PayPal" Visible="false" />
                            </asp:PlaceHolder>
                        </Content>
                        <LoadingPanelClientTemplate>
                            <p>Refreshing Rate Information...</p>
                            <asp:Image runat="server" ID="Image1" ImageUrl="~/assets/common/icons/z_icon_anim.gif" Width="16" Height="16" border="0" Visible="true" />
                        </LoadingPanelClientTemplate>
                    </ComponentArt:CallBack>
                </asp:Panel>
            </div>
            </div>

            <div class="my-checkout-col-right">
                <div class="single-checkout-product-area">
                    <np:CartListBlock ID="CartList" runat="server" ShowDiscount="true" AllowEditDiscount="false" DisableQuantityUpdate="true" DisableItemDelete="true"></np:CartListBlock>
                </div>
                <div class="item-not-available">
                      <asp:Image ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" runat="server"></asp:Image>
                      <asp:Label ID="lblOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="Some parts in your order are on back order"></asp:Label>
                 <asp:Image ID="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False" runat="server"></asp:Image>
                   <asp:Label ID="lblUnavailableOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="This item is out of stock and    cannot be backordered"></asp:Label>
                   <asp:Image runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
                  <asp:Label ID="lblUnavailable" runat="server" Text="This part is no longer available; remove it from the cart before you check out" Visible="False" CssClass="npwarning"></asp:Label>
                </div>
                <div class="back-to-cart">
                    <a href="~/commerce/cart.aspx" runat="server">
                    Back To Cart
                    </a>
                </div>
            </div>
        </div>
    </div>
    <style>
        .radio input[type="radio"] {
            margin-left: 0px;
        }
    </style>
</asp:Content>



<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
    <asp:Literal ID="errNoShipping" runat="server" Text="Freight rates are not available for this user." Visible="False"></asp:Literal>
    <asp:Literal ID="errConfigureShipping" runat="server" Text="Rates have not been defined for this order: the subtotal or weight of the order does not meet the cost structure of any defined rate." Visible="False"></asp:Literal>
    <asp:Literal ID="errNoOrder" runat="server" Visible="False" Text="Critical Error : No Order was found."></asp:Literal>
    <asp:Literal ID="errTotalPayments" runat="server" Visible="False" Text="Total Payments must match the Total of the Order"></asp:Literal>
    <asp:Literal ID="errCardNotAuthorized" runat="server" Visible="False" Text="Credit Card Not Authorized"></asp:Literal>
    <asp:Literal ID="errCardNotCharged" runat="server" Visible="False" Text="Credit Card Not Charged"></asp:Literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
    <asp:Literal ID="hdnDayPhoneRequired" runat="server" Text="Day Phone can not be blank."></asp:Literal>
</asp:Content>

