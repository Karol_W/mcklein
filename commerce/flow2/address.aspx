﻿<%@ Page Language="C#" MasterPageFile="~/masters/commerce.master" AutoEventWireup="true" CodeBehind="address.aspx.cs" Inherits="netpoint.commerce.flow2.address" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right white-bg">
		<asp:Label ID="sysError" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
		<table id="tblBillingAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
			<tbody>
				<tr>
					<td class="npsubheader"><asp:Literal ID="ltlBillingAddress" runat="server" Text="Billing Address"></asp:Literal></td>
				</tr>
				<tr>
					<td>
						<asp:Literal ID="ltlBillingAddressDropDown" runat="server" Text="Choose from your saved addresses"></asp:Literal>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DropDownList ID="ddlBillingAddress" Style="max-height: 400px; max-width: 400px" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="400" OnSelectedIndexChanged="ddlBillingAddress_SelectedIndexChanged" />
					</td>
				</tr>            
				<tr>
					<td valign="top">
						<np:AddressBlock ID="BillingAddressBlock" runat="server" ShowSave="false" ShipDestination="false" HideAddressType="true"></np:AddressBlock>                    
					</td>                
				</tr>
				<tr>
				<td colspan="2">
					<asp:Literal ID="ltlDifferentShipAddress" runat="server" Text="Use a different shipping address"></asp:Literal>                
					<asp:CheckBox ID="cbxUseUniqueShippingAddress" runat="server" OnCheckedChanged="cbxUseUniqueShippingAddress_CheckedChanged" AutoPostBack="true" />
				</td>
			</tr>
			</tbody>
		</table>
		<br />
		<asp:Panel ID="pnlShippingAddress" runat="server">
		<table id="tblShippingAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
			<tbody>
				<tr>
					<td class="npsubheader"><asp:Literal ID="Literal1" runat="server" Text="Shipping Address"></asp:Literal></td>
				</tr>
				<tr>
					<td>
						<asp:Literal ID="ltlShippingAddressDropDown" runat="server" Text="Choose from your saved addresses"></asp:Literal>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DropDownList ID="ddlShippingAddress" Style="max-height: 400px; max-width: 400px" runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="400" OnSelectedIndexChanged="ddlShippingAddress_SelectedIndexChanged" />
					</td>
				</tr> 
				<tr>
					<td valign="top">
						<np:AddressBlock ID="ShippingAddressBlock" runat="server" ShowSave="false" ShipDestination="true" HideAddressType="true"></np:AddressBlock>
					</td>                
				</tr>            
			</tbody>
		</table>
		</asp:Panel>
		<br />
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr>
				<td class="npsubheader" colspan="2"><asp:Literal ID="ltlTelephoneNumbers" runat="server" Text="Telephone Numbers"></asp:Literal></td>
			</tr>
			<tr>
				<td class="npbody" style="width:20%;"><asp:Literal ID="ltlDay" runat="server" Text="Day"></asp:Literal></td>
				<td class="npbody">
					<asp:TextBox ID="txtDayPhone" runat="server" Width="175px"></asp:TextBox>
					<asp:Image ID="imgPhoneWarning" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image></td>
			</tr>
			<tr>
				<td class="npbody" style="width:20%;"><asp:Literal ID="ltlEvening" runat="server" Text="Evening"></asp:Literal></td>
				<td class="npbody"><asp:TextBox ID="txtEveningPhone" runat="server" Width="175px"></asp:TextBox></td>
			</tr>
			<tr>
				<td class="npbody" align="right" colspan="2">
					<asp:ImageButton ID="lnkNext" runat="server" ImageUrl="~/assets/common/buttons/continue.gif"></asp:ImageButton>
				</td>
			</tr>
		</table>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">    
    <asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
</asp:Content>
