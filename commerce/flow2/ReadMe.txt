﻿This checkout flow has an address page and a verify page that has shipping and payment options.

To enable this checkout flow, create at least there 3 webflow settings
Source Page: commerce/cart.aspx
Source Page: common/accounts/createaccount.aspx
Source Page: common/accounts/login.aspx

Next Page: commerce/flow2/address.aspx

If using PayPal, modify gateway settings:
Return Url : ~/commerce/flow2/providerreturn.aspx
Cancel URL: ~/commerce/flow2/providerreturn.aspx?cancel=1