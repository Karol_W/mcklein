﻿<%@ Page Language="C#" MasterPageFile="~/masters/commerce.master" AutoEventWireup="true" CodeBehind="verify.aspx.cs" Inherits="netpoint.commerce.flow2.verify" %>
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="CartListBlock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="MCWarning" Src="~/commerce/controls/MultiCurrencyWarning.ascx" %>
<%@ Register TagPrefix="np" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PaymentBlock" Src="~/commerce/controls/PaymentBlock.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<script type="text/javascript">
    function selectProduct(param) {
        clbackTotals.callback(param);
    }

    function clbackTotals_onload(sender, e) {        
        var oldPrefix = sender.CallbackPrefix;
        var newPrefix = document.location.href;
        //alert('old: ' + oldPrefix + '\nnew: ' + newPrefix);
        sender.CallbackPrefix = newPrefix
    }
</script>
	<div class="col-md-9 pull-right white-bg">
		<asp:label id="sysErrorMessage" runat="server" font-bold="true" ForeColor="red"></asp:label>    
		<table cellspacing="0" cellpadding="3" width="100%" border="0">
			<tr>
				<td class="npsubheader"><asp:Literal ID="ltlShippingTo" runat="server" Text="Shipping To"></asp:Literal></td>
				<td class="npsubheader"><asp:Literal ID="ltlBillingTo" runat="server" Text="Billing To"></asp:Literal></td>           
			</tr>
			<tr>
				<td class="npbody" valign="top">
					<asp:HyperLink ID="syslinksa" runat="server" NavigateUrl="address.aspx">
						<asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal>
					</asp:HyperLink></td>
				<td class="npbody" valign="top">
					<asp:HyperLink ID="syslinkba" runat="server" NavigateUrl="address.aspx">
						<asp:Literal runat="server" ID="sysBillingAddress"></asp:Literal>
					</asp:HyperLink></td>           
			</tr> 
			<tr id="rowB2B" runat="server">
				<td class="npbody" valign="top">
					<asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Shipping Date"></asp:Literal><br />
					<np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
					<asp:Image ID="imgRequestedShipDate" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image>
				</td>
				<td class="npbody" valign="top" colspan="2">
					<asp:literal id="ltlPONo" runat="server" text="PO #"></asp:literal><br />
					<asp:TextBox id="txtPO" runat="server" width="175px"></asp:TextBox>
				</td>
			</tr>       
		</table>
		<br />
		<np:CartListBlock ID="CartList" runat="server" ShowDiscount="true" AllowEditDiscount="false" DisableQuantityUpdate="true" DisableItemDelete="true"></np:CartListBlock>    
		<table cellspacing="0" cellpadding="3" width="100%" border="0">
			<tr>
				<td>
					<asp:Image ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" runat="server"></asp:Image>                
					<asp:Label ID="lblOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="Some parts in your order are on back order"></asp:Label>
					<asp:Image ID="imgUnavailableOutOfStock" ImageUrl="~/assets/common/icons/outofstock.gif" Visible="False" runat="server"></asp:Image>
					<asp:Label ID="lblUnavailableOutOfStock" runat="server" Visible="False" CssClass="npwarning" Text="This item is out of stock and cannot be backordered"></asp:Label>
					<asp:Image runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
					<asp:Label ID="lblUnavailable" runat="server" Text="This part is no longer available; remove it from the cart before you check out" Visible="False" CssClass="npwarning"></asp:Label>
				</td>
			</tr>
		</table>    
		<asp:Panel ID="pnlOrderNotes" runat="server" CssClass="npbody" Visible="false">
			<asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label><br />
			<np:MultiLineTextBox id="txtNotes" runat="server" LimitInput="150" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></np:MultiLineTextBox>
			<asp:Image ID="imgNotes" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image><br /><br />
		</asp:Panel>
		<br />
		<ComponentArt:CallBack id="clbackTotals" CssClass="CallBack" runat="server" OnCallback="clbackTotals_Callback">
			<ClientEvents>
				<Load EventHandler="clbackTotals_onload" />
			</ClientEvents>
			<Content>
				<asp:PlaceHolder ID="PlaceHolder1" runat="server">                        
					<table id="tblShipInfo" cellspacing="0" cellpadding="2" width="100%" border="0">
						<tr>
							<td class="npsubheader" style="width:50%;" colspan="2"><asp:Literal ID="ltlShippingProvider" runat="server" Text="Delivery Provider"></asp:Literal></td>
						</tr>
						<tr>
							<td class="npbody">
								<asp:RadioButtonList ID="rblShipMethod" runat="server" AutoPostBack="False" CssClass="npbody"
									CellPadding="2" CellSpacing="0">
								</asp:RadioButtonList>              
							</td>
							<td>                
								<np:OrderExpenses ID="expenses" runat="server" />    
							</td>
						</tr> 
						<tr>
							<td colspan="2">
								<asp:Label ID="lblShippingError" runat="server" CssClass="npwarning"></asp:Label>             
							</td>
						</tr>
					</table>    
					<np:PaymentBlock ID="paymentBlock" runat="server"></np:PaymentBlock>    
					<table class="npbody" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td align="right">
								<asp:ImageButton ID="btnOrder" runat="server" ImageUrl="~/assets/common/buttons/placeyourorder.gif" ToolTip="Order" />
								<asp:ImageButton ID="btnPayPalOrder" runat="server" ImageUrl="https://www.paypal.com/en_us/i/btn/btn_xpresscheckout.gif" ToolTip="Go To PayPal" Visible="false" />
							</td>
						</tr>
					</table>
				</asp:PlaceHolder>
			</Content>
			<LoadingPanelClientTemplate>
				<table width="200" height="86" cellspacing="0" cellpadding="0" border="0">
				  <tr>
					<td>
					<table cellspacing="0" cellpadding="0" border="0">
					<tr>
					  <td style="font-size:10px;">Refreshing Rate Information...&nbsp;</td>
					  <td> <asp:Image runat="server" ID="Image1" ImageUrl="~/assets/common/icons/z_icon_anim.gif" width="16" height="16" border="0" Visible="true"/></td>
					</tr>
					</table>
					</td>
				  </tr>
			  </table>
			</LoadingPanelClientTemplate>
		</ComponentArt:CallBack>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errNoShipping" runat="server" Text="Freight rates are not available for this user." Visible="False"></asp:Literal>
    <asp:Literal ID="errConfigureShipping" runat="server" Text="Rates have not been defined for this order: the subtotal or weight of the order does not meet the cost structure of any defined rate." Visible="False"></asp:Literal>
    <asp:literal id="errNoOrder" runat="server" visible="False" text="Critical Error : No Order was found."></asp:literal>
    <asp:literal id="errTotalPayments" runat="server" visible="False" text="Total Payments must match the Total of the Order"></asp:literal>    
    <asp:literal id="errCardNotAuthorized" runat="server" visible="False" text="Credit Card Not Authorized"></asp:literal>
    <asp:literal id="errCardNotCharged" runat="server" visible="False" text="Credit Card Not Charged"></asp:literal>
    <asp:Literal ID="hdnIncorrectTax" runat="server" Text="More than one tax applies to single or multiple items or shipping on this order and therefore cannot be processed."></asp:Literal>
</asp:Content>
