<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" AutoEventWireup="true" CodeBehind="taxadd.aspx.cs" Inherits="netpoint.admin.commerce.TaxAdd" %>
<%@ Register TagPrefix="np" TagName="TaxMaster" Src="~/admin/commerce/controls/TaxMaster.ascx" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
 <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
       <div class="npadminsubheader"></div>
        <np:TaxMaster id="TaxMaster" runat="server" />        
    </div>
</asp:Content>