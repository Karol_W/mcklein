<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.ShipmentCancel" Codebehind="shipmentcancel.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="3">
                <asp:Image ID="imgHeaderIcon" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Cancel Order #"></asp:Literal>
                <asp:Literal ID="ltlOrderNumber" runat="server" Text="Order Number"></asp:Literal>
                </td>
            <td class="npadminsubheader" align="right"><asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;</td>

        </tr>
        <tr>
            <td class="npadminlabel" valign="top" ><asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
            <td class="npadminbody" colspan="3" >
                <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Rows="10" Columns="50"></asp:TextBox>
                </td>
        </tr>
    </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
