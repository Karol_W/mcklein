<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.TaxList" Codebehind="TaxList.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            
        </tr>
    </table>  
        <asp:GridView 
            Width="100%"
            id="gvTaxes"
            runat="server" 
            AutoGenerateColumns="false" 
            HeaderStyle-CssClass="npadminsubheader"
            CssClass="npadmintable" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            EmptyDataText="No Records Found" OnRowCommand="gvTaxes_RowCommand" OnRowDataBound="gvTaxes_RowDataBound">
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField>
		            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
		            <ItemStyle HorizontalAlign="Center"></ItemStyle>
		            <ItemTemplate>
			            <asp:ImageButton id="btnDelete" runat="server" CommandName="remove" ImageUrl="../../assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaxCode") %>'>
			            </asp:ImageButton>
		            </ItemTemplate>
	            </asp:TemplateField>
	            <asp:BoundField DataField="TaxCode" HeaderText="colTaxCode|Tax Code"></asp:BoundField>
	            <asp:BoundField DataField="TaxName" HeaderText="colName|Name"></asp:BoundField>
	            <asp:BoundField DataField="TaxTypeName" HeaderText="colType|Type"></asp:BoundField>
	            <asp:TemplateField>
		            <ItemStyle HorizontalAlign="Center"></ItemStyle>
		            <ItemTemplate>
			            <asp:HyperLink id="lnkDetail" runat="server" ImageUrl="../../assets/common/icons/detail.gif" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.TaxCode", "Tax.aspx?code={0}") %>'>HyperLink</asp:HyperLink>
		            </ItemTemplate>
	            </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <table width="100%">
        <tr>
            <td align="right"><asp:HyperLink id="lnkNew" runat="server" ImageUrl="../../assets/common/icons/add.gif" NavigateUrl="taxadd.aspx" Text="Add a New Tax" ToolTip="New"></asp:HyperLink></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnDeletionsPermanent" runat="server" visible="False" text="Deletions are permanent. Continue?"></asp:literal>
    <asp:literal id="hdnDelete" runat="server" visible="False" text="Delete Tax"></asp:literal>
    <asp:literal id="hdnDetail" runat="server" visible="False" text="Define tax rates"></asp:literal>
    <asp:literal id="hdnAdd" runat="server" visible="False" text="Add Tax"></asp:literal>
</asp:Content>
