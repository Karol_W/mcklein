<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.Reports" Codebehind="Reports.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Sales Reports</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td valign="top" colspan="3">
				<asp:GridView 
                    id="gvReports"
                    runat="server" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-CssClass="npadminsubheader"
                    CssClass="npadmintable" 
                    RowStyle-CssClass="npadminbody" 
                    AlternatingRowStyle-CssClass="npadminbodyalt" 
                    EmptyDataText="No Reports Found" 
                    OnRowDataBound="gvReports_RowDataBound">
                    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                    <Columns>
                        <asp:TemplateField HeaderText="colName|Name">
							<ItemTemplate>
								<asp:HyperLink id="lnkReport" runat="server" navigateurl="~/admin/common/reports/ReportLaunch.aspx?code="
									Target="_blank"><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="Description" HeaderText="colDescription|Description"></asp:BoundField>
                    </Columns>    
                </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
