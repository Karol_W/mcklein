<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.Shipment" Codebehind="Shipment.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete General" OnClick="btnShipmentDelete_Click"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="4">
                    <asp:Literal ID="ltlShipmentNo" runat="server" Text="Delivery No."></asp:Literal>
                    <asp:Literal ID="sysShipmentNo" runat="server" Text="Shipment #"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal></td>
                <td class="npadminbody"><asp:Literal ID="sysAccountName" runat="server"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlAcctID" runat="server" Text="Acct ID"></asp:Literal></td>
                <td class="npadminbody"><asp:Literal ID="sysAccountID" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlShipAddress" runat="server" Text="Delivery Address"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:DropDownList ID="ddlShipAddress" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShipAddress_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlShipMethod" runat="server" Text="Delivery Method"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:DropDownList ID="ddlShipMethod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShipMethod_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlShipCost" runat="server" Text="Delivery Cost"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:TextBox ID="txtShipCost" runat="server" Width="100px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revShipCost" runat="server" ValidationExpression="[0-9]*\.{0,1}[0-9]{0,2}"
                        ControlToValidate="txtShipCost"><img src="~/assets/common/icons/warning.gif" alt="" /></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Ship Date"></asp:Literal></td>
                <td class="npadminbody"><np:NPDatePicker ID="dtRequestedShipDate" runat="server"></np:NPDatePicker></td>
                <td class="npadminlabel"><asp:Literal ID="ltlTrackingNumber" runat="server" Text="Tracking Number"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtTrackingNumber" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlEstShipDate" runat="server" Text="Estimated Ship Date"></asp:Literal></td>
                <td class="npadminbody"><np:NPDatePicker ID="dtEstShipDate" runat="server"></np:NPDatePicker></td>
                <td class="npadminlabel"><asp:Literal ID="ltlEstDelivDate" runat="server" Text="Est. Del. Date"></asp:Literal></td>
                <td class="npadminbody"><np:NPDatePicker ID="dtEstDelivDate" runat="server"></np:NPDatePicker></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlActualShipDate" runat="server" Text="Actual Ship Date"></asp:Literal></td>
                <td class="npadminbody"><np:NPDatePicker ID="dtActualShipDate" runat="server"></np:NPDatePicker></td>
                <td class="npadminlabel"><asp:Literal ID="ltlActualDelivDate" runat="server" Text="Actual Del. Date"></asp:Literal></td>
                <td class="npadminbody"><np:NPDatePicker ID="dtActualDelivDate" runat="server"></np:NPDatePicker></td>
            </tr>
        </table>
        <asp:gridview
            id="gvShipDetail"
            runat="server" 
            AutoGenerateColumns="false" 
            HeaderStyle-CssClass="npadminsubheader"
            CssClass="npadmintable" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            EmptyDataText="No Records Found" OnRowDataBound="gvShipDetail_RowDataBound">
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:BoundField DataField="OrderID" HeaderText="colOrderNo|OrderNo"></asp:BoundField>
                <asp:BoundField DataField="PartNo" HeaderText="ItemNo|ItemNo"></asp:BoundField>
                <asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name"></asp:BoundField>
                <asp:BoundField DataField="Quantity" HeaderText="colQuantity|Quantity"></asp:BoundField>
            </Columns>
        </asp:gridview>        
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permenent. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnShipmentNotFound" runat="server" Text="Delivery Not Found" Visible="False"></asp:Literal>
</asp:Content>
