<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.commerce.PaymentOptionDetail" Codebehind="PaymentOptionDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="PaymentOptionGeneral" Src="~/admin/commerce/controls/paymentoptiongeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="PaymentOptionMessage" Src="~/admin/commerce/controls/paymentoptionmessage.ascx" %>
<%@ Register TagPrefix="np" TagName="PaymentOptionRestrictions" Src="~/admin/commerce/controls/paymentoptionrestrictions.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" Visible="false" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSaveAndReturn_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminsubheader" id="divNew" runat="server">
        <asp:Literal ID="ltlNew" runat="server" Text="New Payment Option"></asp:Literal>
    </div>
    <div class="npadminbody" id="divTabStrip" runat="server">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                 <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsMessage" Text="Message" ></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsRestrictions" Text="Restrictions"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsStrings" Text="Strings" ></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
    </div>
    <div class="npadminbody">
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvPaymentOptionGeneral">
                <np:PaymentOptionGeneral ID="paymentgeneral" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvPaymentOptionMessage">
                <np:PaymentOptionMessage ID="paymentmessage" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvPaymentOptionRestrictions">
                <np:PaymentOptionRestrictions ID="paymentrestrictions" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvLanguage" >
                <np:CultureObjectStrings ID="cos" runat="server" ></np:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
      </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnPaymentName" runat="server" Visible="False" Text="Payment Name"></asp:Literal>
    <asp:Literal ID="errDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent.  Continue?"></asp:Literal>
</asp:Content>
