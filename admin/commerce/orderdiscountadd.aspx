<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="../../common/controls/NPDatePicker.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.OrderDiscountAdd" Codebehind="OrderDiscountAdd.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
        <table class="npadmintable">
            <tr><td class="npadminsubheader" colspan="2"></td></tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDiscountCode" runat="server" Text="Discount Code"></asp:Literal></td>
                <td>
                    <asp:TextBox ID="txtDiscountCode" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv" runat="server" ErrorMessage="Code is required" ControlToValidate="txtDiscountCode">
					<img src='../../../assets/common/icons/warning.gif' alt="Code is required" /></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
                <td><asp:TextBox ID="txtDescription" runat="server" Columns="60"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlValidFrom" runat="server" Text="Valid From"></asp:Literal></td>
                <td>
                    <np:NPDatePicker ID="dtpStartDate" runat="server"></np:NPDatePicker>&nbsp;
                    <asp:Literal ID="ltlTo" runat="server" Text="To"></asp:Literal>&nbsp;
                    <np:NPDatePicker ID="dtpEndDate" runat="server"></np:NPDatePicker>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlLimit" runat="server" Text="Limit"></asp:Literal></td>
                <td>
                    <asp:TextBox ID="txtLimit" runat="server" Width="75px">1</asp:TextBox>&nbsp;
                    <asp:DropDownList ID="ddlLimitType" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errMustBeNumber" runat="server" Text="Enter a number" Visible="False"></asp:Literal>
    <asp:Literal ID="ltlDisplay" runat="server" Text="Display" Visible="False"></asp:Literal>
    <asp:DropDownList ID="ddlDisplayType" runat="server" Visible="False"></asp:DropDownList>
    <asp:Literal ID="ltlResponse" runat="server" Text="Response" Visible="False"></asp:Literal>
    <asp:DropDownList ID="ddlResponse" runat="server" Visible="False"></asp:DropDownList>
    <asp:Literal ID="errCodeunique" runat="server" Visible="false" Text=" is already in use; choose another discount code"></asp:Literal>
    <asp:Literal ID="errdates" runat="server" Visible="false" Text="Enter an end date that is greater than start date"></asp:Literal>
</asp:Content>
