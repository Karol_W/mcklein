﻿<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="shippingzonedetail.aspx.cs" Inherits="netpoint.admin.commerce.shippingzonedetail" ValidateRequest="false" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">                
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSave_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody" >
        <asp:Literal ID="hdrShippingZone" runat="server" Text=""></asp:Literal>
        <table>
            <tr>
                <td>
                    <asp:Literal ID="ltlZoneName" runat="server" Text="Name" /> 
                    <asp:TextBox ID="txtZoneName" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="cbxActive" runat="server" Text="Active" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="3">Countries</td>
            </tr>
            <tr>
                <td>
                    <center><asp:Literal ID="ltlSet" runat="server" Text="Set"></asp:Literal></center>
                    <input type="hidden" runat="server" id="zone_sel_countries_store" value="">        
                    <asp:ListBox SelectionMode="Multiple" ID="zone_sel_countries" runat="server" Rows="15" Width="250px" />
                </td>
                <td>
                    <input type="button" runat="server" id="btnAddCountry" value="&lt;&lt;" > <br />       
                    <input type="button" runat="server" id="btnRemCountry" value="&gt;&gt;" >
                </td>
                <td>
                    <center><asp:Literal ID="ltlUnset" runat="server" Text="Unset"></asp:Literal></center>
                    <asp:ListBox SelectionMode="Multiple" ID="zone_avail_countries" runat="server" Rows="15" Width="250px" />    
                </td>
            </tr>
            <tr>
                <td colspan="3">States</td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" runat="server" id="zone_sel_states_store" value="">        
                    <asp:ListBox SelectionMode="Multiple" ID="zone_sel_states" runat="server" Rows="15" Width="250px" />
                </td>
                <td>
                    <input type="button" runat="server" id="btnAddState" value="&lt;&lt;" /> <br />
                    <input type="button" runat="server" id="btnRemState" value="&gt;&gt;" >
                </td>
                <td>
                    <asp:ListBox SelectionMode="Multiple" ID="zone_avail_states" runat="server" Rows="15" Width="250px" />  
                </td>
            </tr>
            <tr>
                <td colspan="3">Zip</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="zone_sel_zips" runat="server" Rows="15" Width="250px" />
                </td>
                <td colspan="4">
                    <table style="font-size:smaller">
                        <tr><td>&nbsp Examples of zip/postal code masks definition:</td></tr>
                        <tr><td>&nbsp</td></tr>
                        <tr><td>&nbsp 2204%</td></tr>
                        <tr><td>&nbsp 38245</td></tr> 
                        <tr><td>&nbsp 23%</td></tr>             
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    
</asp:Content>
