<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.OrderDiscount"
    Codebehind="OrderDiscount.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="OrderDiscountOwner" Src="controls/OrderDiscountOwner.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderDiscountAudit" Src="controls/OrderDiscountAudit.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderDiscount" Src="controls/OrderDiscount.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderDiscountCondition" Src="controls/OrderDiscountCondition.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderDiscountBenefit" Src="controls/OrderDiscountBenefit.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:label ID="sysError" runat="server" CssClass="npwarning"></asp:label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteDiscount" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete Coupon"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="btnSaveAndClose" runat="server" ToolTip="Save and Close" ImageUrl="~/assets/common/icons/saveandclose.gif">
                </asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="DiscountTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="DiscountMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsRecipients" Text="Recipient">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsRequirements" Text="Requirements">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsBenefits" Text="Benefits">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsAudit" Text="Audit">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="DiscountMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <np:OrderDiscount ID="disc" runat="server"></np:OrderDiscount>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvRecipients">
                <np:OrderDiscountOwner ID="odo" runat="server"></np:OrderDiscountOwner>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvRequirements">
                <np:OrderDiscountCondition ID="odc" runat="server"></np:OrderDiscountCondition>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvBenefits">
                <np:OrderDiscountBenefit ID="odb" runat="server"></np:OrderDiscountBenefit>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvAudit">
                <np:OrderDiscountAudit ID="oda" runat="server"></np:OrderDiscountAudit>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. \n Continue?"></asp:Literal>
    <asp:Literal ID="errEndBeforeStart" runat="server" Visible="false" Text="Valid from date must preceed the valid to date."></asp:Literal>
</asp:Content>
