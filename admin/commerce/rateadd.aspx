<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="rateadd.aspx.cs" Inherits="netpoint.admin.commerce.rateadd" %>

<%@ Register TagPrefix="np" TagName="ratemaster" Src="~/admin/commerce/controls/ratemaster.ascx" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
 <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
       <div class="npadminsubheader">
       <asp:Literal ID="ltlAddNewRate" runat="server" Text="Add New Rate"></asp:Literal>
       </div>
        <np:ratemaster id="rate" runat="server" />        
    </div>    
</asp:Content>
