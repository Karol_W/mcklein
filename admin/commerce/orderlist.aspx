<%@ Reference Page="~/admin/commerce/shipment.aspx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.OrderList"
    Codebehind="OrderList.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Deliveries</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

        <table class="npadmintable">
            <tr class="npadminsubheader">
                <td colspan="3">
                    &nbsp;
                    <asp:Image ID="sysIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" Visible="false" />
                    &nbsp;
                    <asp:Literal ID="ltlFilters" runat="server" Text="Filters" Visible="false"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlDateRange" runat="server" Text="Date Range:"></asp:Literal>
                </td>
                <td>
                    <table>
                        <tr>
                            <td class="npadminlabel">
                                <asp:Label ID="StartDateLabel" runat="server">Start Date</asp:Label>
                                <br />
                                <np:NPDatePicker ID="StartDate" runat="server"></np:NPDatePicker>
                            </td>
                            <td class="npadminlabel">
                                <asp:Label ID="EndDateLabel" runat="server">End Date</asp:Label>
                                <br />
                                <np:NPDatePicker ID="EndDate" runat="server"></np:NPDatePicker>
                            </td>
                            <td class="npadminlabel">
                                <asp:Literal ID="ltlShipStatus" runat="server" Text="Delivery Status"></asp:Literal>
                                <br />
                                <asp:DropDownList ID="ddlShipStatus" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="npadminlabel">
                    <asp:Button ID="DateFindButton" runat="server" Text="Search" OnClick="DateFindButton_Click">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlOrderRange" runat="server" Text="Order Numbers Between:"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <table width="300px">
                        <tr>
                            <td class="npadminlabel">
                                <asp:Label ID="LowLabel" runat="server">Low Order Number</asp:Label>
                                <br />
                                <asp:TextBox ID="LowOrderText" runat="server" Text="0" Width="100px"></asp:TextBox>
                            </td>
                            <td class="npadminlabel">
                                &nbsp;<asp:Label ID="HighOrderLabel" runat="server">High Order Number</asp:Label>
                                <br />
                                <asp:TextBox ID="HighOrderText" runat="server" Width="100px"></asp:TextBox>
                                <br />
                                <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="npadminlabel">
                    <asp:Button ID="OrderFindButton" runat="server" Text="Search" OnClick="OrderFindButton_Click">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td class="npadminsubheader" colspan="3">
                    &nbsp;
                    <asp:Image ID="sysIndicate" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" Visible="false"/>
                    &nbsp;
                    <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:gridview ID="OrdersGrid" runat="server" AllowPaging="True" AllowSorting="True" CssClass="npadmintable" EmptyDataText="No Records Found"
                        AutoGenerateColumns="False" PageSize="15" OnPageIndexChanging="OrdersGrid_PageIndexChanging" OnRowDataBound="OrdersGrid_RowDataBound" OnSorting="OrdersGrid_Sorting">
                        <RowStyle CssClass="npadminbody" />
                        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                        <Columns>
                           <asp:TemplateField HeaderText="colCancel|Cancel">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Hyperlink ID="lnkCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                                        ToolTip="Cancel Order"></asp:Hyperlink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colDate|Date">
                                <ItemStyle HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("PurchaseDate", "{0:yyyy-MMM-dd}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:HyperLinkField HeaderText="colOrderNo|Order No." DataTextField="OrderID" DataNavigateUrlFields="OrderID" Target="_blank"
                                 DataNavigateUrlFormatString="~/commerce/invoice.aspx?orderno={0}" >
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:HyperLinkField>
                            <asp:BoundField DataField="ConfirmationNumber" HeaderText="Conf#|Conf. No." ItemStyle-HorizontalAlign="center"></asp:BoundField>
                            <asp:HyperLinkField HeaderText="colUser|User" DataTextField="UserID" DataNavigateUrlFields="UserId"
                                 DataNavigateUrlFormatString="~/admin/common/accounts/UserInfo.aspx?userid={0}" />
                            <asp:TemplateField HeaderText="Customer|Customer">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'
                                        NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.AccountID", "~/admin/common/accounts/AccountInfo.aspx?accountid={0}") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colShipment|Delivery">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlShipments" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colShip|Deliver">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Hyperlink ID="lnkCreate" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                                        ToolTip="Create A Shipment"></asp:Hyperlink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                    </asp:gridview>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnShipped" runat="server" Text="Delivered" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnPendingShipment" runat="server" Text="Pending Delivery" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnPartial" runat="server" Text="Partial" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAll" runat="server" Text="All" Visible="False"></asp:Literal>
    <asp:Literal ID="errLowInt" runat="server" Text="Enter an integer for low order number "
        Visible="False"></asp:Literal>
    <asp:Literal ID="hdnNoOrdersFound" runat="server" Text="No Orders Found Matching Specified Criteria"></asp:Literal>
    <asp:Literal ID="hdnCreateShipment" runat="server" Text="Create Delivery"></asp:Literal>
    <asp:Literal ID="hdnCancelOrder" runat="server" Text="Cancel Order"></asp:Literal>
    <asp:Literal ID="hdnShippmentAbbreviation" runat="server" Text="No." ></asp:Literal>
</asp:Content>
