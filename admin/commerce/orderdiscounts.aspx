<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.OrderDiscounts"
    EnableViewState="True" Codebehind="OrderDiscounts.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="../../common/controls/NPDatePicker.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Coupon Management*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Create, view and delete coupons" /></td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkAdd" runat="server" NavigateUrl='OrderDiscountAdd.aspx' ImageUrl='~/assets/common/icons/add.gif'
                    ToolTip="Add New Coupon"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlFilter" runat="server">
                    <asp:Literal ID="ltlDiscountsActive" runat="server" Text="Coupons Active After"></asp:Literal>
                    <np:NPDatePicker ID="dtpStartDate" runat="server"></np:NPDatePicker>
                </asp:Panel>

                <asp:ImageButton ID="btnSearch" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" ToolTip="Search">
                </asp:ImageButton>
            </td>
        </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" PageSize="20"
                        AllowPaging="True" AllowSorting="True" EnableViewState="False" CssClass="npadmintable"
                         EmptyDataText="No Matches Found" OnPageIndexChanging="grid_PageIndexChanging" OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" OnSorting="grid_Sorting">
                        <RowStyle CssClass="npadminbody" />
                        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <PagerStyle CssClass="npadminbody" />
                        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl='~/assets/common/icons/delete.gif'
                                        CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DiscountID") %>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DiscountCode" SortExpression="DiscountCode" HeaderText="Code|Code">
                            </asp:BoundField>
                            <asp:BoundField DataField="Description" SortExpression="Description" HeaderText="Description|Description">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Start|Start" SortExpression="StartDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("StartDate", "{0:yyyy-MMM-dd}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End|End" SortExpression="EndDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("EndDate", "{0:yyyy-MMM-dd}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkEdit" runat="server" NavigateUrl='OrderDiscount.aspx?discountid='
                                        ImageUrl="~/assets/common/icons/edit.gif"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete Coupon" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit Coupon" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. \n Continue?"
        Visible="False"></asp:Literal>
<asp:ImageButton ID="colFilters" runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ToolTip="Collapse" Visible="false"></asp:ImageButton>
<asp:ImageButton ID="expFilters" runat="server" ImageUrl="~/assets/common/icons/expand.gif" Visible="false" ToolTip="Expand"></asp:ImageButton>
<asp:Literal ID="ltlFilter" runat="server" Text="Filters" Visible="false"></asp:Literal>
<asp:ImageButton ID="colResults" runat="server" ImageUrl="~/assets/common/icons/collapse.gif" Visible="false" OnClick="colResults_Click" ToolTip="Collapse"></asp:ImageButton>
<asp:ImageButton ID="expResults" runat="server" ImageUrl="~/assets/common/icons/expand.gif" OnClick="expResults_Click" Visible="false"></asp:ImageButton>
<asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
</asp:Content>
