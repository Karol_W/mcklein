<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" Codebehind="orders.aspx.cs" Inherits="netpoint.admin.commerce.orders" %>
<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Orders</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                    ToolTip="Add Order" NavigateUrl="~/admin/focus/orderadd.aspx"></asp:HyperLink>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                
                <asp:ImageButton ID="colFilter" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colFilter_Click" />
                <asp:ImageButton ID="expFilter" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expFilter_Click" Visible="false" />
                
                <asp:Literal ID="ltlFitlers" runat="server" Text="Save or Edit a Filter Set"></asp:Literal>
                
                <np:SearchSetList runat="server" ID="SearchSetList" />
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" ToolTip="Reset" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2">
                <np:NPSearch ID="search" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colResults" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colResults_Click" Visible="false"/>
                <asp:ImageButton ID="expResults" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expResults_Click" Visible="false" />
                &nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnGo" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnGo_Click" ToolTip="Go"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
        PageSize="25" AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound"
        OnPageIndexChanging="grid_PageIndexChanging" OnSorting="grid_Sorting" EmptyDataText="No Orders Found">
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <HeaderStyle CssClass="npadminsubheader" />
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <Columns>
            <asp:TemplateField SortExpression="OrderID" HeaderText="colID|ID"><HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:HyperLink ID="lnkOrder" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderID") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colCartType|Type"><HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Literal ID="ltlCartType" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:HyperLinkField HeaderText="Customer|Customer" DataTextField="AccountID" DataNavigateUrlFields="AccountID"
                DataNavigateUrlFormatString="~/admin/common/accounts/accountinfo.aspx?accountid={0}&backpage=~/admin/commerce/orders.aspx"
                SortExpression="AccountID" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="colTotal|Total" ItemStyle-HorizontalAlign="right"><HeaderStyle HorizontalAlign="Right" />
                <ItemTemplate>
                    <np:PriceDisplay ID="prcTotal" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colPurchaseDate|Purchase Date" SortExpression="PurchaseDate" ItemStyle-HorizontalAlign="right"><HeaderStyle HorizontalAlign="Right" />
                <ItemTemplate>
                    <asp:Literal ID="ltlPurchaseDate" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
    </asp:GridView>
</asp:Content>
