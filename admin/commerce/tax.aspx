<%@ Reference Control="~/common/controls/npdatepicker.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.Tax"
    Codebehind="Tax.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="TaxDefinition" Src="~/admin/commerce/controls/taxdefinition.ascx" %>
<%@ Register TagPrefix="np" TagName="TaxMaster" Src="~/admin/commerce/controls/TaxMaster.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete" OnClick="btnDelete_Click"></asp:ImageButton> &nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover" LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab" LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsDefinition" Text="Definition" ></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvTaxGeneral">
                <np:TaxMaster id="TaxMaster" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvTaxDefinition">
                 <np:TaxDefinition ID="definition" runat="server" />      
            </ComponentArt:PageView>
       </ComponentArt:MultiPage>     
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnDeletionsPermanent" runat="server" visible="False" text="Deletions are permanent.  Continue?"></asp:literal>
</asp:Content>