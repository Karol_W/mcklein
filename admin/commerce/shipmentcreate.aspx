<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.ShipmentCreate"
    Codebehind="ShipmentCreate.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="4">
                    <asp:Image ID="imgHeaderIcon" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                    <asp:Literal ID="ltlHeader" runat="server" Text="General"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlShippingAddress" runat="server" Text="Delivery Address"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:DropDownList ID="ddlShipAddress" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShipAddress_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <asp:Literal ID="sysShipAddress" runat="server"></asp:Literal>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlShippingMethod" runat="server" Text="Delivery Method"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:DropDownList ID="ddlShipMethod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShipMethod_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <asp:Literal ID="sysShipMethod" runat="server"></asp:Literal>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlShippingCost" runat="server" Text="Delivery Cost"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:TextBox ID="txtShipCost" runat="server" Width="100px">0</asp:TextBox>
                    <asp:RegularExpressionValidator ID="revShipCost" runat="server" ControlToValidate="txtShipCost"
                        ValidationExpression="[0-9]*\.{0,1}[0-9]{0,2}"><asp:Image ID="sysWarn" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Ship Date"></asp:Literal></td>
                <td class="npadminbody">
                    <np:DatePicker ID="dtRequestedShipDate" runat="server"></np:DatePicker>
                </td>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlTrackingNumber" runat="server" Text="Tracking Number"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtTrackingNumber" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlEstShipDate" runat="server" Text="Estimated Ship Date"></asp:Literal></td>
                <td class="npadminbody">
                    <np:DatePicker ID="dtEstShipDate" runat="server"></np:DatePicker>
                </td>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlEstDeliveryDate" runat="server" Text="Estimated Delivery Date"></asp:Literal></td>
                <td class="npadminbody">
                    <np:DatePicker ID="dtEstDelivDate" runat="server"></np:DatePicker>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlActualShipDate" runat="server" Text="Actual Ship Date"></asp:Literal></td>
                <td class="npadminbody">
                    <np:DatePicker ID="dtActualShipDate" runat="server"></np:DatePicker>
                </td>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlActualDeliveryDate" runat="server" Text="Actual Delivery Date"></asp:Literal></td>
                <td class="npadminbody">
                    <np:DatePicker ID="dtActualDelivDate" runat="server"></np:DatePicker>
                </td>
            </tr>
        </table>
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader">
                    <asp:Image ID="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
                    <asp:Literal ID="ltlOrderNumber" runat="server" Text="Order Number"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminbody">
                    <asp:GridView ID="gvOrder" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="npadminsubheader"
                        CssClass="npadmintable" RowStyle-CssClass="npadminbody" AlternatingRowStyle-CssClass="npadminbodyalt"
                        EmptyDataText="No Records Found" OnRowDataBound="gvOrder_RowDataBound">
                        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                        <Columns>
                            <asp:BoundField DataField="PartNo" HeaderText="ItemNo|ItemNo"></asp:BoundField>
                            <asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name"></asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="colAvail|Avail"></asp:BoundField>
                            <asp:TemplateField HeaderText="colQuantityToShip|Quantity to Ship">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQuantity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'
                                        Columns="8">
                                    </asp:TextBox>
                                    <asp:RangeValidator ID="rvQuantity" runat="server" MinimumValue="0" MaximumValue='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'
                                        ControlToValidate="txtQuantity" ErrorMessage="Invalid Quantity">
                                    </asp:RangeValidator>
                                    <asp:Literal ID="ltlOrderDetailID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>'
                                        Visible="False">
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlOtherOrders" runat="server">
            <table class="npadmintable">
                <tr>
                    <td class="npadminsubheader">
                        <asp:Image ID="imgIndicatorOthers" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
                        <asp:Literal ID="ltlOtherOrders" runat="server" Text="Other Orders"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="npadminbody">
                        <asp:GridView ID="gvOther" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="npadminsubheader"
                            CssClass="npadmintable" RowStyle-CssClass="npadminbody" AlternatingRowStyle-CssClass="npadminbodyalt"
                            EmptyDataText="No Records Found" OnRowDataBound="gvOther_RowDataBound">
                            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                            <Columns>
                                <asp:BoundField DataField="OrderID" HeaderText="colOrderNo|OrderNo"></asp:BoundField>
                                <asp:BoundField DataField="PartNo" HeaderText="ItemNo|ItemNo"></asp:BoundField>
                                <asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name"></asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="colAvail|Avail"></asp:BoundField>
                                <asp:TemplateField HeaderText="ltlQuantityToShip|Quantity to Ship">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtQuantityOther" runat="server" Width="60px">0</asp:TextBox>
                                        <asp:RangeValidator ID="rvQuantityOther" runat="server" MinimumValue="0" MaximumValue='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'
                                            ControlToValidate="txtQuantityOther" ErrorMessage="Invalid Quantity">
                                        </asp:RangeValidator>
                                        <asp:Literal ID="ltlOrderDetailIDOther" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>'
                                            Visible="False">
                                        </asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>        
    </div>    
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errNoShippingAddress" runat="server" Text="Enter a delivery address on the order"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errInvalidQuantity" runat="server" Text="The quantity is not valid; reenter a valid quantity" Visible="False"></asp:Literal>
    <asp:Literal ID="errNoShipMethod" runat="server" Text="Select a delivery method for the order"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errInvalidDeliveryDate" runat="server" Text="Delivery date must be greater than the ship date." Visible="false"></asp:Literal>
</asp:Content>
