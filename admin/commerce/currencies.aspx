<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="currencies.aspx.cs" Inherits="netpoint.admin.commerce.currencies" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
 <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
             <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />
             </td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" CssClass="npadmintable" EmptyDataText="No Records Found" OnRowEditing="grid_RowEditing" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowCommand="grid_RowCommand" OnRowDataBound="grid_RowDataBound">
        <RowStyle CssClass="npadminbody" />
        <HeaderStyle CssClass="npadminsubheader" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <Columns>
            <asp:TemplateField HeaderText="Code|Code">
                <ItemTemplate>
                    <asp:Literal ID="ltlCode" runat="server" Text='<%# Bind("CurrencyCode") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtCode" runat="server" Width="50" Text='<%# Bind("CurrencyCode") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ISOCode|ISO Code">
                <ItemTemplate>
                    <asp:Literal ID="ltlISOCode" runat="server" Text='<%# Bind("ISOCode") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlISOCode" runat="server" Width="50" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name|Name">
                <ItemTemplate>
                    <asp:Literal ID="ltlName" runat="server" Text='<%# Bind("Name") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Symbol|Symbol">
                <ItemTemplate>
                    <asp:Literal ID="ltlSymbol" runat="server" Text='<%# Bind("Symbol") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtSymbol" runat="server" Width="50" Text='<%# Bind("Symbol") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Prefix|Prefix">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                   <asp:Image ID="imgIsPrefix" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Bind("IsPrefix") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="chkIsPrefix" runat="server" Checked='<%# Bind("IsPrefix") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Rate|Exchange Rate">
                <ItemTemplate>
                    <asp:Literal ID="ltlExchangeRate" runat="server" Text='<%# Bind("ExchangeRate", "{0:0.0000}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtExchangeRate" runat="server" Width="50" Text='<%# Bind("ExchangeRate", "{0:0.0000}") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Available|Available">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                   <asp:Image ID="imgAvailable" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Bind("Available") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="chkAvailable" runat="server" Checked='<%# Bind("Available") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                     <asp:ImageButton ID="btnDelete" runat="server" CommandName="deletethis" ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.CurrencyCode") %>' ToolTip="Delete" />
                     <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" ToolTip="Edit" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:ImageButton ID="btnSave" runat="server" CommandName="savethis" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.CurrencyCode") %>' ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save" />
                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="cancel" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
<asp:Content ID="contentHidden" runat="server" ContentPlaceHolderID="hiddenslot">
<asp:Literal ID="ltlDeleteWarning" runat="server" Text="Deletions are permanent. Continue?" />
</asp:Content>