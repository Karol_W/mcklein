<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true"
    Codebehind="prices.aspx.cs" Inherits="netpoint.admin.commerce.prices" %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="pricetree" Src="~/admin/catalog/controls/pricelisttree.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/accountpicker.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>View pricelist information</title>
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="PriceTabStrip" ImagesBaseUrl="~/assets/common/admin/"
            ClientScriptLocation="~/scripts" Width="100%" runat="server"
            DefaultGroupCssClass="TopGroupTabs" DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0"
            OnItemSelected="PriceTabStrip_ItemSelected" AutoPostBackOnSelect="true">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsPrices" Text="Price Lists">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsPartnerPrices" Text="Partner Prices">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsDiscountGroup" Text="Discount Groups">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>        
        <asp:panel id="pnlPriceList" runat=server style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px">
            <p class="current-default-price-list-label">
            <asp:Literal ID="ltlDefaultPricelist" runat="server" Text="Current Default Price List"></asp:Literal>: 
            <asp:Literal ID="sysDefaultPricelist" runat="server"></asp:Literal>
            
            <span class="select-price-list-label"><asp:Literal ID="ltlPricelist" runat="server" Text="Select Price List"></asp:Literal></span>
            </p>
            
            <asp:DropDownList ID="ddlPricelists" runat="server" OnSelectedIndexChanged="ddlPricelists_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>&nbsp;            
        </asp:panel>
        <asp:panel id="pnlSpecialPrice" runat="server" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px" Visible="false">
            <table class="npadmintable">                
                <tr>
                    <td class="npadminlabel" style="width:100px;"><asp:Literal ID="ltlBPCode" runat="server" Text="BP Code"></asp:Literal></td>
                    <td width="175px"><np:AccountPicker ID="acctPicker" runat="server" /></td>
                    <td><asp:ImageButton ID="btnSearch" runat="server" ToolTip="Find Special Pricing" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" /></td>
                </tr>
            </table>
        </asp:panel>
        <np:pricetree ID="priceTree" runat="server"></np:pricetree>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDefaultPriceList" runat="server" Text="Default Price List" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnStandard" runat="server" Text="Standard" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnBPSpecialPricing" runat="server" Text="Business Partner Special Pricing"></asp:Literal>
    <asp:Literal ID="hdnPeriodAndVolume" runat="server" Text="Period and Volume Discounts"></asp:Literal>
    <asp:Literal ID="hdnItemGroupDiscount" runat="server" Text="Item Group Discounts"></asp:Literal>
    <asp:Literal ID="hdnManufactuerDiscount" runat="server" Text="Manufactuer Discounts"></asp:Literal>
</asp:Content>
