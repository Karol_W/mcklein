<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="rate.aspx.cs" Inherits="mcklein.admin.commerce.rate" %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="ratemaster" Src="~/admin/commerce/controls/ratemaster.ascx" %>
<%@ Register TagPrefix="np" TagName="ratedetail" Src="~/admin/commerce/controls/ratedetail.ascx" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete General" OnClick="btnDeleteGeneral_Click"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave2_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0" >
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsDefinition" Text="Definitions">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400" OnPageSelected="ProjectMultiPage_OnPageSelected" AutoPostBack="true" >
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvRateMaster">
                <np:ratemaster ID="theMaster" runat="server" />        
            </ComponentArt:PageView>
            <ComponentArt:PageView id="pvRateDetail" runat="server" cssclass="npadminbody">
                <np:ratedetail ID="theDetail" runat="server" /> 
            </ComponentArt:PageView>
       </ComponentArt:MultiPage>
  </div>
</asp:Content>

<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?" Visible="False"></asp:Literal>
</asp:Content>