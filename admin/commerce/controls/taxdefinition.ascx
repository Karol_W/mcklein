<%@ Control Language="C#" AutoEventWireup="true" Codebehind="taxdefinition.ascx.cs" Inherits="netpoint.admin.commerce.controls.taxdefinition" %>  
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/npdatepicker.ascx" %>
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>   
<asp:GridView 
    ID="gridDetail" 
    runat="server" 
    CssClass="npadmintable" 
    AutoGenerateColumns="False"
    OnRowCancelingEdit="gridDetail_RowCancelingEdit" 
    OnRowDataBound="gridDetail_RowDataBound" 
    OnRowDeleting="gridDetail_RowDeleting" 
    OnRowEditing="gridDetail_RowEditing" 
    OnRowUpdating="gridDetail_RowUpdating" 
    EmptyDataText="No Records Found">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <FooterStyle CssClass="npadminbody" />
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDeleteDetail" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaxDetailID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colLowerLimit|Lower Limit">
            <ItemTemplate>
                <asp:Literal ID="ltlLowerLimit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LowerLimit", "{0:0.00##}") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtLowerLimit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LowerLimit", "{0:0.00##}") %>'
                    Columns="5" MaxLength="14"></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colUpperLimit|Upper Limit">
            <ItemTemplate>
                <asp:Literal ID="ltlUpperLimit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UpperLimit", "{0:0.00##}") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtUpperLimit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UpperLimit", "{0:0.00##}") %>'
                    Columns="10" MaxLength="14"></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCharge|Charge">
            <ItemTemplate>
                <asp:Literal ID="ltlCharge" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Charge", "{0:0.00####}") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtCharge" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Charge", "{0:0.00####}") %>'
                    Columns="8" MaxLength="14"></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEffective|Effective">
            <ItemTemplate>
                <asp:Literal ID="ltlEffectiveDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EffectiveDate", "{0:yyyy-MMM-dd}") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <np:npdatepicker id="dtpEffectiveDate" runat="server" selecteddate='<%# DataBinder.Eval(Container.DataItem, "EffectiveDate") %>'></np:npdatepicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colExpiration|Expiration">
            <ItemTemplate>
                <asp:Literal ID="ltlExpirationDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExpirationDate", "{0:yyyy-MMM-dd}") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <np:npdatepicker id="dtpExpirationDate" runat="server" selecteddate='<%# DataBinder.Eval(Container.DataItem, "ExpirationDate") %>'></np:npdatepicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" Wrap="false"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaxDetailID") %>' />
            </ItemTemplate>          
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                    CommandName="cancel" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaxDetailID") %>' />&nbsp;
                <asp:ImageButton ID="btnSaveDetail" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    CommandName="update" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaxDetailID") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" CommandName="add" CommandArgument="0" OnClick="btnAdd_Click" ToolTip="Add" />&nbsp;
</div>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete Tax" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit Tax" Visible="False"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save Tax" Visible="False"></asp:Literal>
<asp:Literal ID="errTaxOverlaps" runat="server" Text="You defined a tax period that overlaps an existing period. Check the start and expire date and reenter." Visible="False"></asp:Literal>
<asp:Literal ID="errExp" runat="server" Text="Enter an expiration date that is later than effective date" Visible="false"></asp:Literal>