<%@ Control Language="C#" AutoEventWireup="true" Codebehind="paymentoptionrestrictions.ascx.cs"
    Inherits="netpoint.admin.commerce.controls.paymentoptionrestrictions" %>
<table class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCardholder" runat="server" Text="Cardholder"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="chkCardholderFlag" runat="server"></asp:CheckBox>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBankCardNumber" runat="server" Text="Bank Card Number"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="chkBankCardNumberFlag" runat="server" AutoPostBack="true"></asp:CheckBox>
        </td>        
        <td class="npadminlabel">
            <asp:Literal ID="ltlSystem" runat="server" Text="System"></asp:Literal>
        </td>
        <td style="width: 202px">
            <asp:Image ID="imgSystemFlag" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible="False"></asp:Image>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAddress" runat="server" Text="Address"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="chkAddressFlag" runat="server"></asp:CheckBox>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlIssueDate" runat="server" Text="Issue Date"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:CheckBox ID="chkIssueDateFlag" runat="server"></asp:CheckBox>
        </td>                          
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="chkEmailFlag" runat="server" />
        </td>        
        <td class="npadminlabel">
            <asp:Literal ID="ltlIssueNumber" runat="server" Text="Issue Number"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:CheckBox ID="chkIssueNumberFlag" runat="server"></asp:CheckBox>
        </td>        
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPhone" runat="server" Text="Phone"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="chkPhoneFlag" runat="server" />
        </td>    
        <td class="npadminlabel">
            <asp:Literal ID="ltlExpirationDate" runat="server" Text="Expiration Date"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:CheckBox ID="chkExpirationDateFlag" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="rowBankCardNumberRules" runat="server">
        <td class="npadminsubheader" colspan="6"><asp:Literal ID="ltlBankCardNumberRules" runat="server" Text="Bank Card Number Rules"></asp:Literal></td>
    </tr>
    <tr>
        <td colspan="6">
            <asp:gridview
                id="gvBankCardNumberRules"
                runat="server" 
                AutoGenerateColumns="false" 
                HeaderStyle-CssClass="npadminsubheader"
                CssClass="npadmintable" 
                RowStyle-CssClass="npadminbody" 
                AlternatingRowStyle-CssClass="npadminbodyalt" 
                EmptyDataText="No Records Found" 
                OnRowDataBound="gvBankCardNumberRules_RowDataBound"
                OnRowCancelingEdit="gvBankCardNumberRules_RowCancelingEdit"
                OnRowEditing="gvBankCardNumberRules_RowEditing" 
                OnRowUpdating="gvBankCardNumberRules_RowUpdating" 
                OnRowDeleting="gvBankCardNumberRules_RowDeleting" >
                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                                CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RuleID") %>'>
                            </asp:ImageButton>
                        </ItemTemplate>
                        <EditItemTemplate>                                
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colIINRange|IIN Range">
                        <ItemTemplate>
                            <asp:Literal ID="ltlIINRange" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "IINRange") %>'></asp:Literal>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIINRange" runat="server" Columns="25" Text='<%# DataBinder.Eval(Container.DataItem, "IINRange") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colPANLength|PAN Length">
                        <ItemTemplate>
                            <asp:Literal ID="ltlPANLength" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PANLength") %>'></asp:Literal>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPANLength" runat="server" Columns="15" Text='<%# DataBinder.Eval(Container.DataItem, "PANLength") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colCSCLength|CSC Length">
                        <ItemTemplate>
                            <asp:Literal ID="ltlCSCLength" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CSCLength") %>'></asp:Literal>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCSCLength" runat="server" Columns="2" Text='<%# DataBinder.Eval(Container.DataItem, "CSCLength") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>            
                    <asp:TemplateField HeaderText="colLuhnFlag|Luhn">
                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Image ID="imgUseLuhnFlag" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                                Visible='<%# DataBinder.Eval(Container.DataItem, "UseLuhnFlag") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkUseLuhnFlag" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "UseLuhnFlag") %>' />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="center" Wrap="false" />
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif"
                                CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RuleID") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="cancel" ImageUrl="~/assets/common/icons/cancel.gif" />
                            <asp:ImageButton ID="btnSave" runat="server" CommandName="update" ImageUrl="~/assets/common/icons/save.gif" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:gridview>
        </td>
    </tr>
    <tr>
        <td align="right" colspan="6">
            <asp:ImageButton ID="btnNew" runat="server" CommandName="add" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New" OnClick="btnNew_Click">
            </asp:ImageButton>
        </td>
    </tr>
</table>
<asp:Literal ID="hdnCancel" runat="server" Text="Cancel" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="false"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?" Visible="False"></asp:Literal>