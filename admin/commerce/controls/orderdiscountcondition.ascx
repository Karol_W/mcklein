<%@ Reference Control="~/admin/common/controls/partpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="../../common/controls/PartPicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.commerce.controls.OrderDiscountCondition" Codebehind="OrderDiscountCondition.ascx.cs" %>

<asp:Label id="sysError" runat="server" CssClass="npwarning"></asp:Label>
<asp:Panel ID="pnlDetail" runat="server" Visible="false">

    <table class="npadmintable" >
        <tr>
            <td class="npadminlabel">&nbsp;
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel" 
                    OnClick="btnCancel_Click" /></td>
            <td class="npadminlabel" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save" 
                    OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="sysConditionType" runat="server" Text="Type"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList id="ddlConditionType" Runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlConditionType_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlValue" runat="server" Text="Value"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList id="ddlConditionValue" Runat="server" Visible="False"></asp:DropDownList>
				<np:PartPicker id="ppkConditionValue" runat="server" visible="false"></np:PartPicker>
				<asp:TextBox id="txtConditionValue" Runat="server" ></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlQuantity" runat="server" Text="Quantity"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtQuantity" runat="server" Columns="7" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlErrorExpression" runat="server" Text="Error Expression"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtErrorExpression" runat="server" Columns="55"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="btnCOSDetail" runat="server" ImageUrl="~/assets/common/icons/language.gif" ToolTip="Add Language" /></td>
        </tr>
    </table>
    </asp:Panel>
<asp:Panel ID="pnlConditions" runat="server">
<asp:gridview id="gridCondition" AutoGenerateColumns="False" cssclass="npadmintable" runat="server"
    OnRowDataBound="gridCondition_RowDataBound" OnRowDeleting="gridCondition_RowDeleting" 
    OnRowEditing="gridCondition_RowEditing" EmptyDataText="No Entries Found" >
	<RowStyle CssClass="npadminbody" />
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<AlternatingRowStyle CssClass="npadminbodyalt" />
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	<Columns>
	    <asp:TemplateField>
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton ID="btnDelete" Runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete" 
				    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DiscountConditionID") %>' />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colType|Type">
			<ItemTemplate>
				<asp:Literal id="ltlType" Runat="server"></asp:Literal>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colValue|Value">
			<ItemTemplate>
				<asp:Literal ID="ltlValue" Runat="server"></asp:Literal>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colQty|Qty">
		    <ItemStyle HorizontalAlign="center" />
			<ItemTemplate>
				<asp:literal id="ltlQuantity" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:literal>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colError|Error">
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
			    <asp:Literal ID="ltlDesc" runat="server"></asp:Literal>
				<asp:ImageButton ID="btnLanguage" Runat="server" ImageUrl="~/assets/common/icons/language.gif" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton ID="btnEdit" Runat="server" CommandName="edit" imageurl="~/assets/common/icons/edit.gif" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:gridview>
<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnAdd" Runat="server" tooltip="Add" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />&nbsp;
</div>
</asp:Panel>
<asp:literal id="hdnDelete" runat="server" Visible="False" Text="Delete Condition"></asp:literal>
<asp:literal id="hdnEdit" runat="server" Visible="False" Text="Edit Condition"></asp:literal>
<asp:literal id="hdnCancel" runat="server" Visible="False" Text="Cancel"></asp:literal>
<asp:literal id="hdnSave" runat="server" Visible="False" Text="Save"></asp:literal>
<asp:literal id="errDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:literal>
<asp:Literal id="errMustBeNumber" runat="server" Text="You must enter a number" Visible="False"></asp:Literal>
<asp:Literal id="hdnAdd" runat="server" Text="Add Requirement" Visible="False"></asp:Literal>
<asp:Literal ID="hdnNoMessageDefined" runat="server" Text="[no message defined]" Visible="false"></asp:Literal>
