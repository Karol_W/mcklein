<%@ Control Language="c#" Inherits="netpoint.admin.commerce.controls.OrderDiscountAudit" Codebehind="OrderDiscountAudit.ascx.cs" %>

<asp:gridview id="grid" runat="server" AutoGenerateColumns="False" cssclass="npadmintable" EmptyDataText="No Discounts Redeemed"
	AllowPaging="True" AllowSorting="True" OnRowDataBound="grid_RowDataBound" PageSize="10" OnPageIndexChanging="grid_PageIndexChanging">
	<RowStyle CssClass="npadminbody" />
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<AlternatingRowStyle CssClass="npadminbodyalt" />
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	<PagerStyle CssClass="npadminbody" />
	<Columns>
		<asp:TemplateField SortExpression="OrderID" HeaderText="colOrder|Order">
			<ItemTemplate>
				<asp:HyperLink ID="lnkOrder" Runat="server"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="AccountID" HeaderText="Customer|Customer">
			<ItemTemplate>
				<asp:HyperLink ID="lnkAccount" Runat="server"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="UserID" HeaderText="colUser|User">
			<ItemTemplate>
				<asp:HyperLink ID="lnkUser" Runat="server"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="colDate|Date" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
		<asp:TemplateField HeaderText="colCost|Cost" SortExpression="cost">
			<ItemTemplate>
				<asp:Literal ID="ltlCost" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "cost", "{0:c}") %>'></asp:Literal>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:gridview>
