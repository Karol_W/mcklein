<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ratemaster.ascx.cs"
    Inherits="mcklein.admin.commerce.controls.ratemaster" %>

<%@ Register Src="~/admin/common/controls/partpicker.ascx" TagPrefix="np" TagName="partpicker" %>

<table class="npadmintable">
    <tr>
        <td colspan="2">
            <asp:Label ID="sysError" runat="server" cssclass="npwarning"></asp:Label></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlRateName" runat="server" Text="Rate Name"></asp:Literal></td>
        <td class="npadminbody">
            <table id="tblCountry" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtRateName" runat="server" Columns="50" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvRateName" runat="server" ControlToValidate="txtRateName"
                            ErrorMessage="RequiredFieldValidator">
                                <asp:Image ID="imgWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBlurb" runat="server" Text="Blurb"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtBlurb" runat="server" Columns="50" Width="400px" TextMode="MultiLine"
                Height="60px"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlProvider" runat="server" Text="Provider" Visible="false"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlProviderType" runat="server" Visible="false">
            </asp:DropDownList>
            <asp:TextBox ID="txtProviderCode" runat="server" Columns="10" Visible="false"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlFlags" runat="server" Text="Flags" Visible="false"></asp:Literal></td>
        <td class="npadminbody">
            <asp:CheckBox ID="chkNotifyLaterFlag" runat="server" Text="Notify Later" Visible="false"></asp:CheckBox>&nbsp;
            <asp:CheckBox ID="chkRoleRestricted" runat="server" Text="Restrict Role" Visible="false">
            </asp:CheckBox>
        </td>
    </tr>
    <tr>
       <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlShipping" runat="server" Text="Standard Shipping Options"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminbody" valign="top" colspan="2">
            <table class="npadmintable">
                <tr>
                    <td class="npadminlabel" align="center">
                        <asp:Literal ID="ltlBasis" runat="server" Text="Basis"></asp:Literal></td>
                    <td class="npadminlabel" align="center">
                        <asp:Literal ID="ltlUnitName" runat="server" Text="Unit Name"></asp:Literal></td>
                    <td class="npadminlabel" align="center">
                        <asp:Literal ID="ltlChargeType" runat="server" Text="Charge Type"></asp:Literal></td>
                    <td class="npadminlabel" align="center">
                        <asp:Literal ID="ltlSortCode" runat="server" Text="Sort Code"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="npadminbody" align="center">
                        <asp:DropDownList ID="ddlCheckField" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCheckField_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td class="npadminbody" align="center">
                        <asp:DropDownList ID="ddlWeight" runat="server">
                        </asp:DropDownList>
                        <np:partpicker id="ppkBasis" runat="server" visible="false" />
                        <asp:TextBox id="dscCode" runat="server" visible="false" />
                    </td>
                    <td class="npadminbody" align="center">
                        <asp:DropDownList ID="ddlChargeType" runat="server">
                        </asp:DropDownList></td>
                    <td class="npadminbody" align="center">
                        <asp:TextBox ID="txtSortCode" runat="server" Width="65px" Columns="7"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSortCode" runat="server" ControlToValidate="txtSortCode"
                            ErrorMessage="RequiredFieldValidator">
                            <asp:Image ID="imgWarn1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                        </asp:RequiredFieldValidator></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <asp:Panel ID="realTimeShipping" runat="server">
    <tr>
       <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlRealTime" runat="server" Text="Real Time Shipping Options"></asp:Literal></td>
    </tr>
    <tr> 
        <td class="npadminbody" valign="top" colspan="2">
            <table class="npadmintable">
                <tr>
                    <td class="npadminlabel">
                        <asp:Literal ID="ltlRealProvider" runat="server" Text="Provider"></asp:Literal></td>
                    <td class="npadminlabel">
                        <asp:Literal ID="ltlRealTimeName" runat="server" Text="Provider Service Type"></asp:Literal></td>        
                </tr>
                <tr>
                    <td class="npadminbody">
                        <asp:DropDownList ID="ddlRealTimeProviders" runat="server" Width="125" AutoPostBack="True" OnSelectedIndexChanged="ddlRealTimeProviders_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td class="npadminbody">
                        <asp:DropDownList ID="ddlRealTimeProviderQuotes" runat="server" Enabled=false Width="200" >
                        </asp:DropDownList>
                    </td>            
                </tr>
            </table>
            <asp:Literal ID="NoRealTimeProviders" runat="server" Text="" Visible="false"></asp:Literal>
        </td>
    </tr>
    </asp:Panel>
</table>

<asp:Literal ID="errIsRequired" runat="server" Text="Required" Visible="False"></asp:Literal>
<asp:Literal ID="errMustBeNumber" runat="server" Text="Enter a number" Visible="False"></asp:Literal>
