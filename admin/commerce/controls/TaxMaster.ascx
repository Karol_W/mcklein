<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxMaster.ascx.cs" Inherits="netpoint.admin.commerce.controls.TaxMaster" %>

<table class="npadmintable">
    <tr>        
        <td class="npadminbody" colspan="2">
            <asp:Label ID="sysErrorMessage" runat="server" CssClass="npwarning" Visible="false" EnableViewState="false"></asp:Label><br />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlTaxCode" runat="server" Text="Tax Code"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtTaxCode" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvTaxCode" runat="server" ControlToValidate="txtTaxCode" ErrorMessage="RequiredFieldValidator">
		    <img src="../../../assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtName" runat="server" Width="350px" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvTaxName" runat="server" ControlToValidate="txtName" ErrorMessage="RequiredFieldValidator">
		    <img src="../../../assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlPostalCode" runat="server" Text="Postal Code"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtPostalCode" runat="server" TextMode="MultiLine" Rows="3" Width="350px"></asp:TextBox></td>
    </tr>    
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlShippingTax" runat="server" Text="Apply Tax to Freight"></asp:Literal></td>
        <td class="npadminbody"><asp:CheckBox ID="chkFreight" runat="server" /></td>
    </tr>
</table>