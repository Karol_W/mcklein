<%@ Control Language="c#" Inherits="netpoint.admin.commerce.controls.OrderDiscountBenefit" Codebehind="OrderDiscountBenefit.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<asp:Panel ID="pnlGrid" runat="server">    
    <asp:GridView 
        ID="gridBenefit" 
        runat="server" 
        AutoGenerateColumns="False" 
        CssClass="npadmintable" 
        EmptyDataText="No Benefits Defined" 
        OnRowDataBound="gridBenefit_RowDataBound" 
        OnRowDeleting="gridBenefit_RowDeleting" 
        OnRowEditing="gridBenefit_RowEditing">
    <RowStyle CssClass="npadminbody"></RowStyle>
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" CommandArgument='<%# Eval("DiscountBenefitID") %>'
                    ImageUrl="~/assets/common/icons/delete.gif" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colName|Name">
            <ItemTemplate>
                <asp:Literal ID="ltlBenefitDescription" runat="server" Text='<%# Eval("BenefitDescription") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colType|Type">
            <ItemTemplate>
                <asp:Literal ID="ltlBenefitType" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colValue|Value">
            <ItemTemplate>
                <asp:Literal ID="ltlBenefitValue" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPriceType|Price Type">
            <ItemTemplate>
                <asp:Literal ID="ltlPriceType" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPrice|Price">
            <ItemTemplate>
                <asp:Literal ID="ltlPrice" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" ToolTip="Edit" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div style="text-align:right">
    <asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />
</div>
</asp:Panel>
<asp:Panel ID="pnlBenefit" runat="server" Visible="False">
    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel">&nbsp;
                <asp:ImageButton ID="btnCancelEdit" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" OnClick="btnCancelEdit_Click" ToolTip="Cancel Edit" /></td>
            <td class="npadminlabel" align="right">
                <asp:ImageButton ID="btnSaveDetail" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSaveDetail_Click" ToolTip="Save Detail" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel" ><asp:Literal ID="ltlBenefitName" runat="server" Text="Benefit Name"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtBenefitName" runat="server" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDiscountType" runat="server" Text="Discount Type"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlDiscountType" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="sysDiscountValue" runat="server" Text="Value"></asp:Literal></td>
            <td class="npadminbody">
                <asp:Literal ID="sysConditions" runat="server" Visible="false"></asp:Literal>
                <asp:Literal ID="ltlCash" runat="server" Text="Cash" Visible="False"></asp:Literal>
                <np:partpicker id="ppk" runat="server"></np:partpicker>
                <asp:DropDownList ID="ddlExpenseType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExpenseType_SelectedIndexChanged" ></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlCartCalcMethod" runat="server" Text="Cart Calc. Method"></asp:Literal>
                <asp:Literal ID="ltlQuantity" runat="server" Text="Quantity"></asp:Literal>
                <asp:Literal ID="ltlExpenseDetail" runat="server"></asp:Literal>
            </td>
            <td class="npadminbody">
                <asp:TextBox ID="txtQuantity" runat="server" Columns="7">1</asp:TextBox>
                <asp:Image ID="imgWarn0" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:DropDownList ID="ddlCalcType" runat="server"></asp:DropDownList>
                <asp:DropDownList ID="ddlExpenseDetail" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlPriceType" runat="server" Text="Price Type"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlDiscountBenefitPrice" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlDiscountBenefitPrice_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="sysDiscountPrice" runat="server" Text="Discount Value"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtPrice" runat="server" Columns="7" Width="75px"></asp:TextBox>
                <asp:Image ID="imgWarn1" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="hdnEdit" Visible="False" Text="Edit" runat="server"></asp:Literal>
<asp:Literal ID="hdnDelete" Visible="False" Text="Delete" runat="server"></asp:Literal>
<asp:Literal ID="hdnDeletionsPermanent" Visible="False" Text="Deletions are permanent. Continue?" runat="server"></asp:Literal>
<asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add Benefit"></asp:Literal>
<asp:Literal ID="hdnCash" runat="server" Visible="false" Text="Cash"></asp:Literal>
<asp:Literal ID="errMustBeNumber" runat="server" Text="You must enter a number" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPart" runat="server" Text="Item to Add" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCondition" runat="server" Text="Condition to Discount" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSubtotal" runat="server" Text="Subtotal Discount Method" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSubtract" runat="server" Text="Subtract from Price List Price" Visible="False"></asp:Literal>
<asp:Literal ID="hdnPercent" runat="server" Text="Discount by Percent" Visible="False"></asp:Literal>
<asp:Literal ID="hdnExpenseType" runat="server" Text="Expense type" Visible="false"></asp:Literal>
<asp:Literal ID="hdnShipping" runat="server" Text="Shipping Rates" Visible="false"></asp:Literal>
<asp:Literal ID="hdnHandling" runat="server" Text="Handling Rates" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPaymentMethod" runat="server" Text="Payment Options" Visible="false"></asp:Literal>