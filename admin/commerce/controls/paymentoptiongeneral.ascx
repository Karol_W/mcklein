<%@ Control Language="C#" AutoEventWireup="true" Codebehind="paymentoptiongeneral.ascx.cs" Inherits="netpoint.admin.commerce.controls.paymentoptiongeneral" %>  
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<div class="npwarning"><asp:Literal ID="sysError" runat="server"></asp:Literal></div>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlOptionName" runat="server" Text="Option Name"></asp:Literal>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtPaymentName">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
            </asp:RequiredFieldValidator></td>
        <td class="npadminbody"><asp:TextBox ID="txtPaymentName" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCode" runat="server" Text="Code"></asp:Literal>
            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="RequiredFieldValidator"
                ControlToValidate="txtPaymentCode">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
            </asp:RequiredFieldValidator></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtPaymentCode" runat="server" Visible="False"></asp:TextBox>
            <asp:Literal ID="ltlPaymentCode" runat="server" Visible="False" Text="Payment Code"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlAvailableDate" runat="server" Text="Available Date"></asp:Literal></td>
        <td class="npadminbody"><np:npdatepicker id="calAvailableDate" runat="server"></np:npdatepicker></td>
        <td class="npadminlabel"><asp:Literal ID="ltlExpireDate" runat="server" Text="Expire Date"></asp:Literal></td>
        <td class="npadminbody"><np:npdatepicker id="calExpireDate" runat="server"></np:npdatepicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlAccountType" runat="server" Text="Account Type"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlAccountType" /></td>
        <td class="npadminlabel"><asp:Literal ID="ltlIndustryCode" runat="server" Text="Industry"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlIndustryCode" /></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlPaymentCharge" runat="server" Text="Payment Charge"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><asp:TextBox ID="txtPaymentCharge" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlProvider" runat="server" Text="Provider" /></td>
        <td class="npadminbody" colspan="3">
            <asp:DropDownList ID="ddlProvider" runat="server" />
            <asp:Literal ID="ltlNoProvider" runat="server" Visible="false" Text="No Payment Providers" />
        </td>
    </tr>
</table>
<asp:Literal ID="errDates" runat="server" Visible="false" Text="Enter an expiration date that is later than the available date"></asp:Literal>
<asp:Literal ID="errNameCode" runat="server" Visible="false" Text="A payment option with this name or code already exists; choose another"></asp:Literal>
