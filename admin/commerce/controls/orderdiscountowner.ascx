<%@ Reference Control="~/admin/common/controls/accountpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="../../common/controls/AccountPicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.commerce.controls.OrderDiscountOwner" Codebehind="OrderDiscountOwner.ascx.cs" %>
<div id="divNew" runat="server" visible="false">
<table cellpadding="1" cellspacing="0" width="100%" border="0">
    <tr>
        <td class="npadminlabel">&nbsp;
            <asp:ImageButton ID="btnCancelEdit" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" OnClick="btnCancelEdit_Click" ToolTip="Cancel Edit" /></td>
        <td class="npadminlabel" colspan="2"><asp:Literal ID="ltlCurrentID" runat="server" Visible="false" Text="Current ID"></asp:Literal></td>
        <td class="npadminlabel" align="right">
            <asp:ImageButton ID="btnSaveDetail" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSaveDetail_Click" ToolTip="Save Detail" />&nbsp;</td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
        <td><asp:DropDownList ID="ddlOwnerType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOwnerType_SelectedIndexChanged"></asp:DropDownList></td>
        <td class="npadminlabel"><asp:Literal ID="ltlRecipient" runat="server" Text="Recipient"></asp:Literal></td>
        <td><asp:DropDownList ID="ddlOwnerValue" runat="server" Visible="false"></asp:DropDownList>
            <np:AccountPicker ID="acct" runat="server" Visible="false"></np:AccountPicker>
            <asp:Literal ID="ltlAll" runat="server" Text="All" Visible="false"></asp:Literal></td>
    </tr>
</table>
</div>
<div id="divGrid" runat="server">
<asp:GridView ID="grid" ShowHeader="False" Width="100%" AutoGenerateColumns="False"
    runat="server" OnRowCommand="grid_RowCommand" OnRowDataBound="grid_RowDataBound"
    CssClass="npadminbody" DataKeyNames="DiscountOwnerID"
    EmptyDataText="No Recipient Filters Defined" OnRowEditing="grid_RowEditing">
    <EmptyDataRowStyle HorizontalAlign="center" Height="50" CssClass="npadminempty" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DiscountOwnerID") %>'
                    ImageUrl="~/assets/common/icons/remove.gif" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Literal ID="ltlOwnerType" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Literal ID="ltlOwnerValue" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DiscountOwnerID") %>' ImageUrl="~/assets/common/icons/edit.gif" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <RowStyle CssClass="npadminbody" />
</asp:GridView>
</div>
<table cellpadding="1" cellspacing="0" width="100%" border="0">
    <tr>
        <td align="right" class="npadminbody">
            <asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add" CommandName="add"
                ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />&nbsp;</td>
    </tr>
</table>
<asp:Literal ID="hdnRemove" Visible="False" Text="Remove" runat="server"></asp:Literal>
<asp:Literal ID="hdnAdd" Text="Add Owner" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEdit" Text="Edit Owner" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCancel" Text="Cancel" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="hdnSave" Text="Save" runat="server" Visible="False"></asp:Literal>
