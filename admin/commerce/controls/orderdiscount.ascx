<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.commerce.controls.OrderDiscount" Codebehind="OrderDiscount.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>

<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDiscountCode" runat="server" Text="Discount Code"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="sysDiscountCode" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtDescription" runat="server" Width="500px" Columns="40"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlValidFrom" runat="server" Text="Valid From"></asp:Literal>
            <asp:Image ID="sysInvalidDateImg" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" />
        </td>
        <td>
            <np:DatePicker ID="dtpStartDate" runat="server"></np:DatePicker>
            &nbsp;
            <asp:Literal ID="ltlTo" runat="server" Text="To"></asp:Literal>&nbsp;
            <np:DatePicker ID="dtpEndDate" runat="server"></np:DatePicker>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlLimit" runat="server" Text="Limit"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtLimit" runat="server" Width="75px" Columns="7">1</asp:TextBox>&nbsp;
            <asp:DropDownList ID="ddlLimitType" runat="server">
            </asp:DropDownList></td>
    </tr>
</table>
<asp:Panel ID="pnlTemp" runat="server" Visible="False">
    <table>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDisplay" runat="server" Text="Display"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlDisplayType" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlResponse" Text="Response" runat="server"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlResponse" runat="server">
                </asp:DropDownList></td>
        </tr>
    </table>
</asp:Panel>

