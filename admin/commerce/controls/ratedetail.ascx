<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ratedetail.ascx.cs" Inherits="netpoint.admin.commerce.controls.ratedetail" %>  
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Panel ID="definitionListPanel" runat="server">
    <asp:gridview 
        ID="gvList" 
        runat="server" 
        CssClass="npadmintable" 
        AutoGenerateColumns="False" 
        AllowPaging="True" 
        PageSize="25" 
        EmptyDataText="No Records Found" 
        OnRowDataBound="gvList_RowDataBound"         
        OnPageIndexChanging="gvList_PageIndexChanging"
        OnRowEditing="gvList_RowEditing"
        OnRowCommand="gvList_RowCommand">
        <RowStyle CssClass="npadminbody" />
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderRatesDefinitionID") %>' CommandName="remove">
                    </asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colDefName|Definition Name">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblDefName" Text='<%# DataBinder.Eval(Container.DataItem, "DefinitionName") %>'>
                    </asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtDefName" runat="server" Width="250px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.DefinitionName") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>                
            <asp:TemplateField HeaderText="colActive|Active">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="imgActiveItem" runat="server" ImageUrl="~/assets/common/icons/checked.gif">
                    </asp:Image>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="chkActive" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Active") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="15%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderRatesDefinitionID") %>'
                    CommandName="edit"></asp:ImageButton>  
                    <asp:ImageButton ID="btnDetail" runat="server" ImageUrl="~/assets/common/icons/detail.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderRatesDefinitionID") %>'
                    OnCommand="gvList_RowEdit"></asp:ImageButton>                                                          
                </ItemTemplate>
                <EditItemTemplate>
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderRatesDefinitionID") %>'
                    CommandName="savethis"></asp:ImageButton>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancelthis"></asp:ImageButton>
            </EditItemTemplate>
            </asp:TemplateField>            
        </Columns>
    </asp:gridview>
    <div class="npadminbody" style="text-align:right">
        <asp:ImageButton ID="btnNewDefinition" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New Definition" OnClick="btnNewDef_Click" />
    </div>
    <br/>
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Group"></asp:Literal>
    <asp:Literal ID="hdnSave" runat="server" Visible="False" Text="Save Group"></asp:Literal>
    <asp:Literal ID="hdnCancel" runat="server" Visible="False" Text="Cancel"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete Group"></asp:Literal>
    <asp:Literal ID="hdnDetails" runat="server" Visible="False" Text="Edit Details"></asp:Literal>
    <asp:Literal ID="hdnNew" runat="server" Visible="False" Text="Add New Group"></asp:Literal>
    <asp:Literal ID="hdnConfirmDelete" runat="server" Visible="False" Text="Deletions are permanent. Continue?" />
    <asp:Literal ID="hdnNoDefName" runat="server" Visible="False" Text="Definition name required." />    
</asp:Panel>
<asp:Panel ID="definitionDetailPanel" runat="server" Visible="false">
<asp:Label ID="sysError" runat="server" cssclass="npwarning"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminbody">
            <asp:Literal ID="sysMsg" runat="server" Text="Zones assigned to the definition: " />
            <asp:Label ID="lblZoneName" runat="server" />
        </td>
        <td class="npadminbody" align="right">
            <asp:ImageButton ID="btnGoBack" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnCommand="ShowDefinitionList"></asp:ImageButton>&nbsp;
        </td>
    </tr>
</table>
<asp:gridview 
    ID="gvZones" 
    runat="server" 
    CssClass="npadmintable" 
    AutoGenerateColumns="False" 
    AllowPaging="True" 
    PageSize="5" 
    EmptyDataText="No Records Found" 
    OnRowDataBound="gvZones_RowDataBound"    
    OnPageIndexChanging="gvZones_PageIndexChanging">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" OnCommand="btnRemoveZone_Click">
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colZoneName|Zone Name">
            <ItemTemplate>
                <asp:Label runat="server" ID="lblZoneName" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>                
        <asp:TemplateField HeaderText="colActive|Active">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Image ID="imgActiveItem" runat="server" ImageUrl="~/assets/common/icons/checked.gif">
                </asp:Image>
            </ItemTemplate>
        </asp:TemplateField>        
    </Columns>
</asp:gridview>
<br />
<div class="npadminbody" style="text-align:right">
    <asp:Literal ID="errNoZone" runat="server" Text="Select a zone to add." Visible="false"></asp:Literal>
    <asp:DropDownList ID="ddlZones" runat="server"></asp:DropDownList>
    <asp:ImageButton ID="btnAddZone" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add Zone" OnCommand="btnAddZone_Click" />
</div>

<!-- REAL TIME SHIPPING OPTIONS -->
<div class="npadminbody">
<asp:RadioButton ID="rbRealTime" runat="server" AutoPostBack="true" Checked ="false" Text="Use real time quote" GroupName="rbButtons" OnCheckedChanged="rbRealTime_CheckedChanged"/>&nbsp;&nbsp;&nbsp;
<asp:RadioButton ID="rbRateMatrix" runat="server" AutoPostBack="true" Text="Use rate calculation matrix" GroupName="rbButtons" OnCheckedChanged="rbRateMatrix_CheckedChanged"/><br /><br />
</div>

<asp:Literal ID="ltlShippingWhouse" runat="server" Text="Ship From Warehouse" Visible="false"></asp:Literal>
<asp:DropDownList ID="ddlWarehouses" runat="server" Width="200" Visible="false" Enabled="true"></asp:DropDownList>
                        
<asp:gridview
    id="gvRate"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowCommand="gvRate_RowCommand" OnRowDataBound="gvRate_RowDataBound" OnRowEditing="gvRate_RowEditing">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDeleteDetail" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RateDetailID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colLowerLimit|Lower Rate">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <%# DataBinder.Eval(Container, "DataItem.LesserAmount", "{0:0.00}") %>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtLesserAmount" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container, "DataItem.LesserAmount") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colUpperLimit|Upper Rate">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <%# DataBinder.Eval(Container, "DataItem.GreaterAmount", "{0:0.00}") %>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtGreaterAmount" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container, "DataItem.GreaterAmount") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCharge|Charge">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <np:PriceDisplay ID="prcCharge" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Charge") %>' />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtCharge" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container, "DataItem.Charge") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEffectiveDate|Effective Date">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <%# DataBinder.Eval(Container, "DataItem.EffectiveDate", "{0:yyyy-MMM-dd}") %>
            </ItemTemplate>
            <EditItemTemplate>
                <np:datepicker id="dtEffectiveDate" runat="server" selecteddate='<%# DataBinder.Eval(Container, "DataItem.EffectiveDate") %>'></np:datepicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colExpirationDate|Expiration Date">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <%# DataBinder.Eval(Container, "DataItem.ExpirationDate", "{0:yyyy-MMM-dd}")%>
            </ItemTemplate>
            <EditItemTemplate>
                <np:datepicker id="dtExpirationDate" runat="server" selecteddate='<%# DataBinder.Eval(Container, "DataItem.ExpirationDate") %>'></np:datepicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RateDetailID") %>'
                    CommandName="edit"></asp:ImageButton>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RateDetailID") %>'
                    CommandName="savethis"></asp:ImageButton>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancelthis"></asp:ImageButton>
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:gridview>

<br />
<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New" OnClick="btnNew_Click" />
</div>
</asp:Panel>
<asp:Literal ID="errMustBeNumber" runat="server" Text="Enter a number" Visible="False"></asp:Literal>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?" Visible="False"></asp:Literal>
<asp:Literal ID="hdnDeleteDetail" runat="server" Text="Delete Rate Detail" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEditDetail" runat="server" Text="Edit Rate Details" Visible="False"></asp:Literal>
<asp:Literal ID="hdnGreater" runat="server" Text="Greater amount must be less than 900,000,000,000.00" Visible="false"></asp:Literal>
<asp:Literal ID="hdnGreater2" runat="server" Text="Greater amount must be greater than or equal to 0.00" Visible="false"></asp:Literal>
<asp:Literal ID="hdnLesser" runat="server" Text="Lesser amount must be greater or equal to 0.00 " Visible="false"></asp:Literal>
<asp:Literal ID="hdnLesser2" runat="server" Text="Lesser Amount must be less than 900,000,000,000.00." Visible="false"></asp:Literal>
<asp:Literal ID="hdnDates" runat="server" Text="Enter an effective date that is earlier than expiration date" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCharge" runat="server" Text="Charge must be less than 900,000,000,000.00" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCharge2" runat="server" Text="Charge must be greater than or equal to 0.00" Visible="false"></asp:Literal>
<asp:Literal ID="hdnEffect" runat="server" Text="Enter a valid date for effective date" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSaveRateDetail" runat="server" Text="Save Detail" Visible="false"></asp:Literal>
<asp:Literal ID="hdnGoBack" runat="server" Visible="False" Text="Cancel"></asp:Literal>
<asp:Literal ID="hdnSaveAndReturn" runat="server" Visible="False" Text="Save and return to definitions"></asp:Literal>