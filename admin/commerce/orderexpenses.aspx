<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true"
    Codebehind="orderexpenses.aspx.cs" Inherits="netpoint.admin.commerce.orderexpenses" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" CssClass="npadmintable"
            OnRowCancelingEdit="grid_RowCancelingEdit" OnRowDataBound="grid_RowDataBound" EmptyDataText="No Records Found"
            OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <RowStyle CssClass="npadminbody" />
            <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
            <HeaderStyle CssClass="npadminsubheader" />
            <Columns>               
                <asp:TemplateField HeaderText="colCode|Code">
                    <ItemTemplate>
                        <asp:Literal ID="ltlCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ExpenseCode") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colName|Name">
                    <ItemTemplate>
                        <asp:Literal ID="ltlName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ExpenseName") %>'></asp:Literal>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtName" runat="server" Columns="15" Text='<%# DataBinder.Eval(Container.DataItem, "ExpenseName") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="colSort|Sort">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Literal ID="ltlSort" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SortOrder") %>'></asp:Literal>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtSortOrder" runat="server" Columns="3" Text='<%# DataBinder.Eval(Container.DataItem, "SortOrder") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colInTotals|In Totals" Visible="false">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Image ID="imgActive" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                            Visible='<%# DataBinder.Eval(Container.DataItem, "Active") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Active") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="colIfZero|Display If Zero">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Image ID="imgVisibleAlways" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                            Visible='<%# DataBinder.Eval(Container.DataItem, "VisibleAlways") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkVisibleAlways" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "VisibleAlways") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colInDetails|In Details" Visible="false">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Image ID="imgShowDetails" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                            Visible='<%# DataBinder.Eval(Container.DataItem, "ShowDetails") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkShowDetails" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "ShowDetails") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colTax|Tax" Visible="false">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Hyperlink ID="lnkTax" runat="server" ToolTip="Edit Tax for this Expense" ImageUrl="~/assets/common/icons/tax.gif"  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="colTranslate|Translate">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>                        
                         <asp:ImageButton ID="btnCOS" runat="server" ImageUrl="~/assets/common/icons/language.gif" CommandName="cos">
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="colEditName|Edit Name">
                    <ItemStyle HorizontalAlign="center" Wrap="false" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="btnCancel" runat="server" CommandName="cancel" ImageUrl="~/assets/common/icons/cancel.gif" />
                        <asp:ImageButton ID="btnSave" runat="server" CommandName="update" ImageUrl="~/assets/common/icons/save.gif"
                            ToolTip="Save" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center"  HeaderText="colViewDetails|View Details">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkDetail" runat="server" ImageUrl="~/assets/common/icons/detail.gif"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnCancel" runat="server" Text="Cancel"></asp:Literal>
    <asp:Literal ID="hdnSave" runat="server" Text="Save"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete"></asp:Literal>
    <asp:Literal ID="hdnDetail" runat="server" Text="Expense Detail"></asp:Literal>
    <asp:Literal ID="hdnAltLanguage" runat="server" Text="Alternate Languages"></asp:Literal>
    <asp:Literal ID="hdnDeleteWarning" runat="server" Text="All relevant definition data for this expense will also be deleted.  Continue?"></asp:Literal>
    <asp:Literal ID="hdnEditTax" runat="server" Text="Edit Tax for Expense"></asp:Literal>
    <asp:Literal ID="hdnMustBeNumber" runat="server" Text=" must be a number"></asp:Literal>
</asp:Content>
