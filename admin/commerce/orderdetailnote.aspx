<%@ Page AutoEventWireup="true" MasterPageFile="~/masters/admindefault.master" Language="c#" Inherits="netpoint.admin.commerce.OrderDetailNote" Codebehind="OrderDetailNote.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<script type="text/javascript">
    function cbkNoteSaved_onload(sender, e) {        
        var oldPrefix = sender.CallbackPrefix;
        var newPrefix = document.location.href;
        //alert('old: ' + oldPrefix + '\nnew: ' + newPrefix);
        sender.CallbackPrefix = newPrefix
    }
</script>
    <table class="npadmintable" id="Table1" cellspacing="0" cellpadding="1" width="100%"
        border="0">
        <tr>
            <td class="npadminsubheader" align="left">
            &nbsp;
                <asp:literal id="ltlHeader" runat="server" Visible="false" text="Notes For"></asp:literal>
                <asp:literal id="ltlHeaderNewOD" runat="server" Visible="false" text="New Row"></asp:literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:Image id="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif"></asp:Image>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:textbox id="txtNote" runat="server" columns="70"  rows="22" textmode="MultiLine"></asp:textbox>
            </td>
        </tr>
    </table>
    <ComponentArt:CallBack ID="cbkNoteSaved" runat="server" OnCallback="cbkNoteSaved_Callback">
        <ClientEvents>
	        <Load EventHandler="cbkNoteSaved_onload" />
	    </ClientEvents>
    </ComponentArt:CallBack>
</asp:Content>
