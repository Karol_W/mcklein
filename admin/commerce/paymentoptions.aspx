<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.PaymentOptions"
    Codebehind="PaymentOptions.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnNew" runat="server" CommandName="add" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New">
                </asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:gridview
            id="gvOptions"
            runat="server" 
            AutoGenerateColumns="false" 
            HeaderStyle-CssClass="npadminsubheader"
            CssClass="npadmintable" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            EmptyDataText="No Records Found" OnRowCommand="gvOptions_RowCommand" OnRowDataBound="gvOptions_RowDataBound">
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                            CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PaymentID") %>'>
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PaymentName" HeaderText="colName|Name"></asp:BoundField>
                <asp:TemplateField HeaderText="colCharge|Charge" ItemStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <np:PriceDisplay ID="prcCharge" runat="server" PriceObject='<%# DataBinder.Eval(Container.DataItem, "PaymentCharge") %>'/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="AvailableDateString" HeaderText="colAvailableDate|Available Date">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ExpireDateString" HeaderText="colExpireDate|Expire Date">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="colSystem|System">
                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Image ID="imgSystemFlag" runat="server" ImageUrl="~/assets/common/icons/checked.gif">
                        </asp:Image>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="editthis" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PaymentID") %>'
                            ImageUrl="../../assets/common/icons/edit.gif"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?"
        Visible="False"></asp:Literal>
</asp:Content>
