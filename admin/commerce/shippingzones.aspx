﻿<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" CodeBehind="shippingzones.aspx.cs" Inherits="netpoint.admin.commerce.shippingzones" %>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
            <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/commerce/shippingzonedetail.aspx?zoneid=0" ToolTip="New Zone"></asp:HyperLink>
            &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:gridview 
            ID="gvZones" 
            runat="server" 
            CssClass="npadmintable" 
            AutoGenerateColumns="False" 
            AllowPaging="True" 
            PageSize="25" 
            EmptyDataText="No Records Found" 
            OnRowDataBound="gvZones_RowDataBound" 
            OnRowDeleting="gvZones_RowDeleting" 
            OnPageIndexChanging="gvZones_PageIndexChanging">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete">
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colZoneName|Zone Name">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblZoneName" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="colActive|Active">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Image ID="imgActiveItem" runat="server" ImageUrl="~/assets/common/icons/checked.gif">
                        </asp:Image>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="../../assets/common/icons/edit.gif">
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview>
        &nbsp
        <asp:Label ID="sysError" runat="server" cssclass="npwarning" Text="" Visible="false"></asp:Label>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hiding">    
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Zone"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete Zone"></asp:Literal>
    <asp:Literal ID="hdnConfirmDelete" runat="server" Visible="False" Text="Deletions are permanent. Continue?" />
    <asp:Literal ID="errShippingZoneInUse" runat="server" Visible="false" Text="Shipping Zone is in use. Please remove the shipping zone from all shipping rates and try again." />
</asp:Content>