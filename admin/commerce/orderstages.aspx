<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.commerce.OrderStages"
    Codebehind="OrderStages.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
            <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/commerce/orderstage.aspx?stageid=0" ToolTip="New"></asp:HyperLink>
            &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:gridview 
            ID="gvStages" 
            runat="server" 
            CssClass="npadmintable" 
            AutoGenerateColumns="False" 
            EmptyDataText="No Records Found" OnRowDataBound="gvStages_RowDataBound" OnRowDeleting="gvStages_RowDeleting">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete">
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colStageName|Stage Name">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblStageName" Text='<%# DataBinder.Eval(Container.DataItem, "StageName") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colCode|Code">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "StageCode") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colWeight|Weight">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "StageWeight") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSort|Sort">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "SortOrder") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSystem|System">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Image ID="imgSystemItem" runat="server" ImageUrl="~/assets/common/icons/checked.gif">
                        </asp:Image>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="../../assets/common/icons/edit.gif">
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Stage"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete Stage"></asp:Literal>
</asp:Content>
