<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true"
    Codebehind="orderstage.aspx.cs" Inherits="netpoint.admin.commerce.orderstage"
    %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSave_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                 <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLanguage" Text="Language">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvStageGeneral">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlStageName" runat="server" Text="Stage Name"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtName">
                                <asp:Image ID="imgErr0" runat="server" ImageUrl="../../assets/common/icons/warning.gif">
                                </asp:Image>
                            </asp:RequiredFieldValidator></td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlStageCode" runat="server" Text="Stage Code"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtCode">
                                <asp:Image ID="Image1" runat="server" ImageUrl="../../assets/common/icons/warning.gif">
                                </asp:Image>
                            </asp:RequiredFieldValidator></td>
                        <td class="npadminbody">
                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                            <asp:Literal ID="sysStageCodeDisplay" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlWeight" runat="server" Text="Weight"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvWeight" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtWeight">
                                <asp:Image ID="Image2" runat="server" ImageUrl="../../assets/common/icons/warning.gif">
                                </asp:Image>
                            </asp:RequiredFieldValidator></td>
                        <td>
                            <asp:TextBox ID="txtWeight" runat="server"></asp:TextBox></td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvSort" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtSort">
                                <asp:Image ID="Image3" runat="server" ImageUrl="../../assets/common/icons/warning.gif">
                                </asp:Image>
                            </asp:RequiredFieldValidator></td>
                        <td>
                            <asp:TextBox ID="txtSort" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlRGBColor" runat="server" Text="RGB Pipeline Color"></asp:Literal></td>
                        <td>
                                <np:HtmlColorDropDown id="ddColor" runat="server" Palette="WebSafe" SortOption="ByValue"
                                    AutoColorChangeSupport="true">
                                </np:HtmlColorDropDown>
                        </td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlSystem" runat="server" Text="System"></asp:Literal></td>
                        <td>
                            <asp:Image ID="imgSystem" runat="server" ImageUrl="../../assets/common/icons/checked.gif">
                            </asp:Image></td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvLanguage">
                <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errMustBeNumber" runat="server" Visible="False" Text=" must be a number"></asp:Literal>
</asp:Content>
