<%@ Page Language="C#" MasterPageFile="~/masters/admindefault.master" Inherits="netpoint.admin.commerce.priceeval"
    Codebehind="priceeval.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="Server">
    <div style="padding-left: 10px;">
        <div id="divPriceEvalHeader" class="npadminbody" style="padding-top: 10px; padding-bottom: 10px;">
            <b>
                <asp:Literal ID="ltlPartner" runat="server" Text="BP Code"></asp:Literal>:&nbsp;&nbsp;
                <asp:Literal ID="sysAccountID" runat="server"></asp:Literal>
                <br />
                <asp:Literal ID="ltlItemCode" runat="server" Text="Item No."></asp:Literal>:&nbsp;&nbsp;
                <asp:Literal ID="sysPartNo" runat="server"></asp:Literal>
                <br />
                <asp:Literal ID="ltlPricelist" runat="server" Text="Partner Price List"></asp:Literal>:
                &nbsp;&nbsp;
                <asp:Literal ID="sysPricelist" runat="server"></asp:Literal>
            </b>        
        </div>
        <asp:GridView ID="grid" runat="server" OnRowDataBound="grid_OnRowDataBound" AutoGenerateColumns="False"
            Width="100%" EmptyDataText="No Records Found" CssClass="npadmintable">
            <RowStyle CssClass="npadminbody" />
            <AlternatingRowStyle CssClass="npadminbodyalt" Height="50px" />
            <HeaderStyle CssClass="npadminsubheader" />
            <EmptyDataRowStyle CssClass="npadminempty" />
            <FooterStyle CssClass="npadminbodyalt" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Literal ID="ltlOperandPrice" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Operator"  Visible="false" />
                <asp:BoundField DataField="Operand" Visible="false" />
                <asp:TemplateField HeaderText="colPrice|Price">
                    <ItemStyle HorizontalAlign="right" Font-Bold="true" />
                    <ItemTemplate>
                        <np:PriceDisplay ID="prcPrice" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colPriceType|Price Type">
                    <ItemTemplate>
                        <asp:Literal ID="ltlDesc" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colQualifier|Qualifier">
                    <ItemTemplate>
                        <asp:Literal ID="ltlPriceAdjustmentValue" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="colPriceList|Price List">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkPricelist" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle CssClass="npadminbody" />
        </asp:GridView>
        <div id="divPriceEvalFinalPrice" class="npadminbody" style="text-align:right">
            <b>
                <asp:Literal ID="ltlFinalPrice" runat="server" Text="Final Price"></asp:Literal>:
                <np:PriceDisplay ID="prcFinalPrice" runat="server" />
            </b>
        </div>
        <br />
        <asp:Panel ID="pnlData" runat="server" Visible="false">
            <div id="divPriceEvalDataHeader" class="npadminbody">
                <b>
                    <asp:Literal ID="ltlPricingData" runat="server" Text="Pricing Data"></asp:Literal>
                    <asp:CheckBox ID="sysShowData" runat="server" AutoPostBack="true" />
                </b>
            </div>
            <asp:GridView ID="gvwPartPricing" runat="server" AutoGenerateColumns="False" Width="100%"
                EmptyDataText="No Records Found" CssClass="npadmintable">
                <RowStyle CssClass="npadminbody" />
                <AlternatingRowStyle CssClass="npadminbodyalt" Height="50px" />
                <HeaderStyle CssClass="npadminsubheader" />
                <EmptyDataRowStyle CssClass="npadminempty" />
                <FooterStyle CssClass="npadminbodyalt" />
                <Columns>
                    <asp:BoundField HeaderText="colPricelist|Price List" DataField="PriceListCode" />
                    <asp:BoundField HeaderText="colPriceType|Price Type" DataField="SpecialPriceType" />
                    <asp:BoundField HeaderText="colPriceValue|Value" DataField="SpecialPriceValue" />
                    <asp:BoundField HeaderText="colFactorType|Factor Type" DataField="PriceFactorType" />
                    <asp:BoundField HeaderText="colFactorValue|Factor Value" DataField="PriceFactorValue" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>
