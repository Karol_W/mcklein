<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#"
    Inherits="netpoint.admin.commerce.Rates" Codebehind="Rates.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register Src="~/admin/common/controls/partpicker.ascx" TagPrefix="np" TagName="partpicker" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                      tooltip="Add Rate" NavigateUrl="~/admin/commerce/rateadd.aspx?type=S"></asp:HyperLink>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:GridView runat="server" CssClass="npadmintable" AutoGenerateColumns="false" ID="gvRates" EmptyDataText="No Records Found" OnRowCommand="gvRates_RowCommand" OnRowDataBound="gvRates_RowDataBound">
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <RowStyle CssClass="npadminbody" />
            <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
            <HeaderStyle CssClass="npadminsubheader" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                            CommandName="del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderRatesMasterID") %>'>
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colName|Name">
                    <ItemTemplate>
                        <asp:Literal ID="ltlRate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RateName") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colBasis|Basis">
                    <ItemTemplate>
                        <asp:Literal ID="ltlRateBasis" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CheckFieldDescription") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colUnit|Unit">
                    <ItemTemplate>
                        <asp:Literal ID="ltlUnit" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitDescription") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colCharge|Charge">
                    <ItemTemplate>
                        <asp:Literal ID="ltlCharge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChargeTypeDescription") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEditRate" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                            CommandName="ed" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderRatesMasterID") %>'>
                        </asp:ImageButton>
                    </ItemTemplate> 
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?"
        Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Text="Add Rate" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete Rate" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit Rate" Visible="False"></asp:Literal>
</asp:Content>
