<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.errorlog" Codebehind="errorlog.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Web application log</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr class="npadminsubheader">
                <td colspan="3">
                    &nbsp;
                    <asp:Image ID="sysIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" Visible="false"/>
                    &nbsp;
                    <asp:Literal ID="ltlFilters" runat="server" Text="Filters" Visible="false"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlDateRange" runat="server" Text="Date Range"></asp:Literal>
                </td>
                <td>
                    <table>
                        <tr>
                            <td class="npadminlabel">
                                <asp:Label ID="StartDateLabel" runat="server">Start Date</asp:Label>
                                <br />
                                <np:NPDatePicker ID="StartDate" runat="server"></np:NPDatePicker>
                            </td>
                            <td class="npadminlabel">
                                <asp:Label ID="EndDateLabel" runat="server">End Date</asp:Label>
                                <br />
                                <np:NPDatePicker ID="EndDate" runat="server"></np:NPDatePicker>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="npadminlabel">
                    <asp:Button ID="DateFindButton" runat="server" Text="Find" OnClick="DateFindButton_Click">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td class="npadminsubheader" colspan="3">
                    &nbsp;
                    <asp:Image ID="sysIndicate" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" Visible="false" />
                    &nbsp;
                    <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:gridview ID="ErrorsGrid" runat="server" AllowPaging="True" AllowSorting="True" CssClass="npadmintable" EmptyDataText="No Records Found" Width="100%"
                        AutoGenerateColumns="False" PageSize="50" OnPageIndexChanging="ErrorsGrid_PageIndexChanging" OnRowDataBound="ErrorsGrid_RowDataBound" OnSorting="ErrorsGrid_Sorting">
                        <RowStyle CssClass="npadminbody" />
                        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                        <Columns>
                            <asp:ButtonField DataTextField="LogID" />
                            <asp:TemplateField HeaderText="TimeStamp|Time Stamp" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTmstmp" runat="server" Text='<%# Bind("Tmstmp", "{0:yyyy-MMM-dd HH:mm}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Message" HeaderText="Message|Message"></asp:BoundField>
                            <asp:BoundField DataField="UserID" HeaderText="User|User"></asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                    </asp:gridview>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
