<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.support.workorderadd" Codebehind="workorderadd.aspx.cs" %>
<%@ Register Src="controls/workordergeneral.ascx" TagName="workordergeneral" TagPrefix="np" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" Runat="Server" >
    <table class="npadminpath" width="100%">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" ToolTip="Save"/></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlSubHeader" runat="server" Text="New Service Call"></asp:Literal></td>
        </tr>
    </table>
    <div class="npadminbody" style="height:400px;width:100%">
        <np:workordergeneral ID="workordergeneral" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="hidden" ContentPlaceHolderID="hiddenslot" Runat="Server">
</asp:Content>

