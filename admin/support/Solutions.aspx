<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.support.Solutions" CodeBehind="Solutions.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Knowledge Base Solutions</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="SolutionDetail.aspx" ToolTip="Add Knowledge-Base Entry" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter" OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter" OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or edit a filter set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right" style="white-space:nowrap;">&nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults"
                    OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" Visible="false"/></td>
            <td class="npadminsubheader" colspan="1" align="right" style="white-space:nowrap;">&nbsp;
                <asp:ImageButton runat="server" ID="btnFind" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnFind_Click" ToolTip="Find" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="gvResults" runat="server" Width="100%" PageSize="20" CssClass="npadminbody"
                    AllowPaging="True" AllowSorting="true"  AutoGenerateColumns="False"  
                    OnRowCommand="gvResults_RowCommand" OnPageIndexChanging="gvResults_PageIndexChanging" OnSorting="gvResults_Sorting"
                    EmptyDataText="No Entries Found">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:HyperLinkField DataTextField="SolutionID" DataNavigateUrlFields="SolutionID"
                            DataNavigateUrlFormatString="SolutionDetail.aspx?SolutionID={0}" HeaderText="colID|ID" SortExpression="SolutionID">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="center" />
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText="colSolution|Knowledge-Base Entry" SortExpression="SolutionName">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("SolutionName") %>' NavigateUrl='<%# Bind("SolutionID","SolutionDetail.aspx?SolutionID={0}") %>'></asp:HyperLink>
                                <br />
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colInternal|Internal" SortExpression="InternalFlag">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Image ID="imgInternal" runat="Server" ImageUrl="~/assets/common/icons/availableyes.gif" Visible='<%# Bind("InternalFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colFeatured|Featured" SortExpression="FeaturedFlag">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Image ID="imgFeatured" runat="Server" ImageUrl="~/assets/common/icons/favorites.gif" Visible='<%# Bind("FeaturedFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colActive|Active" SortExpression="ActiveFlag">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Image ID="imgActive" runat="Server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Bind("ActiveFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
