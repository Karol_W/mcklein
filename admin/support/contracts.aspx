<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.contracts" Codebehind="contracts.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Service Contracts</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkNew" runat="server" NavigateUrl="~/admin/support/contractadd.aspx" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New"></asp:HyperLink>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">&nbsp;
                <asp:ImageButton ID="colFilter" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif" OnClick="colFilter_Click" />
                <asp:ImageButton ID="expFilter" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif" OnClick="expFilter_Click" Visible="false" />&nbsp;
                <asp:Literal ID="ltlFitlers" runat="server" Text="Save or edit a filter set"></asp:Literal>&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" /> &nbsp;</td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2"><np:NPSearch ID="search" runat="server" /></td>
        </tr>
        <tr>
            <td class="npadminsubheader">&nbsp;
                <asp:ImageButton ID="colResults" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif" OnClick="colResults_Click" Visible="false"/>
                <asp:ImageButton ID="expResults" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif" Visible="false" OnClick="expResults_Click" />&nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnSearch" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="gridContracts" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    AllowSorting="true" CellPadding="1" CssClass="npadmintable" OnPageIndexChanging="gridContracts_PageIndexChanging"
                    OnSorting="gridContracts_Sorting" OnRowDataBound="gridContracts_RowDataBound"
                    EmptyDataText="No Contracts Found">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:HyperLinkField DataTextField="SupportContractID" SortExpression="SupportContractID"
                            HeaderText="colID|ID" DataNavigateUrlFields="SupportContractID" DataNavigateUrlFormatString="ContractDetail.aspx?id={0}">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                        <asp:HyperLinkField DataTextField="ContractCode" SortExpression="SupportContractID"
                            HeaderText="colCode|Description" DataNavigateUrlFields="SupportContractID" DataNavigateUrlFormatString="ContractDetail.aspx?id={0}">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                        <asp:BoundField DataField="AccountID" SortExpression="AccountID" HeaderText="Customer|Customer"
                            DataFormatString="{0}" />
                        <asp:TemplateField HeaderText="colStart|Start" ItemStyle-HorizontalAlign="center" SortExpression="StartDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colEnd|End" ItemStyle-HorizontalAlign="center" SortExpression="EndDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlEndDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
