<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="taskadd.aspx.cs" Inherits="netpoint.admin.common.support.taskadd" %>
<%@ Register TagPrefix="np" TagName="NPTask" Src="~/admin/support/controls/task.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<asp:Label ID="sysError" runat="server" CssClass="NPWarning"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminheader">
        &nbsp;</td>
        <td class="npadminheader" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSave_Click" /></td>
    </tr>
    <tr>
        <td class="npadminsubheader" colspan="2">&nbsp;<asp:Literal ID="ltlHeader" runat="server" Text="New Task"></asp:Literal></td>
    </tr>
    <tr>
        <td colspan="2"><np:NPTask ID="task" runat="server" /></td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnSubjectRequired" runat="server" Text="The subject is required."></asp:Literal>
</asp:Content>