<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master"
    CodeBehind="project.aspx.cs" Inherits="netpoint.admin.common.support.project" %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="projectgeneral" Src="~/admin/support/controls/projectgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="tasks" Src="~/admin/support/controls/tasks.ascx" %>
<%@ Register TagPrefix="np" TagName="workorders" Src="~/admin/support/controls/workorders.ascx" %>
<%@ Register TagPrefix="np" TagName="projectusers" Src="~/admin/support/controls/projectusers.ascx" %>
<%@ Register TagPrefix="np" TagName="projecttickets" Src="~/admin/support/controls/projecttickets.ascx" %>
<%@ Register TagPrefix="np" TagName="projectpicker" Src="~/admin/common/controls/projectpicker.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<input type="hidden" id="hdnWarning" runat="server" value="This Project and all related data will be deleted." />
    <asp:Label ID="sysError" runat="Server" CssClass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" Visible="true" OnClick="btnDeleteGeneral_Click"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Save Project" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    OnClick="btnSave_Click" />
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsWorkOrders" Text="Service Calls">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTickets" Text="Tickets">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTasks" Text="Tasks">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsUsers" Text="Users">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsSubProjects" Text="Subprojects">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <np:projectgeneral ID="projectgeneral" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:workorders ID="workorders" runat="server" />
                <div class="npadminactionbar">
                    <asp:HyperLink ID="lnkAddWorkOrder" runat="server" ToolTip="Add Service Call" ImageUrl="~/assets/common/icons/add.gif"
                        NavigateUrl="~/admin/support/workorderadd.aspx"></asp:HyperLink>
                </div>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:projecttickets ID="projecttickets" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:tasks ID="tasks" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:projectusers ID="projectusers" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Subprojects"
                    OnRowDataBound="grid_RowDataBound">
                    <HeaderStyle CssClass="npadminsubheader" />
                    <EmptyDataRowStyle HorizontalAlign="center" Height="50" CssClass="npadminempty" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="SupportProjectID" DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}"
                            SortExpression="SupportProjectID" DataTextField="SupportProjectID">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                        <asp:HyperLinkField HeaderText="colName|Name" DataNavigateUrlFields="SupportProjectID"
                            DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}"
                            SortExpression="ProjectName" DataTextField="ProjectName" />
                        <asp:HyperLinkField HeaderText="Customer|Customer" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/accountinfo.aspx?accountid={0}"
                            SortExpression="AccountID" DataTextField="AccountID" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="colStart|Start" DataField="ActualStartDate" DataFormatString="{0: yyyy-MMM-dd}"
                            SortExpression="ActualStartDate" />
                        <asp:BoundField HeaderText="colEnd|End" DataField="ActualEndDate" DataFormatString="{0:yyyy-MMM-dd}"
                            SortExpression="ActualEndDate" />
                    </Columns>
                </asp:GridView>
                <div class="npadminactionbar">
                    <asp:Literal ID="ltlProjectID" runat="server" Text="Sub-Project ID"></asp:Literal>&nbsp;
                    <np:projectpicker ID="projPicker" runat="server" /> 
                    <asp:ImageButton ID="btnAddProject" runat="server" ToolTip="Add a Project" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAddProject_Click" />
                </div>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnProdjectDoesNotExist" runat="server" Text="The project does not exist."></asp:Literal>
    <asp:Literal ID="hdnParentIsItself" runat="server" Text="The parent project can not be itself."></asp:Literal>
</asp:Content>
