<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.ContractDetail" CodeBehind="ContractDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="contractcoverage" Src="~/admin/support/controls/contractcoverage.ascx" %>
<%@ Register TagPrefix="np" TagName="contractgeneral" Src="~/admin/support/controls/contractgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="contractparts" Src="~/admin/support/controls/contractparts.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDelete_Click" ToolTip="Delete" />&nbsp;
                <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSubmit_Click" ToolTip="Submit" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Coverage"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Associated Parts"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:contractgeneral ID="contractgeneral" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:contractcoverage ID="contractcoverage" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:contractparts ID="contractparts" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal runat="server" ID="hdnDeletionsPermanent" Visible="false" Text="Deletions are Permanent.\nContinue?" />
</asp:Content>
