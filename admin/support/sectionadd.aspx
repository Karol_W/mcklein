<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="sectionadd.aspx.cs" Inherits="netpoint.admin.support.sectionadd" %>

<%@ Register TagPrefix="np" TagName="sectiongeneral" Src="~/admin/support/controls/sectiongeneral.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save" OnClick="btnSave_Click" ></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:Literal ID="ltlSubHeader" runat="server" Text="New Support Queue" />
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <np:sectiongeneral ID="section" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hiddenslot" runat="server">
</asp:Content>
