<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.support.SolutionDetail" Codebehind="SolutionDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SolutionGeneral" Src="~/admin/support/controls/solutiongeneral.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.edit",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" style="width:15%;" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDeleteGeneral_Click" ToolTip="Delete" />&nbsp;
                <asp:ImageButton runat="server" ID="btnSaveClose" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveClose_Click" ToolTip="Save and Close" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts2" Text="More Info"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Symptoms"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Cause"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Resolution"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCustomers">
                <np:SolutionGeneral ID="npgeneral" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:TextBox ID="MoreInfo" CssClass="edit" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:TextBox ID="Symptoms" CssClass="edit" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:TextBox ID="Cause" CssClass="edit" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:TextBox ID="Resolution" CssClass="edit" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <asp:GridView runat="server" ID="gvRequirements" Width="100%" AllowPaging="True"
                    AllowSorting="True" EmptyDataText="No Records Found" AutoGenerateColumns="False">
	                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                    <RowStyle CssClass="npadminbody" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <Columns>
                        <asp:CommandField DeleteImageUrl="~/assets/common/icons/delete.gif" DeleteText="" ShowDeleteButton="True" />
                        <asp:BoundField DataField="RequirementType" HeaderText="Type|Type" />
                        <asp:BoundField DataField="Requirement" HeaderText="Requirement|Requirement" />
                    </Columns>
                </asp:GridView>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteSolution" runat="server" Text="This knowledge-base entry and all related data will be deleted" Visible="False"></asp:Literal>
</asp:Content>
