<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.supportsections" Codebehind="supportsections.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %> 



<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="npadmintable">
    <tr>
    <td class="npadminsubheader">&nbsp;</td>
    </tr>
    <tr>
    <td>
      <ComponentArt:TreeView id="tvList"
        DragAndDropEnabled="false" 
        NodeEditingEnabled="false"
        KeyboardEnabled="true"
        CssClass="TreeView" 
        NodeCssClass="TreeNode" 
        SelectedNodeCssClass="SelectedTreeNode" 
        HoverNodeCssClass="HoverTreeNode"
        NodeEditCssClass="NodeEdit"
        LineImageWidth="19" 
        LineImageHeight="20"
        DefaultImageWidth="16" 
        DefaultImageHeight="16"
        ItemSpacing="0" 
        ImagesBaseUrl="~/assets/common/tree/"
        NodeLabelPadding="3"
        ParentNodeImageUrl="folder.gif" 
        LeafNodeImageUrl="file.gif" 
        ShowLines="true" 
        LineImagesFolderUrl="~/assets/common/tree/lines/"
        EnableViewState="true"
        runat="server" >
      </ComponentArt:TreeView>
      
</td>
    </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
<asp:literal id="hdnAddSection" runat="server" text="Add a new queue" visible="False"></asp:literal>
<asp:literal id="hdnTopNodeName" runat="server" text="Support Queues" visible="False"></asp:literal>
</asp:Content>
