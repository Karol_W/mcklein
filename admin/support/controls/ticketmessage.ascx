<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ticketmessage.ascx.cs" Inherits="netpoint.admin.common.support.controls.ticketmessage" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCurrentStatus" runat="server" Text="Current Status"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysCurrentStatus" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubmitUser" runat="server" Text="Submitted By"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:HyperLink ID="sysLinkSubmitUser" runat="server" NavigateUrl="~/admin/common/accounts/userinfo.aspx?UserID="></asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCurrentAssign" runat="server" Text="Assigned To"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:HyperLink ID="sysLinkCurrentAssign" runat="server" NavigateUrl="~/admin/common/accounts/userinfo.aspx?UserID="></asp:HyperLink>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubmitDate" runat="server" Text="Submit Date"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysSubmitDate" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTicketType" runat="server" Text="Type"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysTicketType" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubmitEmail" runat="server" Text="Submit Email"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysSubmitEmail" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlOriginator" runat="server" Text="Originator"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysOriginator" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubmitIP" runat="server" Text="Submit IP"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysSubmitIP" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlProject" runat="server" Text="Project"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysProject" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountName" runat="server" Text="Account"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:HyperLink ID="sysLinkAccountName" runat="server" NavigateUrl="~/admin/common/accounts/accountinfo.aspx?accountid="></asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSection" runat="server" Text="Queue"></asp:Literal>
        </td>
        <td class="npadminbody">
             <asp:Literal ID="sysSection" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlServer" runat="server" Text="Theme"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysServer" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlResolveBy" runat="server" Text="Resolve By"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysResolveBy" runat="server"></asp:Literal>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPage" runat="server" Text="Page"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysPage" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
             <asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal>
        </td>
        <td class="npadminbody" colspan="3">
            <asp:Literal ID="sysSubject" runat="server"></asp:Literal>
        </td>
    </tr>
</table>
<table class="npadmintable">
    <tr>
        <td class="npadminsubheader">
             <asp:Literal ID="ltlThreadCreatedBy" runat="server" Text="Created By"></asp:Literal>
        </td>
        <td class="npadminsubheader">
             <asp:Literal ID="ltlThreadCreateDate" runat="server" Text="Create Date"></asp:Literal>
        </td>
        <td class="npadminsubheader">
             <asp:Literal ID="ltlThreadStatus" runat="server" Text="Status"></asp:Literal>
        </td>
        <td class="npadminsubheader">
             <asp:Literal ID="ltlThreadAssignedTo" runat="server" Text="Assigned To"></asp:Literal>
        </td>
    </tr>
    <asp:Repeater ID="rptThreads" runat="server">
        <ItemTemplate>
            <tr>
                <td class="npadminlabel">
                    <%#DataBinder.Eval(Container, "DataItem.AssignedByUserName") %>
                </td>
                <td class="npadminlabel">
                    <%#DataBinder.Eval(Container, "DataItem.CreateDate", "{0:dd-MMM-yyyy HH:mm}")%>
                </td>
                <td class="npadminlabel">
                    <%#DataBinder.Eval(Container, "DataItem.StatusName") %>
                </td>
                <td class="npadminlabel">
                    <%#DataBinder.Eval(Container, "DataItem.AssignedUserName") %>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Label runat="Server" id="lblSent" Text="Sent:"></asp:Label>
                    <%#DataBinder.Eval(Container, "DataItem.SentFlag") %>
                </td>
                <td class="npadminlabel">
                    <asp:Label runat="Server" id="lblInternal" Text="Is Internal:"></asp:Label>
                    <%#DataBinder.Eval(Container, "DataItem.InternalFlag")%>
                </td>
                <td class="npadminlabel">
                    <asp:Label runat="Server" id="lblReply" Text="Is a Reply:"></asp:Label>
                    <%#DataBinder.Eval(Container, "DataItem.ReplyFlag") %>
                </td>
                <td class="npadminlabel">
                    <asp:Checkbox runat="Server" id="cbSelected" />
                    <asp:Literal ID="ltlID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SupportTicketThreadID") %>' Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="npadminbody" colspan="4" style="padding-left:20px;padding-top:10px; padding-bottom:10px;">
                    <%#DataBinder.Eval(Container, "DataItem.MessageHTML") %>
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <tr>
        <td class="npadminheader" colspan="4" align="right">
            <asp:Button ID="btnSendAll" runat="server" Text="Mark Selected for Resend" OnClick="btnSendAll_Click" />
        </td>
    </tr>
</table>