<%@ Control Language="C#" AutoEventWireup="true" Codebehind="tasks.ascx.cs" Inherits="netpoint.admin.common.support.controls.tasks" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/npdatepicker.ascx" %>
<asp:Panel ID="pnlEnable" runat="server" > 
<asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
    AllowPaging="True" AllowSorting="True" OnPageIndexChanging="grid_PageIndexChanging"
    EmptyDataText="No Matches Found" OnSorting="grid_Sorting"
    OnRowDataBound="grid_RowDataBound" HeaderStyle-CssClass="npadminlabel" OnRowDeleting="grid_RowDeleting">
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/admin/support/task.aspx?taskid={0}"
             DataTextField="Subject" SortExpression="Subject" ItemStyle-HorizontalAlign="Left">
        </asp:HyperLinkField>
        <asp:BoundField HeaderText="colUser|User" DataField="UserID" SortExpression="UserID" />
        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="status">
            <ItemTemplate>
                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDue|Due" SortExpression="DueDate">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="lblDueDate" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ToolTip="Delete Task" ImageUrl="~/assets/common/icons/delete.gif" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle CssClass="npadminlabel" HorizontalAlign="Right" />
</asp:GridView>
<br />
<div class="npadminactionbar">
<table><tr><td valign="top">
    <np:PromptTextBox runat="server" id="ptbSubject" Prompt="new task"></np:PromptTextBox>&nbsp;
    </td><td valign="top">
    <asp:Literal runat="server" ID="ltlDue" Text="Due:"></asp:Literal>&nbsp;
    </td><td valign="top">
    <np:NPDatePicker ID="dtpDueDate" runat="server" Required="false" />&nbsp;
    </td><td valign="top">
    &nbsp;&nbsp;&nbsp;
    <asp:ImageButton ID="btnAddTask" runat="server" ToolTip="Add Task" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAddTask_Click" />
    </td></tr></table>
</div>
</asp:Panel>
<asp:Literal ID="hdnDelete" runat="server" Text="Deletions are permanent. Continue?" Visible="false"></asp:Literal>