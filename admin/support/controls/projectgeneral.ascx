<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="projectgeneral.ascx.cs" Inherits="netpoint.admin.common.support.controls.projectgeneral" %>

<%@ Register TagPrefix="np" TagName="NPAccount" src="~/admin/common/controls/accountpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/npdatepicker.ascx" %>
<%@ register TagPrefix="np" TagName="NPProjectPicker" Src="~/admin/common/controls/projectpicker.ascx" %>

<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlProjectName" runat="server" Text="Name"></asp:Literal>
        </td>
        <td colspan="3" class="npadminbody">
            <asp:TextBox ID="txtProjectName" runat="server" Columns="75"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal id="ltlParentProject" runat="server" Text="Parent Project"></asp:Literal>&nbsp;
            <asp:Image ID="sysParentProjectWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" />
        </td>
        <td colspan="3" class="npadminbody">
            <np:NPProjectPicker ID="projectpicker" runat="server" />&nbsp;
            <asp:HyperLink ID="sysParentID" runat="server" ></asp:HyperLink>                        
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal>
        </td>
        <td class="npadminbody">
            <np:NPAccount ID="acct" runat="server" />
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContact" runat="server" Text="User"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlContactUserID" runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal>
        </td>
        <td colspan="3" class="npadminbody">
            <asp:RadioButtonList ID="rblStatus" RepeatDirection="Horizontal" runat="server" CssClass="npadminbody">
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal>            
        </td>
        <td  class="npadminbody">
            <asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList>        
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubType" runat="server" Text="SubType"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlSubType" runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlActual" runat="server" Text="Actual"></asp:Literal>
        </td>
        <td class="npadminbody">
            <table cellpadding="1" cellspacing="0" border="0" class="npadminbody">
                <tr>
                    <td>
                        <asp:Literal ID="ltlStartDate" runat="server" Text="Start Date"></asp:Literal>
                    </td>
                    <td>
                        <np:NPDatePicker ID="dtpActualStartDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="ltlEndDate" runat="server" Text="End Date"></asp:Literal>
                    </td>
                    <td>
                        <np:NPDatePicker ID="dptActualEndDate" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlQuote" runat="server" Text="Quote"></asp:Literal>
        </td>
        <td class="npadminbody">
            <table cellpadding="1" cellspacing="0" border="0" class="npadminbody">
                <tr>
                    <td>
                        <asp:Literal ID="ltlQuoteStartDate" runat="server" Text="Start Date"></asp:Literal>&nbsp;
                    </td>
                    <td>
                        <np:NPDatePicker ID="dtpQuoteStartDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="ltlQuoteEndDate" runat="server" Text="End Date"></asp:Literal>
                    </td>
                    <td>
                        <np:NPDatePicker ID="dtpQuoteEndDate" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>   
    <tr>
        <td class="npadminlabel" runat="server">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal>
        </td>
        <td  colspan="3" class="npadminbody">
            <asp:TextBox ID="txtDescription" runat="server" Columns="55" TextMode="MultiLine" Rows="4"></asp:TextBox>
        </td>
    </tr>
</table>
