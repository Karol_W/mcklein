<%@ Control Language="C#" AutoEventWireup="true" Codebehind="sectionsections.ascx.cs"
    Inherits="netpoint.admin.support.controls.sectionsections" %>
<asp:gridview ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Subqueues Created" OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:templatefield>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SectionId") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:templatefield>
        <asp:templatefield HeaderText="colName|Name">
            <ItemTemplate>
                <asp:Literal ID="ltlSection" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SectionName") %>'>
                </asp:Literal>
            </ItemTemplate>
        </asp:templatefield>
        <asp:templatefield HeaderText="colActive|Active">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Image ID="imgActive" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                    Visible='<%# DataBinder.Eval(Container.DataItem, "ActiveFlag") %>'></asp:Image>
            </ItemTemplate>
        </asp:templatefield>
        <asp:templatefield HeaderText="colSort|Sort">
            <ItemStyle HorizontalAlign="center"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlSort" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SortCode") %>'>
                </asp:Literal>
            </ItemTemplate>
        </asp:templatefield>
        <asp:templatefield>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"></asp:HyperLink>
            </ItemTemplate>
        </asp:templatefield>
    </Columns>
</asp:gridview>
<div style="text-align: right">
    <asp:HyperLink ID="lnkAddQueue" runat="server" ImageUrl="~/assets/common/icons/add.gif"
        ToolTip="Add Subqueue" ></asp:HyperLink>
</div>
    
    <asp:Literal ID="hdnRemoveSection" runat="server" Text="Remove Subsection" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEditSection" runat="server" Text="Edit Section" Visible="False"></asp:Literal>