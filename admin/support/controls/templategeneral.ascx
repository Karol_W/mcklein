<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="templategeneral.ascx.cs" Inherits="netpoint.admin.support.controls.templategeneral" %>


 <asp:Label ID="sysError" runat="server"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTemplateName" runat="server" Text="Template Name"></asp:Literal>
        </td>
        <td>
            <asp:TextBox ID="txtTemplateName" runat="server"></asp:TextBox>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContractType" runat="server" Text="Contract Type"></asp:Literal>
        </td>
        <td>
            <asp:DropDownList ID="ddlContractType" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:TextBox ID="txtDescription" runat="server" Columns="40" TextMode="multiLine"
                Rows="3"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDuration" runat="server" Text="Duration"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:TextBox ID="txtDuration" runat="server" Columns="7"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlDurationInterval" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlRenewDays" runat="server" Text="Renewal Reminder Days"></asp:Literal></td>
        <td colspan="3">
            <asp:TextBox ID="txtRenewDays" runat="server" Columns="7"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlResponseHours" runat="server" Text="Response Hours"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtResponseHours" runat="server" Columns="7"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlResolutionHours" runat="server" Text="Resolution Hours"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtResolutionHours" Columns="7" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlChargeable" runat="server" Text="Chargeable"></asp:Literal></td>
        <td class="npadminbody" colspan="3">
            <asp:CheckBox ID="chkLabor" runat="server" Text="Labor" TextAlign="left" />&nbsp;
            <asp:CheckBox ID="chkParts" runat="server" Text="Items" TextAlign="left" />&nbsp;
            <asp:CheckBox ID="chkTravel" runat="server" Text="Travel" TextAlign="left" />&nbsp;
            <asp:CheckBox ID="chkHoliday" runat="server" Text="Holidays" TextAlign="left" />
        </td>
    </tr>
</table>

<asp:Literal ID="hdnMustBeNumber" runat="server" Text="Must be a number." Visible="false"></asp:Literal>