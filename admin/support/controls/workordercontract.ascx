<%@ Control Language="C#" AutoEventWireup="true" Inherits="netpoint.admin.common.support.controls.workordercontract"
    Codebehind="workordercontract.ascx.cs" %>
<asp:Panel ID="pnlContract" runat="server">
    <table class="npadminbody" width="100%">
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlContractID" runat="server" Text="Contract"></asp:Literal></td>
            <td colspan="3">
                <asp:DropDownList ID="ddlContract" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged">
                </asp:DropDownList>&nbsp;
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="sysContractDetails" Visible="false">
        <tr>
            <td id="Td1" class="npadminlabel" runat="server">
                <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
            <td colspan="3">
                <asp:Literal ID="sysDescription" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td id="Td2" class="npadminlabel" runat="server">
                <asp:Literal ID="ltlCoveragePeriod" runat="server" Text="Coverage Period"></asp:Literal></td>
            <td>
                <asp:Literal ID="sysCoveragePeriod" runat="server"></asp:Literal></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlResolution" runat="server" Text="Response/Resolution Hours"></asp:Literal></td>
            <td>
                <asp:Literal ID="sysHours" runat="server"></asp:Literal></td>
        </tr>
        </asp:PlaceHolder>
    </table>
</asp:Panel>
