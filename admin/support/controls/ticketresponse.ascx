<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ticketresponse.ascx.cs"
    Inherits="netpoint.admin.common.support.controls.ticketresponse" %>
<table class="npadmintable">
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlFromName" runat="server" Text="From:"></asp:Literal>
        </td>
        <td class="npadminbody">
            <b>
                <asp:Label ID="sysFromName" runat="server"></asp:Label>
                &lt;<asp:Label ID="sysFromEmail" runat="server"></asp:Label>&gt;</b>
        </td>
        <td align="right" class="npadminbody">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlEmailAdress" runat="server" Text="To:"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList runat="server" ID="ddlSendReponseTo">
            </asp:DropDownList>
        </td>
        <td align="right" class="npadminbody">
            <asp:ImageButton runat="server" ID="btnSubmit" ImageUrl="~/assets/common/icons/send.gif" OnClick="btnSubmit_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="3" class="npadminbody" align="center">
            <asp:TextBox runat="server" ID="tbMessage" TextMode="multiLine" Columns="70" Rows="15"></asp:TextBox>
        </td>
    </tr>
</table>

<asp:Literal runat="Server" Visible="false" ID="hdnReply" Text="RE: "></asp:Literal>
<asp:Literal runat="Server" Visible="false" ID="hdnInternalOnly" Text="Internal Only"></asp:Literal>
<asp:literal id="hdnStatusChanged" runat="server" text="Ticket status changed" visible="False"></asp:literal>