<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ticketgeneral.ascx.cs" Inherits="netpoint.admin.common.support.controls.ticketgeneral" %>
<%@ Register Src="../../../common/controls/npdatepicker.ascx" TagName="npdatepicker" TagPrefix="uc1" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlQueue" runat="server" Text="Queue"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlSectionID" runat="server">
            </asp:DropDownList>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlTicketStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketStatus_SelectedIndexChanged">
            </asp:DropDownList>&nbsp;
            <asp:DropDownList ID="ddlTicketSubStatus" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlProject" runat="server" Text="Project"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlProject" runat="server">
            </asp:DropDownList>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAssignedTo" runat="server" Text="Assigned To"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlAssignedTo" runat="server">
            </asp:DropDownList>
            <asp:Literal ID="sysAssignedTo" runat="server"></asp:Literal>
            <asp:ImageButton ID="lnkSteal" runat="server" ImageUrl="~/assets/common/icons/steal.gif"
                ToolTip="Steal Ticket" OnClick="lnkSteal_Click"></asp:ImageButton>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDepartment" runat="server" Text="Type"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlTicketType" runat="server">
            </asp:DropDownList>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPrioritySequence" runat="server" Text="Priority / Sequence"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="nbPriority" runat="server" Columns="3"></asp:TextBox>&nbsp;/&nbsp;
            <asp:TextBox ID="nbSequence" runat="server" Columns="3"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
        <asp:Literal ID="ltlResolveDate" runat="server" Text="Resolve Date"></asp:Literal>
        </td>
        <td class="npadminbody" colspan="3">
            <uc1:npdatepicker ID="dpResolveDate" runat="server" />
        </td>
    </tr>
</table>
<br />
<table class="npadmintable">
    <tr>
        <td class="npadminsubheader">
            <asp:Literal ID="ltlMessage" runat="server" Text="Enter a message describing your changes."></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminbody">
            <asp:CheckBox ID="chkReply" runat="server" Text="Send Reply"></asp:CheckBox>&nbsp;
            <asp:CheckBox ID="chkInternal" runat="server" Text="This is an internal message"></asp:CheckBox>
            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Width="100%" Height="150"></asp:TextBox>
        </td>
    </tr>
</table>
<asp:Literal ID="hdnNA" runat="server" Text="N/A" Visible="False"></asp:Literal>
<asp:Literal ID="hdnError" runat="server" Text="ERROR" Visible="False"></asp:Literal>