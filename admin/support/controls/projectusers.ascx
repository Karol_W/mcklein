<%@ Control Language="C#" AutoEventWireup="true" Codebehind="projectusers.ascx.cs"
    Inherits="netpoint.admin.common.support.controls.projectusers" %>

<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" DataKeyNames="UserID"
    AllowPaging="True" AllowSorting="True" EmptyDataText="No Users" OnRowDataBound="grid_RowDataBound" OnRowDeleting="gvUsers_RowDeleting">
    <EmptyDataRowStyle HorizontalAlign="center" Height="50" CssClass="npadminempty" />
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:CommandField DeleteImageUrl="~/assets/common/icons/remove.gif" ButtonType="Image" ShowDeleteButton="true" />
        <asp:BoundField HeaderText="colUser|User" DataField="UserID" SortExpression="UserID" />
    </Columns>
    <PagerStyle CssClass="npadminlabel" HorizontalAlign="Right" />
</asp:GridView>
<div class="npadminactionbar">
    <asp:DropDownList runat="Server" ID="ddlUsers"></asp:DropDownList>
    &nbsp;
    <asp:ImageButton ID="lnkAddUser" runat="server" ToolTip="Add User" ImageUrl="~/assets/common/icons/add.gif" OnClick="lnkAddUser_Click">
    </asp:ImageButton>
</div>
