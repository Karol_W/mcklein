<%@ Control Language="C#" AutoEventWireup="true" Codebehind="contractgeneral.ascx.cs" Inherits="netpoint.admin.common.support.controls.contractgeneral" %>      
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<div class="npwarning"><asp:Literal ID="sysError" runat="server"></asp:Literal></div>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContractName" runat="server" Text="Description"></asp:Literal>
       </td>
        <td colspan="3" class="npadminbody">
            <asp:TextBox ID="txtContractCode" runat="server" Columns="55"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal>
        </td>
        <td class="npadminbody">
            <np:AccountPicker id="acctPicker" runat="server"></np:AccountPicker>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAssignedTo" runat="server" Text="Assigned To"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlAssignedTo" runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTemplate" runat="server" Text="Template"></asp:Literal>&nbsp;
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlTemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlServiceType" runat="server" Text="Service Type"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlServiceType" runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContractType" runat="server" Text="Contract Type"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlContractType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlContractType_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContractStatus" runat="server" Text="Contract Status"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlContractStatus" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlStartDate" runat="server" Text="Start Date"></asp:Literal>
        </td>
        <td  class="npadminbody" style="white-space:nowrap;">
            <np:NPDatePicker id="calStart" runat="server"></np:NPDatePicker>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEndDate" runat="server" Text="End Date"></asp:Literal>
        </td>
        <td  class="npadminbody" style="white-space:nowrap;">
            <np:NPDatePicker id="calEnd" runat="server"></np:NPDatePicker>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTerminationDate" runat="server" Text="Termination Date"></asp:Literal>
        </td>
        <td  class="npadminbody" style="white-space:nowrap;">
            <np:NPDatePicker id="calTerm" runat="server"></np:NPDatePicker>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSortOrder" runat="server" Text="SortOrder"></asp:Literal>
            &nbsp;</td>
        <td class="npadminbody">
            <asp:TextBox ID="txtSortOrder" runat="server" Columns="4"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlResponseHours" runat="server" Text="Response Hours"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:TextBox ID="txtResponseHours" runat="server" Columns="4"></asp:TextBox>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlResolutionHours" runat="server" Text="Resolution Hours"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:TextBox ID="txtResolutionHours" runat="server" Columns="4"></asp:TextBox>
        </td>
    </tr>
<%--    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal>
        </td>
        <td colspan="3" class="npadminbody">
            <asp:TextBox ID="txtDescription" runat="server" Columns="40"></asp:TextBox>
        </td>
    </tr>--%>
    <tr>
        <td class="npadminlabel" valign="top">
            <asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal>
        </td>
        <td colspan="3" class="npadminbody">
            <asp:TextBox ID="txtNotes" runat="server" Columns="50" Rows="5" TextMode="multiline"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:literal ID="ltlFlags" runat="server" Text="Flags"></asp:literal>
        </td>
        <td class="npadminbody" colspan="3">&nbsp;
            <asp:CheckBox ID="chkParts" runat="server" Text="Items" />&nbsp;
            <asp:CheckBox ID="chkLabor" runat="server" Text="Labor" />&nbsp;
            <asp:CheckBox ID="chkTravel" runat="server" Text="Travel" />&nbsp;
            <asp:CheckBox ID="chkHolidays" runat="server" Text="Holidays" />
        </td>
    </tr>
</table>
<asp:Literal ID="errContractType" runat="server" Visible="false" Text="Contract Type cannot be empty."></asp:Literal>
