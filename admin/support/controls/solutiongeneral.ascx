<%@ Control Language="C#" AutoEventWireup="true" Codebehind="solutiongeneral.ascx.cs"
    Inherits="netpoint.admin.common.support.controls.solutiongeneral" %>
<table class="npadminbody">
    <tr>
        <td class="npadminlabel">
            <asp:Literal runat="server" ID="ltlSolutionName" Text="Knowledge-Base Entry Name:"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox runat="server" ID="tbSolutionName" Columns="70" MaxLength="100"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlDescription" Text="Description"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox runat="server" ID="tbDescription" Columns="50" Rows="7" TextMode="multiLine" MaxLength="255"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlSectionID" Text="Section"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlSectionID"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal runat="server" ID="ltSolutionlType" Text="Type"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlSolutionType"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlSolutionStatus" Text="Status"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlSolutionStatus"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal runat="server" ID="ltlFlags" Text="Flags"></asp:Literal></td>
        <td class="npadminbody">
            <asp:CheckBox runat="server" ID="cbActiveFlag" Text="Active" Checked="true" />&nbsp;
            <asp:CheckBox runat="server" ID="cbInternalFlag" Text="Internal" />&nbsp;
            <asp:CheckBox runat="server" ID="cbFeaturedFlag" Text="Featured" /></td>
    </tr>
</table>
