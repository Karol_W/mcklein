<%@ Control Language="c#" Inherits="netpoint.support.controls.workorderevents" Codebehind="workorderevents.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<style type="text/css">
    .iFrameCss
    {
        overflow:auto;
        width:640px;
        height:550px;
        padding:0px;
        margin:0px;
    }
</style>
<asp:Panel ID="pnlEnable" runat="server" > 
  <script  language="javascript" type="text/javascript" >
      function editActivity(ActivityId) {
          dlgActivity.set_contentUrl("~/admin/focus/Activity.aspx?activityid=" + ActivityId + "&modal=Y");
          dlgActivity.show();
      }

      function newActivity() {
          dlgActivity.set_contentUrl("~/admin/focus/Activity.aspx?parenttype=V&parentid=" + GetParentID() + "&modal=Y");
          dlgActivity.show();
      }

      function dlgActivity_Close(sender, Eventargs) {
          window.location.href = window.location.pathname + "?workid=" + GetParentID();
      }

      function GetParentID() {
          var url = window.location.href;
          var qparts = url.split("?");
          var query = qparts[1];
          var vars = query.split("&");
          var value = "";
          for (i = 0; i < vars.length; i++) {
              var parts = vars[i].split("=");
              if (parts[0] == "workid") {
                  value = parts[1];
                  break;
              }

          }

          return value;
      }
    </script>
<asp:GridView 
    id="EventsGrid" 
    runat="server" 
    CssClass="npadmintable" 
	AutoGenerateColumns="False" 
	Width="100%"
    EmptyDataText="No Activities Found" 
    OnRowDataBound="EventsGrid_RowDataBound">
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<Columns>
		<asp:TemplateField>
		    <ItemStyle HorizontalAlign="center" />
			<ItemTemplate>
				<asp:HyperLink runat="server" ID="lnkID"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="StartDate" SortExpression="StartDate" HeaderText="colDate|Date" DataFormatString="{0:ddd yyyy-MMM-dd HH:mm}" />
		<asp:BoundField DataField="ContactID" SortExpression="ContactID" HeaderText="colContact|Contact" />
		<asp:BoundField DataField="AssignedUserID" SortExpression="AssignedUserID" HeaderText="colOwner|Owner" />
		<asp:TemplateField HeaderText="colName|Name" SortExpression="Remarks">
			<ItemTemplate>
				<a href="javascript:editActivity('<%# DataBinder.Eval(Container.DataItem, "ActivityID") %>');"><%# DataBinder.Eval(Container.DataItem, "Remarks") %></a>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
		    <ItemStyle HorizontalAlign="center" />
			<ItemTemplate>
				<asp:HyperLink runat="server" ID="lnkStatus" ImageUrl="~/assets/common/icons/checked.gif" Visible="False" tooltip="Closed"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>   
<div class="npadminactionbar">
    <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" Text="New" onclick="javascript:newActivity();"></asp:HyperLink>
</div>
</asp:Panel>
<ComponentArt:Dialog 
    Modal="true"
    HeaderCssClass = "Dialog_Header"
    ContentCssClass="Dialog_Content" 
    ModalMaskCssClass="ModalBackground" 
    ContentUrl="about:blank"   
    runat="server"  
    ID="dlgActivity" 
    Alignment="MiddleCentre" 
    Height="550" 
    Width="650" 
    IFrameCssClass="iFrameCss" 
    Enabled="true">
    <Header>
        <table width="99%">
            <tr>
                <td align="left" style="width:50%">
                    <asp:Literal ID="Literal1" runat="server" Text="Activity" />  
                </td>
                <td align="right" style="width:50%">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Close Without Saving" ImageUrl="~/assets/common/admin/close.gif" OnClientClick="dlgActivity.close();" />       
                </td>
            </tr>
        </table>
    </Header>
    <ClientEvents>
        <OnClose EventHandler="dlgActivity_Close" />
    </ClientEvents>
</ComponentArt:Dialog>
<asp:Literal Runat="server" Visible="False" ID="hdnCall" Text="Call" />
<asp:Literal Runat="server" Visible="False" ID="hdnAppointment" Text="Appointment" />
<asp:Literal Runat="server" Visible="False" ID="hdnNote" Text="Note" />
<asp:Literal Runat="server" Visible="False" ID="hdnTask" Text="Task" />
