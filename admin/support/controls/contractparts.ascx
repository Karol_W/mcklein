<%@ Control Language="C#" AutoEventWireup="true" Codebehind="contractparts.ascx.cs"
    Inherits="netpoint.admin.common.support.controls.contractparts" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/partPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<div class="npadminactionbar" id="divAdd" runat="server" visible="false">
    <table class="npadminbody">
        <tr>
            <td><asp:Label runat="server" ID="lblAddPart" CssClass="npadminbody" Text="Add Item" /></td>
 
            <td></td>
            <td><asp:Label runat="server" ID="lblEffectiveDays" CssClass="npadminbody" Text="Effective Days:" /></td>
            <td><asp:TextBox runat="server" ID="txtEffectiveDays" Columns="4" Text="0" /></td>
        </tr>
    </table>   
</div>
<asp:GridView ID="gridContractParts" runat="server" AllowPaging="True" AutoGenerateColumns="False"
     CssClass="npadmintable" OnRowCommand="gridContractParts_RowCommand" 
    OnRowDataBound="gridContractParts_RowDataBound" EmptyDataText="No Associated Items" OnPageIndexChanging="gridContractParts_PageIndexChanging">
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton runat="server" ID="btnRemovePart" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:Literal ID="ltlBlank" runat="server"></asp:Literal>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ItemNo|Item No." SortExpression="PartNo">
            <ItemStyle Wrap="false" />
            <ItemTemplate>
                <asp:HyperLink ID="lnkPartNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartNo") %>'></asp:HyperLink>
            </ItemTemplate>
            <EditItemTemplate>
                <np:PartPicker ID="partPicker" runat="server"></np:PartPicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCategory|Category" SortExpression="CategoryCode">
            <ItemTemplate>
                <asp:Literal ID="ltlCategoryCode" runat="server"></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlCategoryCode" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colSerialNo|Serial No.">
            <ItemStyle Wrap="false" />
            <ItemTemplate>
                <asp:Literal ID="ltlSerialNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNumber") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtSerialNo" runat="server" Width="150px"></asp:TextBox>
                <asp:Image ID="imgWarningSN" runat="server" ImageUrl="~/assets/common/icons/warning.gif" visible="false" />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField SortExpression="EffectiveDays" HeaderText="colEffectiveDays|Effective Days">
            <ItemTemplate>
                <asp:Literal ID="ltlDays" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EffectiveDays") %>'></asp:Literal>                
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtEffectiveDays" runat="server" Width="45"></asp:TextBox>
                <asp:Image ID="imgWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" visible="false" />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEffectiveUntil|Effective Until" SortExpression="EffectiveDays">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Label ID="lblEffectiveDate" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colRegistration|Registration" SortExpression="PartNo">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkRegistration" runat="server"></asp:HyperLink>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancelthis" />
                <asp:ImageButton ID="imgSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="addthis" />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="Bottom" Mode="numeric" />
    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel" />
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton runat="server" ID="btnAddPart" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAddPart_Click" ToolTip="Add Item" /></td>
    </tr>
</table>
<asp:Label ID="sysError" runat="server" CssClass="npwarning" ></asp:Label>
<asp:Literal ID="ltlPart" runat="server" Visible="false" Text="Add Item"></asp:Literal>
<asp:Literal ID="ltlCategory" runat="server" Visible="false" Text="Add Category"></asp:Literal>
<asp:Literal ID="hdnRequired" runat="server" Visible="false" Text="Required"></asp:Literal>
<asp:Literal ID="hdnPartDoesNotExist" runat="server" Visible="false" Text="This item does not exist."></asp:Literal>
<asp:Literal ID="hdnPartAlreadyAdded" runat="server" Visible="false" Text="The item has already been added to the list."></asp:Literal>
<asp:Literal ID="hdnCategoryAlreadyAdded" runat="server" Visible="false" Text="The item group has already been added to the list."></asp:Literal>

