<%@ Control Language="C#" AutoEventWireup="true" Codebehind="sectiongeneral.ascx.cs"
    Inherits="netpoint.admin.support.controls.sectiongeneral" %>
    
    
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSectionName" runat="server" Text="Section Name"></asp:Literal>
            <asp:RequiredFieldValidator ID="rfvNewsletterName" runat="server" ErrorMessage="RequiredFieldValidator"
                ControlToValidate="txtSectionName">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
            </asp:RequiredFieldValidator>
        </td>
        <td colspan="3">
            <asp:TextBox ID="txtSectionName" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
        </td>
    </tr>
        <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSectionDescription" runat="server" Text="Description"></asp:Literal>
            <asp:RequiredFieldValidator ID="rfvSectionDescription" runat="server" ErrorMessage="RequiredFieldValidator"
                ControlToValidate="txtSectionDescription">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
            </asp:RequiredFieldValidator>
        </td>
        <td colspan="3">
            <asp:TextBox ID="txtSectionDescription" runat="server" Columns="75"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlParent" runat="server" Text="Parent"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:DropDownList ID="ddlParentID" runat="server">
            </asp:DropDownList>
            <asp:Literal ID="sysParent" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPageDesc" runat="server" Text="Static Page"></asp:Literal>
        </td>
        <td colspan="3">
            <asp:DropDownList ID="ddlPageCode" runat="server">
            </asp:DropDownList>
            <asp:ImageButton ID="btnNewPage" OnClick="btnNewPage_Click" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New Page">
            </asp:ImageButton>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlFinishURL" runat="server" Text="Finish URL"></asp:Literal>
        </td>
        <td class="npadminbody" colspan="3">
            <asp:TextBox ID="txtFinishURL" runat="server" Columns="75"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSectionManager" runat="server" Text="Manager"></asp:Literal></td>
        <td>
            <asp:DropDownList ID="ddlManager" runat="server">
            </asp:DropDownList></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlManagerEmail" runat="server" Text="Email"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtManagerEmail" runat="server" Columns="53"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                ControlToValidate="txtSortOrder">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
            </asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:TextBox ID="txtSortOrder" runat="server" Columns="5">0</asp:TextBox>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlActive" runat="server" Text="Active"></asp:Literal>
        </td>
        <td>
            <asp:CheckBox ID="sysActiveFlag" runat="server" Checked="True"></asp:CheckBox>
        </td>
    </tr>
</table>

<asp:Literal ID="errCantBeParent" runat="server" Text="You cannot select a section as its own parent"
        Visible="False"></asp:Literal>
