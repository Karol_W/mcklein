<%@ Control Language="C#" AutoEventWireup="true" Inherits="netpoint.admin.common.support.controls.contractcoverage" Codebehind="contractcoverage.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="TimePicker" Src="~/common/controls/NPTimePicker.ascx" %>
<asp:Label runat="server" ID="sysError" CssClass="npwarning"></asp:Label>
<asp:GridView 
    ID="grid" 
    runat="server" 
    AutoGenerateColumns="False" 
    CssClass="npadmintable" 
    OnRowDataBound="grid_RowDataBound"
    OnRowDeleting="grid_RowDeleting" 
    DataKeyNames="CoverageID" 
    EmptyDataText="No Coverage Found"
    >
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDay|Day">
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:Literal ID="ltlDay" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStartTime|Start Time">
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:Literal ID="ltlStartTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StartTime") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEndTime|End Time">
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:Literal ID="ltlEndTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EndTime") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:Label runat="server" ID="lblDay" CssClass="npadminbody" Text="Day"></asp:Label>
    <asp:DropDownList ID="ddlNewDay" runat="server"></asp:DropDownList>
    <asp:Label runat="server" ID="lblStart" CssClass="npadminbody" Text="Start"></asp:Label>
    <np:TimePicker ID="tpkNewStartTime" runat="server"></np:TimePicker>
    <asp:Label runat="server" ID="lblEnd" CssClass="npadminbody" Text="End"></asp:Label>
    <np:TimePicker ID="tpkNewEndTime" runat="server"></np:TimePicker>
    <asp:ImageButton ID="btnAddCoverage" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add New Coverage" OnClick="btnAddCoverage_Click" />
</div>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete" Visible="false"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCancel" runat="server" Text="Cancal" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="false"></asp:Literal>
<asp:Literal ID="hdnBadTime" runat="server" Text="Start time must preceed end time." Visible="false"></asp:Literal>
<asp:Literal ID="hdnDoubleDay" runat="server" Text="There is already an entry for that day" Visible="false"></asp:Literal>
