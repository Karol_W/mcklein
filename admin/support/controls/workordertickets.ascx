<%@ Control Language="c#" Inherits="netpoint.support.controls.workordertickets" Codebehind="workordertickets.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="ticketpicker" Src="~/admin/common/controls/ticketpicker.ascx" %>
<asp:Panel ID="pnlEnable" runat="server" > 
<asp:GridView ID="gvTickets" ShowHeader="True" runat="server" AllowSorting="True"
    CssClass="npadmintable" AutoGenerateColumns="False" EmptyDataText="No Associated Tickets Found">
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <Columns>
        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="TicketID" DataNavigateUrlFormatString="~/admin/support/ticketadmin.aspx?ticketid={0}"
            DataTextField="TicketID" SortExpression="TicketID" ItemStyle-HorizontalAlign="center">
            <ItemStyle VerticalAlign="top" />
        </asp:HyperLinkField>
        <asp:TemplateField HeaderText="colSubject|Subject" SortExpression="Subject">
            <ItemTemplate>
                <asp:Label ID="TicketType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketType") %>' />
                <b>
                    <asp:HyperLink runat="server" ID="lnkTicket" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.TicketID","~/admin/support/ticketadmin.aspx?ticketid={0}") %>'
                        Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>' /></b>
                <br />
                <asp:Label runat="server" ID="lbl" Text='<%# DataBinder.Eval(Container, "DataItem.UserEmailAddress") %>'></asp:Label>
                -
                <asp:Label runat="server" ID="Label2" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'></asp:Label>
                <br />
                <br />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colSubmitDate|Submit Date" SortExpression="SubmitDate, AssignedTo">
            <ItemStyle Wrap="false" HorizontalAlign="center" VerticalAlign="Top" />
            <ItemTemplate>
                <asp:Label ID="lblSubmitDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate","{0:yyyy-MMM-dd}") %>' />
                <br />
                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedUserID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPriority|Priority" SortExpression="Priority,Sequence">
            <ItemStyle HorizontalAlign="center" VerticalAlign="top" />
            <ItemTemplate>
                <asp:Label ID="lblPriority" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Priority") %>' />
                /
                <asp:Label ID="lblSequence" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Sequence") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:Label runat="Server" ID="lblTicketID" Text="TicketID"></asp:Label>
    <np:ticketpicker runat="server" ID="ticketpicker" />
    <asp:ImageButton runat="server" ID="btnAddTicket" ImageUrl="~/assets/common/icons/link.gif"
        OnClick="btnAddTicket_Click" ToolTip="Add Support Ticket" />
</div>
</asp:Panel>
