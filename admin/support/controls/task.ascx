<%@ Control Language="C#" AutoEventWireup="true" Codebehind="task.ascx.cs" Inherits="netpoint.admin.common.support.controls.task" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/npdatepicker.ascx" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal>
            <asp:Image ID="sysSubject" runat="server" ImageUrl="~/assets/common/icons/warning.gif" visible="false" />
        </td>
        <td class="npadminbody">
            <asp:TextBox ID="txtSubject" runat="server" Columns="80"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:TextBox ID="txtDescription" runat="server" TextMode="multiLine" Columns="60"
                Rows="5"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlUserID" runat="server" Text="User"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlUserID" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCreateDate" runat="server" Text="Create Date"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysCreateDate" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDueDate" runat="server" Text="Due Date"></asp:Literal>
        </td>
        <td class="npadminbody">
            <np:NPDatePicker ID="dtpDueDate" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlStartDate" runat="server" Text="Start Date"></asp:Literal>
        </td>
        <td class="npadminbody">
            <np:NPDatePicker ID="dtpStartDate" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlCompleteDate" runat="server" Text="Complete Date"></asp:Literal>
        </td>
        <td class="npadminbody">
            <np:NPDatePicker ID="dtpCompleteDate" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblStatus" runat="server" CssClass="npadminbody" RepeatDirection="vertical">
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPriority" runat="server" Text="Priority"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblPriority" runat="server" CssClass="npadminbody" RepeatDirection="vertical">
            </asp:RadioButtonList>
        </td>
    </tr>
</table>
