<%@ Control Language="C#" AutoEventWireup="true" Inherits="netpoint.admin.common.support.controls.workordergeneral" Codebehind="workordergeneral.ascx.cs" %>
<%@ Register Src="~/common/controls/npdatepicker.ascx" TagName="DatePicker" TagPrefix="np" %>
<%@ Register Src="~/admin/common/controls/accountpicker.ascx" TagName="accountpicker" TagPrefix="np" %>
<%@ Register Src="~/admin/support/controls/workordercontract.ascx" TagName="workordercontract" TagPrefix="np" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<table border="0" cellpadding="1" cellspacing="0" width="100%" class="npadminbody">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
        <td><asp:DropDownList ID="ddlStatus" runat="server" OnSelectedIndexChanged="StatusChanged" AutoPostBack="true" ></asp:DropDownList></td>
    </tr>
    <asp:Panel ID="pnlEnable" runat="server" > 
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlWorkName" runat="server" Text="Subject"></asp:Literal></td>
        <td><asp:TextBox ID="txtWorkName" runat="server" Columns="60"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtWorkName" ErrorMessage="RequiredFieldValidator">
            <asp:Image ID="imgWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlPriority" runat="server" Text="Priority"></asp:Literal></td>
        <td><asp:RadioButtonList ID="rblPriority" runat="server" CssClass="npadminbody" RepeatDirection="Horizontal"></asp:RadioButtonList></td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlStarted" runat="server" Text="Started"></asp:Literal></td>
                                <td><np:DatePicker ID="dpStarted" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlClosed" runat="server" Text="Closed"></asp:Literal></td>
                                <td><np:DatePicker ID="dpClosed" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlAssignee" runat="server" Text="Assignee"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlEmpl" runat="server"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlQueue" runat="server" Text="Queue"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlQueue" runat="server"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlProject" runat="server" Text="Project"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlProject" runat="server"></asp:DropDownList></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlAccount" runat="server" Text="Customer Account"></asp:Literal></td>
                                <td><np:accountpicker ID="acct" runat="server" Required="true" PostBack="true" /></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlContact" runat="server" Text="Customer User"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlPeople" runat="server" Width="225px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlPhone" runat="server" Text="User Phone"></asp:Literal></td>
                                <td><asp:TextBox ID="txtContactPhone" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlItem" runat="server" Text="Item No."></asp:Literal></td>
                                <td colspan="3">
                                    <np:PartPicker ID="ppPartNo" runat="server" PostBack="true"></np:PartPicker>
                                    <asp:HyperLink ID="sysPartNo" runat="server"></asp:HyperLink>
                                </td>                           
                            </tr>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlSerial" runat="server" Text="Serial No."></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlSerial" runat="server" Width="150px"></asp:DropDownList></td>
                            </tr>                         
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="npadminlabel"><asp:Literal ID="ltlWorkType" runat="server" Text="Work Type"></asp:Literal></td>
                                <td class="npadminbody" colspan="3"><table border="0" width="100%" class="npadminbody">
                            <tr>
                                <td><asp:Literal ID="ltlTypeA" runat="server" Text="Origin"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlTypeA" runat="server"></asp:DropDownList></td>
                                <td><asp:Literal ID="ltlTypeB" runat="server" Text="Problem"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlTypeB" runat="server"></asp:DropDownList></td>
                                <td><asp:Literal ID="ltlTypeC" runat="server" Text="Call"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlTypeC" runat="server"></asp:DropDownList></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
                <table class="npadmintable">
                    <tr>
                        <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlContract" runat="server" Text="Contract"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <np:workordercontract ID="workordercontract" runat="server" />
                        </td>                        
                    </tr>
                </table>
         </td>
     </tr>
  </table>
        </td>
    </tr>
    </asp:Panel>
</table>
<asp:Literal ID="errCannotCloseWithoutSolution" runat="server" Text="Service call cannot be closed without a solution assigned to it or a resolution entered." Visible="false"></asp:Literal>