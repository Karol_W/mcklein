<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="workordersolution.ascx.cs" Inherits="netpoint.admin.support.controls.workordersolution" %>
<%@ Register TagPrefix="np" TagName="solutionpicker" Src="~/admin/common/controls/solutionpicker.ascx" %>

<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<br />
<asp:Panel ID="pnlEnable" runat="server" > 
<asp:gridview ID="gvSolutions" runat="server" DataKeyNames="SolutionID" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Solutions Associated" OnRowCommand="gvSolutions_RowCommand">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:templatefield>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# Bind("SolutionID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:templatefield>
        <asp:HyperLinkField HeaderText="ID" DataTextField="SolutionID" DataNavigateUrlFields="SolutionID" DataNavigateUrlFormatString="~/admin/support/SolutionDetail.aspx?SolutionID={0}"  />
        <asp:TemplateField HeaderText="Solution|Knowledge Base Solution" >
            <ItemTemplate>
                <b><asp:Literal ID="ltlName" runat="server" Text='<%# Bind("SolutionName") %>' ></asp:Literal></b>
                <br />
                <asp:Literal ID="ltlDesc" runat="server" Text='<%# Bind("Description") %>' ></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>        
    </Columns>
</asp:gridview>

<div class="npadminactionbar">
<asp:Label runat="server" CssClass="npadminbody" ID="lblSolutionID" Text="ArticleID"></asp:Label>
<np:solutionpicker runat="server" ID="solutionpicker" />
    <asp:ImageButton runat="server" ID="btnAddSolution" ImageUrl="~/assets/common/icons/link.gif" OnClick="btnAddSolution_Click" ToolTip="Add Knowledge-Base Entry" />
</div>
</asp:Panel>

<asp:Literal ID="hdnInvalidSolution" runat="server" Visible="False" text="Solution ID must be a number.  Enter a valid solution id or search for a new one."></asp:Literal>