<%@ Control Language="C#" AutoEventWireup="true" Inherits="netpoint.admin.common.support.controls.sectionusers"
    Codebehind="sectionusers.ascx.cs" %>
<asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="grid_RowCancelingEdit"
    OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" OnRowUpdating="grid_RowUpdating"
    OnRowEditing="grid_RowEditing" CssClass="npadmintable" EmptyDataText="No Users Assigned to Section">
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/remove.gif">
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colUser|User">
            <ItemTemplate>
                <asp:Literal ID="ltlUser" runat="server"></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlUserID" runat="server">
                </asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEmail|Email">
            <ItemTemplate>
                <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtEmail" runat="server" Columns="50" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colActive|Active">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Image runat="server" ID="imgActive" ImageUrl="~/assets/common/icons/checked.gif"
                    Visible='<%# DataBinder.Eval(Container.DataItem, "Active") %>' />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Active") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" CommandName="cancel" ImageUrl="~/assets/common/icons/cancel.gif" />
                &nbsp;
                <asp:ImageButton ID="btnSave" runat="server" CommandName="update" ImageUrl="~/assets/common/icons/save.gif" />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div style="text-align: right">
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
        ToolTip="Add New User to Section" OnClick="btnAdd_Click" />
</div>
<asp:Literal ID="hdnRemove" runat="server" Text="Remove this user from the queue."
    Visible="false"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCancel" runat="server" Text="Cancel this edit" Visible="false"></asp:Literal>
<asp:Literal ID="hdnActive" runat="server" Text="Active" Visible="false"></asp:Literal>