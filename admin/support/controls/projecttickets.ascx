<%@ Control Language="C#" AutoEventWireup="true" Codebehind="projecttickets.ascx.cs"
    Inherits="netpoint.admin.support.controls.projecttickets" %>
<asp:GridView ID="grid" runat="server" CssClass="npadmintable" AutoGenerateColumns="false"
    AllowPaging="true" AllowSorting="true" EmptyDataText="No Tickets Created in Section" OnPageIndexChanging="grid_PageIndexChanging" OnSorting="grid_Sorting">
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <RowStyle CssClass="npadminbody" />
    <PagerStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="TicketID" DataTextField="TicketID"
            DataNavigateUrlFormatString="~/admin/support/ticketadmin.aspx?ticketid={0}" />
        <asp:TemplateField HeaderText="colSubject|Subject" SortExpression="Subject">
            <ItemTemplate>
                <asp:Label ID="TicketType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketType") %>' />
                <b>
                    <asp:HyperLink runat="server" ID="lnkTicket" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.TicketID","~/admin/support/ticketadmin.aspx?ticketid={0}") %>'
                        Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>' /></b>
                <br />
                <asp:Label runat="server" ID="lbl" Text='<%# DataBinder.Eval(Container, "DataItem.UserEmailAddress") %>'></asp:Label>
                -
                <asp:Label runat="server" ID="Label2" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'></asp:Label>
                <br />
                <br />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colSubmitDate|Submit Date" SortExpression="SubmitDate, AssignedUserID">
            <ItemStyle Wrap="false" HorizontalAlign="right" VerticalAlign="Top" />
            <ItemTemplate>
                <asp:Label ID="lblSubmitDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate","{0:dd-MMM-yyyy}") %>' />
                <br />
                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedUserID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPriority|Priority" SortExpression="Priority,Sequence">
            <ItemStyle HorizontalAlign="center" VerticalAlign="top" />
            <ItemTemplate>
                <asp:Label ID="lblPriority" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Priority") %>' />
                /
                <asp:Label ID="lblSequence" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Sequence") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="npadminbody" style="text-align:right">
    <asp:Literal ID="ltlMaxRows" runat="server" Text="MaxRows"></asp:Literal>&nbsp;
    <asp:DropDownList ID="ddlMaxRows" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMaxRows_SelectedIndexChanged"></asp:DropDownList>&nbsp;
    <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal>&nbsp;
    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;
    <asp:HyperLink ID="lnkAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add"></asp:HyperLink>
</div>
