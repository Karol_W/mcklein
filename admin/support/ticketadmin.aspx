<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.ticketAdmin" Codebehind="ticketAdmin.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="ticketgeneral" Src="~/admin/support/controls/ticketgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="ticketmessage" Src="~/admin/support/controls/ticketmessage.ascx" %>
<%@ Register TagPrefix="np" TagName="ticketattributes" Src="~/admin/support/controls/ticketattributes.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader">
                <asp:HyperLink ID="lnkPrevious" runat="server" ImageUrl="~/assets/common/icons/previous.gif" Visible="false" ToolTip="Previous"></asp:HyperLink>&nbsp;
                <asp:HyperLink ID="lnkNext" runat="server" ImageUrl="~/assets/common/icons/next.gif" Visible="false" ToolTip="Next"></asp:HyperLink></td>
            <td class="npadminheader" align="right">
                <asp:Literal ID="ltlHeader" runat="server"></asp:Literal>&nbsp;
                <asp:ImageButton ID="btSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save Ticket"></asp:ImageButton></td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsSummary" Text="Summary"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsEdit" Text="Edit"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsAttributes" Text="Attributes"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" id="pgMessage" runat="server">
               <np:ticketmessage runat="server" id="ticketmessage"></np:ticketmessage></ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:ticketgeneral runat="server" id="ticketgeneral"></np:ticketgeneral></ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                 <np:ticketattributes runat="server" id="ticketattributes"></np:ticketattributes></ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
<asp:Literal ID="hdnTicketNo" runat="server" Text="Ticket No." Visible="False"></asp:Literal>
</asp:Content>
