<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="projectadd.aspx.cs" Inherits="netpoint.admin.common.support.projectadd" %>
<%@ Register TagPrefix="np" TagName="projectgeneral" Src="~/admin/support/controls/projectgeneral.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        <td class="npadminheader" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />
            &nbsp;
        </td>
    </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:Literal ID="ltlSubHeader" runat="server" Text="Add Project" />
            </td>
        </tr>
    </table>
    <np:projectgeneral ID="projectgeneral" runat="server" />
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnProdjectDoesNotExist" runat="server" Text="The project does not exist."></asp:Literal>
    <asp:Literal ID="hdnParentIsItself" runat="server" Text="The parent project can not be itself."></asp:Literal>
</asp:Content>