<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master"
    Codebehind="workorders.aspx.cs" Inherits="netpoint.admin.common.support.workorders" %>
<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Service Calls</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                    ToolTip="Add Service Call" NavigateUrl="~/admin/support/workorderadd.aspx"></asp:HyperLink>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colFilter" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colFilter_Click" />
                <asp:ImageButton ID="expFilter" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expFilter_Click" Visible="false" />
                &nbsp;
                <asp:Literal ID="ltlFitlers" runat="server" Text="Save or edit a filter set"></asp:Literal>
                &nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" />
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" ToolTip="Reset" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2">
                <np:NPSearch ID="search" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colResults" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colResults_Click" Visible="false" />
                <asp:ImageButton ID="expResults" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expResults_Click" Visible="false" />
                &nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnGo" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnGo_Click" ToolTip="Go"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
        AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound" OnPageIndexChanging="grid_PageIndexChanging"
        OnSorting="grid_Sorting" EmptyDataText="No Service Calls Found">
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <HeaderStyle CssClass="npadminsubheader" />
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <Columns>
            <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="SupportWorkOrderID"
                DataNavigateUrlFormatString="~/admin/support/workorder.aspx?workid={0}&backpage=~/admin/support/workorders.aspx"
                DataTextField="SupportWorkOrderID" SortExpression="SupportWorkOrderID" ItemStyle-HorizontalAlign="center" />
            <asp:BoundField HeaderText="colName|Subject" DataField="WorkName" SortExpression="WorkName" />
            <asp:BoundField HeaderText="Customer|Customer" DataField="AccountID" SortExpression="AccountID" />
            <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Status" ItemStyle-HorizontalAlign="center">
                <ItemTemplate>
                    <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Priority|Priority" SortExpression="Priority" ItemStyle-HorizontalAlign="center">
                <ItemTemplate>
                    <asp:Literal ID="ltlPriority" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
    </asp:GridView>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
