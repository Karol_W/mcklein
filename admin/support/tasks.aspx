<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" Codebehind="tasks.aspx.cs" Inherits="netpoint.admin.common.support.tasks" %>
<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Tasks*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkNew" runat="server" ToolTip="Add Task" NavigateUrl="~/admin/support/taskadd.aspx?backpage=~/admin/support/tasks.aspx"
                    ImageUrl="~/assets/common/icons/add.gif"></asp:HyperLink>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">&nbsp;
                <asp:ImageButton ID="colFilter" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif" OnClick="colFilter_Click" Visible="false" />
                <asp:ImageButton ID="expFilter" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif" OnClick="expFilter_Click" />&nbsp;
                <asp:Literal ID="ltlFitlers" runat="server" Text="Save or edit a filter set"></asp:Literal>&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:NPSearch ID="search" runat="server" /></td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colResults" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colResults_Click" Visible="false" />
                <asp:ImageButton ID="expResults" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expResults_Click" Visible="false"/>&nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal></td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnGo" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnGo_Click" ToolTip="Go"></asp:ImageButton>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView 
                    ID="grid" 
                    runat="server" 
                    AutoGenerateColumns="False" 
                    CssClass="npadminbody"
                    Width="100%" 
                    AllowPaging="True" 
                    AllowSorting="True" 
                    OnPageIndexChanging="grid_PageIndexChanging"
                    OnRowDataBound="grid_RowDataBound"
                    OnSorting="grid_Sorting" 
                    EmptyDataText="No Tasks Found" 
                    PageSize="20">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:HyperLinkField HeaderText="colSubject|Subject" DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/admin/support/task.aspx?taskid={0}&backpage=~/admin/support/tasks.aspx"
                            DataTextField="Subject" SortExpression="Subject" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField HeaderText="colUser|User" DataField="UserID" SortExpression="UserID" />
                        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Status" ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colPriority|Priority" SortExpression="Priority" ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>
                                <asp:Literal ID="ltlPriority" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colDue|Due" ItemStyle-HorizontalAlign="center" SortExpression="DueDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlDueDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
