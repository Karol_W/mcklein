<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.tickets" CodeBehind="tickets.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Support Tickets*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnReport" runat="server" ImageUrl="~/assets/common/icons/report.gif" Visible="false" OnClick="btnReport_Click" ToolTip="Report" />&nbsp;
                <asp:HyperLink ID="lnkNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/support/ticketadd.aspx" ToolTip="Add Ticket"></asp:HyperLink></td>
        </tr>
        <tr>
            <td class="npadminsubheader">&nbsp;
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="colFilter" ToolTip="Collapse" OnClick="colFilter_Click" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter" ToolTip="Expand" Visible="false" OnClick="expFilter_Click" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or edit a filter set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" /></td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder ID="search" runat="server" PersistFilter="true" /></td>
        </tr>
        <tr>
            <td class="npadminsubheader">&nbsp;
                <asp:Image ID="sysDot" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" Visible="false" />&nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal></td>
            <td class="npadminsubheader" align="right">
                <asp:HyperLink ID="lnkReport" runat="server" ImageUrl="~/assets/common/icons/report.gif" ToolTip="Report"></asp:HyperLink>
                <asp:ImageButton ID="btnGo" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnGo_Click" ToolTip="Go" /></td>
        </tr>
       </table>
        <asp:GridView ID="grid" 
            runat="server" 
            AllowPaging="True" 
            AllowSorting="True" 
            AutoGenerateColumns="False"
            CssClass="npadmintable" 
            PageSize="50" 
            OnRowDataBound="grid_RowDataBound"
            OnPageIndexChanging="grid_PageIndexChanging" 
            OnSorting="grid_Sorting"
            DataKeyNames="TicketID"
            >
            <HeaderStyle CssClass="npadminsubheader" />
            <EmptyDataRowStyle CssClass="npadminempty" height="50px" />
            <RowStyle CssClass="npadminbody" />
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <Columns>
                <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="TicketID" DataNavigateUrlFormatString="~/admin/support/ticketadmin.aspx?ticketid={0}"
                    DataTextField="TicketID" SortExpression="TicketID" ItemStyle-HorizontalAlign="center">
                    <ItemStyle VerticalAlign="top" />
                </asp:HyperLinkField>
                <asp:TemplateField HeaderText="colSubject|Subject" SortExpression="Subject">
                    <ItemTemplate>
                        <asp:Label ID="TicketType" runat="server" />
                        <b><asp:HyperLink runat="server" ID="lnkTicket" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.TicketID", "~/admin/support/ticketadmin.aspx?ticketid={0}") %>'
                            Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>' /></b>
                        <br />
                        <asp:Label runat="server" ID="lbl" Text='<%# DataBinder.Eval(Container, "DataItem.UserEmailAddress") %>'></asp:Label>
                        - 
                        <asp:Label runat="server" ID="Label2" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'></asp:Label>
                        <br /><br />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSection|Section">
                    <ItemTemplate>
                        <asp:Literal ID="ltlSectionName" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSubmitDate|Submit Date" SortExpression="SubmitDate,AssignedUserID">
                    <ItemStyle Wrap="false" HorizontalAlign="right" VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:Label ID="lblSubmitDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate","{0:dd-MMM-yyyy}") %>' />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedUserID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colPriority|Priority" SortExpression="Priority,Sequence">
                    <ItemStyle HorizontalAlign="center" VerticalAlign="top" />
                    <ItemTemplate>
                        <asp:Label ID="lblPriority" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Priority") %>' />
                        /
                        <asp:Label ID="lblSequence" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Sequence") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
        </asp:GridView>
        <br />
        <table class="npadmintable">
            <tr>
                <td class="npsubheader">&nbsp;
                    <asp:Image ImageUrl="~/assets/common/icons/indicator.gif" runat="server"></asp:Image>&nbsp;
                    <asp:Literal ID="ltlBulkOperations" runat="server" Text="Bulk Operations"></asp:Literal></td>
            </tr>
        </table>
         <ComponentArt:TabStrip ID="TabStripBulk" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ActionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0" Visible="false">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="ts1" Text="Assign To User"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Update Status"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Assign To Queue"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Assign To Project"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ActionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="100" Visible="false">
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel" style="width:20%;"><asp:Literal runat="server" ID="ltlAssign" Text="Assign To"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlAssignTo"></asp:DropDownList></td>
                        <td class="npadminbody" style="width:5px;">
                            <asp:ImageButton ID="btnAssignGo" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnAssignGo_Click" ToolTip="AssignGo" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlAssignMessage" Text="Message"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox ID="txtAssignMessage" runat="server" TextMode="MultiLine" Width="100%" Height="50"></asp:TextBox></td>                   
                    </tr>
                </table>
            </ComponentArt:PageView>
             <ComponentArt:PageView CssClass="npadminbody" runat="server">
               <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel" style="width:20%;"><asp:Literal runat="server" ID="ltlStatus" Text="New Status"></asp:Literal></td>
                        <td class="npadminbody">
                            <asp:DropDownList runat="server" ID="ddlStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>
                            <asp:DropDownList runat="server" ID="ddlSubStatus"></asp:DropDownList></td>
                        <td class="npadminbody" style="width:5px;">
                            <asp:ImageButton ID="btnStatus" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnStatus_Click" ToolTip="Status" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlStatusMessage" Text="Message"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox ID="txtStatusMessage" runat="server" TextMode="MultiLine" Width="100%" Height="50"></asp:TextBox></td>     
                    </tr>
                </table>
            </ComponentArt:PageView>
             <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel" style="width:20%;"><asp:Literal runat="server" ID="ltlQueue" Text="New Queue"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlQueue"></asp:DropDownList></td>
                        <td class="npadminbody" style="width:5px;">
                            <asp:ImageButton ID="btnQueue" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnQueue_Click" ToolTip="Queue" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlQueueMessage" Text="Message"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox ID="txtQueueMessage" runat="server" TextMode="MultiLine" Width="100%" Height="50"></asp:TextBox></td> 
                    </tr>
                </table>
            </ComponentArt:PageView>
             <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel" style="width:20%;"><asp:Literal runat="server" ID="ltlProject" Text="New Project"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList runat="server" ID="ddlProject"></asp:DropDownList></td>
                        <td class="npadminbody" style="width:5px;">
                            <asp:ImageButton ID="btnProject" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnProject_Click" ToolTip="Project" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlProjectMessage" Text="Message"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox ID="txtProjectMessage" runat="server" TextMode="MultiLine" Width="100%" Height="50"></asp:TextBox></td>
                    </tr>
                </table>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
        <table class="npadmintable" runat="server" id="tblEmpty">
            <tr>
                <td class="npadminempty" style="height:50px;">
                    <asp:Literal ID="ltlNoResultsForBulk" runat="server" Text="A search with results will enable bulk tools."></asp:Literal></td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="ltlNoRecords" runat="server" Text="No tickets match criteria" Visible="False"></asp:Literal>
</asp:Content>
