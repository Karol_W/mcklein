<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="contracttemplateadd.aspx.cs" Inherits="netpoint.admin.support.contracttemplateadd" %>

<%@ Register TagPrefix="np" TagName="template" Src="~/admin/support/controls/templategeneral.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        <td class="npadminheader" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />
            &nbsp;
        </td>
    </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:Literal ID="ltlSubHeader" runat="server" Text="Add Contract Template" />
            </td>
        </tr>
    </table>
    <np:template ID="template" runat="server" />
</asp:Content>