<%@ Page MasterPageFile="~/masters/admin.Master" Language="C#" AutoEventWireup="true" CodeBehind="attributedef.aspx.cs" Inherits="netpoint.admin.support.attributedef" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlHeader" runat="server" Text="Support Ticket Attributes" /></td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add"></asp:ImageButton></td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" CssClass="npadmintable" AutoGenerateColumns="False" EmptyDataText="No Attributes Defined" OnRowDataBound="grid_RowDataBound" OnRowCommand="grid_RowCommand" OnRowEditing="grid_RowEditing" OnRowCreated="grid_RowCreated">
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <FooterStyle CssClass="npadminbody"></FooterStyle>
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif" CommandName="gvdelete" CommandArgument='<%#Bind("AttributeDefinitionID") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colLabel|Label">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLabel" runat="server" Text='<%#Bind("Label") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="ltlLabel" runat="server" Text='<%#Bind("Label") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colDataType|Data Type">
                <ItemTemplate>
                    <asp:Literal ID="ltlDataType" runat="server" Text='<%#Bind("DataType") %>'></asp:Literal>
                </ItemTemplate>
                 <EditItemTemplate>
                    <asp:DropDownList ID="ddlDataType" runat="server"></asp:DropDownList>
                 </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colLookupType|Lookup Type">
                <ItemTemplate>
                    <asp:Literal ID="ltlLookupType" runat="server" Text='<%#Bind("LookupType") %>'></asp:Literal>
                </ItemTemplate>
                 <EditItemTemplate>
                    <asp:DropDownList ID="ddlLookupType" runat="server"></asp:DropDownList>
                 </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colSort|Sort">
                <ItemTemplate>
                    <asp:Literal ID="ltlSortOrder" runat="server" Text='<%#Bind("SortOrder") %>'></asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtSortOrder" Width="50px" runat="server" Text='<%#Bind("SortOrder") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif"></asp:ImageButton>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:ImageButton ID="btnSave" runat="server" CommandName="gvsave" ImageUrl="~/assets/common/icons/save.gif" CommandArgument='<%#Bind("AttributeDefinitionID") %>'></asp:ImageButton>
                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="gvcancel" ImageUrl="~/assets/common/icons/cancel.gif"></asp:ImageButton>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
<asp:Literal ID="ltlDeleteWarning" runat="server" Visible="false" Text="Deletions are permanent\nAll data saved for this Attribute will be lost.\nContinue?" />
</asp:Content>