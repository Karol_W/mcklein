<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.TicketAdd" ValidateRequest="false" Codebehind="TicketAdd.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" CommandName="submit" />&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader"><asp:Literal ID="ltlSubHeader" runat="server" Text="New Ticket" /></td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlOrigiation" runat="server" Text="Origination"></asp:Literal></td>
            <td class="npadminbody" colspan="3"><div id="FormDiv1"><asp:DropDownList ID="ddlOrigination" runat="server"></asp:DropDownList></div></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlAccountID" runat="server" Text="AccountID:"></asp:Literal></td>
            <td class="npadminbody"><np:AccountPicker ID="acct" runat="server" PostBack="true"></np:AccountPicker></td>
             <td class="npadminlabel"><asp:Literal ID="ltlUserID" runat="server" Text="UserID:"></asp:Literal></td>
            <td class="npadminbody">
                <div id="FormDiv2">
                    <asp:DropDownList ID="ddlUser" runat="server" Enabled="False" AutoPostBack="true" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged"></asp:DropDownList>
                </div></td>
        </tr>
         <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal></td>
            <td class="npadminbody" colspan="3">
                <asp:TextBox ID="txtSubject" runat="server" Columns="60"></asp:TextBox>
                <asp:Image ID="imgValidSubject" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" valign="top"><asp:Literal ID="ltlMessage" runat="server" Text="Message:"></asp:Literal></td>
            <td class="npadminbody" valign="top" colspan="3">
                <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Columns="60" Rows="10" Wrap="false"></asp:TextBox>
                <asp:Image ID="imgValidMessage" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" ImageAlign="Top" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlEmail1" runat="server" Text="Email:"></asp:Literal></td>
            <td class="npadminbody" colspan="3">
                <asp:TextBox ID="txtUserEmailAddress" runat="server" Columns="40"></asp:TextBox>
                <asp:Image ID="imgEmail" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Literal ID="ltlInvalidEmail" runat="server" Text="Invalid Email address" Visible="False"></asp:Literal>
                <asp:CheckBox ID="cbSendContactEmail" runat="server" Checked="false" Text="Send Confirmation"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlProject" runat="server" Text="Project"></asp:Literal></td>
            <td class="npadminbody"colspan="3"><asp:DropDownList ID="ddlProjectID" runat="server"></asp:DropDownList></td>
        </tr>
         <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTicketType" runat="server" Text="Type"></asp:Literal></td>
            <td class="npadminbody"colspan="3"><asp:DropDownList ID="ddlTicketType" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlSection" runat="server" Text="Queue:"></asp:Literal></td>
            <td class="npadminbody" colspan="3"><asp:DropDownList ID="ddlSectionID" runat="server"></asp:DropDownList>
            <asp:Image ID="imgValidSection" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" ImageAlign="Top" /></td>
        </tr>
       
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errIsRequired" runat="server" Visible="False" Text="is required"></asp:Literal>
</asp:Content>
