<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Register Src="~/admin/support/controls/sectionusers.ascx" TagPrefix="np" TagName="SectionUsers" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.support.supportsectiondetail" Codebehind="supportsectiondetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="supportsections" Src="~/admin/support/controls/sectionsections.ascx" %>
<%@ Register TagPrefix="np" TagName="sectiongeneral" Src="~/admin/support/controls/sectiongeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="sectiontickets" Src="~/admin/support/controls/sectiontickets.ascx"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDelete_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSaveAndReturn_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="SectionsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="SectionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsUsers" Text="Users"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsSubQueues" Text="SubQueues"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTickets" Text="Tickets"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsStrings" Text="Strings" />
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="SectionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvQueueGeneral">
                <np:sectiongeneral ID="general" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvQueueUsers">
                <np:SectionUsers ID="users" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvSubQueues">
                <np:supportsections ID="sections" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvTickets">
                <np:sectiontickets ID="tickets" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvLanguage">
                <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteMessage" runat="server" Text="Deletions are permanent.\nAll sub-queues will be deleted, too.\nContinue with deletion?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnSectionName" runat="server" Text="Section Name" Visible="False"></asp:Literal>  
</asp:Content>
