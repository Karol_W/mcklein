<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master"
    Codebehind="projects.aspx.cs" Inherits="netpoint.admin.common.support.projects" %>

<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Services Projects*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkNew" runat="server" ToolTip="Add Project" NavigateUrl="~/admin/support/projectadd.aspx"
                    ImageUrl="~/assets/common/icons/add.gif"></asp:HyperLink>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colFilter" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colFilter_Click" />
                <asp:ImageButton ID="expFilter" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expFilter_Click" Visible="false" />
                &nbsp;
                <asp:Literal ID="ltlFitlers" runat="server" Text="Save or edit a filter set"></asp:Literal>
                &nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" />
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" ToolTip="Reset" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <np:NPSearch ID="search" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:ImageButton ID="colResults" runat="server" ToolTip="Collapse" ImageUrl="~/assets/common/icons/collapse.gif"
                    OnClick="colResults_Click" Visible="false" />
                <asp:ImageButton ID="expResults" runat="server" ToolTip="Expand" ImageUrl="~/assets/common/icons/expand.gif"
                    OnClick="expResults_Click" Visible="false" />
                &nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results" Visible="false"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnGo" runat="server" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnGo_Click" ToolTip="Go"></asp:ImageButton>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
                    Width="100%" AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound"
                    OnPageIndexChanging="grid_PageIndexChanging" OnRowCommand="grid_RowCommand" OnSorting="grid_Sorting"
                    EmptyDataText="No Projects Found">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="SupportProjectID" DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}&backpage=~/admin/support/projects.aspx"
                            SortExpression="SupportProjectID" DataTextField="SupportProjectID">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                        <asp:HyperLinkField HeaderText="colName|Name" DataNavigateUrlFields="SupportProjectID"
                            DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}&backpage=~/admin/support/projects.aspx"
                            SortExpression="ProjectName" DataTextField="ProjectName" />
                        <asp:HyperLinkField HeaderText="Customer|Customer" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/accountinfo.aspx?accountid={0}&backpage=~/admin/support/projects.aspx"
                            SortExpression="AccountID" DataTextField="AccountID" />
                        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colStart|Start" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colEnd|End" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlEndDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
