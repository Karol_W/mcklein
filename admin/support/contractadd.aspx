<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="contractadd.aspx.cs" Inherits="netpoint.admin.common.support.contractadd" %>
<%@ Register TagPrefix="np" TagName="contractgeneral" Src="~/admin/support/controls/contractgeneral.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        <td class="npadminheader" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSave_Click" />&nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2" class="npadminsubheader">
            <asp:Literal ID="ltlSubHeader" runat="server" Text="New Contract"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <np:contractgeneral ID="contractgeneral" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>
