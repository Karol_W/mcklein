<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="task.aspx.cs" Inherits="netpoint.admin.common.support.task" %>
<%@ Register TagPrefix="np" TagName="NPTask" Src="~/admin/support/controls/task.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" CssClass="NPWarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete Task" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDelete_Click" />
                <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save Task" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"><np:NPTask ID="taskMain" runat="server" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDelete" runat="server" Text="Deletions are permanent.  Continue?" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnSubjectRequired" runat="server" Text="The subject is required."></asp:Literal>
</asp:Content>
