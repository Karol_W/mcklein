<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" Codebehind="default.aspx.cs" Inherits="netpoint.admin.support._default" %>
<%@ Register TagPrefix="np" TagName="LandingPage" Src="~/admin/common/controls/landingpage.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <asp:Literal ID="sysMessage" runat="server"></asp:Literal>
    <np:LandingPage ID="lpage" runat="server" />
</asp:Content>
