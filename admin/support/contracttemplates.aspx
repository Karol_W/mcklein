<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true"
    Inherits="netpoint.admin.common.support.contracttemplates" Title="Untitled Page"
    Codebehind="contracttemplates.aspx.cs" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="Server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:Hyperlink ID="lnkAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                ToolTip="Add New Template" Navigateurl="~/admin/support/contracttemplateadd.aspx"></asp:Hyperlink>
            &nbsp;
            </td>
        </tr>
    </table>
    <div id="psuedoHeader" runat="server" class="npadminsubheader"></div>
    <div class="npadminbody">
        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false"  OnRowDataBound="grid_RowDataBound"
            OnRowDeleting="grid_RowDeleting" CssClass="npadmintable" EmptyDataText="No Contracts Found" OnPreRender="grid_PreRender">
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <Columns>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif"
                            ToolTip="Delete this template" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField HeaderText="colName|Name" DataNavigateUrlFields="ContractTemplateID" 
                    DataNavigateUrlFormatString="~/admin/support/contracttemplate.aspx?templateid={0}"
                    DataTextField="TemplateName" />
                <asp:BoundField HeaderText="colDuration|Duration" DataField="Duration" />
                <asp:TemplateField HeaderText="colType|Type">
                    <ItemTemplate>
                        <asp:Literal ID="ltlType" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="colDescription|Description" DataField="Description" />
            </Columns>
        </asp:GridView>
       
    </div>
</asp:Content>
<asp:Content ID="hidden" ContentPlaceHolderID="hiddenslot" runat="Server">
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete Template"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit Template"></asp:Literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.  Continue?"></asp:Literal>
</asp:Content>
