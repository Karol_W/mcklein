<%@ Page MasterPageFile="~/masters/default.master" Language="c#" Inherits="netpoint.admin.common.support.ticketsreports" Codebehind="ticketsreports.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<style>
hr.e {border: none 0; 
	border-top: 1px dashed #000;
	border-bottom: 1px dashed #ccc;
	height: 2px;
	margin: 10px auto 0 0;
	text-align: left;
	}	
	
	</style>
	
    <table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="750" border="0">
        <tr>
            <td style="background-color:#ffffff;" align="center"><b><asp:literal id="ltlReport" runat="server" text="Report"></asp:literal></b><hr class="e" ></td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlFilterItem" runat="server" Text="FilterItem"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:Repeater ID="rptTickets" runat="server" OnItemDataBound="rptTickets_ItemDataBound">
                    <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td class="npadminlabel" width="70"><asp:Literal ID="ltlTicketID" Text="Ticket #" runat="server"></asp:Literal></td>
                                <td class="npadminbody" width="100"><asp:Literal ID="ltlTicketIDActual" runat="server"></asp:Literal></td>
                                <td class="npadminlabel" width="70"><asp:Literal ID="ltlPriority" Text="Priority/Seq:" runat="server"></asp:Literal></td>
                                <td class="npadminbody" width="100"><asp:Literal ID="ltlPriorityActual" runat="server"></asp:Literal> /
                                <asp:Literal ID="ltlSequenceActual" runat="server"></asp:Literal></td>
                                <td class="npadminlabel" width="100"><asp:Literal ID="ltlStatus" Text="Status:" runat="server"></asp:Literal></td>
                                <td class="npadminbody" width="180"><asp:Literal ID="ltlStatusActual" runat="server"></asp:Literal></td>
                                <td class="npadminlabel" width="100"><asp:Literal ID="ltlAssigned" Text="Assigned User:" runat="server"></asp:Literal></td>
                                <td class="npadminbody" width="200"><asp:Literal ID="ltlAssignedActual" runat="server"></asp:Literal></td>                              
                                <td class="npadminlabel" width="70"><asp:Literal ID="ltlDateOpen" Text="Date Open:" runat="server"></asp:Literal></td>
                                <td class="npadminbody" width="100"><asp:Literal ID="ltlDateOpenLiteral" runat="server"></asp:Literal></td>
                            </tr>
							<tr>
							<td class="npadminlabel" width="70" valign="top"><asp:Literal ID="ltlSubject" Text="Subject:" runat="server"></asp:Literal></td>
							<td colspan="5" class="npadminbody"><b><asp:Literal ID="ltlSubjectActual" runat="server"></asp:Literal></b></td>
							</tr>
                            <tr>
                                <td class="npadminlabel" width="70" valign="top"><asp:Literal ID="ltlMessage" Text="Message:" runat="server"></asp:Literal></td>
                                <td colspan="5" class="npadminbody"><asp:Literal ID="ltlMessageActual" runat="server"></asp:Literal></td>
                            </tr>
                        </table>
                    </ItemTemplate>
					<SeparatorTemplate><hr class="e" ></SeparatorTemplate>

                </asp:Repeater>
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" Visible="false" ID="hdnEqualTo" Text="Equal To"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnNotEqualTo" Text="Not Equal To"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnStartsWith" Text="Starts With"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnEndsWith" Text="Ends With"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnContains" Text="Contains"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnGreaterThan" Text="Greater Than"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnGreaterThanEqualTo" Text="Greater Than or Equal To"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnLessThan" Text="Less Than"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnLessThanEqualTo" Text="Less Than or Equal To"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnIS" Text="IS NULL"></asp:TextBox>
    <asp:TextBox runat="server" Visible="false" ID="hdnISNOT" Text="IS NOT NULL"></asp:TextBox>
</asp:Content>
