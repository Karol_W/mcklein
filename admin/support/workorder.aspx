<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.support.workorder" CodeBehind="workorder.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Src="controls/workordergeneral.ascx" TagName="workordergeneral" TagPrefix="np" %>
<%@ Register Src="controls/workordertickets.ascx" TagName="workordertickets" TagPrefix="np" %>
<%@ Register Src="controls/workorderevents.ascx" TagName="workorderevents" TagPrefix="np" %>
<%@ Register Src="controls/workordersolution.ascx" TagName="workordersolution" TagPrefix="np" %>
<%@ Register Src="controls/workorderresolution.ascx" TagName="workorderresolution" TagPrefix="np" %>
<%@ Register Src="controls/workorderremarks.ascx" TagName="workorderremarks" TagPrefix="np" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="Server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:Literal ID="ltlHeader" runat="server"></asp:Literal>&nbsp;               
                <asp:ImageButton ID="btnSaveAndReturn" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save and Return" OnClick="btnSaveAndReturn_Click" />&nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts9" Text="Remarks"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Activities"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts6" Text="Solution"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts8" Text="Resolution"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts7" Text="Tickets"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:workordergeneral ID="workordergeneral" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
               <np:workorderremarks ID="workorderremarks" runat="server" />
            </ComponentArt:PageView>              
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:workorderevents ID="workorderevents" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
               <np:workordersolution ID="workordersolution" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
               <np:workorderresolution ID="workorderresolution" runat="server" />
            </ComponentArt:PageView>            
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:workordertickets ID="workordertickets" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" ContentPlaceHolderID="hiddenslot" runat="Server">
</asp:Content>
