<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="uicontrols.aspx.cs" Inherits="netpoint.admin.common.uicontrols" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                &nbsp;
                <asp:Hyperlink ID="lnkAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                    ToolTip="Add" NavigateUrl="~/admin/common/uicontrol.aspx?controlid=0"></asp:Hyperlink>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
    <asp:GridView ID="grid" runat="server" CssClass="npadmintable" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting"
         OnPageIndexChanging="grid_PageIndexChanging" OnSorting="grid_Sorting" EmptyDataText="No Records Found">
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <RowStyle CssClass="npadminbody" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <HeaderStyle CssClass="npadminsubheader" />
        <Columns>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField headertext="colID|ID" DataField="ControlName" SortExpression="ControlName" />
            <asp:BoundField HeaderText="colNamespace|Namespace" DataField="NameSpace" SortExpression="NameSpace" />
            <asp:BoundField HeaderText="colName|Name" DataField="PublicName" SortExpression="PublicName" />
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Hyperlink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal ID="hdnDelete" runat="server" Text="Delete this control"></asp:literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.\nContine?"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit this control"></asp:Literal>
</asp:Content>