<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.common.webflow.WebFlow" Codebehind="WebFlow.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader" colspan="2">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="2"><asp:Label ID="sysError" runat="server" CssClass="npadminwarning"></asp:Label></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlSourcePage" runat="server" Text="Source Page"></asp:Literal></td>
                <td><asp:TextBox ID="txtPage" runat="server" Width="400px" Columns="60"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlNextPage" runat="server" Text="Next Page"></asp:Literal></td>
                <td><asp:TextBox ID="txtNextPage" runat="server" Width="400px" Columns="60"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlSecure" runat="server" Text="Secure"></asp:Literal></td>
                <td><asp:CheckBox ID="sysSecure" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlAcctType" runat="server" Text="Account Type"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlAcctType" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlAcctIndustry" runat="server" Text="Account Industry"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlAcctIndustry" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal></td>
                <td><asp:TextBox ID="txtSortOrder" runat="server" Text="0" Columns="5"></asp:TextBox></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnNone" runat="server" Text="None" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnMustBeNumber" runat="server" Text="Sort Order must be a number" Visible="false"></asp:Literal>
</asp:Content>
