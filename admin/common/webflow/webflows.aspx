<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="webflows.aspx.cs" Inherits="netpoint.admin.common.webflow.webflows" %>

<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
<table class="npadminpath">
        <tr>
            <td class="npadminheader" colspan="2">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:DropDownList ID="ddlFlow" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFlow_SelectedIndexChanged">
                </asp:DropDownList>&nbsp;
                 <asp:Hyperlink ID="lnkAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                ToolTip="Add New Item to Web Flow" NavigateUrl="~/admin/common/webflow/webflow.aspx?flow=0&flowname=checkout" ></asp:Hyperlink>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:GridView 
            id="gvWebflows"
            runat="server" 
            AutoGenerateColumns="false" 
            HeaderStyle-CssClass="npadminsubheader"
            CssClass="npadmintable" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            EmptyDataText="No Records Found" OnRowCommand="gvWebflows_RowCommand" OnRowDataBound="gvWebflows_RowDataBound">
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                            CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "webflowID") %>'>
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colFlow|Web Flow">
                    <ItemTemplate>
                        <asp:Literal ID="ltlFlowName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NamedFlow") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSourcePage|Source Page">
                    <ItemTemplate>
                        <asp:Literal ID="ltlSource" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Page") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colNextPage|Next Page">
                    <ItemTemplate>
                        <asp:Literal ID="ltlAction" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TheAction") %>'>
                        </asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colFilters|Filters">
                    <ItemTemplate>
                        <asp:Literal ID="ltlFilters" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                            CommandName="editthis" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "webflowid") %>'>
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <asp:Literal ID="ltlDeleteText" runat="server" Visible="false" Text="Deletions are permanent. Continue?"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hiddenslot" runat="server">
<asp:Literal ID="hdnNone" runat="server" Text="None" Visible="false"></asp:Literal>
</asp:Content>
