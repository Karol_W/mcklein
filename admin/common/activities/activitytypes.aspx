<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.activities.activitytypes" CodeBehind="activitytypes.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <div runat="server" id="divSubject" visible="false">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader">
                    <asp:Literal ID="ltlParentType" runat="server" Text="ParentType"></asp:Literal>&nbsp;
                    <asp:Literal ID="ltlSubjectHeader" runat="server" Text="Subjects"></asp:Literal></td>
            </tr>
        </table>
    </div>
    <asp:Label ID="sysError" runat="server" Visible="False" CssClass="npwarning"></asp:Label>
    <asp:GridView   
        ID="gvTypes" 
        runat="server" 
        CssClass="npadmintable" 
        AutoGenerateColumns="false" 
        EmptyDataText="No Results Found" 
        RowStyle-CssClass="npadminbody"
        EmptyDataRowStyle-CssClass="npadminempty" 
        EmptyDataRowStyle-Height="50px"  
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        HeaderStyle-CssClass="npadminsubheader"
        OnRowCommand="gvTypes_RowCommand" 
        OnRowDataBound="gvTypes_RowDataBound" 
        OnRowDeleting="gvTypes_RowDeleting" 
        OnRowEditing="gvTypes_RowEditing" 
        OnRowUpdating="gvTypes_RowUpdating" 
        OnRowCancelingEdit="gvTypes_RowCancelingEdit">
        <Columns>
            <asp:TemplateField HeaderText="colName|Name">
                <ItemTemplate>
                    <asp:Literal ID="ltlName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TypeName") %>'></asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TypeName") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField HeaderText="colValue|Value">
                <HeaderStyle HorizontalAlign="center" />
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal ID="ltlValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TypeCode") %>'></asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtValue" Style="text-align:center" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TypeCode") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colSysVal|Sys. Val.">
                <HeaderStyle HorizontalAlign="center" />
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="sysImage" runat="server" Visible="false" ImageUrl="~/assets/common/icons/checked.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ActivityTypeID") %>' CommandName="delete" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnCOS" runat="server" ImageUrl="~/assets/common/icons/language.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ActivityTypeID") %>' CommandName="cos" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ActivityTypeID") %>' CommandName="edit" />
                </ItemTemplate>
               <EditItemTemplate>
                    <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancel" />&nbsp;
                    <asp:ImageButton ID="btnSaveCode" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ActivityTypeID") %>' CommandName="update" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnSubject" runat="server" ImageUrl="~/assets/common/icons/detail.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ActivityTypeID") %>' CommandName="subject" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table class="npadmintable">
        <tr>
            <td align="right"><asp:ImageButton ID="imgNew" runat="server" ToolTip="New" ImageUrl="~/assets/common/icons/add.gif" OnClick="imgNew_Click" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hiding">
    <asp:Literal ID="errDelete" runat="server" Text="Deletions are permanent, are you sure?" Visible="false"></asp:Literal>
    <asp:Literal ID="errCode" runat="server" Text=" is already in use, please choose another code." Visible="false"></asp:Literal>
    <asp:Literal ID="ltlDelete" runat="server" Text="Delete" Visible="false"></asp:Literal>
    <asp:Literal ID="ltlSubject" runat="server" Text="Add Subject" Visible="false"></asp:Literal>
    <asp:Literal ID="ltlTranslate" runat="server" Text="Add Translation" Visible="false"></asp:Literal>
    <asp:Literal ID="ltlEdit" runat="server" Text="Edit" Visible="false"></asp:Literal>
</asp:Content>
