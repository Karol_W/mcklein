<%@ Page Language="c#" ValidateRequest="false" Trace="false" Inherits="netpoint.admin.common.controls.Gallery"
    Codebehind="Gallery.aspx.cs" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title></head>
<body>
    <form id="Form1" runat="server" enctype="multipart/form-data">
        <FTB:ImageGallery ID="ImageGallery1" JavaScriptLocation="ExternalFile" UtilityImagesLocation="ExternalFile"
            SupportFolder="~/assets/common/FreeTextBox/" runat="Server" />
    </form>
</body>
</html>
