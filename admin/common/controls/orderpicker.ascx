<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="orderpicker.ascx.cs" Inherits="netpoint.admin.common.controls.orderpicker" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<asp:TextBox id="txtOrderID" runat="server" Width="50px"></asp:TextBox>
<asp:ImageButton id="btnPop" runat="server" ImageUrl="~/assets/common/icons/popup.gif" ToolTip="Pop"></asp:ImageButton><br />
<asp:HyperLink ID="sysOrder" runat="server">
    <asp:Literal ID="ltlOrderType" runat="server" />#<asp:Literal ID="ltlOrderID" runat="server" />-[<asp:Literal ID="ltlAccountID" runat="server" />]-<np:PriceDisplay ID="prcGrandTotal" runat="server" />-<asp:Literal ID="ltlDate" runat="server" />
</asp:HyperLink>