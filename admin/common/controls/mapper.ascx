<%@ Control Language="c#" Inherits="netpoint.admin.common.controls.mapper" Codebehind="mapper.ascx.cs" %>
<asp:Panel runat="Server" id="pnlStep1" Visible="True">
	<table class="npadminbody" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td class="npadminsubheader">
				<asp:Image id="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif"></asp:Image>&nbsp; Import Data Mapper (Step 1/2)</td>
			<td class="npadminsubheader" align="right">
				<asp:imagebutton id="btnImport" onclick="ImportIt" runat="server" ImageUrl="~/assets/common/icons/upload.gif" ToolTip="Upload File and Continue"></asp:imagebutton>&nbsp;</td>
		</tr>
		<tr>
			<td class="npadminlabel">Source File:</td>
			<td><input id="file1" type="file" size="40" name="file1" /></td>
		</tr>
		<tr>
			<td class="npadminlabel">Starting Line Number:</td>
			<td><asp:textbox id="tbLineNumber" runat="server" EnableViewState="true" visible="true" text="0" MaxLength="4" columns="2"></asp:textbox></td>
		</tr>
		<tr>
			<td class="npadminlabel">Destination Table:</td>
			<td class="npadminbody">
			    <div id="formdiv1">
			        <asp:DropDownList id="ddlTable" runat="Server">
			            <asp:listitem Text="Prospects" value="Prospects" ></asp:listitem>
			            <asp:ListItem Text="Parts" Value="PartsMaster" Enabled="false" ></asp:ListItem>
			        </asp:DropDownList>
			    </div>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:label id="sysError" runat="server" CssClass="npwarning" visible="false"></asp:label>
<asp:Panel runat="Server" id="pnlStep2" Visible="False">
	<table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr>
			<td class="npadminsubheader">
				<asp:Image id="Image1" runat="server" ImageUrl="~/assets/common/icons/indicator.gif"></asp:Image>&nbsp;
				<asp:Label id="lblTableName" runat="Server"></asp:Label>&nbsp;Data Mapper (Step 2/2)
				<asp:Label id="lblFileName" Visible="False" runat="Server"></asp:Label></td>
			<td class="npadminsubheader" align="right">
				<asp:HyperLink id="lnkBack" ImageUrl="~/assets/common/icons/cancel.gif" NavigateUrl="javascript:history.go(-1);" Runat="server" ToolTip="Back"></asp:HyperLink>&nbsp;
				<asp:ImageButton id="ImportButton" onclick="ImportData" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" visible="true"></asp:ImageButton>&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<table id="dataCollectionTable" cellspacing="0" cellpadding="3" style="text-align:center;" border="0" runat="server"></table></td>
		</tr>
	</table>
</asp:Panel>
<asp:Literal ID="errFileType" runat="server" Text="Invalid File Type: Only CSV files are supported for Import." Visible="False"></asp:Literal>
