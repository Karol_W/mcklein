<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="npcontrol.ascx.cs" Inherits="netpoint.admin.common.controls.npcontrol" %>


<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlControlID" runat="server" Text="Control ID"></asp:Literal>
        </td>
        <td>
            <asp:TextBox ID="txtContolName" runat="server" Columns="55"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvControlName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtContolName">
                <asp:Image ID="imgW1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlNamespace" runat="server" Text="Namespace"></asp:Literal>
        </td>
        <td>
            <asp:TextBox id="txtNamespace" runat="server" Columns="55"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvNamespace" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtNamespace">
                <asp:Image ID="imgW2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPublicName" runat="server" Text="Public Name"></asp:Literal>
        </td>
        <td>
            <asp:textbox ID="txtPublicName" runat="server" Columns="55"></asp:textbox><asp:RequiredFieldValidator
                ID="rfvPublicName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtPublicName"><asp:Image ID="imgW3" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal>
        </td>
        <td>
            <asp:TextBox ID="txtDescription" runat="server" Columns="55"></asp:TextBox>
        </td>
    </tr>
</table>
