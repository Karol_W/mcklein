<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="projectpicker.ascx.cs" Inherits="netpoint.admin.common.controls.projectpicker" %>
<asp:TextBox id="txtProjectID" runat="server" Columns="6"></asp:TextBox>
<asp:Image id="btnPop" runat="server" ImageUrl="~/assets/common/icons/popup.gif"></asp:Image>
<asp:Literal ID="hdnProjectDoesNotExist" runat="server" Text="The project does not exist." Visible="false"></asp:Literal>
