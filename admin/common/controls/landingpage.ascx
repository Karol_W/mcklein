<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="landingpage.ascx.cs" Inherits="netpoint.admin.common.controls.landingpage" %>
<asp:Literal ID="sysMessage" runat="server"></asp:Literal>
<asp:Repeater ID="rptMenuItems" runat="server" OnItemDataBound="rptMenuItems_ItemDataBound">
    <ItemTemplate>
        <div>
            <asp:HyperLink runat="server" ID="lnkImage" ImageUrl="~/assets/common/icons/indicator.gif" Width="20px"></asp:HyperLink>
            <asp:HyperLink runat="server" ID="lnkText"></asp:HyperLink>
            <br />
            <asp:Label runat="server" ID="lblDescription"></asp:Label>
        </div>
        <br />
    </ItemTemplate>
</asp:Repeater>
<asp:Literal ID="ltlAdmin" runat="server" Text="Admin" Visible="false"></asp:Literal>