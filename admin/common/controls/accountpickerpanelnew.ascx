<%@ Control Language="C#" AutoEventWireup="true" Codebehind="accountpickerpanelnew.ascx.cs"
    Inherits="netpoint.admin.common.controls.accountpickerpanelnew" %>
<asp:Label runat="server" ID="errAccountIDRequired" CssClass="npwarning" Visible="false" Text="Account ID or Automatically Generate required" EnableViewState="false"></asp:Label>
<asp:Label runat="server" ID="errAccountExists" CssClass="npwarning" Visible="false" Text="Account ID Already Exists" EnableViewState="false"></asp:Label>
<asp:Label runat="server" ID="errUserExists" CssClass="npwarning" Visible="false" Text="User ID Already Exists" EnableViewState="false"></asp:Label>
<asp:Label runat="server" ID="errPasswordsDontMatch" CssClass="npwarning" Visible="false" Text="Passwords do not match" EnableViewState="false"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel" style="width:25%;">
            <asp:Literal ID="ltlAccountID" runat="server" Text="Account ID"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox>
            &nbsp;
            <asp:CheckBox runat="server" ID="cbGenerateAccount" Text="Automatically Generate" />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountName" runat="server" Text="Account Name"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtAccountName" runat="server" Columns="50"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvAccountName" runat="server" ControlToValidate="txtAccountName" ErrorMessage="RequiredFieldValidator">
                <asp:Image runat="server" ImageUrl="~/assets/common/icons/warning.gif" ID="Image2" />
            </asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAcctType" runat="server" Text="Account Type"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:DropDownList ID="ddlAccountType" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPrimaryPhone" runat="server" Text="Main Phone"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtPhone1" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="3" class="npadminsubheader">
            <asp:Label runat="server" ID="lblUser" Text="User Information (optional)"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlUserID" runat="server" Text="User ID"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"">
            <asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"">
            <asp:Literal ID="ltlFirstName" runat="server" Text="First Name"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtFirstname" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"">
            <asp:Literal ID="ltlMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlLastName" runat="server" Text="Last Name"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"">
            <asp:Literal ID="ltlSuffix" runat="server" Text="Suffix"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtSuffix" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlPassword" runat="server" Text="Password"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;
            <asp:CheckBox runat="server" ID="cbGenerateUserPassword" Text="Automatically Generate" />
            </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlConfirmPassword" runat="server" Text="Confirm Password"></asp:Literal></td>
        <td class="npadminbody" colspan="2">
            <asp:TextBox ID="txtPasswordConfirm" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr>
</table>