<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="adminbar.ascx.cs" Inherits="netpoint.admin.common.controls.adminbar" %>
<asp:Literal runat="server" ID="ltlAdminShortcuts" Text="My Shortcuts" EnableViewState="false"></asp:Literal>
<table width="100%">
<tr>
<td align="left">
<asp:Image runat="server" ID="imgAccount" ImageUrl="~/assets/common/icons/contactinfo.gif" Width="15" Height="15" ToolTip="Account"></asp:Image>
<asp:HyperLink runat="server" ID="lnkAccount" Text="Current Account"></asp:HyperLink>
</td>
<td align="left">
<asp:Image runat="server" ID="imgTheme" ImageUrl="~/assets/common/icons/slots.gif" Width="15" Height="15" ToolTip="Theme"></asp:Image>
<asp:HyperLink runat="server" ID="lnkTheme" Text="Current Theme"></asp:HyperLink>
</td>
</tr>
<tr>
<td align="left">
<asp:Image runat="server" ID="imgUser" ImageUrl="~/assets/common/icons/user.gif" Width="15" Height="15" ToolTip="User"></asp:Image>
<asp:HyperLink runat="server" ID="lnkUser" Text="Current User"></asp:HyperLink>
</td>
<td align="left">
<asp:Image runat="server" ID="imgCatalog" ImageUrl="~/assets/common/icons/catalog.gif" Width="15" Height="15" ToolTip="Catalog"></asp:Image>
<asp:HyperLink runat="server" ID="lnkCatalog" Text="Current Catalog"></asp:HyperLink>
</td>
</tr>
</table>
<asp:Literal runat="Server" ID="hdnNotAvailable" Text="N/A" Visible="false"></asp:Literal>