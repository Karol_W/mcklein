<%@ Page Language="c#" MasterPageFile="~/masters/default.master" Inherits="netpoint.admin.common.controls.FilePickerPanel" Codebehind="FilePickerPanel.aspx.cs" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">

    <script type="text/javascript">
		function populate(theText, theControl){
			window.opener.document.getElementById(theControl).value = theText;
			window.close();
			return false;
		}
    </script>

    <table width="100%" class="npadminbody" cellspacing="0" cellpadding="2">
        <tr>
            <td class="npadminlabel">
                <asp:HyperLink runat="server" ID="UpLevelLink" ImageUrl="~/assets/common/icons/uplevel.gif"
                    Visible="False" /></td>
            <td class="npadminheader">
                <asp:Label runat="server" ID="lblCurrentDirectory" /></td>
            <td class="npadminheader">
                <asp:CheckBox AutoPostBack="True" ID="cbShowImages" runat="server" Text="Show Images" checked="true" /></td>
        </tr>
        <tr>
            <td colspan="3" class="npadminlabel">
                <asp:DataList ID="DirectoryList" runat="server" CellSpacing="0" CellPadding="0" GridLines="Both"
                    BorderWidth="1" RepeatColumns="4" RepeatLayout="Table" CssClass="npadminlabel"
                    ItemStyle-VerticalAlign="Bottom" Width="100%" OnItemDataBound="DirectoryListDataBound">
                    <ItemTemplate>
                        <table class="npadminlabel" width="100%" cellspacing="0" cellpadding="5" border="0">
                            <tr>
                                <td valign="middle" align="center">
                                    <asp:HyperLink runat="server" ID="DirectoryLink" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataList ID="FilesList" runat="server" CellSpacing="0" CellPadding="0" GridLines="Both"
                    BorderWidth="1" RepeatColumns="4" RepeatLayout="Table" CssClass="npbody" ItemStyle-VerticalAlign="Bottom"
                    Width="100%" OnItemDataBound="FilesListDataBound">
                    <ItemTemplate>
                        <asp:HyperLink ID="ImageLink" runat="server" /><br />
                        <asp:HyperLink runat="server" ID="FileLink" />
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>
