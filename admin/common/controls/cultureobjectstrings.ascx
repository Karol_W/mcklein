<%@ Control Language="c#" Inherits="netpoint.admin.common.controls.CultureObjectStrings" Codebehind="CultureObjectStrings.ascx.cs" %>
<asp:Label ID="sysError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="Larger" Visible="False"></asp:Label>
<asp:gridview
    id="gvStrings"
    Allowpaging="true" 
    PageSize="20"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found"
    OnRowDataBound="gvStrings_RowDataBound"
    OnRowCommand="gvStrings_RowCommand"
    OnRowEditing="gvStrings_RowEditing"
    OnPageIndexChanging="gvStrings_PageIndexChanging">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelte" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "COSID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="sysVarChar">
            <ItemTemplate>
                <%# DataBinder.Eval (Container.DataItem, "DisplayName") %>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtDisplayName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DisplayName") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="sysText">
            <ItemTemplate>
                <%# DataBinder.Eval (Container.DataItem, "DisplayDescription") %>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtDisplayDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DisplayDescription") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Language|Language">
            <ItemTemplate>
                <%# DataBinder.Eval (Container.DataItem, "Language") %>
            </ItemTemplate>
            <EditItemTemplate>
                <div id="formdiv49"><asp:DropDownList ID="ddlEncoding" runat="server"></asp:DropDownList></div>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandName="edit" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancelthis" />
                <asp:ImageButton ID="imgSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="savethis" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "COSID") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:gridview>
<div style="text-align: right">
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add"></asp:ImageButton>&nbsp;&nbsp;
</div>
<asp:Literal ID="errTransalationAlreadyExists" runat="server" Text="translation already exists for this entry" Visible="False"></asp:Literal>
<asp:Literal ID="errDuplicateSameLanguageTranslation" runat="server" Text="Duplicate translations in the same language are not allowed" Visible="False"></asp:Literal>
