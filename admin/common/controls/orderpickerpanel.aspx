<%@ Page Language="C#" MasterPageFile="~/masters/admindefault.master" AutoEventWireup="true" CodeBehind="orderpickerpanel.aspx.cs" Inherits="netpoint.admin.common.controls.orderpickerpanel" %>

<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

    <script type="text/javascript">
		function populate(theText, theControl){
			window.opener.document.getElementById(theControl).value = theText;
			window.close();
			return false;
		}		
    </script>

<table class="npadmintable">
    <tr>
        <td class="npadminsubheader">
            &nbsp;
            <asp:Image ID="sysIndicator0" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
            &nbsp;
            <asp:Literal ID="ltlFitlers" runat="server" Text="Filters"></asp:Literal>
        </td>
        <td class="npadminsubheader" align="right" >
            <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="npadminbody" colspan="2">
            <np:NPSearch ID="search" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="npadminsubheader">
            &nbsp;
            <asp:Image ID="sysIncidator1" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
            &nbsp;
            <asp:Literal ID="ltlResults" runat="server" Text="Results"></asp:Literal>
        </td>
        <td class="npadminsubheader" align="right">
            <asp:imagebutton id="btnGo" runat="server" imageurl="~/assets/common/icons/search.gif" OnClick="btnGo_Click" ToolTip="Go" ></asp:imagebutton>&nbsp;
        </td>
    </tr>
</table>
    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Orders Match Criteria"
        AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound" OnPageIndexChanging="grid_PageIndexChanging" OnSorting="grid_Sorting">
                <HeaderStyle CssClass="npadminsubheader" />
                <RowStyle CssClass="npadminbody" />
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:TemplateField HeaderText="colID|Order ID">
                <ItemStyle HorizontalAlign="center" />
                <ItemTemplate>
                    <asp:HyperLink ID="lnkOrderID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderID") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="colAcount|Account ID" DataField="AccountID" SortExpression="AccountID" />            
            <asp:TemplateField HeaderText="colDetail|Detail">
                <ItemTemplate>
                    <asp:literal ID="ltlDetail" runat="server"></asp:literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colPrice|Price">
                <ItemStyle HorizontalAlign="right" />
                <ItemTemplate>
                    <np:PriceDisplay ID="prcPrice" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
    </asp:GridView>
</asp:Content>

<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnEmpty" runat="server" Text="No orders match this criteria" Visible="false"></asp:Literal>
</asp:Content>
