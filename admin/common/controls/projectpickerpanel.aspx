<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admindefault.Master" CodeBehind="projectpickerpanel.aspx.cs" Inherits="netpoint.admin.common.controls.projectpickerpanel" %>
<%@ Register TagPrefix="np" TagName="NPSearch" Src="~/common/controls/SearchBuilder.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <script type="text/javascript">
		function populate(theText, theControl){
			window.opener.document.getElementById(theControl).value = theText;
			window.close();
			return false;
		}		
    </script>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">                               
                &nbsp;
                <asp:Literal ID="ltlFitlers" runat="server" Text="Filters"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <np:NPSearch ID="search" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">                
                &nbsp;
                <asp:Literal ID="ltlResults" runat="server" Text="Results"></asp:Literal>
            </td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnGo" runat="server" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnGo_Click"></asp:ImageButton>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
                    Width="100%" AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound"
                    OnPageIndexChanging="grid_PageIndexChanging" OnRowCommand="grid_RowCommand" OnSorting="grid_Sorting"
                    EmptyDataText="No Projects Found">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:TemplateField HeaderText="colID|Project ID" ItemStyle-HorizontalAlign="center" SortExpression="SupportProjectID">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HyperLink id="lnkID" runat="server" text='<%# Bind("SupportProjectID") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                        <asp:TemplateField HeaderText="colName|Name" SortExpression="ProjectName">
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("ProjectName") %>' ></asp:HyperLink>                                
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:HyperLinkField HeaderText="Customer|Customer" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/accountinfo.aspx?accountid={0}&backpage=~/admin/support/projects.aspx"
                            SortExpression="AccountID" DataTextField="AccountID" />
                        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colStart|Start" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colEnd|End" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
                            <ItemTemplate>
                                <asp:Literal ID="ltlEndDate" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>