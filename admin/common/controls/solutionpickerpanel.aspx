<%@ Page MasterPageFile="~/masters/admindefault.master" Language="c#" Inherits="netpoint.admin.common.controls.SolutionPickerPanel" Codebehind="solutionpickerpanel.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">

    <script type="text/javascript">
		function populate(theText, theControl){
			window.opener.document.getElementById(theControl).value = theText;
			window.close();
			return false;
		}
		
    </script>

    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter"
                    OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />
                &nbsp;<asp:Literal runat="server" ID="ltlFilterHeader" Text="Filter" /></td>
            <td class="npadminsubheader" align="right">
                &nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" ToolTip="Reset" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder>
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults"
                    OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="true" />
                &nbsp;<asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" /></td>
            <td class="npadminsubheader" colspan="1" align="right">
                &nbsp;
                <asp:ImageButton runat="server" ID="btnFind" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnFind_Click" ToolTip="Find" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="gvResults" runat="server" Width="100%" PageSize="20" CssClass="npadminbody"
                    AllowPaging="True" AllowSorting="True" EmptyDataText="No Results Found" AutoGenerateColumns="False" OnRowDataBound="gvResults_RowDataBound" OnPageIndexChanging="gvResults_PageIndexChanging" OnSorting="gvResults_Sorting">
                                    <HeaderStyle CssClass="npadminsubheader" />
                <RowStyle CssClass="npadminbody" />
                <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <Columns>
                        <asp:TemplateField HeaderText="colID|ID" SortExpression="SolutionID">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkID" runat="server" NavigateUrl='<%# Eval("SolutionID", "SolutionDetail.aspx?SolutionID={0}") %>'
                                    Text='<%# Eval("SolutionID") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colSolution|Solution" SortExpression="SolutionName">
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("SolutionName") %>' NavigateUrl='<%# Bind("SolutionID","SolutionDetail.aspx?SolutionID={0}") %>'></asp:HyperLink>
                                <br />
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colInternal|Internal" SortExpression="InternalFlag">
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Image ID="imgInternal" runat="Server" ImageUrl="~/assets/common/icons/availableyes.gif"
                                    Visible='<%# Bind("InternalFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colActive|Active" SortExpression="ActiveFlag">
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Image ID="imgActive" runat="Server" ImageUrl="~/assets/common/icons/approvalgo.gif"
                                    Visible='<%# Bind("ActiveFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    
</asp:Content>
