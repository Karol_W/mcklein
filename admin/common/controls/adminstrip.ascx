<%@ Control Language="c#" Inherits="netpoint.admin.common.controls.AdminStrip" Codebehind="AdminStrip.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
    
<div id="AdminIcons" class="AdminIcons">
<asp:HyperLink ID="lnkHome" runat="server" ImageUrl="~/assets/common/icons/home.gif" NavigateUrl="~/default.aspx" ToolTip="Website Home"></asp:HyperLink>
<asp:HyperLink ID="lnkToday" runat="server" ImageUrl="~/assets/common/icons/today.gif" NavigateUrl="~/admin/focus/Today.aspx" ToolTip="Today"></asp:HyperLink>
<asp:HyperLink ID="lnkCalendar" runat="server" ImageUrl="~/assets/common/icons/event.gif" NavigateUrl="~/admin/focus/Calendar.aspx" ToolTip="Calendar"></asp:HyperLink>
<asp:HyperLink ID="lnkFavorites" runat="server" ImageUrl="~/assets/common/icons/favorites.gif" NavigateUrl="~/admin/common/accounts/Favorites.aspx" ToolTip="Favorites"></asp:HyperLink>
<asp:HyperLink ID="lnkActivity" runat="server" ImageUrl="~/assets/common/icons/appointment.gif" NavigateUrl="~/admin/focus/Activity.aspx?activityid=0" ToolTip="Activity"></asp:HyperLink>
<asp:ImageButton ID="btnStringEditor" runat="server" ImageUrl="~/assets/common/icons/language.gif" OnClick="btnStringEditor_Click" ToolTip="Toggle String Editor" />
<asp:HyperLink ID="lnkHelp" runat="server" ImageUrl="~/assets/common/icons/help.gif" NavigateUrl="" onclick="javascript:w= window.open('http://help.zedsuite.com/ecommerce.html','mywin','left=20,top=20,width=760,height=760,toolbar=0,resizable=0');"  ToolTip="Online Help"></asp:HyperLink>
</div>

 <ComponentArt:NavBar id="AdminNavBar" KeyboardEnabled="false"
      CssClass="NavBar" 
      DefaultItemLookID="TopItemLook"
      ExpandSinglePath="true"
      ImagesBaseUrl="~/assets/common/admin/"
      DefaultSelectedItemLookId="Level2SelectedItemLook"
      ClientScriptLocation="~/scripts"
      runat="server" >
    <ItemLooks>
      <ComponentArt:ItemLook LookID="TopItemLook" CssClass="TopItem" HoverCssClass="TopItemHover" ActiveCssClass="TopItemActive" />
      <ComponentArt:ItemLook LookID="Level2ItemLook" CssClass="Level2Item" HoverCssClass="Level2ItemHover" />
      <ComponentArt:ItemLook LookID="Level2SelectedItemLook" CssClass="Level2ItemSelected" HoverCssClass="Level2ItemSelected" />
    </ItemLooks>
    </ComponentArt:NavBar>
    <asp:Panel ID="pnlStringEditor" runat="server"></asp:Panel>
    <asp:PlaceHolder ID="phStringEditor" runat="server"></asp:PlaceHolder>