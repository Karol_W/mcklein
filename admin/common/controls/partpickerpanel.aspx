<%@ Register TagPrefix="np" TagName="machinesblock" Src="~/catalog/controls/MachinesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PartVariant" Src="~/catalog/controls/PartVariant.ascx" %>

<%@ Page MasterPageFile="~/masters/admindefault.master" Language="c#" Inherits="netpoint.admin.common.controls.PartPickerPanel" Codebehind="PartPickerPanel.aspx.cs" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">
    <table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="1">
        <tr>
            <td class="npadminlabel" width="25%">
                <asp:Literal ID="ltlKeyword" runat="server" Text="ltlKeyword"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox></td>
            <td class="npadminbody" align="right">
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/assets/common/icons/search.gif" ToolTip="Search">
                </asp:ImageButton></td>
        </tr>
        <!--<tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlFits" runat="server" Text="Fits" Visible="false"></asp:Literal></td>
            <td class="npadminbody" colspan="2">
                <np:machinesblock ID="mmy" runat="server" BorderWidth="0" Filter="true" FilterMake="true" Visible="false">
                </np:machinesblock>
            </td>
        </tr>-->
    </table>
    <br />
    <asp:gridview ID="gridResults" runat="server" AutoGenerateColumns="False" AllowSorting="True"
        AllowPaging="True" PageSize="20" CssClass="npadmintable" EmptyDataText="No Matching Items Found" OnPageIndexChanging="gridResults_PageIndexChanging" OnRowCommand="gridResults_RowCommand" OnRowDataBound="gridResults_RowDataBound" OnSorting="gridResults_Sorting">
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <PagerStyle CssClass="npadminbody" />
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:templatefield SortExpression="p.PartNo" HeaderText="ItemNo|Item No.">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkSelect" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNo") %>'>
                    </asp:HyperLink>
                    <asp:ImageButton ID="btnProdLine" runat="server" ImageUrl="~/assets/common/icons/search.gif"
                        CommandName="prodline"></asp:ImageButton>
                </ItemTemplate>
            </asp:templatefield>
            <asp:boundfield DataField="PartName" SortExpression="p.PartName" HeaderText="ItemName|Item Name">
            </asp:boundfield>
            <asp:boundfield DataField="PartType" SortExpression="p.PartType" HeaderText="ItemType|Item Type">
            </asp:boundfield>
            <asp:boundfield Visible="False" DataField="Description" SortExpression="p.PartDescription"
                HeaderText="colDescription|Description"></asp:boundfield>
            <asp:boundfield DataField="ManufacturerName"  HeaderText="colManufacturer|Manufacturer">
            </asp:boundfield>
            <asp:boundfield DataField="Inventory"  HeaderText="colInStock|In Stock">
            </asp:boundfield>
        </Columns>
    </asp:gridview>
    <asp:Literal ID="ltlNoRecordsFound" runat="server" Text="NoRecordsFound"></asp:Literal>
    <asp:Panel ID="pnlVariant" runat="server" Visible="False">
        <table id="tblVariant" cellspacing="0" cellpadding="1" width="100%" border="1">
            <tr>
                <td>
                    <np:PartVariant ID="pvt" runat="server"></np:PartVariant>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" align="center">
                    <asp:ImageButton ID="btnAddToCart" ImageUrl="../../../assets/common/icons/addtocart.gif"
                        runat="server" ToolTip="Add to Cart"></asp:ImageButton></td>
            </tr>
        </table>
        <asp:Literal ID="hdnNoSearch" runat="server" Text="Search requires at least two characters"
            Visible="False"></asp:Literal>
    </asp:Panel>
</asp:Content>
