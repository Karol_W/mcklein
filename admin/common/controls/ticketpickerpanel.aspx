<%@ Page Language="c#" MasterPageFile="~/masters/admindefault.master" Inherits="netpoint.admin.common.controls.TicketPickerPanel" Codebehind="TicketPickerPanel.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">

    <script type="text/javascript">
		function populate(theText, theControl){
			window.opener.document.getElementById(theControl).value = theText;
			window.close();
			return false;
		}
		
    </script>

    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter"
                    OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />
                &nbsp;<asp:Literal runat="server" ID="ltlFilterHeader" Text="Filter" /></td>
            <td class="npadminsubheader" align="right">
                &nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png"
                    OnClick="btnReset_Click" ToolTip="Reset" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder>
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults"
                    OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="true" />
                &nbsp;<asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" /></td>
            <td class="npadminsubheader" colspan="1" align="right">
                &nbsp;
                <asp:ImageButton runat="server" ID="btnFind" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnFind_Click" ToolTip="Find" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:GridView ID="gvResults" runat="server" Width="100%" PageSize="20" CssClass="npadminbody" EmptyDataText="No Tickets Found"
                    AllowPaging="True" AllowSorting="True"  AutoGenerateColumns="False" OnRowDataBound="gvResults_RowDataBound" OnPageIndexChanging="gvResults_PageIndexChanging" OnSorting="gvResults_Sorting">
                <HeaderStyle CssClass="npadminsubheader" />
                <RowStyle CssClass="npadminbody" />
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <Columns>
                        <asp:TemplateField HeaderText="colID|Ticket ID" SortExpression="TicketID">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkID" runat="server" NavigateUrl='<%# Eval("TicketID", "~/admin/support/ticketadmin.aspx?ticketid={0}") %>'
                                    Text='<%# Eval("TicketID") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colSubject|Subject" SortExpression="Subject">
                            <ItemTemplate>
                                <asp:Label ID="TicketType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketType") %>' />
                                <b>
                                    <asp:HyperLink runat="server" ID="lnkName" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.TicketID","~/admin/support/ticketadmin.aspx?ticketid={0}") %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>' /></b>
                                        <br />
                                                                        <asp:Label runat="server" ID="lbl" Text='<%# DataBinder.Eval(Container, "DataItem.UserEmailAddress") %>'></asp:Label>
                                 - <asp:Label runat="server" ID="Label2" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'></asp:Label>
                                        <br /><br />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colSubmitDate|Submit Date" SortExpression="SubmitDate, AssignedTo">
                            <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:Label ID="lblSubmitDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate","{0:yyyy-MMM-dd}") %>' />
                                <br />
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedUserID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    
</asp:Content>
