<%@ Control Language="c#" Inherits="netpoint.admin.common.controls.AdminMenu" Codebehind="AdminMenu.ascx.cs" %>
<link href="<%# VirtualPath%>admin/adminmenu.css" type="text/css" rel="stylesheet" />

<script src="<%# VirtualPath%>admin/adminmenu.js" type="text/javascript"></script>

<table cellspacing="0" cellpadding="0" width="99.3%">
    <tr>
        <td>
            <asp:Literal ID="ltlAdminMenu" runat="server" EnableViewState="False"></asp:Literal></td>
        <td>
            <asp:HyperLink runat="server" ID="lnkHelp" ImageUrl="~/assets/common/icons/help.gif"
                NavigateUrl="javascript:popUpHelp();" />
        </td>
    </tr>
</table>
<br />

<script type="text/javascript">
var WindowObjectReference; // global variable
function popUpHelp()
{
    WindowObjectReference = window.open("<%# VirtualPath%>admin/common/help/help.aspx?namespace=<%=PageNameSpace%>",
    "DescriptiveWindowName",
    "width=400,height=450,resizable,menubar=0,location=0,toolbar=0,personalbar=0,status=0,scrollbars=1");
}
</script>

