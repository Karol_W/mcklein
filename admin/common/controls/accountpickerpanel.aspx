<%@ Page MasterPageFile="~/masters/admindefault.master" Language="c#" Inherits="netpoint.admin.common.controls.AccountPickerPanel" Codebehind="AccountPickerPanel.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Src="~/admin/common/controls/accountpickerpanelnew.ascx" TagName="accountpickerpanelnew" TagPrefix="np" %>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <ComponentArt:TabStrip ID="AccountTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="AccountMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="tsSearch" Text="Search"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="tsNew" Text="New"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="AccountMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
        <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvSearch">
            <table id="theTable" class="npadmintable">
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlAccountID" runat="server" Text="Account"></asp:Literal></td>
                    <td class="npadminbody">
                        <asp:TextBox ID="txtAccount" runat="server"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/assets/common/icons/search.gif" CausesValidation="false" OnClick="btnSearch_Click" ToolTip="Search"></asp:ImageButton></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlAccountType" runat="server" Text="Account Type"></asp:Literal></td>
                    <td class="npadminbody"><asp:DropDownList ID="ddlAccountType" runat="server"></asp:DropDownList></td>
                </tr>
            </table>
            <asp:gridview
                id="gvPicker"
                runat="server" 
                AutoGenerateColumns="false" 
                HeaderStyle-CssClass="npadminsubheader"
                CssClass="npadmintable" 
                RowStyle-CssClass="npadminbody" 
                AlternatingRowStyle-CssClass="npadminbodyalt" 
                EmptyDataText="No Records Found" OnRowDataBound="gvPicker_RowDataBound">
                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                <Columns>
                    <asp:BoundField DataField="AccountID" HeaderText="colID|ID"></asp:BoundField>
                    <asp:TemplateField HeaderText="ltlName|Name">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkSelect" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountName")%>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="City" HeaderText="colCity|City" Visible="false"></asp:BoundField>
                    <asp:BoundField DataField="State" HeaderText="colState|State"></asp:BoundField>
                    <asp:BoundField DataField="Country" HeaderText="colCountry|Country"></asp:BoundField>
                </Columns>
            </asp:gridview>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvNew">
            <table class="npadmintable">
                <tr>
                    <td colspan="2" class="npadminsubheader"><asp:Label runat="server" ID="lblAccount" Text="Account Information"></asp:Label></td>
                    <td class="npadminsubheader" align="right"><asp:ImageButton runat="server" ID="btnSaveNewAccount" 
                        ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveNewAccount_Click" ToolTip="Save New Account" />&nbsp;</td>
                </tr>
            </table>
            <np:accountpickerpanelnew ID="accountpickerpanelnew" runat="server"></np:accountpickerpanelnew>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
