<%@ Control Language="c#" Inherits="netpoint.admin.common.controls.AccountPicker"
    Codebehind="AccountPicker.ascx.cs" %>
    
<asp:Panel ID="pnlAcctPicker" runat="server" DefaultButton="btnPop">
    <asp:TextBox ID="txtAccountID" runat="server" Width="100px" OnTextChanged="txtAccountID_TextChanged"></asp:TextBox>&nbsp;
    <asp:ImageButton ID="btnPop" runat="server" ImageUrl="~/assets/common/icons/popup.gif"
        ToolTip="Search for Customer"></asp:ImageButton>&nbsp;
    <asp:HyperLink ID="sysAccountName" runat="server"></asp:HyperLink>
    <asp:RequiredFieldValidator ID="rfv" runat="server" ErrorMessage="RequiredFieldValidator"
        ControlToValidate="txtAccountID" Enabled="false">
        <asp:Image ID="imgWarn" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
</asp:Panel>
