<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.common.templates.templateeditor" Codebehind="templateeditor.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="SaveButton_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tbody>
                <tr><td colspan="3" class="npadminsubheader">&nbsp;</td></tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal>&nbsp;</td>
                    <td colspan="2">
                        <asp:TextBox EnableViewState="true" ID="TemplateName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="val_Name" ControlToValidate="TemplateName"
                            ErrorMessage="Name is a required field." CssClass="errormsg" Display="Static">
                            <asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlServerID" runat="server" Text="ServerID"></asp:Literal></td>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td><asp:DropDownList ID="ddlServerID" runat="server" EnableViewState="true"></asp:DropDownList></td>
                                <td><span style="height:25px; width:1px;">&nbsp;</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlTemplateCode" runat="server" Text="Template Code"></asp:Literal>&nbsp;</td>
                    <td colspan="2">
                        <asp:TextBox EnableViewState="true" ID="TemplateCode" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="pcval" ControlToValidate="TemplateCode"
                            ErrorMessage="TemplateCode is a required field." CssClass="errormsg" Display="Static">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlMailFrom" runat="server" Text="From"></asp:Literal>&nbsp;</td>
                    <td colspan="2"><asp:TextBox EnableViewState="true" ID="MailFrom" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlMailBCC" runat="server" Text="BCC"></asp:Literal>&nbsp;</td>
                    <td colspan="2"><asp:TextBox EnableViewState="true" ID="MailBCC" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlMailSubject" runat="server" Text="Subject"></asp:Literal>&nbsp;</td>
                    <td colspan="2"><asp:TextBox EnableViewState="true" ID="MailSubject" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" class="npadminlabel"><asp:TextBox ID="Template" runat="server" TextMode="MultiLine" Columns="72" Rows="20" Wrap="False" /></td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
</asp:Content>