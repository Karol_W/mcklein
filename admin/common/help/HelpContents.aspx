<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true"
    Codebehind="HelpContents.aspx.cs" Inherits="netpoint.admin.common.help.HelpContents"
    Title="Untitled Page" %>

<asp:Content ID="Content3" ContentPlaceHolderID="mainslot" runat="server">

    <script type="text/javascript">
function JpopUpHelp(newURL)
{
win = window.open("","HelpTopics","width=400,height=450,resizable,menubar=0,location=0,toolbar=0,personalbar=0,status=0,scrollbars=1");
  win.location.href = newURL;
win.focus();
}
    </script>

    &nbsp;
    <br />
    <br />
    <p>
        <strong>Online Help Table of Contents</strong></p>
    <p>
        This page contains links to online help topics in zed eCommerce. You can access the same help information while working in the application
        by clicking the <em>Help</em> <strong>icon
            <asp:Image ID="imgHelp" runat="server" ImageUrl="~/assets/common/icons/help.gif"
                Width="16" Height="15" />
        </strong>&nbsp;from any window.
    </p>
    <p>
        Online help is intended for administrators and other users who are responsible for
        setting up and managing Web sites and the sales and service (CRM) components.
    </p>
    <p>
        <strong>Note: There is not a separate manual for customers and other users of Web sites</strong>.</p>
    <p>
        The help links are grouped according to the menu items that display on the left
        side of the Administration section of the Web site.
    </p>
    <p>
        <strong>Note</strong>: To print a help topic, right-click in the topic window and
        click <em>Print</em>.
    </p>
    <strong>My Focus</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus._default');">
            About My Focus Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.Activity');">
            Create Activity</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.dashboard.ActivityDetail');">
            Activity Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.dashboard.Today');">Today(Calendar)</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.dashboard.Calendar');">
            Calendar</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.dashboard.sales');">Sales
            View</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.dashboard.service');">Service
            View</a>
        <br />
    </p>
    <strong>Business Partners</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help/help.aspx?namespace=netpoint.admin.common.accounts._default');">
            About Business Partner Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.Favorites');">
            Favorite Business Partners</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.AccountNew');">
            Create Business Partners</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.accounts');">
            Business Partner Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.AccountInfo');">
            Business Partner Details</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.users');">
            User Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.UserInfo');">
            User Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.accounts.Roles');">
            Licensed Users</a>
    </p>
    <strong>Sales</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce._default');">
            About Sales Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.opportunities');">
            Opportunity Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.opportunityadd');">
            Create Opportunity</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OpportunityDetail');">
            Opportunity Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.quotes');">
            Quote Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.quoteadd');">
            Create Quote</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.orders');">
            Sales Order Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.focus.orderadd');">
            Create Sales Order</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OrderList');">
            Shipping Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.Shipment');">
            Deliveries</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.ShipmentCreate');">
            Create Delivery</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.prices');">
            Price Lists</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.PriceAdjustmentDetail');">
            Price List Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.Price');">
            Edit Price List</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OrderDiscounts');">
            Coupon Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OrderDiscountAdd');">
            Create Coupon</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OrderDiscount');">
            Coupon Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.Reports');">
            Reports</a>
    </p>
    <strong>Support </strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.support._default');">
            About Support Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.projects');">
            Projects Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.project');">
            Create a Project</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.projectadd');">
            Project Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.Solutions');">
            Knowledge Base Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.SolutionDetail');">
            Knowledge Base</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.tasks');">
            Task Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.taskadd');">
            Create Task</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.task');">
            Task Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.tickets');">
            Support Ticket Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.TicketAdd');">
            Create Support Ticket</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.ticketAdmin');">
            Support Ticket Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.workorders');">
            Service Call Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.workorderadd');">
            Create Service Call</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.workorder');">
            Service Call Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.contracts');">
            Contract Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.contractadd');">
            Create Contract</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.ContractDetail');">
            Contract Detail</a>
    </p>
    <strong>Catalog</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog._default');">
            About Catalog Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('Catalog%20and%20Category%20Component.htm');">AboutCatalogs</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.CatalogNew');">
            Create Catalog</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Catalog');">
            Catalog Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('Catalog%20Maintenance%20PROCESS.htm');">Catalog Maintenance</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.CatalogList');">
            Catalog Browser</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.parts');">
            Item Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.PartNew');">
            Create Items</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Part');">
            Item Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.CategoryNew');">
            Create Category</a>
        <br />
        <a href="Javascript:JpopUpHelp('Category%20Details%20Window%20OBJECT.htm');">CategoryDetails</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Category');">
            Maintaining Category Details</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.PartList');">
            Category Items</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Variants');">
            Variants</a> <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.PartVariant');">
                Variant Configuration</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.variant');">
            Variant Configuration Setup</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.manufacturers');">
            Create Manufacturer</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Manufacturer');">
            Manufacturer Detail</a>
    </p>
    <strong>Prospecting</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects._default');">
            About Prospecting Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.prospects');">
            Prospect Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.ProspectDetail');">
            Prospect Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Duplicates');">
            Duplicate Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.DuplicateDetail');">
            Duplicate Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Import');">
            Import Prospects</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.Reports');">
            Prospect Reports</a>
    </p>
    <strong>Marketing </strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.campaignhome');">
            About Marketing Campaign Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.Campaigns');">
            Marketing Campaign Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.campaignadd');">
            Create Marketing Campaign</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.Campaigns');">
            Marketing Campaign</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.CampaignDetail');">
            Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.Lists');">
            Contact List Search</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.prospects.ListAdd');">
            Create Contact List</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.ListDetail');">
            Contact List Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.ads._default');">
            Banner Ads</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.ads.addetail');">
            Banner Ad Details</a>
    </p>
    <strong>Layout</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.layout');">
            About Layout Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.themeslandingpage');">
            About Themes and Slots</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.themeadd');">
            Create Theme</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.themedetail');">
            Theme Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.themeupload');">
            Theme Upload</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.thememasterdetail');">
            Theme Master Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.themes.themeslotdetail');">
            Slot Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.pages.pageslandingpage');">
            Static Pages</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.pages.pageeditor');">
            Create Static Page</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.media.mediamainpage');">
            Media Browser</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.reports.ReportList');">
            Reports Builder</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.reports.Report');">
            Create Reports</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.reports.ReportGraphLayer');">
            Report Info</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.reports.reportcopy');">
            Copy Reports</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.menus.MenuEdit');">
            Menu Builder</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.templates.templateslandingpage');">
            E-mail Templates</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.templates.templateeditor');">
            E-mail Template Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.help.Import');">
            Online Help Manager</a>
    </p>
    <strong>Setup</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.setup');">
            About Setup Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.OrderStages');">
            Opportunity Stages</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.orderstage');">
            Create Opportunity Stages</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.contracttemplates');">
            Contract Templates</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.support.contracttemplateadd');">
            Create Contract Template</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.contracttemplate');">
            Contract Template Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.supportsections');">
            Support Queues</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.support.sectionadd');">
            Create Support Queue</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.support.supportsectiondetail');">
            Support Queue Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.support.attributedef');">
            Support Ticket Attributes</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.Warehouse');">
            Warehouses</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.WarehouseDetail');">
            Warehouse Detail</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.orderexpenses');">
            Expenses</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.Rates');">
            Expense Rates</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.rateadd');">
            Create Rates</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.rate');">
            Rate Definition</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.catalog.PartIndex');">
            Search Indexes</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.TaxList');">
            Taxes</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.Tax');">
            Tax Details</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.orderexpensetax');">
            Tax on Expenses</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.webconfig.webconfigs');">
            Web Configuration</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.codes.UnitList');">
            Configuring Units</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.webconfig.ccgatewayconfig');">
            Credit Card Gateways</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.PaymentOptions');">
            Payment Options</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.PaymentOptionDetail');">
            Payment Option Details</a>
    </p>
    <strong>Definitions</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.definitions');">
            About Definition Functions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.activities.activitytypes');">
            Activity Types</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.codes.codes');">
            Code Definitions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.codes.CountryList');">
            Country Definitions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.codes.StateList');">
            State Definitions</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.resources.resource');">
            Resources</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.resources.ResourceList');">
            Resource List</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.security.SecurityRoles');">
            Security Roles</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.security.securityroleadd');">
            Create Security Role</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.StringEditor');">
            Language Strings</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.common.StringLoad');">
            Import/Export Strings</a>
        <br />
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.commerce.currencies');">
            Currencies</a>
    </p>
    <strong>Help</strong>
    <p>
        <a href="Javascript:JpopUpHelp('help.aspx?namespace=netpoint.admin.errorlog');">Application
            Log</a>
    </p>    
        <strong>General Help Topics</strong> 
    <p>
        <a href="Javascript:JpopUpHelp('Filter%20FUNCTION.htm');">
            About Filters</a>
        <br />
        <a href="Javascript:JpopUpHelp('Code%20OBJECT.htm');">About Codes</a>
        <br />
        <a href="Javascript:JpopUpHelp('Synch%20OBJECT_BJM.htm');">About Synchronization</a>
    </p>
    <p>
        <br />
        <br />
    </p>
    <asp:Literal ID="sysMessage" runat="server"></asp:Literal>
</asp:Content>
