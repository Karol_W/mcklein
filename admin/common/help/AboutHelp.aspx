<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true"
    Codebehind="AboutHelp.aspx.cs" Inherits="netpoint.admin.common.help.AboutHelp"
    Title="About Online Help" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">

    <script type="text/javascript">
        function JpopUpHelp(newURL)
        {
        win = window.open("","HelpTopics","width=400,height=450,resizable,menubar=0,location=0,toolbar=0,personalbar=0,status=0,scrollbars=1");
          win.location.href = newURL;
        win.focus();
        }
    </script>

    <br />
    <br />
      <p><strong>About Online Help</strong></p>
      <p>Online help for zed eCommerce contains information you need to configure and maintain Web sites. Online help   is intended for administrators and other users who are responsible for setting   up and managing Web sites and the sales and service (CRM) components.</p>
      <p><strong>Note: <strong>There is not a separate manual for customers and other   users of Web sites</strong></strong>.</p>
      <p><strong>Accessing Online Help</strong></p>
      <p>Click the <strong><em>Help</em></strong> icon <strong>
        <asp:Image ID="imgHelp" runat="server" ImageUrl="~/assets/common/icons/help.gif" AlternateText="Help Icon" Width="16" Height="15" />        
      </strong> on any window to see   context-specific information for that window. A separate browser window displays   the help. Note that the help window may display behind the application window.   You may need to select the help window from the Internet Explorer task tray to   ensure it displays on top of the application window.</p>
    <asp:Literal ID="sysMessage" runat="server"></asp:Literal>
<br />
<br />

</asp:Content>
