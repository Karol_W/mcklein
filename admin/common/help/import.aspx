<%@ Page MasterPageFile="~/masters/admin.Master" Language="c#" Inherits="netpoint.admin.common.help.Import" Codebehind="Import.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="3"></td>
            </tr>
            <tr>
                <td class="npadminlabel" style="width:35%"><asp:Literal ID="ltlMakeHelpFile" runat="server" Text="Create and download help file archive"></asp:Literal></td>
                <td colspan="1"></td>
                <td align="left" colspan="2"><asp:ImageButton ID="MakeHelpButton" runat="server" ImageUrl="~/assets/common/icons/compress.gif"
                    ToolTip="Create and download help file archive"></asp:ImageButton></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlUploadFromArchive" runat="server" Text="Create Help File Archive"></asp:Literal></td>
                <td><input id="UploadFile" type="file" name="UploadFile" runat="server" /></td>
                <td><asp:ImageButton ID="UploadHelpButton" runat="server" ImageUrl="~/assets/common/icons/upload.gif"
                    ToolTip="Create Help File Archive"></asp:ImageButton></td>
            </tr>
            <tr>
                <td class="npadminlabel" ><asp:Literal ID="ltlThemesList" runat="server" Text="Import uploaded help file archive"></asp:Literal></td>
                <td><asp:DropDownList ID="HelpLists" runat="server"></asp:DropDownList></td>
                <td><asp:ImageButton ID="LoadHelpButton" runat="server" ImageUrl="~/assets/common/icons/import.gif"
                        ToolTip="Import uploaded help file archive"></asp:ImageButton></td>
            </tr>
        </table>
    </div>
<asp:Literal ID="errOverwrite" runat="server" Text="Overwrite Existing Help?" Visible="false"></asp:Literal>
</asp:Content>
