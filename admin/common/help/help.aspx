<%@ Page ValidateRequest="false" MasterPageFile="~/masters/default.master" Language="c#" Inherits="netpoint.admin.common.help.Help" Codebehind="Help.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<script language="JavaScript" type="text/javascript">
<!--
self.moveTo(screen.availWidth-400,0);
self.resizeTo(400,screen.availHeight);
-->
</script>

<asp:Panel runat="server" ID="pnlView">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="npadminheader">&nbsp;<asp:Literal ID="ltlHelpTitle" runat="server" Text="HelpTitle"></asp:Literal></td>
            <td class="npadminheader" align="right" style="width:25%"><asp:ImageButton ID="btnEdit" runat="server" Visible="False" ToolTip="Edit" ImageUrl="~/assets/common/icons/edit.gif"></asp:ImageButton>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2">
                <asp:DataList ID="lstEncodings" runat="server" CssClass="npadminlabel" RepeatLayout="Table" RepeatColumns="5" BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="lnkPage" />&nbsp;
                    </ItemTemplate>
                </asp:DataList></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="npadminbody" cellspacing="0" cellpadding="3" width="100%">
                    <tr>
                        <td><asp:Literal ID="ltlHelpText" runat="server" Text="HelpText"></asp:Literal><br /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="pnlEdit" Visible="False">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="npadminheader">&nbsp;
                <asp:ImageButton ID="btnCancel" runat="server" ToolTip="Cancel" ImageUrl="~/assets/common/icons/cancel.gif"></asp:ImageButton>&nbsp;
                <asp:TextBox ID="tbHelpTitle" runat="server" CssClass="npadminbody" Columns="40"></asp:TextBox></td>
            <td class="npadminheader" align="right" style="width:25%">
                <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif"></asp:ImageButton>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2">
                <asp:DropDownList ID="ddlEncoding" runat="server" CssClass="npadminbody" AutoPostBack="True" OnSelectedIndexChanged="ddlEncoding_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="npadminbody" cellspacing="0" cellpadding="3" width="100%">
                    <tr>
                        <td>
                            <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Content>
