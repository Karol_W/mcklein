<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/accounts/controls/UserGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="communication" Src="~/admin/common/accounts/controls/UserCommunication.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.UserNew" Codebehind="UserNew.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close" />&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="2"><asp:imagebutton runat="server" imageurl="~/assets/common/icons/indicator.gif" id="imgIndicator" tooltip=" " />&nbsp;
                <asp:literal runat="server" id="ltlGeneralHeader" text="General" /></td>
            <td class="npadminsubheader" colspan="1" align="right" style="white-space:nowrap;">&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><np:general ID="general" runat="server"></np:general></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2">
                <asp:imagebutton runat="server" imageurl="~/assets/common/icons/indicator.gif" id="imgIndCommunication" tooltip=" " />&nbsp;
                <asp:literal runat="server" id="ltlCommunicationHeader" text="communication" /></td>
            <td class="npadminsubheader" colspan="1" align="right" style="white-space:nowrap;">&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><np:communication ID="communication" runat="server"></np:communication></td>
        </tr>
    </table>
</asp:Content>
