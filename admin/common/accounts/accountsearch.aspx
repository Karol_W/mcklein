<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.AccountSearch" Codebehind="AccountSearch.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td align="right" class="npadminheader"><asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search"></asp:ImageButton></td>
        </tr>
    </table>
    <asp:Panel ID="pnlSearch" runat="server">
        <table class="npadmintable">
            <tr>
                <td class="npadminlabel" style="WIDTH:20%"><asp:Literal ID="ltlAccountType" runat="server" Text="Account Type"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlAccountType" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtAccount" runat="server" Columns="40"></asp:TextBox>
                    <asp:CheckBox ID="cbInactiveAccounts" runat="server" Text="Inactive Business Partners"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlUser" runat="server" Text="User"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtUser" runat="server" Columns="40"></asp:TextBox>
                    <asp:CheckBox ID="cbInactiveUsers" runat="server" Text="Inactive Users"></asp:CheckBox></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlResults" runat="server" Visible="False">
        <asp:GridView ID="gridResults" runat="server" AutoGenerateColumns="False" EmptyDataText="No Results Found"
            CssClass="npadmintable" PageSize="50" AllowPaging="True" AllowSorting="true" OnPageIndexChanging="gridResults_PageIndexChanging" OnRowDataBound="gridResults_RowDataBound" OnSorting="gridResults_Sorting">
            <RowStyle CssClass="npadminbody" />
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <PagerStyle CssClass="npadminbody" />
            <Columns>
                <asp:TemplateField HeaderText="Customer|Customer" SortExpression="AccountID">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkAccountID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Customer Name|Customer Name" SortExpression="AccountName">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkAccountName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccountName") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="State" HeaderText="colState|State" SortExpression="State,City">
                </asp:BoundField>
                <asp:BoundField DataField="CountryAbbr" HeaderText="colCountry|Country" SortExpression="CountryAbbr,State"  />
                <asp:TemplateField HeaderText="colName|Name" SortExpression="LastName, FirstName">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkUserID" runat="server" Visible="True" />
                        -
                        <%# DataBinder.Eval(Container, "DataItem.FirstName") %>
                        <%# DataBinder.Eval(Container, "DataItem.LastName") %>
                        <br>
                        <asp:Literal ID="ltlEmail" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="Email">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEmail" runat="server" Visible="False" ImageUrl="~/assets/common/icons/send.gif" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
