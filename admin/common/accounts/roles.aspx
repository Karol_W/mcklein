<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.Roles" Codebehind="Roles.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Licensed WebCRM Users</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel">
                <asp:Literal runat="server" ID="ltlCurrentLicensedUsers" Text="Current Licensed Users:" />
                <asp:Label runat="server" ID="TotalCurrentLicensedUsers" />
                /
                <asp:Literal runat="server" ID="ltlAvailableLicensedUsers" Text="Total User Licenses:" />
                <asp:Label runat="server" ID="TotalAvailableLicensedUsers" />
            </td>
        </tr>
    </table>
    <asp:GridView 
        id="gvRoles"
        runat="server" 
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        EmptyDataText="No Licensed Users Found" OnRowCommand="gvRoles_RowCommand" OnRowDataBound="gvRoles_RowDataBound">
	    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	    <Columns>
	        <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="UserInfo.aspx?userid={0}&backpage=~/admin/common/accounts/Roles.aspx"
                DataTextField="UserID" HeaderText="colUserID|User ID" SortExpression="UserID"></asp:HyperLinkField>
            <asp:TemplateField HeaderText="colName|Name">
                <ItemTemplate>
                    <asp:Literal runat="server" ID="ltlFullName" />                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colRoles|Roles">
                <ItemTemplate>
                    <asp:Literal runat="server" ID="ltlRoles" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <ItemStyle HorizontalAlign = "center" />
                <ItemTemplate>
                    <asp:ImageButton ID="btnRemove" runat="server" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' ImageUrl="~/assets/common/icons/remove.gif" />
                </ItemTemplate>
            </asp:TemplateField>
	    </Columns>
	</asp:GridView>
</asp:Content>
