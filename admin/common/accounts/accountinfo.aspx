<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" ValidateRequest="false" Inherits="netpoint.admin.common.accounts.AccountInfo" Codebehind="AccountInfo.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="resources" Src="~/admin/common/accounts/controls/Resources.ascx" %>
<%@ Register TagPrefix="np" TagName="contracts" Src="~/admin/common/accounts/controls/AccountContracts.ascx" %>
<%@ Register TagPrefix="np" TagName="childaccounts" Src="~/admin/common/accounts/controls/ChildAccounts.ascx" %>
<%@ Register TagPrefix="np" TagName="taxes" Src="~/admin/common/accounts/controls/Taxes.ascx" %>
<%@ Register TagPrefix="np" TagName="salespeople" Src="~/admin/common/accounts/controls/Salespeople.ascx" %>
<%@ Register TagPrefix="np" TagName="people" Src="~/admin/common/accounts/controls/People.ascx" %>
<%@ Register TagPrefix="np" TagName="accountaddressesblock" Src="~/common/controls/AccountAddressesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="financials" Src="~/admin/common/accounts/controls/AccountFinancials.ascx" %>
<%@ Register TagPrefix="np" TagName="communication" Src="~/admin/common/accounts/controls/AccountCommunication.ascx" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/accounts/controls/AccountGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="logs" Src="~/admin/prospects/controls/logs.ascx" %>
<%@ Register TagPrefix="np" TagName="tickets" Src="~/admin/focus/controls/tickets.ascx" %>
<%@ Register TagPrefix="np" TagName="activities" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="np" TagName="carts" Src="~/admin/common/accounts/controls/Carts.ascx" %>
<%@ Register TagPrefix="np" TagName="invoices" Src="~/admin/common/accounts/controls/Invoices.ascx" %>
<%@ Register TagPrefix="np" TagName="receivables" Src="~/admin/common/accounts/controls/Receivables.ascx" %>
<%@ Register TagPrefix="np" TagName="orders" Src="~/admin/focus/controls/orders.ascx" %>
<%@ Register TagPrefix="np" TagName="quotes" Src="~/admin/focus/controls/quotes.ascx" %>
<%@ Register TagPrefix="np" TagName="opportunities" Src="~/admin/focus/controls/opportunities.ascx" %>
<%@ Register TagPrefix="np" TagName="servicecalls" Src="~/admin/focus/controls/workorders.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Business Partner Details - <asp:Literal ID="ltlAccountTitle" runat="server" Text="Account"></asp:Literal></title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">    
    
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right" style="white-space:nowrap">
                &nbsp;<asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal>&nbsp;
                <asp:ImageButton runat="server" ID="btnFavorites" OnClick="btnFavorites_Click" ImageUrl="~/assets/common/icons/favorites.gif" ToolTip="Favorites" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btnDeleteGeneral" OnClick="btnDeleteGeneral_Click" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSave" OnClick="btnSave_Click" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSaveAll" OnClick="btnSaveAll_Click" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="AccountTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="AccountMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsAccountInfo" Text="Account Info" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsCommunication" Text="Communication"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsFinancials" Text="Financials"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsResources" Text="Resources"></ComponentArt:TabStripTab>                    
                    <ComponentArt:TabStripTab runat="server" ID="tstaxes" Text="Taxes"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsOrganization" Text="Organization"
                    SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsPeople" Text="People"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsSalesPeople" Text="Sales People"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsLocations" Text="Locations"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsChildren" Text="Child Accounts"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsActivities" Text="Activities" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsEvents" Text="Events"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsLogs" Text="Logs"></ComponentArt:TabStripTab>                    
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTransactions" Text="Transactions"
                    SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsOrders" Text="Orders"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsOpportunities" Text="Opportunities"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsQuotes" Text="Quotes"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsInvoices" Text="Invoices"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsReceivables" Text="Receivables"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsCarts" Text="Carts"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsService" Text="Service"
                    SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsContracts1" Text="Contracts"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsServiceCall" Text="Service Call"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsTickets" Text="Tickets"></ComponentArt:TabStripTab>                    
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="AccountMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:general ID="general" runat="server"></np:general>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvCommunication">
                <np:communication ID="communication" runat="server"></np:communication>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" id="financials" runat="server">
                <np:financials ID="npfinancials" runat="server"></np:financials>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:resources ID="resources" runat="server"></np:resources>
            </ComponentArt:PageView>            
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:taxes ID="taxes" runat="server"></np:taxes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:people ID="people" runat="server"></np:people>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:salespeople ID="salespeople" runat="server"></np:salespeople>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:accountaddressesblock ID="accountaddressesblock" runat="server" AdminMode="true"></np:accountaddressesblock>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:childaccounts ID="childaccounts" runat="server"></np:childaccounts>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:activities ID="activitytab" runat="server"></np:activities>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:logs ID="logs" runat="server"></np:logs>
            </ComponentArt:PageView>            
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:orders ID="orders" runat="server"></np:orders>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvopportunities">
                <np:opportunities ID="opportunities" runat="server"></np:opportunities>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:quotes ID="quotes" runat="server"></np:quotes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:invoices ID="invoices" runat="server"></np:invoices>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:receivables ID="receivables" runat="server"></np:receivables>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:carts ID="carts" runat="server"></np:carts>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:contracts ID="contracts" runat="server"></np:contracts>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:servicecalls ID="servicecalls" runat="server"></np:servicecalls>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:tickets ID="tickets" runat="server"></np:tickets>
            </ComponentArt:PageView>            
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteAccount" runat="server" Text="The following items will be deleted: \n - All Child Accounts \n - Their Users \n - Their User Resources \n - Their Addresses \n - All Account Users \n - All Account User Resources \n - All Account Addresses \n Continue?" Visible="False"></asp:Literal>
</asp:Content>
