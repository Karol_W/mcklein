<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.accounts.accounts" CodeBehind="accounts.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Business Partners</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="accountnew.aspx" ToolTip="New" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader"> 
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter" OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter" OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or Edit a Filter Set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right" style="white-space:nowrap">&nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults" OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults" OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" Visible="false" /></td>
            <td class="npadminsubheader" colspan="1" align="right" style="white-space:nowrap">&nbsp;
                <asp:ImageButton runat="server" ID="btnFind" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnFind_Click" ToolTip="Find" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
            <asp:Panel runat="server" ID="pnlResults" Visible="false">
                 <ComponentArt:Grid id="gvResults" DataAreaCssClass="Grid"
                            ClientObjectId="grid"
                            ClientScriptLocation="~/scripts"
                            AutoFocusSearchBox="false"
                            AutoTheming="true"
                            RunningMode="Callback"
                            ShowHeader="true" 
                            ShowFooter="true"
                            ShowSearchBox="true"
                            SearchOnKeyPress="true"
                            PageSize="20" 
                            PagerStyle="Numbered" 
                            FooterCssClass="GridFooter"
                            HeaderCssClass="GridHeader"
                            SliderPopupOffsetX="72"
                            GroupingPageSize="5"
                            ImagesBaseUrl="~/assets/common/admin/"
                            PagerImagesFolderUrl="~/assets/common/admin/pager/" 
                            LoadingPanelEnabled="false"
                            GroupingNotificationText="Drag a column to this area to group by it."
                            runat="server"
                            AutoAdjustPageSize="true" 
                            AutoSortOnGroup="true" 
                            Width="100%"
                            CollapseImageUrl="~/assets/common/admin/topItem_col.gif" 
                            ExpandImageUrl="~/assets/common/admin/topItem_exp.gif" 
                            GroupingCountHeadingsAsRows="false" 
                            AllowColumnResizing="true">
                        <Levels>
                          <ComponentArt:GridLevel DataKeyField="PartNo" 
                                AllowReordering="true"   
                                ColumnGroupIndicatorImageUrl=""  
                                RowCssClass="Row" 
                                ShowSelectorCells="true" 
                                AllowGrouping= "true" 
                                SortAscendingImageUrl="~/assets/common/admin/asc.gif" 
                                SortDescendingImageUrl="~/assets/common/admin/desc.gif" 
                                HeadingTextCssClass="HeadingCellText" 
                                HeadingCellCssClass="HeadingCell" 
                                DataCellCssClass="DataCell" 
                                SortedDataCellCssClass="SortedDataCell" 
                                GroupHeadingCssClass="GroupHeading" 
                                AlternatingRowCssClass="AltRow" 
                                HoverRowCssClass="SelectedRow">
                            <Columns>
                              <ComponentArt:GridColumn DataField="AccountID" HeadingText="colID|ID" DataCellServerTemplateId="IdTemplate"/>
                              <ComponentArt:GridColumn DataField="AccountName" HeadingText="colName|Name" DataCellServerTemplateId="NameTemplate"/>
                            </Columns>
                          </ComponentArt:GridLevel>
                        </Levels>
                        <ServerTemplates>
                             <ComponentArt:GridServerTemplate ID="IdTemplate">
                                <Template>
                                    <asp:HyperLink ID="lnkId" runat="server" Text='<%# Container.DataItem["AccountID"] %>'></asp:HyperLink>
                               </Template>
                              </ComponentArt:GridServerTemplate>
                              <ComponentArt:GridServerTemplate ID="NameTemplate">
                               <Template>
                                    <asp:HyperLink ID="lnkName" runat="server" Text='<%# Container.DataItem["AccountName"] %>'></asp:HyperLink>
                               </Template>
                              </ComponentArt:GridServerTemplate>
                        </ServerTemplates>
                    </ComponentArt:Grid>
                    <div class="npadminsubheader">
                        <asp:Image runat="Server" ID="imgIndicator" ImageUrl="~/assets/common/icons/indicator.gif" />
                        <asp:Label runat="server" ID="lblBulkOperations" Text="Bulk Operations"></asp:Label>
                    </div>
                    <ComponentArt:TabStrip ID="ActionsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
                        MultiPageId="ActionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs BulkGroupTabs"
                        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
                        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
                        <ItemLooks>
                            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
                        </ItemLooks>
                        <Tabs>
                            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="Salesperson"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts6" Text="Delete"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts7" Text="Activation"></ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                    <ComponentArt:MultiPage ID="ActionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="100">
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSalesPerson" Text="Assign all records returned to this salesperson: "></asp:Label>
                            <asp:DropDownList ID="ddlAssignSalesPersonID" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnAssignSalesPerson" runat="server" Text="Submit" OnClick="btnAssignSalesPerson_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblDelete" Text="Delete all records returned from accounts "></asp:Label>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:label runat="server" ID="lblActive" Text="Toggle Acccount Active Flag"></asp:label>
                            <asp:button runat="server" ID="btnToggleActive" Text="Submit" OnClick="btnToggleActive_Click" />
                        </ComponentArt:PageView>
                    </ComponentArt:MultiPage>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
    <asp:Literal runat="server" ID="errWarning" Text="The bulk operation you are about to do\n will affect ALL RECORDS shown in your search set.\nContinue?"></asp:Literal>
</asp:Content>