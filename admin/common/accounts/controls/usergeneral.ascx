<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserGeneral" Codebehind="UserGeneral.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<asp:Label ID="sysError" CssClass="npwarning" runat="server"></asp:Label>
<table class="npadmintable">
    <tr>
        <td>
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlUserID" runat="server" Text="User ID"></asp:Literal></td>
                    <td class="npadminbody">
                        <asp:Literal ID="sysUserID" runat="server"></asp:Literal>
                        <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUserID" runat="server" ErrorMessage="*" ControlToValidate="txtUserID">
                            <asp:Image runat="server" ID="imgWarning" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlAccountIDLabel" runat="server" Text="Account ID"></asp:Literal></td>
                    <td class="npadminbody"><asp:Literal ID="ltlAccountID" runat="server" Text="AccountID"></asp:Literal><np:AccountPicker ID="accountPicker" runat="server"></np:AccountPicker></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtTitle" runat="server" Columns="35" MaxLength="10"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlFirstName" runat="server" Text="First Name"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtFirstName" runat="server" Columns="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtMiddleName" runat="server" Columns="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlLastName" runat="server" Text="Last Name"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtLastName" runat="server" Columns="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlSuffix" runat="server" Text="Suffix"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtSuffix" runat="server" Columns="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlJobTitle" runat="server" Text="Job Title"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtJobTitle" runat="server" Columns="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlDefaultTheme" runat="server" Text="Default Theme"></asp:Literal></td>
                    <td class="npadminbody" colspan="2"><asp:DropDownList ID="ddlServerID" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlApprovalLimit" runat="server" Text="Approval Limit"></asp:Literal></td>
                    <td class="npadminbody" colspan="2"><asp:TextBox ID="txtApprovalLimit" runat="server">0</asp:TextBox></td>
                </tr>
            </table>
        </td>
        <td>
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlActive" runat="server" Text="Active"></asp:Literal></td>
                    <td class="npadminbody"><asp:CheckBox ID="chkActiveFlag" runat="server" Checked="true" Text="Active"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlBirthDate" runat="server" Text="Date of birth"></asp:Literal></td>
                    <td class="npadminbody"><np:DatePicker ID="dtBirthDate" runat="server"></np:DatePicker></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlTaxID" runat="server" Text="Tax ID"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtTaxID" runat="server"></asp:TextBox></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="errDuplicateUserID" Visible="False" runat="server" Text="This User ID is already being used. Please select another User ID."></asp:Literal>
<asp:Literal ID="hdnMustBeNumber" runat="server" Visible="False" Text="must be a number"></asp:Literal>
<asp:Literal ID="errTitleExceedsMaximumLength" Visible="False" runat="server" Text="User's Title exceeds maximum length."></asp:Literal>
