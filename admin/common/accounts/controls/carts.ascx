<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.carts" Codebehind="Carts.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:gridview 
    ID="OrdersGrid" 
    runat="server" 
    PageSize="20" 
    AllowPaging="True" 
    AllowSorting="True"
    AutoGenerateColumns="False" 
    CssClass="npadmintable" 
    OnPageIndexChanging="OrdersGrid_PageIndexChanging" 
    OnRowDataBound="OrdersGrid_RowDataBound" 
    OnSorting="OrdersGrid_Sorting"
    EmptyDataText="No Carts for Account">
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField SortExpression="OrderID" HeaderText="colID|ID">
            <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderID")%>'>
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="colCreateDate|Create Date"
            DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
        <asp:BoundField DataField="AccountID" SortExpression="AccountID" HeaderText="Customer|Customer"
            DataFormatString="{0}"></asp:BoundField>
        <asp:BoundField DataField="UserID" SortExpression="UserID" HeaderText="colUserID|User ID"
            DataFormatString="{0}"></asp:BoundField>
        <asp:BoundField DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colQuantity|Quantity">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="colTotal|Total" SortExpression="GrandTotal" HeaderStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcTotal" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:gridview>