<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserMembership" Codebehind="UserMembership.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<td class="npadminlabel" style="WIDTH:25%"><asp:literal id="ltlMemberID" runat="server" Text="Member ID"></asp:literal></td>
		<td class="npadminbody"><asp:textbox id="txtMemberID" runat="server"></asp:textbox></td>
		<td class="npadminlabel"><asp:literal id="ltlPoints" runat="server" Text="Points"></asp:literal></td>
		<td class="npadminbody"><asp:textbox id="txtPoints" runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlMemberDate" runat="server" Text="Member Date"></asp:literal></td>
		<td class="npadminbody"><np:NPDatePicker id="dtMemberDate" runat="server"></np:NPDatePicker></td>
		<td class="npadminlabel"><asp:literal id="ltlExpireDate" runat="server" Text="Expire Date"></asp:literal></td>
		<td class="npadminbody"><np:NPDatePicker id="dtMemberExpireDate" runat="server"></np:NPDatePicker></td>
	</tr>
</table>
