<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.ChildAccounts"
    Codebehind="ChildAccounts.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="../../controls/AccountPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel" valign="top">
            <asp:Literal ID="ltlParentAccount" runat="server" Text="Parent Account"></asp:Literal></td>
        <td class="npadminbody" colspan="3">
            <np:AccountPicker ID="apParentID" runat="server"></np:AccountPicker>
            &nbsp;</td>
    </tr>
</table>
<asp:GridView ID="cvChildAccounts" runat="server" CssClass="npadmintable" PageSize="20"
    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Child Accounts" OnRowDataBound="cvChildAccounts_RowDataBound" OnRowCommand="cvChildAccounts_RowCommand">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" /> 
    <Columns>
        <asp:TemplateField>
            <ItemStyle horizontalalign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>'>
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataTextField="AccountName" HeaderText="colChildName|Child Name" SortExpression="AccountName"
            DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?accountid={0}">
        </asp:HyperLinkField>
        <asp:HyperLinkField DataTextField="OpenActivitiesCount" HeaderText="colActitivities|Activities"
            SortExpression="OpenActivitiesCount" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountContacts.aspx?accountid={0}&selected=6">
            <ItemStyle horizontalalign="Center"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataTextField="OpenTicketsCount" HeaderText="colTickets|Tickets" SortExpression="OpenTicketsCount"
            DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/support/tickets.aspx?status=open&accountid={0}">
            <ItemStyle horizontalalign="Center"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataTextField="OpenOpportunitiesCount" HeaderText="colOpportunities|Opportunities"
            SortExpression="OpenOpportunitiesCount" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/focus/Opportunities.aspx?selected=9&accountid={0}">
            <ItemStyle horizontalalign="Center"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataTextField="OpenQuotesCount" HeaderText="colQuotes|Quotes" DataTextFormatString="{0:c}"
            SortExpression="OpenQuotesCount" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountQuotes.aspx?accountid={0}&selected=7">
            <ItemStyle horizontalalign="Right"></ItemStyle>
        </asp:HyperLinkField>
        <asp:TemplateField SortExpression="Orders30" HeaderText="colOrders30|Orders 30" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcOrders30" runat="server" Price='<%# Eval("Orders30") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField SortExpression="Orders60" HeaderText="colOrders60|Orders 60" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcOrders60" runat="server" Price='<%# Eval("Orders60") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField SortExpression="Orders90" HeaderText="colOrders90|Orders 90" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcOrders90" runat="server" Price='<%# Eval("Orders90") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:HyperLink ID="AddNew" ImageUrl="~/assets/common/icons/add.gif" runat="server"
        ToolTip="Add New Child"></asp:HyperLink>
</div>
<asp:Literal ID="hdnAdd" Text="Add New Child" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="hdnRemove" Text="Remove Child Account" runat="server" Visible="False"></asp:Literal>
