<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="usermessaging.ascx.cs" Inherits="netpoint.admin.common.accounts.controls.usermessaging" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel" style="width:25%;">
            <asp:Literal ID="ltlMessengerID" runat="server" Text="MessengerID"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtMessengerID" runat="server" Columns="30"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMessengerType" runat="server" Text="MessengerType"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtMessengerType" runat="server" Columns="30"></asp:TextBox></td>
    </tr>
     <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEmailLastCheck" Text="Last Email Check" runat="server"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="sysEmailLastCheck" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEmailLogin" Text="Email Login" runat="server"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="tbEmailLogin" runat="server" Columns="20"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEmailPassword" Text="Email Password" runat="server"></asp:Literal>
            <br />
            <asp:Literal ID="ltlPasswordOnlyChange" Text="(only to change)" runat="server"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:TextBox ID="tbEmailPassword" runat="server" Columns="20" TextMode="Password"></asp:TextBox></td>
    </tr>
</table>