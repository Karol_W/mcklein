<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.Equipment" Codebehind="Equipment.ascx.cs" %> 
<%@ Register TagPrefix="np" TagName="MachinesBlock" Src="~/catalog/controls/MachinesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Panel ID="pnlDetail" runat="server" Visible="False" Width="100%">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><asp:HyperLink ID="lnkBack" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Back"></asp:HyperLink></td>
            <td class="npadminlabel" align="right">
                <asp:Label ID="sysError" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save"></asp:ImageButton></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlMachine" runat="server" EnableViewState="False" Text="Machine"></asp:Literal></td>
            <td class="npadminbody"><div id="formdiv1"><np:MachinesBlock id="mmySearch" runat="server"></np:MachinesBlock></div></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlMachineName" runat="server" EnableViewState="False" Text="Machine Name"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtMachineName" Width="350px" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="height: 23px"><asp:Literal ID="ltlMachineImageLabel" runat="server" EnableViewState="False" Text="Machine Image"></asp:Literal></td>
            <td class="npadminbody" style="height: 23px"><asp:TextBox ID="txtMachineImage" Width="350px" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlPurchaseDate" runat="server" EnableViewState="False" Text="Purchase Date"></asp:Literal></td>
            <td class="npadminbody"><np:DatePicker id="calPurchaseDate" runat="server"></np:DatePicker></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlNotes" runat="server" EnableViewState="False" Text="Notes"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtMachineNotes" Width="350px" runat="server" Font-Size="Larger" Height="75px" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlFlags" runat="server" Text="Flags"></asp:Literal></td>
            <td class="npadminbody"><asp:CheckBox ID="chkLocked" runat="server" Text="Locked"></asp:CheckBox></td>
        </tr>
    </table>
</asp:Panel>
<asp:GridView 
    id="gvMachines"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Equipment Found" OnRowCommand="gvMachines_RowCommand" OnRowDataBound="gvMachines_RowDataBound">
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:BoundField DataField="MachineName" HeaderText="ltlName|Name"></asp:BoundField>
        <asp:BoundField DataField="Make" HeaderText="ltlMake|Make"></asp:BoundField>
        <asp:BoundField DataField="Model" HeaderText="ltlModel|Model"></asp:BoundField>
        <asp:BoundField DataField="MachineDates" HeaderText="ltlDates|Dates"></asp:BoundField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnShow" runat="server" CommandName="show" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UMID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UMID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>	
</asp:GridView>
<table id="Table1" runat="server" width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="imgAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="imgAdd_Click"></asp:ImageButton></td>
    </tr>
</table>
<asp:Literal ID="ltlCatalogID" runat="server" Visible="False" Text="Catalog ID"></asp:Literal>
<asp:Literal ID="errDeletionsPermanent" Visible="False" runat="server" Text="Deletions are Permanent.  Continue?"></asp:Literal>
<asp:Literal ID="errMakeModelYearRequired" runat="server" Visible="False" Text="Make, model and year are required."></asp:Literal>
