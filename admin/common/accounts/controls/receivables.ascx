<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.receivables" Codebehind="Receivables.ascx.cs" %>
<table class="npadmintable">
    <tr>
        <td colspan="4" class="npadminbody" align="center">
            <asp:Literal ID="ltlBalance" runat="server" Text="Balance"></asp:Literal>
            <np:PriceDisplay ID="prcBalanceValue" runat="server" />
            &nbsp;-&nbsp;
            <asp:Literal ID="ltlCreditLimit" runat="server" Text="Credit Limit"></asp:Literal>
            <np:PriceDisplay ID="prcCreditLimitValue" runat="server" />
            &nbsp;-&nbsp;
            <asp:Literal ID="ltlOverCreditLimit" runat="server" Text="Over Limit"></asp:Literal>
            <np:PriceDisplay ID="prcOverCreditLimitValue" runat="server" />
            <hr />
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlAgingDate" runat="server" Text="Aging Date"></asp:Literal></td>
        <td class="npadminbody"><np:NPDatePicker ID="dtAgingDate" runat="server"></np:NPDatePicker></td>
        <td class="npadminlabel"><asp:Literal ID="ltlAgeBy" runat="server" Text="Age By"></asp:Literal></td>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblAgeBy" runat="server" Width="100%" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <asp:ListItem Value="PostDate" Selected="True">Post Date</asp:ListItem>
                <asp:ListItem Value="ValueDate">Value Date</asp:ListItem>
            </asp:RadioButtonList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlInterval" runat="server" Text="Interval"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtInterval" runat="server" Columns="4">30</asp:TextBox></td>
        <td class="npadminlabel"><asp:Literal ID="ltlNumberOfIntervals" runat="server" Text="# of Intervals"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtNumberOfIntervals" runat="server" Columns="4">4</asp:TextBox>&nbsp;&nbsp;
            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/refresh.gif" ToolTip="Submit"></asp:ImageButton>
        </td>
    </tr>
</table>
<asp:gridview
    id="gvRecievables"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowDataBound="gvRecievables_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:BoundField DataField="Interval" HeaderText="colInterval|Interval"></asp:BoundField>
        <asp:TemplateField HeaderText="colAmount|Amount">
            <ItemTemplate>
                <asp:HyperLink ID="lnkAmount" runat="server"><np:PriceDisplay ID="prcAmount" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Amount") %>' /></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStartDate|Start Date">
            <ItemTemplate>
                <asp:Literal ID="ltlStartDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEndDate|End Date">
            <ItemTemplate>
                <asp:Literal ID="ltlEndDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:gridview>