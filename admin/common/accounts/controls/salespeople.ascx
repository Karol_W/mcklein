<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.Salespeople"
    Codebehind="Salespeople.ascx.cs" %>
<table>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountSalesManager" runat="server" Text="Account Sales Manager"></asp:Literal></td>
        <td class="npadminbody">
                <asp:DropDownList ID="ddlSalesManager" runat="server">
                </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="npadminbody">
        <asp:Literal ID="ltlAssociatedSalesStaff" runat="server" Text="Associated Sales Staff"></asp:Literal><br /><br />
            <asp:CheckBoxList ID="sysListSalesStaff" runat="server" CssClass="npadminbody">
            </asp:CheckBoxList>
            <asp:Literal ID="sysSalespeople" runat="server"></asp:Literal></td>
    </tr>
</table>
<asp:Literal ID="hdnNoneSelected" runat="server" Text="None Selected" Visible="False"></asp:Literal>
