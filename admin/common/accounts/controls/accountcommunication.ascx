<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.AccountCommunication" Codebehind="AccountCommunication.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tbody>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlPrimaryPhone" runat="server" Text="Primary Phone"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtPhone1" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlOtherPhone" runat="server" Text="Other Phone"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtPhone2" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlFax" runat="server" Text="Fax"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtPhoneFax" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlMobile" runat="server" Text="Mobile"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtPhoneMobile" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="100"></asp:TextBox>
                <asp:HyperLink id="lnkEmail" runat="server" ImageUrl="~/assets/common/icons/send.gif" ToolTip="Email"></asp:HyperLink></td>
            <td class="npadminlabel"><asp:Literal ID="ltlWebsite" runat="server" Text="Website"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtWebsite" runat="server"></asp:TextBox>
                <asp:HyperLink ID="lnkWebsite" runat="server" ImageUrl="~/assets/common/icons/preview.gif" Target="_blank" ToolTip="Display Web Site"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
            <td class="npadminbody" colspan="3"><asp:TextBox ID="txtNotes" runat="server" Columns="55" Rows="15" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
    </tbody>
</table>
