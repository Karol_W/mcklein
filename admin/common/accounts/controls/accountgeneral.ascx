<%@ Reference Control="~/admin/common/controls/accountpicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.AccountGeneral" Codebehind="AccountGeneral.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="../../controls/AccountPicker.ascx" %>
<asp:Label ID="sysError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
<table class="npadmintable">
    <tbody>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlAccountID" runat="server" Text="Account ID"></asp:Literal></td>
            <td class="npadminbody" colspan="3">
                <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAccountID" runat="server" ControlToValidate="txtAccountID" ErrorMessage="RequiredFieldValidator">
                <asp:Image runat="server" ImageUrl="~/assets/common/icons/warning.gif" ID="Image1" /></asp:RequiredFieldValidator>&nbsp;
                <asp:ImageButton runat="server" ID="btnFaveAdd" ImageUrl="~/assets/common/icons/bookmark.gif" Visible="False" ToolTip="Add to Favorites" /> &nbsp;&nbsp;&nbsp;
                <asp:Literal ID="ltlSynchInfo" runat="server" Text="Last Synch"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlAccountName" runat="server" Text="Account Name"></asp:Literal></td>
            <td class="npadminbody" colspan="3"><asp:TextBox ID="txtAccountName" runat="server" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAccountName" runat="server" ControlToValidate="txtAccountName" ErrorMessage="RequiredFieldValidator">
                <asp:Image runat="server" ImageUrl="~/assets/common/icons/warning.gif" ID="Image2" /></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlActive" runat="server" Text="Active"></asp:Literal></td>
            <td class="npadminbody"><asp:CheckBox ID="sysActive" runat="server" Text=" " Checked="true"></asp:CheckBox></td>
            <td class="npadminlabel">&nbsp;</td>
            <td class="npadminbody">&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlIndustry" runat="server" Text="Industry"></asp:Literal></td>
            <td class="npadminbody">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td><div id="formdiv2" style="position: relative"><asp:DropDownList ID="ddlIndustryCode" runat="server"></asp:DropDownList></div></td>
                        <td><img height="25" src="" alt="" width="1" /></td>
                    </tr>
                </table>
            </td>
            <td class="npadminlabel"><asp:Literal ID="ltlAcctType" runat="server" Text="Acct Type"></asp:Literal></td>
            <td class="npadminbody">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td><div id="formdiv3" style="position: relative"><asp:DropDownList ID="ddlAccountType" runat="server"></asp:DropDownList></div></td>
                        <td><img src="" width="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<asp:Panel runat="server" ID="pnlExtended" BorderWidth="0">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlMainContact" runat="server" Text="Main contact"></asp:Literal></td>
            <td class="npadminbody" colspan="3">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td><div id="formdiv1" style="position: relative"><asp:DropDownList ID="ddlMainContact" runat="server"></asp:DropDownList></div></td>
                        <td><img height="25" src="" width="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="errAccountDuplicate" Visible="False" runat="server" Text="Account already exists. Please use another Account ID."></asp:Literal>
<asp:Literal ID="hdnNever" Visible="False" runat="server" Text="Never"></asp:Literal>
<asp:Literal ID="hdnMustBeNumber" Visible="False" runat="server" Text="Must be a number"></asp:Literal>
<asp:Literal ID="hdnInternal" Visible="False" runat="server" Text="A zed eCommerce internal account already exists. If you would like to change this to an internal account, change the account type of the currently existing internal account."></asp:Literal>
<asp:Literal ID="hdnAccountIDTooLong" Visible="false" runat="server" Text="The AccountID must be less than 15 characters." />
<asp:Literal ID="hdnCyclicRelationship" Visible="false" runat="server" Text="A cyclic relationship in account hierarchy has been detected." />