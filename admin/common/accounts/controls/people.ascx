<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.People" Codebehind="People.ascx.cs" %>

<asp:gridview id="UserListGrid" runat="server" CssClass="npadmintable" AllowSorting="True" AutoGenerateColumns="False" 
OnRowDataBound="UserListGrid_RowDataBound" OnSorting="UserListGrid_Sorting"
 EmptyDataText="No Users on This Account">
	<RowStyle CssClass="npadminbody" />
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<AlternatingRowStyle CssClass="npadminbodyalt" />
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	<Columns>
		<asp:templatefield>
			<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink ID="lnkEdit" Runat="server" imageurl="~/assets/common/icons/user.gif"></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield HeaderText="colUserName|User Name" SortExpression="UserID">
			<ItemTemplate>
				<asp:HyperLink ID="lnkUserID" Runat="server" text='<%# DataBinder.Eval(Container.DataItem, "UserID") %>'></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="LastName, FirstName" HeaderText="colName|Name">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "FirstName") %>
				<%# DataBinder.Eval(Container.DataItem, "LastName") %>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="Email" HeaderText="colEmail|Email">
			<ItemTemplate>
				<asp:HyperLink ID="lnkEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>'></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="DayPhone" HeaderText="colDayPhone|Day Phone">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "DayPhone") %>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="LastVisitDate" HeaderText="colLastVisit|Last Visit">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "LastVisitDate", "{0:yyyy-MMM-dd HH:mm}") %>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="ActiveFlag" HeaderText="colActive|Active">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:Image id="imgActiveFlag" runat="server" visible="false" imageUrl="~/assets/common/icons/checked.gif"></asp:Image>
			</ItemTemplate>
		</asp:templatefield>
	</Columns>
</asp:gridview>
<br />
<div class="npadminactionbar">
<asp:HyperLink runat="Server" id="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/common/accounts/usernew.aspx" ToolTip="New"></asp:HyperLink>
</div>
<asp:Literal id="hdnEdit" runat="server" Visible="False" Text="Edit"></asp:Literal>
<asp:Literal id="hdnActive" runat="server" Visible="False" Text="Active"></asp:Literal>