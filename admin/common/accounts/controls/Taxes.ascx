<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.Taxes"
    Codebehind="Taxes.ascx.cs" %>
<asp:Panel ID="pnlTaxes" runat="server">
    <asp:GridView 
        id="gvAssign"
        runat="server" 
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        EmptyDataRowStyle-CssClass="npadminempty" 
        EmptyDataRowStyle-Height="50"
        EmptyDataText="No Unassigned Taxes Found" 
        ShowFooter="true"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt"
        OnRowDataBound="gvAssign_RowDataBound">
        <Columns>
            <asp:BoundField DataField="TaxCode" HeaderText="colAssignedTaxCodes|Assigned Tax Codes"></asp:BoundField>
            <asp:BoundField DataField="TaxName" HeaderText="colName|Name"></asp:BoundField>
            <asp:TemplateField HeaderText="colActive|Active">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:CheckBox ID="chkActive" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>    
    </asp:GridView>
    <asp:GridView 
        ID="gvExemptions" 
        runat="server" 
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        EmptyDataRowStyle-CssClass="npadminempty" 
        EmptyDataRowStyle-Height="50"
        EmptyDataText="No Exemptions Found" 
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt"
        OnRowDataBound="gvExemptions_RowDataBound">
            <Columns>
                <asp:BoundField DataField="TaxCode" HeaderText="colExemptTaxCodes|Exempt Tax Codes"></asp:BoundField>
                <asp:BoundField DataField="TaxName" HeaderText="colName|Name"></asp:BoundField>
                <asp:TemplateField HeaderText="colExempt|Exempt">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkExempt" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>                
            </Columns>
    </asp:GridView>
</asp:Panel>

