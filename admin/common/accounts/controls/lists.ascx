<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.lists"
    Codebehind="Lists.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
    
<asp:GridView ID="OrdersGrid" runat="server" Width="100%" PageSize="20" AllowPaging="True"
    AllowSorting="True" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="There are no lists" OnPageIndexChanging="OrdersGrid_PageIndexChanging" OnRowDataBound="OrdersGrid_RowDataBound" OnSorting="OrdersGrid_Sorting">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <PagerStyle CssClass="npadminbody" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:templatefield SortExpression="OrderID" HeaderText="colID">
            <headerstyle horizontalalign="Center" width="5%"></headerstyle>
            <itemstyle horizontalalign="Center"></itemstyle>
            <itemtemplate>
							<asp:HyperLink id="lnkID" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "OrderID")%>'>
							</asp:HyperLink>
						</itemtemplate>
        </asp:templatefield>
        <asp:boundfield DataField="CreateDate" SortExpression="CreateDate" HeaderText="colCreateDate"
            DataFormatString="{0:yyyy-MMM-dd}"></asp:boundfield>
        <asp:boundfield DataField="CartName" SortExpression="CartName" HeaderText="colCartName">
        </asp:boundfield>
        <asp:boundfield DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colQuantity">
            <itemstyle horizontalalign="Right"></itemstyle>
        </asp:boundfield>
        <asp:TemplateField HeaderText="colSubTotal" SortExpression="SubTotal">
            <headerstyle horizontalalign="Right"></headerstyle>
            <itemstyle horizontalalign="Right"></itemstyle>
            <ItemTemplate>
                <np:PriceDisplay ID="prcSubTotal" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "SubTotal") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
