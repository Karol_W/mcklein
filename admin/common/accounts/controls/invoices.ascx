<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.invoices" Codebehind="Invoices.ascx.cs" %>  
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:gridview ID="OrdersGrid" runat="server" CssClass="npadmintable" PageSize="20"
    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Invoices for Account" OnRowDataBound="OrdersGrid_RowDataBound" OnPageIndexChanging="OrdersGrid_PageIndexChanging" OnSorting="OrdersGrid_Sorting">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField SortExpression="OrderID" HeaderText="colID|ID">
            <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderID")%>'>
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="colCreateDate|Create Date"
            DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
        <asp:TemplateField SortExpression="AccountID" HeaderText="Customer|Customer">
            <ItemTemplate>
                <asp:HyperLink ID="lnkAccount" runat="server" NavigateUrl="">
								<%# DataBinder.Eval(Container.DataItem, "AccountID") %> :
								<%# DataBinder.Eval(Container.DataItem, "Account.AccountName") %>
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="SalesPersonID" SortExpression="SalesPersonID" HeaderText="colSalesperson|Salesperson">
        </asp:BoundField>
        <asp:BoundField DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colQuantity|Quantity">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="colTotal|Total" SortExpression="GrandTotal" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcTotal" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:gridview>
