<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserRoles" Codebehind="UserRoles.ascx.cs" %>
<table class="npadmintable">
    <asp:Repeater ID="rptRoles" runat="server">
        <ItemTemplate>
            <tr>
                <td class="npadminbody" style="width:5%" align="center"><asp:Image ID="imgRole" runat="server" ImageUrl="~/assets/common/icons/role.gif"></asp:Image></td>
                <td class="npadminbody"><%# DataBinder.Eval(Container, "DataItem.RoleName") %></td>
                <td class="npadminbody" align="center">
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                        CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UserRoleID") %>'></asp:ImageButton></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <tr>
        <td class="npadminbody">&nbsp;</td>
        <td class="npadminbody"><div id="formdiv1"><asp:DropDownList ID="ddlRole" runat="server"></asp:DropDownList></div></td>
        <td class="npadminbody" align="center"><asp:ImageButton ID="btnAdd" runat="server" CommandName="add" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add"></asp:ImageButton></td>
    </tr>
    <tr>
        <td class="npadminlabel" colspan="3">
            <asp:Literal runat="server" ID="ltlCurrentLicensedUsers" Text="Current Licensed Users:" />
            <asp:Label runat="server" ID="TotalCurrentLicensedUsers" />
            /
            <asp:Literal runat="server" ID="ltlAvailableLicensedUsers" Text="Total User Licenses:" />
            <asp:Label runat="server" ID="TotalAvailableLicensedUsers" />
        </td>
    </tr>
</table>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete this Role" Visible="False"></asp:Literal>
