<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.AccountFinancials"
    Codebehind="AccountFinancials.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="pricetree" Src="~/admin/catalog/controls/pricelisttree.ascx" %>

<table class="npadmintable">
    <tbody>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlPurchaseLimit" Text="Credit Limit" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtPurchaseLimit" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" style="width:20%;"><asp:Literal ID="ltlPriceList" Text="Price List" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList ID="ddlPriceListCode" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTaxID" Text="Tax ID" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtTaxID" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlTaxExemptionID" Text="Exemption ID" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtTaxExemptionID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDefaultShipOption" Text="Default Delivery Opt." runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList ID="ddlDefaultShipOption" runat="server"></asp:DropDownList></td>
            <td class="npadminlabel" style="width:20%;"><asp:Literal ID="ltlDefaultShipAddr" Text="Default Delivery Addr." runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList ID="ddlDefaultShipAddress" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDefaultBillOption" Text="Default Bill Opt." runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList ID="ddlDefaultBillOption" runat="server"></asp:DropDownList></td>
            <td class="npadminlabel" style="width:20%;"><asp:Literal ID="ltlDefaultBillAddr" Text="Default Bill Addr." runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:DropDownList ID="ddlDefaultBillAddress" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlUseApprovals" Text="Use Approvals" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:CheckBox ID="sysUseApprovals" runat="server"></asp:CheckBox></td>
            <td class="npadminlabel" style="width:20%;"><asp:Literal ID="ltlAccountDiscount" Text="Account Discount" runat="server"></asp:Literal></td>
            <td class="npadminbody"><asp:Literal ID="ltlAccountDiscountDisplay" runat="server"></asp:Literal></td>
        </tr>        
    </tbody>
</table>
<np:pricetree id="specialPriceTree" runat="server" ></np:pricetree>