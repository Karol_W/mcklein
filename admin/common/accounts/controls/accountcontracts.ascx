<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.AccountContracts"
    Codebehind="AccountContracts.ascx.cs" %>

<asp:GridView ID="gridContracts" runat="server" AllowPaging="True" AutoGenerateColumns="False"
    AllowSorting="true" CssClass="npadmintable" OnPageIndexChanging="gridContracts_PageIndexChanging" 
    OnRowDataBound="gridContracts_RowDataBound" OnSorting="gridContracts_Sorting" EmptyDataText="No Contracts for Account">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <PagerStyle CssClass="npadminbody" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="colID|ID" SortExpression="SupportContractID">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkId" runat="server"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="ContractStatus">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Description" SortExpression="Description" HeaderText="colDescription|Description"
            DataFormatString="{0}" />
        <asp:TemplateField HeaderText="colStartDate|Start Date" SortExpression="StartDate">
            <ItemTemplate>
                <asp:literal ID="ltlStartDate" runat="server"></asp:literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEndDate|End Date" SortExpression="EndDate">
            <ItemTemplate>
                <asp:literal ID="ltlEndDate" runat="server"></asp:literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="imgWarning" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"
                    runat="server"></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:HyperLink runat="Server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif"
        NavigateUrl="~/admin/support/ContractDetail.aspx" ToolTip="New"></asp:HyperLink>
</div>
<asp:Literal ID="hdnError" runat="server" Text="ERROR" Visible="False"></asp:Literal>
<asp:Literal ID="errDoesNotExist" runat="server" Text="Does not exist in the zed eCommerce database"
    Visible="False"></asp:Literal>