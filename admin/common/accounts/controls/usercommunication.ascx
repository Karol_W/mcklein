<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserCommunication" Codebehind="UserCommunication.ascx.cs" %>
<table class="npadmintable">
    <tr>
        <td valign="top">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="npadminlabel" style="width:100px;"><asp:Literal ID="ltlDayPhone" runat="server" Text="Office Phone"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtDayPhone" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlEveningPhone" runat="server" Text="Home Phone"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtEveningPhone" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlMobilePhone" runat="server" Text="Mobile Phone"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="txtMobilePhone" runat="server"></asp:TextBox></td>
                </tr>
                  <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
                    <td class="npadminbody">
                        <asp:TextBox ID="txtEmail" runat="server" Columns="30"></asp:TextBox>
                        <asp:HyperLink ID="lnkEmail" runat="server" Visible="False" ImageUrl="~/assets/common/icons/send.gif" ToolTip="Email" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal><br />
            <asp:TextBox ID="txtNotes" runat="server" Columns="70" TextMode="MultiLine" Rows="10"></asp:TextBox></td>
    </tr>
</table>
