<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserStats" Codebehind="UserStats.ascx.cs" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel" style="width:25%;"><asp:Literal ID="ltlAddDateLabel" Text="Date Added" runat="server"></asp:Literal></td>
        <td class="npadminbody"><asp:Literal ID="sysAddDate" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlLastVisitDate" Text="Last Visit" runat="server"></asp:Literal></td>
        <td class="npadminbody"><asp:Literal ID="sysLastVisit" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlLastIP" Text="Last IP" runat="server"></asp:Literal></td>
        <td class="npadminbody"><asp:Literal ID="sysLastIPAddress" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlVisits" Text="Visits" runat="server"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtVisits" runat="server"></asp:TextBox></td>
    </tr>
</table>
