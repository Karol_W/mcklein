<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="usersecurity.ascx.cs" Inherits="netpoint.admin.common.accounts.controls.usersecurity" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlLoginName" runat="server" Text="Login Name"></asp:Literal>
            <asp:Image ID="sysLoginNameWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" EnableViewState="false" Visible="false" />
        </td>
        <td class="npadminbody"><asp:TextBox ID="txtLoginName" runat="server"></asp:TextBox></td>        
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlNewPassword" runat="server" Text="New Password"></asp:Literal>
        </td>
        <td class="npadminbody">
             <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlConfirmNewPassword" runat="server" Text="Confirm New Password"></asp:Literal>
        </td>
        <td class="npadminbody">
             <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlQuestion" runat="server" Text="Security Question"></asp:Literal>
        </td>
        <td class="npadminbody">
             <asp:TextBox ID="txtQuestion" runat="server" Width="400px" MaxLength="255"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAnswer" runat="server" Text="Security Answer"></asp:Literal>
        </td>
        <td class="npadminbody">
             <asp:TextBox ID="txtAnswer" runat="server"></asp:TextBox>
        </td>
    </tr>  
     <tr>
        <td class="npwarning" colspan="2" align="center">
            <asp:Literal ID="sysMessage" runat="server" EnableViewState="false"></asp:Literal>
        </td>
    </tr>
</table>
<asp:Literal ID="ltlPasswordMatch" runat="server" Visible="false" Text="The new password and it's confirmation must match."></asp:Literal>
<asp:Literal ID="ltlPasswordLength" runat="server" Visible="false" Text="The password must be {0} characters long."></asp:Literal>
<asp:Literal ID="ltlNoUser" runat="server" Visible="false" Text="The User was not found in the database."></asp:Literal>