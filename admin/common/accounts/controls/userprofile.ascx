<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.UserProfile" Codebehind="UserProfile.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlDisplayName" runat="server" text="Display Name"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtDisplayName" runat="server" Columns="55"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlProfilePicture" runat="server" Text="Profile Picture"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtProfilePicture" runat="server" Columns="55"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlNotes" runat="server" Text="Notes"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtProfileNotes" runat="server" TextMode="MultiLine" Columns="55" Rows="5"></asp:textbox></td>
	</tr>
</table>
