<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.accounts.controls.Resources"
    Codebehind="Resources.ascx.cs" %>
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<asp:Panel ID="pnlDetail" runat="server" Visible="False">
    <table id="tblDetail" class="npadmintable" runat="server">
        <tr>
            <td class="npadminlabel">
                <asp:ImageButton ID="lnkBack" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" OnClick="lnkBack_Click" ToolTip="Back"></asp:ImageButton></td>
            <td class="npadminlabel" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    Visible="False" OnClick="btnDelete_Click" ToolTip="Delete"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    OnClick="btnSave_Click" ToolTip="Save"></asp:ImageButton></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
            <td class="npadminbody">
                <div id="formdiv1">
                    <asp:DropDownList ID="ddlResourceType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlResourceType_SelectedIndexChanged">
                    </asp:DropDownList>&nbsp;
                    <asp:CheckBox ID="chkCommunityVisible" runat="server" Text="Visible to Community"></asp:CheckBox></div>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension1" runat="server" Text="Dimension1"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension1" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension2" runat="server" Text="Dimension2"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension2" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension3" runat="server" Text="Dimension3"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension3" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension4" runat="server" Text="Dimension4"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension4" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension5" runat="server" Text="Dimension5"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension5" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension6" runat="server" Text="Dimension6"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension6" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension7" runat="server" Text="Dimension7"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension7" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension8" runat="server" Text="Dimension8"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension8" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDimension9" runat="server" Text="Dimension9"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension9" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlURLDimension" runat="server" Text="URLDimension"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtURLDimension" runat="server" Width="350px" Columns="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlCodeDimension1" runat="server" Text="CodeDimension1"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlCode1" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlCodeDimension2" runat="server" Text="CodeDimension2"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlCode2" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDateDimension" runat="server" Text="DateDimension"></asp:Literal></td>
            <td class="npadminbody">
                <np:NPDatePicker ID="dtDateDimension" runat="server"></np:NPDatePicker>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlPictureDimension" runat="server" Text="PictureDimension"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtPictureDimension" runat="server" Width="350px"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlCommentsDimension" runat="server" Text="CommentsDimension"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtCommentDimension" runat="server" Width="350px" TextMode="MultiLine"
                    Height="100px" Columns="40" Rows="10"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlOptions" runat="server" Text="Options"></asp:Literal></td>
            <td class="npadminbody">
                <asp:CheckBoxList ID="chkListOptions" runat="server">
                </asp:CheckBoxList></td>
        </tr>
    </table>
</asp:Panel>
<asp:GridView ID="gvResources" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="npadminsubheader"
    EmptyDataRowStyle-CssClass="npadminempty" EmptyDataRowStyle-Height="50" EmptyDataText="No Associated Resources"
    CssClass="npadmintable" RowStyle-CssClass="npadminbody" AlternatingRowStyle-CssClass="npadminbodyalt"
    OnRowCommand="gvResources_RowCommand" OnRowDataBound="gvResources_RowDataBound">
    <FooterStyle CssClass="npadminlabel"></FooterStyle>
    <Columns>
        <asp:BoundField DataField="ResourceName" HeaderText="colType|Type"></asp:BoundField>
        <asp:BoundField DataField="Dimension1" HeaderText="colDimension1|Dimension 1"></asp:BoundField>
        <asp:BoundField DataField="Dimension2" HeaderText="colDimension2|Dimension 2"></asp:BoundField>
        <asp:BoundField DataField="Dimension3" HeaderText="colDimension3|Dimension 3"></asp:BoundField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnShow" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandName="show" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.URID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif"
        CommandName="show" CommandArgument="0" OnClick="btnAdd_Click" ToolTip="Add" />
</div>
<asp:Literal ID="errNoMetaData" runat="server" Text="No Resource MetaData Definitions have been created"
    Visible="false"></asp:Literal>