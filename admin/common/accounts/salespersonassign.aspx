<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.SalesPersonAssign" Codebehind="SalesPersonAssign.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/assets/common/icons/search.gif" ToolTip="Search"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminlabel" style="WIDTH:20%;"><asp:Literal ID="ltlAccountID" runat="server" Text="Account ID"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlAccountName" runat="server" Text="Account Name"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtAccountName" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="npadminheader"><asp:Literal ID="ltlAssignHeader" runat="server" Text="Assign SalesPerson To Accounts"></asp:Literal></td>
                <td class="npadminheader"><asp:DropDownList ID="ddlSalesPeople" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesPeople_SelectedIndexChanged"></asp:DropDownList></td>
                <td align="right" class="npadminheader"><asp:ImageButton ID="btnSave" runat="server" ImageUrl="../../../assets/common/icons/save.gif" CommandName="save" OnClick="btnSave_Click" ToolTip="Save"></asp:ImageButton></td>
            </tr>
        </table>
        <asp:gridview
            id="gvSalesPeople"
            Allowpaging="true" 
            PageSize="20"
            runat="server" 
            AutoGenerateColumns="false" 
            HeaderStyle-CssClass="npadminsubheader"
            CssClass="npadmintable" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            EmptyDataText="No Records Found" OnRowDataBound="gvSalesPeople_RowDataBound" OnPageIndexChanging="gvSalesPeople_PageIndexChanging">
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:BoundField DataField="AccountID" HeaderText="AccountID|AccountID"></asp:BoundField>
                <asp:BoundField DataField="AccountName" HeaderText="Account Name|Account Name"></asp:BoundField>
                <asp:TemplateField HeaderText="Assign|Assign">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkApply" runat="server"></asp:CheckBox>
                        <asp:Literal ID="ltlHiddenAccountID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>' Visible="False"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
