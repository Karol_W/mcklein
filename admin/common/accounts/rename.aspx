<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.Rename" Codebehind="Rename.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="../../common/controls/AccountPicker.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npadminheader">
                <asp:hyperlink id="lnkBack" runat="server" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Back"></asp:hyperlink>&nbsp;
                <asp:literal id="ltlAddressHeader" runat="server" text="Rename"></asp:literal></td>
            <td class="npadminheader" align="right">&nbsp;</td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlCurrentAccount" runat="server" text="Current Account ID"></asp:literal></td>
            <td class="npadminbody"><np:AccountPicker ID="apCurrentAccount" runat="server"></np:AccountPicker></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlDestinationAccount" runat="server" text="Destination Account ID"></asp:literal></td>
            <td class="npadminbody"><asp:textbox id="tbDestinationAccount" runat="server" /></td>
        </tr>
        <tr>
            <td class="npadminlabel">&nbsp;</td>
            <td class="npadminbody" align="center"><asp:button runat="server" id="btnRenameAccount" text="Rename Business Partner" onclick="btnRenameAccount_Click" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlCurrentUser" runat="server" text="Current User ID"></asp:literal></td>
            <td class="npadminbody"><asp:textbox id='tbCurrentUser' runat="server" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlDestinationUser" runat="server" text="Destination User ID"></asp:literal></td>
            <td class="npadminbody"><asp:textbox id="tbDestinationUser" runat="server" /></td>
        </tr>
        <tr>
            <td class="npadminlabel">&nbsp;</td>
            <td class="npadminbody" align="center"><asp:button runat="server" id="btnRenameUser" text="Rename User" onclick="btnRenameUser_Click" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnAccountRenamed" runat="server" text="Account Renamed" visible="False"></asp:literal>
    <asp:literal id="hdnAddressValid" runat="server" text="Valid Address" visible="False"></asp:literal>
    <asp:literal id="hdnInvalidZip" runat="server" text="Invalid Zip Code" visible="False"></asp:literal>
    <asp:literal id="hdnAccountNotRenamed" runat="server" text="Account NOT Renamed" visible="False"></asp:literal>
    <asp:literal id="Literal2" runat="server" text="Valid Address" visible="False"></asp:literal>
    <asp:literal id="Literal3" runat="server" text="Invalid Zip Code" visible="False"></asp:literal>
    <asp:literal id="hdnUserRenamed" runat="server" text="User Renamed" visible="False"></asp:literal>
    <asp:literal id="Literal5" runat="server" text="Valid Address" visible="False"></asp:literal>
    <asp:literal id="Literal6" runat="server" text="Invalid Zip Code" visible="False"></asp:literal>
    <asp:literal id="hdnUserNotRenamed" runat="server" text="User NOT Renamed" visible="False"></asp:literal>
    <asp:literal id="Literal8" runat="server" text="Valid Address" visible="False"></asp:literal>
    <asp:literal id="Literal9" runat="server" text="Invalid Zip Code" visible="False"></asp:literal>
</asp:Content>
