<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.Favorites" Codebehind="Favorites.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Favorite Business Partners</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <asp:gridview ID="FavoritesGrid" runat="server" CssClass="npadmintable" AutoGenerateColumns="False"
        PageSize="50" AllowPaging="True" AllowSorting="True" EmptyDataText="No Favorites Selected" OnRowDeleting="FavoritesGrid_RowDeleting" OnPageIndexChanging="FavoritesGrid_PageIndexChanging" OnSorting="FavoritesGrid_Sorting">
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <HeaderStyle CssClass="npadminsubheader" />
        <PagerStyle cssclass="npadminbody" />
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:Templatefield>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                        CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.AccountID") %>'></asp:ImageButton>
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Center"></FooterStyle>
            </asp:Templatefield>
            <asp:HyperLinkField DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="AccountInfo.aspx?accountid={0}&backpage=~/admin/common/accounts/Favorites.aspx"
                DataTextField="AccountID" HeaderText="Customer|Customer" SortExpression="AccountID"></asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="AccountInfo.aspx?accountid={0}&backpage=~/admin/common/accounts/Favorites.aspx"
                DataTextField="AccountName" HeaderText="Customer Name|Customer Name" SortExpression="AccountName">
            </asp:HyperLinkField>
            <asp:boundfield DataField="Phone1" HeaderText="colPhone1|Phone 1" SortExpression="Phone1"></asp:boundfield>
            <asp:boundfield DataField="Website" HeaderText="colWebsite|Web Site" SortExpression="Website">
            </asp:boundfield>
            <asp:HyperLinkField DataNavigateUrlFields="MainContactID" DataNavigateUrlFormatString="UserInfo.aspx?userid={0}&backpage=~/admin/common/accounts/Favorites.aspx"
                DataTextField="MainContactID" HeaderText="colMainContact|Main User" SortExpression="MainContactID">
            </asp:HyperLinkField>
        </Columns>
    </asp:gridview>
</asp:Content>
