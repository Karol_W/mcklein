<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.UserInfo" Codebehind="UserInfo.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="equipment" Src="~/admin/common/accounts/controls/Equipment.ascx" %>
<%@ Register TagPrefix="np" TagName="roles" Src="~/admin/common/accounts/controls/UserRoles.ascx" %>
<%@ Register TagPrefix="np" TagName="stats" Src="~/admin/common/accounts/controls/UserStats.ascx" %>
<%@ Register TagPrefix="np" TagName="resources" Src="~/admin/common/accounts/controls/Resources.ascx" %>
<%@ Register TagPrefix="np" TagName="communication" Src="~/admin/common/accounts/controls/UserCommunication.ascx" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/accounts/controls/UserGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="activity" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="np" TagName="tickets" Src="~/admin/focus/controls/tickets.ascx" %>
<%@ Register TagPrefix="np" TagName="logs" Src="~/admin/prospects/controls/logs.ascx" %>
<%@ Register TagPrefix="np" TagName="invoices" Src="~/admin/common/accounts/controls/Invoices.ascx" %>
<%@ Register TagPrefix="np" TagName="carts" Src="~/admin/common/accounts/controls/Carts.ascx" %>
<%@ Register TagPrefix="np" TagName="lists" Src="~/admin/prospects/controls/lists.ascx" %>
<%@ Register TagPrefix="np" TagName="orders" Src="~/admin/focus/controls/orders.ascx" %>
<%@ Register TagPrefix="np" TagName="quotes" Src="~/admin/focus/controls/quotes.ascx" %>
<%@ Register TagPrefix="np" TagName="opportunities" Src="~/admin/focus/controls/opportunities.ascx" %>
<%@ Register TagPrefix="np" TagName="forums" Src="~/admin/common/accounts/controls/Forums.ascx" %>
<%@ Register TagPrefix="np" TagName="listings" Src="~/admin/common/accounts/controls/Listings.ascx" %>
<%@ Register TagPrefix="np" TagName="membership" Src="~/admin/common/accounts/controls/UserMembership.ascx" %>
<%@ Register TagPrefix="np" TagName="profile" Src="~/admin/common/accounts/controls/UserProfile.ascx" %>
<%@ Register TagPrefix="np" TagName="security" Src="~/admin/common/accounts/controls/usersecurity.ascx" %>
<%@ Register TagPrefix="np" TagName="messaging" Src="~/admin/common/accounts/controls/usermessaging.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>User Details - <asp:literal id="Literal1" runat="server" Text="User"></asp:literal></title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<asp:label ID="sysError" runat="server" CssClass="npwarning" EnableViewState="false" ></asp:label>
<table class="npadminpath">
    <tr>
        <td class="npadminheader">&nbsp;</td>
        <td class="npadminheader" align="right" style="white-space:nowrap">&nbsp;
            <asp:literal id="ltlUser" runat="server" Text="User"></asp:literal>&nbsp;
            <asp:hyperlink id="lnkUp" runat="server" navigateurl="~/admin/common/accounts/AccountInfo.aspx" imageurl="~/assets/common/icons/uplevel.gif" tooltip="Up to Account" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="btnDeleteGeneral" OnClick="btnDeleteGeneral_Click" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete"></asp:ImageButton>&nbsp;
            <asp:ImageButton ID="btnSave" OnClick="btnSave_Click" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save"></asp:ImageButton>&nbsp;</td>
            <asp:ImageButton ID="btnSaveAll" OnClick="btnSaveAll_Click" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close"></asp:ImageButton>&nbsp;</td>
    </tr>
</table>
<div class="npadminbody">
    <ComponentArt:TabStrip ID="UserTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="UserMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="tsAccountInfo" Text="User Info" SubGroupCssClass="Level2GroupTabs"
                DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsCommunication" Text="Communication"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsStatistics" Text="Statistics"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsResources" Text="Resources"></ComponentArt:TabStripTab>  
                <ComponentArt:TabStripTab runat="server" ID="tsEquipment" Text="Equipment"></ComponentArt:TabStripTab>
            </ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="tsActivities" Text="Activities" SubGroupCssClass="Level2GroupTabs"
                DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                <ComponentArt:TabStripTab runat="server" ID="tsEvents" Text="Events"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLogs" Text="Logs"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTickets" Text="Tickets"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLists" Text="Lists"></ComponentArt:TabStripTab>
            </ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="tsTransactions" Text="Transactions"
                SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                <ComponentArt:TabStripTab runat="server" ID="tsOrders" Text="Orders"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsOpportunities" Text="Opportunities"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsQuotes" Text="Quotes"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsInvoices" Text="Invoices"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsCarts" Text="Carts"></ComponentArt:TabStripTab>
            </ComponentArt:TabStripTab>
             <ComponentArt:TabStripTab runat="server" ID="tsSecurity" Text="Security"
                SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                <ComponentArt:TabStripTab runat="server" ID="tsPassword" Text="Password"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsRoles" Text="Roles"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsEmail" Text="Email"></ComponentArt:TabStripTab>
              </ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="UserMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
        <ComponentArt:PageView CssClass="npadminbody" runat="server" />
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:general ID="general" runat="server"></np:general>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvCommunication">
            <np:communication ID="communication" runat="server"></np:communication>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:stats ID="stats" runat="server" Visible="true"></np:stats>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:resources ID="resources" runat="server" Visible="true"></np:resources>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:equipment ID="equipment" runat="server" Visible="true"></np:equipment>
        </ComponentArt:PageView>
        
        <ComponentArt:PageView CssClass="npadminbody" runat="server" />
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:activity ID="activity" runat="server"></np:activity>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:logs ID="logs" runat="server"></np:logs>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:tickets ID="tickets" runat="server"></np:tickets>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:lists ID="lists" runat="server"></np:lists>
        </ComponentArt:PageView>
        
        <ComponentArt:PageView CssClass="npadminbody" runat="server" />
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:orders ID="orders" runat="server"></np:orders>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:opportunities ID="opportunities" runat="server"></np:opportunities>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:quotes ID="quotes" runat="server"></np:quotes>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:invoices ID="invoices" runat="server"></np:invoices>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:carts ID="carts" runat="server"></np:carts>
        </ComponentArt:PageView>
        
        <ComponentArt:PageView CssClass="npadminbody" runat="server" />
         <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:security ID="security" runat="server" Visible="true"></np:security>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:roles ID="roles" runat="server" Visible="true"></np:roles>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:messaging ID="messaging" runat="server" Visible="true"></np:messaging>
        </ComponentArt:PageView>
   
         <ComponentArt:PageView CssClass="npadminbody" runat="server" />
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:profile ID="profile" runat="server"></np:profile>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:membership ID="membership" runat="server"></np:membership>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:listings ID="listings" runat="server" Visible="true"></np:listings>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:forums ID="forums" runat="server" Visible="true"></np:forums>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnDeleteUser" runat="server" text="The following will be deleted: \n - Open Carts \n - Saved Order Lists \n - User Roles \n - User Wallet \n User Resources \n - User Equipment \n -Continue?" visible="False"></asp:literal>
    <asp:literal ID="hdnLoginInUse" runat="server" Text="The login name you have selected is already in use.  Please select another."></asp:literal>
    <asp:Literal ID="hdnLoginNameRequired" runat="server" Text="Login name is required."></asp:Literal>
</asp:Content>
