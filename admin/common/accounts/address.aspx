<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.Address" Codebehind="Address.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:label id="sysError" runat="server" forecolor="red" font-bold="true"></asp:label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                <asp:hyperlink id="lnkBack" runat="server" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Back"></asp:hyperlink>&nbsp;
                <asp:literal id="ltlAddressHeader" runat="server" text="Address"></asp:literal></td>
            <td class="npadminheader" align="right">
                <asp:imagebutton id="btnSave" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" ToolTip="Save"></asp:imagebutton>&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader"><asp:Literal ID="ltlSubHeader" runat="server" Text="New Address" /></td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><np:AddressBlock id="npaddressblock" runat="server" ShowSave="false" AdminMode="true"></np:AddressBlock></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>    
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
</asp:Content>
