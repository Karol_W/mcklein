<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.common.accounts.users" Codebehind="users.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Users</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="usernew.aspx" Visible="false" ToolTip="New" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter" OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter" OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or Edit a Filter Set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right" style="white-space:nowrap;">&nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults" OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults" OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" Visible="false"/></td>
            <td class="npadminsubheader" colspan="1" align="right" style="white-space:nowrap;">&nbsp;
                <asp:ImageButton runat="server" ID="btnFind" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnFind_Click" ToolTip="Find" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:Panel runat="server" ID="pnlResults" Visible="false">
<%--                    <asp:GridView ID="gvResults" runat="server" PageSize="50" CssClass="npadmintable"
                        AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" OnRowCommand="gvResults_RowCommand"
                        OnPageIndexChanging="gvResults_PageIndexChanging" OnSorting="gvResults_Sorting"
                        EmptyDataText="No Users Found" OnRowDataBound="gvResults_RowDataBound" PagerStyle-CssClass="GridPaging">
                        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                        <HeaderStyle CssClass="npadminsubheader" />
                        <RowStyle CssClass="npadminbody" />
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <Columns>                                                        
                            <NPWC:HyperLinkField DataTextField="UserID" DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="userinfo.aspx?UserID={0}"
                                HeaderText="colUserID|User ID" SortExpression="UserID">
                                <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="25%" />
                            </NPWC:HyperLinkField>
                            <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderText="colFirstName|First Name" />
                            <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderText="colLastName|Last Name" />
                            <asp:TemplateField HeaderText="colEmail|Email" SortExpression="Email">
                                <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="25%" />
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="lnkEmail" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView><br />
--%>
                    
                    <ComponentArt:Grid id="gvResults" DataAreaCssClass="Grid"
                            ClientObjectId="grid"
                            ClientScriptLocation="~/scripts"
                            AutoFocusSearchBox="false"
                            AutoTheming="true"
                            RunningMode="Callback"
                            ShowHeader="true" 
                            ShowFooter="true"
                            ShowSearchBox="true"
                            SearchOnKeyPress="true"
                            PageSize="20" 
                            PagerStyle="Numbered" 
                            FooterCssClass="GridFooter"
                            HeaderCssClass="GridHeader"
                            SliderPopupOffsetX="72"
                            GroupingPageSize="5"
                            ImagesBaseUrl="~/assets/common/admin/"
                            PagerImagesFolderUrl="~/assets/common/admin/pager/" 
                            LoadingPanelEnabled="false"
                            GroupingNotificationText="Drag a column to this area to group by it."
                            runat="server"
                            AutoAdjustPageSize="true" 
                            AutoSortOnGroup="true" 
                            Width="100%"
                            CollapseImageUrl="~/assets/common/admin/topItem_col.gif" 
                            ExpandImageUrl="~/assets/common/admin/topItem_exp.gif" 
                            GroupingCountHeadingsAsRows="false" 
                            AllowColumnResizing="true">
                        <Levels>
                          <ComponentArt:GridLevel DataKeyField="PartNo" 
                                AllowReordering="true"   
                                ColumnGroupIndicatorImageUrl=""  
                                RowCssClass="Row" 
                                ShowSelectorCells="true" 
                                AllowGrouping= "true" 
                                SortAscendingImageUrl="~/assets/common/admin/asc.gif" 
                                SortDescendingImageUrl="~/assets/common/admin/desc.gif" 
                                HeadingTextCssClass="HeadingCellText" 
                                HeadingCellCssClass="HeadingCell" 
                                DataCellCssClass="DataCell" 
                                SortedDataCellCssClass="SortedDataCell" 
                                GroupHeadingCssClass="GroupHeading" 
                                AlternatingRowCssClass="AltRow" 
                                HoverRowCssClass="SelectedRow">
                            <Columns>
                              <ComponentArt:GridColumn DataField="UserID" HeadingText="colUserID|User ID" DataCellServerTemplateId="UserTemplate"/>
                              <ComponentArt:GridColumn DataField="FirstName" HeadingText="colFirstName|First Name" />
                              <ComponentArt:GridColumn DataField="LastName" HeadingText="colLastName|Last Name" />
                              <ComponentArt:GridColumn DataField="Email" HeadingText="colEmail|Email" DataCellServerTemplateId="EmailTemplate" />

                            </Columns>
                          </ComponentArt:GridLevel>
                        </Levels>
                        <ServerTemplates>
                             <ComponentArt:GridServerTemplate ID="UserTemplate">
                                <Template>
                                    <asp:HyperLink ID="lnkUser" runat="server"  
                                        NavigateUrl='<%# "userinfo.aspx?UserID=" + Container.DataItem["UserID"] %>'
                                         Text='<%# Container.DataItem["UserID"] %>'></asp:HyperLink>
                               </Template>
                              </ComponentArt:GridServerTemplate>
                            <ComponentArt:GridServerTemplate ID="EmailTemplate">
                                <Template>
                                    <asp:HyperLink runat="server" ID="lnkEmail" 
                                        NavigateUrl='<%# "mailto:" + Container.DataItem["Email"] %>'
                                        Text='<%# Container.DataItem["Email"] %>'></asp:HyperLink>
                               </Template>
                              </ComponentArt:GridServerTemplate>
                        </ServerTemplates>
                    </ComponentArt:Grid>
                    <div class="npadminsubheader">
                        <asp:Image runat="Server" ID="imgIndicator" ImageUrl="~/assets/common/icons/indicator.gif" />
                        <asp:Label runat="server" ID="lblBulkOperations" Text="Bulk Operations"></asp:Label>
                    </div>
                    <ComponentArt:TabStrip ID="ActionsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
                        MultiPageId="ActionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs BulkGroupTabs"
                        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
                        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
                        <ItemLooks>
                            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
                        </ItemLooks>
                        <Tabs>
                            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Lists"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts7" Text="Theme"></ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                    <ComponentArt:MultiPage ID="ActionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
                        Width="100%" Height="100">
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSubscribe" Text="Subscribe all records returned to this user list: "></asp:Label>
                            <asp:DropDownList ID="ddlSubscribeContactList" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnSubscribeContactList" runat="server" Text="Submit" OnClick="btnSubscribeContactList_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblTheme" Text="Assign all records returned to this theme: "></asp:Label>
                            <asp:DropDownList ID="ddlTheme" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnAssignTheme" runat="server" Text="Submit" OnClick="btnAssignTheme_Click"></asp:Button>
                        </ComponentArt:PageView>                        
                    </ComponentArt:MultiPage>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="gridFilter" runat="server" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
    <asp:Literal runat="server" ID="errWarning" Text="The bulk operation you are about to do\n will affect ALL RECORDS shown in your search set.\nContinue?"></asp:Literal>
</asp:Content>
