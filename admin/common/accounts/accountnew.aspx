<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/accounts/controls/AccountGeneral.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.accounts.AccountNew" Codebehind="AccountNew.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                &nbsp;<asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="2"><asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/indicator.gif" ID="imgIndicator" ToolTip=" " />
                &nbsp;<asp:Literal runat="server" ID="ltlGeneralHeader" Text="General" /></td>
            <td class="npadminsubheader" colspan="1" align="right">&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><np:general ID="general" runat="server"></np:general></td>
        </tr>
        <tr>
            <td colspan="3"><asp:Label runat=server ID="sysError" CssClass="npwarning" Visible="false"></asp:Label></td>
        </tr>
    </table>
</asp:Content>
