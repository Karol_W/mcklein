<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/admindefault.master" Inherits="netpoint.admin.common.CultureObjectStrings" Codebehind="CultureObjectStrings.aspx.cs" %>


<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                <br />
                <asp:Literal ID="sysHeader" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnAlternateLanguageText" runat="server" Text="Alternate language text for"
        Visible="False"></asp:Literal>
    <asp:Literal ID="hdnRole" runat="server" Text="Role Name" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDiscountCondition" runat="server" Visible="False" Text="Discount Condition Description"></asp:Literal>
    <asp:Literal ID="hdnOrderExpense" runat="server" Visible="false" Text="Expense Name"></asp:Literal>
    <asp:Literal ID="hdnCode" runat="server" Text="Code" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnUnitAbbr" runat="server" Text="Unit Abbr." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnUnitName" runat="server" Text="Unit Name" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnNodeTitle" runat="server" Text="Site Node Title" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnActivityType" runat="server" Text="Activity Type or Subject Name" Visible="false"></asp:Literal>
</asp:Content>
