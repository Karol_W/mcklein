<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.books.GLSearch" Codebehind="GLSearch.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table width="100%" cellpadding="1" cellspacing="0" border="0">
        <tr>
            <td colspan="3" class="npadminheader">
                <asp:hyperlink runat="server" id="lnkBack" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Back" />
                <asp:literal id="ltlHeader" runat="server" text="Search For Financial Transactions"></asp:literal>
            </td>
            <td class="npadminheader" align="right">
                <asp:imagebutton id="btnSubmit" runat="server" imageurl="~/assets/common/icons/search.gif"></asp:imagebutton>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlAccountID" runat="server" text="Account ID"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:textbox id="txtAccountID" runat="server"></asp:textbox>
            </td>
            <td class="npadminlabel">
                <asp:literal id="ltlReference" runat="server" text="Reference"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:textbox id="txtReference" runat="server"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2">
                <asp:radiobuttonlist id="rblSearchDate" runat="server" repeatdirection="Horizontal"
                    repeatlayout="Flow">
                    <asp:ListItem Value="PostDate" Selected="True">Post Date</asp:ListItem>
                    <asp:ListItem Value="ValueDate">Value Date</asp:ListItem>
                </asp:radiobuttonlist>
                </td>
                <td class="npadminbody" colspan="2">
                    <asp:literal id="ltlFrom" runat="server" text="From"></asp:literal>
                    &nbsp;
                    <np:NPDatePicker ID="dtFrom" runat="server"></np:NPDatePicker>
                    <asp:literal id="ltlTo" runat="server" text="To"></asp:literal>
                    <np:NPDatePicker ID="dtTo" runat="server"></np:NPDatePicker>
                </td>
        </tr>
        <tr>
            <td colspan="4" class="npadminbody" align="center">
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView 
        id="gvSearch"
        runat="server" 
        AllowPaging="true"
        PageSize="20"
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        EmptyDataText="No Records Found" OnPageIndexChanging="gvSearch_PageIndexChanging" OnRowDataBound="gvSearch_RowDataBound">
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:BoundField DataField="AccountCode" HeaderText="Customer|Customer" Visible="False"></asp:BoundField>
            <asp:BoundField DataField="AccountName" HeaderText="Customer Name|Customer Name" Visible="False"></asp:BoundField>
            <asp:BoundField DataField="AccountID" HeaderText="colUserAccount|Account ID"></asp:BoundField>
            <asp:TemplateField HeaderText="colDebit|Debit">
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay id="prcDebit" runat="server" Price='<%# Eval("Debit") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colCredit|Credit">
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay id="prcCredit" runat="server" Price='<%# Eval("Credit") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colPostDate|Posting Date">
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal id="ltlPostDate" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colValueDate|Value Date">
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal id="ltlValueDate" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DocumentType" HeaderText="colDocumentType|Document Type">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Reference" HeaderText="colReference|Reference"></asp:BoundField>
        </Columns>
    </asp:GridView>
</asp:Content>
