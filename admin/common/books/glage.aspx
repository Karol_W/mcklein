<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.books.GLAge" Codebehind="GLAge.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table width="100%" cellpadding="2" cellspacing="0" border="1">
    <tr>
        <td colspan="6" class="npadminheader"><asp:Literal ID="ltlHeader" runat="server" Text="Age Accounts"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlAgingDate" runat="server" Text="Aging Date"></asp:Literal></td>
        <td class="npadminbody"><np:DatePicker id="dtAgingDate" runat="server"></np:DatePicker></td>
        <td class="npadminlabel"><asp:Literal ID="ltlAgeBy" runat="server" Text="Age By"></asp:Literal></td>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblAgeBy" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <asp:ListItem Value="PostDate" Selected="True">Post Date</asp:ListItem>
                <asp:ListItem Value="ValueDate">Value Date</asp:ListItem>
            </asp:RadioButtonList></td>
        <td class="npadminlabel"><asp:Literal ID="ltlInterval" runat="server" Text="Interval"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtInterval" runat="server" Width="75px">30</asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="6" class="npadminbody" align="center"><asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/buttons/search.gif" ToolTip="Submit"></asp:ImageButton></td>
     </tr>
</table>
    <asp:GridView 
        id="gvGLage"
        runat="server" 
        AllowPaging="true"
        PageSize="20"
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        EmptyDataText="No Records Found" OnRowDataBound="gvGLage_RowDataBound">
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:BoundField DataField="AccountName" HeaderText="Account|Account"></asp:BoundField>
            <asp:TemplateField HeaderText="AccountID|AccountID">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:HyperLink ID="lnkAccountID" runat="server" NavigateUrl="~/admin/common/accounts/AccountInfo.aspx?tab=orders&accountid=">AccountID</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interval1|Interval 1">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay ID="prcInterval1" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Interval1") %>' />
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                <FooterTemplate>
                    <np:PriceDisplay ID="prcInterval1Total" runat="server" Price='<%# i1 %>'/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interval2|Interval 2">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay ID="prcInterval2" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Interval2") %>' />
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                <FooterTemplate>
                    <np:PriceDisplay ID="prcInterval2Total" runat="server" Price='<%# i2 %>'/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interval3|Interval 3">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay ID="prcInterval3" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Interval3") %>' />
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                <FooterTemplate>
                    <np:PriceDisplay ID="prcInterval3Total" runat="server" Price='<%# i3 %>'/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interval4|Interval 4">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay ID="prcInterval4" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Interval4") %>' />
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                <FooterTemplate>
                    <np:PriceDisplay ID="prcInterval4Total" runat="server" Price='<%# i4 %>'/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Balance|Balance">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                    <np:PriceDisplay ID="prcBalance" runat="server" Price='<%# DataBinder.Eval(Container, "DataItem.Balance") %>' />
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                <FooterTemplate>
                    <np:PriceDisplay ID="prcBalanceTotal" runat="server" Price='<%# bal %>'/>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
