<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master" CodeBehind="securityroleadd.aspx.cs" Inherits="netpoint.admin.common.security.securityroleadd" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath" width="100%">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveNew" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save Role" OnClick="btnSaveNew_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2">
                <asp:Image id="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif"></asp:Image>&nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Role Detail"></asp:Literal></td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable" width="100%">
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlRoleCode" runat="server" Text="Role Code"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtRoleCode" runat="server" Width="100px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRoleCode" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtRoleCode">
						<img src="../../../assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
                <td class="npadminlabel"><asp:Literal ID="ltlRoleName" runat="server" Text="Role Name"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtRoleName" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRoleName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtRoleName">
						<img src="../../../assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
            </tr>
        </table>
    </div>
</asp:Content>