<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.security.SecurityRoles" Codebehind="SecurityRoles.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteRole" runat="server" ToolTip="Delete Role" ImageUrl="~/assets/common/icons/delete.gif"></asp:ImageButton>&nbsp;
                <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged"></asp:DropDownList>&nbsp;
                <asp:HyperLink ID="lnkAddRole" runat="server" ToolTip="Add Role" ImageUrl="~/assets/common/icons/add.gif"
                    NavigateUrl="~/admin/common/security/securityroleadd.aspx"></asp:HyperLink>&nbsp;
                <asp:ImageButton ID="btnLanguage" runat="server" ToolTip="Role Translations" ImageUrl="~/assets/common/icons/language.gif"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" CssClass="npadmintable" AutoGenerateColumns="false" OnRowDataBound="grid_RowDataBound" OnRowCommand="grid_RowCommand">
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <RowStyle CssClass="npadminbody"></RowStyle>
        <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
         <Columns>
            <asp:TemplateField HeaderText="colObject|Object">
                <ItemTemplate>
                    <asp:Label runat="server" ID="sysObjectName"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colValue|Value">
                <ItemTemplate>
                    <asp:Label runat="server" ID="sysRefDescription"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colCreate|Create">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="imgCreate" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Eval("Create")%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colRead|Read">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="imgRead" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Eval("Read")%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colUpdate|Update">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="imgUpdate" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Eval("Update")%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colDestroy|Destroy">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="imgDestroy" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Eval("Destroy")%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete this permission." CommandArguement='<%# Eval("SRPID")%>'
                        ImageUrl="~/assets/common/icons/delete.gif" CommandName="del"></asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Panel ID="pnlPermissions" runat="server">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="5">&nbsp;<asp:Literal ID="ltlAddPermission" runat="server" Text="Add Permission"></asp:Literal></td>
                <td class="npadminsubheader" align="right"><asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Save" />&nbsp;</td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlObjectType" runat="server" Text="Object Type"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlReference" runat="server" Text="Reference"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlCreate" runat="server" Text="Create"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlRead" runat="server" Text="Read"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlUpdate" runat="server" Text="Update"></asp:Literal></td>
                <td class="npadminlabel"><asp:Literal ID="ltlDestroy" runat="server" Text="Destroy"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminbody">
                    <asp:DropDownList ID="ddlObjectCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlObjectCode_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td class="npadminbody">
                    <asp:DropDownList ID="ddlRefValue" runat="server"></asp:DropDownList>
                    <asp:TextBox ID="txtRefValue" runat="server"></asp:TextBox></td>
                <td class="npadminbody" align="center"><asp:CheckBox ID="chkCreate" runat="server"></asp:CheckBox></td>
                <td class="npadminbody" align="center"><asp:CheckBox ID="chkRead" runat="server"></asp:CheckBox></td>
                <td class="npadminbody" align="center"><asp:CheckBox ID="chkUpdate" runat="server"></asp:CheckBox></td>
                <td class="npadminbody" align="center"><asp:CheckBox ID="chkDestroy" runat="server"></asp:CheckBox></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.  Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDeleteRole" runat="server" Text="The following will be deleted: \n -User Role Assignments \n - Role Permissions \n - The Selected Role \n Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete this Role"></asp:Literal>
</asp:Content>
