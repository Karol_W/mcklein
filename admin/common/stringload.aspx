<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.common.StringLoad" Codebehind="StringLoad.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminlabel" style="width:25%"><asp:Literal ID="ltlUploadFromArchive" runat="server" Text="Upload String Archive"></asp:Literal></td>
            <td><input id="UploadFile" type="file" name="UploadFile" runat="server" /></td>
            <td><asp:ImageButton ID="UploadStringsButton" runat="server" ImageUrl="~/assets/common/icons/upload.gif" ToolTip="Upload Strings File" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:35%"><asp:Literal ID="ltlStringsList" runat="server" Text="Create Strings from Archive"></asp:Literal></td>
            <td><div><asp:DropDownList ID="StringsLists" runat="server"></asp:DropDownList></div></td>
            <td><asp:ImageButton ID="LoadStringsButton" runat="server" ImageUrl="~/assets/common/icons/import.gif" ToolTip="Create New Strings from File" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:35%"><asp:Literal ID="ltlCreateArchive" runat="server" Text="Create a Strings Archive"></asp:Literal></td>
            <td><div><asp:DropDownList ID="ddlEncoding" runat="server"></asp:DropDownList></div></td>
            <td><asp:ImageButton ID="CreateArchiveButton" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Create New Archive File" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:35%"><asp:Literal ID="ltlDeleteStrings" runat="server" Text="Delete a Culture"></asp:Literal></td>
            <td><div><asp:DropDownList ID="ddlEncodingDel" runat="server"></asp:DropDownList></div></td>
            <td><asp:ImageButton ID="DeleteStringsButton" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete a Culture" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Label ID="sysError" runat="server" ForeColor="Red"></asp:Label>
    <asp:Literal ID="hdnStringsExists" runat="server" Text="Strings Exists" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnCultureDeleted" runat="server" Text="Culture Deleted" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnCultureInstalled" runat="server" Text="Culture Installed" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnArchiveCreate" runat="server" Text="Archive Created" Visible="False"></asp:Literal>
</asp:Content>
