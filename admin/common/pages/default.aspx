<%@ Reference Page="~/admin/default.aspx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.pages.pageslandingpage" Codebehind="default.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:DropDownList runat="server" ID="PageType" AutoPostBack="True" OnSelectedIndexChanged="PageType_SelectedIndexChanged"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="Owner" AutoPostBack="True" OnSelectedIndexChanged="Owner_SelectedIndexChanged"></asp:DropDownList>
                <asp:ImageButton runat="server" ToolTip="Add Page" ID="AddPageButton" CommandName="add" ImageUrl="~/assets/common/icons/add.gif" OnClick="AddPageButton_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:gridview 
            ID="PagesGrid" 
            runat="server" 
            AutoGenerateColumns="False" 
            AllowSorting="True" 
            EmptyDataText="No Records Found"
            AllowPaging="True" 
            PageSize="25" 
            CssClass="npadmintable" 
            OnPageIndexChanging="PagesGrid_PageIndexChanging" 
            OnRowDataBound="PagesGrid_RowDataBound" 
            OnRowDeleting="PagesGrid_RowDeleting" 
            OnSorting="PagesGrid_Sorting">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <FooterStyle CssClass="npadminbody"></FooterStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <PagerSettings Position="Bottom" Mode="Numeric" />
                    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif" 
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem,"PageID") %>'></asp:ImageButton>
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:Image runat="server" ToolTip="Visible to Public" ID="VisibleImage" ImageUrl="~/assets/common/icons/availableyes.gif" Visible="False" />
                        <asp:Image runat="server" ToolTip="Not Visible" ID="NotVisibleImage" ImageUrl="~/assets/common/icons/availableno.gif" />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:Image runat="server" ToolTip="Not Approved" ID="NotApprovedImage" ImageUrl="~/assets/common/icons/approvalstop.gif" />
                        <asp:Image runat="server" ToolTip="Approved" ID="ApprovedImage" ImageUrl="~/assets/common/icons/approvalgo.gif" Visible="False" />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:boundfield ItemStyle-Width="10%" DataField="PageCode" HeaderText="colPageCode|Page Code"
                    SortExpression="PageCode"></asp:boundfield>
                <asp:boundfield ItemStyle-Width="45%" DataField="Description" HeaderText="colTitle|Title"
                    SortExpression="Description"></asp:boundfield>
                <asp:boundfield ItemStyle-Width="10%" DataField="UserID" HeaderText="colOwner|Owner" SortExpression="UserID">
                </asp:boundfield>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ToolTip="Edit Page" ImageUrl="~/assets/common/icons/edit.gif"
                            NavigateUrl='<%# "pageeditor.aspx?PageID="+DataBinder.Eval(Container.DataItem,"PageID")%>' ID="lnkEdit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ToolTip="Preview Page" ImageUrl="~/assets/common/icons/preview.gif"
                            Target="_blank" NavigateUrl='<%# "~/common/pagedetail.aspx?PageCode="+DataBinder.Eval(Container.DataItem,"PageCode")%>'
                            ID="lnkPreview"></asp:HyperLink>
                    </ItemTemplate>
                </asp:templatefield>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
    <asp:Literal ID="hdnVisibleToPublic" runat="server" Visible="False" Text="Visible to Public"></asp:Literal>
    <asp:Literal ID="hdnNotVisible" runat="server" Visible="False" Text="Not Visible"></asp:Literal>
    <asp:Literal ID="hdnApproved" runat="server" Visible="False" Text="Approved"></asp:Literal>
    <asp:Literal ID="hdnNotApproved" runat="server" Visible="False" Text="Not Approved"></asp:Literal>
    <asp:Literal ID="hdnEditPage" runat="server" Visible="False" Text="Edit Page"></asp:Literal>
    <asp:Literal ID="hdnPreviewPage" runat="server" Visible="False" Text="Preview Page"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete"></asp:Literal>
    <asp:Literal ID="hdnAddPage" runat="server" Visible="False" Text="Add Page"></asp:Literal>
</asp:Content>
