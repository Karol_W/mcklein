<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.common.pages.pageeditor" Codebehind="pageeditor.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements :  "*[*]"
    });
</script>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="UpdateButton_Click" ValidationGroup="PageSave" /></td>
        </tr>
    </table>
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                 <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"> </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsIndexing" Text="Indexing"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsSEO" Text="SEO"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox EnableViewState="true" ID="Description" runat="server" Width="250"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="val_ContentTitle" ControlToValidate="Description"
                                ErrorMessage="Description is a required field." CssClass="errormsg" Display="Static" ValidationGroup="PageSave">
                                <asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                            </asp:RequiredFieldValidator></td>
                        <td class="npadminbody">
                            <asp:Image ID="imgVisibleToPublic" runat="server" ToolTip="Visible to Public" ImageUrl="~/assets/common/icons/availableyes.gif" />
                            <asp:CheckBox EnableViewState="true" ID="CommunityVisible" runat="server" Text="Visible"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlPageCode" runat="server" Text="PageCode"></asp:Literal></td>
                        <td class="npadminbody" colspan="2">
                            <asp:TextBox EnableViewState="true" ID="PageCode" runat="server" Width="75"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="pcval" ControlToValidate="PageCode"
                                ErrorMessage="PageCode is a required field." CssClass="errormsg" Display="Static" ValidationGroup="PageSave">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                        <td class="npadminbody">
                             <asp:Image runat="server" ToolTip="Approved" ID="ApprovedImage" ImageUrl="~/assets/common/icons/approvalgo.gif" />
                            <asp:CheckBox EnableViewState="true" ID="Approved" runat="server" Text="Approved"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlRelease" runat="server" Text="Release"></asp:Literal></td>
                        <td class="npadminbody"><np:DatePicker ID="ReleaseDate" runat="server"></np:DatePicker></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlLastModified" runat="server" Text="Last Modified"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="DateModified" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlExpires" runat="server" Text="Expires"></asp:Literal></td>
                        <td class="npadminbody"><np:DatePicker ID="ExpireDate" runat="server"></np:DatePicker></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlViews" runat="server" Text="Views" /></td>
                        <td class="npadminbody"><asp:Literal ID="sysViews" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList EnableViewState="true" ID="PageType" runat="server" 
                            DataValueField="CodeValue" DataTextField="CodeName"></asp:DropDownList></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlOwner" runat="server" Text="Owner"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList EnableViewState="true" ID="Owner" runat="server"></asp:DropDownList></td>
                    </tr>

                </table>
                <table class="npadmintable">
                    <tr>
                        <td align="right" class="npadminlabel"><asp:CheckBox runat="server" ID="sysFullView" AutoPostBack="true" OnCheckedChanged="ShowFullView" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" class="npadminlabel">
                            <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                            <asp:TextBox ID="ContentFullSource" runat="server" TextMode="MultiLine" Visible="false"  Columns="65" Rows="20" Wrap="False" /></td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvIndexing">
                <div class="npadminbody" style="text-align:center">
                    <asp:Label runat="server" ID="lblKeywords" Text="Keywords" /><br />
                    <asp:TextBox EnableViewState="true" ID="Keywords" runat="server" Columns="70"></asp:TextBox><br />
                    <asp:Label runat="server" ID="lblBlurb" Text="Blurb" /><br />
                    <asp:TextBox runat="server" ID="PageBlurb" TextMode="MultiLine" Columns="60" Rows="3" />
                </div>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvSEO">
                <asp:Label id="sysError" runat="server" CssClass="npwarning"></asp:Label>    
                <div class="npadminbody" style="text-align:center">
                    <asp:Label runat="server" ID="lblFriendlyURL" Text="Friendly URL" />&nbsp;
                    <asp:TextBox EnableViewState="true" ID="txtFriendlyURL" runat="server" Columns="55" Width="350px"></asp:TextBox><br />                    
                </div>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="sysPageID" runat="server" Visible="false"></asp:Literal>
    <asp:Literal ID="ltlErr" Text="The friendly url you selected already exists; choose another" runat="server" Visible="false"></asp:Literal>
</asp:Content>
