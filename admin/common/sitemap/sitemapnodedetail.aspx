<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" AutoEventWireup="true" CodeBehind="sitemapnodedetail.aspx.cs" Inherits="netpoint.admin.common.sitemap.sitemapnodedetail" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">&nbsp;
                <asp:ImageButton ID="imgSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="imgSave_Click" /></td>
        </tr>
        <tr>
            <td colspan="2" class="npadminsubheader">
                <asp:Literal runat="server" ID="ltlHeader" Text="Site Node Editor"></asp:Literal>
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtTitle" runat="server"></asp:TextBox> 
            &nbsp;<a id="imgLanguage" runat="server"><img src="../../../assets/common/icons/language.gif" alt="" /></a></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlUrlLabel" runat="server" Text="Application Url"></asp:Literal></td>
            <td class="npadminbody"><asp:Literal ID="ltlUrl" runat="server"></asp:Literal>
                <asp:TextBox ID="txtUrl" runat="server" Width="300px" Visible="false"></asp:TextBox>
                &nbsp;<asp:ImageButton ID="imgEditUrl" runat="server" ImageUrl="~/assets/common/icons/edit.gif" OnClick="imgEditUrl_Click" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" text="Description"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtDescription" runat="server" Width="300px" TextMode="MultiLine" Height="60"></asp:TextBox></td>
        </tr>
    </table>
</asp:Content>
