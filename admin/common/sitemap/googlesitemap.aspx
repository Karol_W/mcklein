<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" CodeBehind="googlesitemap.aspx.cs" Inherits="netpoint.admin.common.sitemap.googlesitemap" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Administration - Generate XML Sitemap</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <script type="text/javascript">
        function toggleDisplay() {
            var createDisplay = document.getElementById('ctl00_mainslot_divCreateNew').style.display;
            if (createDisplay == 'none') {
                document.getElementById('ctl00_mainslot_divCreateNew').style.display = 'block';
                document.getElementById('ctl00_mainslot_divExisting').style.display = 'none';
            }
            else {
                document.getElementById('ctl00_mainslot_divCreateNew').style.display = 'none';
                document.getElementById('ctl00_mainslot_divExisting').style.display = 'block';
            }
        }
    </script>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:Literal ID="ltlGoogleSiteMap" runat="server" Text="Generate XML Sitemap"></asp:Literal>
            </td>
        </tr>
    </table>
    <div runat="server" id="divExisting" style="display:block;">
        <table class="npadmintable">
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlPath" Text="Physical Path" runat="server"></asp:Literal>&nbsp;
                </td>
                <td class="npadminlabel">&nbsp;
                    <asp:Literal ID="sysPhysicalPath" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlMapCreated" Text="Sitemap Create Date" runat="server"></asp:Literal>&nbsp;
                </td>
                <td class="npadminlabel">&nbsp;
                    <asp:Literal ID="sysCreateDate" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;<a href="#" title="Create New Sitemap" onclick="toggleDisplay()">Create New Sitemap</a>
                </td>
            </tr>
        </table>
        <div runat="server" id="divLinks">
            <ul>
                <li>&nbsp;</li>
                <li><asp:Literal ID="ltlWebmasterLinks" runat="server" EnableViewState="false" Text="Below are links to register your sitemap with Google, Yahoo and Bing"></asp:Literal></li>
                <li>&nbsp;</li>
                <li><asp:HyperLink ID="lnkGoogle" runat="server" Text="Google Webmaster Central" Target="_blank" NavigateUrl="http://www.google.com/webmasters/"></asp:HyperLink></li>
                <li>&nbsp;</li>
                <li><asp:HyperLink ID="lnkYahoo" runat="server" Text="Yahoo Site Explorer" Target="_blank" NavigateUrl="http://siteexplorer.search.yahoo.com/submit"></asp:HyperLink></li>
                <li>&nbsp;</li>
                <li><asp:HyperLink ID="lnkMSN" runat="server" Text="Bing Webmaster Tools" Target="_blank" NavigateUrl="http://www.bing.com/toolbox/webmaster/"></asp:HyperLink></li>
            </ul>
        </div>
    </div>
    <div runat="server" id="divCreateNew" style="display:none;">
        <table class="npadmintable">
            <tr>    
                <td colspan="2" align="right">
                    <asp:ImageButton ID="imgSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="imgSave_Click" ToolTip="Save Sitemap Configuration" />&nbsp;
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlIncludeStatic" runat="server" Text="Static Pages"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="sysStatic" Text="Include Static Pages" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlFeaturedItems" runat="server" Text="Featured Items"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkFeaturedItems" Text="Include Featured Items" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlDiscounted" runat="server" Text="Discount Pages"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkDiscounted" Text="Include Discount Pages" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlSearchPages" runat="server" Text="Search Pages"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkSearch" Text="Include Search Pages" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" >
                    <asp:Literal ID="ltlCatalogs" runat="server" Text="Categories and Items"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <ComponentArt:TreeView id="tvCatalogs" ClientScriptLocation="~/scripts"
                        DragAndDropEnabled="false" 
                        NodeEditingEnabled="false"
                        KeyboardEnabled="true"
                        CssClass="TreeView" 
                        NodeCssClass="TreeNode" 
                        SelectedNodeCssClass="SelectedTreeNode" 
                        HoverNodeCssClass="HoverTreeNode"
                        NodeEditCssClass="NodeEdit"
                        LineImageWidth="19" 
                        LineImageHeight="20"
                        DefaultImageWidth="16" 
                        DefaultImageHeight="16"
                        ItemSpacing="0" 
                        ImagesBaseUrl="~/assets/common/tree/"
                        NodeLabelPadding="3"
                        ShowLines="true" 
                        LineImagesFolderUrl="~/assets/common/tree/lines/"
                        EnableViewState="true"
                        Visible="false"
                        runat="server" 
                        BorderWidth="0" >
                    </ComponentArt:TreeView>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"></td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkAllParts" runat="server" Text="Select All Categories and Items" OnCheckedChanged="chkAllParts_CheckedChanged" AutoPostBack="True" />
                            </td>
                        </tr>
                    </table>
                </td>      
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlManufacturers" runat="server" Text="Manufacturers"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <ComponentArt:TreeView id="tvManufacturers" ClientScriptLocation="~/scripts"
                        DragAndDropEnabled="false" 
                        NodeEditingEnabled="false"
                        KeyboardEnabled="true"
                        CssClass="TreeView" 
                        NodeCssClass="TreeNode" 
                        SelectedNodeCssClass="SelectedTreeNode" 
                        HoverNodeCssClass="HoverTreeNode"
                        NodeEditCssClass="NodeEdit"
                        LineImageWidth="19" 
                        LineImageHeight="20"
                        DefaultImageWidth="16" 
                        DefaultImageHeight="16"
                        ItemSpacing="0" 
                        ImagesBaseUrl="~/assets/common/tree/"
                        NodeLabelPadding="3"
                        ShowLines="true" 
                        LineImagesFolderUrl="~/assets/common/tree/lines/"
                        EnableViewState="true"
                        Visible="false"
                        runat="server" 
                        BorderWidth="0" >
                    </ComponentArt:TreeView>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"></td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkManufacturers" runat="server" Text="Select All Manufacturers" AutoPostBack="True" OnCheckedChanged="chkManufacturers_CheckedChanged" />
                            </td>
                        </tr>
                    </table>
                </td>      
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlSupport" runat="server" Text="Support Pages"></asp:Literal>
                </td>
                <td class="npadminlabel">
                    <ComponentArt:TreeView id="tvSupport" ClientScriptLocation="~/scripts"
                        DragAndDropEnabled="false" 
                        NodeEditingEnabled="false"
                        KeyboardEnabled="true"
                        CssClass="TreeView" 
                        NodeCssClass="TreeNode" 
                        SelectedNodeCssClass="SelectedTreeNode" 
                        HoverNodeCssClass="HoverTreeNode"
                        NodeEditCssClass="NodeEdit"
                        LineImageWidth="19" 
                        LineImageHeight="20"
                        DefaultImageWidth="16" 
                        DefaultImageHeight="16"
                        ItemSpacing="0" 
                        ImagesBaseUrl="~/assets/common/tree/"
                        NodeLabelPadding="3"
                        ShowLines="true" 
                        LineImagesFolderUrl="~/assets/common/tree/lines/"
                        EnableViewState="true"
                        Visible="false"
                        runat="server" 
                        BorderWidth="0" >
                    </ComponentArt:TreeView>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"></td>
                <td class="npadminlabel">
                    <table>
                        <tr>
                            <td class="TreeNode">
                                <asp:CheckBox ID="chkAllSupport" runat="server" Text="Select All Support Pages" AutoPostBack="True" OnCheckedChanged="chkAllSupport_CheckedChanged" />
                            </td>
                        </tr>
                    </table>
                </td>      
            </tr>
        </table>
    </div>
    <asp:Literal runat="server" ID="ltlNoMap" Text="No sitemap created" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="ltlIncludeParts" Text="Include Category Items" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="ltlSupportDefault" Text="Include Default Support Page" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="ltlArticle" Text="Include Support Articles" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="ltlMnf" Text="Include Default Manufacturer Page" Visible="false"></asp:Literal>
</asp:Content>
