<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" CodeBehind="sitemap.aspx.cs" Inherits="netpoint.admin.common.sitemap.sitemap" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <script type="text/javascript">
    </script>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <ComponentArt:TreeView ID="treeNodes" DragAndDropEnabled="false" NodeEditingEnabled="false" ClientScriptLocation="~/scripts"
        KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
        HoverNodeCssClass="HoverTreeNode" NodeEditCssClass="NodeEdit" LineImageWidth="19" AutoPostBackOnSelect="true" 
        LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
        ImagesBaseUrl="~/assets/common/tree/" NodeLabelPadding="3" ParentNodeImageUrl="folder.gif"
        LeafNodeImageUrl="file.gif" ShowLines="true" LineImagesFolderUrl="~/assets/common/tree/lines/"
        EnableViewState="true" runat="server" OnNodeSelected="treeNodes_NodeSelected">
    </ComponentArt:TreeView>
</asp:Content>
