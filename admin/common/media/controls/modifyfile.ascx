<%@ Control Language="c#" Inherits="netpoint.admin.common.media.controls.ModifyFile" Codebehind="ModifyFile.ascx.cs" %>
<table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0" class="npadminbody">
	<tr>
		<td class="npadminlabel"><asp:Label id="sysError" runat="server" CssClass="npwarning" Visible="False"></asp:Label></td>
	</tr>
	<tr>
		<td class="npadminlabel">
			<asp:literal ID="ltlSaveAs" Runat="server" Text="Save As"></asp:literal>:&nbsp;
			<asp:TextBox id="txtSaveAs" runat="server" Width="449px" Columns="60"></asp:TextBox></td>
	</tr>
	<tr>
		<td align="center"><asp:TextBox id="txtContents" runat="server" TextMode="MultiLine" Width="100%" Height="400px" Rows="25" Columns="72"></asp:TextBox></td>
	</tr>
	<tr>
		<td align="center" class="npadminlabel">
			<asp:Button id="btnSave" runat="server" Text="Save" Enabled="False" onclick="btnSave_Click"></asp:Button>
			<asp:Button id="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></asp:Button></td>
	</tr>
</table>
<asp:Literal id="hdnFileTooLarge" runat="server" Text="File is too large" Visible="False"></asp:Literal>
