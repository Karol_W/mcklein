<%@ Reference Control="~/admin/common/media/controls/modifyfile.ascx" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.admin.common.media.controls._Default" Codebehind="Default.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="npadminbody">
<ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
    MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
    DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
    DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
    <ItemLooks>
        <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
            LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
            LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
    </ItemLooks>
    <Tabs>
        <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
        <ComponentArt:TabStripTab runat="server" ID="tsOther" Text="Other"></ComponentArt:TabStripTab>
    </Tabs>
</ComponentArt:TabStrip>
<ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
    Width="100%" Height="400">
    <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" style="width:5%;">
                    <asp:Label ID="lblDir" runat="server"></asp:Label>&nbsp;<asp:Label ID="sysError" runat="server"></asp:Label></td>
                <td class="npadminsubheader" valign="top" align="right">
                    <asp:ImageButton ID="btnRoot" runat="server" ToolTip="Root Directory" ImageUrl="~/assets/common/icons/rootfolder.gif" OnClick="btnRoot_Click" />&nbsp;
                    <asp:ImageButton ID="btnGoUp" runat="server" ToolTip="Up One Level" ImageUrl="~/assets/common/icons/uplevel.gif" OnClick="btnGoUp_Click" />&nbsp;
                    <asp:ImageButton ID="btnRefresh" runat="server" ToolTip="Refresh" ImageUrl="~/assets/common/icons/refresh.gif" OnClick="btnRefresh_Click" />&nbsp;</td>
            </tr>
        </table>
        <table class="npadmintable">
            <tr>
                <td class="npadminlabel" colspan="2" align="left">
                    <asp:ImageButton ID="btnCopy" runat="server" ToolTip="Copy" OnClick="btnCopy_Click" ImageUrl="~/assets/common/editoricons/copy_off.gif" />
                    <asp:ImageButton ID="btnCut" runat="server" OnClick="btnCut_Click" ToolTip="Cut" ImageUrl="~/assets/common/editoricons/cut_off.gif" />
                    <asp:ImageButton ID="btnPaste" runat="server" OnClick="btnPaste_Click" ToolTip="Paste" ImageUrl="~/assets/common/editoricons/paste_off.gif" />
                    <asp:ImageButton ID="imgDelete" runat="server" ToolTip="Delete" OnClick="imgDelete_Click" ImageUrl="~/assets/common/editoricons/delete_off.gif" />&nbsp;
                    <asp:Label ID="sysBuffer" runat="server"></asp:Label>&nbsp;
                    <asp:Literal ID="ltlZipName" Text="Zip Name" runat="server"></asp:Literal>:
                    <asp:TextBox ID="txtZipName" runat="server" MaxLength="50"></asp:TextBox>&nbsp;
                    <asp:ImageButton ID="btnZip" runat="server" ToolTip="Add to Zip File" OnClick="btnZip_Click" ImageUrl="~/assets/common/icons/compress.gif" /></td>
                <td class="npadminlabel" align="right">
                    <asp:Literal ID="ltlPageSize" Text="Page Size" runat="server"></asp:Literal>:
                    <asp:DropDownList ID="drpPageSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPageSize_SelectedIndexChanged">
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="40">40</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="100" Selected="True">100</asp:ListItem>
                        <asp:ListItem Value="200">200</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3" class="npadminlabel">
                    <asp:GridView ID="dgFiles" runat="server" ShowFooter="False" AllowPaging="True" PageSize="100" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Files Found"
                        OnRowDataBound="dgFiles_RowDataBound" OnRowCommand="dgFiles_RowCommand" OnRowUpdating="dgFiles_RowUpdating"
                        OnRowEditing="dgFiles_RowEditing" OnRowCreated="dgFiles_RowCreated" OnPageIndexChanging="dgFiles_PageIndexChanging"
                         OnRowCancelingEdit="dgFiles_RowCancelingEdit" OnSorting="dgFiles_Sorting" >
                        <SelectedRowStyle BackColor="#FFFFCC" />
                        <RowStyle CssClass="npadminbody" />
                        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <PagerStyle CssClass="npadminbody" />
                        <EmptyDataRowStyle CssClass="npadminempty" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkChecked" runat="server" Checked='<%#ViewState["IsChecked"]%>'></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgType" runat="server"></asp:Image>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="filename" HeaderText="colName|Name">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnUnzip" runat="server" ImageUrl="~/assets/common/icons/uncompress.gif"
                                        ToolTip="Unzip" CommandName="UnzipIt" Visible="False" />
                                    <asp:ImageButton ID="btnModify" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                                        ToolTip="Modify" CommandName="ModifyIt" Visible="False" />
                                    <asp:HyperLink ID="lnkPreview" runat="server" ImageUrl="~/assets/common/icons/image.gif"
                                        Visible="False" Target="_blank" ToolTip="Preview"></asp:HyperLink>
                                    <asp:LinkButton ID="lnkName" runat="server" CommandName="ItemClicked" Text='<%# DataBinder.Eval(Container, "DataItem.filename") %>'
                                        CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEditName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "filename") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="size" SortExpression="size" ReadOnly="True" HeaderText="colKB|KB">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="5%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="modified" SortExpression="modified" ReadOnly="True" HeaderText="colLastModified|Last Modified">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundField>
                            <asp:CommandField EditImageUrl="~/assets/common/icons/edit.gif" ShowEditButton="true" ButtonType="Image" 
                                UpdateText="Update" UpdateImageUrl="~/assets/common/icons/save.gif" CancelImageUrl="~/assets/common/icons/cancel.gif" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </ComponentArt:PageView>
    <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvOther">
        <table class="npadmintable">
            <tr>
                <td class="npadminlabel" align="right">
                    <asp:Literal ID="ltlCreateDirectory" runat="server" Text="Create Directory"></asp:Literal>
                    <asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtDirName" runat="server" Columns="50">
                    </asp:TextBox>
                    <asp:ImageButton ID="btnNewDir" ImageUrl="~/assets/common/icons/upload.gif" runat="server" OnClick="btnNewDir_Click" ToolTip="Create" /></td>
            </tr>
            <tr>
                <td class="npadminlabel" align="right">
                    <asp:Literal ID="ltlUpload" runat="server" Text="Upload File">
                    </asp:Literal>&nbsp;</td>
                <td>
                    <input id="file1" type="file" size="37" name="file1" />
                    <asp:ImageButton ID="btnUpload" ImageUrl="~/assets/common/icons/upload.gif" runat="server" OnClick="btnUpload_Click" ToolTip="Upload" /></td>
            </tr>
        </table>
    </ComponentArt:PageView>
</ComponentArt:MultiPage>
</div>
<asp:Literal ID="hdnDeleteMessage" runat="server" Text="All checked folders and thier contents will be deleted.  Continue?" Visible="False"></asp:Literal>
<asp:Literal ID="hdnObjectsInBuffer" runat="server" Text="object(s) in a buffer" Visible="False"></asp:Literal>
<asp:Literal ID="errCannotMove" runat="server" Text="Cannot move above the root folder" Visible="False"></asp:Literal>
<asp:Literal ID="hdnDownloadStarted" runat="server" Text="Download started on another thread. Refresh the page to see the progress." Visible="False"></asp:Literal>
<asp:Literal ID="hdnDownloaded" runat="server" Text="download" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCopied" runat="server" Text="Copied" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCut" runat="server" Text="Cut" Visible="False"></asp:Literal>
<asp:Literal ID="errInvalidFileName" runat="server" Text="Zip file name  if invalid.  Please enter another." Visible="False"></asp:Literal>
<asp:Literal ID="hdnUnzip" runat="server" Text="Unzip" Visible="False"></asp:Literal>
<asp:Literal ID="hdnModify" runat="server" Text="Modify" Visible="False"></asp:Literal>
<asp:Literal ID="hdnPreview" runat="server" Text="Preview" Visible="False"></asp:Literal>
