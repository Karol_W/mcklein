<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#"
    Inherits="netpoint.admin.common.media.mediamainpage" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="Default" Src="controls/Default.ascx" %>
<%@ Register TagPrefix="np" TagName="ModifyFile" Src="controls/ModifyFile.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Upload, edit and delete files and folders</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Upload, edit and delete files and folders" /></td>
        </tr>
    </table>
    <div class="npadminbody">
		<np:Default id="media" runat="server" Visible="False"></np:Default>
		<np:ModifyFile id="modify" runat="server" Visible="False"></np:ModifyFile>
	</div>
</asp:Content>
