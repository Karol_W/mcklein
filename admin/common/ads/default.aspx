<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.ads._default" Codebehind="default.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Banner Ad Management*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
    <tr>
        <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Create and edit Banner Ads(use Banner Ad plugin in theme slot to display in theme)" /></td>
        <td class="npadminheader" align="right">
            <asp:dropdownlist runat="server" id="ddlAdType" autopostback="True" onselectedindexchanged="ddlAdType_SelectedIndexChanged" />&nbsp;
            <asp:Hyperlink runat="server" ToolTip="Add" ID="AddButton" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="addetail.aspx" />&nbsp;</td>
    </tr>
</table>
<div class="npadminbody">
    <asp:gridview id="AdsGrid" runat="server" autogeneratecolumns="False"
        allowsorting="True" allowpaging="True" pagesize="50" cssclass="npadmintable" 
        OnPageIndexChanging="AdsGrid_PageIndexChanging" OnRowDeleting="AdsGrid_RowDeleting" OnSorting="AdsGrid_Sorting"
        OnRowDataBound="AdsGrid_RowDataBound"  EmptyDataText="No Advertisements Created">
		<RowStyle CssClass="npadminbody" />
		<AlternatingRowStyle CssClass="npadminbodyalt" />
		<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
		<PagerStyle CssClass="npadminbody" />
		<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
		<FooterStyle CssClass="npadminbody"></FooterStyle>
		<Columns>
			<asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
				<ItemTemplate>
					<asp:ImageButton id="btnDelete" runat="server" ToolTip="Delete" commandname="delete" ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AdID") %>'></asp:ImageButton>
				</ItemTemplate>
			</asp:templatefield>
			<asp:boundfield ItemStyle-Width="10%" DataField="AdID" HeaderText="colAdID|Ad ID" SortExpression="AdID"></asp:boundfield>
			<asp:boundfield ItemStyle-Width="25%" DataField="StartDate" HeaderText="colStartDate|Start Date" SortExpression="StartDate"></asp:boundfield>
			<asp:boundfield ItemStyle-Width="45%" DataField="TagLine" HeaderText="colTagLine|Tag Line"></asp:boundfield>
			<asp:boundfield ItemStyle-Width="10%" DataField="ClientID" HeaderText="colClientID|Client ID" SortExpression="ClientID"></asp:boundfield>
			<asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="center">
				<ItemTemplate>
					<asp:HyperLink runat="server" ToolTip="Edit" ImageUrl="~/assets/common/icons/edit.gif" NavigateUrl='<%# "addetail.aspx?AdID="+DataBinder.Eval(Container.DataItem,"AdID")%>' ID="lnkEdit"></asp:HyperLink>
				</ItemTemplate>
			</asp:templatefield>
		</Columns>
	</asp:gridview>
</div>  
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="Hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="This banner ad and all of location data will be deleted.\nContinue?"></asp:Literal>
</asp:Content>