<%@ Control Language="C#" AutoEventWireup="true" Codebehind="adlocation.ascx.cs"
    Inherits="netpoint.admin.common.ads.controls.adlocation" %>
<asp:GridView ID="LocationsGrid" runat="server" Width="100%" AutoGenerateColumns="False"
    AllowSorting="True" AllowPaging="True" PageSize="25" CssClass="npadmintable"
    EmptyDataText="No Ad Locations Defined" OnPageIndexChanging="LocationsGrid_PageIndexChanging"
    OnRowCancelingEdit="LocationsGrid_RowCancelingEdit" OnRowDeleting="LocationsGrid_RowDeleting"
    OnRowEditing="LocationsGrid_RowEditing" OnRowUpdating="LocationsGrid_RowUpdating"
    OnSorting="LocationsGrid_Sorting" OnRowCommand="LocationsGrid_RowCommand" OnRowDataBound="LocationsGrid_RowDataBound">
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <PagerStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <Columns>
        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="delete"
                    ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"rapid") %>'>
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colLocationMatch|Location Match" SortExpression="LocationMatch">
            <ItemStyle Width="70%"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlLocationMatch" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LocationMatch") %>'>
                </asp:Literal>
            </ItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="tbLocationMatchNew" runat="server" Columns="60"></asp:TextBox>
            </FooterTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="tbLocationMatch" runat="server" Columns="60" Text='<%# DataBinder.Eval(Container, "DataItem.LocationMatch") %>'>
                </asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colSortCode|SortCode" SortExpression="SortCode">
            <ItemStyle Width="20%"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlSortCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SortCode") %>'>
                </asp:Literal>
            </ItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="tbSortCodeNew" runat="server" Columns="5"></asp:TextBox>
            </FooterTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="tbSortCode" runat="server" Columns="5" Text='<%# DataBinder.Eval(Container, "DataItem.SortCode") %>'>
                </asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle Width="5%" HorizontalAlign="Center" Wrap="false"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rapid") %>' CommandName="edit">
                </asp:ImageButton>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnSaveCode" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rapid") %>' CommandName="update">
                </asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                    CommandName="cancel"></asp:ImageButton>
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="npadminbody" style="text-align:right">
<asp:ImageButton ID="btnNewCode" runat="server" ImageUrl="~/assets/common/icons/add.gif"
         tooltip="New Code"  CommandName="add" OnClick="btnNewCode_Click"></asp:ImageButton>&nbsp;
</div>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.\nContinue?" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete this location" Visible="False"></asp:Literal>
<asp:literal ID="hdnEdit" runat="server" Text="Edit this location" Visible="false"></asp:literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save this location" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCancel" runat="server" Text="Cancel edit" Visible="false"></asp:Literal>

