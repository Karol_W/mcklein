<%@ Control Language="C#" AutoEventWireup="true" Codebehind="adgeneral.ascx.cs" Inherits="netpoint.admin.common.ads.controls.adgeneral" %>

<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblAdID" runat="server" Text="Ad ID"></asp:Label>
        </td>
        <td>
            <asp:Literal ID="ltlAdID" runat="server" Text="0"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblAdType" runat="server" Text="Ad Type"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlAdType" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblTagLine" runat="server" Text="TagLine"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbTagLine" runat="server" Rows="3" Columns="55" TextMode="multiline"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblDaes" runat="server" Text="Dates (Start/End)"></asp:Label>
        </td>
        <td>
            <np:datepicker id="dpStartDate" runat="server">
                    </np:datepicker>
            <np:datepicker id="dpEndDate" runat="server">
                    </np:datepicker>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblClientID" runat="server" Text="ClientID"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbClientID" runat="server" Columns="30"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblCampaignCode" runat="server" Text="Campaign Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbCampaignCode" runat="server" Columns="30"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblClicks" runat="server" Text="Clicks / Max"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbClicks" runat="server" Columns="5"></asp:TextBox>
            /
            <asp:TextBox ID="tbMaxClicks" runat="server" Columns="5"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblPictureUrl" runat="server" Text="Picture URL"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbPictureUrl" runat="server" Columns="65"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblClickUrl" runat="server" Text="Click URL"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbClickUrl" runat="server" Columns="65"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Label ID="lblSourceCode" runat="server" Text="Source Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbSourceCode" runat="server" Rows="6" Columns="55" TextMode="multiline"></asp:TextBox>
        </td>
    </tr>
</table>
