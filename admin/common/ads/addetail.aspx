<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.ads.addetail" ValidateRequest="false" Codebehind="addetail.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="adgeneral" Src="~/admin/common/ads/controls/adgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="adlocation" Src="~/admin/common/ads/controls/adlocation.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td>&nbsp;</td> 
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete" OnClick="btnDelete_Click"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ToolTip="Save" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="AdTabStrip" ImagesBaseUrl="~/assets/common/admin/" MultiPageId="AdMultiPage" ClientScriptLocation="~/scripts"
            Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs" DefaultItemLookId="TopLevelTabLook"
            DefaultSelectedItemLookId="SelectedTopLevelTabLook" DefaultChildSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLocations" Text="Locations"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="AdMultiPage" CssClass="MultiPage" runat="server" Width="100%" ClientScriptLocation="~/scripts"
            Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <np:adgeneral ID="general" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:adlocation ID="location" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="This banner ad and all of location data will be deleted.\nContinue?"></asp:Literal>
</asp:Content>