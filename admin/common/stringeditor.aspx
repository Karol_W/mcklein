<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" ValidateRequest="false" Inherits="netpoint.admin.common.StringEditor" Codebehind="StringEditor.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td align="right">
                <asp:HyperLink runat="server" ID="lnkLoad" ImageUrl="~/assets/common/icons/upload.gif"
                    ToolTip="Import/Export Strings" NavigateUrl="StringLoad.aspx" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="4"><asp:Literal ID="ltlHeader" runat="server" Text="zed eCommerce String Editor"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlFilters" runat="server" Text="Filters"></asp:Literal></td>
                <td colspan="2" class="npadminbody">
                    <br />
                    <asp:Literal ID="ltlNamespace" runat="server" Text="Namespace"></asp:Literal><br />
                    <asp:DropDownList ID="ddlNamespace" runat="server"></asp:DropDownList><br />
                    <br />
                    <asp:CheckBox ID="chkShowEdited" runat="server" Text="Show Edited" TextAlign="Left" CssClass="npadminbody"></asp:CheckBox>&nbsp;
                    <asp:CheckBox ID="chkShowOK" runat="server" Text="Show Accepted" TextAlign="Left" CssClass="npadminbody"></asp:CheckBox><br /></td>
                <td class="npadminlabel" align="center">
                    <asp:ImageButton ID="btnSearch" runat="server" ToolTip="Search" ImageUrl="~/assets/common/icons/search.gif" /></td>
            </tr>
            <tr>
                <td class="npadminsubheader" colspan="3"><asp:Literal ID="ltlResults" runat="server" Text="Results"></asp:Literal></td>
                <td class="npadminsubheader" align="right"><asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save" /></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlReference" runat="server" Text="Reference Encoding"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlReference" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReference_SelectedIndexChanged"></asp:DropDownList></td>
                <td class="npadminlabel"><asp:Literal ID="ltlDisplay" runat="server" Text="Edit Encoding"></asp:Literal></td>
                <td class="npadminbody"><asp:Literal ID="sysEncoding" runat="server"></asp:Literal></td>
            </tr>
        </table>
        <asp:gridview ID="grid" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            AllowSorting="True" PageSize="15" CssClass="npadmintable" EmptyDataText="No Strings Match Criteria" 
            OnPageIndexChanging="grid_PageIndexChanging" OnRowDataBound="grid_RowDataBound" OnSorting="grid_Sorting">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <PagerStyle CssClass="npadminbody" />
            <Columns>
                <asp:templatefield SortExpression="ReferenceString" HeaderText="colRefString|Ref. String">
                    <ItemTemplate>
                        <asp:Literal ID="ltlRefString" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReferenceString") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnControl" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ObjectType") %>' />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield HeaderText="colNamespace|Namespace" SortExpression="sen.Namespace">
                    <ItemTemplate>
                        <asp:Literal ID="ltlNS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Namespace") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield SortExpression="StringValue" HeaderText="colDisplay|Display">
                    <ItemTemplate>
                        <asp:TextBox ID="txtEncodingValue" runat="server" Columns="30" Text='<%# DataBinder.Eval(Container.DataItem, "StringValue") %>'></asp:TextBox>
                        <asp:Literal ID="ltlOldEncodingValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StringValue") %>' Visible="false" />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield SortExpression="Edited" HeaderText="colEditted|Edited">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Image ID="imgEdited" runat="server" ImageUrl="~/assets/common/icons/checked.gif" />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:templatefield SortExpression="Accepted" HeaderText="colOK|OK">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkOK" runat="server"></asp:CheckBox>
                        <asp:CheckBox ID="chkWasOK" runat="server" Visible="false" />
                    </ItemTemplate>
                </asp:templatefield>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
