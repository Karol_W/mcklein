<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="uicontrol.aspx.cs" Inherits="netpoint.admin.common.control" %>

<%@ Register TagPrefix="np" TagName="controledit"  Src="~/admin/common/controls/npcontrol.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:Imagebutton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDelete_Click" ToolTip="Delete" />
                &nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save" OnClick="btnSave_Click" ></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npdaminbody">
        <div class="npadminsubheader"></div>
        <np:controledit ID="theControl" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="Hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.  Continue?"></asp:Literal>
</asp:Content>
