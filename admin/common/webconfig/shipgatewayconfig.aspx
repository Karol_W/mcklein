<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.master" CodeBehind="shipgatewayconfig.aspx.cs" Inherits="netpoint.admin.common.webconfig.shipgatewayconfig" %>
<%@ Register TagPrefix="np" TagName="config" Src="~/admin/common/webconfig/controls/webconfig.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<np:config ID="webconfig" runat="server" DropDownFilter="NetPoint.ShippingProvider" />
</asp:Content>
