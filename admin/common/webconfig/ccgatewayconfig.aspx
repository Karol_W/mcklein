<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.master" CodeBehind="ccgatewayconfig.aspx.cs" Inherits="netpoint.admin.common.webconfig.ccgatewayconfig" %>
<%@ Register TagPrefix="np" TagName="config" Src="~/admin/common/webconfig/controls/webconfig.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<np:config ID="webconfig" runat="server" DropDownFilter="netpoint.paymentprovider" />
<asp:Panel ID="disableFields" runat="server" Enabled="false" Visible="false">
<np:config ID="firewall" runat="server" DropDownFilter="Firewall"/>
</asp:Panel>
</asp:Content>
