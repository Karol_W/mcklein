<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.master" CodeBehind="webconfigs.aspx.cs" Inherits="netpoint.admin.common.webconfig.webconfigs" %>
<%@ Register TagPrefix="np" TagName="config" Src="~/admin/common/webconfig/controls/webconfig.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<np:config ID="webconfig" runat="server" SortGroup="common" />
</asp:Content>
