<%@ Control Language="C#" AutoEventWireup="true" Codebehind="webconfig.ascx.cs" Inherits="netpoint.admin.common.webconfig.controls.webconfig" %>
<asp:Panel ID="pnlAppDropDown" runat="server">
    <table class="npadmintable" width="100%">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:Literal ID="ltlConfigLabel" runat="server" Text="Choose a provider" Visible="false"></asp:Literal>
                <asp:DropDownList ID="ddlTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTypes_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ImageButton ID="imgSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    OnClick="imgSaveAll_Click" /></td>
        </tr>
    </table>
</asp:Panel>
<table id="config-table" width="100%" cellpadding="0" cellspacing="0" border="1">
    <asp:Repeater ID="rptConfigs" runat="server" OnItemDataBound="rptConfigs_ItemDataBound">
        <ItemTemplate>
            <tr>
                <td class="npadminlabel" style="width: 35%; white-space:normal;" valign="top">
                    <asp:Literal ID="ltlConfigKey" runat="server"></asp:Literal>
                    <asp:Literal ID="ltlName" runat="server"></asp:Literal></td>                  
                <td class="npadminsmall" align="left" valign="top">
                    <asp:Literal ID="ltlID" runat="server" Visible="false"></asp:Literal>
                    <asp:TextBox ID="txtValue" runat="server" Visible="false" Columns="50" MaxLength="255" Width="200px"></asp:TextBox>
                    <asp:CheckBox ID="chkValue" runat="server" Visible="false" />
                    <asp:DropDownList ID="ddlValue" runat="server" Visible="false">
                    </asp:DropDownList>
                    <div id="divPassword" visible="false" runat="server">
                        <%=ltlNewPassword.Text%>
                        <br />
                        <asp:TextBox ID="txtPass1" runat="server" TextMode="Password" Columns="50" MaxLength="255" Width="200px"></asp:TextBox>
                        <br />
                        <%=ltlConfPassword.Text%>
                        <br />
                        <asp:TextBox ID="txtPass2" runat="server" TextMode="Password" Columns="50" MaxLength="255" Width="200px"></asp:TextBox>
                        <br />
                        <asp:CompareValidator ID="cvPassword" runat="server" ControlToCompare="txtPass1"
                            ControlToValidate="txtPass2" Display="Dynamic">
                            <%=errPassword.Text%><br />
                        </asp:CompareValidator>
                        <span>
                            <input type="checkbox" id="chkNoPass" runat="server" />&nbsp;<%=ltlNoPassword.Text%></span>
                    </div>
                    <br />
                    <asp:Literal ID="ltlDescription" runat="server"></asp:Literal>&nbsp;&nbsp;</td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>
<asp:Literal ID="errPermissions" runat="server" Text="Only user with admin permissions can view this page."
    Visible="false"></asp:Literal>
<asp:Literal ID="errValue" runat="server" Text=" must be assigned a value." Visible="false"></asp:Literal>
<asp:Literal ID="ltlNewPassword" runat="server" Text="New password" Visible="false"></asp:Literal>
<asp:Literal ID="ltlConfPassword" runat="server" Text="Confirm password" Visible="false"></asp:Literal>
<asp:Literal ID="errPassword" runat="server" Text="Passwords do not match" Visible="false"></asp:Literal>
<asp:Literal ID="ltlNoPassword" runat="server" Text="No password" Visible="false"></asp:Literal>
