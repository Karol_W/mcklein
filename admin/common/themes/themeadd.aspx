<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="themeadd.aspx.cs" Inherits="netpoint.admin.common.themes.themeadd" %>


<%@ Register TagPrefix="np" TagName="themegeneral" Src="~/admin/common/themes/controls/themegeneral.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

    <table class="npadmintable" cellpadding="0" cellspacing="0">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
               <asp:ImageButton ID="UpdateButton"  runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save and Close" Visible="true" OnClick="UpdateButton_Click"></asp:ImageButton>
                &nbsp;
            </td>
            </tr>
            <tr>
                <td colspan="2">                    
                    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
                </td>
            </tr>
    </table>
    <div class="npadminsubheader">
        &nbsp;
        <asp:literal ID="ltlHeader" runat="server" Text="Add a New Theme"></asp:literal>
    </div>
    <div class="npadminbody">
        <np:themegeneral ID="general" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hiddenslot" runat="server">
</asp:Content>
