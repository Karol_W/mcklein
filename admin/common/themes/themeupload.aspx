<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.themes.themeupload" Codebehind="themeupload.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" ForeColor="Red"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader" colspan="3">&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="3"><asp:Literal ID="ltlHeader" runat="server" Text="Load Theme Data from a ZIP file"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:25%;"><asp:Literal ID="ltlUploadFromArchive" runat="server" Text="Upload Theme Archive"></asp:Literal></td>
            <td><input id="UploadFile" type="file" name="UploadFile" runat="server" /></td>
            <td><asp:ImageButton ID="UploadThemeButton" runat="server" ImageUrl="~/assets/common/icons/upload.gif" ToolTip="Upload Theme File" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:35%;"><asp:Literal ID="ltlThemesList" runat="server" Text="Create Theme from Archive"></asp:Literal></td>
            <td style="height:25;"><div id="formdiv1"><asp:DropDownList ID="ThemesLists" runat="server"></asp:DropDownList></div></td>
            <td><asp:ImageButton ID="LoadThemeButton" runat="server" ImageUrl="~/assets/common/icons/import.gif" ToolTip="Create New Theme from File" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnThemeExists" runat="server" Text="Theme already exists" Visible="False"></asp:Literal>
</asp:Content>
