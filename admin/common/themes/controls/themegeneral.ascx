<%@ Control Language="C#" AutoEventWireup="true" Codebehind="themegeneral.ascx.cs" Inherits="netpoint.admin.common.themes.controls.themegeneral" %>  
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable" cellspacing="0" cellpadding="2">
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlServerID" runat="server" Text="ThemeID"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtServerID" runat="server" EnableViewState="true" Columns="20"></asp:TextBox>
            <asp:RequiredFieldValidator ID="val_ServerID" runat="server" ControlToValidate="txtServerID"
                ErrorMessage="ServerID is a required field." CssClass="errormsg" Display="Static">
                <asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
            <asp:CheckBox ID="sysDefaultServer" runat="server" Text="Default Theme"></asp:CheckBox></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="ServerName" runat="server" EnableViewState="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="val_ServerName" runat="server" ControlToValidate="ServerName"
                ErrorMessage="Name is a required field." CssClass="errormsg" Display="Static">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
            <asp:CheckBox ID="ExpressCheckout" runat="server" Text="Enable Express Checkout"></asp:CheckBox></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlAssociatedDomain" runat="server" Text="Associated Domain"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="AssociatedDomain" runat="server" EnableViewState="true" Width="500"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlCatalog" runat="server" Text="Catalog"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlAssociatedCulture" runat="server" Text="Associated Culture"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="CultureList" runat="server" EnableViewState="true"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlCurrencyHeader" runat="server" Text="Currency Display" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlDisplayCurrency" runat="server" Text="Default Currency" /></td>
        <td><asp:DropDownList ID="ddlDisplayCurrency" runat="server" /></td>
    </tr>
    <tr>
        <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlTaxHeader" runat="server" Text="Tax Display" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlAddTaxToPrice" runat="server" Text="Add Tax to Price" /></td>
        <td class="npadminbody"><asp:CheckBox ID="chkAddTaxToPrice" runat="server" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlShowItemizedTax" runat="server" Text="Show Itemized Tax" /></td>
        <td class="npadminbody"><asp:CheckBox ID="chkShowItemizedTax" runat="server" /></td>
    </tr>
</table>
<asp:Literal ID="hdnFail" runat="server" Visible="False" Text="Failed to create directory"></asp:Literal>
<asp:Literal ID="hdnMultipleDefault" runat="server" Text="A default theme already exists for this domain." Visible="False"></asp:Literal>
<asp:Literal ID="hdnDuplicateID" runat="server" Visible="False" Text="A theme with the ID you have specified already exists."></asp:Literal>