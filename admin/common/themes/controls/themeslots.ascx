<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="themeslots.ascx.cs" Inherits="netpoint.admin.common.themes.controls.themeslots" %>



<asp:gridview ID="SlotsGrid" runat="server" AllowSorting="True" CssClass="npadmintable"
     AutoGenerateColumns="False" EmptyDataText="No Slots in Theme" 
     OnPageIndexChanging="SlotsGrid_PageIndexChanging" 
     OnRowDataBound="SlotsGrid_RowDataBound" OnRowDeleting="SlotsGrid_RowDeleting" OnSorting="SlotsGrid_Sorting">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <FooterStyle CssClass="npadminbody"></FooterStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle CssClass="npamdinempty" Height="50px" />
    <PagerStyle CssClass="npadminbody" />
    <Columns>
        <asp:templatefield SortExpression="SlotPosition,SortCode" HeaderText="colPosition|Position">
            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
            <ItemTemplate>
                <asp:Label ID="SlotPosition" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:templatefield>
        <asp:boundfield DataField="SortCode" SortExpression="SortCode" HeaderText="colSort|Sort"
            DataFormatString="{0}">
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
        </asp:boundfield>
        <asp:templatefield SortExpression="SectionType,SortCode" HeaderText="colType|Type">
            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
            <ItemTemplate>
                <asp:Label runat="server" ID="SectionType" />
            </ItemTemplate>
        </asp:templatefield>
        <asp:HyperLinkField DataTextField="SectionName" SortExpression="SectionName" HeaderText="colName|Name" 
             DataNavigateUrlFields="SectionID" DataNavigateUrlFormatString="~/admin/common/themes/themeslotdetail.aspx?slotid={0}">
            <ItemStyle Width="65%"></ItemStyle>
        </asp:HyperLinkField>
        <asp:templatefield>
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete this slot." ></asp:ImageButton>
            </ItemTemplate>
        </asp:templatefield>
    </Columns>
</asp:gridview>
<br />
<div class="npadminactionbar">
<asp:HyperLink runat="Server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/common/themes/themeslotdetail.aspx?themeid=" ToolTip="New"></asp:HyperLink>
</div>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit Slot" Visible="False"></asp:Literal>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent.  Continue?"
    Visible="False"></asp:Literal>
<asp:Literal ID="hdnDeleteSlot" runat="server" Visible="False" Text="Delete Slot"></asp:Literal>
<asp:Literal ID="errDoesNotExist" runat="server" Visible="False" Text="does not exist in the zed eCommerce database."></asp:Literal>
<asp:Literal ID="hdnError" runat="server" Visible="False" Text="ERROR"></asp:Literal>