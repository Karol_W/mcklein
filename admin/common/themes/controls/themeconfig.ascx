﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="themeconfig.ascx.cs" Inherits="netpoint.admin.common.themes.controls.themeconfig" %>
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable" cellspacing="0" cellpadding="2" width="100%">
    <tr>
        <td class="npadminlabel" align="right" width="25%"><asp:Literal ID="ltlLoginRequired" runat="server" Text="Login Required" /></td>
        <td class="npadminbody"><asp:CheckBox ID="chkLoginRequired" runat="server" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlAccountIdPrefix" runat="server" Text="Account Id Prefix"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtAccountIdPrefix" runat="server" EnableViewState="true"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlDisplayCheckoutNotes" runat="server" Text="Display Checkout Notes" /></td>
        <td class="npadminbody"><asp:CheckBox ID="chkDisplayCheckoutNotes" runat="server" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlProductThumbnailSize" runat="server" Text="Product Thumbnail Size"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtProductThumbnailSize" runat="server" EnableViewState="true"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlProductPreviewSize" runat="server" Text="Product Preview Size"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtProductPreviewSize" runat="server" EnableViewState="true"></asp:TextBox></td>
    </tr>  
    <tr>
        <td class="npadminlabel" align="right"><asp:Literal ID="ltlV7Compatible" runat="server" Text="Version 7 Theme"></asp:Literal></td>
        <td class="npadminbody"><asp:CheckBox ID="chkV7Compatible" runat="server" /></td>
    </tr>    
</table>
<asp:Literal ID="hdnProductThumbSizeError" runat="server" Visible="False" Text="Product thumbnail size must be a valid integer"></asp:Literal>
<asp:Literal ID="hdnProductPreviewSizeError" runat="server" Visible="False" Text="Product preview size must be a valid integer"></asp:Literal>
