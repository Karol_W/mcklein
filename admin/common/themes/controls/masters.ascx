<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="masters.ascx.cs" Inherits="netpoint.admin.common.themes.controls.masters" %>
<asp:GridView runat="server" ID="gvMasters" AutoGenerateColumns="False" OnRowDataBound="gvMasters_OnDataBound" 
    CssClass="npadmintable" EmptyDataText="No Records Found">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <FooterStyle CssClass="npadminbody" />
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:BoundField DataField="LastWriteTime" HeaderText="colLastModified|Last Modified" />
        <asp:TemplateField HeaderText="colFileName|File Name">
            <ItemTemplate>
                <asp:HyperLink ID="lnkEditFile" runat="server"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit this Master" Visible="False"></asp:Literal>
