<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stylesheet.ascx.cs" Inherits="netpoint.admin.common.themes.controls.stylesheet" %>
<table width="100%">
<tr>
<td align="Center" class="npadminbody">
<asp:TextBox runat="server" ID="tbStyleSheetEdit" TextMode="MultiLine" Wrap="false" Columns="70" Rows="25"></asp:TextBox>
</td>
</tr>
</table>
<asp:Literal ID="ltlNoPermissions" runat="server" Visible="false" 
Text="The application does not have write permission to the assets directory. Add write permission to save a theme." />
