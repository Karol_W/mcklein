<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.common.themes.themedetail" Codebehind="themedetail.aspx.cs" %>
<%@ Register Src="controls/themeslots.ascx" TagName="slots" TagPrefix="np" %>
<%@ Register Src="controls/themegeneral.ascx" TagName="themegeneral" TagPrefix="np" %>
<%@ Register Src="controls/masters.ascx" TagName="masters" TagPrefix="np" %>
<%@ Register Src="controls/stylesheet.ascx" TagName="stylesheet" TagPrefix="np" %>
<%@ Register Src="controls/themeconfig.ascx" TagName="configs" TagPrefix="np" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

    <table class="npadminpath" cellpadding="0" cellspacing="0">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="deletebutton" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete Theme " OnClick="deletebutton_Click" />&nbsp;
                <asp:ImageButton ID="duplicatebutton" runat="server" ImageUrl="~/assets/common/icons/duplicate.gif" ToolTip="Duplicate Theme" OnClick="duplicatebutton_Click" />&nbsp;
                <asp:ImageButton ID="archivebutton" runat="server" ImageUrl="~/assets/common/icons/compress.gif" ToolTip="Create Theme Archive" />&nbsp;
                <asp:ImageButton ID="UpdateButton" OnClick="SavePage" runat="server" ImageUrl="~/assets/common/icons/save.png"
                    ToolTip="Save and Close" Visible="true" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label></td>
        </tr>
    </table> 
    <ComponentArt:TabStrip ID="TabStrip1" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="MultiPage1" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="TabStripTab1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="TabStripTab2" Text="Slots"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="TabStripTab3" Text="Masters"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="TabStripTab4" Text="StyleSheet"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="TabStripTab5" Text="Config Settings"></ComponentArt:TabStripTab>
        </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage id="MultiPage1" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%">
        <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvGeneral">
            <np:themegeneral ID="general" runat="server" />
        </ComponentArt:PageView>

        <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvSlots">
            <np:slots ID="slots" runat="server"></np:slots>
        </ComponentArt:PageView>

        <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvMasters">
            <np:masters ID="masters" runat="server"></np:masters>
        </ComponentArt:PageView>

        <ComponentArt:PageView CssClass="PageContent" runat="server">
            <np:stylesheet ID="stylesheet" runat="server"></np:stylesheet>       
        </ComponentArt:PageView>
        
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:configs ID="configs" runat="server"></np:configs>       
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnFail" runat="server" Visible="False" Text="Failed to create directory"></asp:Literal>
    <asp:Literal ID="hdnMultipleDefault" runat="server" Text="A default theme already exists for this domain." Visible="False"></asp:Literal>
    <asp:Literal ID="ltlConfirmDelete" runat="server" Visible="false" Text="Deletions are permanent. Continue?" />
</asp:Content>
