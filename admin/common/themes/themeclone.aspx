<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.themes.themeClone" Codebehind="themeClone.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader" colspan="2">&nbsp;</td>
            <td align="right" class="npadminheader"><asp:imagebutton id="btnSave" runat="server" imageurl="~/assets/common/icons/save.gif" ToolTip="Save" /></td>
        </tr>
        </table>
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="2"></td>
            </tr>
            <tr>
                <td class="npadminlabel" style="width:25%;">
                    <asp:literal id="ltlServerID" runat="server" text="New ThemeID"></asp:literal>
                    <asp:requiredfieldvalidator id="rfvServerID" runat="server" errormessage="RequiredFieldValidator" controltovalidate="txtServerID">
					    <asp:Image id="imgValid" runat="server" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image></asp:requiredfieldvalidator></td>
                <td><asp:textbox id="txtServerID" runat="server" Columns="20"></asp:textbox></td>
            </tr>
        </table>
    <asp:label id="sysError" runat="server" cssclass="npwarning"></asp:label>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnFail" runat="server" text="Failed to copy theme." visible="False"></asp:literal>
</asp:Content>
