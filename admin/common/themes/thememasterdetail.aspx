<%@ Page ValidateRequest="false" MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.themes.thememasterdetail" Codebehind="thememasterdetail.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable" width="100%">
    <tr>
        <td class="npadminheader" align="left">
            &nbsp;
        </td>
        <td class="npadminheader" align="right">
            <asp:ImageButton runat="server" ID="btnEdit" ImageUrl="~/assets/common/icons/edit.gif" OnClick="btnEdit_Click" ToolTip="Edit" />
            &nbsp;
            <asp:ImageButton runat="server" ID="btnSave" ImageUrl="~/assets/common/icons/saveandclose.gif"
                Visible="false" OnClick="btnSave_Click" ToolTip="Save" />
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="npadminsubheader" colspan="2">
            <asp:Literal ID="ltlSubHeader" runat="server" Text="Edit Master"></asp:Literal>
        </td>
    </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="npadmintable">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlServerID" runat="server" Text="ServerID"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="sysServerID" runat="server" Text=""></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMasterPath" runat="server" Text="Master Path"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="sysMasterPath" runat="server" Text=""></asp:Literal>
            &nbsp;
            <asp:ImageButton runat="server" ID="btnDelete" ImageUrl="~/assets/common/icons/delete.gif"
                OnClick="btnDelete_Click" ToolTip="Delete" />
            </td>
    </tr>
    <tr>
        <td colspan="2" class="npadminbody">
            <asp:TextBox runat="server" ID="tbMasterEdit" TextMode="MultiLine" Wrap="false" Columns="72" Rows="25"></asp:TextBox>
        </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit this Master" Visible="False"></asp:Literal>
</asp:Content>
