<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.themes.themeslandingpage" CodeBehind="default.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
        <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="UploadThemeButton" runat="server" NavigateUrl="themeupload.aspx"
                    ImageUrl="~/assets/common/icons/upload.gif" Width="20" ToolTip="Upload Theme"></asp:HyperLink>&nbsp;
                <asp:HyperLink runat="server" ToolTip="New Theme" ID="AddThemeButton" ImageUrl="~/assets/common/icons/add.gif"  NavigateUrl="themeadd.aspx" Width="20" /></td>
        </tr>
        </table>
        <asp:Label CssClass="nperror" ID="errMaxThemes" runat="server" Text="Max. Themes Reached" Visible="false" />
        <table class="npadmintable" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <asp:gridview ID="ThemesGrid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" 
                    EmptyDataText="No Themes Found" ShowFooter="False" AllowSorting="True" PageSize="25"
                    AllowPaging="True" OnPageIndexChanging="ThemesGrid_PageIndexChanging" 
                    OnRowDataBound="ThemesGrid_RowDataBound" OnSorting="ThemesGrid_Sorting">
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                    <Columns>
                        <asp:HyperLinkField DataTextField="ServerID" SortExpression="ServerID" HeaderText="colID|ID"
                            DataTextFormatString="{0}" DataNavigateUrlFields="ServerID" DataNavigateUrlFormatString="themedetail.aspx?ThemeID={0}">
                            <ItemStyle Width="15%"></ItemStyle>
                        </asp:HyperLinkField>
                        <asp:boundfield DataField="ServerName" SortExpression="ServerName" HeaderText="colName|Name" DataFormatString="{0}">
                            <ItemStyle Width="60%"></ItemStyle>
                        </asp:boundfield>
                        <asp:templatefield HeaderText="colDefault|Default">
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="imgDefault" runat="server" ImageUrl="~/assets/common/icons/checked.gif"></asp:Image>
                            </ItemTemplate>
                        </asp:templatefield>
                        <asp:templatefield>
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkPreview" runat="server" ToolTip="Preview" ImageUrl="~/assets/common/icons/preview.gif" Target="_blank"
                                    NavigateUrl='<%# "~/default.aspx?serverid="+DataBinder.Eval(Container.DataItem,"ServerID")%>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:templatefield>
                    </Columns>
                    <PagerSettings Position="Bottom" Mode="Numeric" />
                    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
                </asp:gridview>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteTheme" runat="server" Text="Selected theme, all slots, and associated theme images will be permanently deleted. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnNewTheme" runat="server" Text="New Theme" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEditTheme" runat="server" Text="Edit Theme" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnUploadTheme" runat="server" Text="Upload Theme" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnSlots" runat="server" Text="Slots" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDuplicate" runat="server" Text="Duplicate" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnPreview" runat="server" Text="Preview" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDefaultServer" runat="server" Visible="False" Text="Default Theme"></asp:Literal>
</asp:Content>
