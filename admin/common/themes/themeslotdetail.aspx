<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.common.themes.themeslotdetail" Codebehind="themeslotdetail.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]",
        forced_root_block : ""
    });
</script><%-- The forced_root_block setting forces the root wrapper block for unformatted content to be "" (or blank) instead of the default "<p>". 
This is to support "<head>" content in the 'header' slot position --%>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="UpdateButton" OnClick="SavePage" runat="server" 
                ImageUrl="~/assets/common/icons/saveandclose.gif" Visible="true" />&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="4"></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right"><asp:Literal ID="ltlServerID" runat="server" Text="ServerID"></asp:Literal></td>
            <td colspan="3"><asp:DropDownList ID="ddlServerID" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlSlotName" runat="server" Text="Slot Name"></asp:Literal></td>
            <td>
                <asp:TextBox ID="SectionName" runat="server" Columns="25" EnableViewState="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_SectionName" runat="server" Display="Static" CssClass="errormsg" 
                    ErrorMessage="SectionName is a required field." ControlToValidate="SectionName">
                    <asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
                <asp:CheckBox ID="chkShowSectionName" runat="server" Visible="false" Text="Show Name"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlRestrictedByRole" runat="server" Text="Restricted By Role"></asp:Literal></td>
            <td><asp:CheckBox ID="sysRoleRestricted" runat="server"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlSlotPosition" runat="server" Text="Position"></asp:Literal></td>
            <td><asp:DropDownList ID="SlotPosition" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlSortCode" runat="server" Text="Sort Code"></asp:Literal></td>
            <td>
                <asp:TextBox ID="SortCode" runat="server" Columns="4" EnableViewState="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSort" runat="server" ControlToValidate="SortCode"
                    ErrorMessage="RequiredFieldValidator">
                    <asp:Image runat="Server" ID="imgWarn" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="ltlSlotType" runat="server" Text="Slot Type"></asp:Literal></td>
            <td><asp:DropDownList ID="SectionType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SectionType_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">&nbsp;<asp:Literal ID="sysFeed" runat="server" Text="Feed Url"></asp:Literal></td>
            <td>
                <asp:TextBox ID="SectionFeedUrl" runat="server" Columns="25" EnableViewState="true"></asp:TextBox>&nbsp;
                <asp:DropDownList ID="ddlPlugins" runat="server"></asp:DropDownList>
                <asp:DropDownList ID="ddlPage" runat="server"></asp:DropDownList></td>
        </tr>
    </table>
    <asp:Label ID="sysError" runat="server" ForeColor="red" Font-Bold="True" Font-Size="Larger"></asp:Label>
    <asp:Panel ID="pnlSource" runat="server">
        <table class="npadmintable">
            <tr style="text-align:top">
                <td class="npadminsubheader" colspan="2">
                    <asp:Button ID="btnSourceCode" runat="server" AutoPostBack="true" OnClick="ShowFullView" Height="22px" ></asp:Button>
                    <asp:CheckBox ID="sysFullView" runat="server" Visible="false"></asp:CheckBox>
                    <asp:Literal ID="ltlSourceCode" runat="server" Text="Show Source Code" Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" align="center" colspan="2">
                    <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    <asp:TextBox ID="SectionFullSourceCode" runat="server" Visible="false" TextMode="MultiLine"
                        Columns="70" Rows="20" Wrap="False"></asp:TextBox></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnPluginName" runat="server" Visible="False" Text="Plugin Name"></asp:Literal><asp:Literal ID="hdnPage" runat="server" Visible="False" Text="Page"></asp:Literal><asp:Literal ID="hdnFeedURL" runat="server" Visible="False" Text="Feed URL"></asp:Literal>
    <asp:Literal ID="errBadConfig" runat="server" Visible="False" Text="Plugins can not be placed in header or footer."></asp:Literal>
    <asp:Literal ID="errMustBeNumber" runat="server" Visible="False" Text="must be a number"></asp:Literal>
    <asp:Literal ID="showHTML" runat="server" Visible="false" Text="Show HTML"></asp:Literal>
    <asp:Literal ID="hideHTML" runat="server" Visible="false" Text="Hide HTML"></asp:Literal>
</asp:Content>
