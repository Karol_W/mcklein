<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.codes.CountryList"
    Codebehind="CountryList.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    ToolTip="Save All" OnClick="btnUpdate_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="2"></td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlEncoding" runat="server" Text="Culture"></asp:Literal>
                </td>
                <td class="npadminbody">
                    <asp:DropDownList ID="ddlEncoding" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEncoding_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <asp:gridview 
            ID="gvCountries" 
            runat="server" 
            CssClass="npadmintable" 
            PageSize="20"
            AllowPaging="True" 
            AllowSorting="True" 
            AutoGenerateColumns="False" 
            EmptyDataText="No Records Found" OnPageIndexChanging="gvCountries_PageIndexChanging" OnRowCommand="gvCountries_RowCommand" OnRowDataBound="gvCountries_RowDataBound">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:BoundField DataField="CountryCode" HeaderText="colCode|Code" ItemStyle-HorizontalAlign="center"></asp:BoundField>
                <asp:TemplateField HeaderText="colCountryName|Country Name">
                    <ItemTemplate>
                        <asp:TextBox ID="txtCountryName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CountryName") %>'
                            Width="200px">
                        </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colShipping|Delivery">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkValidForShipping" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.ValidForShipping") %>'>
                        </asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colBilling|Billing">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkValidForBilling" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.ValidForBilling") %>'>
                        </asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colStates|States">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnStates" runat="server" ImageUrl="../../../assets/common/icons/detail.gif"
                            CommandName="states" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CountryCode") %>'>
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview>
        <asp:Panel ID="pnlAssociate" runat="server">
            <table class="npadmintable">
                <tr>
                    <td class="npadminbody" colspan="2">
                        <br />
                        <br />
                        <asp:Literal ID="ltlNoRecords" runat="server" Text="No Countries Found"></asp:Literal><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Literal ID="ltlAddCountries" runat="server" Text="Add Countries"></asp:Literal></td>
                    <td class="npadminbody">
                        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="../../../assets/common/icons/add.gif"
                            ToolTip="Add New Country"></asp:ImageButton></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnEditStates" runat="server" Text="Edit States" Visible="False"></asp:Literal>
</asp:Content>
