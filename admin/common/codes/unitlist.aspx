<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.codes.UnitList" Codebehind="UnitList.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                  <asp:DropDownList ID="ddlUnitType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUnitType_SelectedIndexChanged">
                  </asp:DropDownList>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">      
        <asp:Label ID="lblErr" runat="server" CssClass="npwarning" Visible="false"></asp:Label> 
        <asp:GridView 
            ID="gvUnits" 
            runat="server" 
            AutoGenerateColumns="false" 
            CssClass="npadmintable" 
            EmptyDataRowStyle-CssClass="npadminempty"
            emptydatatext="No Records Found" 
            RowStyle-CssClass="npadminbody" 
            AlternatingRowStyle-CssClass="npadminbodyalt" 
            HeaderStyle-CssClass="npadminsubheader" 
            ShowFooter="true" 
            OnRowEditing="gvUnits_RowEditing" 
            OnRowDataBound="gvUnits_RowDataBound" OnRowCommand="gvUnits_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" Visible='<%# DataBinder.Eval(Container, "DataItem.Removable") %>'
                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UnitCode") %>' ImageUrl="../../../assets/common/icons/delete.gif"
                            CommandName="remove" />
                    </ItemTemplate>
                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colUnitCode|Unit Code">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UnitCode") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtUnitCodeNew" runat="server" MaxLength="10"></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colUnitAbbr|Unit Abbr.">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UnitAbbr") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtUnitAbbrNew" runat="server" MaxLength="10"></asp:TextBox>
                    </FooterTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUnitAbbr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UnitAbbr") %>' MaxLength="10"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colUnitName|Unit Name">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UnitName") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtUnitNameNew" runat="server"></asp:TextBox>
                    </FooterTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUnitName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UnitName") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colTranslation|Translation">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnTranslation" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UnitCode") %>'
                            ImageUrl="../../../assets/common/icons/language.gif" CommandName="translation" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="../../../assets/common/icons/edit.gif"
                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UnitCode") %>' />
                    </ItemTemplate>
                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                    <FooterTemplate>
                        <asp:ImageButton ID="btnNew" runat="server" CommandName="new" ImageUrl="../../../assets/common/icons/add.gif" />
                    </FooterTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="btnSave" runat="server" CommandName="saveunit" ImageUrl="../../../assets/common/icons/save.gif"
                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.UnitCode") %>' />&nbsp;
                        <asp:ImageButton ID="btnCancel" runat="server" CommandName="canceledit" ImageUrl="../../../assets/common/icons/cancel.gif" />
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>      
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete this Unit" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit this Unit" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Text="Add a New Unit" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnLanguage" runat="server" Text="Language" Visible="False"></asp:Literal>
    <asp:Literal ID="errRequired" runat="server" Text="Unit Code, Abbreviation and Name are all required." Visible="false"></asp:Literal>
</asp:Content>
