<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.codes.codes" Codebehind="default.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="Mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:DropDownList ID="ddlCodeType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCodeType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" CssClass="npadmintable" 
        DataKeyNames="CodeID"
        OnRowDataBound="grid_RowDataBound" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowDeleting="grid_RowDeleting" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
        <RowStyle CssClass="npadminbody" />
        <HeaderStyle CssClass="npadminsubheader" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
         <Columns>
            <asp:TemplateField HeaderText="colType|Type">
                <ItemStyle Width="20%"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal ID="ltlItemType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CodeType") %>'>
                    </asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlItemType" runat="server">
                    </asp:DropDownList>
                    <asp:Literal ID="ltlItemTypeDisplay" runat="server" Visible="False"></asp:Literal>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colValue|Value">
                <ItemStyle Width="20%" HorizontalAlign="center"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal ID="ltlValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CodeValue") %>'>
                    </asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Literal ID="ltlValueDisplay" runat="server" Visible="False"></asp:Literal>
                    <asp:TextBox ID="txtValue" runat="server" Columns="8" MaxLength="50" Style="text-align:center" Text='<%# DataBinder.Eval(Container, "DataItem.CodeValue") %>'>
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colSort|Sort">
                <ItemStyle Width="10%" HorizontalAlign="center"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal ID="ltlSortCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SortCode") %>'>
                    </asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtSortCode" runat="server" Style="text-align:center" Columns="3" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.SortCode") %>'>
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colName|Name">
                <ItemStyle Width="45%"></ItemStyle>
                <ItemTemplate>
                    <asp:Literal ID="ltlCodeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CodeName") %>'>
                    </asp:Literal>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtCodeName" runat="server" Columns="30" Text='<%# DataBinder.Eval(Container, "DataItem.CodeName") %>'>
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colSysVal|Sys. Val.">
                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                <ItemTemplate>
                    <asp:Image ID="SystemCodeImage" runat="server" Visible="false" ImageUrl="~/assets/common/icons/checked.gif">
                    </asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="5%"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CodeID") %>' CommandName="delete">
                    </asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnCOS" runat="server" ImageUrl="~/assets/common/icons/language.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CodeID") %>' CommandName="cos">
                    </asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CodeID") %>' CommandName="edit">
                    </asp:ImageButton>
                </ItemTemplate>
               <EditItemTemplate>
                    <asp:ImageButton ID="btnSaveCode" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CodeID") %>' CommandName="update">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                        CommandName="cancel"></asp:ImageButton>
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table class="npadmintable">
        <tr>
            <td class="npadminbody">
                 <asp:TextBox ID="txtCodeType" runat="server" Columns="10"></asp:TextBox>
            </td>
            <td class="npadminbody">
                <asp:TextBox ID="txtValueNew" runat="server" Columns="8" MaxLength="50" Style="text-align:center"></asp:TextBox>
            </td>
            <td class="npadminbody">
                 <asp:TextBox ID="txtSortCodeNew" runat="server" Columns="3" MaxLength="5" Style="text-align:center" Text="0"></asp:TextBox>
            </td>
            <td class="npadminbody">
                <asp:TextBox ID="txtCodeNameNew" runat="server" Columns="30"></asp:TextBox>
            </td>
            <td class="npadminbody">
                 <asp:ImageButton ID="btnNewCode" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnNewCode_Click" ToolTip="New Code"></asp:ImageButton>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="Hiddenslot">
    <asp:Literal ID="hdnCodeName" runat="server" Visible="False" Text="Code Name"></asp:Literal>
    <asp:Literal ID="hdnAlternateLanguageText" runat="server" Visible="False" Text="Alternate Language Text"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete this Code"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit this Code"></asp:Literal>
    <asp:Literal ID="hdnSave" runat="server" Visible="False" Text="Save changes"></asp:Literal>
    <asp:Literal ID="hdnLanguage" runat="server" Visible="False" Text="Language"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add a New Code"></asp:Literal>
</asp:Content>
