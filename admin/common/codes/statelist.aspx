<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.codes.StateList" Codebehind="StateList.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">&nbsp;&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close" OnClick="btnSaveAll_Click"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <asp:Panel ID="pnlStates" runat="server">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader"><asp:Literal ID="sysHeader" runat="server"></asp:Literal></td>
                <td class="npadminsubheader" align="right"><asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save" />&nbsp;</td>
            </tr>
        </table>
        <asp:GridView ID="grid" runat="server" CssClass="npadmintable" AutoGenerateColumns="false" ShowFooter="false" OnRowDataBound="grid_RowDataBound">
            <RowStyle CssClass="npadminbody"></RowStyle>
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
             <Columns>
                <asp:TemplateField HeaderText="colCode|Code">
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:Literal ID="ltlStateCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StateCode") %>'></asp:Literal>
                        <asp:TextBox ID="txtStateCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StateCode") %>' Width="50"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colStateName|State Name">
                    <ItemTemplate>
                        <asp:TextBox ID="txtStateName" runat="server" Width="200px" Text='<%# DataBinder.Eval(Container, "DataItem.StateName") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colShipping|Delivery">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkValidForShipping" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.ValidForShipping") %>'></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colBilling|Billing">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkValidForBilling" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.ValidForBilling") %>'></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlAddState" runat="server">
        <div class="npadminbody" style="text-align: right">
            <asp:Literal ID="lblCopyStates" runat="server" Text="Copy States From"></asp:Literal>&nbsp;
            <asp:DropDownList ID="ddlEncoding" runat="server"></asp:DropDownList>&nbsp;
            <asp:ImageButton ID="btnAddStates" runat="server" ImageUrl="../../../assets/common/icons/add.gif" ToolTip="Add New State"></asp:ImageButton>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnStatesFor" runat="server" Visible="False" Text="States for"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete this state"></asp:Literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent.  Continue?"></asp:Literal>
</asp:Content>
