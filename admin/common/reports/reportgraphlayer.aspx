<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.reports.ReportGraphLayer" Codebehind="ReportGraphLayer.aspx.cs" %>

<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete General" OnClick="btnDelete_Click"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlHeader" runat="server" Text="Edit Layer"></asp:Literal></td>
         </tr>
    </table>
    
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td colspan="4">
                <asp:label id="sysError" runat="server" forecolor="red" font-bold="true"></asp:label>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlType" runat="server" text="Type"></asp:literal>
            </td>
            <td colspan="3">
                <asp:dropdownlist id="ddlGraphType" runat="server"></asp:dropdownlist>
            </td>
            
        </tr>
        <tr>
        <td class="npadminlabel">
                <asp:literal id="ltlDataColumn" runat="server" text="Data Column"></asp:literal>
            </td>
            <td colspan="3">
                <asp:dropdownlist id="ddlDataColumn" runat="server"></asp:dropdownlist>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlDataColor" runat="server" text="Data Color"></asp:literal>
            </td>
            <td>
                <NP:HtmlColorDropDown ID="ddDataColor" runat="server" AutoColorChangeSupport="true"
                    SortOption="ByValue" Palette="WebSafe"></NP:HtmlColorDropDown></td>
            <td class="npadminlabel">
                <asp:literal id="ltlDepth" runat="server" text="Depth"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtDepth" runat="server" width="80px"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlLegend" runat="server" text="Legend"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtLegendName" runat="server" width="80px"></asp:textbox>
            </td>
            <td class="npadminlabel">
                <asp:literal id="ltlLineWidth" runat="server" text="LineWidth"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtLineWidth" runat="server" width="80px"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td class="npadminheader" colspan="4">
                <asp:literal id="ltlSymbolHeader" runat="server" text="Data Symbols"></asp:literal>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlSymbol" runat="server" text="Symbol"></asp:literal>
            </td>
            <td>
                <asp:dropdownlist id="ddlSymbol" runat="server"></asp:dropdownlist>
            </td>
            <td class="npadminlabel">
                <asp:literal id="ltlSymbolSize" runat="server" text="SymbolSize"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtSymbolSize" runat="server" width="80px"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlColor" runat="server" text="Color"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:literal id="ltlFill" runat="server" text="Fill"></asp:literal>
                &nbsp;
                <NP:HtmlColorDropDown ID="ddSymbolFillColor" runat="server" AutoColorChangeSupport="true"
                    SortOption="ByValue" Palette="WebSafe"></NP:HtmlColorDropDown>&nbsp;
                <asp:literal id="ltlEdge" runat="server" text="Edge"></asp:literal>
                &nbsp;
                <NP:HtmlColorDropDown ID="ddSymbolEdgeColor" runat="server" AutoColorChangeSupport="true"
                    SortOption="ByValue" Palette="WebSafe"></NP:HtmlColorDropDown>
            </td>
            <td class="npadminlabel">
                <asp:literal id="ltlSymbolLineWidth" runat="server" text="Symbol Line Width"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtSymbolLineWidth" runat="server" width="80px"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td class="npadminheader" colspan="4">
                <asp:literal id="ltlLabelHeader" runat="server" text="Data Labels"></asp:literal>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlIncludeLabel" runat="server" text="Include Label"></asp:literal>
            </td>
            <td colspan="3">
                <asp:checkbox id="sysLabelFlag" runat="server"></asp:checkbox>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlFont" runat="server" text="Font"></asp:literal>
            </td>
            <td colspan="3" class="npadminbody">
                <asp:dropdownlist id="ddlLabelFont" runat="server"></asp:dropdownlist>
                <asp:checkbox id="chkFontBold" runat="server" text="Bold Font" textalign="Left"></asp:checkbox>
                <asp:checkbox id="chkFontItalic" runat="server" text="Italic Font" textalign="Left"></asp:checkbox>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlFontAngle" runat="server" text="Font Angle"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtFontAngle" runat="server" width="80px"></asp:textbox>
            </td>
            <td class="npadminlabel">
                <asp:literal id="ltlLabelSize" runat="server" text="Label Size"></asp:literal>
            </td>
            <td>
                <asp:textbox id="txtLabelSize" runat="server" width="80px"></asp:textbox>
            </td>
          </tr>
    </table>
    </div>
    <asp:literal id="errMustBeNumber" runat="server" text="Must be a number." visible="False"></asp:literal>
    
</asp:Content>
