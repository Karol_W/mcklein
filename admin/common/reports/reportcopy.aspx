<%@ Page MasterPageFile="~/masters/admin.Master" Language="C#" CodeBehind="reportcopy.aspx.cs" Inherits="netpoint.admin.common.reports.reportcopy" AutoEventWireup="true"%>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadmintable">
    <tr>
        <td class="npadminheader">
            &nbsp;
        </td>
        <td class="npadminheader" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSave_Click" ToolTip="Save"></asp:ImageButton>
        </td>
     </tr>
     <tr>
        <td class="npadminsubheader" colspan="2">
            <asp:Literal ID="ltlHeader" runat="server" Text="Copy"></asp:Literal>
        </td>
      </tr>
</table>
<table class="npadmintable">
     <tr>
        <td class="npadminlabel" style="width: 20%;"><asp:Literal ID="ltlReportCode" runat="server" Text="Report Code"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtReportCode" runat="server"></asp:TextBox>
            <asp:Image ID="imgErr" runat="server" ImageUrl="~/assets/common/icons/warning.gif"
                Visible="False" /></td>
      </tr>
       <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlReportName" runat="server" Text="Report Name"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtReportName" runat="server"></asp:TextBox></td>
      </tr>
</table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
 <asp:literal id="hdnArgMessage" runat="server" visible="False" text="Invalid request: the report code was not provided."></asp:literal>
 <asp:literal id="hdnReportExists" runat="server" visible="False" text="Your selected Report Code has already been used."></asp:literal>
</asp:Content>