<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.reports.ReportUpload" Codebehind="ReportUpload.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%"
        border="0">
        <tr>
            <td class="npadminheader" colspan="3">
                <asp:hyperlink id="lnkCancel" runat="server" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:hyperlink>&nbsp;
                <asp:literal id="ltlHeader" runat="server" text="Upload Report"></asp:literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:25%;"><asp:literal id="ltlUploadFromArchive" runat="server" text="Upload Report Archive"></asp:literal></td>
            <td><input id="UploadFile" type="file" name="UploadFile" runat="server" /></td>
            <td><asp:imagebutton id="UploadReportButton" runat="server" imageurl="~/assets/common/icons/upload.gif" tooltip="Upload Report File" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" style="width:35%;"><asp:literal id="ltlReportsList" runat="server" text="Create Report from Archive"></asp:literal></td>
            <td style="height:25;"><div id="formdiv1"><asp:dropdownlist id="ReportsLists" runat="server"></asp:dropdownlist></div></td>
            <td><asp:imagebutton id="LoadReportButton" runat="server" imageurl="~/assets/common/icons/import.gif" tooltip="Create New Report from File" /></td>
        </tr>
    </table>
    <br />
    <br />
    <asp:label id="sysError" runat="server" forecolor="Red"></asp:label>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnReportExists" runat="server" text="Report already exists" visible="False"></asp:literal>
</asp:Content>
