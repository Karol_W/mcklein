<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.reports.ReportGraphAxis" Codebehind="ReportGraphAxis.aspx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" cssclass="npadminwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlSubHeader" runat="server" Text="Edit Axis"></asp:Literal></td>
        </tr>
    </table>
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td align="right"><asp:DropDownList ID="ddlAxis" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAxis_SelectedIndexChanged"></asp:DropDownList></td>
            <td class="npadminlabel" colspan="3"><asp:Literal ID="ltlHeader" runat="server" Text="Axis data for"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminheader" colspan="4"><asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlText" runat="server" Text="Text"></asp:Literal></td>
            <td colspan="3"><asp:TextBox ID="txtTitle" runat="server" Width="400px" Columns="55"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTitleFont" runat="server" Text="Font"></asp:Literal></td>
            <td colspan="3"><asp:DropDownList ID="ddlTitleFont" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTitleSize" runat="server" Text="Font Size"></asp:Literal></td>
            <td><asp:TextBox ID="txtTitleFontSize" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlTitleFontColor" runat="server" Text="Title Font Color"></asp:Literal></td>
            <td><NP:HtmlColorDropDown ID="ddTitleColor" runat="server" Palette="WebSafe" SortOption="ByValue"
                AutoColorChangeSupport="true"></NP:HtmlColorDropDown></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlTitleAlignment" runat="server" Text="Title Alignment"></asp:Literal></td>
            <td><asp:DropDownList ID="ddlTitleAlignment" runat="server"></asp:DropDownList></td>
            <td class="npadminlabel"><asp:Literal ID="ltlTitleGap" runat="server" Text="Title Gap"></asp:Literal></td>
            <td><asp:TextBox ID="txtTitleGap" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminheader" colspan="4"><asp:Literal ID="ltlTic" runat="server" Text="Tic"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlLowerLimit" runat="server" Text="Lower Limit"></asp:Literal></td>
            <td><asp:TextBox ID="txtLowerLimit" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlUpperLimit" runat="server" Text="Upper Limit"></asp:Literal></td>
            <td><asp:TextBox ID="txtUpperLimit" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlMajorTicInterval" runat="server" Text="Major Tic Interval"></asp:Literal></td>
            <td><asp:TextBox ID="txtMajorTicInterval" runat="server"></asp:TextBox></td>
            <td class="npadminlabel"><asp:Literal ID="ltlMinorTicInterval" runat="server" Text="Minor Tic Interval"></asp:Literal></td>
            <td><asp:TextBox ID="txtMinorTicInterval" runat="server"></asp:TextBox></td>
        </tr>
    </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errMustBeNumber" runat="server" Visible="False" Text="Must be a number."></asp:Literal>
</asp:Content>
