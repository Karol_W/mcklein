<%@ Page Language="c#" MasterPageFile="~/masters/admindefault.master" Inherits="netpoint.admin.common.reports.ReportLaunch" Codebehind="ReportLaunch.aspx.cs" %>
<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">
    <table cellpadding="1" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="npadminheader"><asp:Literal ID="sysReportName" runat="server" /></td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkCancel" runat="server" NavigateUrl="ReportList.aspx" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel" /></td>
        </tr>
        <tr>
            <td class="npadminbody" colspan="2"><asp:Table ID="tblControls" runat="server" CellPadding="1" CellSpacing="0" /></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="center" colspan="2">
                <asp:Literal ID="sysStep" runat="server" Visible="False" Text="0" />&nbsp;
                <asp:ImageButton ID="btnPDF" runat="server" Visible="False" ImageUrl="~/assets/common/icons/pdficon.gif" OnClick="btnPDF_Click" ToolTip="PDF" />&nbsp;
                <asp:ImageButton ID="btnHTML" runat="server" ImageUrl="~/assets/common/icons/preview.gif" OnClick="btnHTML_Click" ToolTip="HTML" />&nbsp;
                <asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/assets/common/icons/next.gif" OnClick="btnNext_Click" ToolTip="Next" /></td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat="server" ID="hidden" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnNoCode" runat="server" Text="No Report Code was given... Error creating the Report." Visible="False" />
</asp:Content>
