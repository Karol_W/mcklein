<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" AutoEventWireup="true" Inherits="netpoint.admin.common.reports.Report" Codebehind="Report.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="parameters" Src="~/admin/common/reports/controls/parameters.ascx" %>
<%@ Register TagPrefix="np" TagName="queries" Src="~/admin/common/reports/controls/queries.ascx" %>
<%@ Register TagPrefix="np" TagName="template" Src="~/admin/common/reports/controls/template.ascx" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/reports/controls/general.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="graphgeneral" Src="~/admin/common/reports/controls/graphgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="graphlayers" Src="~/admin/common/reports/controls/graphlayer.ascx" %>
<%@ Register TagPrefix="np" TagName="graphadvanced" Src="~/admin/common/reports/controls/graphadvanced.ascx" %>
<%@ Register TagPrefix="np" TagName="graphproperties" Src="~/admin/common/reports/controls/graphproperties.ascx" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:Literal ID="sysReportName" runat="server"></asp:Literal>&nbsp;
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete General" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnArchive" runat="server" ImageUrl="~/assets/common/icons/compress.gif" OnClick="btnArchive_Click"
                    ToolTip="Archive" />&nbsp;
                <asp:HyperLink ID="lnkPreview" runat="server" ImageUrl="~/assets/common/icons/preview.gif" Target="_blank" ToolTip="Preview"></asp:HyperLink>&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All" OnClick="btnSaveAll_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ReportTabs" ImagesBaseUrl="~/assets/common/admin/" MultiPageId="ReportMultiPage" ClientScriptLocation="~/scripts"
            Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs" DefaultItemLookId="TopLevelTabLook"
            DefaultSelectedItemLookId="SelectedTopLevelTabLook" DefaultChildSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsReport" Text="Report" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsTemplate" Text="Template"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsQueries" Text="Queries"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsParameters" Text="Parameters"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsGraph" Text="Graph" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab id="tsGraphGeneral" runat="server" Text="General" />
                    <ComponentArt:TabStripTab ID="tsLayers" runat="server" Text="Layers" />
                    <ComponentArt:TabStripTab ID="tsProperties" runat="server" Text="Properties" />
                    <ComponentArt:TabStripTab ID="tsAdvanced" runat="server" Text="Advanced" ></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ReportMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvReportPlaceHolder" />
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvReportGeneral">
                <np:general ID="general" runat="server"></np:general>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvReportTemplate">
                <np:template ID="template" runat="server"></np:template>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvReportQuery">
                <np:queries ID="queries" runat="server"></np:queries>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvReportParameters">
                <np:parameters ID="parameters" runat="server"></np:parameters>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvGraphPlaceHolder" />
            <ComponentArt:PageView id="pvGraphGeneral" runat="server">
                 <np:graphgeneral ID="graphgeneral" runat="server" />     
            </ComponentArt:PageView>
            <ComponentArt:PageView id="pvLayers" runat="server">
                <np:graphlayers ID="layers" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView id="pvGraphProperties" runat="server">
                <np:graphproperties ID="properties" runat="server" />  
            </ComponentArt:PageView>
            <ComponentArt:PageView id="pvAdvanced" runat="server" >
                <np:graphadvanced ID="graphadvanced" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDelete" runat="server" Text="The following items will be deleted: \n - All report details \n - Associated templates \n - Associated queries \n - Associated graphs \n Continue?" Visible="False"></asp:Literal>
</asp:Content>
