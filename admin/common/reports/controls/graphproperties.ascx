<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="graphproperties.ascx.cs" Inherits="netpoint.admin.common.reports.controls.graphproperties" %>

 <asp:GridView 
    id="gvProperties"
    runat="server" 
    AllowPaging="true"
    PageSize="20"
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowDataBound="gvProperties_RowDataBound" OnPageIndexChanging="gvProperties_PageIndexChanging">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
             <ItemTemplate>
                 <asp:Literal ID="ltlName" runat="server"></asp:Literal>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField>
             <ItemTemplate>
                 <asp:TextBox ID="txtValue" Style="display: none" runat="server"></asp:TextBox>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Literal ID="ltlDefCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DefinitionCode") %>'></asp:Literal>
            </ItemTemplate>
         </asp:TemplateField>
     </Columns>
 </asp:GridView>
