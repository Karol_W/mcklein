<%@ Control Language="C#" AutoEventWireup="true" Codebehind="graphgeneral.ascx.cs" Inherits="netpoint.admin.common.reports.controls.graphgeneral" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>

<table class="npadmintable">
    <tr>
        <td colspan="2">
            <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlGraphCode" runat="server" Text="Graph Code"></asp:Literal>
        </td>
        <td class="npadminbody">
            <asp:Literal ID="sysGraph" runat="server"></asp:Literal>
            <asp:TextBox ID="txtGraphCode" runat="server" Width="80px" Columns="8"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal>
        </td>
        <td>
            <asp:TextBox ID="txtTitle" runat="server" Columns="50"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlQuery" runat="server" Text="Query"></asp:Literal>
        </td>
        <td>
            <asp:DropDownList ID="ddlQuery" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlGraphType" runat="server" Text="Graph Type"></asp:Literal>
        </td>
        <td>
            <asp:DropDownList ID="ddlGraphType" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
</table>
