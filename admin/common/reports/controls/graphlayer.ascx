<%@ Control Language="C#" AutoEventWireup="true" Codebehind="graphlayer.ascx.cs" Inherits="netpoint.admin.common.reports.controls.graphlayer" %>
<asp:GridView 
    ID="gvLayers" 
    runat="server" 
    EmptyDataText="No Layers Found" 
    CssClass="npadmintable" 
    EmptyDataRowStyle-CssClass="npadminempty"
    EmptyDataRowStyle-Height="50px"
    HeaderStyle-CssClass="npadminsubheader" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    OnRowDataBound="gvLayers_RowDataBound"
    AutoGenerateColumns="false" 
    OnRowCommand="gvLayers_RowCommand" 
    OnRowEditing="gvLayers_RowEditing">
    <RowStyle CssClass="npadminbody" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" CommandName="remove" 
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ReportGraphLayerID") %>' ImageUrl="~/assets/common/icons/remove.gif" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCol|Color">
            <HeaderStyle width="5%"></HeaderStyle>
            <ItemStyle horizontalalign="Center" />
            <ItemTemplate>
                <asp:Literal ID="ltlDataColumn" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DataColumn") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlDataColumn" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colLayers|Layer">
            <ItemTemplate>
                <asp:Literal ID="ltlGraphLayer" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReportGraphType.Name") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlGraphLayer" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="5%" Wrap="False"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ReportGraphLayerID") %>' CommandName="edit" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="canceledit" />&nbsp;
                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    CommandName="savelayer" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ReportGraphLayerID") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkDetail" runat="server" ImageUrl="~/assets/common/icons/detail.gif"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div style="WIDTH:100%; text-align:right;">
    <asp:ImageButton ID="imgNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="imgNew_Click" />
</div>
<asp:Literal ID="hdnAddLayer" runat="server" Text="Add Layer" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEditLayer" runat="server" Text="Edit Layer" Visible="False"></asp:Literal>
<asp:Literal ID="hdnSaveLayer" runat="server" Text="Save Layer" Visible="False"></asp:Literal>
<asp:Literal ID="hdnAdvanced" runat="server" Text="Advanced" Visible="False"></asp:Literal>
<asp:Literal ID="hdnRemoveLayer" runat="server" Text="Remove Layer" Visible="False"></asp:Literal>