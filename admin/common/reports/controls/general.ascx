<%@ Control Language="c#" Inherits="netpoint.admin.common.reports.controls.general" Codebehind="general.ascx.cs" %>
<asp:label id="lblError" runat="server" CssClass="npwarning" Visible="False">You entered a report code that already exists; enter a new code</asp:label>
<table cellspacing="0" cellpadding="1" width="100%" border="0">
	<tr>
		<td class="npadminlabel" width="20%"><asp:literal id="ltlReportCode" runat="server" Text="Report Code"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtReportCode" runat="server" Width="100px"></asp:textbox>
		    <asp:RequiredFieldValidator ID="rfvReportCode" runat="server" ControlToValidate="txtReportCode" ><asp:Image ID="imgRptRqd" runat="server" ImageUrl="~/assets/common/icons/warning.gif" ToolTip="Report Code is Required" /></asp:RequiredFieldValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlDisplay" runat="server" text="Display"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlReportType" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="npadminlabel" width="20%"><asp:literal id="ltlReportName" runat="server" Text="Report Name"></asp:literal></td>
		<td class="npadminbody" colspan="3">
		    <asp:textbox id="txtReportName" runat="server" Width="350px" Columns="65"></asp:textbox>
		    <asp:RequiredFieldValidator ID="rfvReportName" runat="server" ControlToValidate="txtReportName" ><asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" ToolTip="Report Name is Required" /></asp:RequiredFieldValidator>
		</td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlDescription" runat="server" Text="Description"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtDescription" runat="server" columns="65" width="350px"></asp:textbox></td>
	</tr>
</table>
