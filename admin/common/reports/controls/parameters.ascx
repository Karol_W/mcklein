<%@ Control Language="c#" Inherits="netpoint.admin.common.reports.controls.parameters"
    Codebehind="parameters.ascx.cs" %>
<asp:Panel ID="pnlDetail" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel" align="left" style="width:20%;">
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel">
                </asp:ImageButton></td>
            <td class="npadminlabel" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlLabel" runat="server" Text="Label"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtLabel" runat="server" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlVariableName" runat="server" Text="Variable Name"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtVariableName" runat="server" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlDefault" runat="server" Text="Default"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDefault" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlStep" runat="server" Text="Step"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtStep" runat="server" Width="50px">0</asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtSortOrder" runat="server" Width="50px">0</asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlControl" runat="server" Text="Control"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlControl" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlControl_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlQuery" runat="server" Text="Query" Visible="False"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtQuery" runat="server" Columns="60" TextMode="MultiLine" Rows="4"
                    Visible="false"></asp:TextBox></td>
        </tr>
    </table>
</asp:Panel>
<asp:GridView 
    ID="grid" 
    runat="server" 
    CssClass="npadmintable" 
    AutoGenerateColumns="false" 
    OnRowCommand="grid_RowCommand" EmptyDataText="No Records Found">
    <RowStyle CssClass="npadminbody"></RowStyle>
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <Columns>
        <asp:TemplateField>
             <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="del" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ParameterID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Label" HeaderText="colLabel|Label"></asp:BoundField>
        <asp:BoundField DataField="VariableName" HeaderText="colVariable|Variable"></asp:BoundField>
        <asp:BoundField DataField="ControlName" HeaderText="colControl|Control"></asp:BoundField>
        <asp:BoundField DataField="Step" HeaderText="colStep|Step">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
         <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" CommandName="edi" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ParameterID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table class="npadmintable">
    <tr>
        <td class="npadminbody" align="right">
            <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnNew_Click" ToolTip="New"></asp:ImageButton>
        </td>
    </tr>
</table>
