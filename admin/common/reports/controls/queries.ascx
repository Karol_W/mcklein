<%@ Control Language="c#" Inherits="netpoint.admin.common.reports.controls.queries" Codebehind="queries.ascx.cs" %>
<asp:Repeater id="rptQueries" runat="server">
	<HeaderTemplate>
		<table class="npadmintable">
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td>
				<asp:textbox id="txtQuery" runat="server" columns="50" rows="9" TextMode="MultiLine"  Text='<%# DataBinder.Eval(Container.DataItem, "Query")%>'>
				</asp:textbox></td>
			<td>
				<asp:literal id="ltlCode" runat="server" text="Code:"></asp:literal><br>
				<asp:TextBox id="txtQueryCode" runat="server" columns="10" text='<%# DataBinder.Eval(Container.DataItem, "QueryCode") %>'></asp:TextBox><br>
				<asp:Literal id="ltlEval" runat="server" text="EvalOrder:"></asp:Literal><br>
				<asp:TextBox id="txtEvalOrder" runat="server" columns="4" Text='<%# DataBinder.Eval(Container.DataItem, "EvalOrder")%>'></asp:TextBox><br>
				<asp:literal id="ltlSection" runat="server" text="Section:"></asp:literal><br>
				<asp:TextBox id="txtSection" runat="server" columns="10" Text='<%# DataBinder.Eval(Container.DataItem, "Section")%>'></asp:TextBox><br></td>
			<td>
				<asp:ImageButton id="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="save" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "QueryID")%>' />
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "QueryID")%>' /></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
<asp:Literal id="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?" Visible="False"></asp:Literal>
<asp:Literal id="hdnQuery" runat="server" Text="Query" Visible="False"></asp:Literal>
<asp:Literal id="hdnEval" runat="server" Text="Eval" Visible="False"></asp:Literal>
<asp:Literal id="hdnSection" runat="server" Text="Section" Visible="False"></asp:Literal>
<asp:Literal id="hdnCode" runat="server" Text="Code" Visible="False"></asp:Literal>
