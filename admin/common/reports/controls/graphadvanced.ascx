<%@ Control Language="C#" AutoEventWireup="true" Codebehind="graphadvanced.ascx.cs"
    Inherits="netpoint.admin.common.reports.controls.graphadvanced" %>
<%@ Register TagPrefix="np" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>


<table class="npadmintable">
    <tr>
        <td>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMoreTitle" runat="server" Text="Title"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlFontName" runat="server" Text="Font Name"></asp:Literal></td>
        <td>
            <asp:DropDownList ID="ddlTitleFont" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlFontSize" runat="server" Text="Font Size"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtTitleFontSize" runat="server" Width="80px" Columns="7"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlFontColor" runat="server" Text="Font Color"></asp:Literal></td>
        <td>
            <np:HtmlColorDropDown id="ddTitleFontColor" runat="server" AutoColorChangeSupport="true"
                SortOption="ByValue" Palette="WebSafe">
            </np:HtmlColorDropDown></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlTitleBackground" runat="server" Text="Title Background"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="ltlColor" runat="server" Text="Color"></asp:Literal>&nbsp;
            <np:HtmlColorDropDown id="ddTitleBackgroundColor" runat="server" AutoColorChangeSupport="true"
                SortOption="ByValue" Palette="WebSafe">
            </np:HtmlColorDropDown>&nbsp;
            <asp:Literal ID="ltlEdgeColor" runat="server" Text="Edge Color"></asp:Literal>&nbsp;
            <np:HtmlColorDropDown id="ddTitleBackgroundEdgeColor" runat="server" AutoColorChangeSupport="true"
                SortOption="ByValue" Palette="WebSafe">
            </np:HtmlColorDropDown></td>
    </tr>
    <tr>
        <td>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlLegend" runat="server" Text="Legend"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlLocation" runat="server" Text="Location"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="ltlX" runat="server" Text="X"></asp:Literal>
            <asp:TextBox ID="txtX" runat="server" Width="80px" Columns="8"></asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="rfvX" runat="server" ControlToValidate="txtX" ErrorMessage="RequiredFieldValidator">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
            </asp:RequiredFieldValidator>
            <asp:Literal ID="ltlY" runat="server" Text="Y"></asp:Literal>
            <asp:TextBox ID="txtY" runat="server" Width="80px" Columns="8"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvY" runat="server" ControlToValidate="txtY" ErrorMessage="RequiredFieldValidator">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
            </asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabeL">
            <asp:Literal ID="ltlLegendFlags" runat="server" Text="Flags"></asp:Literal></td>
        <td class="npadminbody">
            <asp:CheckBox ID="chkLegend" runat="server" Text="Visible"></asp:CheckBox>
            <asp:CheckBox ID="chkVertical" runat="server" Text="Vertical"></asp:CheckBox></td>
    </tr>
    <tr>
        <td>
        </td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlContainer" runat="server" Text="Container"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDimensions" runat="server" Text="Dimensions"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="ltlHeight" runat="server" Text="Height"></asp:Literal>&nbsp;
            <asp:TextBox ID="txtHeight" runat="server" Width="80px" Columns="8"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvHeight" runat="server" ControlToValidate="txtHeight"
                ErrorMessage="RequiredFieldValidator">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
            </asp:RequiredFieldValidator>&nbsp;
            <asp:Literal ID="ltlWidth" runat="server" Text="Width"></asp:Literal>&nbsp;
            <asp:TextBox ID="txtWidth" runat="server" Width="80px" Columns="8"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvWidth" runat="server" ControlToValidate="txtWidth"
                ErrorMessage="RequiredFieldValidator">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
            </asp:RequiredFieldValidator>&nbsp;
            <asp:Literal ID="ltlBackgroundDepth" runat="server" Text="Depth"></asp:Literal>&nbsp;
            <asp:TextBox ID="txtBackgroundDepth" runat="server" Columns="8" Width="80px"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBackgroundColor" runat="server" Text="Color"></asp:Literal></td>
        <td>
            <np:HtmlColorDropDown id="ddBackgroundColor" runat="server" AutoColorChangeSupport="true"
                SortOption="ByValue" Palette="WebSafe">
            </np:HtmlColorDropDown></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBackgroundEdgeColor" runat="server" Text="Edge Color"></asp:Literal></td>
        <td>
            <np:HtmlColorDropDown id="ddBackgroundEdgeColor" runat="server" AutoColorChangeSupport="true"
                SortOption="ByValue" Palette="WebSafe">
            </np:HtmlColorDropDown></td>
    </tr>
    <tr>
        <td class="npadminlabeL">
            <asp:Literal ID="ltlWallpaper" runat="server" Text="Wallpaper"></asp:Literal></td>
        <td>
            <np:FilePicker ID="fpk" runat="server" StartDirectory="~/assets/common/images/"></np:FilePicker>
        </td>
    </tr>
</table>
<asp:Label ID="sysError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="Larger"></asp:Label>
<asp:Literal ID="errMustBeNumber" runat="server" Text="Must be a number." Visible="False"></asp:Literal>
