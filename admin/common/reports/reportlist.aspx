<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.reports.ReportList" Codebehind="ReportList.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Import, create, edit and delete eCommerce reports</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
    <tr>
        <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Import, create, edit and delete eCommerce reports" /></td>
        <td class="npadminheader" align="right">
            <asp:hyperlink id="lnkImport" runat="server" imageurl="~/assets/common/icons/import.gif"
                tooltip="Import Report" navigateurl="ReportUpload.aspx"></asp:hyperlink>&nbsp;
            <asp:hyperlink id="lnkNew" runat="server" imageurl="~/assets/common/icons/add.gif"
                tooltip="Add New Report" navigateurl="ReportNew.aspx"></asp:hyperlink>&nbsp;</td>
    </tr>
</table>
<div class="npadminbody">
<asp:GridView 
    ID="gvReports" 
    runat="server" 
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt"
    HeaderStyle-CssClass="npadminsubheader" 
    EmptyDataRowStyle-CssClass="npadminempty" 
    EmptyDataText="No Reports Found" 
    EmptyDataRowStyle-Height="50px"
    OnRowCommand="gvReports_RowCommand" 
    OnRowDataBound="gvReports_RowDataBound" 
    AutoGenerateColumns="false" 
    EditRowStyle-Height="50px">
    <Columns>
		<asp:BoundField DataField="ReportCode" HeaderText="colCode|Code"></asp:BoundField>
		<asp:BoundField DataField="Name" HeaderText="colName|Name"></asp:BoundField>
		<asp:TemplateField HeaderText="Duplicate" HeaderStyle-HorizontalAlign="Center">
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
			    <asp:HyperLink ID="lnkCopy" runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.ReportCode", "reportcopy.aspx?reportcode={0}") %>' 
			        ImageUrl="~/assets/common/icons/duplicate.gif" ToolTip="Duplicate this report"></asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnEdit" runat="server" CommandName="editreport" ImageUrl="~/assets/common/icons/edit.gif" 
				    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ReportCode") %>' />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" 
				    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ReportCode") %>' CommandName="deletereport" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colPreview|Preview">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink id="lnkPreview" runat="server" target="_blank" 
				    NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.ReportCode", "ReportLaunch.aspx?code={0}") %>' 
				    ToolTip="Preview" ImageUrl="../../../assets/common/icons/preview.gif">Preview this report</asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
    </Columns>        
</asp:GridView>
</div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnPreview" runat="server" text="Preview" visible="False"></asp:literal>
    <asp:literal id="hdnDelete" runat="server" visible="False" text="Delete this Report"></asp:literal>
    <asp:literal id="hdnEdit" runat="server" visible="False" text="Edit this Report"></asp:literal>
    <asp:literal id="hdnDeleteMessage" runat="server" visible="False" text="The following will be deleted: \n - This report \n - Report queries \n - Report parameters \n - Graphs for this report \n Continue?"></asp:literal>
</asp:Content>
