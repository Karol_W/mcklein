<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.common.reports.ReportNew" Codebehind="ReportNew.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/common/reports/controls/general.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tbody>
            <tr>
                <td class="npadminsubheader" style="width:20%;">
                    <asp:hyperlink id="lnkBack" runat="server" navigateurl="~/admin/common/reports/ReportList.aspx"
                        imageurl="~/assets/common/icons/cancel.gif" tooltip="Cancel" /></td>
                <td class="npadminsubheader"><asp:literal id="sysReportName" runat="server"></asp:literal></td>
            </tr>
            <tr>
                <td class="npadminsubheader">
                    <asp:imagebutton runat="server" imageurl="~/assets/common/icons/indicator.gif" id="imgGeneral" />&nbsp;
                    <asp:literal runat="server" id="ltlGeneralHeader" text="General" /></td>
                <td class="npadminsubheader"align="right">
                    <asp:imagebutton id="btnSaveGeneral" runat="server" imageurl="~/assets/common/icons/save.gif" tooltip="Save Section" /></td>
            </tr>
            <tr>
                <td colspan="2"><np:general ID="general" runat="server"></np:general></td>
            </tr>
        </tbody>
    </table>
</asp:Content>
