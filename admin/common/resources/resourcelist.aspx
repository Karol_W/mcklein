<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.common.resources.ResourceList" Codebehind="ResourceList.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:DropDownList ID="ddlResourceType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlResourceType_SelectedIndexChanged">
                </asp:DropDownList>&nbsp;
                <asp:HyperLink ID="lnkAdd" runat="server" ToolTip="Add Resource" ImageUrl="~/assets/common/icons/add.gif"
                    NavigateUrl="~/admin/common/resources/resource.aspx"></asp:HyperLink></td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:GridView ID="gvResources" runat="server" AutoGenerateColumns="false" CssClass="npadmintable" RowStyle-CssClass="npadminbody"
            AlternatingRowStyle-CssClass="npadminbodyalt" HeaderStyle-CssClass="npadminsubheader" OnRowCommand="gvResources_RowCommand" OnRowDataBound="gvResources_RowDataBound" EmptyDataRowStyle-Height="50px" EmptyDataText="No Records Found" EmptyDataRowStyle-CssClass="npadminempty">
            <Columns>
                <asp:HyperLinkField HeaderText="colID|ID" DataTextField="ResourceID" DataNavigateUrlFields="ResourceId" DataNavigateUrlFormatString="~/admin/common/resources/resource.aspx?resourceid={0}"
                     ItemStyle-HorizontalAlign="center"></asp:HyperLinkField>
                <asp:HyperLinkField HeaderText="colName|Name" DataTextField="Name" DataNavigateUrlFields="ResourceID" DataNavigateUrlFormatString="~/admin/common/resources/resource.aspx?resourceid={0}"
                     ItemStyle-HorizontalAlign="center"></asp:HyperLinkField>
                <asp:BoundField DataField="Dimension1" HeaderText="colDimension1|Dimension 1"></asp:BoundField>
                <asp:BoundField DataField="Dimension2" HeaderText="colDimension2|Dimension 2"></asp:BoundField>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                            ToolTip="Delete this Resource." CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ResourceID") %>'
                            CommandName="remove"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>    
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteResource" runat="server" Visible="False" Text="Delete this Resource"></asp:Literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="The resource definition and all associated user data will also be deleted. Continue?"></asp:Literal>
    <asp:Literal ID="errResourceExists" runat="server" Visible="False" Text="This Resource ID has already been used. Please select another."></asp:Literal>
</asp:Content>
