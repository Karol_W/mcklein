<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master"
    Codebehind="resource.aspx.cs" Inherits="netpoint.admin.common.resources.resource" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSaveAll_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
    <asp:Label ID="sysError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="4"></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlResourceID" runat="server" Text="Resource ID"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtResourceID" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage="required" ControlToValidate="txtResourceID">
						<img src='../../../assets/common/icons/warning.gif' alt="" /></asp:RequiredFieldValidator></td>
                <td class="npadminlabel"><asp:Literal ID="ltlResourceTypeTag" runat="server" Text="Resource Type"></asp:Literal></td>
                <td class="npadminbody"><asp:Literal ID="sysResourceType" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlResourceName" runat="server" Text="Resource Name"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlSortCode" runat="server" Text="Sort Code"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtSortCode" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
                <td class="npadminbody" colspan="3"><asp:TextBox ID="txtDescription" runat="server" Height="100px" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDimension1Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;1</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension1" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlRequired1" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL1" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible1" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension2Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;2</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension2" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired2" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL2" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible2" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension3Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;3</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension3" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired3" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL3" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible3" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension4Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;4</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension4" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired4" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL4" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible4" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension5Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;5</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension5" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired5" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL5" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible5" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel" style="height: 25px"><asp:Literal ID="sysDimension6Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;6</td>
                <td class="npadminbody" style="height: 25px"><asp:TextBox ID="txtDimension6" runat="server"></asp:TextBox></td>
                <td class="npadminlabel" style="height: 25px"><asp:Literal ID="sysRequired6" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody" style="height: 25px">
                    <asp:CheckBox ID="sysURL6" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible6" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension7Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;7</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension7" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired7" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL7" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible7" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension8Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;8</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension8" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired8" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL8" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible8" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="sysDimension9Label" runat="server" Text="Dimension"></asp:Literal>&nbsp;9</td>
                <td class="npadminbody"><asp:TextBox ID="txtDimension9" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="sysRequired9" runat="server" Text="URL/Required"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:CheckBox ID="sysURL9" runat="server"></asp:CheckBox>&nbsp;/
                    <asp:CheckBox ID="sysVisible9" runat="server"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlURL" runat="server" Text="URL"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtURLDimension" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlImage" runat="server" Text="Image"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtPictureDimension" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlText" runat="server" Text="Text"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtTextDimension" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlDate" runat="server" Text="Date"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtDateDimension" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlCode1Label" runat="server" Text="Code"></asp:Literal>&nbsp;1</td>
                <td class="npadminbody"><asp:TextBox ID="txtCode1Dimension" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlCode1Type" runat="server" Text="Code 1 Type"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlCode1" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlCode2Label" runat="server" Text="Code"></asp:Literal>&nbsp;2</td>
                <td class="npadminbody"><asp:TextBox ID="txtCode2Dimension" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlCode2TypeLabel" runat="server" Text="Code 2 Type"></asp:Literal></td>
                <td class="npadminbody"> <asp:DropDownList ID="ddlCode2" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlOptions" runat="server" Text="Options"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtOptionsDimension" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlOptionType" runat="server" Text="Options Type"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlOptionType" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlFeatureCode" runat="server" Text="Feature Code"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtFeatureCode" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlIndexType" runat="server" Text="Index Type"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlIndexType" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlModerator" runat="server" Text="Moderator"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlModerator" runat="server"></asp:DropDownList></td>
                <td class="npadminlabel"><asp:Literal ID="ltlSortGroup" runat="server" Text="Sort Group"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtSortGroup" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlFlags" runat="server" Text="Flags"></asp:Literal></td>
                <td class="npadminbody" colspan="3">
                    <asp:CheckBox ID="chkUserVisible" runat="server" Text="User Visible"></asp:CheckBox>&nbsp;
                    <asp:CheckBox ID="chkAllowMultiple" runat="server" Text="Allow Multiple" ToolTip="Allows the user to create multiple records of this resource"></asp:CheckBox>&nbsp;
                    <asp:CheckBox ID="chkSystemResource" runat="server" Text="System Resource"></asp:CheckBox></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeleteResource" runat="server" Visible="False" Text="Delete this Resource"></asp:Literal>
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="The resource definition and all associated user data will also be deleted. Continue?"></asp:Literal>
    <asp:Literal ID="errResourceExists" runat="server" Visible="False" Text="This Resource ID has already been used. Please select another."></asp:Literal>
</asp:Content>
