﻿<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" CodeBehind="about.aspx.cs" Inherits="netpoint.admin.about" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <asp:Panel ID="AboutPanel" runat="server">
        <br />
        <table class="npbody" cellpadding="3" width="100%" cellspacing="0">
            <tr>
                <td class="nplabel" align="center" style="width:0%;"><asp:Literal ID="ltlVersion" runat="server" Text=""></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointVersionNumber" runat="server"></asp:Label></td>
            </tr> 
            <tr>
                <td class="npsubheader" align="left" colspan="2"><asp:Literal ID="ltlLicenses" runat="server" Text="Licenses"></asp:Literal></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlSerial" runat="server" Text="Serial No."></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseSerial" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlOrganization" runat="server" Text="Organization"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseOrginization" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlCustomerNumber" runat="server" Text="Customer No."></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseCustomerNumber" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlUser" runat="server" Text="User"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseUser" runat="server"></asp:Label></td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlAssembly" runat="server" Text="Assembly(s)"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseAssembly" runat="server"></asp:Label></td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlConstraints" runat="server" Text="Constraints"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysNetPointLicenseConstraints" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <table class="npbodyalt" cellpadding="3" width="100%" cellspacing="0">
            <tr>
                <td class="npsubheader" align="left" colspan="2"><asp:Literal ID="ltlComponents" runat="server" Text="Components"></asp:Literal></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;">NetPoint &nbsp;</td>
                <td><asp:Label ID="sysNetPoint" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> NetPoint.API &nbsp;</td>
                <td><asp:Label ID="sysNetPointAPI" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> NetPoint.CreditCards &nbsp;</td>
                <td><asp:Label ID="sysNetPointCreditCards" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> NetPoint.WebControls &nbsp;</td>
                <td><asp:Label ID="sysNetPointWebControls" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> zed eCommerce Database &nbsp;</td>
                <td><asp:Label ID="sysNetPointDatabase" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <table class="npbody" cellpadding="3" width="100%" cellspacing="0">
            <tr>
                <td class="npsubheader" align="left" colspan="2">WebServer</td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> Server Name &nbsp;</td>
                <td><asp:Label ID="sysWebServerName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"> zed eCommerce Memory &nbsp;</td>
                <td><asp:Label ID="sysSystemMemory" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;">OS Version &nbsp;</td>
                <td><asp:Label ID="sysOSVersion" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;">.NET Version &nbsp;</td>
                <td><asp:Label ID="sysDOTNETVersion" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;">MDAC Version &nbsp;</td>
                <td><asp:Label ID="sysMDACVersion" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <table class="npbodyalt" cellpadding="3" width="100%" cellspacing="0">
            <tr>
                <td class="npsubheader" align="left" colspan="2"><asp:Literal ID="ltlCurrentVariables" runat="server" Text="Current Variables"></asp:Literal></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlServerID" runat="server" Text="Server ID"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysPageServerID" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlEncoding" runat="server" Text="Encoding"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysPageEncoding" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlCatalogCode" runat="server" Text="Catalog Code"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysPageCatalogCode" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlVirtualPath" runat="server" Text="Virtual Path"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysPageVirtualPath" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <table class="npbody" cellpadding="3" width="100%" cellspacing="0">
            <tr>
                <td class="npsubheader" align="left" colspan="2"><asp:Literal ID="ltlSQLDatabase" runat="server" Text="SQL Database"></asp:Literal></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlServerName" runat="server" Text="Server Name"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysDBServerName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;">Language &nbsp;</td>
                <td><asp:Label ID="sysDBLanguage" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlVersion2" runat="server" Text="Version"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysDBVersion" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="left" style="width:30%;"><asp:Literal ID="ltlAppName" runat="server" Text="Appl. Name"></asp:Literal>&nbsp;</td>
                <td><asp:Label ID="sysDBAppName" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <br />
    </asp:Panel>
    <hr />
    <br />
    <asp:Panel runat="server" ID="ActionPanel" CssClass="action-panel-about">
        <asp:Literal ID="ltlItemsInNetPointCache" runat="server" Text="Items in Cache "></asp:Literal>
        <asp:Label ID="sysCacheSize" runat="server"></asp:Label>&nbsp;
        <asp:LinkButton ID="lnkReCacheLink" runat="server" Text="Recache Theme" Visible="false" OnClick="ReCacheLink_Click">Recache Theme and Strings</asp:LinkButton>&nbsp;|
        <asp:LinkButton ID="lnkCleanMemory" runat="server" Text="Clean Up Memory" Visible="false" OnClick="CleanMemory_Click"></asp:LinkButton>&nbsp;| &nbsp;
        <asp:LinkButton ID="lnkEmailSupport" runat="server" Text="Email This to Support" Visible="false" OnClick="EmailSupport_Click"></asp:LinkButton>
    </asp:Panel>
    <asp:Literal ID="errFailedToSendEmail" runat="server" Text="Failed to send email" Visible="False"></asp:Literal>
</asp:Content>
