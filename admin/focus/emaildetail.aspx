<%@ Page ValidateRequest="false" MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.dashboard.EmailDetail" Codebehind="EmailDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="customers" Src="controls/customers.ascx" %>
<%@ Register TagPrefix="np" TagName="DashboardTab" Src="controls/DashboardTab.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/accountpicker.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:Label ID="lblBreadCrumbs" Text="Focus > Activities > Send Email" CssClass="npbody" runat="server"></asp:Label>
    <np:DashboardTab ID="DashboardTab" runat="server"></np:DashboardTab>
    <table class="npadminbody" style="border-color:LightGray;" cellspacing="0" cellpadding="5"
        width="100%" border="0">
        <tr>
            <td colspan="3"><asp:Label ID="errMessage" runat="server" CssClass="npwarning" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="npadminbody">
                    <tr>
                        <td><np:AccountPicker ID="SelectedAccount" runat="server"></np:AccountPicker></td>
                        <td>&nbsp;
                            <asp:Literal ID="ltlContactID" runat="server" Text="Contact:"></asp:Literal>
                            <asp:DropDownList ID="ContactID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ContactID_SelectedIndexChanged"></asp:DropDownList>
                            &nbsp;&nbsp;<asp:Literal ID="ltlContactDisplay" runat="server" Text="Display:"></asp:Literal>
                            <asp:TextBox ID="ContactDisplay" runat="server" Columns="20"></asp:TextBox>
                            <asp:TextBox ID="EventID" runat="server" Visible="False"></asp:TextBox></td>
                    </tr>
                </table>
                <br />
                <hr />
            </td>
            <td style="width:5%;"><asp:ImageButton ID="btnEmail" runat="server" ImageUrl="~/assets/common/icons/email.gif" ToolTip="Email" /></td>
        </tr>
        <tr>
            <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlFromName" runat="server" Text="From:"></asp:Literal></td>
            <td><b><asp:Label ID="sysFromName" runat="server"></asp:Label>&lt;<asp:Label ID="sysFromEmail" runat="server"></asp:Label>&gt;</b></td>
        </tr>
        <tr>
            <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlEmailAdress" runat="server" Text="To:"></asp:Literal></td>
            <td>
                <asp:TextBox ID="EmailAddress" runat="server" Columns="70" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmailAddress" runat="server" CssClass="errormsg"
                    Display="Static" ErrorMessage="Email is a required field." ControlToValidate="EmailAddress">
                    <asp:Image ID="imgWarningEmail" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                </asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlSubject" runat="server" Text="Subject:"></asp:Literal></td>
            <td>
                <asp:TextBox ID="Subject" runat="server" Columns="70" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSubject" runat="server" CssClass="errormsg" Display="Static"
                    ErrorMessage="Subject is a required field." ControlToValidate="Subject">
                    <asp:Image ID="imgWarningSubject" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                </asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:CheckBoxList ID="FinishActions" runat="server" CssClass="npadminbody">
                    <asp:ListItem Value="ccme">Send Myself a Copy</asp:ListItem>
                    <asp:ListItem Value="ticket">Auto-Create Ticket From Email</asp:ListItem>
                </asp:CheckBoxList></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
