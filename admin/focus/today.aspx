<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.dashboard.Today" Codebehind="Today.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="EventPending" Src="controls/EventPending.ascx" %>
<%@ Register TagPrefix="np" TagName="overdue" Src="controls/overdue.ascx" %>
<%@ Register TagPrefix="np" TagName="tasks" Src="~/admin/support/controls/tasks.ascx" %>
<%@ Register TagPrefix="MC" Namespace="Mediachase.Web.UI.WebControls" Assembly="Mediachase.Web.UI.WebControls" %>
<%@ Register TagPrefix="np" TagName="DashboardTab" Src="controls/DashboardTab.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Today's Activities</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

    <script type="text/javascript">
    function addnew(time){
	    window.location.href="Activity.aspx?time="+escape(time)+"&backpage=~/admin/focus/Today.aspx";
    }
    </script>

    <np:DashboardTab ID="tab" runat="server"></np:DashboardTab>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsToday" Text="Today"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsOverdue" Text="Overdue"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsPending" Text="Pending"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsTasks" Text="Tasks"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvToday">
                <div class="npadminsubheader">
                    <asp:label runat="server" id="TodayLabel" />
                </div>
                <MC:Calendar ID="CalendarCtrl" Width="100%" runat="server" CellPadding="1" CellSpacing="0"
                    Font-Names="Arial" ForeColor="#80FF80" BorderWidth="1px" Palette="Windows" ViewType="DayView"
                    MaxDisplayedItems="6" AbbreviatedDayNames="False" BorderStyle="Solid" AutoPostBack="True"
                    NewLinkFormat="addnew('{0}');" DataLinkFormat="Activity.aspx?activityid={0}&amp;backpage=~/admin/focus/Today.aspx"
                    TimescaleBottomLabelFormat="dd" TimescaleTopLabelFormat="dd, MMMM" MergeEvents="True"
                    EventBarShow="True">
                    <CalendarHeaderStyle HorizontalAlign="Center"></CalendarHeaderStyle>
                </MC:Calendar>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" ID="pvOverdue">
                <np:overdue ID="overdue" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" ID="pvPending">
                <np:EventPending ID="evtPending" runat="server"></np:EventPending>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" ID="pvTasks">
                <np:tasks ID="tasks" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
