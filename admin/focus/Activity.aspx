<%@ Page ValidateRequest="false" MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.focus.Activity" CodeBehind="Activity.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="notes" Src="controls/ActivityNotes.ascx" %>
<%@ Register TagPrefix="np" TagName="apicker" Src="~/admin/common/controls/accountpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="datepicker" Src="~/common/controls/npdatepicker.ascx" %>
<%@ Register TagPrefix="np" TagName="timepicker" Src="~/common/controls/NPTimePicker.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Activity Details</title>
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">    
    <table class="npadminpath" cellpadding="0" cellspacing="0">
        <tr>
            <td class="npadminheader" align="right">&nbsp;
                <asp:imagebutton id="btnDelete" runat="server" imageurl="~/assets/common/icons/delete.gif" tooltip="Delete" OnClick="btnDelete_Click" />&nbsp;
                <asp:imagebutton id="btnFollowup" runat="server" imageurl="~/assets/common/icons/followup.gif" tooltip="Follow Up" OnClick="btnFollowup_Click" />&nbsp;
                <asp:imagebutton id="btnSave" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" tooltip="Save and Go Back" OnClick="btnSave_Click" />&nbsp;
        </tr>
    </table>    
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ActivityTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ActivityMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGenaral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsNotes" Text="Notes"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLinked" Text="Linked"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <asp:Panel ID="pnlEnable" runat="server" >
        <ComponentArt:MultiPage ID="ActivityMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400" >
            <ComponentArt:PageView ID="PageView1" CssClass="npadminbody" runat="server">
                <asp:Label CssClass="npwarning" ID="sysErr" Visible="False" runat="server"></asp:Label>
                <table class="npadmintable">
                    <tr>
                        <td><asp:CheckBox ID="chkActive" runat="server" Text="Active" /></td>
                        <td><asp:CheckBox ID="chkClosed" runat="server" Text="Closed" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlActivity" runat="server" Text="Activity"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList ID="ddlActivities" runat="server" AutoPostBack="true"
                             OnSelectedIndexChanged="ddlActivity_SelectedIndexChanged" ></asp:DropDownList></td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal>
                            <asp:Literal ID="ltlProspect" runat="server" Text="Prospect" Visible="false"></asp:Literal></td>
                        <td class="npadminbody">
                            <np:apicker ID="apAccount" runat="server" Required="true" PostBack="true" />
                            <asp:HyperLink ID="lnkProspect" runat="server" Visible="false"></asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlActivityType" runat="server" Text="Type"></asp:Literal></td>
                        <td class="npadminbody">
                            <asp:DropDownList ID="ddlActivityType" runat="server" AutoPostBack="true" 
                              OnSelectedIndexChanged="ddlActivityType_SelectedIndexChanged" ></asp:DropDownList></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlContactName" runat="server" Text="User"></asp:Literal></td>
                        <td class="npadminbody">
                            <asp:DropDownList ID="ddlContact" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlContact_SelectedIndexChanged" />
                            <asp:Literal ID="ltlProspectContact" runat="server" Visible="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList ID="ddlSubject" runat="server"></asp:DropDownList></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlContactPhone" runat="server" Text="Phone"></asp:Literal></td>
                        <td class="npadminbody"><asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlAssignedUser" runat="server" Text="Assigned To"></asp:Literal></td>
                        <td class="npadminbody"><asp:DropDownList ID="ddlAssignedTo" runat="server"></asp:DropDownList></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlPersonal" runat="server" Text="Personal"></asp:Literal></td>
                        <td class="npadminbody"><asp:CheckBox ID="chkPersonal" runat="server"/></td>
                    </tr>
                </table>
                <table class="npadmintable">
                    <tr>
                        <td class="npadminsubheader" colspan="5"><asp:Literal ID="ltlDetails" runat="server" Text="Details"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlRemarks" runat="server" Text="Remarks"></asp:Literal>
                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="149" Width="500px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlStart" runat="server" Text="Start Date"></asp:Literal>                        
                        <np:datepicker ID="dpStart" runat="server" />
                        <np:timepicker ID="tpStart" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlEnd" runat="server" Text="End Date"></asp:Literal>
                        <np:datepicker ID="dpEnd" runat="server" />
                        <np:timepicker ID="tpEnd" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                        
                        <asp:Literal ID="ltlPriority" runat="server" Text="Priority"></asp:Literal>
                        <asp:DropDownList ID="ddlPriority" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                        
                        <asp:CheckBox ID="chkReminder" runat="server" Text="Reminder" />
                        
                            <asp:DropDownList ID="ddlOffset" runat="server">
                                <asp:ListItem Value="10">10 min</asp:ListItem>
                                <asp:ListItem Value="20">20 min</asp:ListItem>
                                <asp:ListItem Value="30">30 min</asp:ListItem>
                                <asp:ListItem Value="60"> 1 hr </asp:ListItem>
                                <asp:ListItem Value="120"> 2 hrs</asp:ListItem>
                                <asp:ListItem Value="240"> 4 hrs</asp:ListItem>
                                <asp:ListItem Value="480"> 8 hrs</asp:ListItem>
                                <asp:ListItem Value="1440"> 1 day</asp:ListItem>
                            </asp:DropDownList>
                        
                        </td>
                    </tr>
                </table>
                <div id="divLocation" runat="server" visible="false">
                    <table class="npadmintable">
                        <tr>
                            <td class="npadminsubheader" colspan="2"><asp:Literal ID="ltlLocation" runat="server" Text="Location"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlStreet" runat="server" Text="Street 1"></asp:Literal></td>
                            <td class="npadminbody">
                                <asp:TextBox ID="txtStreet" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="chkTentative" runat="server" Text="Tentative" /></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlStreet2" runat="server" Text="Street 2"></asp:Literal></td>
                            <td class="npadminbody"><asp:TextBox ID="txtStreet2" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlCity" runat="server" Text="City"></asp:Literal></td>
                            <td class="npadminbody"><asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
                            <td class="npadminbody"><asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
                            <td class="npadminbody"><asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList></td>
                        </tr>
                    </table>
                </div>
            </ComponentArt:PageView>
            <ComponentArt:PageView ID="PageView2" CssClass="PageContent" runat="server">
                <np:notes ID="activitynotes" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView ID="PageView3" CssClass="PageContent" runat="server"> 
                <div id="divNew" runat="server">
                    <table class="npadminbody" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="npadminlabel" style="width:30%;"><asp:Literal ID="ltlDocType" runat="server" Text="Document Type"></asp:Literal></td>
                            <td class="npadminbody">
                                <asp:DropDownList ID="ddlDocType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged"></asp:DropDownList>
                                <asp:Literal id="ltlB1DocType" runat="server" Visible="false" Text="B1 Doc Type"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlDocNum" runat="server" Text="Document Number"></asp:Literal></td>
                            <td class="npadminbody">
                                <asp:DropDownList ID="ddlDocNum" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDocNum_SelectedIndexChanged"></asp:DropDownList>&nbsp;
                                <asp:HyperLink ID="lnkDoc" runat="server" Visible="false" Text="Document"></asp:HyperLink>
                                <asp:Literal ID="ltlB1DocNum" runat="server" Visible="false" Text="B1 Doc Num"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlParent" runat="server" Text="Parent" Visible="false"></asp:Literal></td>
                            <td class="npadminbody"><asp:HyperLink ID="lnkParent" runat="server" Visible="false" Text="Parent"></asp:HyperLink></td>
                        </tr>
                    </table>
                </div>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
        </asp:Panel>
    </div>   
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errDates" runat="server" Visible="false" Text="End date must be greater than start date."></asp:Literal>
    <asp:literal id="hdnFollowupText" runat="server" text="Follow Up: " visible="False"></asp:literal>
    <asp:literal id="ltlOrderID" runat="server" text="Order: " visible="False"></asp:literal>
    <asp:literal id="ltlShipDate" runat="server" text="Ship Date: " visible="False"></asp:literal>
    <asp:Literal ID="errRemarks" runat="server" Text="Remarks is a required field." Visible="false"></asp:Literal>
    <asp:Literal ID="ltlOpportunity" runat="Server" Text="Opportunity: " Visible="false"></asp:Literal>
    <asp:Literal ID="ltlWorkOrder" runat="Server" Text="Service Call: " Visible="false"></asp:Literal>
    <asp:Literal ID="hdnParentActivity" runat="server" Text="Activity: " Visible="false"></asp:Literal>
    <asp:Literal ID="hdnServiceCall" runat="server" Text="Service Call: " Visible="false"></asp:Literal> 
</asp:Content>
