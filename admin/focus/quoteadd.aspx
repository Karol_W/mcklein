<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" CodeBehind="quoteadd.aspx.cs" Inherits="netpoint.admin.focus.quoteadd" %>
<%@ Register Src="controls/ordercustomer.ascx" TagName="quotecustomer" TagPrefix="np" %>
<%@ Register Src="controls/ordercart.ascx" TagName="quotecart" TagPrefix="np" %>
<%@ Register Src="controls/ordernotes.ascx" TagName="quotenotes" TagPrefix="np" %>
<%@ Register Src="controls/orderaddresses.ascx" TagName="quoteaddresses" TagPrefix="np" %>
<%@ Register Src="controls/ordershippingmethod.ascx" TagName="quoteshippingmethod" TagPrefix="np" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysErrorMessage" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:Label runat="server" ID="sysOrderID"></asp:Label>&nbsp;
                <asp:ImageButton ID="btnDelete" runat="server" Visible="false" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete Quote" OnClick="btnDelete_Click"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif"
                    ToolTip="Save Quote" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnComplete" runat="server" Visible="false" ImageUrl="~/assets/common/buttons/complete.gif"
                    ToolTip="Complete" OnClick="btnComplete_Click"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div id="divNewQuote" runat="server" visible="false" style="width:100%;" class="npadminsubheader">
        <asp:Literal ID="ltlNewQuote" runat="server" Text="New Quote"></asp:Literal>
    </div>
    <div id="divTabStrip" runat="server">
         <ComponentArt:TabStrip ID="OrderTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="OrderMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0" >
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" Enabled="true" ID="tsCustomer" Text="Customer" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsCart" Text="Cart" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsAddresses" Text="Addresses" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsShippingMethod" Text="Shipping Method" />
            </Tabs>
        </ComponentArt:TabStrip>
    </div>
        <ComponentArt:MultiPage ID="OrderMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCustomer">
                <np:quotecustomer ID="quotecustomer" runat="server"></np:quotecustomer>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCart">
                <np:quotecart ID="quotecart" runat="server"></np:quotecart>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvAddresses">
                <np:quoteaddresses ID="quoteaddresses" runat="server"></np:quoteaddresses>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvShippingMethod">
                <np:quoteshippingmethod ID="quoteshippingmethod" runat="server"></np:quoteshippingmethod>
                <np:quotenotes ID="quotenotes" runat="server"></np:quotenotes>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    <br />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
    <asp:Literal ID="errSelectSaveAddress" runat="server" Text="Please Select or Save a Bill To and a Delivery Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errNoItemsInCart" runat="server" Text="No Items in cart. Add an item." Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>  
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
</asp:Content>