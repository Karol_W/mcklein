<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" ValidateRequest="false" Inherits="netpoint.dashboard.sales" CodeBehind="sales.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="DashboardTab" Src="controls/DashboardTab.ascx" %>
<%@ Register TagPrefix="np" TagName="logs" Src="~/admin/prospects/controls/logs.ascx" %>
<%@ Register TagPrefix="np" TagName="activity" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="np" TagName="orders" Src="~/admin/focus/controls/orders.ascx" %>
<%@ Register TagPrefix="np" TagName="quotes" Src="~/admin/focus/controls/quotes.ascx" %>
<%@ Register TagPrefix="np" TagName="opportunities" Src="~/admin/focus/controls/opportunities.ascx" %>
<%@ Register TagPrefix="np" TagName="pipeline" Src="controls/pipeline.ascx" %>
<%@ Register TagPrefix="np" TagName="salesstats" Src="controls/salesstats.ascx" %>
<%@ Register TagPrefix="np" TagName="customers" Src="controls/customers.ascx" %>
<%@ Register TagPrefix="np" TagName="reports" Src="controls/reports.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Sales View</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <np:DashboardTab ID="tab" runat="server"></np:DashboardTab>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="SalesTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts" 
            MultiPageId="SalesMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsOverview" Text="Overview" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsCustomers" Text="Customers"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsPipeline" Text="Pipeline"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsSalesReport" Text="Sales Report"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsreports" Text="Reports"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsActivity" Text="Activity" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsEvents" Text="Events"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsOrders" Text="Orders"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsQuotes" Text="Quotes"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsOpportunities" Text="Opportunities"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsLogs" Text="Logs"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="SalesMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCustomers">
                <np:customers ID="customers" runat="server"></np:customers>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:pipeline ID="pipeline" runat="server"></np:pipeline>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:salesstats ID="salesstats" runat="server"></np:salesstats>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:reports ID="reports" runat="server"></np:reports>
            </ComponentArt:PageView>
            
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:activity ID="activity" runat="server"></np:activity>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:orders ID="orders" runat="server"></np:orders>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:quotes ID="quotes" runat="server"></np:quotes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvopportunities">
                <np:opportunities ID="opportunities" runat="server"></np:opportunities>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:logs ID="logs" runat="server"></np:logs>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot"></asp:Content>
