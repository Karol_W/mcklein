<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.customers" Codebehind="customers.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:GridView ID="gvCustomers" 
    runat="server" 
    AllowPaging="true"
    AutoGenerateColumns="false" 
    PageSize="50"
    HeaderStyle-CssClass="npadminsubheader"
    EmptyDataRowStyle-CssClass="npadminempty" 
    EmptyDataRowStyle-Height="50"
    EmptyDataText="No Pending Tasks" 
    CssClass="npadmintable" 
    AllowSorting="True"
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" OnSorting="gvCustomers_Sorting" OnPageIndexChanging="gvCustomers_PageIndexChanging">
    <Columns>
        <asp:HyperLinkField DataTextField="AccountName" HeaderText="colName|Name" SortExpression="AccountName" DataNavigateUrlFields="AccountID"
			DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?accountid={0}"></asp:HyperLinkField>
		<asp:BoundField DataField="Phone1" HeaderText="colPhone|Phone" SortExpression="Phone1"></asp:BoundField>
		<asp:HyperLinkField DataTextField="OpenActivitiesCount" HeaderText="colActivities|Activities" SortExpression="OpenActivitiesCount"
			DataTextFormatString="{0:#}" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?accountid={0}&tab=tsEvents">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
		</asp:HyperLinkField>
		<asp:HyperLinkField DataTextField="OpenTicketsCount" HeaderText="colTickets|Tickets" SortExpression="OpenTicketsCount"
			DataTextFormatString="{0:#}" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/support/tickets.aspx?status=open&accountid={0}">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
		</asp:HyperLinkField>
		<asp:HyperLinkField DataTextField="OpenOpportunitiesCount" HeaderText="colOpportunities|Opportunities" DataTextFormatString="{0:#}"
			SortExpression="OpenOpportunitiesCount" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?accountid={0}&tab=tsOpportunities">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
		</asp:HyperLinkField>
		<asp:HyperLinkField DataTextField="OpenQuotesCount" HeaderText="colQuotes|Quotes" DataTextFormatString="{0:#}"
			SortExpression="OpenQuotesCount" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?accountid={0}&tab=tsQuotes">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
		</asp:HyperLinkField>
		<asp:TemplateField SortExpression="Orders30" HeaderText="colOrders30|Orders 30">
			<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
			<ItemStyle HorizontalAlign="Right"></ItemStyle>
			<ItemTemplate>
				<np:PriceDisplay ID="prcOrders30" Runat="server" PriceObject='<%# Eval("Orders30") %>' />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="Orders60" HeaderText="colOrders60|Orders 60">
			<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
			<ItemStyle HorizontalAlign="Right"></ItemStyle>
			<ItemTemplate>
				<np:PriceDisplay ID="prcOrders60" Runat="server" PriceObject='<%# Eval("Orders60") %>' />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="Orders90" HeaderText="colOrders90|Orders 90">
			<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
			<ItemStyle HorizontalAlign="Right"></ItemStyle>
			<ItemTemplate>
				<np:PriceDisplay ID="prcOrders90" Runat="server" PriceObject='<%# Eval("Orders90") %>' />
			</ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
<asp:DropDownList runat="Server" ID="ddlMaxRows" AutoPostBack="true" OnSelectedIndexChanged="ddlMaxRows_SelectedIndexChanged"></asp:DropDownList>&nbsp;
&nbsp;&nbsp;
<asp:HyperLink runat="Server" id="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/common/accounts/accountnew.aspx" ToolTip="Add New Customer"></asp:HyperLink>
</div>
