<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.calendar" Codebehind="calendar.ascx.cs" %>
<table class="npadminbody" border="1" style="border-color:silver;" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="npadminheader">
            <asp:Image runat="server" ID="dot" ImageUrl="~/assets/common/icons/indicator.gif" />
            My Calendar
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Calendar ID="Calendar1" runat="server" CssClass="npadminbody" DayNameFormat="FirstTwoLetters">
                <TodayDayStyle BackColor="Silver"></TodayDayStyle>
                <DayStyle Font-Size="XX-Small" Font-Names="Verdana"></DayStyle>
                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana" CssClass="npadminlabel"></DayHeaderStyle>
                <TitleStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True"></TitleStyle>
                <WeekendDayStyle BackColor="LightYellow"></WeekendDayStyle>
            </asp:Calendar>
        </td>
    </tr>
</table>
