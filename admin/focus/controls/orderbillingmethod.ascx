<%@ Control Language="C#" AutoEventWireup="true" Codebehind="orderbillingmethod.ascx.cs" Inherits="netpoint.admin.focus.controls.orderbillingmethod" %>
<%@ Register TagPrefix="np" TagName="PaymentBlock" Src="~/admin/focus/controls/orderpayment.ascx" %>
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td class="npadminbody" colspan="3">
            <np:PaymentBlock ID="PaymentBlock" runat="server"></np:PaymentBlock>
        </td>
    </tr>
</table>
