<%@ Control Language="C#" AutoEventWireup="true" Codebehind="reports.ascx.cs" Inherits="netpoint.dashboard.controls.reports" %>
<asp:GridView 
    ID="gvReports" 
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    EmptyDataRowStyle-CssClass="npadminempty" 
    EmptyDataRowStyle-Height="50"
    EmptyDataText="No Reports Found" 
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" OnRowDataBound="gvReports_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText="colName|Name">
            <ItemTemplate>
                <asp:HyperLink ID="lnkReport" runat="server" NavigateUrl="~/admin/common/reports/ReportLaunch.aspx?code="
                    Target="_blank"><%# DataBinder.Eval(Container.DataItem, "Name") %>
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Description" HeaderText="colDescription|Description"></asp:BoundField>
    </Columns>    
</asp:GridView>