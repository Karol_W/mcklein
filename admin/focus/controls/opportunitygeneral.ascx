<%@ Control Language="C#" AutoEventWireup="true" Codebehind="opportunitygeneral.ascx.cs"
    Inherits="netpoint.admin.focus.controls.opportunitygeneral" %>
    
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Label ID="sysError" runat="server" cssclass="npwarning"></asp:Label>
<asp:Panel ID="pnlStatus" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlHeader" runat="server" Text="Opportunity # "></asp:Literal>
            </td>
            <td class="npadminbody" colspan="3">
                <asp:Literal ID="sysOrderID" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccount" runat="server" Text="Customer"></asp:Literal>
                <asp:Image ID="imgWarning" runat="server" ToolTip="Account is required" ImageUrl="~/assets/common/icons/warning.gif"
                    Visible="False"></asp:Image>
                <asp:ImageButton ID="btnReset" runat="server" Visible="false" ImageUrl="~/assets/common/buttons/reset.gif" ToolTip="Reset">
                </asp:ImageButton>
            </td>
            <td>
                <np:accountpicker id="apAccount" runat="server" PostBack="true" >
                </np:accountpicker>
            </td>
            <td class="npadminlabel" rowspan="2">
                <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
            <td rowspan="2">
                <asp:RadioButtonList ID="rblStatus" runat="server" CssClass="npadminbody" RepeatDirection="vertical">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlContact" runat="server" Text="User"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlUserID" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlOpportunityName" runat="server" Text="Opportunity Name"></asp:Literal></td>
            <td colspan="3">
                <asp:TextBox ID="txtName" runat="server" Columns="55"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlSales" runat="server" Text="Salesperson"></asp:Literal>
            </td>
            <td colspan="3">
                <asp:DropDownList ID="ddlSalesPersonID" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlGrandTotal" runat="server" Text="Total Sale"></asp:Literal>
            </td>
            <td class="npadminbody">
                <np:PriceDisplay ID="sysCashMoney" runat="server" />
            </td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlStage" runat="server" Text="Stage"></asp:Literal>
            </td>
            <td class="npadminbody">
                <asp:Literal ID="sysStage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlStartDate" runat="server" Text="Start Date"></asp:Literal>
            </td>
            <td class="npadminbody">
                <asp:Literal ID="sysStartDate" runat="server"></asp:Literal>
            </td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlPriority" runat="server" Text="Priority"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlPriorityCode" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlExpectedCloseDate" runat="server" Text="Expected Close Date"></asp:Literal></td>
            <td>
                <np:datepicker id="dtpExpectedCloseDate" runat="server">
                </np:datepicker>
            </td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlSource" runat="server" Text="Source"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlSource" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
            <td colspan="3">
                <asp:TextBox ID="txtNotes" runat="server" Columns="55" Rows="4" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
    </table>
</asp:Panel>


<asp:literal id="errBadAccount" runat="server" text="The account does not exist in the zed eCommerce database."
    visible="False"></asp:literal>