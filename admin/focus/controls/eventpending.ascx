<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.EventPending" Codebehind="EventPending.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%"
    border="0">
    <tr>
        <td class="npadminsubheader">
            <asp:ImageButton ID="btnExpand" ImageUrl="~/assets/common/icons/expand.gif" runat="server"
                Visible="False" ToolTip="Expand"></asp:ImageButton>
            <asp:ImageButton ID="btnCollapse" runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ToolTip="Collapse">
            </asp:ImageButton>
            <asp:Literal runat="server" ID="ltlPendingTasks" Text="Pending Activities" /></td>
        <td class="npadminsubheader" align="right">          
            <np:DatePicker ID="dtpCutoff" runat="server"></np:DatePicker>
        </td>
        <td class="npadminsubheader">
            <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
           <asp:GridView ID="gvEvents" 
			    runat="server" 
			    AutoGenerateColumns="false" 
			    ShowHeader="false" 
			    EmptyDataRowStyle-CssClass="npadminempty" 
			    EmptyDataRowStyle-Height="50"
			    EmptyDataText="No Pending Tasks" 
			    CssClass="npadmintable" 
			    RowStyle-CssClass="npadminbody" 
			    AlternatingRowStyle-CssClass="npadminbodyalt" 
			    OnRowDataBound="gvEvents_RowDataBound">
			    <Columns>
			        <asp:TemplateField SortExpression="ActivityCode">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="lnkType" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" ImageUrl="~/assets/common/icons/edit.gif" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StartDate" HtmlEncode="false" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEventName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Remarks") %>'
                                NavigateUrl="~/admin/focus/Activity.aspx?activityid={0}&backpage=~/admin/focus/Today.aspx">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
			    </Columns>
		    </asp:GridView>
           </td>
    </tr>
</table>
<asp:Literal runat="server" Visible="False" ID="hdnEvent" Text="Event" />
<asp:Literal runat="server" Visible="False" ID="hdnCall" Text="Call" />
<asp:Literal runat="server" Visible="False" ID="hdnAppointment" Text="Meeting" />
<asp:Literal runat="server" Visible="False" ID="hdnNote" Text="Note" />
<asp:Literal runat="server" Visible="False" ID="hdnTask" Text="Task" />
<asp:Literal runat="server" Visible="False" ID="hdnHoliday" Text="Holiday" />
