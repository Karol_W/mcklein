<%@ Control Language="C#" AutoEventWireup="true" Codebehind="opportunstages.ascx.cs"
    Inherits="netpoint.admin.focus.controls.opportunstages" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderPicker" Src="~/admin/common/controls/orderpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Panel ID="pnlStatus" runat="server">
    <asp:Label ID="sysStageError" runat="server" CssClass="npwarning"></asp:Label>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="List" ShowSummary="true" ValidationGroup="Stage" CssClass="npwarning" ForeColor="" />
    <div class="npadminbody">
        <asp:Panel runat="server" ID="pnlDetail" Visible="false">
            <table class="npadmintable">
                <tr>
                    <td class="npadminlabel" align="left">
                        <asp:ImageButton runat="server" ID="btnCancel" ImageUrl="~/assets/common/icons/cancel.gif"
                            OnClick="btnCancel_Click" ToolTip="Cancel" CausesValidation="false" /></td>
                    <td class="npadminlabel" align="right">
                        <asp:ImageButton runat="server" ID="btnSave" ImageUrl="~/assets/common/icons/saveandclose.gif"
                            OnClick="btnSave_Click" ToolTip="Save" ValidationGroup="Stage" /></td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Label runat="server" ID="lblCreateDate" Text="Start Date"></asp:Label></td>
                    <td class="npadminbody">
                        <np:DatePicker ID="calCreateDate" runat="server" Width="70px"></np:DatePicker>
                    </td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Label runat="server" ID="lblSalesPerson" Text="SalesPerson"></asp:Label></td>
                    <td class="npadminbody">
                        <asp:DropDownList ID="ddlSPID" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Label runat="server" ID="lblStage" Text="Stage"></asp:Label></td>
                    <td class="npadminbody">
                        <asp:DropDownList ID="ddlStage" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Label runat="server" ID="lblTotal" Text="Total"></asp:Label></td>
                    <td class="npadminbody">
                        <asp:TextBox ID="txtGTot" runat="server" Columns="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvGTot" runat="server" ControlToValidate="txtGTot" ErrorMessage="Total is missing." Display="Dynamic" ValidationGroup="Stage" SetFocusOnError="true">
                            <asp:Image runat="server" ID="imgRfvGTot" ImageUrl="~/assets/common/icons/warning.gif" />
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvGTot" runat="server" ControlToValidate="txtGTot" ErrorMessage="Total must be a number greater than zero." Display="Dynamic" Type="Double" Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Stage" SetFocusOnError="true">
                            <asp:Image runat="server" ID="imgCvGTot" ImageUrl="~/assets/common/icons/warning.gif" />
                        </asp:CompareValidator>
                        </td>
                </tr>
                <tr>
                    <td class="npadminlabel">
                        <asp:Label runat="server" ID="lblOrder" Text="Associated Document"></asp:Label></td>
                    <td class="npadminbody">
                        <np:OrderPicker ID="pckOrder" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="hdnOpportunityStageID" Text="0" Visible="false"></asp:Label></asp:Panel>
        <asp:GridView ID="grid" CssClass="npadmintable" runat="server" AutoGenerateColumns="false"
            OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" EmptyDataText="No Stages Assigned"
            OnRowCommand="grid_RowCommand" OnRowEditing="grid_RowEditing">
            <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
            <RowStyle CssClass="npadminbody"></RowStyle>
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
            <Columns>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                            CommandName="delete" ToolTip="Remove this stage" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colStartDate|Start Date">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colSalesPerson|Sales Person">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Literal ID="ltlSalesPerson" runat="server" Text='<%# Bind("SalesPersonID") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colStage|Stage">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnStageName" runat="server" CommandName="edit"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colTotalSale|Total Sale">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <ItemTemplate>
                        <np:PriceDisplay ID="prcTotalSale" runat="server" Price='<%# Eval("Amount") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colWgtTotal|Afflicted Total">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <ItemTemplate>
                        <np:PriceDisplay ID="prcWTot" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colOrderID|DocID">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Wrap="false"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="lnkOrderID" Target="_blank" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="npadminbody" style="text-align: right">
        <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/icons/add.gif"
            OnClick="btnNew_Click" ToolTip="New" CausesValidation="false"></asp:ImageButton>&nbsp;
    </div>
</asp:Panel>

<asp:Literal ID="errZeroAmount" runat="server" Text="Total sale must exceed zero."
    Visible="False"></asp:Literal>
<asp:Literal ID="errBadStageDate" runat="server" Visible="false" Text="The start date must be later than those of previous stages."></asp:Literal>
<asp:Literal ID="hdnLinkDocument" runat="server" Text="Link a Document." Visible="false"></asp:Literal>
