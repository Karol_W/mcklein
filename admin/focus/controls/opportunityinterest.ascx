<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="opportunityinterest.ascx.cs" Inherits="netpoint.admin.focus.controls.opportunityinterest" %>


<div class="npdaminbody">
<asp:GridView ID="gridInterest" runat="server" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" CssClass="npadmintable" 
    EmptyDataText="No Interests Designated" OnPageIndexChanging="gridInterest_PageIndexChanging" OnRowCancelingEdit="gridInterest_RowCancelingEdit" 
    OnRowDataBound="gridInterest_RowDataBound" OnRowDeleting="gridInterest_RowDeleting" OnRowEditing="gridInterest_RowEditing" 
    OnRowUpdating="gridInterest_RowUpdating" OnSorting="gridInterest_Sorting">
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <RowStyle CssClass="npadminbody" />
    <PagerStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnRemove" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/remove.gif" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colInterest|Interest" SortExpression="InterestCode">
            <ItemTemplate>
                <asp:Literal ID="ltlInterestCode" runat="server"></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlInterestCode" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPrimary|Primary" SortExpression="PrimaryFlag">
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:Image ID="imgChecked" runat="server" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Bind("PrimaryFlag") %>' />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:CheckBox ID="chkPrimary" runat="server" Checked='<%# Bind("PrimaryFlag") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" commandname="cancel" ImageUrl="~/assets/common/icons/cancel.gif" />
                &nbsp;
                <asp:ImageButton ID="btnSave" runat="server" CommandName="update" ImageUrl="~/assets/common/icons/save.gif" />
                </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />&nbsp;
</div>
</div>
<asp:Literal ID="hdnRemoveInterest" Text="Remove this Interest" runat="server" Visible="false"></asp:Literal>
<asp:Literal ID="hdnEditInterest" Text="Edit this Interest" runat="server" Visible="false"></asp:Literal>
<asp:Literal ID="hdnSaveInterest" Text="Save" runat="server" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCancel" Text="Cancel" runat="server" Visible="false"></asp:Literal>