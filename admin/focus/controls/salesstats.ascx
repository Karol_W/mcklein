<%@ Register TagPrefix="chart" Namespace="ChartDirector" Assembly="netchartdir" %>
<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.salesStats" Codebehind="salesStats.ascx.cs" %>
<table class="npadmintable">
    <tr>
        <td class="npadminbody" align="center">
            <chart:WebChartViewer ID="wcvStats" runat="server" CssClass="npadminbody"></chart:WebChartViewer>
        </td>
    </tr>
</table>
