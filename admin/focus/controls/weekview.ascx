<%@ Register TagPrefix="MC" Namespace="Mediachase.Web.UI.WebControls" Assembly="Mediachase.Web.UI.WebControls" %>
<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.weekview" Codebehind="weekview.ascx.cs" %>
<script type="text/javascript">
function addnew(time){
	window.location.href = "Activity.aspx?time=" + escape(time) + "&backpage=~/admin/focus/Calendar.aspx";
}
</script>
<table class="npadmintable">
	<tr>
		<td style="width:5%;" class="npadminsubheader"><asp:ImageButton Runat="server" ID="PreviousButton" ImageUrl="~/assets/common/icons/previous.gif" OnClick="PreviousButton_Click" ToolTip="Previous" /></td>
		<td style="width:30%;" align="left" class="npadminsubheader">&nbsp;&nbsp;<asp:Label Runat="server" ID="HeaderLabel">Header</asp:Label></td>
		<td align="right" class="npadminsubheader"><asp:DropDownList Runat="server" ID="ViewType" AutoPostBack="True" OnSelectedIndexChanged="ViewType_SelectedIndexChanged"></asp:DropDownList></td>
		<td align="right" class="npadminsubheader"><asp:DropDownList Runat="server" ID="EventType" AutoPostBack="True"></asp:DropDownList></td>
		<td align="right" class="npadminsubheader"><asp:DropDownList Runat="server" ID="AccountID" AutoPostBack="True"></asp:DropDownList></td>
		<td style="width:5%;" align="right" class="npadminsubheader"><asp:ImageButton Runat="server" ID="NextButton" ImageUrl="~/assets/common/icons/Next.gif" OnClick="NextButton_Click" ToolTip="Next" /></td>
	</tr>
	<tr>
		<td align="center" colspan="6">
			<MC:calendar id="CalendarCtrl" runat="server" CssClass="npadmintable"
				ForeColor="#80FF80" BorderWidth="1px" Palette="Windows" ViewType="WorkWeekView" MaxDisplayedItems="6" AbbreviatedDayNames="True"
				BorderStyle="Solid" AutoPostBack="True" NewLinkFormat="addnew('{0}');" Font-Size="X-Small"
				DataLinkFormat="Activity.aspx?activityid={0}&backpage=~/admin/focus/Calendar.aspx" TimescaleBottomLabelFormat="dd" TimescaleTopLabelFormat="dd, MMMM"
				MergeEvents="True" EventBarShow="True" OnItemDataBound="CalendarCtrl_ItemDataBound" OnSelectedViewChange="CalendarCtrl_SelectedViewChange" >
				<CalendarHeaderStyle HorizontalAlign="Center"></CalendarHeaderStyle>
			</MC:calendar>
		</td>
	</tr>
</table>
<asp:Literal Runat="server" Visible="False" ID="hdnTenative" Text="Tentative" />
<asp:Literal Runat="server" Visible="False" ID="hdnFeatured" Text="Featured/Reminder" />
<asp:Literal Runat="server" Visible="False" ID="hdnPersonal" Text="Personal" />
<asp:Literal Runat="server" Visible="False" ID="hdnInActive" Text="InActive" />
<asp:Literal Runat="server" Visible="False" ID="hdnEvent" Text="Event" />
<asp:Literal Runat="server" Visible="False" ID="hdnCall" Text="Call" />
<asp:Literal Runat="server" Visible="False" ID="hdnAppointment" Text="Meeting" />
<asp:Literal Runat="server" Visible="False" ID="hdnNote" Text="Note" />
<asp:Literal Runat="server" Visible="False" ID="hdnTask" Text="Task" />
<asp:Literal Runat="server" Visible="False" ID="hdnHoliday" Text="Holiday" />
