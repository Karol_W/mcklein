<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ordernotes.ascx.cs" Inherits="netpoint.admin.focus.controls.ordernotes" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>

<table class="npadmintable">
<tr>
<td class="npadminsubheader"><asp:Label runat="Server" ID="lblNotes" Text="Notes"></asp:Label></td>
</tr><tr>
<td><asp:TextBox ID="txtNotes" runat="server"  Columns="60" Rows="15" TextMode="MultiLine"></asp:TextBox></td>
</tr></table>



<asp:Table ID="tblNotesAdditionalInfo" runat="server" Width="100%">
    <asp:TableRow ID="trPONumber" runat="server">
        <asp:TableCell ID="tcPONumberLabel" runat="server" CssClass="npadminlabel">
            <asp:Literal ID="ltlPO" runat="server" Text="PO Number: "></asp:Literal>
        </asp:TableCell>
        <asp:TableCell ID="tcPOTextBox" runat="server" CssClass="npadminbody">
            <asp:TextBox ID="tbPONumber" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="trEcpirationDate" runat="server">
        <asp:TableCell ID="tcExpDateLabel" runat="server" CssClass="npadminlabel">
            <asp:Literal ID="ltlExpDate" runat="server" Text="Expiration Date: "></asp:Literal>
        </asp:TableCell>
        <asp:TableCell ID="tcExpDatePicker" runat="server" CssClass="npadminbody">
            <np:DatePicker ID="dpExpirationDate" runat="server"></np:DatePicker>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>