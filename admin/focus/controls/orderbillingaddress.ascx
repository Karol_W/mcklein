<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="orderbillingaddress.ascx.cs" Inherits="netpoint.admin.focus.controls.orderbillingaddress" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<table id="tblAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr>
        <td class="npadminsubheader">
            <asp:Literal ID="ltlAddBillingAddress" runat="server" Text="Billing Address"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblAddressSource" runat="server" OnSelectedIndexChanged="rblAddressSource_SelectedIndexChanged"
                AutoPostBack="true" RepeatDirection="Horizontal" CssClass="npadminbody">
                <asp:ListItem Text="New Address" Value="new"></asp:ListItem>
                <asp:ListItem Text="Same as Delivery" Value="sameasdelivery"></asp:ListItem>
                <asp:ListItem Text="Existing Billing Address" Value="existingbilling" Selected="true"></asp:ListItem>
                <asp:ListItem Text="Existing Delivery Address" Value="existingshipping"></asp:ListItem>
            </asp:RadioButtonList>       
        </td>
    </tr>
    <tr>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlBillingAddress" Style="max-height: 400px; max-width: 600px"
                runat="server" DataTextField="AddressDisplay" AutoPostBack="True" Width="100%"
                OnSelectedIndexChanged="ddlBillingAddress_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
    </tr>  
    <tr id="rowAddressDetails" runat="server">
        <td class="npadminsubheader">
            <asp:Literal ID="ltlAddressDetails" runat="server" Text="Address Details"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td valign="top" class="npadminbody">
            <np:AddressBlock ID="AddAddress" runat="server" HideAddressType="true" ShipDestination="false" AdminMode="true"></np:AddressBlock>
        </td>
    </tr>
</table>
<asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>