<%@ Control Language="C#" CodeBehind="activities.ascx.cs" Inherits="netpoint.admin.focus.controls.activities" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<style>	
.dateRangeCell
{
    padding:0 5px 0 0;
	white-space:nowrap;
	vertical-align:middle;
}
</style>
<table width="100%">
    <tr>
        <td class="dateRangeCell"><asp:Literal ID="Literal1" runat="server">Display activities created between</asp:Literal></td>
        <td class="dateRangeCell"><np:DatePicker ID="dtStartDate" runat="server"></np:DatePicker></td>
        <td class="dateRangeCell"><asp:Literal ID="Literal2" runat="server">and</asp:Literal></td>
        <td class="dateRangeCell"><np:DatePicker ID="dtEndDate" runat="server"></np:DatePicker></td>
        <td align="right" width="100%"><asp:ImageButton ID="btnSubmit" Runat="server" ImageUrl="~/assets/common/buttons/submit.gif" ToolTip="Search for activities between the specified date range." OnClick="btnFind_Click" /></td>
    </tr>
</table>
<asp:gridview 
    id="EventsGrid" 
    runat="server" 
    CssClass="npadmintable" 
    PageSize="20" 
    AllowPaging="True" 
    AllowSorting="True"
	AutoGenerateColumns="False" 
	EmptyDataText="No Events Created" 
	OnPageIndexChanging="EventsGrid_PageIndexChanging" 
	OnRowDataBound="EventsGrid_RowDataBound" 
	OnSorting="EventsGrid_Sorting">
	<RowStyle CssClass="npadminbody" />
	<AlternatingRowStyle CssClass="npadminbodyalt" />
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<PagerStyle CssClass="npadminbody" />
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	<Columns>
		<asp:templatefield>
			<ItemTemplate>
				<asp:HyperLink runat="server" ID="lnkID"></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:BoundField DataField="StartDate" SortExpression="StartDate" HeaderText="colDate|Date" DataFormatString="{0:ddd yyyy-MMM-dd HH:mm}"></asp:BoundField>
		<asp:HyperLinkField DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/AccountInfo.aspx?tab=tsEvents&accountid={0}"
			DataTextField="AccountID" SortExpression="AccountID" HeaderText="Customer Account|Customer Account"></asp:HyperLinkField>
		<asp:boundfield DataField="AssignedUserID" SortExpression="AssignedUserID" HeaderText="colOwner|Owner"></asp:boundfield>
		<asp:templatefield HeaderText="colName|Name" SortExpression="Remarks">
			<ItemTemplate>
				<asp:HyperLink ID="lnkRemarks" Runat="server" text='<%# DataBinder.Eval(Container.DataItem, "Remarks") %>'></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield>
			<ItemTemplate>
				<asp:HyperLink runat="server" ID="lnkStatus" ImageUrl="~/assets/common/icons/checked.gif" Visible="False" tooltip="Closed"></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
	</Columns>
</asp:gridview>
<div class="npadminactionbar">
    <asp:ImageButton ID="btnNew" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New Activity" runat="server" OnClick="btnNew_Click" />
</div>
<asp:Literal Runat="server" Visible="False" ID="hdnCall" Text="Call" />
<asp:Literal Runat="server" Visible="False" ID="hdnAppointment" Text="Meeting" />
<asp:Literal Runat="server" Visible="False" ID="hdnNote" Text="Note" />
<asp:Literal Runat="server" Visible="False" ID="hdnTask" Text="Task" />
<asp:Literal Runat="server" Visible="False" ID="hdnOther" Text="Other" />
<asp:Literal runat="server" Visible="false" ID="hdnOpportunity" Text="Opportunity: "></asp:Literal>