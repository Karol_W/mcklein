<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.tickets" Codebehind="tickets.ascx.cs" %>
<asp:gridview ID="TicketsGrid" ShowHeader="True" runat="server" AllowSorting="True"
    AllowPaging="True" CssClass="npadmintable" PageSize="25" AutoGenerateColumns="False"
    EmptyDataText="No Tickets Found" OnPageIndexChanging="TicketsGrid_OnPageIndexChanging" OnRowDataBound="TicketsGrid_OnRowDataBound" OnSorting="TicketsGrid_OnSorting">
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <PagerStyle CssClass="npadminbody" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:HyperLinkField DataTextField="TicketID" DataTextFormatString="<b>{0}</b>" DataNavigateUrlFields="TicketID"
            DataNavigateUrlFormatString="~/admin/support/ticketAdmin.aspx?ticketid={0}"
            HeaderText="colID|ID" SortExpression="TicketID">
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField HeaderText="colSubject|Subject" SortExpression="Subject" DataTextField="Subject" DataNavigateUrlFields="TicketID"
         DataNavigateUrlFormatString="~/admin/support/ticketAdmin.aspx?ticketid={0}" />
         <asp:BoundField HeaderText="colSection|Section" DataField="SectionName" SortExpression="SectionID" />
         <asp:BoundField HeaderText="colProject|Project" DataField="ProjectName" SortExpression="ProjectID" />         
         <asp:TemplateField HeaderText="colType|Type" SortExpression="TicketType">
            <ItemTemplate>
                <asp:Literal ID="ltlType" runat="server"></asp:Literal>
            </ItemTemplate>
         </asp:TemplateField>
        <asp:templatefield HeaderText="colPriority|Priority" ItemStyle-HorizontalAlign="Center" SortExpression="Priority">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "Priority") %>
                /
                <%# DataBinder.Eval(Container.DataItem, "Sequence") %>
            </ItemTemplate>
        </asp:templatefield>
    </Columns>
</asp:gridview>
<br />
<div class="npadminbody" style="text-align:right">
    <asp:Literal ID="hdnCount" runat="server" Text="Count" Visible="False"></asp:Literal>
    <asp:Literal ID="sysCount" runat="server"></asp:Literal>
    &nbsp;
    <asp:DropDownList ID="StatusList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="StatusList_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:Literal ID="ltlTicketID" runat="server" Text="Ticket ID"></asp:Literal>
    <asp:TextBox runat="server" ID="findTicketID" Columns="2" />
    <asp:ImageButton ID="btnSearch" ImageUrl="~/assets/common/icons/search.gif" runat="server"
        ToolTip="Filter"></asp:ImageButton>    
    &nbsp;
    <asp:HyperLink runat="Server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif"
        NavigateUrl="~/admin/support/ticketadd.aspx" ToolTip="Add New Ticket"></asp:HyperLink>
</div>
