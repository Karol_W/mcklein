<%@ Control Language="c#" AutoEventWireup="true" Inherits="netpoint.dashboard.controls.queue" Codebehind="queue.ascx.cs" %>
<asp:GridView ID="gvQueue" 
    runat="server" 
    AutoGenerateColumns="False" 
    EmptyDataRowStyle-CssClass="npadminempty" 
    EmptyDataRowStyle-Height="50px"
    EmptyDataText="No Tickets Found in Queues" 
    CssClass="npadmintable" 
    HeaderStyle-CssClass="npadminsubheader"
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" OnRowDataBound="gvQueue_RowDataBound">
    <Columns>
		<asp:BoundField DataField="ProjectName" HeaderText="colProject|Project"></asp:BoundField>
		<asp:BoundField DataField="SectionName" HeaderText="colSection|Section"></asp:BoundField>
		<asp:BoundField DataField="CodeName" HeaderText="colStatus|Status"></asp:BoundField>
        <asp:TemplateField HeaderText="colCount|Count" ItemStyle-HorizontalAlign="center">
            <ItemTemplate>
                <asp:HyperLink ID="lnkCount" runat="server" NavigateUrl="~/admin/support/tickets.aspx?"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataRowStyle CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
</asp:GridView>