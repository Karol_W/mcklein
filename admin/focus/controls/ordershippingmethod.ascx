<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ordershippingmethod.ascx.cs" Inherits="netpoint.admin.focus.controls.ordershippingmethod" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<table cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlDeliveryDate" runat="server" Text="Delivery Date"></asp:Literal></td>
        <td class="npadminbody"><np:DatePicker ID="DeliveryDate" runat="server"></np:DatePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlShipMethod" runat="server" Text="Delivery Method"></asp:Literal></td>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblShipMethod" runat="server" CssClass="npbody"
                CellPadding="2" CellSpacing="0" >
            </asp:RadioButtonList>
            
            <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
        </td>
    </tr>
</table>
<asp:Literal ID="errNoShipping" runat="server" Text="Freight rates are not available for this user." Visible="False"></asp:Literal>
<asp:Literal ID="errConfigureShipping" runat="server" Text="Rates have not been defined for this order: the subtotal or weight of the order does not meet the cost structure of any defined rate." Visible="False"></asp:Literal>
