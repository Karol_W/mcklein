<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.ActivityActions" Codebehind="ActivityActions.ascx.cs" %>
<table class="npadminbody" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="2" class="npadminsubheader"><asp:Literal ID="Literal1" runat="server" Text="Actions"></asp:Literal></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;</td>
        <td>
            <asp:CheckBox ID="chkEmailContact" runat="server" Text="Email Contact a copy of this Event" />
            <br />
            <asp:CheckBox ID="chkCreateOpporunity" runat="server" Text="Create Opportunity" />
            <br />
            <asp:CheckBox ID="chkCreateTicket" runat="server" Text="Create Ticket" />
            <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
        </td>
    </tr>
</table>
<asp:Literal ID="errNoSubject" runat="server" Text="Support Ticket can not be created without an event description" Visible="False"></asp:Literal>
<asp:Literal ID="errNoMessage" runat="server" Text="Support ticket can not be created with event notes" Visible="False"></asp:Literal>
