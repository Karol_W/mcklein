<%@ Control Language="C#" AutoEventWireup="true" Codebehind="orderopportunity.ascx.cs" Inherits="netpoint.admin.focus.controls.orderopportunity" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlLinkOpp" runat="server" Text="Link Opportunity"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlOpportunity" runat="server"></asp:DropDownList></td>
    </tr>
</table>
<asp:Literal ID="hdnCreateOpportunity" runat="server" Text="Auto-Create New Opportunity" Visible="false"></asp:Literal>
<asp:Literal ID="hdnAutoOpportunity" runat="server" Text="Opportunity Created from #" Visible="false"></asp:Literal>
