<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="orderaddresses.ascx.cs" Inherits="netpoint.admin.focus.controls.orderaddresses" %>
<%@ Register Src="ordershippingaddress.ascx" TagName="ordershippingaddress" TagPrefix="np" %>
<%@ Register Src="orderbillingaddress.ascx" TagName="orderbillingaddress" TagPrefix="np" %>
<np:ordershippingaddress ID="shippingaddress" runat="server"></np:ordershippingaddress>
<np:orderbillingaddress ID="billingaddress" runat="server"></np:orderbillingaddress>