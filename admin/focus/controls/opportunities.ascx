<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.opportunities" Codebehind="opportunities.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:gridview id="OpportunityGrid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable" AllowSorting="True" AllowPaging="True" PageSize="20"
    EmptyDataText="No Opportunities" OnPageIndexChanging="OpportunityGrid_PageIndexChanging" OnRowDataBound="OpportunityGrid_RowDataBound" OnSorting="OpportunityGrid_Sorting">
				<RowStyle CssClass="npadminbody" />
				<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
				<AlternatingRowStyle cssclass="npadminbodyalt" />
				<PagerStyle CssClass="npadminbody" />
				<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
				<Columns>
					<asp:templatefield HeaderText="colID|ID" SortExpression="o.OpportunityID">
						<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:HyperLink ID="lnkOpportunity" Runat="server" Text='<%# Bind("OpportunityID") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:templatefield>
					<asp:TemplateField HeaderText="colDate|Date" SortExpression="sos.StartDate">
					    <ItemStyle HorizontalAlign="center" />
					    <ItemTemplate>
					        <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
					    </ItemTemplate>
					</asp:TemplateField>
					<asp:HyperLinkField DataNavigateUrlFields="OpportunityID" DataNavigateUrlFormatString="~/admin/focus/OpportunityDetail.aspx?selected=3&OpportunityID={0}"
						DataTextField="Name" HeaderText="colName|Name" SortExpression="o.Name"></asp:HyperLinkField>
					<asp:templatefield HeaderText="Customer|Customer" SortExpression="o.AccountID">
						<HeaderStyle Width="30%"></HeaderStyle>
						<ItemTemplate>
							<asp:HyperLink id="lnkAccountID" runat="server" NavigateUrl="~/admin/common/accounts/AccountInfo.aspx?tab=tsOpportunities&accountid=">
								<%# DataBinder.Eval(Container.DataItem, "AccountID") %>
							</asp:HyperLink>
						</ItemTemplate>
					</asp:templatefield>
					<asp:boundfield HeaderText="colStage|Stage" DataField="CurrentStage" SortExpression="CurrentStage"></asp:boundfield>
					<asp:templatefield HeaderText="colValue|Value" SortExpression="Amount">
						<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
						<ItemTemplate>
							<np:PriceDisplay ID="prcValue" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "Amount") %>' />
						</ItemTemplate>
					</asp:templatefield>
				</Columns>
			</asp:gridview>
<br />
<div class="npadminactionbar">
<asp:DropDownList ID="ddlStatus" Runat="server" AutoPostBack="True" onselectedindexchanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>
<asp:HyperLink runat="Server" id="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/focus/OpportunityAdd.aspx" ToolTip="Add New Opportunity"></asp:HyperLink>
</div>
<asp:Literal id="hdnWon" runat="server" Text="Won" Visible="False"></asp:Literal>
<asp:Literal id="hdnLost" runat="server" Text="Lost" Visible="False"></asp:Literal>
<asp:Literal id="hdnOpen" runat="server" Text="Open" Visible="False"></asp:Literal>
