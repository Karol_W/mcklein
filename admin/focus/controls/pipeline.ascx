<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.pipeline" Codebehind="pipeline.ascx.cs" %>
<%@ Register TagPrefix="chart" Namespace="ChartDirector" Assembly="netchartdir" %>
<table class="npadmintable">
    <tr>
        <td align="center">
            <chart:WebChartViewer ID="wcvPipeline" runat="server" Width="570" Height="70"></chart:WebChartViewer></td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="sysHTML" runat="server" EnableViewState="False"></asp:Literal></td>
    </tr>
    <tr><td align="right"><asp:HyperLink runat="Server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/focus/OpportunityAdd.aspx" ToolTip="New"></asp:HyperLink></td></tr>
</table>
