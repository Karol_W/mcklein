<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ordercustomer.ascx.cs" Inherits="netpoint.admin.focus.controls.ordercustomer" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<asp:Label ID="sysErr" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadminordertable">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlSalesperson" runat="server" Text="Salesperson: "></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlSalesperson" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel" valign="top"><asp:Literal ID="ltlAccount" runat="server" Text="Account: "></asp:Literal></td>
        <td class="npadminbody"><np:AccountPicker runat="server" ID="apAccountId" PostBack="true" Required="true" /></td>
    </tr>
    <tr>
        <td class="npadminlabel" ><asp:Literal ID="ltlContact" runat="server" Text="User: "></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlUserID" runat="server"></asp:DropDownList></td>
    </tr>
</table>
<asp:Literal ID="ltlErr" runat="server" Text="Please choose a valid account" Visible="false"></asp:Literal>
