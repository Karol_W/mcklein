<%@ Control Language="C#" AutoEventWireup="true" Codebehind="orderverify.ascx.cs" Inherits="netpoint.admin.focus.controls.orderverify" %>
<%@ Register TagPrefix="np" TagName="PartPrice" Src="~/commerce/controls/partprice.ascx" %>    
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="Discount" Src="~/commerce/controls/Discount.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

 <table class="npadmintable">
    <tr>
        <td class="npadminsubheader"><asp:Literal ID="ltlShippingTo" runat="server" Text="Deliver To"></asp:Literal></td>
        <td class="npadminsubheader"><asp:Literal ID="ltlBillingTo" runat="server" Text="Billing To"></asp:Literal></td>
        <td class="npadminsubheader"><asp:Literal ID="ltlShippingVia" runat="server" Text="Deliver Via"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminbody" valign="top"><asp:HyperLink ID="syslinksa" runat="server">
            <asp:Literal ID="sysShippingAddress" runat="server"></asp:Literal></asp:HyperLink></td>
        <td class="npadminbody" valign="top"><asp:HyperLink ID="syslinkba" runat="server">
            <asp:Literal runat="server" ID="sysBillingAddress"></asp:Literal></asp:HyperLink></td>
        <td class="npadminbody" valign="top"><asp:HyperLink ID="syslinksm" runat="server">
            <asp:Literal runat="server" ID="sysShipMethod"></asp:Literal></asp:HyperLink></td>
    </tr>
</table>
<br />
<asp:GridView ID="OrderDetailsGrid" runat="server" CssClass="npadmintable"
    AutoGenerateColumns="False" OnRowDataBound="OrderDetailsGrid_RowDataBound">
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <FooterStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:BoundField DataField="Quantity" HeaderText="colQty|Qty">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle Font-Bold="True" HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="colDescription|Description">
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="PartNo" Text='<%# DataBinder.Eval(Container, "DataItem.PartNo") %>' />
                -
                <asp:Label runat="server" ID="PartName" Text='<%# DataBinder.Eval(Container, "DataItem.PartName") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-HorizontalAlign="right" HeaderText="colUnitPrice|Unit Price" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcAmount" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "PurchasePrice") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-HorizontalAlign="right" HeaderText="colExtended|Extended" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PartPrice ID="partPrice" runat="server" ShowOriginalPrice="true" ShowOriginalPriceLabel="true" 
                    ShowDiscount="true" ShowDiscountLabel="true" ShowFinalPriceLabel="false"/>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:Panel ID="pnl" Runat="server">
	<np:Discount id="discount" runat="server" AllowEdit="false"></np:Discount>
</asp:Panel>
<table class="npadmintable">
    <tr>
        <td class="npadminsubheader" style="width:50%;"><asp:Literal ID="ltlPayments" runat="server" Text="Payments"></asp:Literal></td>
        <td class="npadminsubheader" align="center">&nbsp;</td>
    </tr>
    <tr>
        <td class="npadminbody" valign="top">
            <asp:DataGrid ID="gridPayments" runat="server" AutoGenerateColumns="False" Width="100%"
                GridLines="None" CellPadding="3" BorderStyle="None" ShowHeader="False">
                <ItemStyle CssClass="npadminbody"></ItemStyle>
                <Columns>
                    <asp:HyperLinkColumn DataTextField="PaymentName" HeaderText="ltlCurrentPayments|Name"></asp:HyperLinkColumn>
                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="right" HeaderText="colAmmount|Amount" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:Literal ID="ltlAmount" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </td>
        <td class="npadminbody" valign="top" align="right">
            <table id="Table1" cellspacing="0" cellpadding="2" border="0">
                <tr>
                    <td class="npadminbody" align="right" colspan="2"><np:OrderExpenses ID="expenses" runat="server" /></td>
                </tr>
                <tr>
                    <td class="npadminbody"><asp:Literal ID="ltlPay" runat="server" Text="Payments:"></asp:Literal></td>
                    <td class="npadminbody" align="right"><np:PriceDisplay ID="sysPaymentTotal" runat="server" /></td>
                </tr>
                <tr>
                    <td class="npadminbody"></td>
                    <td class="npadminbody" align="right"> - - -</td>
                </tr>
                <tr>
                    <td class="npadminbody"><b><asp:Literal ID="ltlBal" runat="server" Text="Balance"></asp:Literal></b></td>
                    <td class="npadminbody" align="right"><np:PriceDisplay ID="sysBalance" runat="server" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />

    
