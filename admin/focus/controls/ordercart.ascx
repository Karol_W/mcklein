<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ordercart.ascx.cs" Inherits="netpoint.admin.focus.controls.ordercart" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="Discount" Src="~/commerce/controls/Discount.ascx" %>
<%@ Register TagPrefix="np" TagName="partdesc" Src="~/commerce/controls/partdescription.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPrice" Src="~/commerce/controls/partprice.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<script type="text/javascript">
    function doPercent(controlnameAndPrice){
       var parts = controlnameAndPrice.split(',');
       var div1 = document.getElementById(parts[0]);
       var div2 = document.getElementById(parts[0].replace("divPrice", "divPercent"));
       var textBox = document.getElementById(parts[0].replace("divPrice", "txtPercent"));
       
       div1.style.display = 'none';
       div2.style.display = 'block';
    }
</script>
<asp:GridView ID="gvOrderDetails" runat="server" CssClass="npadmintable" ShowFooter="false"
    AutoGenerateColumns="False" OnRowCommand="gvOrderDetails_RowCommand" OnRowDataBound="gvOrderDetails_RowDataBound">
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader" />
    <EmptyDataRowStyle CssClass="npadminempty" />
    <FooterStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderDetailID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colQuantity|Quantity">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
            <ItemTemplate>
                <asp:TextBox ID="Quantity" runat="server" Width="35px" Columns="4"></asp:TextBox>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="Quantity" runat="server" Width="35px" Columns="4" Text='1'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDescription|Description">
            <ItemTemplate>
                <np:partdesc ID="partDesc" runat="server" ShowPartDescription="false" ShowPartName="true" ShowPartNo="true" />
            </ItemTemplate>
            <EditItemTemplate>
                <np:PartPicker ID="PartNum" runat="server" Width="100px"></np:PartPicker>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colPrice|Price">
            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
            <ItemStyle HorizontalAlign="Right" Width="95px"></ItemStyle>
            <ItemTemplate>
                <div id="divPrice" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td colspan="2" align="right">
                                <np:PartPrice ID="partPrice" runat="server" ShowOriginalPrice="true" ShowOriginalPriceLabel="true" 
                                    ShowDiscount="true" ShowDiscountLabel="true" ShowFinalPriceLabel="false"/></td>
                        </tr>
                        <tr>
                            <td align="right"><a style="white-space:nowrap;" id="aDiscount" runat="server">
                                <asp:MultiView ID="mvDiscount" runat="server">
                                    <asp:View ID="viewNoDiscount" runat="server">
                                        <%= hdnApplyDiscount.Text %>
                                    </asp:View>
                                    <asp:View ID="viewHasDiscount" runat="server">
                                        <%= hdnDiscount.Text %> <np:PriceDisplay ID="prcDiscount" runat="server" />
                                    </asp:View>
                                </asp:MultiView>
                            </a></td>
                        </tr>
                    </table>
                </div>
                <div id="divPercent" runat="server" style="display:none;">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td align="right">
                                <asp:TextBox runat="server" ID="txtPercent" Width="40px"></asp:TextBox>%&nbsp;
                                <asp:ImageButton ID="imgCalc" runat="server" CommandName="calcpercent"
                                   CommandArgument='<%# DataBinder.Eval(Container.DataItem,"OrderDetailID") %>' ImageUrl="~/assets/common/icons/calculate.gif" /></td>
                        </tr>
                    </table>
                </div>
                <div id="divTrace" runat="server">
                    <asp:Hyperlink ID="lnkTrace" runat="server" ImageUrl="~/assets/common/icons/query.gif" />
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCalcUoM|Calc UoM" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:DropDownList runat="server" ID="ddlCalcUoM" DataTextField='<%# DataBinder.Eval(Container.DataItem,"CalculationUnitofMeasure") %>'></asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colExtended|Extended" HeaderStyle-HorizontalAlign="right" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcExtended" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "DiscountedPrice") %>'/>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkNote" runat="server" ImageUrl="~/assets/common/icons/notes.gif"></asp:HyperLink>
                <asp:HiddenField ID="hdfNote" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Notes") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="updaterow" 
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>' ToolTip="Update the changes for this line." />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="add" />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table class="npadmintable">
    <tr>
        <td>
            <asp:Image runat="server" ID="imgOutOfStock" ImageUrl="~/assets/common/icons/warning.gif" Visible="False" />
            <asp:Label ID="lblOutOfStock" runat="server" Text="Some items in your order are on back order" Visible="False" CssClass="npwarning"></asp:Label>
            <br />
            <asp:Image runat="server" ID="imgUnavailable" ImageUrl="~/assets/common/icons/availableno.gif" Visible="False" />
            <asp:Label ID="lblUnavailable" runat="server" Text="This item is no longer available; remove it from the cart before you check out"
                Visible="False" CssClass="npwarning"></asp:Label></td>
        <td align="right" class="npadminlabel"><b><asp:Literal ID="ltlSubTotal" runat="server" Text="Sub Total"></asp:Literal>:
            &nbsp;&nbsp;
            <np:PriceDisplay ID="sysSubTotal" runat="server" /></b></td>
        
    </tr>
</table>
<br />
<np:Discount ID="discount" runat="server"></np:Discount>
<br />
<asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
<asp:Literal ID="hdnNotes" runat="server" Text="Line Notes" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDelete" runat="server" Text="Remove Line" Visible="false"></asp:Literal>
<asp:Literal ID="hdnApplyDiscount" runat="server" Text="Apply Discount" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDiscount" runat="server" Text="Discount: " Visible="false"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Update Line" Visible="false"></asp:Literal>
<asp:Literal ID="hdnAdd" runat="server" Text="Add Line" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPercentage" runat="server" Text="Calculate Percentage" Visible="false"></asp:Literal>
<asp:Literal ID="errPercentMustBeNumber" runat="server" Text="Percent must be a number." Visible="false"></asp:Literal>
<asp:Literal ID="errQuantity" runat="server" Text="Quantity must be an integer." Visible="false"></asp:Literal>
<asp:Literal ID="hdnPriceTrace" runat="server" Text="Price Trace Data" Visible="false"></asp:Literal>
