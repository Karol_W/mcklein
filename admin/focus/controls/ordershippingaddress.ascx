<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ordershippingaddress.ascx.cs" Inherits="netpoint.admin.focus.controls.ordershippingaddress" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<table id="tblAddress" cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr>
        <td class="npadminsubheader"><asp:Literal ID="ltlAddShippingAddress" runat="server" Text="Delivery Address"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminbody">
            <asp:RadioButtonList ID="rblAddressSource" runat="server" OnSelectedIndexChanged="rblAddressSource_SelectedIndexChanged"
                AutoPostBack="true" RepeatDirection="Horizontal" CssClass="npadminbody">
                <asp:ListItem Text="New Address" Value="new"></asp:ListItem>
                <asp:ListItem Text="Existing Delivery Address" Value="existingshipping" Selected="true"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
         <td class="npadminbody">
            <asp:DropDownList ID="ddlShippingAddress" Style="max-height: 400px; max-width: 600px"
                runat="server" AutoPostBack="true" DataTextField="AddressDisplay" Width="100%" 
                OnSelectedIndexChanged="ddlShippingAddress_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminsubheader">
            <asp:Literal ID="ltlAddressDetails" runat="server" Text="Address Details"></asp:Literal>
        </td>
    </tr>    
    <tr>    
        <td valign="top" class="npadminbody"><np:AddressBlock ID="AddAddress" runat="server" HideAddressType="true" ShipDestination="true" AdminMode="true"></np:AddressBlock></td>
    </tr>
</table>
<asp:Literal ID="hdnNewAddress" runat="server" Text="New Address" Visible="False"></asp:Literal>