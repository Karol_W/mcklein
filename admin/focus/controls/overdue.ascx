<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.overdue" Codebehind="overdue.ascx.cs" %>
<table class="npadminbody" style="border-color:silver;" cellspacing="0" cellpadding="0" width="100%"
	border="0">
	<tr>
		<td class="npadminsubheader">
		    <asp:imagebutton id="btnExpand" ImageUrl="~/assets/common/icons/expand.gif" Runat="server" Visible="False" ToolTip="Expand" />
			<asp:ImageButton ID="btnCollapse" Runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ToolTip="Collapse" />
			<asp:Literal Runat="server" ID="ltlOverdueHeader" Text="Overdue Activities" /></td>
		<td class="npadminsubheader" align="right">
			<asp:HyperLink ID="lnkNew" Runat="server" ImageUrl="~/assets/common/icons/add.gif" 
			    navigateurl="~/admin/focus/Activity.aspx?activityid=0&amp;backpage=~/admin/focus/Today.aspx"
				ToolTip="Add New Task"></asp:HyperLink></td>
	</tr>
	<tr>
		<td colspan="2">
			<asp:GridView ID="gvTasks" 
			    runat="server" 
			    AutoGenerateColumns="false" 
			    ShowHeader="false" 
			    EmptyDataRowStyle-CssClass="npadminempty" 
			    EmptyDataRowStyle-Height="50px"
			    EmptyDataText="No Overdue Tasks" 
			    CssClass="npadmintable" 
			    RowStyle-CssClass="npadminbody" 
			    AlternatingRowStyle-CssClass="npadminbodyalt" OnRowDataBound="gvTasks_RowDataBound">
			    <Columns>
			        <asp:TemplateField SortExpression="ActivityCode">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
						<ItemTemplate>
							<asp:HyperLink Runat="server" ID="lnkType" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField Visible="False">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:HyperLink id="lnkEdit" ImageUrl="~/assets/common/icons/edit.gif" NavigateUrl="~/common/accounts/userinfo.aspx?ResourceID=link"
								runat="server"></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="StartDate" HtmlEncode="false" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
					<asp:HyperLinkField DataNavigateUrlFields="ActivityID" DataNavigateUrlFormatString="~/admin/focus/Activity.aspx?activityid={0}&backpage=~/admin/focus/Today.aspx"
						DataTextField="Remarks" DataTextFormatString="{0}"></asp:HyperLinkField>
			    </Columns>
		    </asp:GridView>
		</td>
	</tr>
</table>
<asp:Literal Runat="server" Visible="False" ID="hdnEvent" Text="Event" />
<asp:Literal Runat="server" Visible="False" ID="hdnCall" Text="Call" />
<asp:Literal Runat="server" Visible="False" ID="hdnAppointment" Text="Meeting" />
<asp:Literal Runat="server" Visible="False" ID="hdnNote" Text="Note" />
<asp:Literal Runat="server" Visible="False" ID="hdnTask" Text="Task" />
<asp:Literal Runat="server" Visible="False" ID="hdnHoliday" Text="Holiday" />
