<%@ Control Language="C#" AutoEventWireup="true" Codebehind="projects.ascx.cs" Inherits="netpoint.dashboard.controls.projects" %>
<asp:GridView ID="gvProjects" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
    EmptyDataText="No Projects" OnRowDataBound="gvProjects_RowDataBound">
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle HorizontalAlign="center" Height="50" CssClass="npadminempty" />
    <Columns>
        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="SupportProjectID" DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}"
            SortExpression="SupportProjectID" DataTextField="SupportProjectID">
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
        <asp:HyperLinkField HeaderText="colName|Name" DataNavigateUrlFields="SupportProjectID"
            DataNavigateUrlFormatString="~/admin/support/project.aspx?projectid={0}"
            SortExpression="ProjectName" DataTextField="ProjectName" />
        <asp:HyperLinkField HeaderText="Customer|Customer" DataNavigateUrlFields="AccountID" DataNavigateUrlFormatString="~/admin/common/accounts/accountinfo.aspx?accountid={0}"
            SortExpression="AccountID" DataTextField="AccountID" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStart|Start" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
            <ItemTemplate>
                <asp:Literal ID="ltlStartDate" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEnd|End" ItemStyle-HorizontalAlign="center" SortExpression="ActualEndDate">
            <ItemTemplate>
                <asp:Literal ID="ltlEndDate" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
<asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/support/projectadd.aspx" ToolTip="Add New Project"></asp:HyperLink>
</div>