<%@ Register TagPrefix="chart" Namespace="ChartDirector" Assembly="netchartdir" %>
<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.summary" Codebehind="summary.ascx.cs" %>
<table class="npadmintable">
    <tr>
        <td align="center">
            <chart:WebChartViewer ID="theChart" runat="server"></chart:WebChartViewer></td>
    </tr>
    <tr>
        <td align="center">
            <asp:gridview
                id="gvSummary"
                runat="server" 
                AutoGenerateColumns="false" 
                HeaderStyle-CssClass="npadminsubheader"
                CssClass="npadmintable" 
                RowStyle-CssClass="npadminbody" 
                AlternatingRowStyle-CssClass="npadminbodyalt" 
                EmptyDataText="No Records Found">
                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                <Columns>
                    <asp:BoundField DataField="qtr" HeaderText="colQtr|Qtr">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="openCount" HeaderText="colOpen|Open">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="stalledCount" HeaderText="colStalled|Stalled">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="closedCount" HeaderText="colClosed|Closed">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:gridview>
        </td>
    </tr>
</table>
