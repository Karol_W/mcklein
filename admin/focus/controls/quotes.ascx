
<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.quotes" Codebehind="quotes.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Label ID="sysError" Runat="server" CssClass="npwarning"></asp:Label>
<asp:gridview id="QuotesGrid" AutoGenerateColumns="False" AllowSorting="True"
				AllowPaging="True" PageSize="20" CssClass="npadmintable" runat="server" EmptyDataText="No Results Found"  
				OnPageIndexChanging="QuotesGrid_PageIndexChanging" OnRowCommand="QuotesGrid_RowCommand" 
				OnRowDataBound="QuotesGrid_RowDataBound" OnSorting="QuotesGrid_Sorting">
				<RowStyle CssClass="npadminbody" />
				<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
				<AlternatingRowStyle CssClass="npadminbodyalt" />
				<PagerStyle CssClass="npadminbody" />
				<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
				<Columns>
					<asp:templatefield SortExpression="OrderID" HeaderText="colID|ID">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
						<ItemTemplate>
							<asp:HyperLink id="lnkID" runat="server">
								<%# DataBinder.Eval(Container.DataItem, "OrderID") %>
							</asp:HyperLink>
						</ItemTemplate>
					</asp:templatefield>
					<asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="colDate|Date" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
					<asp:templatefield SortExpression="AccountID" HeaderText="Customer|Customer">
						<ItemTemplate>
							<asp:HyperLink id="lnkAccount" runat="server" NavigateUrl="~/admin/common/accounts/AccountInfo.aspx?tab=tsQuotes&accountid=">
								<%# DataBinder.Eval(Container.DataItem, "AccountID") %>
							</asp:HyperLink>
						</ItemTemplate>
					</asp:templatefield>
					<asp:boundfield DataField="SalesPersonID" SortExpression="SalesPersonID" HeaderText="colSalesperson|Salesperson"></asp:boundfield>
					<asp:boundfield DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colQuantity|Quantity">
						<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:boundfield>
					<asp:templatefield SortExpression="GrandTotal" HeaderStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="right" HeaderText="colTotal|Total">
						<ItemTemplate>
							<np:PriceDisplay ID="prcTotal" Runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
						</ItemTemplate>
					</asp:templatefield>
					<asp:templatefield SortExpression="CartType">
						<HeaderStyle Width="5%"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:ImageButton ID="btnLockQuote" Runat="server" ImageUrl="~/assets/common/icons/lock.gif" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderID") %>' commandname="lock">
							</asp:ImageButton>
							<asp:HyperLink ID="lnkPromote" Runat="server" ImageUrl="~/assets/common/icons/upgrade.gif" >
							</asp:HyperLink>
						</ItemTemplate>
					</asp:templatefield>
				</Columns>
			</asp:gridview>
<br />
<div class="npadminactionbar">
<asp:HyperLink runat="Server" id="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/focus/OrderDetail.aspx?OrderType=QU" ToolTip="Add New Quote"></asp:HyperLink>
</div>
<asp:Literal id="hdnLock" runat="server" Text="Lock this quote" Visible="False"></asp:Literal>
<asp:Literal ID="errNoDetail" Runat="server" Text="There are no items on the quote. It can not be completed."
	Visible="False"></asp:Literal>
