<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="opportunityreasons.ascx.cs" Inherits="netpoint.admin.focus.controls.opportunitylostreasons" %>

<asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" cssclass="npadmintable" OnRowCancelingEdit="grid_RowCancelingEdit" 
    OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" OnRowUpdating="grid_RowUpdating" OnRowEditing="grid_RowEditing"
     EmptyDataText="No reasons have been identified for this opportunity.">
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete"
                      />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colReason|Reason">
            <ItemTemplate>
                <asp:literal ID="ltlReason" runat="server"></asp:literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlReason" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colNote|Note">
            <ItemTemplate>
                <asp:Literal ID="ltlNote" runat="server" Text='<%# Bind("Note") %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtNote" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Note") %>' ></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandName="edit" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cancel" />
                &nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="update"
                      />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" />
</div>