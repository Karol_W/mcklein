<%@ Control Language="C#" AutoEventWireup="true" Codebehind="workorders.ascx.cs"
    Inherits="netpoint.dashboard.controls.workorders" %>
<asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="npadmintable"
    AllowPaging="true" AllowSorting="true" OnRowDataBound="grid_RowDataBound" OnPageIndexChanging="grid_PageIndexChanging"
    EmptyDataText="No Service Calls" OnSorting="grid_OnSorting">
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle HorizontalAlign="center" Height="50" CssClass="npadminempty" />
    <Columns>
        <asp:HyperLinkField HeaderText="colID|ID" DataNavigateUrlFields="SupportWorkOrderID"
            DataNavigateUrlFormatString="~/admin/support/workorder.aspx?workid={0}"
            DataTextField="SupportWorkOrderID" SortExpression="SupportWorkOrderID">
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
        <asp:BoundField HeaderText="colName|Name" DataField="WorkName" SortExpression="WorkName" />
        <asp:BoundField HeaderText="Customer|Customer" DataField="AccountID" SortExpression="AccountID" />
        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Status">
            <ItemTemplate>
                <asp:Literal ID="ltlStatus" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Priority|Priority" SortExpression="Priority">
            <ItemTemplate>
                <asp:Literal ID="ltlPriority" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:HyperLink ID="lnkWork" runat="server" ToolTip="View Work Order" NavigateUrl="~/admin/support/workorder.aspx"
                    ImageUrl="~/assets/common/icons/detail.gif"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle CssClass="npadminlabel" HorizontalAlign="Right" />
</asp:GridView>
<br />
<div class="npadminactionbar">
<asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/support/workorderadd.aspx" ToolTip="Add New Service Call"></asp:HyperLink>
</div>

<asp:Literal ID="hdnViewWork" runat="server" Text="View Service Call" Visible="false"></asp:Literal>