<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.ActivityLinked" Codebehind="ActivityLinked.ascx.cs" %>
<div id="divNew" runat="server">
    <table class="npadminbody" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDocType" runat="server" Text="Document Type"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlDocType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="O" Text="Order"></asp:ListItem>
                    <asp:ListItem Value="I" Text="Invoice"></asp:ListItem>
                    <asp:ListItem Value="Q" Text="Quote"></asp:ListItem>
                    <asp:ListItem Value="R" Text="Shipment"></asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDocNum" runat="server" Text="Document Number"></asp:Literal></td>
            <td class="npadminbody">
                <asp:DropDownList ID="ddlDocNum" runat="server"></asp:DropDownList>&nbsp;
                <asp:HyperLink ID="lnkDoc" runat="server"></asp:HyperLink></td>
        </tr>
    </table>
</div>

