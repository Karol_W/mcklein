<%@ Control Language="c#" Inherits="netpoint.dashboard.controls.orders" Codebehind="orders.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:gridview id="OrdersGrid" AutoGenerateColumns="False" AllowSorting="True"
	AllowPaging="True" PageSize="20" CssClass="npadmintable" runat="server" EmptyDataText="No Results Found" 
	OnPageIndexChanging="OrdersGrid_PageIndexChanging" OnRowDataBound="OrdersGrid_RowDataBound" OnSorting="OrdersGrid_Sorting">
	<RowStyle CssClass="npadminbody" />
	<HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
	<AlternatingRowStyle CssClass="npadminbodyalt" />
	<PagerStyle CssClass="npadminbody" />
	<EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	<Columns>
		<asp:templatefield SortExpression="OrderID" HeaderText="colID|ID">
			<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink id="lnkID" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "OrderID")%>'>
</asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:templatefield SortExpression="CartType" HeaderText="colDraft|Draft">
			<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="imgDraft" runat="server" ImageUrl="~/assets/common/icons/checked.gif" />
			</ItemTemplate>
		</asp:templatefield>
		<asp:boundfield DataField="CreateDate" SortExpression="CreateDate" HeaderText="colDate|Date" DataFormatString="{0:yyyy-MMM-dd}"></asp:boundfield>
		<asp:templatefield SortExpression="AccountID" HeaderText="Customer|Customer">
			<HeaderStyle HorizontalAlign="center"></HeaderStyle>
			<ItemStyle HorizontalAlign="center"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink id="lnkAccount" runat="server" NavigateUrl="~/admin/common/accounts/AccountInfo.aspx?tab=tsOrders&accountid=">
					<%# DataBinder.Eval(Container.DataItem, "AccountID") %></asp:HyperLink>
			</ItemTemplate>
		</asp:templatefield>
		<asp:boundfield DataField="SalesPersonID" SortExpression="SalesPersonID" HeaderText="colSalesperson|Salesperson"></asp:boundfield>
		<asp:boundfield DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colQuantity|Quantity">
			<HeaderStyle HorizontalAlign="center"></HeaderStyle>
			<ItemStyle HorizontalAlign="center"></ItemStyle>
		</asp:boundfield>
		<asp:templatefield SortExpression="GrandTotal" HeaderText="colTotal|Total">
		    <ItemStyle HorizontalAlign="right" />
			<ItemTemplate>
				<np:PriceDisplay ID="prcTotal" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "GrandTotal") %>' />
			</ItemTemplate>
		</asp:templatefield>
	</Columns>
</asp:gridview>
<br />
<div class="npadminactionbar">
    <asp:DropDownList runat="Server" ID="ddlMaxRows" AutoPostBack="true" OnSelectedIndexChanged="ddlMaxRows_SelectedIndexChanged"></asp:DropDownList>&nbsp;
    &nbsp;&nbsp;
    <asp:HyperLink runat="Server" id="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/focus/OrderAdd.aspx" ToolTip="Add New Order"></asp:HyperLink>
</div>
