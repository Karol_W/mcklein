<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="opportunitycompetition.ascx.cs" Inherits="netpoint.admin.focus.controls.opportunitycompetition" %>


<asp:GridView ID="grid" runat="server" CssClass="npadmintable" AutoGenerateColumns="false" OnRowCancelingEdit="grid_RowCancelingEdit" 
    OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating"
     EmptyDataText="No Known Competition for Opportunity">
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader" />
    <EmptyDataRowStyle CssClass="npadminempty" height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete"
                     CommandArgument='<%# Bind("OpportunityCompetitionID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colWon|Won">
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:Image runat="server" ID="imgWon" ImageUrl="~/assets/common/icons/checked.gif" Visible='<%# Bind("Won") %>' />
            </ItemTemplate>    
            <EditItemTemplate>
                <asp:CheckBox ID="chkWon" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Won") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCompetitor|Competitor">
            <ItemTemplate>
                <asp:Literal ID="ltlCompetitor" runat="server" ></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlCompetitor" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colThreat|Threat">
            <ItemTemplate>
                <asp:Literal ID="ltlThreat" runat="server" ></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlThreat" runat="server"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colNote|Note">    
            <ItemTemplate>
                <asp:Literal ID="ltlNote" runat="server" Text='<%# Bind("Note") %>' ></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtNote" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Note") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="center" />
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandName="edit" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" CommandName="cacnel" />
                &nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="update"
                     CommandArgument='<%# Bind("OpportunityCompetitionID") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="npadminbody" style="text-align:right">
    <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="New" OnClick="btnNew_Click" />&nbsp;
</div>