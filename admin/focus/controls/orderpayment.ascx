<%@ Control Language="c#" Inherits="netpoint.admin.focus.controls.orderpayment" Codebehind="orderpayment.ascx.cs" %>
<%@ Register Src="~/common/controls/PriceDisplay.ascx" TagName="PriceDisplay" TagPrefix="np" %>
<asp:Panel ID="pnlNew" runat="server">
    <table id="tblNew" style="border-collapse: collapse" cellspacing="0" cellpadding="2" width="100%" class="npbody" border="0">
        <tr>
            <td class="npsubheader" colspan="3">
                <asp:Literal ID="ltlChoosePaymentMethod" runat="server" Text="Choose A Payment Method :"></asp:Literal>
                <asp:DropDownList ID="ddlPaymentMethod" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="3"><asp:Literal ID="sysMessage" runat="server"></asp:Literal></td>
        </tr>        
        <tr>        
            <td><asp:Literal ID="ltlHolder" runat="server" Text="Card Holder"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtCardHolder" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgCardHolderWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Literal ID="sysCardHolder" runat="server" Visible="False"></asp:Literal></td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlAcctNum" runat="server" Text="Account Number"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtAccountNumber" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgAccountNumberWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Literal ID="sysAccountNumber" runat="server" Visible="False"></asp:Literal></td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlVerificationNumber" runat="server" Text="Verification Number"></asp:Literal></td>
            <td style="width:170;color:#cc0000;">
                <asp:TextBox ID="txtVerificationNumber" runat="server" Width="75px" MaxLength="6"></asp:TextBox>&nbsp;
                <asp:Image ID="imgVerificationWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/images/verifypoint.gif" /></td>
                        <td valign="top"><asp:Image ID="imgVerification" runat="server" ImageUrl="~/assets/common/images/verifynumber.gif" /></td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
            <td colspan="2">
                <asp:dropdownlist id="ddlCountry" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCountry_SelectedIndexChanged"></asp:dropdownlist>
                <asp:Literal ID="sysCountry" runat="server" Visible="false"></asp:Literal>
                <asp:ImageButton ID="imgCountryWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr>
            <td><asp:Literal ID="ltlStreet" runat="server" Text="Street"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtStreet" runat="server" Columns="45" MaxLength="150" Width="150px"></asp:TextBox>
                <asp:Literal ID="sysStreet" runat="server" Visible="false"></asp:Literal>
                <asp:ImageButton ID="imgStreetWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlCity" runat="server" Text="City"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtCity" runat="server" Width="150px" Columns="25" MaxLength="150"></asp:TextBox>
                <asp:Literal ID="sysCity" runat="server" Visible="false"></asp:Literal>
                <asp:ImageButton ID="imgCityWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>      
        <tr>
            <td><asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
            <td colspan="2">
                <asp:dropdownlist id="ddlState" runat="server"></asp:dropdownlist>
                <asp:Literal ID="sysState" runat="server" Visible="false"></asp:Literal>
                <asp:ImageButton ID="imgStateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlPostalCode" runat="server" Text="Postal Code"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtPostalCode" runat="server" Width="150px"></asp:TextBox>
                <asp:Literal ID="sysPostalCode" runat="server" Visible="false"></asp:Literal>
                <asp:ImageButton ID="imgPostalCodeWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr>
            <td>
                <asp:Literal ID="ltlEmail" runat="server" Text="Email" Visible="false"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Visible="false"></asp:TextBox>
                <asp:ImageButton ID="imgEmailWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltlPhone" runat="server" Text="Phone" Visible="false"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Visible="false"></asp:TextBox>
                <asp:ImageButton ID="imgPhoneWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
            </td>
        </tr>        
        <tr>
            <td>
                <asp:Literal ID="ltlIssueDate" runat="server" Text="Issue Date"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlIssueMonth" runat="server">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
                <asp:Literal ID="sysIssueMonth" runat="server" Visible="False"></asp:Literal>
                <asp:Literal ID="sysIssueSlash" runat="server" Text="/" Visible="False"></asp:Literal>
                <asp:DropDownList ID="ddlIssueYear" runat="server"></asp:DropDownList>
                <asp:Literal ID="sysIssueYear" runat="server" Visible="False"></asp:Literal>
                <asp:Image ID="imgIssueDateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>
        <tr>
            <td><asp:Literal ID="ltlIssueNumber" runat="server" Text="Issue Number"></asp:Literal></td>
            <td colspan="2">
                <asp:TextBox ID="txtIssueNumber" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="imgIssueNumberWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" />
                <asp:Literal ID="sysIssueNumber" runat="server" Visible="False"></asp:Literal>
            </td>
        </tr>        
        <tr>
            <td>
                <asp:Literal ID="ltlExpirationDate" runat="server" Text="Expiration Date"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="ddlMonth" runat="server">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
                <asp:Literal ID="sysMonth" runat="server" Visible="False"></asp:Literal>
                <asp:Literal ID="sysSlash" runat="server" Text="/" Visible="False"></asp:Literal>
                <asp:DropDownList ID="ddlYear" runat="server"></asp:DropDownList>
                <asp:Literal ID="sysYear" runat="server" Visible="False"></asp:Literal>
                <asp:Image ID="imgExpirationDateWarning" runat="server" Visible="False" ImageUrl="~/assets/common/icons/warning.gif" /></td>
        </tr>       
        <tr>
            <td colspan="3">
                <asp:Literal ID="hdnYouWillBeCharged" Text="Additional charge for this payment method:" runat="server" Visible="False"></asp:Literal>
                <np:PriceDisplay ID="prcPaymentCharge" runat="server" />
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<table id="tblOrder" cellspacing="0" cellpadding="2" width="100%" border="0" class="npbody">
    <tr><td class="npsubheader" style="width:50%;"><asp:Literal ID="ltlOrderTotals" runat="server" Text="Order Totals"></asp:Literal></td>
        <td class="npsubheader" style="width:50%;"><asp:Literal ID="ltlPayments" runat="server" Text="Payments"></asp:Literal></td>
    </tr>
    <tr>
        <td valign="top">
            <table id="Table1" cellspacing="0" cellpadding="2" border="0" class="npbody">
                <tr>
                    <td><b><asp:Literal ID="ltlOrderTotal" runat="server" Text="Order Total:"></asp:Literal></b></td>
                    <td align="right"><np:PriceDisplay ID="sysGrandTotal" runat="server" /></td>
                </tr>
                <tr>
                    <td><asp:Literal ID="ltlPay" runat="server" Text="Payments:"></asp:Literal></td>
                    <td align="right"><np:PriceDisplay ID="sysPaymentTotal" runat="server" /></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <asp:GridView ID="grid" runat="server" AutoGenerateColumns="false" Width="100%"
                GridLines="None" CellPadding="3" BorderStyle="None" ShowHeader="False" 
                DataKeyNames="OrderPaymentID"
                OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting">
                <RowStyle CssClass="npbody"></RowStyle>
                <HeaderStyle CssClass="npsubheader"></HeaderStyle>
                <Columns>
                    <asp:BoundField DataField="PaymentName" HeaderText="colCurrentPayments|Current Payments"></asp:BoundField>
                    <asp:BoundField DataField="MaskedNumber" HeaderText="Customer|Customer"></asp:BoundField>
                    <asp:TemplateField HeaderText="colAmount|Amount">
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcAmount" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "PaymentAmount") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colRemove|Remove">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/buttons/remove.gif"
                                CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderPaymentID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
