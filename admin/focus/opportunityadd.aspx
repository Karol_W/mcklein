<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" Codebehind="opportunityadd.aspx.cs" Inherits="netpoint.admin.focus.opportunityadd" %>
<%@ Register TagPrefix="np" TagName="OppGeneral" Src="~/admin/focus/controls/opportunitygeneral.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save and Continue" OnClick="btnSaveAll_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminsubheader">
        <asp:literal ID="txtAdd" runat="server" Text="Add an Opportunity"></asp:literal>
    </div>
    <div class="npadminbody">
        <np:OppGeneral ID="opp" runat="server" />        
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
