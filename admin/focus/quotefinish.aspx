<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" CodeBehind="quotefinish.aspx.cs" Inherits="netpoint.admin.focus.quotefinish" %>
<%@ Register Src="controls/orderverify.ascx" TagName="orderverify" TagPrefix="np" %>
<%@ Register Src="controls/orderopportunity.ascx" TagName="orderopportunity" TagPrefix="np" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:Label runat="server" ID="lblOrderID"></asp:Label>
                &nbsp;
                <asp:ImageButton ID="btnLock" runat="server" ImageUrl="~/assets/common/buttons/lock.gif"
                    ToolTip="Lock" OnClick="btnLock_Click"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
                <np:orderopportunity ID="orderopportunity" runat="server"></np:orderopportunity>
                <np:orderverify ID="orderverify" runat="server"></np:orderverify>
    <asp:Label ID="sysErrorMessage" runat="server" CssClass="npwarning"></asp:Label>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
        <asp:Literal ID="errTotalPaymentsMustMatch" runat="server" Text="Total Payments must match the Total of the Order"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errSelectSaveBillAddress" runat="server" Text="Please Select or Save a Bill To Address"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errSelectSaveShipAddress" runat="server" Text="Please Select or Save a Delivery Address"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNotAuthorized" runat="server" Text="Credit Card Not Authorized"
        Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNotCharged" runat="server" Text="Credit Card Not Charged"
        Visible="False"></asp:Literal>
</asp:Content>