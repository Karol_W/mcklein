<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.commerce.OpportunityDetail" Codebehind="OpportunityDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="AccountPicker" Src="~/admin/common/controls/AccountPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="NPStages" Src="~/admin/focus/controls/opportunstages.ascx" %>
<%@ Register TagPrefix="np" TagName="OpportunityCompetition" Src="~/admin/focus/controls/opportunitycompetition.ascx" %>
<%@ Register TagPrefix="np" TagName="OpportunityReason" Src="~/admin/focus/controls/opportunityreasons.ascx" %>
<%@ Register TagPrefix="np" TagName="OpportunityGeneral" Src="~/admin/focus/controls/opportunitygeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="OpportunityInterest" Src="~/admin/focus/controls/opportunityinterest.ascx" %>
<%@ Register TagPrefix="np" TagName="activity" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:Panel ID="pnlControls" runat="server">
                    <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                        ToolTip="Delete" OnClick="btnDelete_Click" />&nbsp;
                    <asp:ImageButton ID="lnkActivity" runat="server" ToolTip="Create Activity" ImageUrl="~/assets/common/icons/event.gif"
                        OnClick="lnkActivity_Click" />&nbsp;
                    <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                        ToolTip="Save All and Close" OnClick="btnSaveAll_Click"></asp:ImageButton>&nbsp;
                </asp:Panel>
            </td>                    
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="OpportunityTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="OpportunityMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsStages" Text="Stages"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsActivities" Text="Activities"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsInterests" Text="Interests" />
                <ComponentArt:TabStripTab runat="server" ID="tsCompetiton" Text="Competition"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsReasone" Text="Reason"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="OpportunityMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <np:OpportunityGeneral ID="general" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvStages">
                <np:NPStages ID="stages" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvActivities">
                <np:activity ID="activities" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvInterests">
                <np:OpportunityInterest ID="interests" runat="server" />
            </ComponentArt:PageView>            
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCompetition">
                <np:OpportunityCompetition ID="competition" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvReason">
                <np:OpportunityReason ID="lostreason" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnEvent" Text="Event" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnCall" Text="Call" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAppointment" Text="Meeting" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnNote" Text="Note" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnTask" Text="Task" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnHoliday" Text="Holiday" runat="server" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnOpen" runat="server" Text="open" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnWon" runat="server" Text="won" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnLost" runat="server" Text="lost" Visible="False"></asp:Literal>
    <asp:Literal ID="errDeletionsPermanent" runat="server" Text="Deletions are permanent.  Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnOpportunity" runat="server" Text="Opportunity: " Visible="false"></asp:Literal>
</asp:Content>
