<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.dashboard.Calendar" Codebehind="Calendar.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DashboardTab" Src="controls/DashboardTab.ascx" %>
<%@ Register TagPrefix="np" TagName="weekview" Src="controls/weekview.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Activity Calendar</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <np:DashboardTab ID="tab" runat="server"></np:DashboardTab>
    <np:weekview ID="weekview" runat="server" />
</asp:Content>
