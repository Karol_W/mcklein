<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" CodeBehind="orderadd.aspx.cs" Inherits="netpoint.admin.focus.orderadd" %>
<%@ Register Src="controls/ordercustomer.ascx" TagName="ordercustomer" TagPrefix="np" %>
<%@ Register Src="controls/ordercart.ascx" TagName="ordercart" TagPrefix="np" %>
<%@ Register Src="controls/ordernotes.ascx" TagName="ordernotes" TagPrefix="np" %>
<%@ Register Src="controls/orderaddresses.ascx" TagName="orderaddresses" TagPrefix="np" %>
<%@ Register Src="controls/ordershippingmethod.ascx" TagName="ordershippingmethod" TagPrefix="np" %>
<%@ Register Src="controls/orderbillingmethod.ascx" TagName="orderbillingmethod" TagPrefix="np" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysErrorMessage" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:Label runat="server" ID="sysOrderID"></asp:Label>&nbsp;
                <asp:ImageButton ID="btnDelete" runat="server" Visible="false" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete Order" OnClick="btnDelete_Click" />&nbsp;
                <asp:ImageButton ID="btnSave" runat="server" Visible="true" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save Order" OnClick="btnSave_Click" />&nbsp;
                <asp:ImageButton ID="btnOrder" runat="server" Visible="false" ImageUrl="~/assets/common/buttons/continuetocheckout.gif" ToolTip="Order" OnClick="btnOrder_Click" />&nbsp;
            </td>
        </tr>
    </table>
    <div id="divTabStrip" runat="server">
         <ComponentArt:TabStrip ID="OrderTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="OrderMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0" >
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" Enabled="true" ID="tsCustomer" Text="Customer" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsCart" Text="Cart" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsAddresses" Text="Addresses" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsShippingMethod" Text="Shipping Method" />
                <ComponentArt:TabStripTab runat="server" Enabled="false" ID="tsBillingMethod" Text="Billing Method" />
            </Tabs>
        </ComponentArt:TabStrip>
    </div>
    <div id="divNewOrder" runat="server" visible="false" style="width:100%;" class="npadminsubheader">
        <asp:Literal ID="ltlNewOrder" runat="server" Text="New Order"></asp:Literal>
    </div>
        <ComponentArt:MultiPage ID="OrderMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCustomer">
                <np:ordercustomer ID="ordercustomer" runat="server"></np:ordercustomer>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCart">
                <np:ordercart ID="ordercart" runat="server"></np:ordercart>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvAddresses">
                <np:orderaddresses ID="orderaddresses" runat="server"></np:orderaddresses>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvShippingMethod">
                <np:ordershippingmethod ID="ordershippingmethod" runat="server"></np:ordershippingmethod>
                <np:ordernotes ID="ordernotes" runat="server"></np:ordernotes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvBillingMethod">
                <np:orderbillingmethod ID="orderbillingmethod" runat="server"></np:orderbillingmethod>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    <br />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">   
    <asp:Literal ID="errSelectSaveBillAddress" runat="server" Text="Please Select or Save a Bill To Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errSelectSaveShipAddress" runat="server" Text="Please Select or Save a Delivery Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNumberInvalid" runat="server" Text="Credit Card number or Verification number is invalid." Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardNotCharged" runat="server" Text="Credit Card Not Charged" Visible="False"></asp:Literal>
    <asp:Literal ID="errNoItemsInCart" runat="server" Text="No Items in cart. Add an item." Visible="False"></asp:Literal>
    <asp:Literal ID="errNoPayment" runat="server" Text="No payment has been selected." Visible="false"></asp:Literal>
    <asp:Literal id="errNoWalletName" runat="server" Text="Please name the wallet." Visible="false"></asp:Literal>
    <asp:Literal id="errBadCardHolder" runat="server" text="Card holder name is required." visible="false"></asp:Literal>
    <asp:Literal ID="errBadCVV" runat="server" Text="Card validation number is required." Visible="false"></asp:Literal>
    <asp:Literal ID="errCardExpired" runat="server" Text="The credit card has passed its expiration date." Visible="false"></asp:Literal>
    <asp:Literal ID="errCreditCardMissingStreet" runat="server" Text="Credit card street is required." Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardMissingCity" runat="server" Text="Credit card city is required." Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardMissingPostalCode" runat="server" Text="Credit card postal code is required." Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardBadIssueDate" runat="server" Text="Credit card issue date is incorrect." Visible="False"></asp:Literal>
    <asp:Literal ID="errCreditCardMissingIssueNumber" runat="server" Text="Credit card issue number is required." Visible="False"></asp:Literal>       
    <asp:Literal ID="errDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="errDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
    <asp:Literal ID="hdnEmailIsRequired" runat="server" Text="Email address is required."></asp:Literal>
    <asp:Literal ID="hdnPhoneIsRequired" runat="server" Text="Phone number is required."></asp:Literal>
    <asp:Literal ID="hdnFirstLastNameRequired" runat="server" Text="Both first and last name are required for the card holder."></asp:Literal>
</asp:Content>