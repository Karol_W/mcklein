<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" ValidateRequest="false" Inherits="netpoint.dashboard.service" CodeBehind="service.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="DashboardTab" Src="controls/DashboardTab.ascx" %>
<%@ Register TagPrefix="np" TagName="summary" Src="controls/summary.ascx" %>
<%@ Register TagPrefix="np" TagName="queue" Src="controls/queue.ascx" %>
<%@ Register TagPrefix="np" TagName="tickets" Src="controls/tickets.ascx" %>
<%@ Register TagPrefix="np" TagName="logs" Src="~/admin/prospects/controls/logs.ascx" %>
<%@ Register TagPrefix="np" TagName="activity" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="np" TagName="customers" Src="controls/customers.ascx" %>
<%@ Register TagPrefix="np" TagName="reports" Src="controls/reports.ascx" %>
<%@ Register TagPrefix="np" TagName="projects" Src="controls/projects.ascx" %>
<%@ Register TagPrefix="np" TagName="workorders" Src="controls/workorders.ascx" %>
<%@ Register TagPrefix="np" TagName="tasks" Src="~/admin/support/controls/tasks.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Service View</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <np:DashboardTab ID="tab" runat="server"></np:DashboardTab>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ServiceTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ServiceMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsOverview" Text="Overview" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsQueue" Text="Queue"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsCustomers" Text="Customers"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsSummary" Text="Summary"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsreports" Text="Reports"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsActivity" Text="Activity" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsEvents" Text="Events"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsProjects" Text="Projects"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsTickets" Text="Tickets"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsTasks" Text="Tasks"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsWorkOrder" Text="Service Calls"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsLogs" Text="Logs"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ServiceMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server" ID="pvQueue">
                <np:queue ID="queue" runat="server"></np:queue>
                <br />
                <np:tickets ID="ticketsqueue" runat="server"></np:tickets>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvCustomers">
                <np:customers ID="customers" runat="server"></np:customers>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:summary ID="summary" runat="server"></np:summary>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:reports ID="reports" runat="server"></np:reports>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:activity ID="activity" runat="server"></np:activity>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:projects ID="projects" runat="server"></np:projects>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:tickets ID="tickets" runat="server"></np:tickets>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:tasks ID="tasks" runat="server"></np:tasks>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:workorders ID="workorders" runat="server"></np:workorders>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:logs ID="logs" runat="server"></np:logs>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
