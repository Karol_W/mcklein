<%@ Page Language="c#" Inherits="netpoint.admin.focus.QuoteDetail" Codebehind="QuoteDetail.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
    </head>
    <body>
        <form id="Form1" runat="server" enctype="multipart/form-data">
            <table id="Table1" class="npbody" cellspacing="0" cellpadding="1" width="100%" border="0">
                <tr>
                    <td>
                        <asp:literal id="ltlQuote" runat="server" Text="Quote"></asp:literal>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br /><br />
                        <asp:button id="btnPromote" runat="server" text="Promote to Order" onclick="btnPromote_Click"></asp:button>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:literal id="sysSuccess" runat="server"></asp:literal>
                    </td>
                </tr>
            </table>
            <asp:Literal ID="hdnSuccess" runat="server" Text="Quote was successfully converted to order"
                Visible="false"></asp:Literal>
        </form>
    </body>        
</html>
<asp:literal id="hdnRestricted" runat="server" text="This data is restricted to the owner of the quote." visible="false"></asp:literal>
<asp:literal id="hdnBasedOrder" runat="server" text="Based On zed eCommerce Quote #{0}." visible="false"></asp:literal>
