<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.CampaignReport" Codebehind="CampaignReport.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table width="100%">
         <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:HyperLink runat="server" ID="lnkBack" ImageUrl="~/assets/common/icons/cancel.gif" NavigateUrl="~/admin/prospects/Campaigns.aspx" ToolTip="Back" />
                <asp:Label runat="server" ID="ltlCampaignReportsHeader" Text="Campaign Reports" /></td>
        </tr>
        <tr>
            <td align="center">
                <table cellspacing="0" cellpadding="5" border="0" class="npadminbody">
                    <tr>
                        <td><asp:Literal runat="server" ID="ltlSendStart" Text="Send Start" /></td>
                        <td><asp:Label runat="server" ID="sysSendStart" /></td>
                    </tr>
                    <tr>
                        <td><asp:Literal runat="server" ID="ltlSendEnd" Text="Send End" /></td>
                        <td><asp:Label runat="server" ID="sysSendEnd" /></td>
                    </tr>
                </table>
                <br />
                <table cellspacing="0" cellpadding="5" border="1" class="npadminbody">
                    <tr>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlAction" Text="Action" /></td>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlUnique" Text="Quantity" /></td>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlPercentage" Text="Percentage Overall" /></td>
                        <td class="npadminlabel"><asp:Literal runat="server" ID="ltlResponse" Text="Percentage of Target Response" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" align="right">Sends</td>
                        <td style="width:100px;" align="right"><asp:HyperLink runat="server" ID="sysSends" Text="0" /></td>
                        <td style="width:100px;" align="right"><asp:Label runat="server" ID="sysSendsPercent" /></td>
                        <td style="width:100px;" align="right">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" align="right">Opens</td>
                        <td style="width:100px;" align="right"><asp:HyperLink runat="server" ID="sysOpens" Text="0" /></td>
                        <td style="width:100px;" align="right"><asp:Label runat="server" ID="sysOpensPercent" /></td>
                        <td style="width:100px;" align="right">&nbsp;</td>
                    </tr>                    
                    <tr>
                        <td class="npadminlabel" align="right">Clicks</td>
                        <td style="width:100px;" align="right"><asp:HyperLink runat="server" ID="sysClicks" Text="0" /></td>
                        <td style="width:100px;" align="right"><asp:Label runat="server" ID="sysClicksPercent" /></td>
                        <td style="width:100px;" align="right"><asp:Label runat="server" ID="sysClicksTarget" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" align="right">Bounces</td>
                        <td style="width:100px;" align="right"><asp:HyperLink runat="server" ID="sysBounces" Text="0" /></td>
                        <td style="width:100px;" align="right"><asp:Label runat="server" ID="sysBouncesPercent" /></td>
                        <td style="width:100px;" align="right">&nbsp;</td>
                    </tr>
                </table>
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
