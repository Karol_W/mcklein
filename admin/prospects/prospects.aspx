<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.prospects.prospects" Codebehind="prospects.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Src="~/common/controls/npdatepicker.ascx" TagName="DatePicker" TagPrefix="np" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Prospects*</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif"
                    NavigateUrl="~/admin/prospects/prospectnew.aspx" ToolTip="Add Prospect" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter"
                    OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or edit a filter set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right">&nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults"
                    OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" Visible="false"/></td>
            <td class="npadminsubheader" colspan="1" align="right">
                <asp:Literal ID="ltlMaxRows" runat="server" Text="Maximum number of prospects to return" />
                <asp:DropDownList runat="Server" ID="ddlMaxRows"></asp:DropDownList>&nbsp;
                <asp:ImageButton runat="server" ID="btnFind" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnFind_Click" ToolTip="Find" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:Panel runat="Server" ID="pnlResults" Visible="false">
                    <asp:GridView ID="gvResults" runat="server" PageSize="50" CssClass="npadmintable"
                        AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                        OnPageIndexChanging="gvResults_PageIndexChanging" OnSorting="gvResults_Sorting"
                        EmptyDataText="No Prospects Found in Search Set">
                        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                        <HeaderStyle CssClass="npadminsubheader" />
                        <RowStyle CssClass="npadminbody" />
                        <AlternatingRowStyle CssClass="npadminbodyalt" />
                        <Columns>
                            <asp:HyperLinkField DataTextField="ProspectID" DataNavigateUrlFields="ProspectID"
                                DataNavigateUrlFormatString="~/admin/prospects/prospectdetail.aspx?ProspectID={0}"
                                HeaderText="colID|ID" SortExpression="ProspectID">
                                <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="25%" />
                            </asp:HyperLinkField>
                            <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderText="colFirstName|First Name" />
                            <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderText="colLastName|Last Name" />
                            <asp:BoundField DataField="CompanyName" SortExpression="CompanyName" HeaderText="colCompanyName|Company Name" />
                            <asp:BoundField DataField="PhoneCompanyMain" SortExpression="PhoneCompanyMain" HeaderText="colPhoneCompanyMain|Phone Company Main" />
                        </Columns>
                        <PagerSettings Position="TopAndBottom" Mode="Numeric" />
                        <PagerStyle HorizontalAlign="Right"></PagerStyle>
                    </asp:GridView>
                    <br />
                    <div class="npadminsubheader">
                        <asp:Image runat="Server" ID="imgIndicator" ImageUrl="~/assets/common/icons/indicator.gif" />
                        <asp:Label runat="server" ID="lblBulkOperations" Text="Bulk Operations"></asp:Label>
                    </div>
                    <ComponentArt:TabStrip ID="ActionsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
                        MultiPageId="ActionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
                        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
                        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
                        <ItemLooks>
                            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
                        </ItemLooks>
                        <Tabs>
                            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="Sales People"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Lists"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Status"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Tracking Values"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Followup"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts6" Text="Delete"></ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                    <ComponentArt:MultiPage ID="ActionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
                        Width="100%" Height="100">
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSalesPerson" Text="Assign all records returned to this salesperson: "></asp:Label>
                            <asp:DropDownList ID="ddlAssignSalesPersonID" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnAssignSalesPerson" runat="server" Text="Submit" OnClick="btnAssignSalesPerson_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSubscribe" Text="Subscribe all records returned to this user list: "></asp:Label>
                            <asp:DropDownList ID="ddlSubscribeContactList" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnSubscribeContactList" runat="server" Text="Submit" OnClick="btnSubscribeContactList_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblStatus" Text="Assign all records returned the following status: "></asp:Label>
                            <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnUpdateStatus" runat="server" Text="Submit" OnClick="btnUpdateStatus_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSetType" Text="On all records set "></asp:Label>
                            <asp:DropDownList ID="ddlProspectSetType" runat="server"></asp:DropDownList>
                            <asp:Label runat="server" ID="lblSetTypeTo" Text=" to the value: "></asp:Label>
                            <asp:TextBox runat="server" ID="txtSetValue" Columns="8"></asp:TextBox>
                            <asp:Button ID="btnSetValue" runat="server" Text="Submit" OnClick="btnSetValue_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblFollowUp" Text="Assign all records returned to be followed up on: "></asp:Label>
                            <np:DatePicker runat="server" ID="dpFollowUp" />
                            <asp:Button ID="btnSetFollowup" runat="server" Text="Submit" OnClick="btnSetFollowup_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblDelete" Text="Delete all records returned from prospects. "></asp:Label>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"></asp:Button>
                        </ComponentArt:PageView>
                    </ComponentArt:MultiPage>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
    <asp:Literal runat="server" ID="errWarning" Text="The bulk operation you are about to do\n will affect ALL RECORDS shown in your search set.\nContinue?"></asp:Literal>
</asp:Content>
