<%@ Page Language="c#" MasterPageFile="~/masters/default.master" Inherits="netpoint.admin.prospects.controls.label"
    Codebehind="label.aspx.cs" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">
    <script language="VBScript" type="text/vbscript">
		Sub Btn1_onclick()
			Dim DymoAddIn, DymoLabel
			Set DymoAddIn = CreateObject("DYMO.DymoAddIn")
			Set DymoLabel = CreateObject("DYMO.DymoLabels")
			DymoAddIn.Open "C:\Program Files\Dymo Label\Label Files\Address (30252, 30320).LWL"
			DymoLabel.SetAddress 1,"<%=addressstring%>"
			DymoAddIn.Print 1, TRUE
		End Sub
    </script>

    <asp:Literal runat="server" ID="ltlAddress" Text="Address" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <input type="button" name="Btn1" value="Dymo Label" />
    <asp:Button runat="server" ID="btnRecordPrint" Text="Print" OnClick="btnRecordPrint_Click" />
    <br />
    <asp:Literal runat="server" ID="hdnLabelPrintMessage" Visible="False" Text="Label Printed" />
</asp:Content>
