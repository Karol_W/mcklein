<%@ Control Language="C#" AutoEventWireup="true" Codebehind="duplicatesusers.ascx.cs" Inherits="netpoint.admin.prospects.controls.duplicatesusers" %>
<asp:GridView 
    ID="gridUser" 
    runat="server" 
    CssClass="npadmintable" 
    AutoGenerateColumns="False"
    AllowSorting="False" 
    AllowPaging="True" 
    EmptyDataText="No Matching Users" 
    PageSize="50"
    OnRowDataBound="gridUser_RowDataBound" 
    OnPageIndexChanging="gridUser_PageIndexChanging">
    <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
    <RowStyle CssClass="npadminbody"></RowStyle>
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:BoundField HeaderText="colUserID|User ID" DataField="UserID">
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="colUserDuplicate|User Duplicate">
            <ItemTemplate>
                <asp:HyperLink ID="lnkUser" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DupValue") %>'></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="Bottom" Mode="numeric" />
    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:RadioButtonList ID="sysUser" runat="server" CssClass="npadminbody" AutoPostBack="True"
        RepeatDirection="Horizontal" OnSelectedIndexChanged="rblUser_SelectedIndexChanged">
    </asp:RadioButtonList>&nbsp;
    <asp:ImageButton ID="btnDeleteUserDupes" runat="server" ImageUrl="~/assets/common/icons/delete.gif" Width="17" ToolTip="Delete Matching Prospects" />
</div>
<asp:Literal ID="errDelete" runat="server" Text="This will delete all shown duplicates, keeping newest record only.\nDeletions are Permanent. Are you sure?" Visible="false"></asp:Literal>
<asp:Literal ID="hdnName" runat="server" Text="Name" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCompany" runat="server" Text="Company" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEmail" runat="server" Text="Email" Visible="False"></asp:Literal>
<asp:Literal ID="hdnHomePhone" runat="server" Text="Home Phone" Visible="False"></asp:Literal>
<asp:Literal ID="hdnTrackingNo" runat="server" Text="Tracking No" Visible="False"></asp:Literal>
<asp:Literal ID="hdnOfficePhone" runat="server" Text="Office Phone" Visible="False"></asp:Literal>
<asp:Literal ID="errDupes" runat="server" Text="All matches from the prospect list will be deleted.  Continue?" Visible="False"></asp:Literal>