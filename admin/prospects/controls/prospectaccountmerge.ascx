<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="prospectaccountmerge.ascx.cs" Inherits="netpoint.admin.prospects.controls.prospectaccountmerge" %>

<asp:Label ID="sysErrTop" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable">
        <tr>
            <td class="npadminlabel" align="center" colspan="2">
                <b>
                    <asp:Literal ID="ltlProspectData" runat="server" Text="Prospect Data"></asp:Literal></b></td>
            <td class="npadminlabel" align="center">
                <b>
                    <asp:Literal ID="ltlOverride" runat="server" Text="Use Prospect Data"></asp:Literal></b></td>
            <td class="npadminlabel" align="center" colspan="2">
                <b>
                    <asp:Literal ID="ltlAccountData" runat="server" Text="Account Data"></asp:Literal></b></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlCompanyName" runat="server" Text="Company Name"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox><asp:Image ID="imgCompanyName"
                    runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false"></asp:Image></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyAccountName" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtAccountName" runat="server"></asp:TextBox><asp:Image ID="imgAccountName"
                    runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountName" runat="server" Text="Account Name"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectEmail" runat="server" Text="Email"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtProspectEmail" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyEmail" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountEmail" runat="server" Text="Email"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectIndustry" runat="server" Text="Industry"></asp:Literal></td>
            <td align="center">
                <asp:Label ID="sysProspectIndustry" runat="server" CssClass="nphighlight" Visible="False"></asp:Label><br /><asp:DropDownList
                    ID="ddlProspectIndustry" runat="server">
                </asp:DropDownList></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyIndustry" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:DropDownList ID="ddlIndustry" runat="server">
                </asp:DropDownList></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountIndustry" runat="server" Text="Industry"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlCompanyPhone" runat="server" Text="Company Phone"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtCompanyPhone" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyPhone1" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtPhone1" runat="server"></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlPhone1" runat="server" Text="Phone1"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectFax" runat="server" Text="Fax"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtProspectFax" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyFax" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtFax" runat="server"></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountFax" runat="server" Text="Fax"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectMobile" runat="server" Text="Mobile"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtProspectMobile" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyMobile" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountMobile" runat="server" Text="Mobile"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectWebsite" runat="server" Text="Website"></asp:Literal></td>
            <td align="center">
                <asp:TextBox ID="txtProspectWebsite" runat="server"></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:CheckBox ID="sysCopyWebsite" runat="server"></asp:CheckBox></td>
            <td class="npadminbodyalt" align="center">
                <asp:TextBox ID="txtWebsite" runat="server"></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountWebsite" runat="server" Text="Website"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel" align="right">
                <asp:Literal ID="ltlProspectNotes" runat="server" Text="Notes"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtProspectNotes" runat="server" Rows="7" Columns="15" TextMode="MultiLine"
                   ></asp:TextBox></td>
            <td class="npadminlabel" align="center">
                <asp:RadioButtonList ID="rdoNote" runat="server" CssClass="npadminlabel">
                    <asp:ListItem Value="ignore" Selected="True">Ignore Note</asp:ListItem>
                    <asp:ListItem Value="overwrite">Overwrite Note</asp:ListItem>
                    <asp:ListItem Value="append">Append Note</asp:ListItem>
                </asp:RadioButtonList></td>
            <td class="npadminbodyalt">
                <asp:TextBox ID="txtNotes" runat="server" Rows="7" Columns="15" TextMode="MultiLine"
                    ></asp:TextBox></td>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAccountNotes" runat="server" Text="Notes"></asp:Literal></td>
        </tr>
    </table>
    <asp:Literal ID="errBlankAccountName" runat="server" Visible="false" Text="Business Partner cannot be blank when not using Prospect Company Name"></asp:Literal>
    <asp:Literal ID="errBlankCompanyName" runat="server" Visible="false" Text="Prospect Company Name cannot be blank when using for Business Partner Name"></asp:Literal>