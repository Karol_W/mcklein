<%@ Control Language="c#" Inherits="netpoint.admin.prospects.controls.logs" Codebehind="logs.ascx.cs" %>
<asp:GridView ID="gridLogs" runat="server" AllowPaging="True"
    PageSize="20" CssClass="npadmintable" AutoGenerateColumns="False" 
    OnRowDataBound="gridLogs_RowDataBound" OnPageIndexChanging="gridLogs_PageIndexChanging"
    EmptyDataText="No Records Found">
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:TemplateField HeaderText="colType|Action" SortExpression="LogType">
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
            <ItemTemplate>
                <asp:Label runat="server" ID="lblType"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="colContact|Email" DataField="Email" SortExpression="Email" />
        <asp:TemplateField HeaderText="colMessage|Link" SortExpression="Subject" Visible="false">
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
            <ItemTemplate>
                <b><asp:Label runat="server" ID="lblLogTopic"></asp:Label></b>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDate|Date" SortExpression="TimeStamp">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TimeStamp") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle Width="25%" />
            <ItemTemplate>
                <asp:Label ID="lblTimeStamp" runat="server" Text='<%# Bind("TimeStamp", "{0:yyyy-MMM-dd hh:mm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle CssClass="npadminbody" />
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:DropDownList runat="server" ID="ddlLogType" AutoPostBack="True" OnSelectedIndexChanged="ddlLogType_SelectedIndexChanged" />&nbsp;
    <asp:DropDownList runat="server" ID="ddlCampaigns" AutoPostBack="True" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged" />&nbsp;           
</div>
