<%@ Control Language="C#" AutoEventWireup="true" Codebehind="prospectusermerge.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.prospectusermerge" %>
    
<asp:Label ID="sysErrTop" runat="server" CssClass="npwarning"></asp:Label>
<table class="npadmintable">
    <tr>
        <td class="npadminbodyalt" align="center" colspan="5">
            <asp:RadioButtonList ID="rdoUser" runat="server" CssClass="npadminbodyalt" RepeatDirection="Horizontal">
                <asp:ListItem Value="new" Selected="True">New</asp:ListItem>
                <asp:ListItem Value="ignore">Ignore</asp:ListItem>
                <asp:ListItem Value="merge">Merge</asp:ListItem>
            </asp:RadioButtonList><asp:Label ID="sysUserErr" runat="server" CssClass="npwarning"></asp:Label></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="center" colspan="2">
            <b>
                <asp:Literal ID="ltlProspectUserData" runat="server" Text="Prospect Data"></asp:Literal></b></td>
        <td class="npadminlabel" align="center">
            <b>
                <asp:Literal ID="ltlUseProsData" runat="server" Text="Use Prospect Data"></asp:Literal></b></td>
        <td class="npadminlabel" align="center" colspan="2">
            <b>
                <asp:Literal ID="ltlUserData1" runat="server" Text="User Data"></asp:Literal></b></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectUserID" runat="server" Text="UserID"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox><asp:Literal ID="sysProspectUserID"
                runat="server"></asp:Literal><asp:Image ID="imgUserID" runat="server" ImageUrl="~/assets/common/icons/warning.gif"
                    Visible="false"></asp:Image></td>
        <td class="npadminlabel" align="center">
        </td>
        <td class="npadminbodyalt" align="center">
            <asp:DropDownList ID="ddlUser" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountUserID" runat="server" Text="UserID"></asp:Literal></td>
    </tr>    
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectTitle" runat="server" Text="Title"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectTitle" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyTitle" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountTitle" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountTitle" runat="server" Text="Title"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectFirstName" runat="server" Text="First Name"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectFirstName" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyFirstName" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountFirstName" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountFirstName" runat="server" Text="First Name"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectMiddleName" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyMiddleName" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountMiddleName" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectLastName" runat="server" Text="Last Name"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectLastName" runat="server"></asp:TextBox><asp:Image ID="imgProspectLastName"
                runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyLastName" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountLastName" runat="server"></asp:TextBox><asp:Image ID="imgAccountLastName"
                runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountLastName" runat="server" Text="Last Name"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectSuffix" runat="server" Text="Suffix"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectSuffix" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopySuffix" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountSuffix" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountSuffix" runat="server" Text="Suffix"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectJobTitle" runat="server" Text="Job Title"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectJobTitle" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyJobTitle" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountJobTitle" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountJobTitle" runat="server" Text="Job Title"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectUserEmail" runat="server" Text="Email"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectUserEmail" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyUserEmail" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountUserEmail" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountUserEmail" runat="server" Text="Email"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlOfficePhone" runat="server" Text="Office Phone"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtOfficePhone" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyDayPhone" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtDayPhone" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDayPhone" runat="server" Text="Day Phone"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlHomePhone" runat="server" Text="Home Phone"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtPhoneHome" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyEveningPhone" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtEveningPhone" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlEveningPhone" runat="server" Text="Evening Phone"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectMobilePhone" runat="server" Text="Mobile Phone"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectMobilePhone" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyMobilePhone" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtUserMobilePhone" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMobilePhone" runat="server" Text="Mobile Phone"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlTrackingNo" runat="server" Text="Tracking No."></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtTrackingNo" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyMemberID" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtMemberID" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMemberID" runat="server" Text="Member ID"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectUserNotes" runat="server" Text="Notes"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtProspectUserNotes" runat="server" Rows="7" Columns="15" TextMode="MultiLine"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <br />
            <asp:RadioButtonList ID="rdoUserNotes" runat="server" CssClass="npadminlabel">
                <asp:ListItem Value="ignore" Selected="True">Ignore Note</asp:ListItem>
                <asp:ListItem Value="overwrite">Overwrite Note</asp:ListItem>
                <asp:ListItem Value="append">Append Note</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td class="npadminbodyalt">
            <asp:TextBox ID="txtAccountUserNotes" runat="server" Rows="7" Columns="15" TextMode="MultiLine"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountUserNotes" runat="server" Text="Notes"></asp:Literal></td>
    </tr>
</table>
<asp:Literal ID="errNoUserToMerge" runat="server" Visible="False" Text="No user has been selected to merge with."></asp:Literal>
   <asp:Literal ID="hdnUserProspect" runat="server" Text="User created from prospect." Visible="False"></asp:Literal>
   <asp:Literal ID="errUserExists" runat="server" Visible="False" Text="This UserID already exists.  Choose another."></asp:Literal>
   <asp:Label ID="errBlankUserID" runat="server" CssClass="npwarning" Text="Prospect UserID cannot be blank" Visible="false"></asp:Label>
   <asp:Label ID="errBlankPassword" runat="server" CssClass="npwarning" Text="Prospect Password cannot be blank" Visible="false"></asp:Label>
   <asp:Label ID="errBlankProspectLastName" runat="server" CssClass="npwarning" Text="Prospect Last Name cannot be blank" Visible="false"></asp:Label>
   <asp:Label ID="errBlankAccountLastName" runat="server" CssClass="npwarning" Text="Business Partner Contact last name cannot be blank" Visible="false"></asp:Label>   
 