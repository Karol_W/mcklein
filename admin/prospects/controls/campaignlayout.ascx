<%@ Control Language="C#" AutoEventWireup="true" Codebehind="campaignlayout.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.campaignlayout" %>
<%@ Register TagPrefix="ftb" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<table class="npadmintable">
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlFromEmail" runat="server" Text="From Email:"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="tbSenderEmail" runat="server" Columns="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlFromName" runat="server" Text="From Name:"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="tbSenderName" runat="server" Columns="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlSubject" runat="server" Text="Subject:"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="tbSubject" runat="server" Columns="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlTemplate" runat="server" Text="Template:"></asp:Literal></td>
        <td class="npadminbody">
            <asp:DropDownList ID="ddlTemplates" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTemplates_SelectedIndexChanged">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td colspan="2" class="npadminbody">
            <ftb:FreeTextBox SupportFolder="~/assets/common/FreeTextBox" ID="ftbContent" runat="Server"
                ToolbarLayout="JustifyLeft, JustifyRight, JustifyCenter, JustifyFull, BulletedList, NumberedList, Indent, Outdent | FontFacesMenu, FontSizesMenu, FontForeColorPicker, FontBackColorPicker, Bold, Italic, Underline | RemoveFormat, WordClean, ieSpellCheck, Print, Preview | CreateLink, Unlink, InsertRule, InsertTable, EditTable | SelectAll, Undo, Redo | InsertImage, InsertImageFromGallery"
                Width="100%" ToolbarStyleConfiguration="Office2003">
            </ftb:FreeTextBox>
        </td>
    </tr>
</table>
<asp:Literal runat="server" ID="hdnUnsubscribe" Visible="false" Text="Unsubscribe" />