<%@ Control Language="C#" Codebehind="listsubscribers.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.listsubscribers" %>
<asp:GridView ID="gvSubscribers" runat="server" AutoGenerateColumns="False" PageSize="50" DataKeyNames="SubscriberID"
    CssClass="npadmintable" AllowPaging="True" AllowSorting="True" EmptyDataText="No Subscriptions Found" 
    OnRowDataBound="gvSubscribers_RowDataBound"
     OnSorting="gvSubscribers_Sorting" OnRowCommand="gvSubscribers_RowCommand" OnPageIndexChanging="gvSubscribers_PageIndexChanging" >
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <Columns>
        <asp:BoundField DataField="SubscriberID" Visible="false" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SubscriberID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="colEmail|Email"></asp:BoundField>
        <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderText="colFirstName|First Name"></asp:BoundField>
        <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderText="colLastName|Last Name"></asp:BoundField>
        <asp:TemplateField HeaderText="colDetail|Detail" SortExpression="UserID, ProspectID">
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="lnkDetail"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="SubscribeType">
            <ItemTemplate>
                <asp:Label runat="server" ID="lblStatus"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="SubscribeDate" SortExpression="SubscribeDate" HeaderText="colDate|Date"
            DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
    </Columns>
    <PagerSettings Position="Bottom" Mode="numeric" />
    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
</asp:GridView>
<asp:Label runat="server" ID="hdnPending" Visible="false" Text="Pending"></asp:Label>
<asp:Label runat="server" ID="hdnSubscribed" Visible="false" Text="Subscribed"></asp:Label>
<asp:Label runat="server" ID="hdnUnsubscribed" Visible="false" Text="Unsubscribed"></asp:Label>
<asp:Label runat="server" ID="hdnOpen" Visible="false" Text="Open"></asp:Label>
<asp:Label runat="server" ID="hdnProspect" Visible="false" Text="Prospect"></asp:Label>