<%@ Control Language="C#" AutoEventWireup="true" Codebehind="prospectgeneral.ascx.cs" Inherits="netpoint.admin.prospects.controls.prospectgeneral" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<table class="npadmintable">
    <tr>
        <td valign="top">
            <table class="npadminbody" cellpadding="3" cellspacing="0" width="100%">
                <tr>
                    <td class="npadminsubheader" colspan="2">
                        <asp:Literal ID="ltlContactPerson" runat="server" Text="Contact Person"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Title" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlNameFirstLast" runat="server" Text="First Name"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="FirstName" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="MiddleName" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlLastName" runat="server" Text="Last Name"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="LastName" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlSuffix" runat="server" Text="Suffix"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Suffix" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlJobTitle" runat="server" Text="Job Title"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="JobTitle" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlEmailAddress" runat="server" Text="Email"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Email" Columns="35" runat="server"></asp:TextBox>
                        <asp:HyperLink ID="lnkEmail" runat="server" ImageUrl="~/assets/common/icons/send.gif"
                            Target="_blank" ToolTip="Email"></asp:HyperLink></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlOfficeNo" runat="server" Text="Office #"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PhoneOffice" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlFaxNo" runat="server" Text="Fax #"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PhoneFax" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlHomeNo" runat="server" Text="Home #"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PhoneHome" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlMobileNo" runat="server" Text="Mobile #"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PhoneMobile" Columns="35" runat="server"></asp:TextBox></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="npadminbody" width="100%">
                <tr>
                    <td class="npadminsubheader" colspan="2">
                        <asp:ImageButton ID="btnPop" runat="server" ImageUrl="~/assets/common/icons/contactinfo.gif" Visible="false">
                        </asp:ImageButton>
                        <asp:Literal ID="ltlAddress" runat="server" Text="Address"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlStreet1" runat="server" Text="Street1"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Street1" Columns="25" runat="server" Width="180px" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlStreet2" runat="server" Text="Street2"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Street2" Columns="25" runat="server" Width="180px" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlCity" runat="server" Text="City"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="City" Columns="25" runat="server" Width="180px" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList>
                        <asp:TextBox ID="State" Columns="25" runat="server" Width="180px" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlPostalCode" runat="server" Text="Zip"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PostalCode" Columns="25" runat="server" Width="180px" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
                    <td>
                        <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" Width="180px" ></asp:DropDownList>
                        <asp:TextBox ID="Country" Columns="25" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlMapLink" runat="server" Text="MapLink"></asp:Literal></td>
                    <td align="right" class="npadminlabel">
                        <NP:MapLink runat="Server" ID="npmaplink" Target="_blank" ImageUrl="~/assets/common/icons/map.png" Visible="false" ToolTip="Preview Address"></NP:MapLink>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="npadminbody" width="100%">
                <tr>
                    <td class="npadminsubheader" colspan="2">
                        <asp:Literal ID="ltlOrganization" runat="server" Text="Organization"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlCompanyName" runat="server" Text="Name"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="CompanyName" Columns="60" runat="server"></asp:TextBox></td>
                </tr>
                                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlWebsite" runat="server" Text="Website"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="Website" Columns="60" runat="server"></asp:TextBox>
                        <asp:HyperLink ID="lnkWebsite" runat="server" ImageUrl="~/assets/common/icons/preview.gif"
                            Target="_blank" ToolTip="Display Web Site"></asp:HyperLink></td>
                </tr>
                <tr>
                    <td align="right" class="npadminlabel">
                        <asp:Literal ID="ltlPhoneCompanyMain" runat="server" Text="Main #"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="PhoneCompany" Columns="25" runat="server"></asp:TextBox></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">
            <table class="npadminbody" width="100%">
                <tr>
                    <td class="npadminsubheader">
                        <asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="Notes" Columns="65" runat="server" Rows="8" TextMode="multiline"></asp:TextBox></td>
                </tr>
            </table>
        </td>
        <td valign="top">
        </td>
    </tr>
</table>
