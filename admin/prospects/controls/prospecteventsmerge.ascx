<%@ Control Language="c#" Inherits="netpoint.admin.prospects.controls.prospecteventsmerge" Codebehind="prospecteventsmerge.ascx.cs" %>
 <asp:GridView 
    id="gvEvents"
    runat="server" 
    AutoGenerateColumns="false" AllowPaging="true" PageSize="20"
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" 
    OnPageIndexChanging="gvEvents_PageIndexChanging" 
    OnRowDataBound="gvEvents_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Literal ID="ltlEventID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ActivityID") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
		<asp:TemplateField SortExpression="ActivityCode" HeaderText="colType|Type">
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink Runat="server" ID="lnkType" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="Remarks" HeaderText="colMessage|Message">
			<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
			<ItemTemplate>
				<b>
					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Remarks") %>' ID="Label1"></asp:Label></b>
				<br>
				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ActivityContent") %>' ID="Label2"></asp:Label>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="StartDate" SortExpression="StartDate" HtmlEncode="false" HeaderText="colStartDate|Start Date" DataFormatString="{0:yyyy-MMM-dd hh:mm}">
			<HeaderStyle Width="20%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
		</asp:BoundField>
		<asp:TemplateField>
			<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:RadioButtonList id="rblAction" runat="server">
					<asp:ListItem Value="add" Selected="True">Add</asp:ListItem>
					<asp:ListItem Value="delete">Delete</asp:ListItem>
				</asp:RadioButtonList>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>
<div class="npadminempty" visible="false"  id="divEmpty" runat="server" style="height:50px">
<asp:Literal id="hdnNoEvents" runat="server" Text="There are no activities for this prospect"></asp:Literal>
</div>

<asp:Literal id="hdnAdd" runat="server" Text="Add" Visible="False"></asp:Literal>
<asp:Literal id="hdnDelete" runat="server" Text="Delete" Visible="False"></asp:Literal>
