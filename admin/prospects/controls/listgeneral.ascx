<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listgeneral.ascx.cs" Inherits="netpoint.admin.prospects.controls.listgeneral" %>
<table class="npadminbody">
    <tr>
        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
        <td>
            <div id="formdiv1">
                <asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList>
                <asp:CheckBox ID="cbActiveFlag" runat="server" Text="Active" Checked="true"></asp:CheckBox>
                <asp:CheckBox ID="cbDoNotCallFlag" runat="server" Text="Do Not Call"></asp:CheckBox>
                <asp:TextBox ID="tbListID" Visible="False" runat="server"></asp:TextBox></div></td>
    </tr>
    <tr>
        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
        <td><asp:TextBox ID="tbListName" runat="server" Columns="35"></asp:TextBox></td>
    </tr>
    <tr>
        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlTheme" runat="server" Text="Theme"></asp:Literal></td>
        <td style="height:20px;"><div id="formdiv2"><asp:DropDownList ID="ddlServerID" runat="server"></asp:DropDownList></div></td>
    </tr>
    <tr>
        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlAccountType" runat="server" Text="Account Type"></asp:Literal></td>
        <td style="height:20px;"><div id="Div1"><asp:DropDownList ID="ddlAccountType" runat="server"></asp:DropDownList></div></td>
    </tr>
    <tr>
        <td align="center" colspan="2"></td>
    </tr>
</table>