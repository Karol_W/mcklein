<%@ Control Language="c#" Inherits="netpoint.admin.prospects.controls.lists" Codebehind="lists.ascx.cs" %>
<asp:GridView ID="gridLists" CssClass="npadmintable" AutoGenerateColumns="False"
    PageSize="25" AllowPaging="True" runat="server" EmptyDataText="No Lists Found" 
    OnPageIndexChanging="gridLists_PageIndexChanging" OnRowCommand="gridLists_RowCommand" OnRowDataBound="gridLists_RowDataBound">
    <HeaderStyle CssClass="npadminsubheader" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton runat="server" ID="btnRemove" ImageUrl="~/assets/common/icons/remove.gif"></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="colEmail|Email" />
        <asp:TemplateField HeaderText="colList|List" SortExpression="ListID">
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="lnkList"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colStatus|Status" SortExpression="Subscribed">
            <ItemTemplate>
                <asp:Label runat="server" ID="lblStatus"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDate|Date" SortExpression="SubscribeDate">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SubscribeDate") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblSubscribeDate" runat="server" Text='<%# Bind("SubscribeDate", "{0:yyyy-MMM-dd}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle CssClass="npadminbody" />
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:DropDownList ID="ddlLists" runat="server"></asp:DropDownList>&nbsp;
    <asp:ImageButton ID="btnAddList" ImageUrl="~/assets/common/icons/add.gif" runat="server" OnClick="btnAddList_Click" ToolTip="Add List"></asp:ImageButton>
</div>
<asp:Literal ID="errInvalidSubscriberEmail" runat="server" Text="The subscriber does not have a valid email address and will not be added to the list." Visible="False"></asp:Literal>
