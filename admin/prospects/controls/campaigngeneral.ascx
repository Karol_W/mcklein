<%@ Control Language="C#" AutoEventWireup="true" Codebehind="campaigngeneral.ascx.cs" Inherits="netpoint.admin.prospects.controls.campaigngeneral" %>
<div class="npwarning"><asp:Literal ID="sysError" runat="server"></asp:Literal></div>
<table class="npadmintable">
    <tr>
        <td valign="top">
            <table class="npadminbody">
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlCode" runat="server" Text="Code"></asp:Literal></td>
                    <td class="npadminbody" style="white-space:nowrap;">
                        <asp:TextBox ID="tbCampaignCode" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbCampaignCode" runat="server">
                            <img id="imgWarning" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
                    <td class="npadminbody" style="white-space:nowrap;">
                        <asp:TextBox ID="tbName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tbName" runat="server">
                            <img id="imgErr" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
                    <td class="npadminbody"><asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlOwner" runat="server" Text="Owner"></asp:Literal></td>
                    <td class="npadminbody"><asp:DropDownList ID="ddlOwnerUserID" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlBudget" runat="server" Text="Budget"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="tbBudget" runat="server" Columns="10"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel"><asp:Literal ID="ltlTargetQuantity" runat="server" Text="Target Response"></asp:Literal></td>
                    <td class="npadminbody"><asp:TextBox ID="tbTargetQty" runat="server" Columns="10"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="left"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
                    <td class="npadminbody" align="left"><asp:TextBox ID="tbDescription" runat="server" Columns="33" TextMode="multiline" Rows="4" Width="450px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="left"><asp:Literal ID="ltlObjectives" runat="server" Text="Objectives"></asp:Literal></td>
                    <td class="npadminbody" align="left" ><asp:TextBox ID="tbObjectives" runat="server" Columns="33" TextMode="multiline" Rows="8" Width="450px"></asp:TextBox></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="errName" runat="server" Visible="false" Text="A campaign with this name or code already exists, please choose another."></asp:Literal>
<asp:Literal ID="errDates" runat="server" Visible="false" Text="End date must be greater than start date."></asp:Literal>
