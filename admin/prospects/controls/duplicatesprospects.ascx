<%@ Control Language="C#" AutoEventWireup="true" Codebehind="duplicatesprospects.ascx.cs" Inherits="netpoint.admin.prospects.controls.duplicatesprospects" %>
<asp:GridView 
    ID="gridDuplicates" 
    runat="server" 
    CssClass="npadmintable" 
    AutoGenerateColumns="False"
    AllowSorting="True" 
    AllowPaging="True"
    EmptyDataText="No Matching Prosopects" 
    PageSize="50"
    OnRowDataBound="gridDuplicates_ItemDataBound" 
    OnPageIndexChanging="gridDuplicates_PageIndexChanging"
    OnSorting="gridDuplicates_Sorting">
    <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
    <RowStyle CssClass="npadminbody"></RowStyle>
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <Columns>
        <asp:BoundField DataField="NumOccurrences" SortExpression="NumOccurrences" HeaderText="colProspectCount|Prospect Count">
            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField SortExpression="DupValue" HeaderText="colProspectDuplicate|Prospect Duplicate">
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="lnkDupValue"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="Bottom" Mode="numeric" />
    <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:RadioButtonList ID="sysMatches" runat="server" OnSelectedIndexChanged="rblMatches_OnChange"
        AutoPostBack="True" DataValueField="value" DataTextField="display" EnableViewState="true"
        RepeatLayout="table" RepeatDirection="Horizontal" CssClass="npadminbody">
    </asp:RadioButtonList>&nbsp;
    <asp:ImageButton ID="btnDeleteAll" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
        Width="17" ToolTip="This will delete all shown duplicates, keeping newest record only" OnClick="btnDeleteAll_Click" />&nbsp;
</div>
<asp:Literal ID="errDelete" runat="server" Text="This will delete all shown duplicates, keeping newest record only.\nDeletions are Permanent. Are you sure?" Visible="false"></asp:Literal>
<asp:Literal ID="hdnName" runat="server" Text="Name" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCompany" runat="server" Text="Company" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEmail" runat="server" Text="Email" Visible="False"></asp:Literal>
<asp:Literal ID="hdnHomePhone" runat="server" Text="Home Phone" Visible="False"></asp:Literal>
<asp:Literal ID="hdnTrackingNo" runat="server" Text="Tracking No" Visible="False"></asp:Literal>
<asp:Literal ID="hdnOfficePhone" runat="server" Text="Office Phone" Visible="False"></asp:Literal>
<asp:Literal ID="errDupes" runat="server" Text="All matches from the prospect list will be deleted.  Continue?" Visible="False"></asp:Literal>
