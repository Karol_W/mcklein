<%@ Control Language="C#" AutoEventWireup="true" Codebehind="prospecttracking.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.prospecttracking" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<table class="npadmintable">
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlContactPreference" runat="server" Text="User Pref"></asp:Literal></td>
        <td colspan="3">
            <asp:TextBox ID="ContactPreference" Columns="40" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlSalesPerson" runat="server" Text="Salesperson"></asp:Literal></td>
        <td>
            <asp:DropDownList ID="SalesPersonID" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlFollowUpDate" runat="server" Text="FollowUp Date"></asp:Literal></td>
        <td>
            <np:npdatepicker id="FollowUpDate" runat="server"></np:npdatepicker>
        </td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlTrackingNo" runat="server" Text="Number"></asp:Literal></td>
        <td>
            <asp:TextBox ID="TrackingNo" Columns="25" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlSource" runat="server" Text="Source"></asp:Literal></td>
        <td>
            <asp:TextBox ID="Source" Columns="25" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
        <td>
            <asp:TextBox ID="Type" Columns="25" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlIndustry" runat="server" Text="Industry"></asp:Literal></td>
        <td>
            <asp:TextBox ID="Industry" Columns="25" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
        <td>
            <asp:DropDownList ID="Status" runat="server">
            </asp:DropDownList></td>
    </tr>
        <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal runat="server" ID="ltlAddDate" Text="Add Date"></asp:Literal></td>
        <td colspan="3">
            <asp:Label CssClass="npadminbody" ID="AddDate" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td align="right" class="npadminlabel">
            <asp:Literal ID="ltlProspectID" runat="server" Text="ID"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Label ID="ProspectID" runat="server"></asp:Label></td>
    </tr>
</table>
