<%@ Control Language="C#" AutoEventWireup="true" Codebehind="listbulkadd.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.listbulkadd" %>
    <div style="text-align:center">
<asp:TextBox runat="server" ID="tbSubscribers" Columns="65" TextMode="MultiLine"
    Rows="17" Wrap="False" />
<br /><br />
<asp:Label runat="server" ID="lblSubscribeInstructions" Text="Enter or paste email addresses (one per line) above and then click on the &quot;Subscribe&quot; button below to add them to this list"></asp:Label>
<br /><br />
<asp:Button runat="server" ID="btnSubscribe" Text="Subscribe" OnClick="btnSubscribe_Click" />
</div>