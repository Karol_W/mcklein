<%@ Control Language="C#" AutoEventWireup="true" Codebehind="campaignassigns.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.campaignassigns" %>
<%@ Register TagPrefix="np" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="NPTimePicker" Src="~/common/controls/NPTimePicker.ascx" %>    
<table class="npadmintable">
    <tr>
        <td class="npadminbody">
            <asp:Literal ID="ltlAvailableLists" runat="server" Text="Available Lists:"></asp:Literal><br />
            <asp:DropDownList ID="ddlLists" runat="server" Width="320px">
            </asp:DropDownList>
            <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add">
            </asp:ImageButton><br />
            <br />
            <asp:Literal ID="ltlStartDate" runat="server" Text="Scheduled Send Date"></asp:Literal><br />
            <np:NPDatePicker ID="dpStartDate" runat="server"></np:NPDatePicker> 
            <np:NPTimePicker ID="tpStartTime" runat="server"></np:NPTimePicker>
            <br />
            <br />
            <asp:Literal ID="ltlAssignedLists" runat="server" Text="Assigned Lists:"></asp:Literal><br />
            <asp:ListBox ID="lbCampaignLists" runat="server" Rows="10" Width="320px"></asp:ListBox>
            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/assets/common/icons/remove.gif" OnClick="btnRemove_Click" ToolTip="Remove">
            </asp:ImageButton>
        </td>
        <td valign="middle" align="center" class="npadminbody">
            <asp:Literal ID="ltlSubscribed" runat="server" Text="Subscribed:"></asp:Literal>
            <asp:Label ID="lblSubscribedTotal" runat="server"></asp:Label><br />
            <asp:Literal ID="ltlEmails" runat="server" Text="Emails:"></asp:Literal>
            <asp:Label ID="lblEmailsTotal" runat="server"></asp:Label><br />
            <asp:Literal ID="ltlPending" runat="server" Text="Pending:"></asp:Literal>
            <asp:Label ID="lblPendingTotal" runat="server"></asp:Label><br />
            <br />
            <hr />
            <asp:Label ID="lblSendOneoff" runat="server" Text="Send One Copy To: "></asp:Label><br />
            <asp:TextBox ID="tbOneoffEmailAddress" runat="server" Columns="25"></asp:TextBox>
            <asp:ImageButton ID="btnSendOneoff" runat="server" ImageUrl="~/assets/common/icons/send.gif" OnClick="btnSendOneoff_Click" ToolTip="Send One Off">
            </asp:ImageButton><br />
            <asp:CheckBox ID="cbSubscribeOneoff" runat="server" Text="and subscribe to selected list">
            </asp:CheckBox><br />
            <asp:Label ID="lblSentMessage" runat="server" Visible="false" Text="Email Sent "
                CssClass="npwarning"></asp:Label>
            <hr />
            <br />
            <asp:Literal ID="ltlSentDate" runat="server" Text="Queued Date:" Visible="false"></asp:Literal>
            <asp:Label ID="sysSentTime" runat="server" Visible="false"></asp:Label>
            <asp:Button ID="btnSend" runat="server" Text="Send Campaign to Queue" OnClick="btnSend_Click">
            </asp:Button></td>
    </tr>
</table>
