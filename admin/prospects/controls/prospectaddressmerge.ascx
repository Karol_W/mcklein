<%@ Control Language="C#" AutoEventWireup="true" Codebehind="prospectaddressmerge.ascx.cs"
    Inherits="netpoint.admin.prospects.controls.prospectaddressmerge" %>
    
<table class="npadmintable">
    <tr>
        <td class="npadminbodyalt" align="center" colspan="5">
            <asp:RadioButtonList ID="rdoAddress" runat="server" CssClass="npadminbodyalt" RepeatDirection="Horizontal">
                <asp:ListItem Value="new">New</asp:ListItem>
                <asp:ListItem Value="ignore" Selected="True">Ignore</asp:ListItem>
                <asp:ListItem Value="merge">Merge</asp:ListItem>
            </asp:RadioButtonList><asp:Label ID="sysAddressErr" runat="server" CssClass="npwarning"></asp:Label></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="center" colspan="2">
            <b>
                <asp:Literal ID="ltlProspectAddressData" runat="server" Text="Prospect Data"></asp:Literal></b></td>
        <td class="npadminlabel" align="center">
            <b>
                <asp:Literal ID="ltlUseProspect" runat="server" Text="Use Prospect Data"></asp:Literal></b></td>
        <td class="npadminlabel" align="center" colspan="2">
            <b>
                <asp:Literal ID="ltlUseAddress" runat="server" Text="Address Data"></asp:Literal></b></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlAddressName" runat="server" Text="Name"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtAddressName" runat="server"></asp:TextBox><asp:Image ID="imgProspectAddressName"
                runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
        <td class="npadminlabel">
        </td>
        <td class="npadminbodyalt" align="center">
            <asp:DropDownList ID="ddlAddress" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAddress_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAddName" runat="server" Text="Name"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectCountry" runat="server" Text="Country"></asp:Literal></td>
        <td align="center">
            <asp:DropDownList ID="ddlProspectCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProspectCountry_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyCountry" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:DropDownList ID="ddlAccountCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAccountCountry_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountCountry" runat="server" Text="Country"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectStreet1" runat="server" Text="Street1"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectStreet1" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyStreet1" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountStreet1" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountStreet1" runat="server" Text="Street1"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectStreet2" runat="server" Text="Street2"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectStreet2" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyStreet2" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountStreet2" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountStreet2" runat="server" Text="Street2"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectCity" runat="server" Text="City"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectCity" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyCity" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountCity" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountCity" runat="server" Text="City"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectState" runat="server" Text="State"></asp:Literal></td>
        <td align="center">
            <asp:DropDownList ID="ddlProspectState" runat="server">
            </asp:DropDownList></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyState" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:DropDownList ID="ddlAccountState" runat="server">
            </asp:DropDownList></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountState" runat="server" Text="State"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right">
            <asp:Literal ID="ltlProspectPostalCode" runat="server" Text="Postal Code"></asp:Literal></td>
        <td align="center">
            <asp:TextBox ID="txtProspectPostalCode" runat="server"></asp:TextBox></td>
        <td class="npadminlabel" align="center">
            <asp:CheckBox ID="sysCopyPostalCode" runat="server"></asp:CheckBox></td>
        <td class="npadminbodyalt" align="center">
            <asp:TextBox ID="txtAccountPostalCode" runat="server"></asp:TextBox></td>
        <td class="npadminlabel">
            <asp:Literal ID="ltlAccountPostalCode" runat="server" Text="Postal Code"></asp:Literal></td>
    </tr>
</table>


<asp:Literal ID="errNoAddressToMerge" runat="server" Visible="False" Text="No Address has been selected to merge with."></asp:Literal>
<asp:Literal ID="hdnAddressProspect" runat="server" Text="Address created from prospect." Visible="false"></asp:Literal>
<asp:Literal ID="errName" runat="server" Text="Address requires a name." Visible="false"></asp:Literal>