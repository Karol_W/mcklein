<%@ Control Language="c#" Inherits="netpoint.admin.prospects.controls.logsmerge" Codebehind="logsmerge.ascx.cs" %>
<asp:GridView 
    id="gvLogs"
    runat="server" 
    AllowPaging="true"
    PageSize="20"
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowDataBound="gvLogs_RowDataBound" OnPageIndexChanging="gvLogs_PageIndexChanging">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField SortExpression="LogType" HeaderText="colType|Type">
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
			<ItemTemplate>
				<asp:HyperLink Runat="server" ID="lnkType" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="OwnerID" SortExpression="Owner" HeaderText="colOwnerID|Owner ID"></asp:BoundField>
		<asp:BoundField DataField="Email" SortExpression="Email" HeaderText="colContact|User"></asp:BoundField>
		<asp:TemplateField SortExpression="Subject" HeaderText="colMessage|Message">
			<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
			<ItemTemplate>
				<b>
					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Subject") %>' ID="lblLogTopic">
					</asp:Label></b>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="TimeStamp" SortExpression="TimeStamp" HtmlEncode="false" HeaderText="colDate|Date" DataFormatString="{0:yyyy-MMM-dd hh:mm}">
			<HeaderStyle Width="20%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
		</asp:BoundField>
		<asp:TemplateField>
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemTemplate>
				<asp:RadioButtonList id="rblAction" runat="server">
					<asp:ListItem Value="add" Selected="True">Add</asp:ListItem>
					<asp:ListItem Value="delete">Delete</asp:ListItem>
				</asp:RadioButtonList>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField Visible="false">
		    <ItemTemplate>
		        <asp:Literal ID="ltlLogID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LogID") %>'></asp:Literal>
		    </ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
<div id="divEmpty" runat="server" class="npadminempty" visible="false">
<asp:Literal id="hdnNoLogs"  Text="There are no logs for this prospect" runat="server"></asp:Literal>
</div>
<asp:Literal id="hdnAdd" runat="server" Text="Add" Visible="False"></asp:Literal>
<asp:Literal id="hdnDelete" runat="server" Text="Delete" Visible="False"></asp:Literal>

