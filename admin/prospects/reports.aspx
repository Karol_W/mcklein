<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.Reports" Codebehind="Reports.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Prospect Reports</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">&nbsp;</td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="npadminsubheader">
                <asp:Image id="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Report List"></asp:Literal></td>
        </tr>
    </table>
    <asp:GridView ID="gvReports" CssClass="npadmintable" AutoGenerateColumns="False"
        PageSize="25" AllowPaging="True" runat="server" EmptyDataText="No Records Found" OnRowDataBound="gvReports_RowDataBound">
        <HeaderStyle CssClass="npadminsubheader" />
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <Columns>
            <asp:TemplateField HeaderText="colName|Name">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkReport" runat="server" NavigateUrl="~/admin/common/reports/ReportLaunch.aspx?code="
                        Target="_blank"><%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Description" HeaderText="colDescription|Description"></asp:BoundField>
        </Columns>
    </asp:GridView>
</asp:Content>
