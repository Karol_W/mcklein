<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.DuplicateDetail" Codebehind="DuplicateDetail.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:Label runat="server" ID="ltlBreadCrumb" CssClass="npbody" Text="Prospecting > Prospect Duplicates > Duplicate Detail" />
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="npadminheader">
                <asp:HyperLink runat="server" ID="lnkBack" ImageUrl="~/assets/common/icons/cancel.gif" NavigateUrl="Duplicates.aspx" ToolTip="Back" />
                Duplicates for
                <asp:Label runat="server" ID="lblDuplicateHeader" /></td>
            <td class="npadminheader" align="right">
                <asp:ImageButton runat="server" ID="btnDeleteAll" ImageUrl="~/assets/common/icons/delete.gif" 
                ToolTip="Delete All Marked Duplicates" OnClick="btnDeleteAll_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gridDuplicates" runat="server" Width="100%" CellPadding="1" AutoGenerateColumns="False"
                    AllowSorting="True" AllowPaging="True" PageSize="50" BorderColor="Black" DataKeyNames="ProspectID"
                    EmptyDataText="No Duplicates Found" OnSorting="gridDuplicates_Sorting">
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <RowStyle CssClass="npadminbody" />
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="colID|ID" SortExpression="ProspectID">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkID" runat="server" NavigateUrl='<%# Eval("ProspectID", "ProspectDetail.aspx?ProspectID={0}") %>'
                                    Text='<%# Eval("ProspectID") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderText="colFirstName|First Name" />
                        <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderText="colLastName|Last Name" />
                        <asp:BoundField DataField="CompanyName" SortExpression="CompanyName" HeaderText="colCompanyName|Company Name" />
                        <asp:BoundField DataField="PhoneOffice" SortExpression="PhoneOffice" HeaderText="colPhoneOffice|Phone Office" />
                        <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="colEmail|Email" />
                        <asp:BoundField DataField="City" SortExpression="City" HeaderText="colCity|City" />
                        <asp:BoundField DataField="State" SortExpression="State" HeaderText="colState|State" />
                        <asp:TemplateField>
                        <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkDelete" />
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="npadminbody" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal runat="server" ID="hdnDeleteMessage" Text="This will delete all marked duplicates.\nDeletions are permanent. Continue?" Visible="False" />
</asp:Content>
