<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.prospects.templateeditor" Codebehind="templateeditor.aspx.cs" %>
<%@ Register TagPrefix="ftb" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll"  runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="SaveButton_Click" />&nbsp;</td>
                    
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tbody>
                 <tr>
                    <td class="npadminlabel" align="left">&nbsp;<asp:Literal ID="ltlTemplateCode" runat="server" Text="Code"></asp:Literal>&nbsp;</td>
                    <td>
                        <asp:TextBox EnableViewState="true" ID="TemplateCode" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="pcval" ControlToValidate="TemplateCode"
                            ErrorMessage="TemplateCode is a required field." CssClass="errormsg" Display="Static">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="npadminlabel" align="left">&nbsp;<asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal>&nbsp</td>
                    <td colspan="4">
                        <asp:TextBox EnableViewState="true" ID="TemplateName" runat="server" Width="300px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="val_Name" ControlToValidate="TemplateName"
                            ErrorMessage="Name is a required field." CssClass="errormsg" Display="Static">
                            <asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
                </tr>
                <tr><td colspan="3" class="npadminsubheader">&nbsp;</td></tr>
                <tr>
                    <td colspan="2" class="npadminbody">
                        <ftb:FreeTextBox SupportFolder="~/assets/common/FreeTextBox" ID="ftbContent" runat="Server"
                            ToolbarLayout="JustifyLeft, JustifyRight, JustifyCenter, JustifyFull, BulletedList, NumberedList, Indent, Outdent | FontFacesMenu, FontSizesMenu, FontForeColorPicker, FontBackColorPicker, Bold, Italic, Underline | RemoveFormat, WordClean, ieSpellCheck, Print, Preview | CreateLink, Unlink, InsertRule, InsertTable, EditTable | SelectAll, Undo, Redo | InsertImage, InsertImageFromGallery"
                            Width="100%" ToolbarStyleConfiguration="Office2003">
                        </ftb:FreeTextBox>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
<asp:Literal runat="server" ID="hdnUnsubscribe" Visible="false" Text="Unsubscribe" />
</asp:Content>
