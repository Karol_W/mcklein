<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.Duplicates" Codebehind="Duplicates.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="duplicatesprospects" Src="~/admin/prospects/controls/duplicatesprospects.ascx" %>
<%@ Register TagPrefix="np" TagName="duplicatesaccounts" Src="~/admin/prospects/controls/duplicatesaccounts.ascx" %>
<%@ Register TagPrefix="np" TagName="duplicatesusers" Src="~/admin/prospects/controls/duplicatesusers.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Seek and Destroy Duplicates</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Search for and delete duplicate prospects, business partners, or users" /></td>
            <td class="npadminheader" align="right">
                &nbsp;
            </td>
        </tr>
    </table>
          <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab  runat="server" ID="ts1" Text="Prospects">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Accounts">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Users">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:duplicatesprospects runat="server" id="duplicatesprospects"></np:duplicatesprospects>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:duplicatesaccounts runat="server" id="duplicatesaccounts"></np:duplicatesaccounts>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server">
                <np:duplicatesusers runat="server" id="duplicatesusers"></np:duplicatesusers>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
</asp:Content>
