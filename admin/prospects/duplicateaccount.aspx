<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.DuplicateAccount" Codebehind="DuplicateAccount.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0" class="npadminbody">
        <tr>
            <td class="npadminheader">
                <asp:HyperLink ID="lnkCancel" runat="server" NavigateUrl="Duplicates.aspx" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:HyperLink>&nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Duplicates with Account"></asp:Literal></td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete" ImageUrl="~/assets/common/icons/delete.gif">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2">
                <asp:Image ID="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif">
                </asp:Image>&nbsp;
                <asp:Literal ID="ltlProspect" runat="server" Text="Prospect"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysProspect" runat="server"></asp:Literal></td>
            <td class="npadminlabel">
                <table class="npadminlabel" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td>
                            <asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
                        <td>
                            <asp:Literal ID="sysEmail" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="ltlMainPhone" runat="server" Text="Main Phone"></asp:Literal></td>
                        <td>
                            <asp:Literal ID="sysMain" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="ltlOffice" runat="server" Text="Office"></asp:Literal></td>
                        <td>
                            <asp:Literal ID="sysOffice" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="ltlHome" runat="server" Text="Home"></asp:Literal></td>
                        <td>
                            <asp:Literal ID="sysHome" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="ltlFax" runat="server" Text="Fax"></asp:Literal></td>
                        <td>
                            <asp:Literal ID="sysFax" runat="server"></asp:Literal></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="2">
                <asp:Image ID="imgGridIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif">
                </asp:Image>&nbsp;
                <asp:Literal ID="ltlGridHeader" runat="server" Text="Possible Matches"></asp:Literal></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:gridview
                    id="gvDupe"
                    runat="server" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-CssClass="npadminsubheader"
                    CssClass="npadmintable" 
                    RowStyle-CssClass="npadminbody" 
                    AlternatingRowStyle-CssClass="npadminbodyalt" 
                    EmptyDataText="No Records Found">
                    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                    <Columns>
                        <asp:TemplateField HeaderText="colID|ID"></asp:TemplateField>
                        <asp:TemplateField HeaderText="colName|Name"></asp:TemplateField>
                        <asp:TemplateField HeaderText="colPhone|Phone"></asp:TemplateField>
                    </Columns>
                </asp:gridview>
            </td>
        </tr>
    </table>
</asp:Content>
