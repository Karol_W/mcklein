<%@ Page ValidateRequest="false" Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.CampaignDetail" Codebehind="CampaignDetail.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="campaigngeneral" Src="~/admin/prospects/controls/campaigngeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="campaignlayout" Src="~/admin/prospects/controls/campaignlayout.ascx" %>
<%@ Register TagPrefix="np" TagName="campaignassigns" Src="~/admin/prospects/controls/campaignassigns.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSave_Click" ToolTip="Save" />&nbsp;</td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="CampaignTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="CampaignMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Layout"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Assigns"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="CampaignMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
        Width="100%" Height="100">
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:campaigngeneral ID="campaigngeneral" runat="server"></np:campaigngeneral>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:campaignlayout ID="campaignlayout" runat="server"></np:campaignlayout>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
           <np:campaignassigns ID="campaignassigns" runat="server"></np:campaignassigns>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
</asp:Content>
