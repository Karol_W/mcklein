<%@ Page ValidateRequest="false" Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.ProspectDetail" Codebehind="ProspectDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="prospectgeneral" Src="~/admin/prospects/controls/prospectgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="prospecttracking" Src="~/admin/prospects/controls/prospecttracking.ascx" %>
<%@ Register TagPrefix="np" TagName="logs" Src="~/admin/prospects/controls/logs.ascx" %>
<%@ Register TagPrefix="np" TagName="lists" Src="~/admin/prospects/controls/lists.ascx" %>
<%@ Register TagPrefix="np" TagName="activities" Src="~/admin/focus/controls/activities.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <script type="text/javascript">
        function cantPromote(message){
            alert(message);
            return false;
        }
    </script>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnMerge" runat="server" ImageUrl="~/assets/common/icons/promote.gif" ToolTip="Promote to Account" />&nbsp;
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All" />&nbsp;</td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="ProspectsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="ProspectsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Tracking"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Activities"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Lists"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Logs"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="ProspectsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
        Width="100%" Height="100">
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:prospectgeneral ID="prospectgeneral" runat="server"></np:prospectgeneral>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:prospecttracking ID="prospecttracking" runat="server"></np:prospecttracking>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
           <np:activities ID="activities" runat="server" AccountColumnVisible="false"></np:activities>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:lists ID="lists" runat="server"></np:lists>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:logs ID="logs" runat="server"></np:logs>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal runat="server" ID="errDelete" Visible="false" Text="Deletions are Permanent. Are you sure?" />
    <asp:Literal ID="errCantPromote" runat="server" Text="Prospect must have be saved with a First and Last Name and a Company Name in order to be promoted." Visible="false"></asp:Literal>
</asp:Content>
