<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="netpoint.admin.prospects._default" %>
<%@ Register TagPrefix="np" TagName="LandingPage" Src="~/admin/common/controls/landingpage.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <asp:Literal ID="sysMessage" runat="server"></asp:Literal>
    <np:LandingPage ID="lpage" runat="server" />
</asp:Content>

