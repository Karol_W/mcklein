<%@ Page Language="c#" MasterPageFile="~/masters/default.master" Inherits="netpoint.admin.prospects.LogDetail" Codebehind="LogDetail.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table cellspacing="0" cellpadding="3" width="100%" border="0">
        <tr>
            <td>
                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlOwner" runat="server" Text="Owner"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="sysOwner" runat="server"></asp:Literal></td>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlDate" runat="server" Text="Date"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="sysDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="sysEmail" runat="server"></asp:Literal></td>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlStatus" runat="server" Text="Status"></asp:Literal></td>
                        <td class="npadminbody">
                            <asp:Literal ID="sysStatus" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="btnClose" runat="server" ImageUrl="~/assets/common/icons/lock.gif" Visible="False" ToolTip="Close" />
                            <asp:ImageButton ID="btnOpen" runat="server" ImageUrl="~/assets/common/icons/open.gif" Visible="False" ToolTip="Open" /></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlAccount" runat="server" Text="Account"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="sysAccount" runat="server"></asp:Literal></td>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlDirection" runat="server" Text="Direction"></asp:Literal></td>
                        <td class="npadminbody"><asp:Literal ID="sysDirection" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" style="width:50px;"><asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal></td>
                        <td class="npadminbody" colspan="3"> <b><asp:Literal ID="sysSubject" runat="server"></asp:Literal></b></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><asp:Literal ID="sysMessage" runat="server"></asp:Literal></td>
        </tr>
    </table>
</asp:Content>
