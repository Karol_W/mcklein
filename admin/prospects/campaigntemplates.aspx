﻿<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.prospects.CampaignTemplates" Codebehind="campaigntemplates.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>E-Mail Campaign Templates</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Create, edit and delete the templates used for E-Mail Campaigns" /></td>
                <td class="npadminheader" align="Right">
            <asp:HyperLink runat="server" ToolTip="Add Template" ID="lnkAdd" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="templateeditor.aspx" />
    </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:gridview ID="TemplatesGrid" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
            AllowSorting="True" AllowPaging="True" ShowFooter="true" PageSize="50" CssClass="npadmintable" OnPageIndexChanging="TemplatesGrid_PageIndexChanging" OnRowDataBound="TemplatesGrid_RowDataBound" OnRowDeleting="TemplatesGrid_RowDeleting" OnSorting="TemplatesGrid_Sorting">
            <RowStyle CssClass="npadminbody" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <FooterStyle CssClass="npadminbody"></FooterStyle>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <EmptyDataRowStyle Height="50px" CssClass="npadminempty" />
            <Columns>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="delete"
                            ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"TemplateID") %>' />
                    </ItemTemplate>
                </asp:templatefield>
                <asp:boundfield ItemStyle-Width="10%" DataField="TemplateCode" HeaderText="colTemplateCode|Template Code" SortExpression="TemplateCode"></asp:boundfield>
                <asp:boundfield ItemStyle-Width="45%" DataField="TemplateName" HeaderText="colTemplateName|Template Name" SortExpression="TemplateName"></asp:boundfield>
                <asp:templatefield ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ToolTip="Edit Template" ImageUrl="~/assets/common/icons/edit.gif"
                            NavigateUrl='<%# "templateeditor.aspx?TemplateID="+DataBinder.Eval(Container.DataItem,"TemplateID")%>' ID="lnkEdit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:templatefield>
            </Columns>
        </asp:gridview>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Template"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add Template"></asp:Literal>
</asp:Content>
