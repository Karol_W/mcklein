<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.Lists" Codebehind="Lists.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Contact List Management*</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Manage contact lists used for campaigns and newsletters" /></td>
            <td class="npadminheader" align="right">
                <asp:DropDownList ID="ddlListType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlListType_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;
                <asp:HyperLink runat="server" ID="lnkNewProspect" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="ListAdd.aspx" ToolTip="Add New List" />&nbsp;</td>
        </tr>
    </table>
    <asp:GridView 
        id="gvLists"
        runat="server" 
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        EmptyDataText="No Users Found" OnRowCommand="gvLists_RowCommand" OnRowDataBound="gvLists_RowDataBound">
	    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
	    <Columns>
    	    <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/delete.gif" ID="btnDelete" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False" HeaderText="colActive|Active">
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/checked.gif" ID="btnActive" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ListName" HeaderText="colListName|List Name"></asp:BoundField>
            <asp:BoundField DataField="ServerID" HeaderText="colServerID|Theme ID"></asp:BoundField>
            <asp:BoundField DataField="AccountType" HeaderText="colAccountType|Account Type"></asp:BoundField>
            <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:HyperLink runat="server" ID="lnkEdit" ImageUrl="~/assets/common/icons/edit.gif" NavigateUrl="ListDetail.aspx" />
                </ItemTemplate>
            </asp:TemplateField>
	    </Columns>
	</asp:GridView>
    <br />
    <div class="npadminactionbar">
        <asp:CheckBox ID="cbActiveFlag" runat="server" Text="Display Inactive" AutoPostBack="true" OnCheckedChanged="cbActiveFlag_CheckedChanged"></asp:CheckBox>
    </div>
</asp:Content>
