<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.Import" Codebehind="Import.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="mapper" Src="../common/controls/mapper.ascx" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Import Prospect Data</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminbody">
                <np:mapper runat="server" ID="mapper" />
            </td>
        </tr>
    </table>
</asp:Content>
