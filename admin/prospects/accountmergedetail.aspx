<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.AccountMergeDetail" Codebehind="AccountMergeDetail.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="logsmerge" Src="controls/logsmerge.ascx" %>
<%@ Register TagPrefix="np" TagName="prospecteventsmerge" Src="controls/prospecteventsmerge.ascx" %>
<%@ Register TagPrefix="np" TagName="prospectaccountmerge" Src="~/admin/prospects/controls/prospectaccountmerge.ascx" %>
<%@ Register TagPrefix="np" TagName="prospectusermerge" Src="~/admin/prospects/controls/prospectusermerge.ascx" %>
<%@ Register TagPrefix="np" TagName="prospectaddressmerge" Src="~/admin/prospects/controls/prospectaddressmerge.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:Label ID="sysErrTop" runat="server" CssClass="npwarning"></asp:Label>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAndReturn" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save and Return" OnClick="btnSaveAndReturn_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminsubheader">&nbsp;
                <asp:Literal ID="ltlProspect" runat="server" Text="Merge Prospect"></asp:Literal>
                <asp:Literal ID="ltlAccount" runat="server" Text="to Account"></asp:Literal></td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="ProspectsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="ProspectsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="tsAccount" Text="Account" />
            <ComponentArt:TabStripTab runat="server" ID="tsUser" Text="User" />
            <ComponentArt:TabStripTab runat="server" ID="tsAddress" Text="Address" />
            <ComponentArt:TabStripTab runat="server" ID="tsActivities" Text="Activities" />
            <ComponentArt:TabStripTab runat="server" ID="tsLogs" Text="Logs" />
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="ProspectsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
        Width="100%" Height="100">
        <ComponentArt:PageView ID="pvAccountMerge" CssClass="npadminbody" runat="server">
            <np:prospectaccountmerge ID="accountmerge" runat="server" />
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvUserMerge" CssClass="npadminbody" runat="server">
            <np:prospectusermerge ID="usermerge" runat="server" />
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvAddressMerge" CssClass="npadminbody" runat="server">
        <np:prospectaddressmerge ID="addressmerge" runat="server" />
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvPem" CssClass="npadminbody" runat="server">
            <np:prospecteventsmerge ID="pem" runat="server"></np:prospecteventsmerge>
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvLgm" CssClass="npadminbody" runat="server">
            <np:logsmerge ID="lgm" runat="server"></np:logsmerge>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>  
</asp:Content>