<%@ Page ValidateRequest="false" Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.prospectnew" Codebehind="prospectnew.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="prospectgeneral" Src="~/admin/prospects/controls/prospectgeneral.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <script type="text/javascript">
        function cantPromote(message){
            alert(message);
            return false;
        }
    </script>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveAll_Click" />&nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                <asp:imagebutton runat="server" imageurl="~/assets/common/icons/indicator.gif" id="imgIndicator" tooltip=" " />&nbsp;
                <asp:literal runat="server" id="ltlGeneralHeader" text="General" />
            </td>
        </tr>
        <tr>
            <td><np:prospectgeneral ID="general" runat="server"></np:prospectgeneral></td>
        </tr>
    </table>
</asp:Content>

