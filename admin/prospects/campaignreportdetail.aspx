<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.CampaignReportDetail" Codebehind="CampaignReportDetail.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">&nbsp;</td>
        </tr>
    </table>
    <asp:GridView ID="gridLogs" runat="server" AllowSorting="True" AllowPaging="True"
        PageSize="25" AutoGenerateColumns="False" CssClass="npadmintable" EmptyDataText="No Records Found" OnRowDataBound="gridLogs_RowDataBound" 
        OnPageIndexChanging="gridLogs_PageIndexChanging" OnSorting="gridLogs_Sorting" >
        <HeaderStyle CssClass="npadminsubheader" />
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <Columns>
            <asp:TemplateField HeaderText="colType|Action" SortExpression="LogType">
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblAction" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colName|Name" SortExpression="">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblName" />
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="colEmail|Email" SortExpression="Email">
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <asp:HyperLink runat="server" ID="lnkEmail" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colMessage|Message" SortExpression="Subject" Visible="false">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblSubject" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colDate|Date" SortExpression="TmStmp">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TimeStamp") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                <HeaderStyle Width="20%" />
                <ItemTemplate>
                    <asp:Label ID="lblTimeStamp" runat="server" Text='<%# Bind("TimeStamp", "{0:yyyy-MMM-dd hh:mm}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="npadminbody" />
    </asp:GridView>
</asp:Content>
