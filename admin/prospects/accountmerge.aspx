<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.AccountMerge" Codebehind="AccountMerge.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:Image ID="imgProspectIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                <asp:Literal ID="ltlMergeData" runat="server" Text="Merge Data"></asp:Literal></td>
            <td class="npadminsubheader" align="right">
                <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete Prospect" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDelete_Click" />&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="sysName" runat="server"></asp:Literal></td>
            <td class="npadminlabel">
                <table class="npadminlabel" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td><asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
                        <td><asp:Literal ID="sysEmail" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><asp:Literal ID="ltlMainPhone" runat="server" Text="Main Phone"></asp:Literal></td>
                        <td><asp:Literal ID="sysMain" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><asp:Literal ID="ltlOffice" runat="server" Text="Office"></asp:Literal></td>
                        <td><asp:Literal ID="sysOffice" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><asp:Literal ID="ltlHome" runat="server" Text="Home"></asp:Literal></td>
                        <td><asp:Literal ID="sysHome" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><asp:Literal ID="ltlFax" runat="server" Text="Fax"></asp:Literal></td>
                        <td><asp:Literal ID="sysFax" runat="server"></asp:Literal></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlMatch" runat="server">
                    <table class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td class="npadminsubheader">
                                <asp:Image ID="imgMatchIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                                <asp:Literal ID="ltlMatch" runat="server" Text="Possible Matches with Existing Account Data"></asp:Literal></td>
                            <td class="npadminsubheader" align="right">
                                <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                                    ToolTip="Create New Business Partner from Prospect" OnClick="btnNew_Click" />&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:GridView ID="gridMerge" runat="server" PageSize="50" AllowPaging="True" AutoGenerateColumns="False"
                                    CssClass="npadmintable" EmptyDataText="No Matches Found" OnPreRender="gridMerge_PreRender" OnRowDataBound="gridMerge_RowDataBound">
                                    <HeaderStyle CssClass="npadminsubheader" />
                                    <RowStyle CssClass="npadminbody" />
                                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="colID|ID">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkAccountID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>'
                                                    NavigateUrl="~/admin/common/accounts/accountinfo.aspx?accountid=">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer|Customer">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkAccountName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccountName") %>'
                                                    NavigateUrl="~/admin/common/accounts/accountinfo.aspx?accountid=">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="colAccountData|Account Data">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlMergeSubject" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkMerge" runat="server" ImageUrl="~/assets/common/icons/merge.gif"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="numeric" Position="Bottom" />
                                    <PagerStyle CssClass="npadminbody" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlNew" runat="server" Visible="false">
                    <table cellspacing="0" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td class="npadminsubheader" colspan="3"><asp:Literal ID="ltlNewID" runat="server" Text="New IDs"></asp:Literal></td>
                            <td class="npadminsubheader" align="right">
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSave_Click" ToolTip="Save" /></td>
                        </tr>
                        <tr>
                            <td class="npadminlabel"><asp:Literal ID="ltlAccountID" runat="server" Text="Account ID"></asp:Literal></td>
                            <td class="npadminbody">
                                <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAccountID" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtAccountID">
									<asp:image runat="server" imageurl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator></td>
                            <td class="npadminlabel"><asp:Literal ID="ltlUserID" runat="server" Text="User ID"></asp:Literal></td>
                            <td class="npadminbody">
                                <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUserID" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtUserID">
									<asp:Image runat="server" imageurl="~/assets/common/icons/warning.gif" BorderWidth="0"/>
                                </asp:RequiredFieldValidator></td>
                        </tr>
                    </table>
                    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnMerge" runat="server" Text="Merge Prospect to Account" Visible="False"></asp:Literal>
    <asp:Literal ID="errAccountExists" runat="server" Text="This account ID already exists. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="errUserExists" runat="server" Text="This user ID already exists. Continue with merge?" Visible="False"></asp:Literal>
    <asp:Literal ID="errNoAccountName" runat="server" Text="You must enter a first name, last name, or company name for the prospect before a business partner can be created" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDefaultAddressName" runat="server" Text="Main Address" Visible="False"></asp:Literal>
    <asp:Literal ID="errDelete" runat="server" Text="The prospect and all activities, subscriptions, and logs will be deleted. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAddressProspect" runat="server" Text="Address Created from Prospect " Visible="false"></asp:Literal>
    <asp:Literal ID="hdnUserProspect" runat="server" Text="User Created from Prospect" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnNewBP" runat="server" Text="New Business Partner Data" Visible="false"></asp:Literal>
</asp:Content>
