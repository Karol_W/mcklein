<%@ Page ValidateRequest="false" MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.ListDetail" Codebehind="ListDetail.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="listgeneral" Src="~/admin/prospects/controls/listgeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="listsubscribers" Src="~/admin/prospects/controls/listsubscribers.ascx" %>
<%@ Register TagPrefix="np" TagName="listbulkadd" Src="~/admin/prospects/controls/listbulkadd.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveAll_Click" ToolTip="Save All" />&nbsp;</td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="ListsTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="ListsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Subscribers"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Bulk Add"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="ListsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="100">
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:listgeneral ID="listgeneral" runat="server"></np:listgeneral>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:listsubscribers ID="listsubscribers" runat="server"></np:listsubscribers>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
           <np:listbulkadd ID="listbulkadd" runat="server"></np:listbulkadd>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
