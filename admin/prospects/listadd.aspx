<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.ListAdd" Codebehind="ListAdd.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminheader">
                <asp:HyperLink ID="lnkCancel" runat="server" ToolTip="Cancel" ImageUrl="~/assets/common/icons/cancel.gif"
                    NavigateUrl="Lists.aspx"></asp:HyperLink>&nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Add a Contact List"></asp:Literal></td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="btnSaveAndReturn" runat="server" 
                ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save and Return" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
                        <td><asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList>
                            <asp:CheckBox ID="cbActiveFlag" runat="server" Text="Active" Checked="true"></asp:CheckBox>
                            <asp:CheckBox ID="cbDoNotCallFlag" runat="server" Text="Do Not Call"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
                        <td><asp:TextBox ID="tbListName" runat="server" Columns="35"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="List name is required" ControlToValidate="tbListName">
                            <asp:Image runat="server" ID="wrn1" ImageUrl="~/assets/common/icons/warning.gif" BorderWidth="0" /></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlTheme" runat="server" Text="Theme"></asp:Literal></td>
                        <td><asp:DropDownList ID="ddlServerID" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;" align="right"><asp:Literal ID="ltlAccountType" runat="server" Text="Account Type"></asp:Literal></td>
                        <td><asp:DropDownList ID="ddlAccountType" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
