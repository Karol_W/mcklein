<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.prospects.Campaigns" Codebehind="Campaigns.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>E-Mail Campaign Management*</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Create, edit and send E-Mail Campaigns" /></td>
        </tr>
        <tr>
            <td class="npadminheader" align="Left">
                <asp:Label ID="lblOwner" Text="Owner" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlOwnerUserID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOwnerUserID_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminheader" align="Left">
                <asp:Label ID="lblStatus" Text="Status" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;</td>
            <td class="npadminheader" align="Right">
            <asp:HyperLink runat="server" ID="lnkNewProspect" ImageUrl="~/assets/common/icons/add.gif"
                NavigateUrl="campaignadd.aspx" ToolTip="Add New Campaign" />&nbsp;</td>
        </tr>
    </table>
    <asp:GridView ID="gvCampaigns" runat="server" CssClass="npadmintable" AutoGenerateColumns="False" OnRowCommand="gvCampaigns_RowCommand" OnRowDataBound="gvCampaigns_ItemRowBound" EmptyDataText="No Campaigns Found">
        <AlternatingRowStyle CssClass="npadminbodyalt"></AlternatingRowStyle>
        <RowStyle CssClass="npadminbody"></RowStyle>
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <Columns>
            <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False" HeaderText="Active|Active">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/checked.gif" ID="btnActive" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colName|Name">
                <ItemTemplate>
                    <b> <%# DataBinder.Eval(Container.DataItem, "Name") %></b>
                    <br />
                    <%# DataBinder.Eval(Container.DataItem, "Description") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Duplicate" HeaderStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDuplicate" runat="server" ImageUrl="~/assets/common/icons/duplicate.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="OwnerUserID" HeaderText="colOwner|Owner"></asp:BoundField>
            <asp:TemplateField HeaderText="colStart|Sent On" Visible = "false">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("StartDate", "{0:yyyy-MMM-dd}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                <itemstyle horizontalalign="Center"></itemstyle>
                <itemtemplate>
                    <asp:HyperLink runat="server" ID="lnkEdit" ImageUrl="~/assets/common/icons/edit.gif" NavigateUrl="CampaignDetail.aspx" />
                </itemtemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="View Report" HeaderStyle-HorizontalAlign="Center">
                <itemstyle horizontalalign="Center"></itemstyle>
                <itemtemplate>
                    <asp:HyperLink runat="server" ImageUrl="~/assets/common/icons/reports.gif" NavigateUrl="CampaignReport.aspx" ID="lnkReports" ToolTip="Report" />
                </itemtemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle HorizontalAlign="Right" CssClass="npadminlabel"></PagerStyle>
    </asp:GridView>
    <br />
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errDeletionsArePermanent" runat="server" Text="Deletions are permanent. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="errDuplication" runat="server" Text="This will duplicate the selected campaign. Continue?" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Text="Delete" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDuplicate" runat="server" Text="Duplicate" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnReport" runat="server" Text="Report" Visible="false"></asp:Literal>
</asp:Content>
