<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.search" CodeBehind="search.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:MultiView runat="server" ID="mvSearch">
        <asp:View runat="server" ID="vEntry">
            <table class="npadmintable">
                <tr>
                    <td align="center" class="npadminbody"><br />
                        <asp:TextBox runat="server" ID="tbSearchText" Columns="30"></asp:TextBox>
                        <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search" /><br /></td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server" ID="vResults">
            <asp:GridView runat="server" ID="gvResults" ShowHeader="True" AutoGenerateColumns="False" CssClass="npadmintable"
                OnRowDataBound="gvResults_RowDataBound" OnRowCommand="gvResults_RowCommand" EmptyDataText="No Matching Results">
                <RowStyle CssClass="npadminbody" />
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                <HeaderStyle CssClass="npadminsubheader" />
                <EmptyDataRowStyle CssClass="npadminempty" height="50px" />
                <Columns>
                    <asp:TemplateField HeaderText="colResults|Results">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnResult" runat="server"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View runat="server" ID="vDetails">
            <asp:GridView runat="server" ID="gvDetails" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" 
                AllowSorting="True" CssClass="npadmintable" OnRowDataBound="gvDetails_RowDataBound" EmptyDataText="No Details Found" 
                OnPageIndexChanging="gvDetails_PageIndexChanging">
                <RowStyle CssClass="npadminbody" />
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                <HeaderStyle CssClass="npadminsubheader" />
                <EmptyDataRowStyle CssClass="npadminempty" height="50px" />
                <Columns>
                    <asp:TemplateField HeaderText="colID|ID">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkID" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colName|Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Literal ID="ltlSelectedResult" runat="server" Visible="false" Text="SelectedResult"></asp:Literal>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:HiddenField runat="server" ID="hdnPart" Value="Parts" />
    <asp:HiddenField runat="server" ID="hdnAccount" Value="Accounts" />
    <asp:HiddenField runat="server" ID="hdnUser" Value="Users" />
    <asp:HiddenField runat="server" ID="hdnEvent" Value="Events" />
    <asp:HiddenField runat="server" ID="hdnProspect" Value="Prospects" />
    <asp:HiddenField runat="server" ID="hdnOpportunity" Value="Opportunities" />
    <asp:HiddenField runat="server" ID="hdnSolution" Value="Solutions" />
    <asp:HiddenField runat="server" ID="hdnWorkOrder" Value="Work Orders" />
    <asp:HiddenField runat="server" ID="hdnTicket" Value="Support Tickets" />
</asp:Content>
