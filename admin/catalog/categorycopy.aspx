<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.CatalogCategoryCopy" Codebehind="categorycopy.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:label id="lblBreadCrumbs" text="Commerce > Catalog Browser > Category Info > Copy Category" cssclass="npbody" runat="server"></asp:label>
    <table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminsubheader" align="left"><asp:hyperlink id="lnkCancel" runat="server" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:hyperlink></td>
            <td class="npadminheader"><asp:literal id="ltlHeader" runat="server"></asp:literal></td>
            <td class="npadminheader" align="right"><asp:imagebutton id="btnSave" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" ToolTip="Save"></asp:imagebutton>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2"><asp:literal id="ltlOrigCategoryCode" runat="server" text="Original Code"></asp:literal></td>
            <td><asp:label id="lblOrigCategoryCode" runat="server" Text="Category Code"></asp:label></td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2"><asp:literal id="ltlOrigCategoryName" runat="server" text="Original Name"></asp:literal></td>
            <td><asp:label id="lblOrigCategoryName" runat="server" Text="Category Name"></asp:label></td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2"><asp:literal id="ltlCatalog" runat="server" text="Catalog"></asp:literal></td>
            <td><asp:dropdownlist id="ddlCatalogID" runat="server"></asp:dropdownlist></td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2"><asp:literal id="ltlCategoryCode" runat="server" text="New Code"></asp:literal></td>
            <td><asp:textbox id="txtCategoryCode" runat="server" columns="20"></asp:textbox>
                <asp:requiredfieldvalidator id="rfvCode" runat="server" controltovalidate="txtCategoryCode" errormessage="RequiredFieldValidator">
				<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator></td>
        </tr>
        <tr>
            <td class="npadminlabel" colspan="2"><asp:literal id="ltlCascade" runat="server" text="Copy Subcategories"></asp:literal></td>
            <td><asp:checkbox id="sysCascade" runat="server" checked="true"></asp:checkbox></td>
        </tr>
    </table>
    <asp:label id="sysError" runat="server" font-size="Larger" font-bold="True" forecolor="Red"></asp:label>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnCopyCategoryData" runat="server" text="Copy Category Data From" visible="False"></asp:literal>
    <asp:Literal ID="errDuplicateCategory" runat="server" Text="Category code already exists; choose another" Visible="false"></asp:Literal>
</asp:Content>
