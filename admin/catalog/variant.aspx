<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/admin.Master"
    Codebehind="variant.aspx.cs" Inherits="netpoint.admin.catalog.variant" %>

<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/partpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="Variant" Src="~/admin/catalog/controls/variant.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="Server" CssClass="NPWarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                <asp:HyperLink ID="lnkCatalogDefault" runat="server" Text="Catalog" NavigateUrl="~/admin/catalog/default.aspx"></asp:HyperLink>
                >
                <asp:HyperLink ID="lnkParts" runat="server" Text="Items" NavigateUrl="~/admin/catalog/parts.aspx"></asp:HyperLink>
                >
                <asp:HyperLink ID="lnkPartDetail" runat="server" Text="Item Detail" NavigateUrl="~/admin/catalog/part.aspx"></asp:HyperLink>
                >
                <asp:Literal ID="ltlVariant" runat="server" Text="Variant"></asp:Literal>
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader">
                &nbsp;
                <asp:Image ID="sysIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
                <asp:Literal ID="ltlMasterPart" runat="server" Text="Master Item"></asp:Literal>:
                <asp:Literal ID="sysPart" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gridVariant" runat="server" AutoGenerateColumns="false" CssClass="npadmintable"
                    ShowHeader="false" OnRowDataBound="gridVariant_RowDataBound" 
                    EmptyDataText="No Categories Defined for Variant">
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <HeaderStyle CssClass="npadminlabel" />
                    <RowStyle CssClass="npadminbody" />
                    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <np:Variant ID="variant" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel" rowspan="2" style="width:10px" align="center">
                <b><asp:Literal ID="ltlAddCategory" runat="server" Text="Add Category"></asp:Literal></b>
            </td>
            <td class="npadminlabel" rowspan="2">
                <asp:Literal ID="ltlCategoryName" runat="server" Text="Category Name"></asp:Literal>
                <br />
                <asp:TextBox ID="txtVariantDesc" runat="server"></asp:TextBox>
            </td>
            <td class="npadminlabel" rowspan="2">
                <asp:Literal ID="ltlVariantType" runat="server" Text="Type"></asp:Literal>
                <br />
                <asp:DropDownList ID="ddlVariantType" runat='server'>
                </asp:DropDownList>
            </td>
            <td class="npadminlabel" rowspan="2" style="white-space: nowrap">
                <asp:Literal ID="ltlDefaultPart" runat="server" Text="Default Item"></asp:Literal>
                <br />
                <np:PartPicker ID="ppkDefaultPart" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:ImageButton ID="btnAddVariant" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                    OnClick="btnAddVariant_Click" ToolTip="Add Variant" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnRemove" runat="server" Text="Remove Category" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit Category" Visible="false"></asp:Literal>
    <asp:Literal ID="errAlreadyInCategory" runat="server" Text="The item is already a member of the category" Visible="false"></asp:Literal>
    <asp:literal ID="errItemIsBOM" runat="server" Text="A Bill of Materials and can not be a member of a variant." Visible="False"></asp:literal>
    <asp:Literal ID="errItemIsVariant" runat="server" Text="A variant can not be a member of a variant." Visible="False"></asp:Literal>
</asp:Content>
