<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.PartList"
    Codebehind="PartList.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink ID="lnkUpLevel" runat="server" ImageUrl="~/assets/common/icons/uplevel.gif"
                    NavigateUrl="~/admin/catalog/CatalogList.aspx" ToolTip="UpLevel" />
                &nbsp;
                <asp:Literal runat="server" ID="CategoryName" Text="Category Name" />
                &nbsp;
                <asp:HyperLink ID="lnkAdd" runat="server" NavigateUrl="PartNew.aspx?categoryid=" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add New Item"></asp:HyperLink>
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:GridView ID="gridParts" runat="server" CssClass="npadmintable" AutoGenerateColumns="False"
        AllowSorting="True" AllowPaging="True" ShowFooter="false" PageSize="50" OnRowDataBound="gridParts_RowDataBound" 
        OnSorting="gridParts_Sorting" OnPageIndexChanging="gridParts_PageIndexChanging" EmptyDataText="No Items Found in Specified Category">
        <RowStyle CssClass="npadminbody" />
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <HeaderStyle CssClass="npadminsubheader" />
        <PagerStyle CssClass="npadminbody" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <Columns>
            <asp:TemplateField HeaderText="colAvail|Avail." ItemStyle-HorizontalAlign="center" SortExpression="AvailableFlag">
                <ItemTemplate>
                    <asp:Image runat="server" ID="AvailableFlag" ImageUrl="~/assets/common/icons/checked.gif"
                        Visible='<%# DataBinder.Eval(Container.DataItem, "AvailableFlag").Equals("Y") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ItemNo|Item No." SortExpression="pm.PartNo">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkPart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartNo") %>'>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name" SortExpression="PartName" />
            <asp:TemplateField HeaderText="colIsMaster|Is Master" ItemStyle-HorizontalAlign="center" SortExpression="ProductLineFlag">
                <ItemTemplate>
                    <asp:Image runat="server" ID="ProductLineFlag" ImageUrl="~/assets/common/icons/checked.gif"
                        Visible='<%# DataBinder.Eval(Container.DataItem, "ProductLineFlag").Equals("Y") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="colChildOf|Child Of" ItemStyle-HorizontalAlign="center" SortExpression="ProductLinePartNo">
                <ItemTemplate>
                    <a href='Part.aspx?partno=<%# DataBinder.Eval(Container, "DataItem.ProductLinePartNo") %>&categoryid=<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>'
                        id="lnkProdLine">
                        <%# DataBinder.Eval(Container, "DataItem.ProductLinePartNo") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
