<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.catalog.PartVariant" Codebehind="PartVariant.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" CssClass="NPWarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp; </td>
            <td class="npadminheader" align="right">&nbsp;&nbsp;
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                ToolTip="Save All and Close" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminheader">
                <asp:Literal ID="sysPart" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="VariantTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="VariantMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="Category Header"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsParts" Text="Parts"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsText" Text="Additional Header Text"></ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLanguage" Text="Language Strings"></ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="VariantMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlCategoryName" runat="server" Text="Category Name"></asp:Literal></td>
                        <td >
                            <asp:TextBox ID="txtVariant" runat="server"  Columns="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtVariant">
								<img src='/assets/common/icons/warning.gif' alt="" /></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlDefault" runat="server" Text="Default Item"></asp:Literal></td>
                        <td><np:PartPicker ID="ppkDefaultPartNo" runat="server" Width="400px"></np:PartPicker></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlImageURL" runat="server" Text="Variant Image"></asp:Literal></td>
                        <td ><np:FilePicker ID="fpk" runat="server" Width="400px" StartDirectory="~/assets/catalog/parts/"></np:FilePicker></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal></td>
                        <td><asp:TextBox ID="txtSortOrder" runat="server" Columns="7">0</asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlVariantType" runat="server" Text="Type"></asp:Literal></td>
                        <td><asp:DropDownList ID="ddlVariantType" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlRequired" runat="server" Text="Required"></asp:Literal></td>
                        <td><asp:CheckBox ID="sysRequired" runat="server"></asp:CheckBox></td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvParts">
                  <asp:gridview
                    id="gvVariants"
                    runat="server" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-CssClass="npadminsubheader"
                    CssClass="npadmintable" 
                    RowStyle-CssClass="npadminbody" 
                    AlternatingRowStyle-CssClass="npadminbodyalt" 
                    EmptyDataText="No Records Found" OnRowCommand="gvVariants_RowCommand" OnRowDataBound="gvVariants_RowDataBound">
                    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                                    CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PartsVariantID") %>'>
                                </asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colAvail|Avail.">
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="imgAvailable" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                                    ToolTip="Part is Currently Available"></asp:Image>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item|Item">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkPart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Part.PartNo") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ItemName|Item Name">
                            <ItemTemplate>
                                <asp:Literal ID="ltlPart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Part.PartName") %>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colPriceFromDefault|Price from Default">
                            <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="imgUseBaseFlag" runat="server" ImageUrl="~/assets/common/icons/checked.gif"
                                    Visible='<%# DataBinder.Eval(Container.DataItem, "UseBaseFlag") %>'></asp:Image>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colPriceAdj|Price Adj.">
                            <ItemTemplate>
                                <asp:Literal ID="ltlBaseAdj" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BaseAdj") %>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colSort|Sort">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Literal ID="ltlSort" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SortOrder") %>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PartsVariantID") %>'
                                    CommandName="editthis"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:gridview>
                <table width="100%">
                    <tr>
                        <td align="right"><asp:ImageButton ID="btnAdd" ImageUrl="~/assets/common/icons/add.gif" runat="server" OnClick="btnAdd_Click" ToolTip="Add"></asp:ImageButton></td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvText">
                <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvStrings">
                <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errDelete" runat="server" Visible="False" Text="Deletions are permanent. \n Deleting this category will remove all sub-items from existing configured items that are currently in carts (not ordered). \n Continue?"></asp:Literal>
    <asp:Literal ID="hdnCategoriesIn" runat="server" Visible="False" Text="Categories In"></asp:Literal>
    <asp:Literal ID="errMustBeNumber" runat="server" Visible="False" Text="You must enter a number"></asp:Literal>
    <asp:Literal ID="hdnConfigurablePart" runat="server" Visible="False" Text="Configurable Item"></asp:Literal>
    <asp:Literal ID="hdnRemove" runat="server" Visible="False" Text="Remove this item from the category"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Display and Price Characteristics"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add Item to Variant Category"></asp:Literal>
    <asp:Literal ID="hdnPartAvailable" runat="server" Visible="False" Text="Item Currently Available"></asp:Literal>
    <asp:Literal ID="hdnPriceDefault" runat="server" Visible="False" Text="Add difference of default item and this item to variant price"></asp:Literal>
    <asp:Literal ID="hdnDeleteVariant" runat="server" Visible="False" Text="Delete Variant Category"></asp:Literal>
    <asp:Literal ID="hdnMarketingText" runat="server" Visible="false" Text="Marketing Text"></asp:Literal>
    <asp:Literal ID="hdnCategoryName" runat="server" Visible="false" Text="Category Name"></asp:Literal>
    <asp:Literal ID="errAlreadyInCategory" runat="server" Text="The item is already a member of the category" Visible="false"></asp:Literal>
    <asp:literal ID="errItemIsBOM" runat="server" Text="A Bill of Materials and can not be a member of a variant." Visible="False"></asp:literal>
    <asp:Literal ID="errItemIsVariant" runat="server" Text="A variant can not be a member of a variant." Visible="False"></asp:Literal>
</asp:Content>
