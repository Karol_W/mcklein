<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.Warehouse" Codebehind="Warehouse.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
<tr><td>
	<asp:gridview
        id="gvWarehouse"
        runat="server" 
        AutoGenerateColumns="false" 
        HeaderStyle-CssClass="npadminsubheader"
        CssClass="npadmintable" 
        RowStyle-CssClass="npadminbody" 
        AlternatingRowStyle-CssClass="npadminbodyalt" 
        EmptyDataText="No Records Found" OnRowCommand="gvWarehouse_RowCommand" OnRowDataBound="gvWarehouse_RowDataBound">
        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
        <Columns>
            <asp:TemplateField>
				<ItemStyle HorizontalAlign="Center"></ItemStyle>
				<ItemTemplate>
					<asp:ImageButton id="btnDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.WarehouseID") %>' ImageUrl="../../assets/common/icons/delete.gif" CommandName="remove" ToolTip="Delete This Warehouse.">
					</asp:ImageButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="WarehouseName" HeaderText="colWarehouseName|Warehouse Name"></asp:BoundField>
			<asp:BoundField DataField="City" HeaderText="colCity|City"></asp:BoundField>
			<asp:BoundField DataField="State" HeaderText="colState|State"></asp:BoundField>
			<asp:BoundField DataField="Country" HeaderText="colCountry|Country"></asp:BoundField>
			<asp:TemplateField HeaderText="colDefault|Default">
				<ItemStyle HorizontalAlign="Center"></ItemStyle>
				<ItemTemplate>
					<asp:Image id="imgDefault" runat="server" ImageUrl="~/assets/common/icons/checked.gif"></asp:Image>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<HeaderStyle Width="5%"></HeaderStyle>
				<ItemStyle HorizontalAlign="Center"></ItemStyle>
				<ItemTemplate>
					<asp:ImageButton id="btnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "WarehouseID") %>' ImageUrl="../../assets/common/icons/edit.gif" CommandName="editthis">
					</asp:ImageButton>
				</ItemTemplate>
			</asp:TemplateField>
        </Columns>
    </asp:gridview>
    </td>
    </tr>
        <tr>
            <td align="right"><asp:ImageButton ID="btnAdd" ImageUrl="~/assets/common/icons/add.gif" runat="server" OnClick="btnAdd_Click" ToolTip="Add">
                    </asp:ImageButton></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="hdnDeleteWarehouseWarning" runat="server" text="This will delete all inventory associated with this warehouse. Continue?"
        visible="False"></asp:literal>
    <asp:literal id="hdnDelete" runat="server" text="Delete" visible="False"></asp:literal>
    <asp:literal id="hdnAdd" runat="server" text="Add Warehouse" visible="False"></asp:literal>
    <asp:literal id="hdnEdit" runat="server" text="Edit Warehouse" visible="False"></asp:literal>
    <asp:literal id="hdnDefault" runat="server" visible="False" text="Default Warehouse"></asp:literal>
</asp:Content>
