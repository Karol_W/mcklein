<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.Manufacturer" ValidateRequest="false" Codebehind="Manufacturer.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete" OnClick="btnDeleteGeneral_Click" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close" OnClick="btnSave_Click" />&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="5"></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlMNFCode" runat="server" Text="MNF Code"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtMNFCode" runat="server"></asp:TextBox></td>
                <td class="npadminlabel"><asp:Literal ID="ltlMNFName" runat="server" Text="MNF Name"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtMNFName" runat="server"></asp:TextBox></td>
                <td class="npadminbody" valign="middle" rowspan="4"><asp:Image ID="imgLogo" runat="server"></asp:Image></td>
            </tr>
            <tr>
                <td class="npadminlabel" style="height: 26px">
                    <asp:Literal ID="ltlLogo" runat="server" Text="Logo"></asp:Literal></td>
                <td class="npadminbody" colspan="3" style="height: 26px">
                    <np:FilePicker ID="fpMNFLogo" runat="server" StartDirectory="~/assets/catalog/manufacturers/" Width="350px"></np:FilePicker></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlWebsite" runat="server" Text="Display Web Site"></asp:Literal></td>
                <td class="npadminbody" colspan="3"><asp:TextBox ID="txtMNFWebsite" runat="server" Width="350px"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
                <td class="npadminbody" colspan="3"><NPWC:MultiLineTextBox ID="txtMNFDescription" runat="server" Rows="5" TextMode="MultiLine" Columns="45" LimitInput="254"></NPWC:MultiLineTextBox></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="This will set the &quot;Manufacturer&quot; field\n to empty on all associated items. Continue?"></asp:Literal>
</asp:Content>
