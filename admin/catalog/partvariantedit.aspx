<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#"
    Inherits="netpoint.admin.catalog.PartVariantEdit" Codebehind="PartVariantEdit.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="CultureObjectStrings" Src="../common/controls/CultureObjectStrings.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="../common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ register tagprefix="ComponentArt" namespace="ComponentArt.Web.UI" assembly="ComponentArt.Web.UI" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements: "*[*]"
    });
</script>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <asp:Label ID="sysError" runat="server" cssclass="npwarning"></asp:Label>
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                &nbsp;&nbsp;
                <asp:ImageButton ID="btnSaveAndReturn" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save and Return" OnClick="btnSaveAndReturn_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="npadminheader">
                <asp:Literal ID="sysPart" runat="server"></asp:Literal>                
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="VariantTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="VariantMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsPart" Text="Item">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsPricing" Text="Pricing">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsText" Text="Marketing Text">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLanguage" Text="Language Strings">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="VariantMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvGeneral">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlPartNo" runat="server" Text="Item"></asp:Literal>
                        </td>
                        <td>
                            <np:PartPicker ID="ppk" runat="server"></np:PartPicker>
                            <asp:Image ID="imgNoPart" runat="server" ImageUrl="~/assets/common/icons/warning.gif"
                                Visible="False"></asp:Image>
                        </td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSortOrder" runat="server" Columns="7" Width="75px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlPartName" runat="server" Text="Item Name"></asp:Literal>
                        </td>
                        <td colspan="3" class="npadminbody">
                            <asp:Literal ID="sysPartName" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvPricing">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlCategoryDefault" runat="server" Text="Default Item"></asp:Literal>
                        </td>
                        <td class="npadminbody">
                            <asp:Literal ID="sysDefault" runat="server"></asp:Literal>
                        </td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlDefaultPartPrice" runat="server" Text="Default Base Price"></asp:Literal>
                        </td>
                        <td align="right" class="npadminbody">
                            <np:PriceDisplay ID="sysDefaultPrice" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlUseBasePrice" runat="server" Text="Price from Default"></asp:Literal>
                        </td>
                        <td class="npadminbody">
                            <asp:CheckBox ID="sysUseBasePrice" runat="server"></asp:CheckBox>
                        </td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlBasePrice" runat="server" Text="Base Price"></asp:Literal>
                        </td>
                        <td align="right" class="npadminbody">
                            <np:PriceDisplay ID="sysBasePrice" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlPriceAdj" runat="server" Text="Price Adjustment"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPriceAdj" runat="server">
                            </asp:TextBox>
                        </td>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlPriceDifference" runat="server" Text="Price Difference"></asp:Literal>
                        </td>
                        <td align="right" class="npadminbody">
                            <asp:Panel ID="pnlPriceDifference" runat="server">
                                [<asp:Literal ID="ltlDiffType" runat="server" /> <np:PriceDisplay ID="prcDiff" runat="server" />]
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvText">
                <asp:TextBox ID="Content" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvStrings">
                <uc1:CultureObjectStrings ID="cos" runat="server" ></uc1:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errMustBeNumber" runat="server" Text="You must enter a number" Visible="False">
    </asp:Literal>
    <asp:Literal ID="hdnDeleteVariant" runat="server" Text="Remove Item from Category"
        Visible="False"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnNone" runat="server" Text="None" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Text="Add" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnSubtract" runat="server" Text="Subtract" Visible="False"></asp:Literal>
    <asp:Literal ID="errPartAdded" runat="server" Text="This item has already been added to the category"
        Visible="False"></asp:Literal>
    <asp:Literal ID="hdnMarketingText" runat="server" Text="Marketing Text" Visible="false"></asp:Literal>
    <asp:literal ID="errItemIsBOM" runat="server" Text="A Bill of Materials and can not be a member of a variant." Visible="False"></asp:literal>
    <asp:Literal ID="errItemIsVariant" runat="server" Text="A variant can not be a member of a variant." Visible="False"></asp:Literal>
</asp:Content>
