<%@ Page Language="C#" MasterPageFile="~/masters/admin.master" AutoEventWireup="true" Inherits="netpoint.admin.catalog.parts" Codebehind="parts.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="SearchBuilder" Src="~/common/controls/SearchBuilder.ascx" %>
<%@ Register TagPrefix="np" TagName="SearchSetList" Src="~/common/controls/SearchSetList.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Src="~/common/controls/npdatepicker.ascx" TagName="DatePicker" TagPrefix="np" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Search for Items</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" ID="main" runat="server">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:HyperLink runat="server" ID="lnkNew" ImageUrl="~/assets/common/icons/add.gif" NavigateUrl="~/admin/catalog/partnew.aspx" ToolTip="New" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npwarning">
                <asp:Literal ID="ltlNoCats" runat="server" Visible="false" Text="There are no item categories, to create a item you must first create a category."></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminsubheader">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpFilter"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="filter" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expFilter"
                    OnCommand="Expand" ToolTip="Expand" CommandName="filter" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlFilterHeader" Text="Save or edit a filter set" />&nbsp;
                <np:SearchSetList runat="server" ID="SearchSetList" /></td>
            <td class="npadminsubheader" align="right">&nbsp;
                <asp:ImageButton runat="server" ID="btnReset" ImageUrl="~/assets/common/icons/undo.png" OnClick="btnReset_Click" ToolTip="Reset" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody"><np:SearchBuilder runat="server" ID="npsb"></np:SearchBuilder></td>
        </tr>
        <tr>
            <td class="npadminsubheader" colspan="1">
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/collapse.gif" ID="clpResults"
                    OnCommand="Collapse" ToolTip="Collapse" CommandName="results" Visible="false" />
                <asp:ImageButton runat="server" ImageUrl="~/assets/common/icons/expand.gif" ID="expResults"
                    OnCommand="Expand" ToolTip="Expand" CommandName="results" Visible="false" />&nbsp;
                <asp:Literal runat="server" ID="ltlResultsHeader" Text="Results" Visible="false" /></td>
            <td class="npadminsubheader" colspan="1" align="right">
<%--                <asp:Literal ID="ltlMaxRows" runat="server" Text="Maximum number of items to return" />
                <asp:DropDownList runat="Server" ID="ddlMaxRows"></asp:DropDownList>&nbsp;--%>
                <asp:ImageButton runat="server" ID="btnFind" CssClass="SearchButton" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnFind_Click" ToolTip="Find" />&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="npadminbody">
                <asp:Panel runat="Server" ID="pnlResults" Visible="false">

                    <ComponentArt:Grid id="gvResults" DataAreaCssClass="Grid"
                            ClientScriptLocation="~/scripts"
                            AutoFocusSearchBox="false"
                            AutoTheming="true"
                            RunningMode="Callback"
                            ShowHeader="true" 
                            ShowFooter="true"
                            ShowSearchBox="true"
                            SearchOnKeyPress="true"
                            PageSize="20" 
                            PagerStyle="Numbered" 
                            FooterCssClass="GridFooter"
                            HeaderCssClass="GridHeader"
                            SliderPopupOffsetX="72"
                            GroupingPageSize="5"
                            ImagesBaseUrl="~/assets/common/admin/"
                            PagerImagesFolderUrl="~/assets/common/admin/pager/" 
                            LoadingPanelEnabled="false"
                            GroupingNotificationText="Drag a column to this area to group by it."
                            runat="server"
                            AutoAdjustPageSize="true" 
                            AutoSortOnGroup="true" 
                            Width="100%"
                            CollapseImageUrl="~/assets/common/admin/topItem_col.gif" 
                            ExpandImageUrl="~/assets/common/admin/topItem_exp.gif" 
                            GroupingCountHeadingsAsRows="false" 
                            AllowColumnResizing="true">
                        <Levels>
                          <ComponentArt:GridLevel DataKeyField="PartNo" 
                                AllowReordering="true"   
                                ColumnGroupIndicatorImageUrl=""  
                                RowCssClass="Row" 
                                ShowSelectorCells="true" 
                                AllowGrouping= "true" 
                                SortAscendingImageUrl="~/assets/common/admin/asc.gif" 
                                SortDescendingImageUrl="~/assets/common/admin/desc.gif" 
                                HeadingTextCssClass="HeadingCellText" 
                                HeadingCellCssClass="HeadingCell" 
                                DataCellCssClass="DataCell" 
                                SortedDataCellCssClass="SortedDataCell" 
                                GroupHeadingCssClass="GroupHeading" 
                                AlternatingRowCssClass="AltRow" 
                                HoverRowCssClass="SelectedRow">
                            <Columns>
                              <ComponentArt:GridColumn DataField="PartNo" HeadingText="ItemNo|Item No." DataCellServerTemplateId="PartNoTemplate"/>
                              <ComponentArt:GridColumn DataField="PartName" HeadingText="ItemName|Item Name" DataCellServerTemplateId="PartNameTemplate"/>
                              <ComponentArt:GridColumn DataField="Available" HeadingText="colAvailable|Available" />
                              <ComponentArt:GridColumn DataField="Featured" HeadingText="colFeatured|Featured" />
                            </Columns>
                          </ComponentArt:GridLevel>
                        </Levels>
                        <ServerTemplates>
                            <ComponentArt:GridServerTemplate ID="PartNoTemplate">
                                <Template>
                                    <asp:HyperLink ID="lnkItemnmbr" runat="server"  
                                        NavigateUrl='<%# "part.aspx?partno=" + Container.DataItem["PartNo"] %>'
                                         Text='<%# Container.DataItem["PartNo"] %>'></asp:HyperLink>
                               </Template>
                              </ComponentArt:GridServerTemplate>
                            <ComponentArt:GridServerTemplate ID="PartNameTemplate">
                               <Template>
                                    <asp:HyperLink ID="lnkItemnmbr" runat="server"  
                                        NavigateUrl='<%# "part.aspx?partno=" + Container.DataItem["PartNo"] %>'
                                         Text='<%# Container.DataItem["PartName"] %>'></asp:HyperLink>
                               </Template>
                             </ComponentArt:GridServerTemplate>
                        </ServerTemplates>
                    </ComponentArt:Grid>
                    <br />
                    <div class="npadminsubheader">
                        <asp:Image runat="Server" ID="imgIndicator" ImageUrl="~/assets/common/icons/indicator.gif" />
                        <asp:Label runat="server" ID="lblBulkOperations" Text="Bulk Operations"></asp:Label>
                    </div>
                    <ComponentArt:TabStrip ID="ActionsTabStripBulk" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
                        MultiPageId="ActionsMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs BulkGroupTabs"
                        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
                        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
                        <ItemLooks>
                            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                            <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                            <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
                        </ItemLooks>
                        <Tabs>
                            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="Active"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Featured"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Category"></ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Base Price">
                            </ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts5" Text="Dates">
                            </ComponentArt:TabStripTab>
                            <ComponentArt:TabStripTab runat="server" ID="ts6" Text="Delete">
                            </ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                    <ComponentArt:MultiPage ID="ActionsMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
                        Width="100%" Height="100">
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblMarkActive" Text="Mark all records returned as: "></asp:Label>
                            <asp:RadioButton runat="server" ID="rbActive" Text="Active" Checked="true" GroupName="grpActive" />
                            <asp:RadioButton runat="server" ID="rbInactive" Text="Inactive" GroupName="grpActive" />
                            <asp:Button ID="btnMarkActive" runat="server" Text="Submit" OnClick="btnMarkActive_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblMarkFeatured" Text="Mark all records returned as: "></asp:Label>
                            <asp:RadioButton runat="server" ID="rbFeatured" Text="Featured" Checked="true" GroupName="grpFeatured" />
                            <asp:RadioButton runat="server" ID="rbNotFeatured" Text="Not Featured" GroupName="grpFeatured" />
                            <asp:Button ID="btnMarkFeatured" runat="server" Text="Submit" OnClick="btnMarkFeatured_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblCategories" Text="Assign all records returned the following category: "></asp:Label>
                            <asp:DropDownList ID="ddlCategory" runat="server">
                            </asp:DropDownList>
                            <asp:Button ID="btnSetCategories" runat="server" Text="Submit" OnClick="btnSetCategories_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblSetPrice" Text="On all records set a "></asp:Label>
                            <asp:DropDownList ID="ddlPriceFactor" runat="server">
                            </asp:DropDownList>
                            <asp:Label runat="server" ID="lblSetPriceTo" Text=" to Base Price of: "></asp:Label>
                            <asp:TextBox runat="server" ID="txtPriceFactorValue" Columns="8"></asp:TextBox>
                            <asp:Button ID="btnSetPriceFactor" runat="server" Text="Submit" OnClick="btnSetPriceFactor_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblReleased" Text="Assign all records returned to be: "></asp:Label>
                            <asp:DropDownList ID="ddlPartDates" runat="server">
                            </asp:DropDownList>
                            <np:DatePicker runat="server" ID="dpPartDate" />
                            <asp:Button ID="btnSetDate" runat="server" Text="Submit" OnClick="btnSetDate_Click"></asp:Button>
                        </ComponentArt:PageView>
                        <ComponentArt:PageView CssClass="npadminbody" runat="server">
                            <br />
                            <asp:Label runat="server" ID="lblDelete" Text="Delete all records returned from items. "></asp:Label>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"></asp:Button>
                        </ComponentArt:PageView>
                    </ComponentArt:MultiPage>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="hiddenslot" ID="hidden">
    <asp:Literal runat="server" ID="errWarning" Text="The bulk operation you are about to do\n will affect ALL RECORDS shown in your search set.\nContinue?"></asp:Literal>
</asp:Content>
