<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" AutoEventWireup="true" Codebehind="manufacturers.aspx.cs" Inherits="netpoint.admin.catalog.manufacturers" Title="Manufacturers" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Manufacturer List</title>
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader"><asp:Literal ID="ltlDesc" runat="server" Text="Create, view and delete manufacturers" />
            </td>
            <td class="npadminheader" align="right">
                <asp:Hyperlink ID="lnkAdd" runat="server" ImageUrl="../../assets/common/icons/add.gif"
                    NavigateUrl="~/admin/catalog/manufacturer.aspx?mnfid=0" ToolTip="Add Manufacturer"></asp:Hyperlink>
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <asp:GridView ID="gridManufacturs" runat="server" CssClass="npadmintable" AutoGenerateColumns="False"
            AllowPaging="True" AllowSorting="True" PageSize="25" EmptyDataText="No Manufactuerers Found" OnRowDataBound="gridManufacturs_RowDataBound" OnRowDeleting="gridManufacturs_RowDeleting"
             OnPageIndexChanging="gridManufacturs_PageIndexChanging" OnSorting="gridManufacturs_Sorting">
            <RowStyle CssClass="npadminbody"/>
            <AlternatingRowStyle CssClass="npadminbodyalt" />
            <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
            <PagerStyle CssClass="npadminbody" HorizontalAlign="right" />
            <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
            <Columns>
                <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" >
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" ImageUrl="../../assets/common/icons/delete.gif"
                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.MNFID") %>'></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MNFCode" HeaderText="colCode|Code" SortExpression="MNFCode, Encoding">
                </asp:BoundField>
                <asp:BoundField DataField="MNFName" HeaderText="colName|Name" SortExpression="MNFName"></asp:BoundField>                
                <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Hyperlink ID="lnkEdit" runat="server" ImageUrl="../../assets/common/icons/edit.gif" ToolTip="Edit this manufacturer" ></asp:Hyperlink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent.  Continue?"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Visible="false" Text="Edit this Manufactuer"></asp:Literal>
    <asp:Literal ID="hdnAdd" runat="server" Visible="false" Text="Add a Manufactuer"></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="false" Text="Delete this Manufactuer (from eCommerce only)"></asp:Literal>
</asp:Content>
