﻿<%@ Page MasterPageFile="~/masters/admin.master" ValidateRequest="false" Language="c#" Inherits="netpoint.admin.catalog.Part" Codebehind="Part.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="PartUnits" Src="~/admin/catalog/controls/PartUnits.ascx" %>
<%@ Register TagPrefix="np" TagName="PartTypes" Src="~/admin/catalog/controls/PartTypes.ascx" %>
<%@ Register TagPrefix="np" TagName="PartGeneral" Src="~/admin/catalog/controls/PartGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="PartAvailability" Src="~/admin/catalog/controls/PartAvailability.ascx" %>
<%@ Register TagPrefix="np" TagName="PartOptions" Src="~/admin/catalog/controls/PartOptions.ascx" %>
<%@ Register TagPrefix="np" TagName="PartMedia" Src="~/admin/catalog/controls/PartMedia.ascx" %>
<%@ Register TagPrefix="np" TagName="PartNotes" Src="~/admin/catalog/controls/PartNotes.ascx" %>
<%@ Register TagPrefix="np" TagName="PartAttributes" Src="~/admin/catalog/controls/PartAttributes.ascx" %>
<%@ Register TagPrefix="np" TagName="PartDescriptors" Src="~/admin/catalog/controls/PartDescriptors.ascx" %>
<%@ Register TagPrefix="np" TagName="PartDownloads" Src="~/admin/catalog/controls/PartDownloads.ascx" %>
<%@ Register TagPrefix="np" TagName="PartEquivalents" Src="~/admin/catalog/controls/PartEquivalents.ascx" %>
<%@ Register TagPrefix="np" TagName="PartFitments" Src="~/admin/catalog/controls/PartFitments.ascx" %>
<%@ Register TagPrefix="np" TagName="PartCross" Src="~/admin/catalog/controls/PartCross.ascx" %>
<%@ Register TagPrefix="np" TagName="PartCategory" Src="~/admin/catalog/controls/PartCategory.ascx" %>
<%@ Register TagPrefix="np" TagName="PartTaxes" Src="~/admin/catalog/controls/PartTaxes.ascx" %>
<%@ Register TagPrefix="np" TagName="PartInventory" Src="~/admin/catalog/controls/PartInventory.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceTree" Src="~/admin/catalog/controls/partpricetree.ascx" %>
<%@ Register TagPrefix="np" TagName="BasePrice" Src="~/admin/catalog/controls/PartPricing.ascx" %>
<%@ Register TagPrefix="np" TagName="PartRemarks" Src="~/admin/catalog/controls/PartRemarks.ascx" %>
<%@ Register TagPrefix="np" TagName="PartSEO" Src="~/admin/catalog/controls/PartSEO.ascx" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
                <td class="npadminheader" align="Right">
                    <asp:hyperlink id="lnkUpLevel" runat="server" imageurl="~/assets/common/icons/uplevel.gif" ToolTip="Return to list" />&nbsp;
                    <asp:literal id="ltlPart" runat="server" Text="Item"></asp:literal>&nbsp;
                    <asp:imagebutton id="btnDeleteAll" runat="server" imageurl="~/assets/common/icons/delete.gif" OnClick="btnDeleteAll_Click" ToolTip="Delete All" />&nbsp;
                    <asp:ImageButton ID="btnSave" OnClick="btnSave_Click" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save"></asp:ImageButton>&nbsp;
                    <asp:imagebutton id="btnSaveAll" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveAll_Click" tooltip="Save All and Close" ValidationGroup="PartSave" />&nbsp;
                        </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="PartTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="PartMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                <ComponentArt:ItemLook LookId="Level2TabLook" CssClass="Level2Tab" HoverCssClass="Level2TabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="3" LabelPaddingBottom="5" />
                <ComponentArt:ItemLook LookId="SelectedLevel2TabLook" CssClass="SelectedLevel2Tab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsPartInfo" Text="Part Info" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsAvailability" Text="Availability"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsUnits" Text="Units"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsTypes" Text="Types"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsMedia" Text="Media"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsSEO" Text="SEO"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsDetails" Text="Details"
                    SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsDisplayOptions" Text="Display Options"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsNotes" Text="Notes"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsAttributes" Text="Attributes"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsDescriptors" Text="Descriptors"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsDownloads" Text="Downloads"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsRemarks" Text="Remarks"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsGroups" Text="Groups" SubGroupCssClass="Level2GroupTabs"
                    DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsCategory" Text="Category"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsCrossSell" Text="Cross Sell"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsFitments" Text="Fitments"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsEquivalents" Text="Equivalents"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsFinancials" Text="Financials"
                    SubGroupCssClass="Level2GroupTabs" DefaultSubItemLookId="Level2TabLook" DefaultSubItemSelectedLookId="SelectedLevel2TabLook">
                    <ComponentArt:TabStripTab runat="server" ID="tsTaxes" Text="Taxes"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsInventory" Text="Inventory"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsPricelist" Text="Price Lists"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsSpecialPrice" Text="Special Prices"></ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="tsDiscountGroup" Text="Discount Groups"></ComponentArt:TabStripTab>
                </ComponentArt:TabStripTab>                
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="PartMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
            Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvGeneral">
                <np:PartGeneral ID="PartGeneral" runat="server"></np:PartGeneral>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvAvailability">
                <np:PartAvailability ID="PartAvailability" runat="server"></np:PartAvailability>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartUnits ID="PartUnits" runat="server"></np:PartUnits>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartTypes ID="PartTypes" runat="server"></np:PartTypes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartMedia ID="PartMedia" runat="server" ></np:PartMedia>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server" id="pvSEO">
                <np:PartSEO ID="PartSEO" runat="server" ></np:PartSEO>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartOptions ID="PartOptions" runat="server" ></np:PartOptions>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartNotes ID="PartNotes" runat="server" ></np:PartNotes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartAttributes ID="PartAttributes" runat="server" ></np:PartAttributes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartDescriptors ID="PartDescriptors" runat="server" ></np:PartDescriptors>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartDownloads ID="PartDownloads" runat="server" ></np:PartDownloads>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartRemarks ID="PartRemarks" runat="server" ></np:PartRemarks>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartCategory ID="PartCategory" runat="server" ></np:PartCategory>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartCross ID="PartCross" runat="server" ></np:PartCross>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartFitments ID="PartFitments" runat="server" ></np:PartFitments>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartEquivalents ID="PartEquivalents" runat="server" ></np:PartEquivalents>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" />
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartTaxes ID="PartTaxes" runat="server" ></np:PartTaxes>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">
                <np:PartInventory ID="PartInventory" runat="server" ></np:PartInventory>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">           
                <np:BasePrice ID="basePrice" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">            
                <np:PriceTree ID="SpecialPrices" runat="server" />
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="PageContent" runat="server">            
                <np:PriceTree ID="DiscountGroups" runat="server" />
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
<asp:Literal ID="errPartCategory" Visible="False" runat="server" Text="Items must be included in at least one category."></asp:Literal> 
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:literal runat="server" id="errDeletePartWarning" text="Are you sure? \n This deletes this item and all dependancies." Visible="false" />
</asp:Content>
