<%@ Register TagPrefix="uc1" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.CatalogNew" Codebehind="CatalogNew.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:imagebutton id="btnSaveAndReturn" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" ToolTip="Save and Return"></asp:imagebutton>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" colspan="2">
                &nbsp;
                <asp:Image ID="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
                &nbsp;
                <asp:Literal ID="ltlHeader" runat="server" Text="Create Catalog"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel" width="25%">
                <asp:literal id="ltlCatalogCode" runat="server" text="Catalog Code"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:textbox id="txtCatalogCode" runat="server"></asp:textbox>
                <asp:requiredfieldvalidator id="rfvCatalogCode" runat="server" controltovalidate="txtCatalogCode"
                    errormessage="RequiredFieldValidator">
					<img src="../../assets/common/icons/warning.gif" border="0"></asp:requiredfieldvalidator>
                <asp:label id="sysError" runat="server" cssclass="npwarning"></asp:label>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlCatalogName" runat="server" text="Catalog Name"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:textbox id="txtCatalogName" runat="server" width="400px" rows="40" size="60"></asp:textbox>
                <asp:requiredfieldvalidator id="rfvCatalogName" runat="server" controltovalidate="txtCatalogName"
                    errormessage="RequiredFieldValidator">
					<img src="../../assets/common/icons/warning.gif" border="0"></asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlSortOrder" runat="server" text="Sort Order"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:textbox id="txtSortOrder" runat="server">0</asp:textbox>
                <asp:requiredfieldvalidator id="rfvSortCode" runat="server" controltovalidate="txtSortOrder">
					<img src="../../assets/common/icons/warning.gif" border="0"></asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlViewOnly" runat="server" text="View Only"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:checkbox id="sysViewOnly" runat="server"></asp:checkbox>
            </td>
        </tr>
         <tr>
            <td class="npadminlabel">
                <asp:literal id="ltlSiteMap" runat="server" text="Enable for SiteMap"></asp:literal>
            </td>
            <td class="npadminbody">
                <asp:checkbox id="sysSiteMap" runat="server"></asp:checkbox>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="errCodeExists" text="Catalog code already exists; choose another"
        runat="server" visible="False"></asp:literal>
</asp:Content>
