<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.PartIndex"
    Codebehind="PartIndex.aspx.cs" %>

<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="npadminlabel" style="width:30%">
                    <asp:Literal ID="ltlIndexParts" runat="server" Text="Index Items"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllParts" runat="server" OnClick="btnIndexAllParts_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Items" />                    
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexAccounts" runat="server" Text="Index Accounts"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllAccounts" runat="server" OnClick="btnIndexAllAccounts_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Accounts">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexUsers" runat="server" Text="Index Users"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllUsers" runat="server" OnClick="btnIndexAllUsers_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Users">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexProspects" runat="server" Text="Index Prospects"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllProspects" runat="server" OnClick="btnIndexAllProspects_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Prospects">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexEvents" runat="server" Text="Index Events"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllEvents" runat="server" OnClick="btnIndexAllEvents_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Events">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexTickets" runat="server" Text="Index All Tickets"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllTickets" runat="server" OnClick="btnIndexAllTickets_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Tickets">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexSolutions" runat="server" Text="Index Knowledge Base"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllSolutions" runat="server" OnClick="btnIndexAllSolutions_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Knowledge-Base Entries">
                        </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexWorkOrders" runat="server" Text="Index Service Calls"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllWorkOrders" runat="server" OnClick="btnIndexAllWorkOrders_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Service Calls">
                    </asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel">
                    <asp:Literal ID="ltlIndexOpportunities" runat="server" Text="Index All Opportunites"></asp:Literal>
                </td>
                <td>
                    &nbsp;
                    <asp:ImageButton ID="btnIndexAllOpportunities" runat="server" OnClick="btnIndexAllOpportunities_Click" ImageUrl="~/assets/common/icons/upgrade.gif" ToolTip="Index All Opportunities">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
   
</asp:Content>
