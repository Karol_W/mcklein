<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.Variants" Codebehind="Variants.aspx.cs" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;</td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader"></td>
        </tr>
        <tr>
            <td>
                <ComponentArt:TreeView id="tvwVariant" ClientScriptLocation="~/scripts"
                    DragAndDropEnabled="false" 
                    NodeEditingEnabled="false"
                    KeyboardEnabled="true"
                    CssClass="TreeView" 
                    NodeCssClass="TreeNode" 
                    SelectedNodeCssClass="SelectedTreeNode" 
                    HoverNodeCssClass="HoverTreeNode"
                    NodeEditCssClass="NodeEdit"
                    LineImageWidth="19" 
                    LineImageHeight="20"
                    DefaultImageWidth="16" 
                    DefaultImageHeight="16"
                    ItemSpacing="0" 
                    ImagesBaseUrl="~/assets/common/tree/"
                    NodeLabelPadding="3"
                    ParentNodeImageUrl="folder.gif" 
                    LeafNodeImageUrl="file.gif" 
                    ShowLines="true" 
                    LineImagesFolderUrl="~/assets/common/tree/lines/"
                    EnableViewState="true"
                    runat="server" >
               </ComponentArt:TreeView>
           </td>
       </tr>
   </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnAddPart" runat="server" Visible="False" Text="Add Item"></asp:Literal>
    <asp:Literal ID="hdnAddPartToVariant" runat="server" Visible="False" Text="Add Item to Category"></asp:Literal>
    <asp:Literal ID="hdnAddVariant" runat="server" Visible="False" Text="Add Variant Category"></asp:Literal>
    <asp:Literal ID="hdnDefaultPart" runat="server" Visible="False" Text="Default Item"></asp:Literal>
    <asp:Literal ID="hdnOptionalPart" runat="server" Visible="False" Text="Optional Item"></asp:Literal>
    <asp:Literal ID="hdnRequiredComponent" runat="server" Visible="False" Text="Required Component"></asp:Literal>
    <asp:Literal ID="hdnNotRequired" runat="server" Visible="False" Text="Component Not Required"></asp:Literal>
</asp:Content>
