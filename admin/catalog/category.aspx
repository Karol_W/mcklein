<%@ Page ValidateRequest="false" MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.Category" Codebehind="Category.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="np" TagName="description" Src="~/admin/catalog/controls/CategoryDescription.ascx" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/catalog/controls/CategoryGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="taxes" Src="~/admin/catalog/controls/CategoryTaxes.ascx" %>
<%@ Register TagPrefix="np" TagName="seo" Src="~/admin/catalog/controls/CategorySEO.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="TitleTag" runat="server">
<script type="text/javascript" src="../../scripts/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea#Content",
        plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor macros"
        ],
        valid_elements :  "*[*]"
    });
</script>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="btnDeleteAll" runat="server" ImageUrl="~/assets/common/icons/delete.gif" ToolTip="Delete All" />&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif" ToolTip="Save All and Close" />&nbsp;</td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="CatalogTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="CatalogMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Description"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Taxes"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="tsSEO" Text="SEO"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="CatalogMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
        Width="100%" Height="400">
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:general ID="general" runat="server"></np:general>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:description ID="description" runat="server"></np:description>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:taxes ID="taxes" runat="server"></np:taxes>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvSEO">
            <np:seo ID="seo" runat="server"></np:seo>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
<asp:Content runat="server" ID="hidden" ContentPlaceHolderID="hiddenslot">
    <asp:Literal runat="server" ID="errDeleteCategoryWarning" Text="This deletes this category and all dependencies. Continue?" Visible="false" />
</asp:Content>
