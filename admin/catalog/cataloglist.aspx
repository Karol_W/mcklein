<%@ Page MasterPageFile="~/masters/admin.master" Language="c#" Inherits="netpoint.admin.catalog.CatalogList"
    Codebehind="CatalogList.aspx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Catalog browser and details*</title>
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">

    <script type="text/javascript">
    function treeContextMenu(treeNode, e)
    {
      switch(treeNode.ImageUrl)
      {
        case '~/assets/common/icons/tree-toplevel.gif': 
          ctl00_mainslot_catalogMenu.ShowContextMenu(e, treeNode); 
          break; 
        case '~/assets/common/tree/folder_open.gif': 
          ctl00_mainslot_emptyMenu.ShowContextMenu(e, treeNode); 
          break; 
        default:
          ctl00_mainslot_populatedMenu.ShowContextMenu(e, treeNode); 
          break; 
      }      
    }
    
    function contextMenuClickHandler(menuItem)
    {
      var contextDataNode = menuItem.ParentMenu.ContextData; 
      var feedback = '"' + menuItem.Text + '" command was issued on the "' + contextDataNode.Text + '" node from "' + contextDataNode.Value + '".'; 
      //alert(feedback);
      switch(menuItem.Value){
        case 'catalog':
            window.location='Catalog.aspx?catalogid='+contextDataNode.Value;
            break;
        case 'category':
            window.location='category.aspx?categoryid='+contextDataNode.Value;
            break;
        case 'newcat':
            window.location='categorynew.aspx?parentid='+contextDataNode.Value;
            break;
        case 'newcat4cat':
            window.location='categorynew.aspx?catalogid='+contextDataNode.Value;
            break;
        case 'newpart':
            window.location='PartNew.aspx?&categoryid='+contextDataNode.Value;
            break;
        case 'partlist':
            window.location='PartList.aspx?categoryid='+contextDataNode.Value;
            break;
       }
      return true; 
    }
    </script>
    <ComponentArt:Menu ID="catalogMenu" Orientation="Vertical" DefaultGroupCssClass="MenuGroup" ClientScriptLocation="~/scripts"
        DefaultItemLookId="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="~/assets/common/admin/"
        EnableViewState="false" ContextMenu="Custom" ClientSideOnItemSelect="contextMenuClickHandler"
        runat="server">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover"
                ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10"
                LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak" />
        </ItemLooks>
        <Items>
            <ComponentArt:MenuItem text="Catalog Detail" Value="catalog" />
            <ComponentArt:MenuItem lookid="BreakItem" autopostbackonselect="false" />
            <ComponentArt:MenuItem text="New SubCategory" Value="newcat4cat" />
        </Items>
    </ComponentArt:Menu>
    <ComponentArt:Menu ID="emptyMenu" Orientation="Vertical" DefaultGroupCssClass="MenuGroup" ClientScriptLocation="~/scripts"
        DefaultItemLookId="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="~/assets/common/admin/"
        EnableViewState="false" ContextMenu="Custom" ClientSideOnItemSelect="contextMenuClickHandler"
        runat="server">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover"
                ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10"
                LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak" />
        </ItemLooks>
        <Items>
            <ComponentArt:MenuItem text="Category Detail" Value="category" />
            <ComponentArt:MenuItem lookid="BreakItem" autopostbackonselect="false" />
            <ComponentArt:MenuItem text="New SubCategory" Value="newcat" />
            <ComponentArt:MenuItem text="New Part" Value="newpart" />
        </Items>
    </ComponentArt:Menu>
    <ComponentArt:Menu ID="populatedMenu" Orientation="Vertical" DefaultGroupCssClass="MenuGroup" ClientScriptLocation="~/scripts"
        DefaultItemLookId="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="~/assets/common/admin/"
        EnableViewState="false" ContextMenu="Custom" ClientSideOnItemSelect="contextMenuClickHandler"
        runat="server">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover"
                ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10"
                LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak" />
        </ItemLooks>
        <Items>
            <ComponentArt:MenuItem text="Category Detail" Value="category" />
            <ComponentArt:MenuItem text="View Parts" Value="partlist" />
            <ComponentArt:MenuItem lookid="BreakItem" autopostbackonselect="false" />
            <ComponentArt:MenuItem text="New SubCategory" Value="newcat" />
            <ComponentArt:MenuItem text="New Part" Value="newpart" />
        </Items>
    </ComponentArt:Menu>
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:HyperLink runat="server" ID="lnkNewCatalog" ImageUrl="~/assets/common/icons/add.gif"
                    NavigateUrl="~/admin/catalog/CatalogNew.aspx" Visible="false" ToolTip="Create New Catalog" />
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader" align="right">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="True" CssClass="npadminbody"
                    OnSelectedIndexChanged="ddlCatalog_SelectedIndexChanged" />&nbsp;
            </td>
        </tr>
        <tr>
            <td class="npadminbody"><asp:Literal ID="ltlRightClick" runat="server" Text="Right-Click Items for Submenu"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npadminbody">
                <ComponentArt:TreeView ID="tvList" DragAndDropEnabled="false" NodeEditingEnabled="false" ClientScriptLocation="~/scripts"
                    KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
                    HoverNodeCssClass="HoverTreeNode" NodeEditCssClass="NodeEdit" LineImageWidth="19"
                    LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
                    ImagesBaseUrl="~/assets/common/tree/" NodeLabelPadding="3" ParentNodeImageUrl="folder.gif"
                    LeafNodeImageUrl="file.gif" ShowLines="true" LineImagesFolderUrl="~/assets/common/tree/lines/"
                    EnableViewState="true" runat="server" OnContextMenu="treeContextMenu">
                </ComponentArt:TreeView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="hdnAddCategory" runat="server" Text="Add Category" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnAllCatalogs" runat="server" Visible="false" Text="All Catalogs" />
</asp:Content>
