<%@ Page MasterPageFile="~/masters/admin.Master" Language="c#" Inherits="netpoint.admin.catalog.WarehouseDetail" Codebehind="WarehouseDetail.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteGeneral" runat="server" ImageUrl="~/assets/common/icons/delete.gif"
                    ToolTip="Delete" OnClick="btnDeleteGeneral_Click"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ImageUrl="~/assets/common/icons/saveandclose.gif"
                    ToolTip="Save All and Close" OnClick="btnSave_Click"></asp:ImageButton>&nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <ComponentArt:TabStrip ID="ProjectTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
            MultiPageId="ProjectMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
            DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
            DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
            <ItemLooks>
                <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
                 <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                    LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            </ItemLooks>
            <Tabs>
                <ComponentArt:TabStripTab runat="server" ID="tsGeneral" Text="General">
                </ComponentArt:TabStripTab>
                <ComponentArt:TabStripTab runat="server" ID="tsLanguage" Text="Language">
                </ComponentArt:TabStripTab>
            </Tabs>
        </ComponentArt:TabStrip>
        <ComponentArt:MultiPage ID="ProjectMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts" Width="100%" Height="400">
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvWarehouseGeneral">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminlabel" colspan="1">
                            <asp:Literal ID="ltlWarehouseName" runat="server" Text="Warehouse Name"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtWarehouseName">
                                <asp:Image ID="imgWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                            </asp:RequiredFieldValidator>
                        </td>
                        <td colspan="5"><asp:TextBox ID="txtWarehouseName" runat="server" columns="55"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" colspan="1">
                            <asp:Literal ID="ltlWarehouseCode" runat="server" Text="Warehouse Code"></asp:Literal>
                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtWarehouseCode">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                            </asp:RequiredFieldValidator>
                        </td>
                        <td colspan="5"><asp:TextBox ID="txtWarehouseCode" runat="server" columns="55"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel" colspan="1"><asp:Literal ID="ltlAddress" runat="server" Text="Address"></asp:Literal></td>
                        <td colspan="5"><asp:TextBox ID="txtAddress" runat="server" columns="55"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlCity" runat="server" Text="City"></asp:Literal></td>
                        <td><asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlState" runat="server" Text="State"></asp:Literal></td>
                        <td colspan="3"><asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlZIP" runat="server" Text="Zip"></asp:Literal></td>
                        <td><asp:TextBox ID="txtZip" runat="server"></asp:TextBox></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlCountry" runat="server" Text="Country"></asp:Literal></td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel"><asp:Literal ID="ltlDropShip" runat="server" Text="Drop Ship"></asp:Literal></td>
                        <td><asp:CheckBox ID="sysDropShipFlag" runat="server"></asp:CheckBox></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlAvailable" runat="server" Text="Available"></asp:Literal></td>
                        <td><asp:CheckBox ID="sysAvailable" runat="server"></asp:CheckBox></td>
                        <td class="npadminlabel"><asp:Literal ID="ltlDefaultShip" runat="server" Text="Default Delivery Warehouse"></asp:Literal></td>
                        <td><asp:CheckBox ID="sysDefault" runat="server"></asp:CheckBox>
                            <asp:Label ID="sysError" runat="server" Font-Bold="True" ForeColor="Red" Font-Size="Larger"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ComponentArt:PageView>
            <ComponentArt:PageView CssClass="npadminbody" runat="server" ID="pvLanguage">
                <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
            </ComponentArt:PageView>
        </ComponentArt:MultiPage>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errOneDefault" runat="server" Text="Default address is already defined" Visible="False"></asp:Literal>
    <asp:literal id="hdnDeleteWarehouseWarning" runat="server" text="This will delete all inventory associated with this warehouse. Continue?" visible="False"></asp:literal>
</asp:Content>
