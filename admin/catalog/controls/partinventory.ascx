<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartInventory" Codebehind="PartInventory.ascx.cs" %>
<table class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlInherit" runat="server" Text="Inherit from Global Settings"></asp:Literal></td>
        <td>
            <asp:CheckBox ID="cbxInherit" runat="server" AutoPostBack="true" />            
        </td>
    </tr>    
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlDisplayOOS" runat="server" Text="Display item when out of stock"></asp:Literal></td>
        <td>
            <asp:CheckBox ID="cbxDisplayOOS" runat="server" />            
        </td>
    </tr>    
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlReduceInv" runat="server" Text="Reduce inventory upon order completion"></asp:Literal></td>
        <td>
            <asp:CheckBox ID="cbxReduceInv" runat="server" />            
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlMinQty" runat="server" Text="Onhand quantity for this item to be out of stock"></asp:Literal></td>
        <td>
            <asp:TextBox ID="txtMinQty" runat="server" />            
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBackorders" runat="server" Text="Backorders"></asp:Literal></td>
        <td>
            <asp:DropDownList ID="ddlBackorders" runat="server">
                <asp:ListItem Text="Warn" Value="W" />
                <asp:ListItem Text="Yes" Value="Y" />
                <asp:ListItem Text="No" Value="N" />
            </asp:DropDownList>           
        </td>
    </tr>
</table>
<asp:GridView 
    id="gvInventory"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowDataBound="gvInventory_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="colLocked|Locked">
		    <ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
			    <asp:Image id="imgLocked" runat="server" ImageUrl="~/assets/common/icons/checked.gif"></asp:Image>
			</ItemTemplate>
		</asp:TemplateField>
        <asp:TemplateField HeaderText="colAvailable|Available">
		    <ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
			    <asp:Image id="imgAvailable" runat="server" ImageUrl="~/assets/common/icons/checked.gif"></asp:Image>
			</ItemTemplate>
		</asp:TemplateField>        
		<asp:TemplateField HeaderText="colWarehouse|Warehouse">
			<ItemTemplate>
			    <asp:Label ID="lblWarehouse" runat="server" />				
			</ItemTemplate>			
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colOnHand|On Hand">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "OnHand") %>
			</ItemTemplate>			
		</asp:TemplateField>		
		<asp:TemplateField HeaderText="colUpdated|Updated">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "LastUpdated", "{0:yyyy-MMM-dd}") %>
			</ItemTemplate>
		</asp:TemplateField>				
    </Columns>
</asp:GridView>

<asp:literal id="hdnLocked" runat="server" visible="False" text="Warehouse Locked"></asp:literal>
<asp:literal id="hdnAvailable" runat="server" visible="False" text="Warehouse Available"></asp:literal>

