<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartTypes" Codebehind="PartTypes.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Panel ID="pnlProdLineChild" runat="server">
<table class="npadmintable">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlProdLineParent" runat="server" Text="Prod. Line Name"></asp:Literal></td>
        <td colspan="3">
            <np:PartPicker ID="ppProdLinePartNo" runat="server"></np:PartPicker>
            <br />
            <asp:HyperLink ID="sysProdLinePartNo" runat="server"></asp:HyperLink>
        </td>
    </tr>
</table>
</asp:Panel>
<table class="npadmintable">    
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlPartType" Text="Item Type" runat="server"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><asp:TextBox ID="txtPartType" runat="server" Columns="40"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlMasterType" runat="server" Text="Master Type"></asp:Literal></td>
        <td class="npadminbody" colspan="3">
            <asp:RadioButton ID="rbNone" runat="server" Text="None" GroupName="Master" 
                AutoPostBack="True" OnCheckedChanged="rbNone_CheckedChanged"></asp:RadioButton>&nbsp;
            <asp:RadioButton ID="rbProdLineFlag" runat="server" Text="Product Line" GroupName="Master"
                AutoPostBack="True" OnCheckedChanged="rbProdLineFlag_CheckedChanged"></asp:RadioButton>&nbsp;
            <asp:RadioButton ID="rbSalesKit" runat="server" Text="Sales Kit" GroupName="Master"
                AutoPostBack="True" OnCheckedChanged="rbSalesKit_CheckedChanged"></asp:RadioButton>&nbsp;
            <asp:RadioButton ID="rbAssemblyKit" runat="server" Text="Assembly Kit" GroupName="Master"
                AutoPostBack="True" OnCheckedChanged="rbAssemblyKit_CheckedChanged"></asp:RadioButton>&nbsp;
            <asp:RadioButton ID="rbVariantMaster" runat="server" Text="Variant" GroupName="Master"
                AutoPostBack="True" OnCheckedChanged="rbVariantMaster_CheckedChanged"></asp:RadioButton>&nbsp;
            <asp:HyperLink ID="lnkVariant" runat="server" imageurl="~/assets/common/icons/kit.gif" ToolTip="Edit Variant" Visible="False" ></asp:HyperLink>
            <asp:Label ID="sysBOMUsed" runat="server" Visible="false" CssClass="npwarning"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Panel ID="pnlProdline" Visible="False" runat="server">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminsubheader" colspan="4">
                            <asp:Image ID="imgProdlineIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                            <asp:Literal ID="ltlProdLine" runat="server" Text="Product Line"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminlabel">
                            <asp:Literal ID="ltlDisplay" Text="Display Method" runat="server"></asp:Literal></td>
                        <td class="npadminbody" colspan="3">
                            <asp:Panel ID="pnlChk" runat="server">
                                <asp:CheckBox ID="chkList" Text="List" runat="server" CssClass="npadminbody"></asp:CheckBox>&nbsp;
                                <asp:CheckBox ID="chkDropDownList" Text="Dropdown List" runat="server" CssClass="npadminbody"></asp:CheckBox>&nbsp;
                                <asp:CheckBox ID="chkImagePicker" runat="server" CssClass="npadminbody" Text="Image Picker"></asp:CheckBox>&nbsp;
                                <asp:CheckBox ID="chkAttributePicker" Text="Attribute Picker" runat="server" CssClass="npadminbody"></asp:CheckBox>
                                <asp:CheckBox ID="chkAttributeMatrix" Text="Attribute Matrix" runat="server" CssClass="npadminbody"></asp:CheckBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView 
                                ID="gridProdLine" 
                                runat="server" 
                                AutoGenerateColumns="false" 
                                CssClass="npadmintable"
                                EmptyDataText="No Items in Product Line" 
                                OnRowDeleting="gridProdLine_RowDeleting" AllowPaging="True" OnPageIndexChanging="gvProdLine_PageIndexChanging">
                                <AlternatingRowStyle CssClass="npadminbodyalt" />
                                <RowStyle CssClass="npadminbody" />
                                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                                <HeaderStyle CssClass="npadminlabel" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnRemove" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/remove.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:HyperLinkField HeaderText="ItemNo|Item No." DataTextField="PartNo" DataNavigateUrlFields="PartNo"
                                        DataNavigateUrlFormatString="~/admin/catalog/part.aspx?partno={0}" />
                                    <asp:HyperLinkField HeaderText="ItemName|Item Name" DataTextField="PartName" DataNavigateUrlFields="PartNo"
                                        DataNavigateUrlFormatString="~/admin/catalog/part.aspx?partno={0}" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="npadminbody" align="right" colspan="4">
                            <asp:Literal ID="ltlAddChild" runat="server" Text="Add Item to Product Line"></asp:Literal>&nbsp;
                            <np:PartPicker ID="ppkProdLineChild" runat="server" />&nbsp;
                            <asp:ImageButton ID="btnAddProdLinePart" runat="server" ImageUrl="~/assets/common/icons/add.gif"
                                OnClick="btnAddProdLinePart_Click" ToolTip="Add Item" />&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Panel ID="pnlKit" runat="server">
                <table class="npadmintable">
                    <tr>
                        <td class="npadminsubheader">
                            <asp:Image ID="imgKitIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />&nbsp;
                            <asp:Literal ID="ltlKitMembers" Text="Bill of Materials Items" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView  
                                id="gvKit"
                                runat="server" 
                                AutoGenerateColumns="false" 
                                HeaderStyle-CssClass="npadminsubheader"
                                CssClass="npadmintable" 
                                RowStyle-CssClass="npadminbody" 
                                AlternatingRowStyle-CssClass="npadminbodyalt" 
                                EmptyDataText="No Records Found" 
                                OnRowCommand="gvKit_RowCommand" 
                                OnRowDataBound="gvKit_RowDataBound" 
                                OnRowEditing="gvKit_RowEditing" 
                                OnRowCancelingEdit="gvKit_RowCancelingEdit">
                                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />  
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnRemove" runat="server" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PartsKitID") %>'
                                                ImageUrl="~/assets/common/icons/remove.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ItemNo|Item No.">
                                        <ItemStyle Wrap="False"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkPartNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildPartNo") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <np:PartPicker ID="ppk" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildPartNo") %>'></np:PartPicker>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ItemName|Item Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildPart.PartName") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="colQty|Qty">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "Quantity") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtQuantity" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="colInc|Inc">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlPriceIncrement" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PriceIncrement") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPriceIncrement" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "PriceIncrement") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="colFactor|Factor">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlPriceFactor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PriceFactor") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPriceFactor" runat="server" Columns="7" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "PriceFactor") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="colPrice|Price">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Panel ID="pnlQuantityPrice" runat="server" Visible="false">
                                                <%# Eval("Quantity") %> @ <np:PriceDisplay ID="prcUnitPrice" runat="server" />
                                                <br />= <np:PriceDisplay ID="prcExtendedPrice" runat="server" />
                                            </asp:Panel>
                                            <np:PriceDisplay ID="prcPrice" runat="server" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/icons/edit.gif" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="cancel" ImageUrl="~/assets/common/icons/cancel.gif" />
                                            <asp:ImageButton ID="btnSave" runat="server" CommandName="save" ImageUrl="~/assets/common/icons/save.gif"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PartsKitID") %>' />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><asp:ImageButton ID="btnAdd" runat="server" ToolTip="Add Item to Bill of Materials" ImageUrl="~/assets/common/icons/add.gif"></asp:ImageButton></td>
                    </tr>
                </table>
                <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
            </asp:Panel>
        </td>
    </tr>
</table>
<asp:Literal ID="errInvalidChild" runat="server" Text="The item you are adding to the bill of materials does not exist; choose another" Visible="False"></asp:Literal>
<asp:Literal ID="hdnRemove" runat="server" Text="Remove Item from Bill of Materials" Visible="False"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="False"></asp:Literal>
<asp:Literal ID="hdnCancel" runat="server" Text="Cancel" Visible="False"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="False"></asp:Literal>
<asp:Literal ID="errMustBeNumber" runat="server" Text="Must be a number" Visible="False"></asp:Literal>
<asp:Literal ID="hdnBOMUsed" runat="server" Text="You cannot edit aggregate properties because this item has been commited to a transaction" Visible="false"></asp:Literal>
<asp:Literal ID="hdnRemoveAggregate" runat="server" Text="All configuration data will be removed. Continue?" Visible="false"></asp:Literal>
<asp:Literal ID="errNoVariantInBOM" runat="server" Text="Variant items can not be used as members of a Bill of Materials." Visible="False"></asp:Literal>