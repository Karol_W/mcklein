<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CatalogManufacturers"
    Codebehind="CatalogManufacturers.ascx.cs" %>
<asp:GridView 
    ID="gvManufacturers" 
    runat="server"  
    AutoGenerateColumns="False" 
    CssClass="npadmintable" 
    EmptyDataText="No Records Found" OnRowCommand="gvManufacturers_RowCommand" OnRowDataBound="gvManufacturers_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />  
    <RowStyle CssClass="npadminbody" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandName="remove" ToolTip="Remove this Manufacturer." CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CMNFID") %>'>
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Name" HeaderText="colName|Name"></asp:BoundField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:Literal ID="ltlAddManufacturer" runat="server" Text="Add Manufacturer"></asp:Literal>
    &nbsp;
    <asp:DropDownList ID="ddlManufacturer" runat="server">
    </asp:DropDownList>
    &nbsp;
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add">
    </asp:ImageButton>&nbsp;
</div>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Deletions are permanent. Continue?"
    Visible="False"></asp:Literal>
<asp:Literal ID="hdnDeleteManufactuer" runat="server" Text="Delete Manufacturer"
    Visible="False"></asp:Literal>