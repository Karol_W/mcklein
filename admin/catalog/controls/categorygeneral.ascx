<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CategoryGeneral" Codebehind="CategoryGeneral.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel" width="25%"><asp:Literal ID="ltlCatalog" runat="server" Text="Catalog"></asp:Literal></td>
        <td class="npadminbody"><asp:Literal ID="sysCatalogName" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlParentCategory" runat="server" Text="Parent Category"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlParentCategory" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlCategoryName" runat="server" Text="Category Name"></asp:Literal></td>
        <td class="npadminbody">
            <asp:TextBox ID="txtCategoryName" runat="server" Width="400px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName" ErrorMessage="RequiredFieldValidator">
				<img src="/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlCategoryCode" runat="server" Text="Category Code"></asp:Literal></td>
        <td class="npadminbody">
            <asp:Literal ID="sysCategoryCode" runat="server"></asp:Literal>
            <asp:TextBox ID="txtCategoryCode" runat="server" Width="150px"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvCategoryCode" runat="server" ControlToValidate="txtCategoryCode" ErrorMessage="RequiredFieldValidator">
				<img src="/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator>&nbsp;
            <asp:HyperLink ID="lnkCopy" runat="server" ImageUrl="/assets/common/icons/duplicate.gif"
                ToolTip="Copy" NavigateUrl="~/admin/catalog/CategoryCopy.aspx">HyperLink</asp:HyperLink>
            <asp:Literal ID="errDuplicateCategory" runat="server" Visible="False" Text="The category code you selected already exists; choose another"></asp:Literal></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlImageFile" runat="server" Text="Image File"></asp:Literal></td>
        <td class="npadminbody"><np:FilePicker id="fpImageFile" runat="server" Width="350px" StartDirectory="~/assets/catalog/categories/"></np:FilePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlListType" runat="server" Text="List Type"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlListType" runat="server"></asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlSortCode" runat="server" Text="Sort Code"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtSortCode" runat="server" Width="50px"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlVisible" runat="server" Text="Visible"></asp:Literal></td>
        <td class="npadminbody"><asp:CheckBox ID="sysVisible" runat="server"></asp:CheckBox></td>
    </tr>
</table>
<asp:Literal ID="hdnNone" Text="None" runat="server" Visible="False"></asp:Literal>
