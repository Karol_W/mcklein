﻿<%@ Control Language="C#" CodeBehind="partremarks.ascx.cs" Inherits="netpoint.admin.catalog.controls.partremarks" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td>
            <NPWC:MultiLineTextBox id="txtRemarks" runat="server" LimitInput="4000" TextMode="MultiLine" Height="350px" Rows="20" Width="600px" Columns="72"></NPWC:MultiLineTextBox>
        </td>
    </tr>
</table>
