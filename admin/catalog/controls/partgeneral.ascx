<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartGeneral" Codebehind="PartGeneral.ascx.cs" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Label id="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlPartNo" runat="server" Text="Item No."></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtPartNo" runat="server"></asp:textbox>
			<asp:RequiredFieldValidator id="rfvPartNo" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtPartNo">
				<asp:Image id="imgWarning" runat="server" imageurl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
			<asp:HyperLink Runat="server" ID="lnkPreview" ImageUrl="~/assets/common/icons/preview.gif" Target="_blank" Visible="False" ToolTip="Preview" /></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlPartCode" runat="server" Text="Item Code"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtPartCode" runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlPartName" runat="server" Text="Item Name"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtPartName" runat="server" Columns="55" Width="350px"></asp:textbox><asp:requiredfieldvalidator id="rfvPartName" runat="server" ControlToValidate="txtPartName" ErrorMessage="RequiredFieldValidator">
		    <img src="~/assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlDescription" runat="server" Text="Description"></asp:literal></td>
		<td class="npadminbody" colspan="3"><NPWC:MultiLineTextBox id="txtDescription" runat="server" LimitInput="2000" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></NPWC:MultiLineTextBox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlKeyword" runat="server" Text="Keywords"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtKeywords" runat="server" Width="400px"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlManufacturer" runat="server" Text="Manufacturer"></asp:literal></td>
		<td class="npadminbody" colspan="3">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td><div id="formdiv1" style="POSITION: relative"><asp:dropdownlist id="ddlMNFCode" runat="server"></asp:dropdownlist></div></td>
					<td><span style="height: 25px; width: 1px;">&nbsp;</span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlManufacturerPartNo" runat="server" Text="Manufacturer Item No."></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtManufacturerPartNo" runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlBasePrice" runat="server" Text="Base Price"></asp:literal></td>
		<td class="npadminbody" colspan="3"><np:PriceDisplay ID="prcPrice" runat="server" /></td>
    </tr>
     <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlItemGroup" runat="server" Text="Item Group"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlItemGroup" runat="server"></asp:DropDownList></td
    </tr>
</table>
<asp:Literal id="hdnMustBeNumber" Text="Must be a number" runat="server" Visible="False"></asp:Literal>
<asp:Literal ID="ltlErr" Text="The item number you selected already exists; choose another" runat="server" Visible="false"></asp:Literal>
