<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="partpricetree.ascx.cs" Inherits="netpoint.admin.catalog.controls.partpricetree" %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<ComponentArt:TreeView ID="tvwPrice" runat="server" DragAndDropEnabled="false" NodeEditingEnabled="false"
    ClientScriptLocation="~/scripts" KeyboardEnabled="true" CssClass="npadmintable" NodeCssClass="npadminbody"
    SelectedNodeCssClass="SelectedTreeNode" 
    LineImageWidth="19" LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16"
    ItemSpacing="0" ImagesBaseUrl="~/assets/common/tree/" NodeLabelPadding="3"
    ShowLines="true" LineImagesFolderUrl="~/assets/common/tree/lines/"
    EnableViewState="true">
</ComponentArt:TreeView>

<asp:Literal ID="hdnQuantity" runat="server" Text="Quantity" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPrice" runat="server" Text="Price" Visible="false"></asp:Literal>
<asp:Literal ID="hdnItem" runat="server" Text="Item" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPeriod" runat="server" Text="Period" Visible="false"></asp:Literal>
<asp:Literal ID="hdnItemGroup" runat="server" Text="Group" Visible="False"></asp:Literal>
<asp:Literal ID="hdnManufacturer" runat="server" Text="Manufacturer" Visible="false"></asp:Literal>
<asp:Literal ID="hdnOffListPrice" runat="server" Text="off list price" visible="false"></asp:Literal>