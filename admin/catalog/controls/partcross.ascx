<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartCross" Codebehind="PartCross.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/PartPicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Label CssClass="npwarning" ID="sysError" runat="server"></asp:Label>
<asp:Panel ID="pnlDetail" runat="server" Width="100%" Visible="False">
    <table cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminlabel"><asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:ImageButton></td>
            <td class="npadminlabel" align="right"><asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Update"></asp:ImageButton>&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
            <td class="npadminbody">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <div id="formdiv1" style="position: relative">
                                <asp:DropDownList ID="ddlCrossType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCrossType_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </td>
                        <td><img height="25" src="" width="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlValue" runat="server" Text="Value"></asp:Literal></td>
            <td class="npadminbody">
                <np:PartPicker ID="picker" runat="server"></np:PartPicker>
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <div id="formdiv2" style="position: relative">
                                <asp:DropDownList ID="ddlCategories" runat="server"></asp:DropDownList>
                            </div>
                        </td>
                        <td><img height="25" src="" alt="" width="1" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlAdditionalText" runat="server" Text="Additional Text"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtDescription" runat="server" Columns="50" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtSortOrder" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlVisibilities" runat="server" Text="Visibilities"></asp:Literal></td>
            <td class="npadminbody">
                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td class="npadminbody" align="center"><asp:CheckBox ID="chkShowPartNo" Text="Item No. or Category ID" runat="server"></asp:CheckBox></td>
                        <td class="npadminbody" align="center"><asp:CheckBox ID="chkShowName" Text="Name" runat="server"></asp:CheckBox></td>
                        <td class="npadminbody" align="center"><asp:CheckBox ID="chkShowImage" Text="Image" runat="server"></asp:CheckBox></td>
                        <td class="npadminbody" align="center"><asp:CheckBox ID="chkShowDescription" Text="Description" runat="server"></asp:CheckBox></td>
                        <td class="npadminbody" align="center"><asp:CheckBox ID="chkShowPrice" Text="Item Price" runat="server"></asp:CheckBox></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:GridView 
    id="gvCross"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowCommand="gvCross_RowCommand" OnRowDataBound="gvCross_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="ltlPreview|Preview">
            <HeaderStyle Width="90%"></HeaderStyle>
            <ItemStyle BackColor="White"></ItemStyle>
            <ItemTemplate>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top" class="npbody"><b><asp:Label ID="lblDescription" runat="server" Visible="False"></asp:Label></b></td>
                        <td valign="top" class="npbody" align="right" rowspan="4">
                            <table cellpadding="2" cellspacing="0" class="npbody" width="100%">
                                <tr>
                                    <td align="center"><asp:HyperLink ID="lnkImage" runat="server"></asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td align="center"><np:PriceDisplay ID="prcPrice" runat="server" Visible='<%# Eval("ShowPrice") %>' Price='<%# Convert.ToDecimal(Eval("RefPrice")) %>'/></td>
                                </tr>
                                <tr>
                                    <td align="center"><asp:HyperLink ID="lnkAddToCart" ImageUrl="~/assets/common/icons/addtocart.gif" runat="server" Visible="False"></asp:HyperLink></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="npbody"><asp:HyperLink ID="lnkValue" runat="server" Visible="False"></asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td valign="top" class="npbody"><asp:HyperLink ID="lnkName" runat="server" Visible="False"></asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td valign="top" class="npbody"><asp:Label ID="lblRefDesc" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartCrossID") %>' CommandName="editcross"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartCrossID") %>' CommandName="remove"></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
    </tr>
</table>
<asp:Literal ID="hdnDeletionsPermanent" Visible="False" runat="server" Text="Deletions are permanent. Continue?"></asp:Literal>
<asp:Literal ID="ltlCrossSelf" Visible="False" runat="server" Text="You cannot cross-sell an item to itself"></asp:Literal>
