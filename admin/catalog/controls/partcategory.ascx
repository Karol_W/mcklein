<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartCategory" Codebehind="PartCategory.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %> 
<asp:Label ID="sysError" ForeColor="Red" runat="server"></asp:Label>
<table cellspacing="0" cellpadding="3" width="100%" border="0">
    <tr>
        <td class="npadminbody">
            <ComponentArt:TreeView id="tvList" ClientScriptLocation="~/scripts"
                DragAndDropEnabled="false" 
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView" 
                NodeCssClass="TreeNode" 
                SelectedNodeCssClass="SelectedTreeNode" 
                HoverNodeCssClass="HoverTreeNode"
                NodeEditCssClass="NodeEdit"
                LineImageWidth="19" 
                LineImageHeight="20"
                DefaultImageWidth="16" 
                DefaultImageHeight="16"
                ItemSpacing="0" 
                ImagesBaseUrl="~/assets/common/tree/"
                NodeLabelPadding="3"
                ParentNodeImageUrl="folder.gif" 
                LeafNodeImageUrl="file.gif" 
                ShowLines="true" 
                LineImagesFolderUrl="~/assets/common/tree/lines/"
                EnableViewState="true"
                Visible="false"
                runat="server" >
            </ComponentArt:TreeView>
        </td>
    </tr>
</table>
