﻿<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartMedia" Codebehind="PartMedia.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<table id="Table1" class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlImagePath" runat="server" Text="Image Path"></asp:Literal></td>
        <td class="npadminbody"><np:FilePicker ID="fpThumbnail" runat="server" Width="400px" StartDirectory="~/assets/catalog/parts/"></np:FilePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlThumbnailPath" runat="server" Text="Thumbnail Path"></asp:Literal></td>
        <td class="npadminbody"><np:FilePicker ID="fpStaticThumbnail" runat="server" Width="400px" StartDirectory="~/assets/catalog/parts/"></np:FilePicker></td>
    </tr>
</table>
<table id="Table2" class="npadminbody" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlHeader" runat="server" Text="Additional Images for Catalogs"></asp:Literal></td>
    </tr>
    <tr>
        <td>
            <asp:GridView 
                id="gvMedia"
                runat="server" 
                AutoGenerateColumns="false" 
                HeaderStyle-CssClass="npadminsubheader"
                CssClass="npadmintable" 
                RowStyle-CssClass="npadminbody" 
                AlternatingRowStyle-CssClass="npadminbodyalt" 
                EmptyDataText="No Records Found" OnRowCommand="gvMedia_RowCommand" OnRowDataBound="gvMedia_RowDataBound" OnRowUpdating="gvMedia_RowUpdating">
                <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="remove"></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colDisplayText|Display Text">
                        <ItemTemplate>
                            <div id='FormDiv<%# this.i %>'>
                                <asp:DropDownList runat="server" ID="CatalogCode" DataTextField="CatalogName" DataValueField="CatalogCode" />
                            </div>
                            <asp:TextBox ID="MediaDescription" runat="server" Columns="40"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colLink|Link">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                        <ItemTemplate>
                            <np:FilePicker ID="MediaLink" runat="server" Width="120px" StartDirectory="~/assets/catalog/parts/"></np:FilePicker>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colSort|Sort">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="SortOrder" runat="server" Columns="2"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="update"></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:Literal Visible="false" ID="ltlDeleteText" runat="server" Text="Deletions are permanent. Continue?"></asp:Literal>
