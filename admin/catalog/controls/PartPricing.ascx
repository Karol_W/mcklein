<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartPricing" Codebehind="PartPricing.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceTree" Src="~/admin/catalog/controls/partpricetree.ascx" %>
<table class="npadminbody" id="Table1" cellspacing="0" cellpadding="1" width="100%"
    border="0">
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBaseCurrency" runat="server" Text="Base Currency"></asp:Literal></td>
        <td colspan="2">
            <asp:DropDownList ID="ddlBaseCurrency" runat="server" Enabled="False"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlBasePrice" runat="server" Text="Base Price"></asp:Literal></td>
        <td colspan="2">
            <asp:TextBox ID="txtBasePrice" runat="server" Width="100px">0</asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBasePrice"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBasePrice"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator></td>
    </tr>
    <tr>
        <td class="npadminlabel">
            <asp:Literal ID="ltlOriginalPrice" runat="server" Text="Original Price"></asp:Literal></td>
        <td colspan="2">
            <asp:TextBox ID="txtOriginalPrice" runat="server" Width="100px">0</asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOriginalPrice"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtOriginalPrice"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator></td>
    </tr>
</table>

                <np:PriceTree ID="Pricelists" runat="server" />
<asp:label ID="sysError" runat="server" CssClass="npwarning"></asp:label>
<asp:Literal ID="hdnEndDate" Text="End date must be greater than start date" runat="server" Visible="false"></asp:Literal>
