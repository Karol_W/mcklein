<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartAvailability" Codebehind="PartAvailability.ascx.cs" %>
<div class="npwarning"><asp:Literal id="sysErr" runat="server"></asp:Literal></div>
<table class="npadminbody" id="tblType" cellspacing="0" cellpadding="1" width="100%"
    border="0">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlAvailableDate" runat="server" Text="Available Date"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><np:DatePicker id="dtAvailableDate" runat="server"></np:DatePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlExpirationDate" runat="server" Text="Expiration Date"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><np:DatePicker id="dtExpireDate" runat="server"></np:DatePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlReleaseDate" runat="server" Text="Release Date"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><np:DatePicker ID="dtReleaseDate" runat="server"></np:DatePicker></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlFlags" runat="server" Text="Flags"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><asp:CheckBox ID="chkAvailable" runat="server" Text="Available" Checked="True"></asp:CheckBox>&nbsp;
        <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured"></asp:CheckBox></td>
    </tr>
</table>
<asp:Literal ID="errDates" runat="server" Text="Enter an expiration date that is later than available date" Visible="false"></asp:Literal>
