<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartUnits" Codebehind="PartUnits.ascx.cs" %>
<table class="npadmintable">
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlPoundsPerPackage" runat="server" Text="Pounds per Package"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtPackageQty" runat="server" Width="100px" Text="1"></asp:textbox>
            <asp:RequiredFieldValidator ID="rfvPackageQty" runat="server" ControlToValidate="txtPackageQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPackageQty" runat="server" ControlToValidate="txtPackageQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlPackageUnit" runat="server" Text="Package Unit"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlPackageUnit" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
	    <td class="npadminlabel"><asp:literal id="ltlItemsPerSalesUoM" runat="server" Text="Items per Sales UoM"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtSalesQty" runat="server" Width="100px" Text="1"></asp:textbox>
            <asp:RequiredFieldValidator ID="rfvSalesQty" runat="server" ControlToValidate="txtSalesQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revSalesQty" runat="server" ControlToValidate="txtSalesQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlSalesUnit" runat="server" Text="Sales UoM"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlSalesUnit" runat="server"></asp:dropdownlist></td>
	</tr>	
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlArea" runat="server" Text="Area"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtAreaQty" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAreaQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAreaQty"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlAreaUnit" runat="server" Text="Area Unit"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlAreaUnit" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlDepth" runat="server" Text="Depth"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtDepth" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDepth"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDepth"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlWidth" runat="server" Text="Width"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtWidth" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtWidth"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtWidth"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlHeight" runat="server" Text="Height"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtHeight" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHeight"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtHeight"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlSizeUnit" runat="server" Text="Height Unit"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlSizeUnit" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlWeight1" runat="server" Text="Weight"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtWeight" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtWeight"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtWeight"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlWeightUnit" runat="server" Text="Weight Unit"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlWeightUnit" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlVolume" runat="server" Text="Volume"></asp:literal></td>
		<td class="npadminbody">
		    <asp:textbox id="txtVolume" runat="server" Width="100px" Text="0"></asp:textbox>
		    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtVolume"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationGroup="PartSave" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtVolume"
                CssClass="npwarning" ErrorMessage="*" ForeColor="" ValidationExpression="\d+\.?,?\d*" Display="Dynamic"
                ValidationGroup="PartSave"></asp:RegularExpressionValidator>
		</td>
		<td class="npadminlabel"><asp:literal id="ltlVolumeUnit" runat="server" Text="Volume Unit"></asp:literal></td>
		<td class="npadminbody"><asp:dropdownlist id="ddlVolumeUnit" runat="server"></asp:dropdownlist></td>
	</tr>
</table>
