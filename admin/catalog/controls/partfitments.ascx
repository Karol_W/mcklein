<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartFitments" Codebehind="PartFitments.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="MachinesBlock" Src="../../../catalog/controls/MachinesBlock.ascx" %>
<asp:Label id="sysError" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
<asp:panel id="pnlDetail" runat="server" width="100%" Visible="False">
	<table cellspacing="0" cellpadding="1" width="100%" border="0">
		<tr>
			<td class="npadminlabel"><asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:ImageButton></td>
			<td class="npadminlabel" align="right"><asp:ImageButton id="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Update"></asp:ImageButton></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlMachineCode" runat="server" Text="Master Assembly"></asp:Literal></td>
			<td class="npadminbody"><np:MachinesBlock id="machines" runat="server"></np:MachinesBlock></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlLocation" runat="server" Text="Location"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtPartLocation" runat="server" Width="400px" Columns="45"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlDirectLink" runat="server" Text="Direct Link"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDirectLink" runat="server" Width="400px" Columns="45"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtNotes" runat="server" Width="100%" rows="5" Columns="45" TextMode="MultiLine"></asp:TextBox></td>
		</tr>
	</table>
	<br />
</asp:panel>
<asp:GridView 
    id="gvFitments"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowCommand="gvFitments_RowCommand" OnRowDataBound="gvFitments_RowDataBound" OnRowEditing="gvFitments_RowEditing">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<HeaderStyle Width="5%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartsMachineID") %>' CommandName="remove">
				</asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="CatalogName" HeaderText="colCatalog|Catalog"></asp:BoundField>
		<asp:BoundField DataField="Description" HeaderText="colMakeModel|Make Model"></asp:BoundField>
		<asp:BoundField DataField="PartLocation" HeaderText="colLocation|Location"></asp:BoundField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartsMachineID") %>' CommandName="edit">
				</asp:ImageButton>
			</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Literal ID="ltlCatalogID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CatalogID") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
    </tr>
</table>
<asp:Literal id="hdnAnd" runat="server" Visible="False" Text="and"></asp:Literal>
<asp:Literal id="errIllegalPartMachineAssociation" runat="server" Visible="False" Text="have already been associated.<br>Please choose a different machine."></asp:Literal>
<asp:Literal id="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
<asp:Literal id="hdnRemove" runat="server" Visible="False" Text="Remove Item from Machine"></asp:Literal>
<asp:Literal id="hdnEdit" runat="server" Text="Edit" Visible="False"></asp:Literal>
<asp:Literal ID="hdnAdd" Runat="server" text="Add Item to Machine" Visible="false"></asp:Literal>
