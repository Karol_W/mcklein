<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="variant.ascx.cs" Inherits="netpoint.admin.catalog.controls.variant" %>
<%@ Register TagPrefix="np" TagName="PartPicker" Src="~/admin/common/controls/partpicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel" style="padding-left:50">
            <asp:literal Id="sysVariantDesc" runat="server"></asp:literal> - <%= hdnDefaultPrice.Text %>: <np:PriceDisplay ID="prcPrice" runat="server" />
        </td>
        <td class="npadminlabel" align="right">            
            <asp:ImageButton ID="btnRemove" runat="server" CommandName="delete" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnRemove_Click" />&nbsp;&nbsp;
            <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" ToolTip="Edit the variant category" />&nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:GridView ID="gridVariantParts" runat="server" AutoGenerateColumns="false" Width="500px" CssClass="npadmintable"
            OnRowDataBound="gridVariantParts_RowDataBound" ShowHeader="false">
                <AlternatingRowStyle CssClass="npadminbodyalt" />
                <RowStyle CssClass="npadminbody" />
                <HeaderStyle CssClass="npadminlabel" />
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="center" />
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDelete" runat="server" OnCommand="gridVariantParts_RowDelete" ImageUrl="~/assets/common/icons/remove.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartsVariantID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colDefault" HeaderStyle-Font-Bold="false">
                        <ItemStyle HorizontalAlign="center" />
                        <ItemTemplate>
                            <asp:Image ID="imgFlag" runat="server" ImageUrl="~/assets/common/icons/defaultyes.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item" HeaderStyle-Font-Bold="false">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkPartNo" runat="server" Text='<%# Bind("PartNo") %>'></asp:HyperLink>
                            -
                            <asp:Literal ID="ltlPart" runat="server" Text='<%# Bind("PartName") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colCostDiff" HeaderStyle-Font-Bold="false">
                        <ItemStyle HorizontalAlign="right" />
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcCostDiff" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="center" />
                        <ItemTemplate>
                            <asp:hyperlink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/detail.gif"></asp:hyperlink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td class="npadminlabel" align="right" colspan="2" style="padding-right:80">
            <asp:Literal ID="ltlAddPart" runat="server" Text="Add a part"></asp:Literal>&nbsp;
            <np:PartPicker ID="ppk" runat="server" />&nbsp;
            <asp:ImageButton ID="btnAddPart" runat="server" ImageUrl="~/assets/common/icons/add.gif" ToolTip="Add a part to the category" OnClick="btnAddPart_Click" />&nbsp;
        </td>
    </tr>
</table>
<asp:literal ID="hdnDelete" runat="server" Text="Delete part configuration" Visible="false"></asp:literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Text="Delete configuration data for this variant part?" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDefaultPrice" runat="server" Text="Default Price" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDeleteCat" runat="server" Text="Remove this category and disassociated parts?" visible="false"></asp:Literal>       