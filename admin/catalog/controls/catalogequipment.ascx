<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CatalogEquipment"
    Codebehind="CatalogEquipment.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="catalogequipmentmachine" Src="~/admin/catalog/controls/catalogequipmentmachine.ascx" %>
<asp:Panel runat="server" ID="pnlDetail" Visible="false">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel">
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" OnClick="btnCancel_Click" CausesValidation="false" ToolTip="Cancel" /></td>
            <td align="right" class="npadminlabel">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="btnSave_Click" ToolTip="Save" /></td>
        </tr>
    </table>
    <np:catalogequipmentmachine runat="server" ID="catalogequipmentmachine"></np:catalogequipmentmachine>
</asp:Panel>
<asp:Panel runat="Server" ID="pnlSearch">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlMakeFilter" runat="server" Text="Make"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtMakeFilter0" runat="server" Columns="10"></asp:TextBox>&nbsp;
                <asp:Literal ID="ltlOr" runat="server" Text="or"></asp:Literal>&nbsp;
                <asp:TextBox ID="txtMakeFilter1" runat="server" Columns="10"></asp:TextBox>&nbsp;
                <asp:Literal ID="sysOr1" runat="server" Text="or"></asp:Literal>&nbsp;
                <asp:TextBox ID="txtMakeFilter2" runat="server" Columns="10"></asp:TextBox>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlModelFilter" runat="server" Text="Model"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtModelFilter0" runat="server" Columns="10"></asp:TextBox>&nbsp;
                <asp:Literal ID="sysOr2" Text="or" runat="server"></asp:Literal>&nbsp;
                <asp:TextBox ID="txtModelFilter1" runat="server" Columns="10"></asp:TextBox>&nbsp;
                <asp:Literal ID="sysOr3" runat="server" Text="or"></asp:Literal>&nbsp;
                <asp:TextBox ID="txtModelFilter2" runat="server" Columns="10"></asp:TextBox>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlStartYearFilter" runat="server" Text="Start Year"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtYearFirst" runat="server" Columns="10"></asp:TextBox>&nbsp;
                <asp:Literal ID="ltlTo" runat="server" Text="to"></asp:Literal>&nbsp;
                <asp:TextBox ID="txtYearEnd" runat="server" Columns="10"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlMaxRecords" runat="server" Text="Maximum Records"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="sysMaxRec" runat="server">
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="100">100</asp:ListItem>
                    <asp:ListItem Value="200" Selected="True">200</asp:ListItem>
                    <asp:ListItem Value="500">500</asp:ListItem>
                </asp:DropDownList>&nbsp;
                <asp:CheckBox ID="chkUseWildcard" Text="Use Wildcard" runat="server" TextAlign="Left"></asp:CheckBox>&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btnSearchEquipment" runat="server" ImageUrl="~/assets/common/icons/search.gif"
                    OnClick="btnSearchEquipment_Click" ToolTip="Search Equipment" /></td>
        </tr>
    </table>
    <asp:GridView ID="gridMachine" runat="server" CssClass="npadmintable" AutoGenerateColumns="False"
        AllowPaging="True" AllowSorting="True" OnRowDataBound="gridMachine_RowDataBound" OnRowDeleting="gridMachine_RowDeleting" OnRowEditing="gridMachine_RowEditing" EmptyDataText="No Matching Machines Found" OnPageIndexChanging="gridMachine_PageIndexChanging" OnSorting="gridMachine_Sorting">
        <RowStyle CssClass="npadminbody"/>
        <AlternatingRowStyle CssClass="npadminbodyalt" />
        <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
        <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
        <Columns>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MachineID") %>'
                        CommandName="delete" ImageUrl="~/assets/common/icons/remove.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="MachineCode" HeaderText="colCode|Code"></asp:BoundField>
            <asp:BoundField DataField="Make" HeaderText="colMake|Make"></asp:BoundField>
            <asp:BoundField DataField="Model" HeaderText="colModel|Model"></asp:BoundField>
            <asp:BoundField DataField="StartYear" HeaderText="colStartYear|Start Year"></asp:BoundField>
            <asp:BoundField DataField="EndYear" HeaderText="colEndYear|End Year"></asp:BoundField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                        CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.MachineID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="npadminbody" HorizontalAlign="Right" />
    </asp:GridView>
    <br />
    <div class="npadminactionbar">
        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" />
    </div>
</asp:Panel>
<asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete Assembly"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Assembly"></asp:Literal>
<asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add Assembly"></asp:Literal>
