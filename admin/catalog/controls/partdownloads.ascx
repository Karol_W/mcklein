<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartDownloads" Codebehind="PartDownloads.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="FilePicker" Src="~/admin/common/controls/FilePicker.ascx" %>
<asp:Label ID="sysError" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
<asp:Panel ID="pnlDetail" runat="server" Width="100%" Visible="False">
    <table cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npadminlabel"><asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:ImageButton></td>
            <td class="npadminlabel" align="right"><asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Update"></asp:ImageButton></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlActive" runat="server" Text="Active"></asp:Literal></td>
            <td class="npadminbody"><asp:CheckBox ID="cbActive" runat="server" Checked="true" Text="Active"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtName" runat="server" Width="400px" Columns="45"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlVersion" runat="server" Text="Version"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtVersion" runat="server" Columns="25"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlLocation" runat="server" Text="Location"></asp:Literal></td>
            <td class="npadminbody"><np:FilePicker id="fpLocation" runat="server" Width="400px" StartDirectory="~/assets/" ControlName="fpLocation"></np:FilePicker></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
            <td class="npadminbody"><asp:TextBox ID="txtDescription" runat="server" Width="100%" Rows="5" Columns="45" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
    </table>
    <br />
</asp:Panel>
<asp:GridView 
    id="gvDownloads"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowCommand="gvDownloads_RowCommand" OnRowDataBound="gvDownloads_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.DownloadID") %>' CommandName="remove">
                </asp:ImageButton>
            </ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="DownloadName" HeaderText="colName|Name"></asp:BoundField>
		<asp:BoundField DataField="Version" HeaderText="colVersion|Version"></asp:BoundField>
		<asp:TemplateField HeaderText="colActive|Active">
            <HeaderStyle Width="5%"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton runat="server" ID="imgActive" ImageUrl="~/assets/common/icons/checked.gif" Visible="false" />
            </ItemTemplate>
        </asp:TemplateField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.DownloadID") %>' CommandName="editdown">
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
    </tr>
</table>
<asp:Literal ID="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
<asp:Literal ID="hdnRemove" runat="server" Visible="False" Text="Remove Download"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Text="Edit" Visible="False"></asp:Literal>
<asp:Literal ID="hdnAdd" runat="server" Text="Add Download" Visible="false"></asp:Literal>
