<%@ Control Language="C#" AutoEventWireup="true" Codebehind="catalogequipmentmachine.ascx.cs" Inherits="netpoint.admin.catalog.controls.catalogequipmentmachine" %>
<table class="npadmintable">
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlMachineCode" runat="server" Text="Machine Code"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtMachineCode" runat="server" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Machine Code is required" ControlToValidate="txtMachineCode">
            <asp:Image runat="server" ID="imgWarn1" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
            <asp:Literal ID="sysMachineCode" runat="server"></asp:Literal></td>
        <td class="npadminlabel"><asp:Literal ID="ltlManufacturer" runat="server" Text="Manufacturer"></asp:Literal></td>
        <td class="npadminbody"><asp:DropDownList ID="ddlManufacturer" runat="server"> </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlMake" runat="server" Text="Make"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtMake" runat="server" MaxLength="50"></asp:TextBox></td>
        <td class="npadminlabel"><asp:Literal ID="ltlModel" runat="server" Text="Model"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtModel" runat="server" MaxLength="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlStartYear" runat="server" Text="Start Year"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtStartYear" runat="server" MaxLength="4" Columns="4"></asp:TextBox></td>
        <td class="npadminlabel"><asp:Literal ID="ltlEndYear" runat="server" Text="End Year"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtEndYear" runat="server" MaxLength="4" Columns="4"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlSpecialFeatures" runat="server" Text="Special Features"></asp:Literal></td>
        <td class="npadminbody" colspan="3"><asp:TextBox ID="txtSpecialFeatures" runat="server" MaxLength="255" Columns="45" Width="500px"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlDimension1" runat="server" Text="Dimension 1"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtDimension1" runat="server" MaxLength="50"></asp:TextBox></td>
        <td class="npadminlabel"><asp:Literal ID="ltlDimension2" runat="server" Text="Dimension 2"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtDimension2" runat="server" MaxLength="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="npadminlabel"><asp:Literal ID="ltlDimension3" runat="server" Text="Dimension 3"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtDimension3" runat="server" MaxLength="50"></asp:TextBox></td>
        <td class="npadminlabel"><asp:Literal ID="ltlDimension4" runat="server" Text="Dimension 4"></asp:Literal></td>
        <td class="npadminbody"><asp:TextBox ID="txtDimension4" runat="server" MaxLength="50"></asp:TextBox></td>
    </tr>
</table>
<div class="npwarning"><asp:Literal ID="sysError" runat="server"></asp:Literal></div>
<asp:Literal ID="errDate" runat="server" Text="Enter an end year that is later than start year" Visible="false"></asp:Literal>
<asp:Literal ID="errCode" runat="server" Text="You selected a machine code that is already in use; choose another" Visible="false"></asp:Literal>

