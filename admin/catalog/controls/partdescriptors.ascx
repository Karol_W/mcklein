<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartDescriptors" Codebehind="PartDescriptors.ascx.cs" %>
<asp:Panel id="pnlDetail" runat="server" Visible="False" Width="100%">
	<table cellspacing="0" cellpadding="2" width="100%" border="0">
		<tr>
			<td class="npadminlabel"><asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:ImageButton></td>
			<td class="npadminlabel" align="right"><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Save"></asp:ImageButton>&nbsp;</td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlCatalogName" runat="server" Text="Catalog Name"></asp:Literal></td>
			<td class="npadminbody"><asp:DropDownList id="ddlCatalogName" runat="server"></asp:DropDownList></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlPartName" runat="server" Text="Item Name"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtPartName" Width="175px" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlPartType" runat="server" Text="Item Type"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtPartType" Width="175px" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:Literal id="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDescription" runat="server" rows="5" TextMode="MultiLine" Columns="45"></asp:TextBox></td>
		</tr>
	</table>
	<br />
</asp:Panel>
<asp:GridView 
    id="gvDescriptor"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowCommand="gvDescriptor_RowCommand" OnRowDataBound="gvDescriptor_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="../../assets/common/icons/delete.gif" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PCSID") %>'></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="CatalogName" HeaderText="colCatalog|Catalog"></asp:BoundField>
		<asp:BoundField DataField="PartName" HeaderText="ItemName|Item Name"></asp:BoundField>
		<asp:BoundField DataField="PartType" HeaderText="ItemType|Item Type"></asp:BoundField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnEdit" runat="server" ImageUrl="../../assets/common/icons/edit.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PCSID") %>' CommandName="editdesc"></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
    </tr>
</table>
<asp:Literal id="hdnDeletionsPermanent" Visible="False" runat="server" Text="Deletions are permanent. Continue?"></asp:Literal>
