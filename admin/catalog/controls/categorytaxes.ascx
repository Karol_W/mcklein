<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CategoryTaxes" Codebehind="CategoryTaxes.ascx.cs" %>
<asp:Repeater id="rptTaxes" runat="server">
	<HeaderTemplate>
		<table width="100%" cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td colspan="5" class="tabselected">
				    <br>
					<asp:Literal id="ltlHeader" runat="server" Text="Tax assignment and removal effects all parts in this category and its sub-categories. <br />Choose an action for the appropriate tax and click save to apply the changes."></asp:Literal><br>
					<br>
				</td>
			</tr>
			<tr>
				<td class="npadminsubheader"><asp:Literal id="colTaxCode" runat="server" Text="Tax Code"></asp:Literal></td>
				<td class="npadminsubheader"><asp:Literal id="colTaxName" runat="server" Text="Name"></asp:Literal></td>
				<td class="npadminsubheader"><asp:Literal id="colType" runat="server" Text="Type"></asp:Literal></td>
				<td class="npadminsubheader">&nbsp;</td>
				<td class="npadminsubheader"><asp:Literal id="ltlResultLabel" runat="server" Text="Results"></asp:Literal></td>
			</tr>
	</HeaderTemplate>
	<ItemTemplate>
		    <tr>
			    <td class="npadminbody"><asp:Literal id="ltlTaxCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TaxCode") %>'></asp:Literal></td>
			    <td class="npadminbody"><%# DataBinder.Eval(Container, "DataItem.TaxName") %></td>
			    <td class="npadminbody"><%# DataBinder.Eval(Container, "DataItem.TaxTypeName") %></td>
			    <td class="npadminbody">
				    <asp:RadioButtonList id="rblAction" runat="server" RepeatDirection="Horizontal" Width="100%" RepeatLayout="Table"
					    CssClass="npadminbody">
					    <asp:ListItem Value="assign">Make all items liable</asp:ListItem>
					    <asp:ListItem Value="remove">Make all items exempt</asp:ListItem>
					    <asp:ListItem Value="leave" Selected="True">Do Nothing</asp:ListItem>
				    </asp:RadioButtonList>
			    </td>
			    <td class="npadminbody"><asp:Literal id="ltlResult" runat="server"></asp:Literal>&nbsp;</td>
		    </tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
