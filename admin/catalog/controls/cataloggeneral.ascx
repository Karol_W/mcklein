<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CatalogGeneral" Codebehind="CatalogGeneral.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlCatalogCode" runat="server" Text="Catalog Code"></asp:literal></td>
		<td class="npadminbody"><asp:Literal ID="sysCatalogCode" Runat="server" Visible="False"></asp:Literal>
		<asp:textbox id="txtCatalogCode" runat="server"></asp:textbox>
		<asp:requiredfieldvalidator id="rfvCatalogCode" runat="server" ControlToValidate="txtCatalogCode" ErrorMessage="RequiredFieldValidator">
				<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator>
			<asp:HyperLink id="lnkCopy" runat="server" ToolTip="Copy" ImageUrl="~/assets/common/icons/duplicate.gif"
				NavigateUrl="~/admin/catalog/CatalogCopy.aspx"></asp:HyperLink>
			<asp:Label id="sysError" runat="server" CssClass="npwarning"></asp:Label></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlCatalogName" runat="server" Text="Catalog Name"></asp:literal></td>
		<td class="npadminbody"><asp:textbox id="txtCatalogName" runat="server" Width="400px" Rows="40"></asp:textbox>
		<asp:requiredfieldvalidator id="rfvCatalogName" runat="server" ControlToValidate="txtCatalogName" ErrorMessage="RequiredFieldValidator">
				<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlSortOrder" runat="server" Text="Sort Order"></asp:literal></td>
		<td class="npadminbody"><asp:textbox id="txtSortOrder" runat="server"></asp:textbox>
		<asp:requiredfieldvalidator id="rfvSortCode" runat="server" ControlToValidate="txtSortOrder">
				<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlViewOnly" runat="server" text="View Only"></asp:literal></td>
		<td class="npadminbody"><asp:checkbox id="sysViewOnly" runat="server"></asp:checkbox></td>
	</tr>
    <tr>
		<td class="npadminlabel"><asp:literal id="ltlSiteMap" runat="server" text="Enable for SiteMap"></asp:literal></td>
		<td class="npadminbody"><asp:checkbox id="sysSiteMap" runat="server"></asp:checkbox></td>
	</tr>
</table>
<asp:Literal id="errCodeExists" Text="The catalog code you selected already exists; choose another"
	runat="server" Visible="False"></asp:Literal>
