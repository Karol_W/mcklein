<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartAttributes" Codebehind="PartAttributes.ascx.cs" %>
<asp:panel id="pnlDetail" Width="100%" Visible="False" runat="server">
	<table id="tbldetail" cellspacing="0" cellpadding="1" width="100%" border="0" runat="server">
		<tr id="rowheader">
			<td class="npadminlabel"><asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:ImageButton></td>
			<td class="npadminlabel" align="right"><asp:ImageButton id="btnUpdate" runat="server" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Update"></asp:ImageButton></td>
		</tr>
		<tr id="rowcode">
			<td class="npadminlabel"><asp:Literal id="ltlAttributeCode" runat="server" Text="Attribute"></asp:Literal></td>
			<td class="npadminbody">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td><div id="formdiv1" style="POSITION: relative">
							<asp:DropDownList id="ddlAttributeCode" runat="server" AutoPostBack="True" 
							    onselectedindexchanged="ddlAttributeCode_SelectedIndexChanged"></asp:DropDownList>
							<asp:Image ID="sysAttributeError" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" />
						    </div>
						</td>
						<td><img height="25" src="" width="1" alt="" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="rowdim1">
			<td class="npadminlabel"><asp:Literal id="sysDimension1" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension1" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim2">
			<td class="npadminlabel"><asp:Literal id="sysDimension2" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension2" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim3">
			<td class="npadminlabel"><asp:Literal id="sysDimension3" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension3" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim4">
			<td class="npadminlabel"><asp:Literal id="sysDimension4" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension4" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim5">
			<td class="npadminlabel"><asp:Literal id="sysDimension5" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension5" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim6">
			<td class="npadminlabel"><asp:Literal id="sysDimension6" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension6" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim7">
			<td class="npadminlabel"><asp:Literal id="sysDimension7" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension7" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim8">
			<td class="npadminlabel"><asp:Literal id="sysDimension8" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension8" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
		<tr id="rowdim9">
			<td class="npadminlabel"><asp:Literal id="sysDimension9" runat="server"></asp:Literal></td>
			<td class="npadminbody"><asp:TextBox id="txtDimension9" Width="400px" runat="server"></asp:TextBox></td>
		</tr>
	</table>
	<br />
</asp:panel>
<asp:GridView 
    id="gvAttributes"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Attributes Found" 
    OnRowCommand="gvAttributes_RowCommand" 
    OnRowDataBound="gvAttributes_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="remove" 
				    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.AttributeID") %>' />
			</ItemTemplate>
			<FooterStyle HorizontalAlign="Center"></FooterStyle>
		</asp:TemplateField>
		<asp:BoundField DataField="AttributeName" HeaderText="colAttributeName|Attribute Name"></asp:BoundField>
		<asp:BoundField DataField="Dimension1" HeaderText="colDimension|Dimension"></asp:BoundField>
		<asp:BoundField DataField="Dimension2" HeaderText="colDimension|Dimension"></asp:BoundField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" CommandName="editatt" 
				    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.AttributeID") %>' />
			</ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
<table width="100%">
    <tr>
        <td align="right"><asp:ImageButton id="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
    </tr>
</table>
<asp:literal id="errDeletionsPermanent" Visible="False" runat="server" Text="Deletions are permanent. Continue?"></asp:literal>
<asp:Literal ID="errDuplicateAttribute" Visible="false" runat="server" Text="An attribute can only be used one time per item."></asp:Literal>