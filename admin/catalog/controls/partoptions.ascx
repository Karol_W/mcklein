<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartOptions"
    Codebehind="PartOptions.ascx.cs" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0" class="npadminbody">
    <tr>
        <td>
            <asp:CheckBox ID="chkMedia" runat="server" Text="Media"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkNotes" runat="server" Text="Notes"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkDimensions" runat="server" Text="Dimensions"></asp:CheckBox></td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox ID="chkDescription" runat="server" Text="Description"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkManufacturer" runat="server" Text="Manufacturer"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkAttributes" runat="server" Text="Attributes"></asp:CheckBox></td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox ID="chkOrderNotes" runat="server" Text="Order Notes"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkQuantityTXT" runat="server" Text="Quantity Box"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkQuantityDDL" runat="server" Text="Quantity Dropdown List"></asp:CheckBox></td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox ID="chkRemarks" runat="server" Text="Remarks"></asp:CheckBox></td>
        <td>
            <asp:CheckBox ID="chkVolumeDiscounts" runat="server" Text="Volume Discounts"></asp:CheckBox></td>
        <td>
            &nbsp;</td>
    </tr>
</table>
<br />
<asp:Label runat="server" ID="lblTemplateFile" Text="Layout Template" CssClass="npadminbody"></asp:Label>
<asp:DropDownList runat="Server" ID="ddlTemplateFile">
</asp:DropDownList>