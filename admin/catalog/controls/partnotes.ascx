<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartNotes" Codebehind="PartNotes.ascx.cs" %>
<asp:GridView 
    id="gvNotes"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Attributes Found" OnRowDataBound="gvNotes_RowDataBound" OnRowUpdating="gvNotes_RowUpdating" OnRowDeleting="gvNotes_RowDeleting">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif" CommandName="delete"></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colText|Text">
			<ItemTemplate>
				<div id='FormDiv<%# this.i %>' >
					<asp:DropDownList runat="server" id="CatalogCode" DataTextField="CatalogName" DataValueField="CatalogCode" />
				</div>
				<asp:TextBox id="txtNote" runat="server" Columns="50" rows="4" TextMode="MultiLine"></asp:TextBox>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colSortOrder|Sort Order">
			<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
			<ItemTemplate>
				<asp:TextBox id="txtSortOrder" runat="server" Columns="4"></asp:TextBox>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/save.gif" CommandName="update"></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
