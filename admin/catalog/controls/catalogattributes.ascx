<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.CatalogAttributes" Codebehind="CatalogAttributes.ascx.cs" %>
<div class="npwarning"><asp:Literal ID="sysErr" runat="server" Text=""></asp:Literal></div>
<asp:Panel ID="pnlDetail" Visible="False" runat="server" Width="100%">
    <table class="npadmintable">
        <tr>
            <td class="npadminlabel">
                <asp:HyperLink ID="lnkCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:HyperLink></td>
            <td class="npadminlabel" align="right">
                <asp:ImageButton ID="btnUpdate" runat="server" EnableViewState="False" ImageUrl="~/assets/common/icons/save.gif" ToolTip="Update" /></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAttrCode" runat="server" Text="Attr. Code"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtCode" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtCode" runat="server" ControlToValidate="txtCode"
                    ErrorMessage="RequiredFieldValidator">
				<asp:Image runat="server" ID="imgWarn1" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="ltlAttrName" runat="server" Text="Attr. Name"></asp:Literal></td>
            <td class="npadminbody">
                <asp:TextBox ID="txtName" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ControlToValidate="txtName"
                    ErrorMessage="RequiredFieldValidator">
				<asp:Image runat="server" ID="imgWarn2" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim1" runat="server"></asp:Literal>&nbsp;1</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension1" runat="server" EnableViewState="False" Width="350"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay1" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker1" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim2" runat="server"></asp:Literal>&nbsp;2</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension2" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay2" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker2" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim3" runat="server"></asp:Literal>&nbsp;3</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension3" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay3" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker3" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim4" runat="server"></asp:Literal>&nbsp;4</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension4" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay4" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker4" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim5" runat="server"></asp:Literal>&nbsp;5</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension5" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay5" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker5" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim6" runat="server"></asp:Literal>&nbsp;6</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension6" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay6" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker6" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim7" runat="server"></asp:Literal>&nbsp;7</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension7" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay7" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker7" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim8" runat="server"></asp:Literal>&nbsp;8</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension8" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay8" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker8" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="npadminlabel">
                <asp:Literal ID="sysDim9" runat="server"></asp:Literal>&nbsp;9</td>
            <td class="npadminbody">
                <asp:TextBox ID="txtDimension9" runat="server" EnableViewState="False" Width="350px"></asp:TextBox>&nbsp;
                <asp:CheckBox ID="sysDisplay9" runat="server" Text="Display"></asp:CheckBox>&nbsp;
                <asp:CheckBox ID="sysPicker9" runat="server" Text="Picker"></asp:CheckBox></td>
        </tr>
    </table>
    <br />
</asp:Panel>
<asp:GridView 
    id="gvAttributes"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Attributes Found" OnRowCommand="gvAttributes_RowCommand" OnRowDataBound="gvAttributes_RowDataBound" OnRowEditing="gvAttributes_RowEditing">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AttributeID") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="AttributeCode" HeaderText="colCode|Code"></asp:BoundField>
        <asp:BoundField DataField="AttributeName" HeaderText="colName|Name"></asp:BoundField>
        <asp:BoundField DataField="DimLabel1" HeaderText="colDimension1|Dimension"></asp:BoundField>
        <asp:BoundField DataField="DimLabel2" HeaderText="colDimension2|Dimension"></asp:BoundField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.AttributeID") %>' CommandName="Editthis" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<br />
<div class="npadminactionbar">
    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" />
</div>
<asp:Literal ID="hdnDisplay" Visible="False" runat="server" Text="Display"></asp:Literal>
<asp:Literal ID="hdnPicker" Visible="False" runat="server" Text="Picker"></asp:Literal>
<asp:Literal ID="hdnDimension" Visible="False" runat="server" Text="Dimension"></asp:Literal>
<asp:Literal ID="errDeleteWarning" runat="server" Visible="False" Text="Deleting this attribute will delete the attribute from all items that contain it. Continue?"></asp:Literal>
<asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete Attribute"></asp:Literal>
<asp:Literal ID="hdnAdd" runat="server" Visible="False" Text="Add Attribute"></asp:Literal>
<asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit Attribute"></asp:Literal>
<asp:Literal ID="hdnUnique" runat="server" Visible="false" Text="Attribute with Code Exists"></asp:Literal>
<asp:Literal ID="hdnDimensions" runat="server" Visible="false" Text="Total Dimensions must match dimensions on page"></asp:Literal>
