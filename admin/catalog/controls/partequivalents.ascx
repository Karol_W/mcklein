<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartEquivalents" Codebehind="PartEquivalents.ascx.cs" %>
<asp:Label id="sysError" runat="server" ForeColor="Red"></asp:Label>
<asp:GridView 
    id="gvEquiv"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Equivalents Found" OnRowCommand="gvEquiv_RowCommand" OnRowDataBound="gvEquiv_RowDataBound" OnRowEditing="gvEquiv_RowEditing">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="colDelete|Delete">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/assets/common/icons/delete.gif"></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colManufacturer|Manufacturer">
			<ItemTemplate>
				<div id='FormDiv<%# this.i %>'>
					<asp:DropDownList id="ddlMNFCode" runat="server"></asp:DropDownList>
				</div>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colManufacturerPartNo|Manufacturer Item No.">
			<ItemTemplate>
				<asp:TextBox id="txtMNFPartNo" runat="server"></asp:TextBox>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="colSave|Save">
			<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/save.gif"></asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField Visible="false">
		    <ItemTemplate>
		        <asp:Literal ID="ltlFudge" runat="server"></asp:Literal>
		    </ItemTemplate>
		</asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:Literal id="hdnDeletionsPermanent" runat="server" Visible="False" Text="Deletions are permanent. Continue?"></asp:Literal>
