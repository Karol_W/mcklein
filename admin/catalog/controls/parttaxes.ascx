<%@ Control Language="c#" Inherits="netpoint.admin.catalog.controls.PartTaxes" Codebehind="PartTaxes.ascx.cs" %>
<asp:GridView 
    id="gvTaxes"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npadminsubheader"
    CssClass="npadmintable" 
    RowStyle-CssClass="npadminbody" 
    AlternatingRowStyle-CssClass="npadminbodyalt" 
    EmptyDataText="No Records Found" OnRowDataBound="gvTaxes_RowDataBound">
    <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
    <Columns>
		<asp:BoundField DataField="TaxCode" HeaderText="colTaxCode|Tax Code"></asp:BoundField>
		<asp:BoundField DataField="TaxName" HeaderText="colTaxName|Tax Name"></asp:BoundField>
		<asp:BoundField DataField="TaxTypeName" HeaderText="colType|Type"></asp:BoundField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:CheckBox id="chkApplies" runat="server"></asp:CheckBox>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>