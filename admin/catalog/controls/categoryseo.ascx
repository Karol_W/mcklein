﻿<%@ Control Language="C#" CodeBehind="categoryseo.ascx.cs" Inherits="netpoint.admin.catalog.controls.categoryseo" %>
<%@ Register TagPrefix="NPWC" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Label id="sysError" runat="server" CssClass="npwarning"></asp:Label>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">	
    <tr>
		<td class="npadminlabel"><asp:literal id="ltlPageTitle" runat="server" Text="Page Title"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtPageTitle" runat="server" Columns="55" Width="350px" MaxLength="500"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlFriendlyURL" runat="server" Text="Friendly URL"></asp:literal></td>
		<td class="npadminbody" colspan="3"><asp:textbox id="txtFriendlyURL" runat="server" Columns="55" Width="350px" MaxLength="255"></asp:textbox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlMetaDescription" runat="server" Text="MetaTag Description"></asp:literal></td>
		<td class="npadminbody" colspan="3"><NPWC:MultiLineTextBox id="txtMetaDescription" runat="server" LimitInput="2000" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></NPWC:MultiLineTextBox></td>
	</tr>
	<tr>
		<td class="npadminlabel"><asp:literal id="ltlMetaKeywords" runat="server" Text="MetaTag Keywords"></asp:literal></td>
		<td class="npadminbody" colspan="3"><NPWC:MultiLineTextBox id="txtMetaKeywords" runat="server" LimitInput="2000" rows="4" TextMode="MultiLine" Height="75px" Columns="45"></NPWC:MultiLineTextBox></td>
	</tr>	
</table>
<asp:Literal ID="ltlErr" Text="The friendly url you selected already exists; choose another" runat="server" Visible="false"></asp:Literal>
