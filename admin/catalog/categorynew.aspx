<%@ Register TagPrefix="uc1" TagName="NPDatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.CategoryNew" Codebehind="categorynew.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right"><asp:ImageButton ID="btnSaveAndReturn" runat="server"
                ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveAndReturn_Click" ToolTip="Save and Return"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div class="npadminbody">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader" colspan="2">&nbsp;<asp:Image ID="imgIndicator" runat="server" ImageUrl="~/assets/common/icons/indicator.gif" />
                    &nbsp;<asp:Literal ID="ltlHeader" runat="server" Text="Create Category"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlCategoryCode" runat="server" Text="Category Code"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtCategoryCode" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCategoryCode" runat="server" ControlToValidate="txtCategoryCode" ErrorMessage="RequiredFieldValidator">
                        <asp:Image runat="server" ID="err1" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
                    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlCategoryName" runat="server" Text="Category Name"></asp:Literal></td>
                <td class="npadminbody">
                    <asp:TextBox ID="txtCategoryName" runat="server" Rows="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName" ErrorMessage="RequiredFieldValidator">
                        <asp:Image runat="server" ID="err2" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlParentCategory" runat="server" Text="Parent Category"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlParentCategory" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errCodeExists" Text="Category code already exists; choose another" runat="server" Visible="False"></asp:Literal>
</asp:Content>
