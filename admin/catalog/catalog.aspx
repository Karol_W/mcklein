<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.Catalog" Codebehind="Catalog.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="attributes" Src="~/admin/catalog/controls/CatalogAttributes.ascx" %>
<%@ Register TagPrefix="np" TagName="equipment" Src="~/admin/catalog/controls/CatalogEquipment.ascx" %>
<%@ Register TagPrefix="np" TagName="general" Src="~/admin/catalog/controls/CatalogGeneral.ascx" %>
<%@ Register TagPrefix="np" TagName="mnf" Src="~/admin/catalog/controls/CatalogManufacturers.ascx" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:ImageButton ID="btnDeleteAll" runat="server" ImageUrl="~/assets/common/icons/delete.gif" OnClick="btnDeleteAll_Click" ToolTip="Delete All"></asp:ImageButton>
                &nbsp;
                <asp:ImageButton ID="btnSaveAll" runat="server" ToolTip="Save All and Close" ImageUrl="~/assets/common/icons/saveandclose.gif" OnClick="btnSaveAll_Click"></asp:ImageButton>
                &nbsp;
            </td>
        </tr>
    </table>
    <ComponentArt:TabStrip ID="CatalogTabStrip" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
        MultiPageId="CatalogMultiPage" Width="100%" runat="server" DefaultGroupCssClass="TopGroupTabs"
        DefaultItemLookId="TopLevelTabLook" DefaultSelectedItemLookId="SelectedTopLevelTabLook"
        DefaultChildSelectedItemLookId="SelectedTopLevelTabLook" DefaultGroupTabSpacing="0">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="TopLevelTabLook" CssClass="TopLevelTab" HoverCssClass="TopLevelTabHover"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
            <ComponentArt:ItemLook LookId="SelectedTopLevelTabLook" CssClass="SelectedTopLevelTab"
                LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="5" LabelPaddingBottom="4" />
        </ItemLooks>
        <Tabs>
            <ComponentArt:TabStripTab runat="server" ID="ts1" Text="General"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts2" Text="Manufacturers"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts3" Text="Attributes"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab runat="server" ID="ts4" Text="Equipment"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="CatalogMultiPage" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts"
        Width="100%" Height="400">
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:general ID="general" runat="server"></np:general>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:mnf ID="mnf" runat="server"></np:mnf>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:attributes ID="attributes" runat="server"></np:attributes>
        </ComponentArt:PageView>
        <ComponentArt:PageView CssClass="npadminbody" runat="server">
            <np:equipment ID="equipment" runat="server"></np:equipment>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
</asp:Content>
<asp:Content runat="Server" ID="hidden" ContentPlaceHolderID="hiddenslot">
    <asp:Literal ID="errDeleteCatalogWarning" runat="server" Visible="False" Text="The following items will be deleted: \n - Attribute definitions from this catalog \n - Attribute assignments from this catalog \n - Saved user equipment from this catalog \n - Item equipment definitions from this catalog \n - Catalog manufacturer assignments \n Any themes using this catalog will be unable to view a catalog.  Continue?"></asp:Literal>
</asp:Content>
