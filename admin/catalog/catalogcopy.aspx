<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.catalog.CatalogCopy" Codebehind="CatalogCopy.aspx.cs" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
    <table class="npadmintable">
        <tr>
            <td class="npadminheader">&nbsp;</td>
            <td class="npadminheader" align="right">
                 <asp:imagebutton id="btnSave" runat="server" imageurl="~/assets/common/icons/saveandclose.gif" tooltip="Copy Catalog"></asp:imagebutton>&nbsp;
                 <asp:hyperlink id="lnkCancel" runat="server" imageurl="~/assets/common/icons/cancel.gif" ToolTip="Cancel"></asp:hyperlink>
            </td>
        </tr>
    </table>
    <table class="npadmintable">
        <tr>
            <td class="npadminsubheader"><asp:literal id="ltlHeader" runat="server" text="Copy Catalog"></asp:literal></td>
        </tr>
      </table>
      <table class="npadmintable">
        <tr>
            <td class="npadminlabel"> <asp:literal id="ltlOrigCatalogCode" runat="server" text="Original Code"></asp:literal></td>
            <td><asp:label runat="server" id="sysOrigCatalogCode" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlOrigCatalogName" runat="server" text="Original Name"></asp:literal></td>
            <td><asp:label runat="server" id="sysOrigCatalogName" /></td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlCatalogCode" runat="server" text="Catalog Code"></asp:literal></td>
            <td>
                <asp:textbox id="txtCatalogCode" runat="server" columns="20"></asp:textbox>
                <asp:requiredfieldvalidator id="rfvCode" runat="server" errormessage="RequiredFieldValidator" controltovalidate="txtCatalogCode">
					<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td class="npadminlabel"><asp:literal id="ltlCatalogName" runat="server" text="Catalog Name"></asp:literal></td>
            <td>
                <asp:textbox id="txtCatalogName" runat="server" columns="40"></asp:textbox>
                <asp:requiredfieldvalidator id="rfvName" runat="server" errormessage="RequiredFieldValidator" controltovalidate="txtCatalogName">
					<img src="../../assets/common/icons/warning.gif" alt="" /></asp:requiredfieldvalidator>
            </td>
        </tr>
    </table>
    <asp:label id="sysError" runat="server" CssClass="npwarning"></asp:label>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
    <asp:literal id="errCodeExists" runat="server" text="Catalog code already exists; choose another" visible="False"></asp:literal>
</asp:Content>
