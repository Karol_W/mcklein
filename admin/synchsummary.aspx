﻿<%@ Page MasterPageFile="~/masters/admin.master" Language="C#" CodeBehind="synchsummary.aspx.cs" Inherits="netpoint.admin.synchsummary" %>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="mainslot">
<div>
    <table class="npadmintable">
        <tr class="npadminsubheader">
            <td>
                <asp:Literal runat="server" Text="Website to SBO Synch Summary" />                
            </td>
        </tr>
        <tr>
            <td>
                <asp:gridview ID="WebGrid" runat="server" AllowPaging="False" AllowSorting="False" CssClass="npadmintable" EmptyDataText="No Records Found" Width="100%"
                    AutoGenerateColumns="False" OnRowDataBound="WebGrid_RowDataBound">
                    <RowStyle CssClass="npadminbody" />
                    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <Columns>
                        <asp:BoundField DataField="SynchObject" HeaderText="Synch Object|Synch Object" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>                       
                        <asp:BoundField DataField="Paused" HeaderText="Paused|Paused" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Failed" HeaderText="Failed|Failed" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Skip" HeaderText="Skip|Skip" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>                        
                    </Columns>                    
                </asp:gridview>
            </td>
        </tr>
    </table>
    <div id="WhiteSpace" />
    <table class="npadmintable">
        <tr class="npadminsubheader">
            <td>
                <asp:Literal ID="Literal1" runat="server" Text="SBO to Website Synch Summary" />                
            </td>
        </tr>
        <tr>
            <td>
                <asp:gridview ID="SBOGrid" runat="server" AllowPaging="False" AllowSorting="False" CssClass="npadmintable" EmptyDataText="No Records Found" Width="100%"
                    AutoGenerateColumns="False" OnRowDataBound="SBOGrid_RowDataBound">
                    <RowStyle CssClass="npadminbody" />
                    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
                    <AlternatingRowStyle CssClass="npadminbodyalt" />
                    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
                    <Columns>
                        <asp:BoundField DataField="SynchObject" HeaderText="Synch Object|Synch Object" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>                       
                        <asp:BoundField DataField="Paused" HeaderText="Paused|Paused" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Failed" HeaderText="Failed|Failed" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Skip" HeaderText="Skip|Skip" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center"></asp:BoundField>                        
                    </Columns>                    
                </asp:gridview>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
</asp:Content>
