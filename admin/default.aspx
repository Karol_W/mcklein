<%@ Page language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.adminlandingpage" CodeBehind="default.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="LandingPage" Src="~/admin/common/controls/landingpage.ascx" %>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">&nbsp;</td>
        </tr>
    </table>
    <asp:Literal ID="sysMessage" runat="server"></asp:Literal>
    <np:LandingPage ID="lpage" runat="server" />
</asp:Content>
<asp:Content ID="hidden" runat="server" ContentPlaceHolderID="hiddenslot">
		<asp:Literal id="hdnUnauthorized" runat="server" Text="You are not authorized to view the requested page"
			Visible="False"></asp:Literal>
</asp:Content>


