<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.menus.MenuStrings"
    Codebehind="MenuStrings.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="CultureObjectStrings" Src="~/admin/common/controls/CultureObjectStrings.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
            <td class="npadminheader" align="right">
                <asp:Literal ID="sysHeader" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="npadminbody">
        <np:CultureObjectStrings ID="cos" runat="server"></np:CultureObjectStrings>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnAlternateLanguageText" runat="server" Text="Alternate language text for" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnMenuName" runat="server" Text="Menu Name" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDescription" runat="server" Text="Menu Description" Visible="false"></asp:Literal>
</asp:Content>
