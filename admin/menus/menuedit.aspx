<%@ Page Language="c#" MasterPageFile="~/masters/admin.master" Inherits="netpoint.admin.menus.MenuEdit" Codebehind="MenuEdit.aspx.cs" %>
<asp:Content ID="title" runat="server" ContentPlaceHolderID="TitleTag">
<title>Menu Builder</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npadminpath">
        <tr>
            <td class="npadminheader"><asp:Literal ID="Literal1" runat="server" Text="Add, edit and delete Administration menu items" /></td>
            <td class="npadminheader" align="right">            
            <asp:ImageButton  ID="btnAddNew" runat="server" ToolTip="Add New Menu Item" ImageUrl="~/assets/common/icons/add.gif"></asp:ImageButton>&nbsp;</td>
        </tr>
    </table>
    <div id="divDetail" runat="server" visible="false">
        <table class="npadmintable">
            <tr>
                <td class="npadminsubheader"><asp:ImageButton ID="imgCancel" runat="server" ImageURL="~/assets/common/icons/cancel.gif" OnClick="imgCancel_Click" /></td>
                <td align="right" class="npadminsubheader"><asp:ImageButton ID="imgSave" runat="server" ImageUrl="~/assets/common/icons/save.gif" OnClick="imgSave_Click" /></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlURL" runat="server" Text="Link"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtLink" runat="server" Width="300px"></asp:TextBox>
                    <asp:Literal ID="ltlLink" runat="server" Text="Link"></asp:Literal></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlName" Text="Text" runat="server"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtText" runat="server"></asp:TextBox>
                <asp:ImageButton ID="btnNameLanguage" runat="server" ToolTip="Alternate Language" ImageUrl="~/assets/common/icons/language.gif" OnClick="btnNameLanguage_Click"></asp:ImageButton></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
                <td class="npadminbody"><asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlSortOrder" runat="server" Text="Sort Order"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtSortOrder" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="npadminlabel"><asp:Literal ID="ltlDesc" runat="server" Text="Description"></asp:Literal></td>
                <td class="npadminbody"><asp:TextBox ID="txtDesc" runat="server" Width="300px" Rows="3" TextMode="MultiLine"></asp:TextBox>
                <asp:ImageButton ID="btnDescLanguage" runat="server" ToolTip="Alternate Language" ImageUrl="~/assets/common/icons/language.gif" OnClick="btnDescLanguage_Click"></asp:ImageButton></td>
            </tr>
        </table>
        <asp:Literal ID="ltlMenuItemID" runat="server" Visible="false" Text="Menu Item ID"></asp:Literal>
    </div>
    <div id="divGrid" runat="server" class="npadminbody">
    <table class="npadmintable">
        <tbody>
            <tr>
                <td colspan="2" class="npadminbody">
                     <asp:GridView 
                        id="gvMenus"
                        runat="server" 
                        AllowPaging="true"
                        PageSize="20"
                        AutoGenerateColumns="false" 
                        HeaderStyle-CssClass="npadminsubheader"
                        CssClass="npadmintable" 
                        RowStyle-CssClass="npadminbody" 
                        AlternatingRowStyle-CssClass="npadminbodyalt" 
                        EmptyDataText="No Records Found" 
                        OnRowDataBound="gvMenus_RowDataBound" 
                        OnRowCommand="gvMenus_RowCommand">
                        <EmptyDataRowStyle CssClass="npadminempty" Height="50px" />
                        <Columns> 
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif"
                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.MenuItemID") %>' ToolTip="Delete this menu item.">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colText|Text">
                                <ItemTemplate>
                                    <asp:Literal ID="ltlName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colLink|Link">
                                <ItemTemplate>
                                    <asp:Literal ID="ltlHREF" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HREF") %>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colType|Type">
                                <ItemTemplate>
                                    <asp:Literal ID="ltlMenuType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MenuItemTypeName") %>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colDetail|Detail">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkDetail" runat="server" ImageUrl="~/assets/common/icons/detail.gif"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colOrder|Order">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlSortOrder" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SortOrder") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="colEdit|Edit" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="False">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                                        CommandName="editthis" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.MenuItemID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>   
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2" class="npadminbody">
                    <asp:Literal ID="sysLevel" runat="server" Visible="False"></asp:Literal>&nbsp;</td>
            </tr>
        </tbody>
    </table>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnDeleteMenuItem" runat="server" Visible="False" Text="This Menu Item will be deleted along with all of its children. \n Continue?"></asp:Literal>
    <asp:Literal ID="hdnAlternateLanguage" runat="server" Visible="False" Text="Alternate Language"></asp:Literal>
    <asp:Literal ID="hdnIsRequired" runat="server" Visible="False" Text="is required."></asp:Literal>
    <asp:Literal ID="hdnDelete" runat="server" Visible="False" Text="Delete this Item"></asp:Literal>
    <asp:Literal ID="hdnViewSubItems" runat="server" Visible="False" Text="View Subitems"></asp:Literal>
    <asp:Literal ID="hdnMoveUp" runat="server" Visible="False" Text="Move Up"></asp:Literal>
    <asp:Literal ID="hdnMoveDown" runat="server" Visible="False" Text="Move Down"></asp:Literal>
    <asp:Literal ID="hdnEdit" runat="server" Visible="False" Text="Edit this Item"></asp:Literal>
</asp:Content>
