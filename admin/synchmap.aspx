﻿<%@ Page Language="C#" MasterPageFile="~/masters/admin.Master" CodeBehind="synchmap.aspx.cs" Inherits="netpoint.admin.synchmap" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ContentPlaceHolderID="TitleTag" ID="TitleTag" runat="server">
    <title>Administration - Synch Datamap</title>
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="mainslot" runat="server">
<table class="npadminpath">
        <tr>
            <td class="npadminheader">
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="npadminbody">
    <table class="npadmintable">
        <tr>
            <td>
      <ComponentArt:Grid id="grdSynch" DataAreaCssClass="Grid"
        ClientScriptLocation="~/scripts"
        AutoFocusSearchBox="false"
        AutoTheming="true"
        RunningMode="Callback"
        ShowHeader="true" 
        ShowFooter="true"
        ShowSearchBox="true"
        SearchOnKeyPress="true"
        PageSize="20" 
        PagerStyle="Slider" 
        FooterCssClass="GridFooter"
        HeaderCssClass="GridHeader"
        SliderPopupOffsetX="72"
        GroupingPageSize="5"
        ImagesBaseUrl="~/assets/common/admin/"
        PagerImagesFolderUrl="~/assets/common/admin/pager/" 
        LoadingPanelEnabled="false"
        GroupingNotificationText="Drag a column to this area to group by it."
        runat="server"
        AutoAdjustPageSize="true" 
        AutoSortOnGroup="true" 
        CollapseImageUrl="~/assets/common/admin/topItem_col.gif" 
        ExpandImageUrl="~/assets/common/admin/topItem_exp.gif" 
        GroupingCountHeadingsAsRows="false" 
        AllowColumnResizing="true" 
        ClientIDMode="AutoID">
        <Levels>
          <ComponentArt:GridLevel 
            DataKeyField="SynchMapID" 
            AllowReordering="true"   
            ColumnGroupIndicatorImageUrl=""  
            RowCssClass="Row" 
            ShowSelectorCells="true" 
            AllowGrouping= "true" 
            SortAscendingImageUrl="~/assets/common/admin/asc.gif" 
            SortDescendingImageUrl="~/assets/common/admin/desc.gif" 
            HeadingTextCssClass="HeadingCellText" 
            HeadingCellCssClass="HeadingCell" 
            DataCellCssClass="DataCell" 
            SortedDataCellCssClass="SortedDataCell" 
            GroupHeadingCssClass="GroupHeading" 
            AlternatingRowCssClass="AltRow" 
            HoverRowCssClass="SelectedRow"
            >
            <Columns>
              <ComponentArt:GridColumn DataField="SynchMapID" AllowGrouping="false" DataCellCssClass="FirstDataCell" Visible="false" />
              <ComponentArt:GridColumn DataField="Name" HeadingText="Name" Width="90" />
              <ComponentArt:GridColumn DataField="zedToolsDbTable" HeadingText="zed Table" Width="100" />
              <ComponentArt:GridColumn DataField="zedToolsDbField" HeadingText="zed Field" Width="100" />
              <ComponentArt:GridColumn DataField="zedToolsDbFieldSize" HeadingText="zed Field Size" Width="80" />
              <ComponentArt:GridColumn DataField="APIClass" HeadingText="API Class" Width="90" />
              <ComponentArt:GridColumn DataField="APIClassProperty" HeadingText="API Property" Width="90" />
              <ComponentArt:GridColumn DataField="DIAPIClass" HeadingText="DI API Class" Width="90" />
              <ComponentArt:GridColumn DataField="DIAPIClassProperty" HeadingText="DI API Property" Width="95" />
              <ComponentArt:GridColumn DataField="SBODbTable" HeadingText="B1 Table" Width="50" />
              <ComponentArt:GridColumn DataField="SBODbField" HeadingText="B1 Field" Width="70" />
              <ComponentArt:GridColumn DataField="SBODbFieldSize" HeadingText="B1 Field Size" Width="80" />
              <ComponentArt:GridColumn DataField="SynchFromzedTools" HeadingText="To B1" Width="35" />
              <ComponentArt:GridColumn DataField="SynchFromB1" HeadingText="To zed" Width="35" />
              <ComponentArt:GridColumn DataField="Comments" Width="200" TextWrap="true" />
            </Columns>
          </ComponentArt:GridLevel>
        </Levels>
      </ComponentArt:Grid>
      </td>
      </tr>
      <tr align="left">
          <td>
            <asp:HyperLink ID="lnkAdvanced" runat="server" Text="Advanced View" navigateurl="~/admin/synchmap.aspx"></asp:HyperLink>
            <asp:HyperLink ID="lnkSimple" runat="server" Text="Basic View" navigateurl="~/admin/synchmap.aspx?Simple=Y" Visible="false"></asp:HyperLink>
          </td>
      </tr>
      </table>
    </div> 
    <div style="position:absolute;top:0px;left:0px;visibility:hidden;">
      <img src="~/assets/common/admin/header_hoverBg.gif" width="0" height="0" alt="" />
      <img src="~/assets/common/admin/header_activeBg.gif" width="0" height="0" alt="" />
    </div>
    </asp:Content>