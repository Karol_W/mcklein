<%@ Register TagPrefix="np" TagName="mnflblock" Src="controls/ManufacturersListBlock.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.manufacturers" Codebehind="manufacturers.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right">
		<np:mnflblock ID="npmnflblock" runat="server" RepeatColumns="2" BorderWidth="1" ShowCategories="True"
			EnableViewState="False" />
	</div>
</asp:Content>
