﻿<%@ Page Language="c#" MasterPageFile="~/masters/catalogpartdetail.master" Inherits="netpoint.catalog.partdetail"
    CodeBehind="partdetail.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="QuantityAddBlock" Src="controls/QuantityAddBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PartAttributePicker" Src="controls/PartAttributePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="PartVariant" Src="controls/PartVariant.ascx" %>
<%@ Register TagPrefix="np" TagName="ProdLineBlock" Src="controls/ProdLineBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="ProdLineMatrix" Src="controls/ProdLineMatrix.ascx" %>
<%@ Register TagPrefix="np" TagName="ProdLineDropBlock" Src="controls/ProdLineDropBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="ProdLineImageBlock" Src="controls/ProdLineImageBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="manfblock" Src="controls/ManufacturerBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="pcblock" Src="controls/PartCrossBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="psblock" Src="controls/PartSizeBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="ppblock" Src="controls/PartPriceBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="pnblock" Src="controls/PartNotesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="pmblock" Src="controls/PartMediaBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="pablock" Src="controls/PartAttributesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="pss" Src="controls/SimilarSellers.ascx" %>
<%@ Register TagPrefix="np" TagName="kpblock" Src="controls/KitPartsBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="prblock" Src="controls/PartRemarksBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="publock" Src="controls/PartUDFBlock.ascx" %>
<asp:Content ContentPlaceHolderID="headerslot" ID="headercontent" runat="server">
  <meta name="keywords" content="<%= Keywords + "" %>" />
  <meta name="description" content="<%= Description + "" %>" />
  <meta property="fb:app_id" content="2223670561264689" />
</asp:Content>
<asp:Content ContentPlaceHolderID="prodlineimage" ID="pliblock" runat="server">
  <np:prodlineimageblock id="ProdLineImageBlock" runat="server" visible="False" swapimage="True" swapdropdown="True"></np:prodlineimageblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partmedia" ID="pmblock" runat="server">
  <np:pmblock id="nppmblock" runat="server"></np:pmblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partsize" ID="psblock" runat="server" visible="False">

</asp:Content>
<asp:Content ContentPlaceHolderID="partmanufacturer" ID="pmanfblock" runat="server">
  <np:manfblock id="npmanfblock" runat="server" borderwidth="0"></np:manfblock>
</asp:Content>

<asp:Content ContentPlaceHolderID="menuslot" ID="menu" runat="server">
  <asp:hyperlink id="lnkEdit" runat="server" navigateurl="~/admin/catalog/Part.aspx?partno=" imageurl="~/assets/common/icons/edit.gif" tooltip="Edit"></asp:hyperlink>
</asp:Content>
<asp:Content ContentPlaceHolderID="partname" ID="pnameblock" runat="server">
  <h1>
    <asp:label id="sysPartName" runat="server"></asp:label>
    <asp:label id="sysPartType" runat="server"></asp:label>
  </h1>
  <h2>
    <asp:label id="sysPartTagline" runat="server"></asp:label>
  </h2>
</asp:Content>
<asp:Content ContentPlaceHolderID="partcode" ID="pcodeblock" runat="server">
  <asp:label id="sysPartCode" runat="server" visible="False"></asp:label>
</asp:Content>
<asp:Content ContentPlaceHolderID="partequivalent" ID="pequivblock" runat="server">
  <asp:label id="sysManufacturerName" runat="server" visible="False"></asp:label>
  <asp:label id="sysManufacturerPartNo" runat="server" visible="False"></asp:label>
</asp:Content>
<asp:Content ContentPlaceHolderID="partdescription" ID="pdescblock" runat="server">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
  <h3 class="mc-model-num">
    Model Number:
    <asp:label id="sysPartNo" CssClass="part_no" runat="server"></asp:label>
  </h3>
  <h1 class="mc-item-msrp">
    MSRP: $
    <asp:label id="sysMSRP" runat="server"></asp:label>

  </h1>

  <%--<h1 class="mc-item-price">--%>
    <h1 class="mc-item-msrp"> Street Retail: 
          

      <asp:repeater id="Repeater_PartPrice" runat="server" onitemdatabound="Repeater_PartPrice_ItemDataBound">
        <%--Repeater is used to determine proper price per color--%>
        <ItemTemplate>
          <span class="mc-item-price">
            <span class="mc-part-origprice" runat="server" id="spanPartOrigPrice">
            </span>

            <span class="mc-item-sprice">
              <span class="mc-part-price" runat="server" id="spanPartPrice"></span>
            </span>

         </span>
        </ItemTemplate>
      </asp:repeater>
      </h1>
      <div style="padding-left: 10px;">
          <h4 class="choose-color">
              CHOOSE COLOR
          </h4>
        <div id="SwatchContainer" class="">
          <div class="">
            <asp:repeater id="Repeater_Content" runat="server" onitemdatabound="Repeater_Content_ItemDataBound">
              <ItemTemplate>
                <div style="display:inline-block;">
                  <td style="width:15px; height:15px;">
                    <asp:Image ID="ColorSwatch" runat="server" Width="40px" Height="15px" CssClass="ColorSwatchs" />
                  </td>
                </div>
              </ItemTemplate>
            </asp:repeater>

          </div>
        </div>
      </div>

    <div class="panel-group partDetailAccordion" id="accordion" role="tablist" aria-multiselectable="true">
      <!--Panel 1 -->
      <%--
      <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a class="btnCollapse" role="button">
              CHOOSE COLOR <span id="span1" class="glyphicon glyphicon-minus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse">
          <div class="panel-body panel-body1 detailBody collapse in">
            <asp:repeater id="Repeater_Content" runat="server" onitemdatabound="Repeater_Content_ItemDataBound">
              <ItemTemplate>
                <div style="display:inline-block;">
                  <td style="width:15px; height:15px;">
                    <asp:Image ID="ColorSwatch" runat="server" Width="40px" Height="15px" CssClass="ColorSwatchs" />
                  </td>
                </div>
              </ItemTemplate>
            </asp:repeater>

          </div>
        </div>
      </div>--%>

      <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab">
          <h3 class="hidden-print">
            QUANTITY</h3>
          <select id="mcqtyInput" class="form-control qty-input">
            <option value="1" placeholder="1">1</option>
            <option value="2" placeholder="2">2</option>
            <option value="3" placeholder="3">3</option>
            <option value="4" placeholder="4">4</option>
            <option value="5" placeholder="5">5</option>
            <option value="6" placeholder="6">6</option>
            <option value="7" placeholder="7">7</option>
            <option value="8" placeholder="8">8</option>
            <option value="9" placeholder="9">9</option>
            <option value="10" placeholder="10">10</option>
          </select>

        </div>
      </div>
      <div class="mcAddToCart">
        <a runat="server" id="mcAddToCart" href="#" class="mc-add-to-cart">Add To Cart</a>
      </div>
      <!--
    <div class="panel panel-default detailBody">
            <div class="panel-heading detailHeading" role="tab" id="headingSeven">
                    <h4 class="panel-title">
                            <a class=" btnCollapse7 collapsed" role="button">
                                    DESIGNER NOTES <span id="span7" class="glyphicon glyphicon-minus pull-right"></span>
                                </a>
                            </h4>
                          </div>
            <div id="collapseSeven" class="panel-collapse">
            <div class="panel-body panel-body7 detailBody collapse in">
                     <asp:label id="SysDesignerNotes" runat="server"></asp:label>
                                </div>
                              </div>

        </div>
        -->
        <!-- AddToAny BEGIN -->
        <br>
        <style type="text/css">
            .a2a_svg, .a2a_count { border-radius: 45 !important; }
        </style>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-icon-color="transparent,#505050">
            <!--<a class="a2a_dd" href="https://www.addtoany.com/share"></a>-->
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_pinterest"></a>
            <a class="a2a_button_reddit"></a>
            <a class="a2a_button_tumblr"></a>
            <a class="a2a_button_whatsapp"></a>
            <a class="a2a_button_telegram"></a>
            <a class="a2a_button_facebook_messenger"></a>
            <a class="a2a_button_line"></a>
            <a class="a2a_button_google_gmail"></a>
            <a class="a2a_button_email"></a>
            <a class="a2a_button_copy_link"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <!-- AddToAny END -->

        <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingZero">
          <h4 class="panel-title">
            <a class=" btnCollapse0 collapsed" role="button">
              DESIGNER NOTES <span id="span0" class="glyphicon glyphicon-minus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseZero" class="panel-collapse">
          <div class="panel-body panel-body0 detailBody collapse in">
            <asp:label id="sysDesignerN" runat="server"></asp:label>
          </div>
        </div>
      </div>

      <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="heading">
          <h4 class="panel-title">
            <a class=" btnCollapse3" role="button">
              DESCRIPTION <span id="span3" class="glyphicon glyphicon-plus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse">
          <div class="panel-body panel-body3 detailBody collapse in">
            <asp:label id="sysPartDescription" runat="server"></asp:label>
          </div>
        </div>
      </div>

      <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="btnCollapse2" role="button">
              DETAILS <span id="span2" class="glyphicon glyphicon-plus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse">
          <div class="panel-body panel-body2 detailBody collapse">
            <np:publock id="nppublock" runat="server" borderwidth="0"></np:publock>
            <h3>UPC</h3>
            <asp:label id="sysPartUPC" runat="server" class="description-body"></asp:label>
          </div>
        </div>
      </div>
      <div class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingFour">
          <h4 class="panel-title">
            <a class=" btnCollapse4 collapsed" role="button">
              SERIES <span id="span4" class="glyphicon glyphicon-plus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseFour" class="panel-collapse">
          <div class="panel-body panel-body4 detailBody collapse">
            <asp:label id="sysSeries" runat="server"></asp:label>
          </div>
        </div>
      </div>

      <div id="videoSection" class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingFive">
          <h4 class="panel-title">
            <a class="btnCollapse5 collapsed" role="button">
              VIDEO <span id="span5" class="glyphicon glyphicon-minus pull-right"></span>
            </a>
          </h4>
        </div>

        <div id="collapseFive" class="panel-collapse">
          <div class="panel-body panel-body5 detailBody collapse in">
            <asp:label id="sysVideo" runat="server"></asp:label>
            <!-- VIDEO DIV -->
            <!--<div id="video1" class="hidden">
              <video height="315" controls poster="https://mckleinusa.com/assets/common/themes/mcklein/assets/img/2in1Removable.png">
                <source src="https://mckleincompany.com/mens.mp4" type="video/mp4">
                Your browser does not support the video tag.
              </video>
            </div>-->

            <div id="video1" class="youtube hidden" data-embed="aExcvDxqRFc"> 
                <div class="play-button"></div> 
            </div>
            <!--<div id="video2" class="hidden">
              <video height="315" controls poster="https://mckleinusa.com/assets/common/themes/mcklein/assets/img/women's.jpg">
                <source src="https://mckleincompany.com/Women'sVideo.mp4" type="video/mp4">
                Your browser does not support the video tag.
              </video>
            </div>-->
            <div id="video2" class="youtube hidden" data-embed="x041wAixrT4"> 
                <div class="play-button"></div> 
            </div>
            <!--<div id="video3" class="hidden">
              <video height="315" controls poster="https://mckleinusa.com/assets/common/themes/mcklein/assets/img/checkpointW.png">
                <source src="https://mckleincompany.com/Checkpoint.mp4" type="video/mp4">
                Your browser does not support the video tag.
              </video>
            </div>-->
            <div id="video3" class="youtube hidden" data-embed="tSExRJedNJM"> 
                <div class="play-button"></div> 
            </div>
       
          
            <!--Aria-->
            <%--<div id="video4" class="hidden">
              <style>.embed-container
                    { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/x1SQOqWvwII" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Auburn-->
            <div id="video5" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/tUK_eQlBLyk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Pasadena-->
            <div id="video6" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/S7UWzIeaetM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Crescent-->
            <div id="video7" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/2Xsuknx_XLU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Cumberland-Nylon-->
            <div id="video8" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AngWOW4kBjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Eastward-->
            <div id="video9" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/x2ECIrV0dgY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Kennedy-->
            <div id="video10" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/iTZ5gRMIyaQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>
            <!--Edgefield-->
            <div id="video11" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/P2SuxJLgZAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Cumberland leather-->
            <div id="video12" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/opb40nEQlxQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Parker-Leather-->
            <div id="video13" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/cRChRCDTFhs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Harpswell-Leather-->
            <div id="video14" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/aVvsPh4MPmI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Arcadia-->
            <div id="video15" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3cTc4eDX8hA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
                        <!--Aldora-->
            <div id="video16" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/XGOZqZNUHyQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Lake Forest-->
            <div id="video17" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/w5DW42DF5qw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Edgebrook-->
            <div id="video18" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/YuEsndFLrtc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Dylan-->
            <div id="video19" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/amVjs7fUNQQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Parker-Nylon-->
            <div id="video20" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/tzgfDG9jtWU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Harpsewell-Nylon-->
            <div id="video21" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/glpurTERYLU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>
             <!--Element-->
            <div id="video22" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UnQrE-mZBLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Hartford-->
            <div id="video23" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AVM8GxS_k8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Brooklyn-->
            <div id="video24" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/2uP0cxPrUAA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Sofia-->
            <div id="video25" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/qXm2A1M644g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--North Park-->
            <div id="video26" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/uJxRCsmKonA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Peyton-->
            <div id="video27" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/VATc7s2G63E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Hyde Park-->
            <div id="video28" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/q9WU7xhpSPA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Springfield-Leather-->
            <div id="video29" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/fBz4wFC4-Mc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Gresham-->
            <div id="video30" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UClJ0NDXRts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Halsted-->
            <div id="video31" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/sR7pvu_yB10" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>


            <!--Avon-->
            <div id="video32" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/KBTXCCCvrHM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>

            </div>

            <!--Lincoln Park-->
            <div id="video33" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ly-YqY0njaM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Verona-->
            <div id="video34" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/eGpBam8ik2s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Willow Springs-->
            <div id="video35" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/s-aY9hLbK_g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Clinton-Nylon-->
            <div id="video36" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bBd8bwXNyRk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Granville-->
            <div id="video37" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/XUlPjS8HK1k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Bucktown-->
            <div id="video38" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UeE8dmJDG-Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Adria-->
            <div id="video39" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Zz8iqJefJa0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Allie-->
            <div id="video40" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/o5lS0gvU3qQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Glenview-->
            <div id="video41" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/q-C2tmH-gsQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Glen-Ellyn-->
            <div id="video42" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/dEIZgePEHYg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>

            <!--Daley-->
            <div id="video43" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/mSYrpCKHm3U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Clarice-->
            <div id="video44" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/KWrssZTC90o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Felicity-->
            <div id="video45" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3mE7cxFd51c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Fiona-->
            <div id="video46" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/4UwwpEXIyrM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Aaryn-->
            <div id="video47" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/CQKaWmNX8Po" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Oak-Grove-->
            <div id="video48" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/JmAoFxXtnTw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Damen-->
            <div id="video49" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/OT5fgWYOV_E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Lawson-->
            <div id="video50" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UOYsVSJyi-w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Lakewood-->
            <div id="video51" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ngyRyJdrv68" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Hubbard-Nylon-->
            <div id="video52" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8Yda61a_O54" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Hagen-->
            <div id="video53" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/kiXa8rsoj2M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Springfield-Nylon-->
            <div id="video54" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UjQml8eZeAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Eveline-->
            <div id="video55" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/yemrMKV1vVY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Gold-Coast-->
            <div id="video56" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/2Iryh6Ak91g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--River-West-->
            <div id="video57" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/w-t04RFn1E8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Flournoy-->
            <div id="video58" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/QSi5oVOaj3s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Bridgeport-->
            <div id="video59" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/80lZcTDGV2A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Davis-->
            <div id="video60" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rj986XSs1UA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Novembre-->
            <div id="video61" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/P0jqa3Pwi2A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Madaline-->
            <div id="video62" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/NwdDyxO4Wik" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Franklin-->
            <div id="video63" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/KzQEUL_62Zs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Chicago-Leather-->
            <div id="video64" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/DAzrgeCks_0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Clinton-Leather-->
            <div id="video65" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/0bA89UOmcDI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Harrison-->
            <div id="video66" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Nn3HUknVxJc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--La-Grange-->
            <div id="video67" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/xw5EvEfsuvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Lexington-->
            <div id="video68" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/c__oklFo0qk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Roseville-->
            <div id="video69" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/C3RTQtAesCg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Uptown-->
            <div id="video70" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/C8aAQ_zMXQ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Reagan-->
            <div id="video71" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/h6fNt1IgO-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Walton-->
            <div id="video72" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/T86OpQBinq8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Montclare-->
            <div id="video73" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/4hnzyzccLaw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Morgan-->
            <div id="video74" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/9dmoLjBO8nY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Elston-->
            <div id="video75" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rV61Jfw_m5E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Hillside-->
            <div id="video76" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/HBCw-JXxvqc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Alexis-->
            <div id="video77" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/WaMD7eD0808" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Serafina-->
            <div id="video78" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/K8ApqpDr2A8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Edison-->
            <div id="video79" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/pev3YcorGbI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Avalon-->
            <div id="video80" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/5nBchoxc9CQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Volo-->
            <div id="video81" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ecI83fCh1sU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Melody-->
            <div id="video82" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/KiaNuKOeOpQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--June-->
            <div id="video83" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/EufTWuP1Rhw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Gianna-->
            <div id="video84" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/k-R9Ysx9e0E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Arya-->
            <div id="video85" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/jtDs6MdCjK0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Alyson-->
            <div id="video86" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/VtgPn2Jq9D4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Cristina-->
            <div id="video87" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AETKS-g_E44" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Deva-->
            <div id="video88" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/MuJtwF1qUhc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Glenna-->
            <div id="video89" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/LhItEYNgWkw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Mayfair-->
            <div id="video90" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/4X8unkJtLuU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Savarna-->
            <div id="video91" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/BM_wgNSWRhA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Hazel-Crest-->
            <div id="video92" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/walqbbDkWM8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Emet-->
            <div id="video93" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/pVDAk2u5PGg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Maya-->
            <div id="video94" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/60pK48d0XQY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Cara-->
            <div id="video95" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/DcXCfwMDIuA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Signorini-->
            <div id="video96" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/hCw3IwgKmI0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Carugetto-->
            <div id="video97" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/nRE-SJ8HZhQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Belvedere-->
            <div id="video98" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/WnL41pBOCRs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Winnetka-->
            <div id="video99" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Qy7rRDnq6oo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Alicia-->
            <div id="video100" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/NOS-5xixIMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Roosevelt-Nylon-->
            <div id="video101" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/no9L3Z_VF8A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Sheridan-->
            <div id="video102" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AMkdpSJ1Ddk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Ceresola-->
            <div id="video103" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/gj-BOcdjA68" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Servano-->
            <div id="video104" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/f0efWAAQYPs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Settembre-->
            <div id="video105" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/biCdlAnFWr8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Chicago-Nylon-->
            <div id="video106" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bRm0qqon6c4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Harper-->
            <div id="video107" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/7x35_ayr8w4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Rockford-->
            <div id="video108" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/NLn_3ia0K_I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Pearson-Nylon-->
            <div id="video109" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/nsY1RlGin-Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Willowbrook-->
            <div id="video110" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8ww9VTQgFi4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Bronzeville-->
            <div id="video111" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/aaURkuneBeY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Coughlin-->
            <div id="video112" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bBtBLUaMiOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--LaSalle-->
            <div id="video113" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Niho6wsytPg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--WestLoop-->
            <div id="video114" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/euT1pG8li98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Bowery-->
            <div id="video115" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/epFrXyW0zMc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Amore-->
            <div id="video116" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/5eBFR2KYW7c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Sabotino-->
            <div id="video117" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/mEtswm1gnlg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Pastenello-->
            <div id="video118" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/IPvcU6buofg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Throop-->
            <div id="video119" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/MVue3giDUXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Pearson-Leather-->
            <div id="video120" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/q76_cWCKhXo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
             <!--Kinzie-->
            <div id="video121" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/F980cUffCUk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Ignoto-->
            <div id="video122" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/iCDxWvAbfDU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--San-Francesco-->
            <div id="video123" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/hkLlAeIfVb0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Serra-->
            <div id="video124" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/X5HT9AuWQ4k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Hubbard-Leather-->
            <div id="video125" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/fIkgcxl1TyQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Wrightwood-->
            <div id="video126" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/vWbuiL1-UOM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--River-North-->
            <div id="video127" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/p8GZZFbCPZU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Wicker-Park-->
            <div id="video128" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/H-74HEJKO6k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--West-Town-->
            <div id="video129" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/FfA_KGVCMx4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Irving-Park-->
            <div id="video130" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/S6UTlhmvLAs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Turner-->
            <div id="video131" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/N3mrLcxymQM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Elston-->
            <div id="video132" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ubiwj1Qxq7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Roosevelt-Leather-->
            <div id="video133" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/DZcJpcxhAHQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Melrose-->
            <div id="video134" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/_KdFaIHJlbA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Berkeley-->
            <div id="video135" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/HcqaaUQdnMI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Joliet-->
            <div id="video136" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3pHhrP20kkk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Walton-->
            <div id="video137" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/WZW4Why-gpc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Women 2in1-->
            <div id="video138" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/x041wAixrT4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Kinzie_U-->
            <div id="video139" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/g_YhjWe_CpE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Eastward_U-->
            <div id="video140" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/mlZldIhuTww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Southport_U-->
            <div id="video141" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/9x1AALLbD_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Wellington_U-->
            <div id="video142" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UEUFh1PPOXc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Englewood_U-->
            <div id="video143" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/vafH0KOYX6M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!--Avondale_U-->
            <div id="video144" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ABZ0aC84BA4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
             <!--Logan_U-->
            <div id="video145" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/y_V8F83GDlk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
             <!--East_Side_U-->
            <div id="video146" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/93TBLi07bus" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
             <!--South_Shore_U-->
            <div id="video147" class="hidden">
              <style>.embed-container
                        { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
                    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
                </style>
              <div class='embed-container'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UBu7ySTOLsM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            --%>
           


            <!-- (1) video wrapper Aria -->
            <div id="video4" class="youtube hidden" data-embed="x1SQOqWvwII"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Auburn -->
            <div id="video5" class="youtube hidden" data-embed="tUK_eQlBLyk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Pasadena -->
            <div id="video6" class="youtube hidden" data-embed="S7UWzIeaetM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Crescent -->
            <div id="video7" class="youtube hidden" data-embed="2Xsuknx_XLU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Cumberland-Nylon -->
            <div id="video8" class="youtube hidden" data-embed="AngWOW4kBjw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Eastward -->
            <div id="video9" class="youtube hidden" data-embed="x2ECIrV0dgY"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Kennedy -->
            <div id="video10" class="youtube hidden" data-embed="iTZ5gRMIyaQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Edgefield -->
            <div id="video11" class="youtube hidden" data-embed="P2SuxJLgZAk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Cumberland leather -->
            <div id="video12" class="youtube hidden" data-embed="opb40nEQlxQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Parker-Leather -->
            <div id="video13" class="youtube hidden" data-embed="cRChRCDTFhs"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Harpswell-Leather -->
            <div id="video14" class="youtube hidden" data-embed="aVvsPh4MPmI"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Arcadia -->
            <div id="video15" class="youtube hidden" data-embed="3cTc4eDX8hA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Aldora -->
            <div id="video16" class="youtube hidden" data-embed="XGOZqZNUHyQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Lake Forest -->
            <div id="video17" class="youtube hidden" data-embed="w5DW42DF5qw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Edgebrook -->
            <div id="video18" class="youtube hidden" data-embed="YuEsndFLrtc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Dylan -->
            <div id="video19" class="youtube hidden" data-embed="amVjs7fUNQQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Parker-Nylon -->
            <div id="video20" class="youtube hidden" data-embed="tzgfDG9jtWU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Harpsewell-Nylon -->
            <div id="video21" class="youtube hidden" data-embed="glpurTERYLU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div> 
            <!-- (1) video wrapper Element -->
            <div id="video22" class="youtube hidden" data-embed="UnQrE-mZBLo"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Hartford -->
            <div id="video23" class="youtube hidden" data-embed="AVM8GxS_k8I"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>   
            <!-- (1) video wrapper Brooklyn -->
            <div id="video24" class="youtube hidden" data-embed="2uP0cxPrUAA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Sofia -->
            <div id="video25" class="youtube hidden" data-embed="qXm2A1M644g"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper North Park -->
            <div id="video26" class="youtube hidden" data-embed="uJxRCsmKonA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div> 
            </div>
            <!-- (1) video wrapper Peyton -->
            <div id="video27" class="youtube hidden" data-embed="VATc7s2G63E"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>        
            <!-- (1) video wrapper Hyde Park -->
            <div id="video28" class="youtube hidden" data-embed="q9WU7xhpSPA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper Springfield-Leather -->
            <div id="video29" class="youtube hidden" data-embed="fBz4wFC4-Mc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper Gresham -->
            <div id="video30" class="youtube hidden" data-embed="UClJ0NDXRts"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Halsted -->
            <div id="video31" class="youtube hidden" data-embed="sR7pvu_yB10"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Avon -->
            <div id="video32" class="youtube hidden" data-embed="KBTXCCCvrHM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Lincoln Park -->
            <div id="video33" class="youtube hidden" data-embed="ly-YqY0njaM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Verona -->
            <div id="video34" class="youtube hidden" data-embed="eGpBam8ik2s"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Willow Springs -->
            <div id="video35" class="youtube hidden" data-embed="s-aY9hLbK_g"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Clinton-Nylon -->
            <div id="video36" class="youtube hidden" data-embed="bBd8bwXNyRk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Granville -->            
            <div id="video37" class="youtube hidden" data-embed="XUlPjS8HK1k"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Bucktown -->            
            <div id="video38" class="youtube hidden" data-embed="UeE8dmJDG-Q"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper ADRIA -->            
            <div id="video39" class="youtube hidden" data-embed="Zz8iqJefJa0"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Allie -->            
            <div id="video40" class="youtube hidden" data-embed="o5lS0gvU3qQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper Glenview -->            
            <div id="video41" class="youtube hidden" data-embed="q-C2tmH-gsQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Glen-Ellyn -->            
            <div id="video42" class="youtube hidden" data-embed="dEIZgePEHYg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>  
            <!-- (1) video wrapper Daley -->            
            <div id="video43" class="youtube hidden" data-embed="mSYrpCKHm3U"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Clarice -->            
            <div id="video44" class="youtube hidden" data-embed="KWrssZTC90o"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Felicity -->   
            <div id="video45" class="youtube hidden" data-embed="3mE7cxFd51c"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper Fiona -->   
            <div id="video46" class="youtube hidden" data-embed="4UwwpEXIyrM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Aaryn -->   
            <div id="video47" class="youtube hidden" data-embed="CQKaWmNX8Po"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Oak-Grove -->   
            <div id="video48" class="youtube hidden" data-embed="JmAoFxXtnTw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Damen -->   
            <div id="video49" class="youtube hidden" data-embed="OT5fgWYOV_E"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>      
            <!-- (1) video wrapper Lawson -->   
            <div id="video50" class="youtube hidden" data-embed="UOYsVSJyi-w"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div> 
            <!-- (1) video wrapper Lakewood -->   
            <div id="video51" class="youtube hidden" data-embed="ngyRyJdrv68"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Hubbard-Nylon -->   
            <div id="video52" class="youtube hidden" data-embed="8Yda61a_O54"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Hagen -->   
            <div id="video53" class="youtube hidden" data-embed="kiXa8rsoj2M"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Springfield-Nylon -->   
            <div id="video54" class="youtube hidden" data-embed="UjQml8eZeAk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Eveline -->   
            <div id="video55" class="youtube hidden" data-embed="yemrMKV1vVY"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Gold-Coast -->   
            <!--<div id="video56" class="youtube hidden" data-embed="yemrMKV1vVY"> -->
            <div id="video56" class="youtube hidden" data-embed="2Iryh6Ak91g"> <!-- updated data-embed and works-->
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper River-West -->   
            <div id="video57" class="youtube hidden" data-embed="w-t04RFn1E8"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Flournoy -->
            <div id="video58" class="youtube hidden" data-embed="QSi5oVOaj3s"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Bridgeport -->
            <div id="video59" class="youtube hidden" data-embed="80lZcTDGV2A"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Davis -->
            <div id="video60" class="youtube hidden" data-embed="rj986XSs1UA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Novembre -->
            <div id="video61" class="youtube hidden" data-embed="P0jqa3Pwi2A"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Madaline -->
            <div id="video62" class="youtube hidden" data-embed="NwdDyxO4Wik"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Franklin -->
            <div id="video63" class="youtube hidden" data-embed="KzQEUL_62Zs"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Chicago-Leather -->
            <div id="video64" class="youtube hidden" data-embed="DAzrgeCks_0"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Clinton-Leather -->
            <div id="video65" class="youtube hidden" data-embed="0bA89UOmcDI"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Harrison -->
            <div id="video66" class="youtube hidden" data-embed="Nn3HUknVxJc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper La-Grange -->
            <div id="video67" class="youtube hidden" data-embed="xw5EvEfsuvU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Lexington -->
            <div id="video68" class="youtube hidden" data-embed="c__oklFo0qk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Roseville -->
            <div id="video69" class="youtube hidden" data-embed="C3RTQtAesCg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Uptown -->
            <div id="video70" class="youtube hidden" data-embed="C8aAQ_zMXQ4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Reagan -->
            <div id="video71" class="youtube hidden" data-embed="h6fNt1IgO-c"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Walton -->
            <div id="video72" class="youtube hidden" data-embed="T86OpQBinq8"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Montclare -->
            <div id="video73" class="youtube hidden" data-embed="4hnzyzccLaw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Morgan -->
            <div id="video74" class="youtube hidden" data-embed="9dmoLjBO8nY"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Elston -->
            <div id="video75" class="youtube hidden" data-embed="rV61Jfw_m5E"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Hillside -->
            <div id="video76" class="youtube hidden" data-embed="HBCw-JXxvqc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Alexis -->
            <div id="video77" class="youtube hidden" data-embed="WaMD7eD0808"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Serafina -->
            <div id="video78" class="youtube hidden" data-embed="K8ApqpDr2A8"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Edison -->
            <div id="video79" class="youtube hidden" data-embed="pev3YcorGbI"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Avalon -->
            <div id="video80" class="youtube hidden" data-embed="5nBchoxc9CQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Volo -->
            <div id="video81" class="youtube hidden" data-embed="ecI83fCh1sU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Melody -->
            <div id="video82" class="youtube hidden" data-embed="KiaNuKOeOpQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper June -->
            <div id="video83" class="youtube hidden" data-embed="EufTWuP1Rhw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Gianna -->
            <div id="video84" class="youtube hidden" data-embed="k-R9Ysx9e0E"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Arya -->
            <div id="video85" class="youtube hidden" data-embed="jtDs6MdCjK0"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Alyson -->
            <div id="video86" class="youtube hidden" data-embed="VtgPn2Jq9D4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Cristina -->
            <div id="video87" class="youtube hidden" data-embed="AETKS-g_E44"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Deva -->
            <div id="video88" class="youtube hidden" data-embed="MuJtwF1qUhc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Glenna -->
            <div id="video89" class="youtube hidden" data-embed="LhItEYNgWkw"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Mayfair -->
            <div id="video90" class="youtube hidden" data-embed="4X8unkJtLuU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Savarna -->
            <div id="video91" class="youtube hidden" data-embed="BM_wgNSWRhA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Hazel-Crest -->
            <div id="video92" class="youtube hidden" data-embed="walqbbDkWM8"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Emet -->
            <div id="video93" class="youtube hidden" data-embed="pVDAk2u5PGg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Maya -->
            <div id="video94" class="youtube hidden" data-embed="60pK48d0XQY"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Cara -->
            <div id="video95" class="youtube hidden" data-embed="DcXCfwMDIuA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Signorini -->
            <div id="video96" class="youtube hidden" data-embed="hCw3IwgKmI0"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Carugetto -->
            <div id="video97" class="youtube hidden" data-embed="nRE-SJ8HZhQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Belvedere -->
            <div id="video98" class="youtube hidden" data-embed="WnL41pBOCRs"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Winnetka -->
            <div id="video99" class="youtube hidden" data-embed="Qy7rRDnq6oo"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Alicia -->
            <div id="video100" class="youtube hidden" data-embed="NOS-5xixIMU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Roosevelt-Nylon -->
            <div id="video101" class="youtube hidden" data-embed="no9L3Z_VF8A"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Sheridan -->
            <div id="video102" class="youtube hidden" data-embed="AMkdpSJ1Ddk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Ceresola -->
            <div id="video103" class="youtube hidden" data-embed="gj-BOcdjA68"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Servano -->
            <div id="video104" class="youtube hidden" data-embed="f0efWAAQYPs"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Settembre -->
            <div id="video105" class="youtube hidden" data-embed="biCdlAnFWr8"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Chicago-Nylon -->
            <div id="video106" class="youtube hidden" data-embed="bRm0qqon6c4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Harper -->
            <div id="video107" class="youtube hidden" data-embed="7x35_ayr8w4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Rockford -->
            <div id="video108" class="youtube hidden" data-embed="NLn_3ia0K_I"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Pearson-Nylon -->
            <div id="video109" class="youtube hidden" data-embed="nsY1RlGin-Q"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Willowbrook -->
            <!--<div id="video110" class="youtube hidden" data-embed="s-AuC8BOg8g"> doesn't work black box "video unavailable"-->
            <div id="video110" class="youtube hidden" data-embed="8ww9VTQgFi4"> <!-- changed data-embed and works -->
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Bronzeville -->
            <div id="video111" class="youtube hidden" data-embed="aaURkuneBeY"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Coughlin -->
            <div id="video112" class="youtube hidden" data-embed="bBtBLUaMiOc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper LaSalle -->
            <div id="video113" class="youtube hidden" data-embed="Niho6wsytPg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper WestLoop -->
            <div id="video114" class="youtube hidden" data-embed="euT1pG8li98"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Bowery -->
            <div id="video115" class="youtube hidden" data-embed="epFrXyW0zMc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Amore -->
            <div id="video116" class="youtube hidden" data-embed="5eBFR2KYW7c"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Sabotino -->
            <div id="video117" class="youtube hidden" data-embed="mEtswm1gnlg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Pastenello -->
            <div id="video118" class="youtube hidden" data-embed="IPvcU6buofg"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper Throop -->
            <div id="video119" class="youtube hidden" data-embed="MVue3giDUXk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Pearson-Leather -->
            <div id="video120" class="youtube hidden" data-embed="q76_cWCKhXo"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Kinzie -->
            <div id="video121" class="youtube hidden" data-embed="F980cUffCUk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Ignoto -->
            <div id="video122" class="youtube hidden" data-embed="iCDxWvAbfDU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper San-Francesco -->
            <div id="video123" class="youtube hidden" data-embed="hkLlAeIfVb0"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Serra -->
            <div id="video124" class="youtube hidden" data-embed="X5HT9AuWQ4k"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Hubbard -->
            <div id="video125" class="youtube hidden" data-embed="fIkgcxl1TyQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Wrightwood -->
            <div id="video126" class="youtube hidden" data-embed="vWbuiL1-UOM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper River-North -->
            <div id="video127" class="youtube hidden" data-embed="p8GZZFbCPZU"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Wicker-Park -->
            <div id="video128" class="youtube hidden" data-embed="H-74HEJKO6k"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper West-Town -->
            <div id="video129" class="youtube hidden" data-embed="FfA_KGVCMx4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Irving-Park -->
            <div id="video130" class="youtube hidden" data-embed="S6UTlhmvLAs"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Turner -->
            <div id="video131" class="youtube hidden" data-embed="N3mrLcxymQM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Elston -->
            <div id="video132" class="youtube hidden" data-embed="ubiwj1Qxq7s"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Roosevelt-Leather -->
            <div id="video133" class="youtube hidden" data-embed="DZcJpcxhAHQ"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Melrose -->
            <div id="video134" class="youtube hidden" data-embed="_KdFaIHJlbA"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Berkeley -->
            <div id="video135" class="youtube hidden" data-embed="HcqaaUQdnMI"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Joliet -->
            <div id="video136" class="youtube hidden" data-embed="3pHhrP20kkk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
            <!-- (1) video wrapper Walton -->
            <div id="video137" class="youtube hidden" data-embed="WZW4Why-gpc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
                 <!-- (1) video wrapper Women 2in1 -->
            <div id="video138" class="youtube hidden" data-embed="x041wAixrT4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
              <!-- (1) video wrapper Kinzie_U -->
            <div id="video139" class="youtube hidden" data-embed="g_YhjWe_CpE"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
               <!-- (1) video wrapper Eastward_U -->
            <div id="video140" class="youtube hidden" data-embed="mlZldIhuTww"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
               <!-- (1) video wrapper Southport_U -->
            <div id="video141" class="youtube hidden" data-embed="9x1AALLbD_Y"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
               <!-- (1) video wrapper Wellington_U -->
            <div id="video142" class="youtube hidden" data-embed="UEUFh1PPOXc"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper Englewood_U -->
            <div id="video143" class="youtube hidden" data-embed="vafH0KOYX6M"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper Avondale_U -->
            <div id="video144" class="youtube hidden" data-embed="ABZ0aC84BA4"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper Logan_U -->
            <div id="video145" class="youtube hidden" data-embed="y_V8F83GDlk"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper East_Side_U -->
            <div id="video146" class="youtube hidden" data-embed="93TBLi07bus"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>
             <!-- (1) video wrapper South_Shore_U -->
            <div id="video147" class="youtube hidden" data-embed="UBu7ySTOLsM"> 
               <!-- (2) the "play" button -->
               <div class="play-button"></div>                
            </div>

            <script type="text/javascript">
                $(document).ready(function () {

                var label = $('#<%=sysVideo.ClientID%>').html();
                var hasVideo = false;
                if (label.includes("2and1 Womens")) {
                 // $('#<%=sysVideo.ClientID%>').html("Patented 2-in-1 Women&rsquo;s Detachable Wheel and Handle System");
                    var source = "https://img.youtube.com/vi/" + $("#video2").data('embed') + "/sddefault.jpg";
                    var image = new Image();
                    image.src = source;
                    image.addEventListener("load", function () {
                        $("#video2").append(image);
                    });
                    $('#<%=sysVideo.ClientID%>').html("");
                    $("#video2").removeClass("hidden");
                    $('#video2').click();
                  hasVideo = true;
                } else if (label.includes("2and1 Mens")) {
               //   $('#<%=sysVideo.ClientID%>').html("Patented 2-in-1 Men&rsquo;s Detachable Wheel and Handle System");
                    var source = "https://img.youtube.com/vi/" + $("#video1").data('embed') + "/sddefault.jpg";
                    var image = new Image();
                    image.src = source;
                    image.addEventListener("load", function () {
                        $("#video1").append(image);
                    });
                //    $('#<%=sysVideo.ClientID%>').html("");
                    $("#video1").removeClass("hidden");
                    $('#video1').click();
                  hasVideo = true;
                } else if (label.includes("Checkpoint")) {
                //  $('#<%=sysVideo.ClientID%>').html("Checkpoint Friendly!");
                    var source = "https://img.youtube.com/vi/" + $("#video3").data('embed') + "/sddefault.jpg";
                    var image = new Image();
                    image.src = source;
                    image.addEventListener("load", function () {
                        $("#video3").append(image);
                    });
                    $('#<%=sysVideo.ClientID%>').html("");
                    $("#video3").removeClass("hidden");
                    $('#video3').click();
                  hasVideo = true;
                } else if (label == "2and1Check") {
                //  $('#<%=sysVideo.ClientID%>').html("Patented 2-in-1 Detachable Wheel and Handle System with Checkpoint Friendly feature");
                  $("#video3").removeClass("hidden");
                  $("#video1").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "2and1CheckWomen") {
                //  $('#<%=sysVideo.ClientID%>').html("Patented 2-in-1 Detachable Wheel and Handle System with Checkpoint Friendly feature");
                  $("#video3").removeClass("hidden");
                  $("#video2").removeClass("hidden");
                  hasVideo = true;
                  }

                  if (label == "Aria") {
                  var source = "https://img.youtube.com/vi/"+ $("#video4").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video4").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video4").removeClass("hidden");
                      //$("#video4 > source").removeClass("hidden");
                  vid2 = 4;
                  hasVideo = true;
                  } else if (label == "Auburn") {
                  var source = "https://img.youtube.com/vi/"+ $("#video5").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video5").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video5").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Pasadena") {
                  var source = "https://img.youtube.com/vi/"+ $("#video6").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video6").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video6").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Crescent") {
                  var source = "https://img.youtube.com/vi/"+ $("#video7").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video7").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video7").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Cumberland") {
                  var source = "https://img.youtube.com/vi/"+ $("#video8").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video8").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video8").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Eastward") {
                  var source = "https://img.youtube.com/vi/"+ $("#video9").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video9").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video9").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Kennedy") {
                  var source = "https://img.youtube.com/vi/"+ $("#video10").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video10").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video10").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Edgefield") {
                  var source = "https://img.youtube.com/vi/"+ $("#video11").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video11").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video11").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Cumberland-Leather") {
                  var source = "https://img.youtube.com/vi/"+ $("#video12").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video12").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video12").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Parker-Leather") {
                  var source = "https://img.youtube.com/vi/"+ $("#video13").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video13").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video13").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Harpswell-Leather") {
                  var source = "https://img.youtube.com/vi/"+ $("#video14").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video14").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video14").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Arcadia") {
                  var source = "https://img.youtube.com/vi/"+ $("#video15").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video15").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video15").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Aldora") {
                  var source = "https://img.youtube.com/vi/"+ $("#video16").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video16").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video16").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Lake-Forest") {
                  var source = "https://img.youtube.com/vi/"+ $("#video17").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video17").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video17").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Edgebrook") {
                  var source = "https://img.youtube.com/vi/"+ $("#video18").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video18").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video18").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Dylan") {
                  var source = "https://img.youtube.com/vi/"+ $("#video19").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video19").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video19").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Parker-Nylon") {
                  var source = "https://img.youtube.com/vi/"+ $("#video20").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video20").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video20").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Harpswell-Nylon") {
                  var source = "https://img.youtube.com/vi/"+ $("#video21").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video21").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video21").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Element") {
                  var source = "https://img.youtube.com/vi/"+ $("#video22").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video22").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video22").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Hartford") {
                  var source = "https://img.youtube.com/vi/"+ $("#video23").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video23").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video23").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Brooklyn") {
                  var source = "https://img.youtube.com/vi/"+ $("#video24").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video24").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video24").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Sofia") {
                  var source = "https://img.youtube.com/vi/"+ $("#video25").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video25").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video25").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "North Park") {
                  var source = "https://img.youtube.com/vi/"+ $("#video26").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video26").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video26").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Peyton") {
                  var source = "https://img.youtube.com/vi/"+ $("#video27").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video27").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video27").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Hyde Park") {
                  var source = "https://img.youtube.com/vi/"+ $("#video28").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video28").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video28").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Springfield-Leather") {
                  var source = "https://img.youtube.com/vi/"+ $("#video29").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video29").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video29").removeClass("hidden");
                  $("#video3").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Gresham") {
                  var source = "https://img.youtube.com/vi/"+ $("#video30").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video30").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video30").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Halsted") {
                  var source = "https://img.youtube.com/vi/"+ $("#video31").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video31").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video31").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Avon") {
                  var source = "https://img.youtube.com/vi/"+ $("#video32").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video32").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video32").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Lincoln Park") {
                  var source = "https://img.youtube.com/vi/"+ $("#video33").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video33").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video33").removeClass("hidden");
                  hasVideo = true;
                } else if (label == "Granville") {
                  var source = "https://img.youtube.com/vi/"+ $("#video37").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video37").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video37").removeClass("hidden");
                  hasVideo = true; 
                } 
                  // Left this guy as an "if" instead of "else if" because we want two videos for this product. The Checkpoint Friendly and Verona
                  // I also changed the if statement to label.includes() for this same reason. Verona has two tags in the SQL database table M: "Checkpoint" and "Verona". 
                  if (label.includes("Verona")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video34").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video34").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video34").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Willow-Springs")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video35").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video35").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video35").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Clinton-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video36").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video36").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video36").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Bucktown")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video38").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video38").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video38").removeClass("hidden");
                  hasVideo = true;  
                } else if (label.includes("Adria")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video39").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video39").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video39").removeClass("hidden");
                  hasVideo = true;  
                } else if (label.includes("Allie")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video40").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video40").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video40").removeClass("hidden");
                  hasVideo = true;   
                } else if (label.includes("Glenview")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video41").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video41").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video41").removeClass("hidden");
                  hasVideo = true;    
                } else if (label.includes("Glen-Ellyn")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video42").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video42").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video42").removeClass("hidden");
                  hasVideo = true;     
                } else if (label.includes("Daley")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video43").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video43").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video43").removeClass("hidden");
                  hasVideo = true;     
                } else if (label.includes("Clarice")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video44").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video44").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video44").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Felicity")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video45").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video45").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video45").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Fiona")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video46").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video46").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video46").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Aaryn")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video47").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video47").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video47").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Oak-Grove")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video48").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video48").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video48").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Damen")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video49").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video49").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video49").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Lawson")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video50").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video50").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video50").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Lakewood")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video51").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video51").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video51").removeClass("hidden");
                  $("#video3").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Hubbard-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video52").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video52").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video52").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Hagen")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video53").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video53").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video53").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Springfield-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video54").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video54").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video54").removeClass("hidden");
                  $("video3").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Eveline")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video55").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video55").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video55").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Gold-Coast")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video56").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video56").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video1").removeClass("hidden");
                  $("#video56").removeClass("hidden");
                  hasVideo = true;   
                } else if (label.includes("River-West")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video57").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video57").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video57").removeClass("hidden");
                  hasVideo = true;  
                } else if (label.includes("Flournoy")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video58").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video58").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video58").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Bridgeport")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video59").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video59").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video59").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Davis")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video60").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video60").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video60").removeClass("hidden");
                  hasVideo = true; 
                } else if (label == "Novembre") {
                  var source = "https://img.youtube.com/vi/"+ $("#video61").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video61").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video61").removeClass("hidden");
                  hasVideo = true; 
                } else if (label == "Madaline") {
                  var source = "https://img.youtube.com/vi/"+ $("#video62").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video62").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video62").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Franklin")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video63").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video63").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video63").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Chicago-Leather")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video64").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video64").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                      $("#video64").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Clinton-Leather")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video65").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video65").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video65").removeClass("hidden");
                  hasVideo = true;  
                } else if (label.includes("Harrison")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video66").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video66").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video66").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("La-Grange")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video67").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video67").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video67").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Lexington")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video68").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video68").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video68").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Roseville")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video69").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video69").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video69").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Uptown")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video70").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video70").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video70").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Reagan")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video71").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video71").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video71").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Walton")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video72").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video72").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video72").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Montclare")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video73").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video73").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video73").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Morgan")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video74").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video74").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video74").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Elston")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video75").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video75").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video75").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Hillside")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video76").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video76").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video76").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Alexis")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video77").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video77").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video77").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Serafina")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video78").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video78").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video78").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Edison")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video79").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video79").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video79").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Avalon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video80").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video80").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video80").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Volo")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video81").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video81").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video81").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Melody")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video82").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video82").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video82").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("June")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video83").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video83").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video83").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Gianna")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video84").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video84").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video84").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Arya")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video85").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video85").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video85").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Alyson")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video86").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video86").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video86").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Cristina")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video87").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video87").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video87").removeClass("hidden");
                  hasVideo = true; 
                } else if (label.includes("Deva")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video88").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video88").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video88").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Glenna")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video89").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video89").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video89").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Mayfair")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video90").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video90").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video90").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Savarna")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video91").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video91").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video91").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Hazel-Crest")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video92").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video92").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video92").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Emet")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video93").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video93").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video93").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Maya")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video94").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video94").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video94").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Cara")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video95").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video95").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video95").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Signorini")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video96").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video96").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video96").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Carugetto")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video97").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video97").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video97").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Belvedere")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video98").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video98").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video98").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Winnetka")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video99").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video99").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video99").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Alicia")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video100").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video100").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video100").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Roosevelt-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video101").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video101").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video101").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Sheridan")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video102").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video102").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video102").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Ceresola")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video103").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video103").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video103").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Servano")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video104").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video104").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video104").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Settembre")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video105").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video105").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video105").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Chicago-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video106").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video106").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                      $("#video106").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Harper")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video107").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video107").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video107").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Rockford")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video108").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video108").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video108").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Pearson-Nylon")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video109").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video109").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video109").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Willowbrook")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video110").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video110").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video110").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Bronzeville")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video111").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video111").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video111").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Coughlin")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video112").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video112").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video112").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("LaSalle")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video113").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video113").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video113").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("WestLoop")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video114").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video114").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video114").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Bowery")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video115").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video115").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video115").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Amore")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video116").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video116").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video116").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Sabotino")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video117").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video117").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video117").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Pastenello")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video118").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video118").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video118").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Throop")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video119").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video119").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video119").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Pearson-Leather")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video120").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video120").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video120").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Kinzie")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video121").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video121").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video121").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Ignoto")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video122").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video122").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video122").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("San-Francesco")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video123").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video123").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video123").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Serra")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video124").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video124").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video124").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Hubbard-Leather")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video125").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video125").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video125").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Wrightwood")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video126").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video126").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video126").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("River-North")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video127").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video127").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video127").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Wicker-Park")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video128").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video128").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video128").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("West-Town")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video129").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video129").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video129").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Irving-Park")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video130").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video130").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video130").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Turner")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video131").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video131").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video131").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Elston")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video132").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video132").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video132").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Roosevelt-Leather")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video133").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video133").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video133").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Melrose")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video134").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video134").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video134").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Berkeley")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video135").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video135").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video135").removeClass("hidden");
                  hasVideo = true;
                } else if (label.includes("Joliet")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video136").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video136").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video136").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Walton")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video137").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video137").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video137").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("2and1 Womens")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video138").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video138").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video138").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("KinzieU")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video139").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video139").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video139").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Eastward_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video140").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video140").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video140").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Southport_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video141").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video141").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video141").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Wellington_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video142").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video142").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video142").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Englewood_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video143").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video143").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video143").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Avondale_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video144").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video144").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video144").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("Logan_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video145").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video145").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video145").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("East_Side_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video146").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video146").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video146").removeClass("hidden");
                  hasVideo = true;
                  } else if (label.includes("South_Shore_U")) {
                  var source = "https://img.youtube.com/vi/"+ $("#video146").data('embed') +"/sddefault.jpg";
                  var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    $("#video147").append( image );
                  });
                  $('#<%=sysVideo.ClientID%>').html("");
                  $("#video147").removeClass("hidden");
                  hasVideo = true;
                  } 
                 
                  else if (!hasVideo) {
                  $('#<%=sysVideo.ClientID%>').html("No video Available");
                  if ($('#<%=sysVideo.ClientID%>').html("No video Available")) {
                    $('.panel-body5').removeClass("in");
                    $("#span5").removeClass("glyphicon-minus");
                    $("#span5").addClass("glyphicon-plus");
                  }
                }
                });

                var youtube = document.querySelectorAll(".youtube");

                for (var i = 0; i < youtube.length; i++) {
                    youtube[i].addEventListener("click", function () {

                        var iframe = document.createElement("iframe");

                        iframe.setAttribute("frameborder", "0");
                        iframe.setAttribute("allowfullscreen", "");
                        //iframe.setAttribute( "allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" );
                        iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");

                        this.innerHTML = "";
                        this.appendChild(iframe);
                    });
                }


            </script>
          </div>
        </div>
      </div>

      <div id="advanceSection" class="panel panel-default detailBody">
        <div class="panel-heading detailHeading" role="tab" id="headingSix">
          <h4 class="panel-title">
            <a class=" btnCollapse6 collapsed" role="button">
              ADVANCE FEATURES <span id="span6" class="glyphicon glyphicon-plus pull-right"></span>
            </a>
          </h4>
        </div>
        <div id="collapseSix" class="panel-collapse">
          <div class="panel-body panel-body6 detailBody collapse">
            <asp:label id="sysAdvanceFeatures" runat="server"></asp:label>
            <!-- ADVANCE FEATURE ICONS -->
            <div id="variation1">
              <ul id="afList">
                <li class="icon1 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="2in1" src="https://mckleinusa.com/assets/common/icons/2in1-RWHS-Icon.png" /></span></a></li>
                <li class="icon2 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Laptop Protection" src="https://mckleinusa.com/assets/common/icons/Laptop-Protection-Icon.png" /></a></li>
                <li class="icon3 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Checkpoint Friendly" src="https://mckleinusa.com/assets/common/icons/Fly-Through-Checkpoint-Friendly-Icon.png" /></a></li>
                <li class="icon4 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Removable Case" src="https://mckleinusa.com/assets/common/icons/Removable-Case-Icon.png" /></a></li>
                <li class="icon5 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Built-in Laptop Compartment" src="https://mckleinusa.com/assets/common/icons/Advanced_features_bc.png" /></a></li>
                <li class="icon6 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Removable Sleeve" src="https://mckleinusa.com/assets/common/icons/Removable-Sleeve-Icon.png" /></a></li>
                <li class="icon7 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Tablet Pocket" src="https://mckleinusa.com/assets/common/icons/Tablet-Pocket-Icon.png" /></a></li>
                <li class="icon8 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Smart Zipper" src="https://mckleinusa.com/assets/common/icons/Smart-Zipper-Icon.png" /></span></a></li>
                <li class="icon9 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Smart Strap" src="https://mckleinusa.com/assets/common/icons/Smart-Strap-Icon.png" /></a></li>
                <li class="icon0 hidden"><a href="http://mckleincompany.com/advance-features"><img class="img-responsive" title="Weather Proof" src="https://mckleinusa.com/assets/common/icons/Weatherproof-Cover-Icon.png" /></a></li>
                <li class="nonIcon hidden">N/A
                  <!--<a href="https://mckleinusa.com">MckleinUsa</a>
                    <a href="https://siamod.com">Siamod</a>-->
                </li>
              </ul>


            </div>

            <script>
              $("document").ready(function() {
                var label = $('#<%=sysAdvanceFeatures.ClientID%>').html();
                for (i = 0; i <= label.length; i++) {
                  if (label[i] == "1") {
                    $(".icon1").removeClass("hidden");
                  } else if (label[i] == "2") {
                    $(".icon2").removeClass("hidden");
                  } else if (label[i] == "3") {
                    $(".icon3").removeClass("hidden");
                  } else if (label[i] == "4") {
                    $(".icon4").removeClass("hidden");
                  } else if (label[i] == "5") {
                    $(".icon5").removeClass("hidden");
                  } else if (label[i] == "6") {
                    $(".icon6").removeClass("hidden");
                  } else if (label[i] == "7") {
                    $(".icon7").removeClass("hidden");
                  } else if (label[i] == "8") {
                    $(".icon8").removeClass("hidden");
                  } else if (label[i] == "9") {
                    $(".icon9").removeClass("hidden");
                  } else if (label[i] == "0") {
                    $(".icon0").removeClass("hidden");
                  }
                }
                if (label == "") {
                  $(".nonIcon").removeClass("hidden");
                }
              });

           

            </script>
          </div>
        </div>
      </div>

    </div>
    </div>
    <script type="text/javascript">
        $(".btnCollapse7").click(function() {
          $(".panel-body7").collapse("toggle");
          if ($("#span7").hasClass("glyphicon-minus")) {
            $("#span7").removeClass("glyphicon-minus");
            $("#span7").addClass("glyphicon-plus");
          } else {
            $("#span7").removeClass("glyphicon-plus");
            $("#span7").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse0").click(function () {
            $(".panel-body0").collapse("toggle");
            if ($("#span0").hasClass("glyphicon-minus")) {
                $("#span0").removeClass("glyphicon-minus");
                $("#span0").addClass("glyphicon-plus");
            } else {
                $("#span0").removeClass("glyphicon-plus");
                $("#span0").addClass("glyphicon-minus");
            }
        });
        $(".btnCollapse").click(function() {
          $(".panel-body1").collapse("toggle");
          if ($("#span1").hasClass("glyphicon-minus")) {
            $("#span1").removeClass("glyphicon-minus");
            $("#span1").addClass("glyphicon-plus");
          } else {
            $("#span1").removeClass("glyphicon-plus");
            $("#span1").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse2").click(function() {
          $(".panel-body2").collapse("toggle");
          if ($("#span2").hasClass("glyphicon-minus")) {
            $("#span2").removeClass("glyphicon-minus");
            $("#span2").addClass("glyphicon-plus");
          } else {
            $("#span2").removeClass("glyphicon-plus");
            $("#span2").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse3").click(function() {
          $(".panel-body3").collapse("toggle");
          if ($("#span3").hasClass("glyphicon-minus")) {
            $("#span3").removeClass("glyphicon-minus");
            $("#span3").addClass("glyphicon-plus");
          } else {
            $("#span3").removeClass("glyphicon-plus");
            $("#span3").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse4").click(function() {
          $(".panel-body4").collapse("toggle");
          if ($("#span4").hasClass("glyphicon-minus")) {
            $("#span4").removeClass("glyphicon-minus");
            $("#span4").addClass("glyphicon-plus");
          } else {
            $("#span4").removeClass("glyphicon-plus");
            $("#span4").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse5").click(function() {
          $(".panel-body5").collapse("toggle");
          if ($("#span5").hasClass("glyphicon-minus")) {
            $("#span5").removeClass("glyphicon-minus");
            $("#span5").addClass("glyphicon-plus");
          } else {
            $("#span5").removeClass("glyphicon-plus");
            $("#span5").addClass("glyphicon-minus");
          }
        });
        $(".btnCollapse6").click(function() {
          $(".panel-body6").collapse("toggle");
          if ($("#span6").hasClass("glyphicon-minus")) {
            $("#span6").removeClass("glyphicon-minus");
            $("#span6").addClass("glyphicon-plus");
          } else {
            $("#span6").removeClass("glyphicon-plus");
            $("#span6").addClass("glyphicon-minus");
          }
        });
        </script>
        <script type="text/javascript">
        $('.ColorSwatchs').click(function() {
          var id = this.id;
          console.log(id);
          var partNoID = id.substr(id.lastIndexOf("_") + 1);
          $('.partDetailImageContainer').hide();
          $('.mc-part-origprice').hide();
          $('.mc-part-price').hide();
          $('.' + partNoID).show();
          $('span.mc-item-price').hide();
          $("." + partNoID).parent().parent().show();
          $('.mc-add-to-cart').attr('href', '../commerce/cart.aspx?AddPartNo=' + partNoID);

        });

        $('.mc-add-to-cart').click(function() {
          var qty = $('#mcqtyInput').val();
          var a_href = $('.mc-add-to-cart').attr('href');
          a_href = a_href + "&Quantity=" + qty
          $('.mc-add-to-cart').attr('href', a_href);
        });
      </script>

        
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>
        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="450" data-numposts="5"></div>
        <script>$(document).ready(function () { $(".fb-comments").attr("data-href", window.location.href.split('?')[0]); });</script>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="partnotes" ID="pnblock" runat="server">
  <np:pnblock id="nppnblock" runat="server" borderwidth="0" style="vertical-align: top;"></np:pnblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partcrosssell" ID="pcblock" runat="server">
  <np:pcblock id="nppcblock" runat="server"></np:pcblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partsimilarsellers" ID="Content1" runat="server">
  <np:pss id="nppss" runat="server"></np:pss>
</asp:Content>
<asp:Content ContentPlaceHolderID="kitpartslist" ID="kpartsblock" runat="server">
  <np:kpblock id="kpblock" runat="server"></np:kpblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partprice" ID="ppblock" runat="server">
  <np:ppblock id="npppblock" runat="server"></np:ppblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partquantityadd" ID="pqablock" runat="server">
  <np:quantityaddblock id="QuantityAddBlock" runat="server" visible="false"></np:quantityaddblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="prodlinedrop" ID="pldblock" runat="server">
  <np:prodlinedropblock id="ProdLineDropBlock" runat="server" visible="False" showparttype="False" showpartno="True" showpartname="True" showinventory="True" showprice="True"></np:prodlinedropblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="prodlinematrix" ID="matrix" runat="server">
  <np:prodlinematrix id="npProdLineMatrix" runat="server" visible="False"></np:prodlinematrix>
</asp:Content>
<asp:Content ContentPlaceHolderID="partattributepicker" ID="papblock" runat="server">
  <np:partattributepicker id="pap" runat="server" visible="False"></np:partattributepicker>
</asp:Content>
<asp:Content ContentPlaceHolderID="prodlinegrid" ID="plgblock" runat="server">
  <np:prodlineblock id="prodLine" runat="server" visible="False" showparttype="False" showpartno="True" showinventory="False" showprice="True" showthumbnail="True" showaddtocart="True" showquantity="False"></np:prodlineblock>
</asp:Content>

<asp:Content ContentPlaceHolderID="partremarks" ID="prblock" runat="server">
  <np:prblock id="npprblock" runat="server" borderwidth="0"></np:prblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partattributes" ID="pablock" runat="server">
  <np:pablock id="nppablock" runat="server" borderwidth="0"></np:pablock>
</asp:Content>
<asp:Content ContentPlaceHolderID="partvariant" ID="pvblock" runat="server">
  <np:partvariant id="partConfig" runat="server"></np:partvariant>
</asp:Content>