<%@ Register TagPrefix="np" TagName="cblock" Src="controls/CategoriesBlock.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.categories" Codebehind="categories.aspx.cs" %>
<asp:Content ContentPlaceHolderID="headerslot" runat="server" ID="header">
    <meta name="keywords" content="<%# Keywords %>" />
    <meta name="description" content="<%# Description %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right ppxs">
        <asp:literal runat="server" id="sysCategoryDescription"></asp:literal>
        <np:cblock ID="npcblock" runat="server" BorderWidth="0" CatsPerCol="2" />
	</div>
</asp:Content>
