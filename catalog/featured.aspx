<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.featured" Codebehind="featured.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="plblock" Src="controls/PartsListBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right">
		<div class="item-listing-wrapper">
			<np:plblock ID="npplblock" runat="server" ListType="F" DisplayNumber="25" DisplayType="I" BorderWidth="1" TypeVisible="False" CompareVisible="False" />
		</div>
	</div>
</asp:Content>
