<%@ Control Language="c#" Inherits="netpoint.catalog.controls.CategoriesBlock" Codebehind="CategoriesBlock.ascx.cs" %>

<asp:Literal ID="ltlDescription" runat="Server" />
	
<asp:DataList ID="categoryrepeater" runat="server" CellSpacing="0" CellPadding="0" CssClass="category-link-block"
    GridLines="Both" RepeatColumns="0" RepeatLayout="Table" OnItemDataBound="DetermineRowVars">
    <ItemTemplate>

                    <asp:HyperLink ID="CategoryImageLink" runat="server">
                        <asp:Image runat="server" ID="CategoryImage" BorderWidth="0" ImageUrl='<%#assetsURL+DataBinder.Eval(Container.DataItem,"ImageFile")%>' />
                    </asp:HyperLink>
                
                
                    <asp:HyperLink ID="CategoryNameLink" runat="server">
                        <asp:Label runat="server" ID="CategoryName" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"CategoryName")%>' />
                    </asp:HyperLink>
                
                
                    <asp:Label runat="server" ID="CategoryDescription" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>'
                        Height="20" />

    </ItemTemplate>
</asp:DataList>
