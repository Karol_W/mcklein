<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartVariant" Codebehind="PartVariant.ascx.cs" %>
<asp:repeater id="rpt" runat="server" onprerender="rpt_PreRender">
	<HeaderTemplate>
      <h2><asp:Literal id="chooseDetailsLtl" runat="server" Text="Choose Product Details"></asp:Literal></h2>
      <asp:literal ID="sysPartName" Runat="server"></asp:literal>
	</HeaderTemplate>
	<ItemTemplate>
        <div class="variant-category">
          <h3><asp:Literal ID="sysVariant" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>'></asp:Literal></h3>
          <p><asp:Literal ID="sysMarketingText" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MarketingText") %>'></asp:Literal></p>
          <div class="variant-image"><asp:Image ID="sysPart" Runat="server" /></div>
          <div class="variant-options">
            <asp:RadioButtonList id="sysRequiredParts" runat="server" CssClass="npbody"></asp:RadioButtonList>
            <asp:CheckBoxList id="sysOptionalParts" runat="server" CssClass="npbody"></asp:CheckBoxList>
          </div>
        </div>
	</ItemTemplate>
	<FooterTemplate>
	</FooterTemplate>
</asp:repeater>
<p style="text-align:right;">
<%--<asp:Button id="btnCalc" runat="server" Text="Calculate" visible="False" CssClass="NPHeader" onclick="btnCalc_Click"></asp:Button>--%>
<asp:ImageButton id="btnCalc" runat="server" Text="Calculate" visible="False" CssClass="NPHeader" onclick="btnCalc_Click"></asp:ImageButton>
</p>
