<%@ Reference Control="~/catalog/controls/manufacturerblock.ascx" %>
<%@ register tagprefix="np" TagName="manfblock" src="ManufacturerBlock.ascx"%>
<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ManufacturersListBlock" Codebehind="ManufacturersListBlock.ascx.cs" %>
<ASP:DataList id="MNFListRepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="Both"
	BorderWidth="1" RepeatColumns="4" RepeatLayout="Table" CssClass="npbody" ItemStyle-VerticalAlign="Top"
	Width="100%">
	<ItemTemplate>
		<table class="npbody" width="100%" cellspacing="0" cellpadding="5" border="0">
			<tr>
				<td valign="Top" align="Center">
					<np:manfblock id="npmanfblock" runat="server" BorderWidth="0" ShowWebsite="False" ShowName="True" ShowDescription="False"></np:manfblock>
					<asp:GridView ID="grid" runat="server" Visible="false" AutoGenerateColumns="false" GridLines="None"
					    ShowHeader="false">
					    <Columns>
					        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
					        <ItemTemplate>
						        <asp:HyperLink runat="server" id="CategoryLink"></asp:HyperLink>
					        </ItemTemplate>
					        </asp:TemplateField>
					    </Columns>
					</asp:GridView>
				</td>
			</tr>
		</table>
	</ItemTemplate>
</ASP:DataList>
