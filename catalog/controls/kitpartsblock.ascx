<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KitPartsBlock.ascx.cs" Inherits="netpoint.catalog.controls.KitPartsBlock" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<table width="100%" cellpadding="3" cellspacing="0" border="0">
    <tr>
        <td class="npheader">
	        <asp:ImageButton id="clpGeneral" ToolTip="Collapse" runat="server" OnClick="clpGeneral_Click" />
	        <asp:ImageButton id="expGeneral" ToolTip="Expand" runat="server" visible="false" OnClick="expGeneral_Click" />&nbsp;
	        <asp:Label id="lblDetailed" text="Bill of Material Items" Runat="server"></asp:Label></td>
	</tr>
	<tr>
	    <td>
	    <NP:GridView ID="gvKitParts" runat="server" AllowPaging="True" EmptyDataText="No Items Found"
            AllowSorting="True" PageSize="25" Width="100%" AutoGenerateColumns="false" OnRowDataBound="gvKitParts_RowDataBound">
            <RowStyle CssClass="npbody" />
            <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
            <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
            <Columns>
                <asp:TemplateField HeaderText="colImage|Image">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkImage" runat="server">view detail</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ItemNo|Item No.">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkPartNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildPartNo") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ItemName|Item Name">
                    <ItemTemplate>
                        <b><asp:Literal ID="ltlPartName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildPart.PartName") %>'></asp:Literal></b>
                        <asp:Literal ID="ltlSeparator" runat="server" Text="/"></asp:Literal>
                        <asp:Literal ID="ltlDescription" runat="server"></asp:Literal>    
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colPrice|Price">
                    <ItemStyle HorizontalAlign="Right" VerticalAlign="top"></ItemStyle>
                    <ItemTemplate>
                        <np:PartPriceDisplay ID="ppdPartPrice" runat="server" ShowPriceDescription="Never" ShowItemizedTax="false" Part='<%# DataBinder.Eval(Container.DataItem, "ChildPart") %>'/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colTax|Tax">
                    <ItemStyle HorizontalAlign="Right" VerticalAlign="top"></ItemStyle>
                    <ItemTemplate>
                        <np:PriceDisplay ID="prcTax" runat="server" ShowNotAvailable="true" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="colQty|Qty">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="top"></ItemStyle>
                    <ItemTemplate>
                        <asp:Literal ID="ltlQty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </NP:GridView>
        </td>
    </tr>
</table>
<asp:Literal ID="hdnPriceInclTax" runat="server" Text="Price incl. Tax" Visible="false" />
<asp:Literal ID="hdnPriceExclTax" runat="server" Text="Price excl. Tax" Visible="false" />
