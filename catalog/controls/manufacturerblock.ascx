<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ManufacturerBlock" Codebehind="ManufacturerBlock.ascx.cs" %>
<asp:Panel ID="MNFPanel" Runat="server" HorizontalAlign="Center">

		<asp:HyperLink id="sysMNFLogo" Runat="server"></asp:HyperLink>
		<asp:Label id="sysMNFName" Runat="server"></asp:Label>
		<asp:Label id="sysMNFDescription" Runat="server"></asp:Label>
		<asp:HyperLink id="sysMNFWebsite" Runat="server" Target="_blank"></asp:HyperLink>
		
</asp:Panel>
