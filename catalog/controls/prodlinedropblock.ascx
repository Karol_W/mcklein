<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ProdLineDropBlock" Codebehind="ProdLineDropBlock.ascx.cs" %>
<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td valign="top">
            <asp:DropDownList ID="ddlProdLine" runat="server" />
            <asp:Panel ID="pnlOrderLineNotes" runat="server" Visible="False">
                <br />
                <asp:Label ID="lblNote" runat="server" Text="Custom Note"></asp:Label>
                <br />
                <asp:TextBox ID="tbNote" runat="server"></asp:TextBox>
            </asp:Panel>
        </td>
        <td valign="top"><asp:ImageButton ID="btnAddToCart" runat="server" ImageUrl="~/assets/common/icons/addtocart.gif" ToolTip="Add to Cart" /></td>
    </tr>
</table>
<asp:Literal ID="hdnInStock" runat="server" Text="In Stock" Visible="False"></asp:Literal>
<asp:Literal ID="hdnNotInStock" runat="server" Text="Not in Stock" Visible="False"></asp:Literal>
<asp:Literal ID="hdnPriceInclTax" runat="server" Text="Price incl. Tax:" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPriceExclTax" runat="server" Text="Price excl. Tax:" Visible="false"></asp:Literal>
<asp:Literal ID="hdnTax" runat="server" Text="Tax:" Visible="false"></asp:Literal>
