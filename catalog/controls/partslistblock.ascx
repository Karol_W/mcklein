<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="machinesblock" Src="MachinesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartsListBlock" Codebehind="PartsListBlock.ascx.cs" %>

<np:machinesblock ID="npmblock" BorderWidth="0" runat="server" Visible="False" Filter="true" FilterMake="true" ShowCatalog="false"></np:machinesblock>

            <!-- <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/buttons/buy-btn-xsmall.png"
                ToolTip="Add Items to Cart" OnClick="btnSubmit_Click" Visible="false" CssClass="qty-pl-btn" />-->

            <asp:Label ID="sysNoData" runat="server"></asp:Label>
            <NP:GridView 
                ID="PartsListGrid"
                runat="server"
                AllowPaging="True" 
                EmptyDataText="No Items Found"
                AllowSorting="True" 
                PageSize="25"
                Width="100%" 
                AutoGenerateColumns="False" 
                OnPageIndexChanging="PartsListGrid_PageIndexChanging"
                OnRowDataBound="PartsListGrid_RowDataBound" OnRowUpdating="PartsListGrid_RowUpdating">
                <RowStyle CssClass="product-table-list-view" />
                
                <pagerstyle CssClass="ecommerce-paging" />
                
                <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                <Columns>
                    <asp:TemplateField HeaderText="colCompare|Compare">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>    
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCompare" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colImage|Image">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkImage" runat="server" NavigateUrl="~/catalog/partdetail.aspx?partno=">view detail</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ItemNo|Item No.">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkPartNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartNo") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ItemName|Item Name">
                        <ItemTemplate>
                            <asp:Literal ID="ltlPartName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartName") %>'></asp:Literal>
                            <br>
                            <!-- <asp:Literal ID="ltlPartDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Literal> -->
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PartType" HeaderText="colType|Type" />
                    <asp:TemplateField HeaderText="colUnit|Unit">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblPkgQty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SalesQty") %>' />
                            /
                            <asp:Label ID="lblPkgUnit" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SalesUnitText") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colPrice|Price">
                        <ItemStyle HorizontalAlign="Right" VerticalAlign="top"></ItemStyle>
                        <ItemTemplate>
                            <np:PartPriceDisplay ID="ppdPartPrice" runat="server" Part='<%# Container.DataItem %>' ShowItemizedTax="false" ShowPriceDescription="Never" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colTax|Tax" Visible="false">
                        <ItemStyle HorizontalAlign="right" VerticalAlign="top" />
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcTax" runat="server" ShowNotAvailable="true" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colAdd|">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAddToCart" runat="server" ImageUrl="~/assets/common/icons/addtocart.gif"
                                NavigateUrl="~/commerce/cart.aspx?AddPartNo=">Add To Cart.</asp:HyperLink>
                            <asp:HyperLink ID="lnkViewMoreDetail" runat="server" Visible="false" ImageUrl="~/assets/common/icons/detail.gif"
                                NavigateUrl="~/catalog/partdetail.aspx?partno=">View More detail</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colQty|Qty" Visible="false">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="txtQty" runat="server" Width="50px" Columns="4" ToolTip='<%# DataBinder.Eval(Container, "DataItem.PartNo") %>'>0</asp:TextBox>
                            <asp:HyperLink ID="lnkViewMoreDetailQty" runat="server" Visible="false" ImageUrl="~/assets/common/icons/detail.gif"
                                NavigateUrl="~/catalog/partdetail.aspx?partno=">View More detail</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Position="TopAndBottom" Mode="Numeric" />
                <PagerStyle HorizontalAlign="Right"></PagerStyle>
            </NP:GridView>

<asp:DataList 
    ID="PartsListRepeater" 
    runat="server" 
    CellSpacing="0" 
    CellPadding="0"
    GridLines="Both" 
    RepeatDirection="Horizontal"  
    RepeatColumns="3"
    RepeatLayout="Table" 
    CssClass="item-detail-list" 
    Visible="False" 
    ItemStyle-VerticalAlign="Bottom"
    Width="100%" 
    HorizontalAlign="Center" 
    OnItemDataBound="PartDetailListRepeater_ItemDataBound" 
    OnItemCommand="PartDetailListRepeater_ItemCommand">
    <ItemStyle VerticalAlign="Top"></ItemStyle>
 <ItemTemplate> 
        <div class="062519">  
            <asp:HyperLink runat="server" ID="PartImageLink" />
        </div>
        <!-- <asp:ImageButton ID="ibtnViewDetails" CssClass="item-list-buy" runat="server" CommandName="ViewDetails" ImageUrl="~/assets/common/buttons/details.gif" AlternateText="view details" /> -->
        <p><asp:HyperLink runat="server" ID="lnkPartName" NavigateUrl="~/catalog/partdetail.aspx?PartNo=" /></p>
        
        <!-- COLOR SWATCH -->
        <!--<div id="colorSwatch">
            <ul>
                <li><button type="button" id="colorBlack" class="colors"></button></li>
                <li><button type="button" id="colorBlue"  class="colors"></button></li>
                <li><button type="button" id="colorBrown" class="colors"></button></li>
                <li><button type="button" id="colorFuchsia" class="colors"></button></li>
                <li><button type="button" id="colorGreen" class="colors"></button></li>
                <li><button type="button" id="colorMint" class="colors"></button></li>
                <li><button id="colorNavy" class="colors"></button></li>
                <li><button id="colorOrange" class="colors"></button></li>
                <li><button id="colorPink" class="colors"></button></li>
                <li><button id="colorRed" class="colors"></button></li>
            </ul>
        </div>-->
        

        <!-- <p> <asp:Literal ID="ltlDescription" runat="server" /> </p> -->
        <!-- <p> <%= hdnProductCode.Text %> <asp:Literal ID="ltlPartCode" runat="server" /> </p> -->
        <asp:Panel ID="pnlOnSale" runat="server" Visible="false">
            <!--<asp:Image ID="imgOnSale" runat="server" ImageUrl="~/assets/common/images/onsale.gif" />-->
            <br /> 
             <div class="pdl-item-price">                
            
            <p><np:PriceDisplay ID="prcOriginalPrice" runat="server" /></p></div>
            <div class="pdl-item-sprice">
            <p><np:PartPriceDisplay ID="ppdSalePrice" runat="server" ShowPriceDescription="Always"
             PriceDescription='<%# hdnSalePrice.Text %>' 
             PriceIncludingTaxDescription='<%# hdnSalePriceInclTax.Text %>' 
             PriceExcludingTaxDescription='<%# hdnSalePriceExclTax.Text %>' 
             Part='<%# Container.DataItem %>' /> </p></div>
        </asp:Panel>
                        
        <asp:Panel ID="pnlNotOnSale" runat="server" Visible="false">
            <p><np:PartPriceDisplay ID="ppdPrice" runat="server" ShowPriceDescription="Always" 
            Part='<%# Container.DataItem %>'/></p>
        </asp:Panel>

       <asp:DropDownList ID="ddlChildParts" runat="server"></asp:DropDownList>
    <div class="button-action-wrapper">
       <asp:ImageButton ID="ibtnAddToWishList" runat="server" CommandName="AddToWishList" AlternateText="add to wishlist" /> 
       <asp:ImageButton ID="ibtnAddToCart" runat="server" CommandName="AddToCart" AlternateText="add to cart" />
           
            <!---<asp:HyperLink ID="ilnkViewMoreDetail" runat="server" Visible="false" ImageUrl="~/assets/common/icons/detail.gif"
                NavigateUrl="~/catalog/partdetail.aspx?partno=">View More detail</asp:HyperLink>-->
        </div>

    </ItemTemplate>
</asp:DataList>
<asp:DataList 
    ID="PartDetailListRepeater" 
    runat="server" 
    CellSpacing="0" 
    CellPadding="0"
    GridLines="Both" 
    RepeatDirection="Horizontal"  
    RepeatColumns="3"
    RepeatLayout="Table" 
    CssClass="item-detail-list" 
    Visible="False" 
    ItemStyle-VerticalAlign="Bottom"
    Width="100%" 
    HorizontalAlign="Center" 
    OnItemDataBound="PartDetailListRepeater_ItemDataBound" 
    OnItemCommand="PartDetailListRepeater_ItemCommand">
    <ItemStyle VerticalAlign="Top"></ItemStyle>
    <ItemTemplate>   
        <asp:HyperLink runat="server" ID="PartImageLink" />
        <!-- <asp:ImageButton ID="ibtnViewDetails" CssClass="item-list-buy" runat="server" CommandName="ViewDetails" ImageUrl="~/assets/common/buttons/details.gif" AlternateText="view details" /> -->
        <p><asp:HyperLink runat="server" ID="lnkPartName" NavigateUrl="~/catalog/partdetail.aspx?PartNo=" /></p>
        <!-- <p> <asp:Literal ID="ltlDescription" runat="server" /> </p> -->
        <!-- <p> <%= hdnProductCode.Text %> <asp:Literal ID="ltlPartCode" runat="server" /> </p> -->
        <asp:Panel ID="pnlOnSale" runat="server" Visible="false">
            <!--<asp:Image ID="imgOnSale" runat="server" ImageUrl="~/assets/common/images/onsale.gif" />-->
            <br /> 
             <div class="pdl-item-price">                
            
            <p><np:PriceDisplay ID="prcOriginalPrice" runat="server" /></p></div>
            <div class="pdl-item-sprice">
            <p><np:PartPriceDisplay ID="ppdSalePrice" runat="server" ShowPriceDescription="Always" PriceDescription='<%# hdnSalePrice.Text %>' PriceIncludingTaxDescription='<%# hdnSalePriceInclTax.Text %>' PriceExcludingTaxDescription='<%# hdnSalePriceExclTax.Text %>' Part='<%# Container.DataItem %>' /> </p></div>
        </asp:Panel>
                        
        <asp:Panel ID="pnlNotOnSale" runat="server" Visible="TRUE">
            <p><np:PartPriceDisplay ID="ppdPrice" runat="server" ShowPriceDescription="Always" Part='<%# Container.DataItem %>'/></p>
        </asp:Panel>

     <div class="child_parts"><asp:DropDownList ID="ddlChildParts" runat="server"></asp:DropDownList></div>
    <div class="button-action-wrapper">
            
          <!-- <asp:ImageButton ID="ibtnAddToWishList" runat="server" CommandName="AddToWishList" AlternateText="add to wishlist" />
           <asp:ImageButton ID="ibtnAddToCart" runat="server" Visible="true" CommandName="AddToCart" AlternateText="add to cart" />-->
           <!-- <asp:HyperLink ID="ilnkViewMoreDetail" runat="server" Visible="false" ImageUrl="~/assets/common/icons/detail.gif"
                NavigateUrl="~/catalog/partdetail.aspx?partno=">View More detail</asp:HyperLink>-->
        </div>

    </ItemTemplate>
</asp:DataList>
<asp:Literal ID="errNoPartsInCategory" runat="server" Visible="False" Text="No items were found in the selected category."></asp:Literal>
<asp:Literal ID="hdnAdminVerify" runat="server" Visible="False" Text="The Web site administrator should verify the following:"></asp:Literal>
<asp:Literal ID="hdnHasParts" runat="server" Visible="False" Text="- The category has items."></asp:Literal>
<asp:Literal ID="hdnPartsUnavailable" runat="server" Visible="False" Text="The available flag is checked for items in the category"></asp:Literal>
<asp:Literal ID="hdnPartsExpired" runat="server" Visible="False" Text="- The items in the category have an expired date prior to today."></asp:Literal>
<asp:Literal ID="hdnNoCategory" runat="server" Visible="False" Text="The category ID in the URL exists in the catalogs category table in the zed eCommerce database"></asp:Literal>
<asp:Literal ID="hdnViewDetails" runat="server" Visible="False" Text="View Details"></asp:Literal>
<asp:Literal ID="hdnCustomize" runat="server" Visible="False" Text="Customize"></asp:Literal>
<asp:Literal ID="hdnProductCode" runat="Server" Visible="False" Text="Product Code:"></asp:Literal>
<asp:Literal ID="hdnOrigPrice" runat="server" Visible="False" Text="Original Price:"></asp:Literal>
<asp:Literal ID="hdnSalePrice" runat="server" Visible="False" Text="Sale Price:"></asp:Literal>
<asp:Literal ID="hdnSalePriceInclTax" runat="server" Visible="False" Text="Sale Price incl. Tax:"></asp:Literal>
<asp:Literal ID="hdnSalePriceExclTax" runat="server" Visible="False" Text="Sale Price excl. Tax:"></asp:Literal>
<asp:Literal ID="hdnPrice" runat="server" Visible="False" Text="Price:"></asp:Literal>
<asp:Literal ID="hdnPriceInclTax" runat="server" Visible="false" Text="Price incl. Tax"></asp:Literal>
<asp:Literal ID="hdnPriceExclTax" runat="server" Visible="false" Text="Price excl. Tax"></asp:Literal>
<asp:Literal ID="hdnTax" runat="server" Visible="false" Text="Tax"></asp:Literal>
<!--new-->