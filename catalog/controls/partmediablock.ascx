<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartMediaBlock" CodeBehind="PartMediaBlock.ascx.cs" %>
<asp:Panel ID="MediaPanel" runat="server">
    <asp:HyperLink ID="sysMMImageLink" runat="server" Target="_blank">
        <asp:Image runat="server" ID="imgPart" Visible="false"></asp:Image></asp:HyperLink>
    <asp:Panel ID="AddlMediaPanel" runat="server">
    </asp:Panel>
    <%--jQuery for closing panels when another is opened--%>
    <script type="text/javascript">
    $(document).ready(function() {
        $("h4.panel-title a").click(function(){
            console.log("test");
            var foundIn = $(this).parent().parent().parent().siblings().find(".in");
            //foundIn.addClass("collapsing");
            //foundIn.removeClass("collapse");
            foundIn.animate({
                height: "30px"
            }, 300, function(){
                foundIn.removeClass("in");
            });

            var glyph = $(this).parent().parent().parent().siblings().find(".glyphicon-minus");
            glyph.removeClass("glyphicon-minus");
            glyph.addClass("glyphicon-plus")
        });
    });
</script>
    <div class="partDetailImageContainers">
        <asp:Repeater ID="Repeater_Content" runat="server" OnItemDataBound="Repeater_Content_ItemDataBound">
            <ItemTemplate>
                <div id="partDetailImageContainer" runat="server">
                    <!--Assign class as partNo-->
                    <!-- The Div ID corresponds to the color code. Ie, 9 = Pink, 0 = Orange-->

                    <div id="carrousel" class="carousel slide"  data-ride="carousel" runat="server">
                        <div class="carousel-inner" role="listbox">
                            <asp:Panel id="Image1_panel" class="item" runat="server">
                                <asp:Image class ="img-responsive" ID="Image1" runat="server" />
                            </asp:Panel>
                            <asp:Panel id="Image1s_panel" class="item" runat="server">
                                <asp:Image ID="Image1s" runat="server" />
                            </asp:Panel>
                            <asp:Panel id="Image1s1_panel" class="item" runat="server">
                                <asp:Image ID="Image1s1" runat="server" />
                            </asp:Panel>
                            <asp:Panel id="Image1s2_panel" class="item" runat="server">
                                <asp:Image ID="Image1s2" runat="server" />
                            </asp:Panel>
                            <asp:Panel id="Image1s3_panel" class="item" runat="server">
                                <asp:Image ID="Image1s3" runat="server" />
                            </asp:Panel>
                        </div>
                        <a id="leftCarousel" class="left carousel-control" href="#carrousel" runat="server" role="button" data-slide="prev">
                            <span class=" sliderControl glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a id="rightCarousel" class="right carousel-control" href="#carrousel" runat="server" role="button" data-slide="next">
                            <span class=" sliderControl glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div id="partImages">
                        <h1 id="clickimageh1">Click Image below to view gallery</h1>
                        <div class="mc-thumbs-left">
                            <div class="partDetailImgBackground hidden">
                                <h3>View Gallery</h3>
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                            <asp:Image ID="SideImage1" runat="server" />
                            
                        </div>
                        <div class="mc-thumbs-right">
                            <div class="partDetailImgBackground2 hidden">
                                <h3>View Gallery</h3>
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                            <asp:Image ID="ModelImage" runat="server" />
                        </div>
                        <div class="mc-thumbs-bottom">
                            <div class="partDetailImgBackground3 hidden">
                                <h3>View Gallery</h3>
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                            <asp:Image ID="SideImage2" runat="server" />
                        </div>
                        <div class="mc-thumbs-bottom-right">
                            <div class="partDetailImgBackground4 hidden">
                                <h3>View Gallery</h3>
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                            <asp:Image ID="SideImage3" runat="server" />
                        </div>
                    </div>
                    <div class="item-FS">
                        <div class="fullscreen-product-banner">
                            <p class="pull-right close-product-fullscreen">
                                CLOSE X
                            </p>
                        </div>
                        <div class="col-md-12 hidden-sm hidden-xs" id="sliderthumbs" clientidmode="Static"
                            runat="server">
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="slider">
                                <div id="allPartImages" clientidmode="Static" runat="server">
                                    <div id="myFullCarousel" class="carousel slide" runat="server">
                                        <div id="allPartImagesCarrousel" clientidmode="Static" runat="server" class="carousel-inner">
                                        </div>
                                        <a id="leftFullCarousel" class="left carousel-control" href="#myFullCarousel" runat="server" role="button"
                                            data-slide="prev">
                                            <img src="/assets/common/themes/ecommerce81//assets/img/left-carousel-arrow.png"
                                                alt="">
                                        </a><a id="rightFullCarousel" class="right carousel-control" href="#myFullCarousel" runat="server" role="button"
                                            data-slide="next">
                                            <img src="/assets/common/themes/ecommerce81//assets/img/right-carousel-arrow.png"
                                                alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl00_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl01_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl02_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl03_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl04_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl05_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl06_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl07_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl08_myFullCarousel').carousel({
                            interval: 4000
                        });

                        // handles the carousel thumbnails
                        $('[id^=carousel-selector-]').click(function () {
                            var id_selector = $(this).attr("id");
                            var id = id_selector.substr(18);
                            id = parseInt(id);
                            $('#ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl00_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl01_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl02_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl03_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl04_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl05_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl06_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl07_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl08_myFullCarousel').carousel(id);
                            $('[id^=carousel-selector-]').removeClass('selected');
                            $(this).addClass('selected');
                        });

                        // when the carousel slides, auto update
                        $('#ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl00_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl01_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl02_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl03_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl04_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl05_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl06_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl07_myFullCarousel, #ctl00_ctl00_mainslot_partmedia_nppmblock_Repeater_Content_ctl08_myFullCarousel').on('slid', function (e) {
                            var id = $('.item.active').data('slide-number');
                            id = parseInt(id);
                            $('[id^=carousel-selector-]').removeClass('selected');
                            $('[id=carousel-selector-' + id + ']').addClass('selected');
                        });
                        $(".mc-thumbs-left").hover(function(){
                            $(".partDetailImgBackground").removeClass("hidden");
                        },function(){
                            $(".partDetailImgBackground").addClass("hidden");
                        }
                        );
                        $(".mc-thumbs-right").hover(function(){
                            $(".partDetailImgBackground2").removeClass("hidden");
                        },function(){
                            $(".partDetailImgBackground2").addClass("hidden");
                        }
                        );
                        $(".mc-thumbs-bottom").hover(function(){
                            $(".partDetailImgBackground3").removeClass("hidden");
                        },function(){
                            $(".partDetailImgBackground3").addClass("hidden");
                        }
                        );
                        $(".mc-thumbs-bottom-right").hover(function(){
                            $(".partDetailImgBackground4").removeClass("hidden");
                        },function(){
                            $(".partDetailImgBackground4").addClass("hidden");
                        }
                        );
                        $(".partDetailImgBackground, .partDetailImgBackground2, .partDetailImgBackground3, .partDetailImgBackground4").click(function(){
                            $(".item-FS").addClass("full-screen-product");
                            $(".fullscreen-product-banner").css("display","block");
                        });
                    </script>

                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
