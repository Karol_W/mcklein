<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="partpricedisplay.ascx.cs" Inherits="netpoint.catalog.controls.PartPriceDisplay" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="QtyPriceDisplay" Src="~/catalog/controls/partquantityprice.ascx" %>
<asp:Label ID="lblAll" runat="server" EnableViewState="false">
<asp:Literal ID="ltlPrice" runat="server" Text="Price:" Visible="false" />
<asp:Literal ID="ltlPriceInclTax" runat="server" Text="Price incl. Tax:" Visible="false" />
<asp:Literal ID="ltlPriceExclTax" runat="server" Text="Price excl. Tax:" Visible="false" />
<asp:Label ID="lblPrice" runat="server"><np:PriceDisplay ID="prcPrice" runat="server" /></asp:Label>
<asp:PlaceHolder ID="pnlTax" runat="server" Visible="false">
   
    <asp:Literal ID="ltlTax" runat="server" Text="Tax:" /><asp:Label ID="lblTax" runat="server"><np:PriceDisplay ID="prcTax" runat="server" /></asp:Label>
</asp:PlaceHolder>
<np:QtyPriceDisplay ID="qtyPriceDisplay" runat="server" Visible="false" />
</asp:Label>
