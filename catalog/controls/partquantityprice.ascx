﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="partquantityprice.ascx.cs" Inherits="netpoint.catalog.controls.partquantityprice" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<asp:Repeater ID="rptQtyPrice" runat="server" OnItemDataBound="PriceBound">
    <HeaderTemplate>
    <ul class="volumeDiscountList">
    </HeaderTemplate>
    <ItemTemplate>
        <li >
            <asp:Literal ID="ltlQty" runat="server" />
            <asp:Literal ID="ltlSeparator" runat="server" Text="@" />
            <np:PriceDisplay ID="prcPrice" runat="server" />
        </li>
    </ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
