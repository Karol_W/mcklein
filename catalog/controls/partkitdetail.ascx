<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartKitDetail" Codebehind="PartKitDetail.ascx.cs" %>
<asp:GridView 
    id="gvKitDetail"
    runat="server" 
    AutoGenerateColumns="false" 
    HeaderStyle-CssClass="npsubheader"
    CssClass="nptable" ShowHeader="false"
    RowStyle-CssClass="npbody" 
    AlternatingRowStyle-CssClass="npbodyalt" 
    EmptyDataText="No Records Found">
    <EmptyDataRowStyle CssClass="npempty" Height="50px" />
    <Columns>
        <asp:TemplateField>
			<ItemTemplate>
				<asp:Image id="imgPart" runat="server"></asp:Image>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
			<ItemTemplate>
				<asp:label id="lblPartNo" runat="server"></asp:label>-
				<asp:label id="lblPartName" runat="server"></asp:label></B>
				<asp:label id="lblPartType" runat="server"></asp:label><br />
				<asp:label id="lblPartCode" runat="server" Visible="False"></asp:label><br />
				<asp:label id="lblManufacturerName" runat="server" Visible="False"></asp:label>
				<asp:label id="lblManufacturerPartNo" runat="server" Visible="False"></asp:label>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>
