<%@ Control Language="C#" AutoEventWireup="true" Codebehind="similarsellers.ascx.cs"
    Inherits="netpoint.catalog.controls.similarsellers" %>
<asp:Label runat="server" ID="lblPeopleWhoBought" CssClass="npbody" Text="People who bought this also bought:"></asp:Label>
<ul>
<asp:DataList ID="dlSellers" runat="server" CssClass="npbody" RepeatLayout="flow" RepeatColumns="10">
    <ItemTemplate>
        <li>
            <asp:HyperLink runat="server" ID="lnkSimilar" NavigateUrl="~/catalog/partdetail.aspx?partno={0}"></asp:HyperLink>
        </li>
    </ItemTemplate>
</asp:DataList>
</ul>