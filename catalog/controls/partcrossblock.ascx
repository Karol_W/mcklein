<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartCrossBlock" Codebehind="PartCrossBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<ASP:DataList id="dlCrossRepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="Both"
	BorderWidth="1" RepeatColumns="3" RepeatLayout="Table" CssClass="suggested-items-product-page-wrapper" Visible="True" ItemStyle-VerticalAlign="Top"
	Width="100%">
	<ItemTemplate>

	<div class="suggested-items-product">
		<div class="suggested-pic-price">
		<asp:HyperLink ID="lnkImage" Runat="server" CssClass="suggested-item-pic"></asp:HyperLink>
			<ul>
				<li><np:PartPriceDisplay id="ppdPrice" runat="server" Visible="False" /></li>
				
				<li><asp:HyperLink ID="lnkAddToCart" ImageUrl="~/assets/common/icons/addtocart.gif" Runat="server" Visible="False"></asp:HyperLink></li>
			</ul>
		</div>	
		
			<ul>
				<li><asp:HyperLink ID="lnkValue" Runat="server" Visible="False"></asp:HyperLink></li>
				
				<li><asp:HyperLink ID="lnkName" Runat="server" Visible="False"></asp:HyperLink></li>
				
				<li class="suggested-item-desc"><asp:Label id="lblRefDesc" runat="server" Visible="False"></asp:Label></li>
				
				<li><asp:Label ID="lblDescription" Runat="server" Visible="False"></asp:Label></li>
			</ul>
	</div>
	
	</ItemTemplate>
</ASP:DataList>
