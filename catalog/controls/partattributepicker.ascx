<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartAttributePicker" Codebehind="PartAttributePicker.ascx.cs" %>
<asp:Panel ID="pnlMain" Runat="server">
	<table class="npbody" cellspacing="0" cellpadding="1" width="100%" border="0">
		<tr>
			<td valign="top" align="right"><asp:ImageButton id="btnReset" ImageUrl="~/assets/common/buttons/reset-btn-small.png" runat="server" ToolTip="Reset" /></td>
		</tr>
		<tr>
			<td>
				<table id="Table1" style="BORDER-COLLAPSE: collapse" cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
						<td class="nplabel"><asp:label id="sysDim0" Runat="server"></asp:label></td>
						<td><asp:dropdownlist id="ddl0" Runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim1" Runat="server"></asp:label></td>
						<td><asp:dropdownlist id="ddl1" Runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim2" Runat="server"></asp:label></td>
						<td><asp:DropDownList id="ddl2" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:Label id="sysDim3" Runat="server"></asp:Label></td>
						<td><asp:DropDownList id="ddl3" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim4" Runat="server"></asp:label></td>
						<td><asp:dropdownlist id="ddl4" Runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim5" Runat="server"></asp:label></td>
						<td><asp:dropdownlist id="ddl5" Runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim6" Runat="server"></asp:label></td>
						<td><asp:DropDownList id="ddl6" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim7" Runat="server"></asp:label></td>
						<td><asp:DropDownList id="ddl7" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:Label id="sysDim8" Runat="server"></asp:Label></td>
						<td><asp:DropDownList id="ddl8" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="nplabel"><asp:label id="sysDim9" Runat="server"></asp:label></td>
						<td><asp:DropDownList id="ddl9" Runat="server" AutoPostBack="True"></asp:DropDownList></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<asp:Literal id="sysError" runat="server"></asp:Literal>
	<asp:Literal id="hdnNoParts" runat="server" Visible="False" Text="There are no associated with this item or category."></asp:Literal>
</asp:Panel>
