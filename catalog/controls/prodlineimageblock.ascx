<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ProdLineImageBlock"
    Codebehind="ProdLineImageBlock.ascx.cs" %>
<asp:DataList ID="rptImages" runat="server" CellPadding="1" RepeatColumns="5">
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle>
    <ItemTemplate>
        <asp:HyperLink ID="lnkThumbnail" runat="server"></asp:HyperLink>
    </ItemTemplate>
</asp:DataList>