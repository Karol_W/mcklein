<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ProdLineBlock" Codebehind="ProdLineBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<table border="0" cellpadding="1" cellspacing="0" class="npbody" width="100%">
    <tr>
        <td align="right">
            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/icons/addtocart.gif"
                ToolTip="Add to Cart" OnClick="btnSubmit_Click"></asp:ImageButton></td>
    </tr>
    <tr>
        <td>
            <NP:GridView ID="gridProdLine" runat="server" CellPadding="1" AllowPaging="True"
                AllowSorting="True" Width="100%" AutoGenerateColumns="False" PageSize="20" EmptyDataText="No Records Found" 
                OnRowCommand="gridProdLine_RowCommand" 
                OnRowDataBound="gridProdLine_RowDataBound" 
                OnSorting="gridProdLine_Sorting"
                OnPageIndexChanging="gridProdLine_PageIndexChanging">
                <RowStyle CssClass="npbody" />
                <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                <Columns>
                    <asp:TemplateField HeaderText="colImage|Image">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkThumbnail" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PartNo" SortExpression="PartNo" HeaderText="ItemNo|Item No.">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PartType" SortExpression="PartType" HeaderText="ItemType|Item Type">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="colPrice|Price">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <np:PartPriceDisplay ID="ppdPrice" runat="server" Part='<%# Container.DataItem %>' ShowItemizedTax="false" ShowPriceDescription="never" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colTax|Tax" Visible="false">
                        <ItemStyle HorizontalAlign="Right" />
                        <ItemTemplate>
                            <np:PriceDisplay ID="prcTax" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Inventory" HeaderText="colInventory|Inventory">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Literal ID="ltlInStock" runat="server" Text="ltlInStock"></asp:Literal>
                            <asp:Literal ID="ltlNotInStock" runat="server" Text="ltlNotInStock"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Inventory" HeaderText="colNotes|Notes">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="tbNote" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colQty|Qty">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="txtQuantity" runat="server" Columns="7" Width="75px"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colAdd|">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnAddToCart" runat="server" ImageUrl="~/assets/common/icons/addtocart.gif"
                                CommandArgument='<%# DataBinder.Eval(Container, "DataItem.PartNo") %>' CommandName='addtocart'>
                            </asp:ImageButton>
                            <asp:HyperLink ID="lnkAddToCart" runat="server" Visible="False"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colEdit|">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Position="TopAndBottom" Mode="Numeric" />
                <PagerStyle HorizontalAlign="Right"></PagerStyle>
            </NP:GridView>
        </td>
    </tr>
</table>
<asp:Literal ID="hdnInStock" runat="server" Text="In Stock" Visible="False"></asp:Literal>
<asp:Literal ID="hdnNotInStock" runat="server" Text="Not in Stock" Visible="False"></asp:Literal>
<asp:Literal ID="hdnAddToCart" runat="server" Visible="False" Text="Add to Cart"></asp:Literal>
<asp:Literal ID="hdnPriceInclTax" runat="server" Text="Price incl. Tax" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPriceExclTax" runat="server" Text="Price excl. Tax" Visible="false"></asp:Literal>
