<%@ Control Language="c#" Inherits="netpoint.catalog.controls.MachinesBlock" Codebehind="MachinesBlock.ascx.cs" %>
<asp:panel id="MachinePanel" runat="server">
	<table>
		<tr>
			<td colspan="4">
				<asp:DropDownList id="ddlCatalog" runat="server" DataTextField="CatalogName" DataValueField="CatalogID"
					EnableViewState="True" AutoPostBack="True" onselectedindexchanged="Catalog_SelectedIndexChanged">
					<asp:ListItem Value="">ddlCatalog</asp:ListItem>
				</asp:DropDownList></td>
		</tr>
		<tr>
			<td>
				<asp:DropDownList id="Make" runat="server" DataTextField="ddtext" DataValueField="ddvalue" EnableViewState="True"
					AutoPostBack="True"></asp:DropDownList></td>
			<td>
				<asp:DropDownList id="Model" runat="server" DataTextField="ddtext" DataValueField="ddvalue" EnableViewState="True"
					AutoPostBack="True"></asp:DropDownList></td>
			<td>
				<asp:DropDownList id="Year" runat="server" DataTextField="ddtext" DataValueField="ddvalue" EnableViewState="True"
					AutoPostBack="True" onselectedindexchanged="Year_SelectedIndexChanged"></asp:DropDownList></td>
			<td>
				<asp:ImageButton id="btnReset" onclick="ResetMachines" ToolTip="Reset Make, Model, and Year" ImageUrl="~/assets/common/buttons/reset.gif" Runat="server" /></td>
		</tr>
	</table>
	<asp:DataList id="MyMachinesList" Runat="server" CellPadding="3" RepeatColumns="5" RepeatDirection="Horizontal">
		<ItemTemplate>
			<asp:LinkButton Runat="server" ID="MachineLink" Text="Machine" />
		</ItemTemplate>
	</asp:DataList>
	<asp:Literal id="errNoPartsAssigned" runat="server" Text="No items have been associated with assemblies in this catalog" Visible="False"></asp:Literal>
</asp:panel><asp:label id="lblMachineCode" Runat="server" Visible="False"></asp:label>
<asp:literal id="sysMachineID" runat="server" Text="0" Visible="False"></asp:literal>
<asp:literal id="hdnMake" runat="server" Text="Make" Visible="False"></asp:literal>
<asp:literal id="hdnYear" runat="server" Text="Year" Visible="False"></asp:literal>
<asp:literal id="hdnModel" runat="server" Text="Model" Visible="False"></asp:literal>
<asp:Label id="errNoCatalog" runat="server" cssclass="npwarning" Visible="False" Text="You cannot select machines until a catalog has been created"></asp:Label>
<asp:Literal id="hdnCatalog" runat="server" Text="Catalog" Visible="False"></asp:Literal>
