<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartAttributesBlock" Codebehind="PartAttributesBlock.ascx.cs" %>
<asp:Panel ID="MainPanel" Runat="server">

				<asp:ImageButton id="clpGeneral" ToolTip="Collapse"
					runat="server"></asp:ImageButton>
				<asp:ImageButton id="expGeneral" ToolTip="Expand" runat="server"
					visible="false"></asp:ImageButton>&nbsp;
				<asp:Label id="lblDetailed" text="Detailed Specifications" Runat="server"></asp:Label>
				<asp:Panel id="AttributesPanel" Runat="server"></asp:Panel>
				
	<asp:Label id="sysStartExpanded" Runat="server" Visible="False" Text="True"></asp:Label>
</asp:Panel>
