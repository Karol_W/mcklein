<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartPriceBlock" Codebehind="PartPriceBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="wishbutton" Src="~/common/controls/AddToWishList.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>


<asp:panel id="PricePanel" HorizontalAlign="center" Runat="server" CssClass="npbody">
	<asp:Panel ID="pnlOriginalPrice" runat="server" Visible="false">
		<asp:Literal ID="ltlOriginalPrice" runat="Server" />
		<np:PriceDisplay id="sysOriginalPrice" Runat="server" />

	</asp:Panel>
	
<div class="item-price-stock-wrapper">
		<div class="item-base-price">
		<np:PartPriceDisplay id="sysBasePrice" Runat="server" ShowVolDiscounts="True"/>
		<asp:Label id="sysUnits" Runat="server"></asp:Label>
		</div>
	
			<div class="item-quantity-stock">
				<asp:Label id="ltlNotInStock" CssClass="npbody" Runat="server" Visible="False" text="Not in Stock"></asp:Label>
				<asp:Label id="sysInventoryCount" CssClass="npbody" Visible="False" runat="server"></asp:Label>
				<asp:Label id="sysUnitQuantity" CssClass="npbody" Runat="server"></asp:Label>
				<asp:Label id="lblUnitSeperator" CssClass="npbody" Runat="server" Visible="False" Text=" / "></asp:Label>
		
		
			<asp:Label id="ltlInStock" CssClass="npbody" Runat="server" Visible="False" text="In Stock"></asp:Label>
			<asp:Label id="ltlNotAvailable" CssClass="npbody" Runat="server" Visible="False" text="This item is out of stock and cannot be backordered"></asp:Label>
			</div>	          
</div>




	<asp:Label id="ltlReleaseDate" CssClass="npbody" Runat="server" Visible="False" text="Available On "></asp:Label>
	<asp:Label id="sysReleaseDate" CssClass="npbody" Runat="server" Visible="False"></asp:Label>
	
	

	<asp:Panel id="pnlOrderLineNotes" Runat="server" Visible="False">
		<asp:Label id="lblNote" Runat="server" text="Custom Note"></asp:Label>
		
		<asp:TextBox id="tbNote" Runat="server"></asp:TextBox>
	</asp:Panel>
	
	<div class="item-purchase-buttons">
		<asp:ImageButton ID="imgWishList" runat="server" OnClick="AddToWishlist" ToolTip="Add to Wishlist" ImageUrl="~/assets/common/buttons/save-btn-small.png"></asp:ImageButton>
		<asp:ImageButton id="AddButton" onclick="AddToCart" Runat="server" ImageUrl="~/assets/common/buttons/buy-btn-small.png"></asp:ImageButton>
	</div>
</asp:panel>
<asp:Literal id="hdnOriginally" Text="Originally" Visible="False" runat="server"></asp:Literal>
<asp:Literal id="hdnCount" Text="Count:" Visible="False" runat="server"></asp:Literal>



