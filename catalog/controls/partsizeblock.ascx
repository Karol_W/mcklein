<%@ Control Language="c#" Inherits="netpoint.catalog.controls.PartSizeBlock" Codebehind="PartSizeBlock.ascx.cs" %>
<asp:Panel ID="pnlMain" Runat="server">

<div class="item-part-details">
		<asp:Panel id="pnlSize" Runat="server">

		<asp:literal id="ltlDepth" Runat="server" Text="Depth"></asp:literal>:

					<asp:Label id="sysDepth" runat="server"></asp:Label>
					<asp:Label id="sysDepthUnits" runat="server"></asp:Label>
					
					<asp:Literal id="ltlWidth" Runat="server" Text="Width"></asp:Literal>:
					
					<asp:Label id="sysWidth" runat="server"></asp:Label>
					<asp:Label id="sysWidthUnits" runat="server"></asp:Label>
					
					<asp:literal id="ltlHeight" runat="server" Text="Height"></asp:literal>:
					
					<asp:Label id="sysHeight" runat="server"></asp:Label>
					<asp:Label id="sysHeightUnits" runat="server"></asp:Label>
					
		</asp:Panel>
		<asp:Panel id="pnlWeightAbbr" Runat="server">
			
			<asp:Literal id="ltlWeight" Runat="server" Text="Weight"></asp:Literal>:
			
					<asp:Label id="sysWeight" runat="server"></asp:Label>
					<asp:Label id="sysWeightUnits" runat="server"></asp:Label>
					
		</asp:Panel>
		<asp:Panel id="pnlVolume" Runat="server">
		
		<asp:literal id="ltlVolume" Runat="server" Text="Volume"></asp:literal>:
		
					<asp:Label id="sysVolume" runat="server"></asp:Label>
					<asp:Label id="sysVolumeUnits" runat="server"></asp:Label>
					
		</asp:Panel>
		<asp:Panel id="pnlArea" Runat="server">
			
			<asp:Literal id="ltlArea" Runat="server" Text="Area"></asp:Literal>:
			
					<asp:Literal id="sysAreaQty" runat="server"></asp:Literal>
					<asp:Literal id="sysAreaUnit" runat="server"></asp:Literal>
					
		</asp:Panel>
</div>
</asp:Panel>
