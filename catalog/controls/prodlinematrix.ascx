<%@ Control Language="c#" Inherits="netpoint.catalog.controls.ProdLineMatrix" Codebehind="ProdLineMatrix.ascx.cs" %>
<input id="hdnXValue" type="hidden" runat="server" value="1" />
<input id="hdnYValue" type="hidden" runat="server" value="0" />
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td class="npheader">
		    <table width="100%"><tr><td>
		        <asp:ImageButton id="clpGeneral" ToolTip="Collapse"
				    runat="server" OnClick="clpGeneral_Click"></asp:ImageButton>
			    <asp:ImageButton id="expGeneral" ToolTip="Expand" runat="server"
				    visible="false" OnClick="expGeneral_Click"></asp:ImageButton>&nbsp;
			    <asp:Label CssClass="npheader" id="lblDetailed" text="Attribute Matrix" Runat="server"></asp:Label>
		    </td></tr></table>
		</td>
	</tr>
	<tr>
	    <td>
	    <asp:Panel id="tblMatrix" runat="server">
	        <table width="100%" cellspacing="0">
                <tr class="npheader">
                    <td colspan="5" align="left">
                        <asp:DropDownList runat="server" ID="ddlXAttribute" AutoPostBack="True"></asp:DropDownList>
                    </td>
                    <td class="nplabel" align="center">
                        <asp:DropDownList runat="server" ID="ddlYAttribute" AutoPostBack="True"></asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table id="theMatrix" width="100%" runat="Server">
            </table>
            <table width="100%">
                <tr>
                    <td class="npheader" align="center">
                        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/buttons/addtocart.gif" OnClick="btnAdd_Click" ToolTip="Add" />
                    </td>
                </tr>
            </table>
            </asp:Panel>
	    </td>
	</tr>
	<tr>
		<td style="height: 69px"><asp:Panel id="AttributesPanel" Runat="server"></asp:Panel></td>
	</tr>
</table>
<asp:Label id="sysStartExpanded" Runat="server" Visible="False" Text="True"></asp:Label>

