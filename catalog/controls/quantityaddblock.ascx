<%@ Control Language="c#" Inherits="netpoint.catalog.controls.QuantityAddBlock" Codebehind="QuantityAddBlock.ascx.cs" %>
<asp:Panel ID="pnlOrderLineNotes" runat="server" Visible="False">

    <asp:Label ID="lblNote" runat="server" Text="Custom Note"></asp:Label>

    <asp:TextBox ID="tbNote" runat="server"></asp:TextBox>
</asp:Panel>
<asp:DropDownList runat="server" ID="ddlQty">
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="nbQuantity" runat="server" MaxLength="9"
    Columns="4" Text='1' Visible="false" />
<asp:ImageButton runat="server" ID="btnAddToCart" ImageUrl="~/assets/common/icons/addtocart.gif" ToolTip="Add to Cart" />
