﻿<%@ Control Language="C#" CodeBehind="partudfblock.ascx.cs" Inherits="netpoint.catalog.controls.partudfblock" %>
<ASP:DataList id="dlUDFRepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
	BorderWidth="0" RepeatColumns="1" RepeatLayout="Table" CssClass="npbody" Visible="True" ItemStyle-VerticalAlign="Top"
	Width="100%">
	<ItemStyle VerticalAlign="Top"></ItemStyle>
	<ItemTemplate>
	    <asp:Label ID="lblName" runat="server" /><asp:Label ID="lblValue" runat="server" class="description-body"/>		
	</ItemTemplate>
</ASP:DataList>