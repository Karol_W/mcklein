<%@ Register TagPrefix="np" TagName="machinesblock" Src="controls/MachinesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="QtyPriceDisplay" Src="~/catalog/controls/partquantityprice.ascx" %>
<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.search" Codebehind="search.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<section class="container">
<asp:Label runat="server" id="totalResults" Text=""></asp:Label>

<div id="searchContainer" class="white-bg">

                <asp:RadioButtonList ID="SearchType" CssClass="npheader" EnableViewState="True" AutoPostBack="True"
                    BorderWidth="0" RepeatDirection="Horizontal" runat="server">
                    <%-- Uncomment to enable basic/advanced radio buttons. Commented out because the drop down for advanced displays options that 
                    should not be available.
                    <asp:ListItem Value="basic" Selected="True">Basic</asp:ListItem>
                    <asp:ListItem Value="advanced">Advanced</asp:ListItem>--%>
                </asp:RadioButtonList>
    
    <table width="100%" class="search-input-wrapper">
        <tr>
            <td class="nplabel" style="width:20%;"><asp:Literal ID="ltlKeyword" runat="server" Text="Keyword"></asp:Literal></td>
            <td style="width: 250px"><asp:TextBox ID="Keywords" runat="server" EnableViewState="True" OnTextChanged="Keywords_TextChanged"></asp:TextBox></td>
            <td id="btnTD"><asp:Button ID="btnSearch1" runat="server" Text="Search" OnClick="btnSearch_Click1" /></td>
        </tr>
        <tr>
            <td class="nplabel"><asp:Literal ID="ltlSearchIn" runat="server" Text="Search In" Visible="False"></asp:Literal></td>
            <td class="searchDropdown"><asp:DropDownList ID="ddlCategory" runat="server" EnableViewState="True" Visible="False"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="nplabel"><asp:Literal ID="ltlManufacturer" runat="server" Text="Manufacturer" Visible="False"></asp:Literal></td>
            <td class="searchDropdown"><asp:DropDownList ID="MNFCode" runat="server" EnableViewState="True" DataValueField="ManufacturerCode" DataTextField="Name" Visible="False"></asp:DropDownList></td>
        </tr>
    </table>
    <asp:Panel ID="AdvancedPanel" runat="server" Visible="False">
        <table class="npbody" width="100%" class="search-input-wrapper">
            <tr>
                <td class="nplabel" style="width:20%;"><asp:Literal ID="ltlFits" runat="server" Text="Fits"></asp:Literal></td>
                <td><np:machinesblock ID="mmy" runat="server" BorderWidth="0" FilterMake="true" Filter="true"></np:machinesblock></td>
            </tr>
        </table>
    </asp:Panel>
    <%--<table width="100%" class="search-input-wrapper">
        <tr>
            <td style="width:20%;">&nbsp;</td>
            <td id="btnTD"><asp:Button ID="btnSearch1" runat="server" Text="Search" OnClick="btnSearch_Click1" /></td>
        </tr>
    </table>
    <br />
    <br />--%>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" class="npbody">
        <tr>
            <td>
                <%--Change pagesize to make more items display in a search. Use a var with radio buttons to give users an option.--%>
                <asp:GridView ID="PartsListGrid" runat="server" AutoGenerateColumns="False" Width="100%" OnRowDataBound="PartsListGrid_RowDataBound"
                    PageSize="10" AllowPaging="True" OnPageIndexChanging="PartsListGrid_PageIndexChanging" AllowSorting="False" EmptyDataText="No Items Found">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle HorizontalAlign="center" Height="50" VerticalAlign="middle" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="150px"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkThumb" runat="server" NavigateUrl="~/catalog/partdetail.aspx?PartNo={0}"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="PartNo" HeaderText="ItemNo|Item">
                            <ItemStyle Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkPartNo" runat="server" NavigateUrl="~/catalog/partdetail.aspx?PartNo={0}"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
<%--                        <asp:HyperLinkField DataNavigateUrlFields="PartNo" DataNavigateUrlFormatString="~/catalog/partdetail.aspx?PartNo={0}"
                            DataTextField="PartNo" SortExpression="PartNo" HeaderText="ItemNo|Item No">
                            <ItemStyle Width="10%"></ItemStyle>
                        </asp:HyperLinkField>--%>
                        <asp:TemplateField SortExpression="PartName" HeaderText="ItemDescription|Item Description">
                            <ItemTemplate>
                                <strong><asp:Literal ID="ltlPartNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartName") %>'></asp:Literal></strong><br/>
                                <asp:Literal ID="ltlPartDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="SpecialPrice" HeaderText="colPrice|Price">
                            <ItemTemplate>
                                <np:PriceDisplay ID="prcPrice" runat="server" />
                                <np:QtyPriceDisplay ID="qtyPriceDisplay" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Details">
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkAddPart" runat="server" ToolTip="Item details"
                                    ImageUrl="~/assets/common/icons/addtocart.gif" NavigateUrl="~/commerce/cart.aspx?AddPartNo=">HyperLink</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Position="TopAndBottom" Mode="Numeric" pagebuttoncount="5" />
                    <PagerStyle CssClass="pagination" HorizontalAlign="Right"></PagerStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>    
</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnAllCategories" runat="server" Visible="False" Text="All Categories"></asp:Literal>
    <asp:Literal ID="hdnAllManufacturers" runat="server" Visible="False" Text="All Manufacturers"></asp:Literal>
    <asp:Literal ID="hdnNA" runat="server" Visible="False" Text="N/A"></asp:Literal>
    <asp:Literal ID="hdnEnterKeyword" runat="server" Visible="False" Text="Please enter search keywords."></asp:Literal>
    <asp:Literal ID="hdnAddToCart" runat="server" Visible="False" Text="Add to cart"></asp:Literal>
    <asp:Label ID="sysError" runat="server" CssClass="npwarning"></asp:Label>
</asp:Content>
