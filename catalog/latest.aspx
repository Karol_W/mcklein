<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.latest" Codebehind="latest.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="plblock" Src="controls/PartsListBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="col-md-9 pull-right">
		<np:plblock ID="npplblock" runat="server" ListType="N" DisplayNumber="50" DisplayType="L" BorderWidth="1" TypeVisible="False" CompareVisible="False" />
	</div>
</asp:Content>
