<%@ Page Language="c#" MasterPageFile="~/masters/catalog.master" Inherits="netpoint.catalog.partlist" Codebehind="partlist.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="cblock" Src="controls/CategoriesBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="plblock" Src="controls/PartsListBlock.ascx" %>
<asp:Content ContentPlaceHolderID="headerslot" runat="server" ID="header">
    <meta name="keywords" content="<%= Keywords %>" />
    <meta name="description" content="<%= Description %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<div>  
  <!-- Remove comments below to show this category's description -->
   <h1><asp:literal runat="server" id="sysCategoryDescription"></asp:literal></h1> 
  
  <div class="item-listing-wrapper">
  <section class="container">
    <!-- Add Visible="false" to hide category links on the partlist page. -->

 
    <np:cblock ID="npcblock" runat="server" CatsPerCol="2" />


    <np:plblock ID="npplblock" runat="server" CompareVisible="False"
        TypeVisible="False" UnitVisible="False" />

</div>
  </section>
</div>
</asp:Content>
