<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.unsubscribe" Codebehind="unsubscribe.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <br />
    <table width="100%" cellpadding="0" cellspacing="0" class="npbody">
        <tr>
            <td align="left" valign="top">
                <asp:Label runat="server" ID="lblEmail" />
                <br />
                <asp:GridView runat="server" ID="gridLists" />
            </td>
        </tr>
    </table>
</asp:Content>
