<%@ Control Language="c#" Inherits="netpoint.common.controls.treemenu" Codebehind="treemenu.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %> 
      <ComponentArt:TreeView id="tvList" ClientScriptLocation="~/scripts" KeyboardEnabled="false"
        DragAndDropEnabled="false" 
        NodeEditingEnabled="false"
        CssClass="TreeView" 
        NodeCssClass="TreeNode" 
        SelectedNodeCssClass="SelectedTreeNode" 
        HoverNodeCssClass="HoverTreeNode"
        NodeEditCssClass="NodeEdit"
        LineImageWidth="19" 
        LineImageHeight="20"
        DefaultImageWidth="16" 
        DefaultImageHeight="16"
        ItemSpacing="0" 
        ImagesBaseUrl="~/assets/common/tree/"
        NodeLabelPadding="3"
        ParentNodeImageUrl="folder.gif" 
        LeafNodeImageUrl="file.gif" 
        ShowLines="true" 
        LineImagesFolderUrl="~/assets/common/tree/lines/"
        EnableViewState="true"
        runat="server" >
      </ComponentArt:TreeView>