<%@ Page Language="C#" MasterPageFile="~/masters/default.master" AutoEventWireup="true" Codebehind="partdownload.aspx.cs" Inherits="netpoint.common.controls.partdownload" %>
<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">
    <br />
</asp:Content>
<asp:Content runat="server" ID="hidden" ContentPlaceHolderID="hiddenslot">
    <asp:Literal runat="server" ID="errNotAuthorized" Text="Current User Not Auhorized to Download this File" Visible="false" ></asp:Literal>
    <asp:Literal runat="server" ID="errNOID" Text="Expecting Item Download ID" Visible="false" ></asp:Literal>
    <asp:Literal runat="server" ID="errNotLoggedID" Text="Please log in before downloading files" Visible="false" ></asp:Literal>
</asp:Content>