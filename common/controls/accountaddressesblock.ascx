<%@ Control Language="c#" Inherits="netpoint.common.controls.AccountAddressesBlock"
    Codebehind="AccountAddressesBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="AddressBlock" Src="~/common/controls/AddressBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="AccountAddresses" Src="~/common/controls/AccountAddresses.ascx" %>
<table class="npbody" width="100%" border="0">
    <tr>
        <td valign="top" align="left">
            <np:AddressBlock id="npaddressblock" runat="server" Visible="False">
            </np:AddressBlock>
        </td>
    </tr>
    <tr>
        <td>
            <np:AccountAddresses ID="ShippingAddresses" runat="server" ShipDestination="true" />
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:ImageButton ID="btnAddShipping" ToolTip="Add New Shipping Address" ImageUrl="~/assets/common/buttons/add.gif" runat="server"></asp:ImageButton>
        </td>
    </tr>
    <tr>
        <td>
            <np:AccountAddresses ID="BillingAddresses" runat="server" ShipDestination="false" />
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:ImageButton ID="btnAddBilling" ToolTip="Add New Billing Address" ImageUrl="~/assets/common/buttons/add.gif" runat="server"></asp:ImageButton>
        </td>
    </tr>
</table>