<%@ Control Language="c#" Inherits="netpoint.common.controls.login" Codebehind="login.ascx.cs" %>

<asp:Login ID="loginMain" runat="server"
    OnLoggedIn="loginMain_LoggedIn" RememberMeText="" OnLoad="loginMain_Load">
    <LayoutTemplate>
    
    
    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
    <asp:literal id="test" runat="server" visible="true"></asp:literal>
                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl00$loginMain">*</asp:RequiredFieldValidator>
            
            
                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                
                <asp:TextBox ID="Password" runat="server" TextMode="Password" AutoComplete="off"></asp:TextBox>
                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$loginMain">*</asp:RequiredFieldValidator>


                <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me on this computer."></asp:CheckBox>


                <asp:HyperLink ID="lnkForgotPassword" runat="server" NavigateUrl="~/common/accounts/forgotpassword.aspx" Text="Forgot your password?" />


                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
            
                <asp:ImageButton ID="LoginButton" runat="server" CommandName="Login" ImageUrl="~/assets/common/buttons/logon.gif" ValidationGroup="ctl00$loginMain"></asp:ImageButton>


    </LayoutTemplate>
</asp:Login>
<asp:Literal ID="hdnLoginFaiureText" runat="server" Text="Your login attempt was not successful. Please try again." Visible="False"></asp:Literal>
<asp:Literal ID="hdnLoginLockoutText" runat="server" Text="Your account has been locked due to too many failed password attempts." Visible="False"></asp:Literal>


