<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Control Language="c#" Inherits="netpoint.common.controls.AccountInformationBlock" Codebehind="AccountInformationBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ Import Namespace="netpoint.api" %>
<table>
	<tr>
		<td class="nplabel"><asp:literal id="ltlAccountName" Runat="server" Text="Account Name"></asp:literal></td>
		<td class="npbody"><asp:textbox id="AccountName" MaxLength="50" Columns="30" runat="server" class="form-control" ></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal id="ltlPhone1" Runat="server" Text="Phone1"></asp:literal></td>
		<td class="npbody"><asp:textbox id="Phone1" MaxLength="50" Columns="15" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal id="ltlPhone2" Runat="server" Text="Phone2"></asp:literal></td>
		<td class="npbody"><asp:textbox id="Phone2" MaxLength="50" Columns="15" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal id="ltlFax" Runat="server" Text="Fax"></asp:literal></td>
		<td class="npbody"><asp:textbox id="PhoneFax" MaxLength="50" Columns="15" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlMobile" Runat="server" Text="Mobile"></asp:literal></td>
		<td class="npbody"><asp:textbox id="PhoneMobile" MaxLength="50" Columns="15" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlEmail" Runat="server" Text="Email"></asp:literal></td>
		<td class="npbody"><asp:textbox id="EmailAddress" MaxLength="100" Columns="50" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlWebsite" Runat="server" Text="Website"></asp:literal></td>
		<td class="npbody"><asp:textbox id="Website" MaxLength="50" Columns="50" runat="server" class="form-control"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlTaxID" Runat="server" Text="TaxID"></asp:literal></td>
		<td class="npbody">
		    <asp:TextBox ID="tbxTaxID" MaxLength="50" runat="server" class="form-control" Visible="false"></asp:TextBox>
		    <asp:label id="TaxID"  MaxLength="50" Columns="20" runat="server"></asp:label>
		    <asp:label id="TaxExempt" Runat="server" Visible="False"></asp:label></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="PurchaseLimitLabel" Runat="server">Purchase Limit</asp:label></td>
		<td class="npbody"><np:PriceDisplay ID="PurchaseLimit" runat="server" DisplayCurrency="Both" /></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal id="ltlDefaultBillTo" runat="server" text="Default Bill To"></asp:literal></td>
		<td class="npbody"><asp:dropdownlist id="DefaultBillAddressID" runat="server" class="form-control"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlDefaultBillOption" Runat="server" Text="Default Bill Option"></asp:literal></td>
		<td class="npbody"><asp:dropdownlist id="DefaultBillOptionID" runat="server" class="form-control"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlDefaultShipTo" Runat="server" Text="Default Ship To"></asp:literal></td>
		<td class="npbody"><asp:dropdownlist id="DefaultShipAddressID" runat="server" class="form-control"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal ID="ltlDefaultShipOption" Runat="server" Text="Default Delivery Option"></asp:literal></td>
		<td class="npbody"><asp:dropdownlist id="DefaultShipOptionID" runat="server" class="form-control"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="npbody" align="center" colspan="2">
		    <asp:imagebutton id="SaveButton" onclick="SaveAccount" Runat="server" ImageUrl="~/assets/common/buttons/update.gif" />
			<br /><asp:Label Runat="server" ID="SaveMessage" Visible="False">Saved</asp:Label></td>
	</tr>
</table>
