<%@ Control Language="c#" Inherits="netpoint.common.controls.NPDatePicker" Codebehind="NPDatePicker.ascx.cs" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<LINK REL="StyleSheet" HREF="<%=WebRoot%>/assets/common/calendar/calendarStyle.css" TYPE="text/css">
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="yyyy-MMM-dd " 
          PickerCssClass="picker"
          ClientScriptLocation="~/scripts"
          runat="server" /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td>
      <asp:HyperLink runat="Server" ID="lnkCalButton" CssClass="calendar_button" ImageUrl="~/assets/common/calendar/btn_calendar.gif" ToolTip="Calendar Button">
      </asp:HyperLink>
      </td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1" 
      AllowMonthSelection="true"
      AllowMultipleSelection="false"
      AllowWeekSelection="true"
      CalendarCssClass="calendar" 
      CalendarTitleCssClass="title" 
      ClientSideOnSelectionChanged="onCalendarChange" 
      ControlType="Calendar"
      DayCssClass="day" 
      DayHeaderCssClass="dayheader" 
      DayHoverCssClass="dayhover" 
      DayNameFormat="FirstTwoLetters"
      ImagesBaseUrl="~/assets/common/calendar/"
      MonthCssClass="month"
      NextImageUrl="cal_nextMonth.gif"
      NextPrevCssClass="nextprev" 
      OtherMonthDayCssClass="othermonthday" 
      PopUp="Custom"
      PopUpExpandControlId="lnkCalButton"
      PrevImageUrl="cal_prevMonth.gif" 
      SelectedDayCssClass="selectedday" 
      SelectMonthCssClass="selector"
      SelectMonthText="&curren;" 
      SelectWeekCssClass="selector"
      SelectWeekText="&raquo;" 
      SwapDuration="300"
      SwapSlide="Linear" 
      Width="100"
      ReactOnSameSelection="true"
    />