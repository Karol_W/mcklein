<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountAddresses.ascx.cs" Inherits="netpoint.common.controls.AccountAddresses" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:gridview id="AddressListGrid" runat="server" AutoGenerateColumns="False" CellPadding="5" CellSpacing="0" Width="100%" OnRowDataBound="AddressListGrid_RowDataBound" OnRowCommand="AddressListGrid_RowCommand">
	<RowStyle CssClass="npbody" />
    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
	<Columns>
	    <asp:TemplateField>
	        <HeaderStyle Width="15%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton ID="btnDelete" runat="server" ImageURL="~/assets/common/buttons/remove.gif" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AddressID") %>'>
				</asp:ImageButton>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField SortExpression="AddressHTML" HeaderText="colAddress|Address">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "AddressHTML") %>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
		    <HeaderStyle Width="15%"></HeaderStyle>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<NP:MapLink runat="Server" ID="npMapLink" Target="_blank" ImageUrl="~/assets/common/icons/map.png" Visible="false" ToolTip="Preview Address"></NP:MapLink>
			</ItemTemplate>
		</asp:TemplateField>
        <asp:TemplateField>            
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <HeaderStyle Width="15%"></HeaderStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/buttons/edit.gif"
                    CommandName="editthis" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AddressID") %>'>
                </asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
	</Columns>
</asp:gridview>
<asp:Literal ID="hdnDeleteText" runat="server" Text="Deletions are permanent. Continue?" Visible="false"></asp:Literal>
<asp:Literal id="hdnDelete" runat="server" Visible="False" Text="Delete this Address"></asp:Literal>
<asp:Literal id="hdnEdit" runat="server" Visible="False" Text="Edit this Address"></asp:Literal>
<asp:Literal id="hdnMapLink" runat="server" Visible="False" Text="Show this Address on the Map"></asp:Literal>
<asp:Literal id="hdnShipping" runat="server" Text="Delivery Addresses" Visible="False"></asp:Literal>
<asp:Literal id="hdnBilling" runat="server" Text="Billing Addresses" Visible="False"></asp:Literal>
<asp:Literal id="hdnNoShippingAddresses" runat="server" Text="No Delivery Addresses for Account" Visible="False"></asp:Literal>
<asp:Literal id="hdnNoBillingAddresses" runat="server" Text="No Billing Addresses for Account" Visible="False"></asp:Literal>