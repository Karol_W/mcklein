<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.common.controls.OrderListsSaveBlock" Codebehind="OrderListsSaveBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<div id="divUpdate" runat="server">
    <table>
        <tr>
            <td class="npbody" style="height: 28px">
                <asp:Literal id="ltlUpdate" runat="server" Text="Add to list:"></asp:Literal>
                <asp:DropDownList Runat="server" ID="ListsDropDown" EnableViewState="True" /></td>
            <td class="npbody" style="height: 28px">
                <asp:ImageButton Runat="server" ID="SaveToList" ImageUrl="~/assets/common/buttons/update.gif" OnClick="SaveTo" />&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click" Text="New"></asp:LinkButton></td>
        </tr>
    </table>
</div>
<div id="divNew" runat="server" visible="false">
    <div class="npwarning"><asp:Literal ID="ltlTooLong" runat="server" Text="Please limit the list name to less than 45 characters" Visible="false"></asp:Literal></div>
    <table>
        <tr>
            <td class="npbody">
                <asp:Literal Runat="server" ID="ltlCreateNewListNamed" Text="Create new list named:" />
                <asp:TextBox Runat="server" ID="NewListName" EnableViewState="True" MaxLength="45" />
                <asp:Image id="imgWarning" ImageUrl="~/assets/common/icons/warning.gif" runat="server" Visible="False"></asp:Image></td>
            <td class="npbody">
                <asp:ImageButton Runat="server" ID="SaveNewList" ImageUrl="~/assets/common/buttons/add.gif" OnClick="SaveNew" />&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkUpdate" runat="server" OnClick="lnkUpdate_Click" Text="Update"></asp:LinkButton></td>
        </tr>
    </table>
</div>
<asp:Literal ID="ltlNewText" runat="server" Text="Create New List" Visible="false"></asp:Literal>
<asp:Literal ID="ltlUpdateText" runat="server" Text="Update Existing List" Visible="false"></asp:Literal>
&nbsp;&nbsp;

