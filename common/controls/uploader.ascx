<%@ Control language="c#" Inherits="netpoint.common.controls.uploader" Codebehind="uploader.ascx.cs" %>
<table border="0">
	<tr>
		<td><input class="formfield" id="FindFile" style="WIDTH: 274px; HEIGHT: 22px" type="file" size="26" runat="server" /></td>
	</tr>
	<tr>
		<td><asp:listbox id="FilesListBox" runat="server" CssClass="formfield" Height="100px" Width="274px" Font-Size="XX-Small"></asp:listbox></td>
	</tr>
	<tr>
		<td>
			<asp:button id="AddFile" runat="server" CssClass="formbutton" Height="23px" Width="72px" Text="Add" onclick="AddFile_Click"></asp:button>
			<asp:button id="RemvFile" runat="server" CssClass="formbutton" Height="23px" Width="72px" Text="Remove" onclick="RemvFile_Click"></asp:button>
			<input class="formbutton" id="Upload" style="WIDTH: 71px; HEIGHT: 24px;" type="submit" value="Upload" runat="server"  onserverclick="Upload_ServerClick" /></td>
	</tr>
	<tr>
		<td class="tableContent">
			<asp:label id="MessageLabel" runat="server" Height="25px" Width="249px"></asp:label>
			<asp:TextBox ID="uploadpath" Runat="server" Visible="False"></asp:TextBox></td>
	</tr>
</table>
<asp:Literal id="errNoFile" runat="server" Text="Error - a file name must be specified." Visible="False"></asp:Literal>
<asp:Literal id="errSaving" runat="server" Text="Error saving file" Visible="False"></asp:Literal>
<asp:Literal id="hdnFilesUploaded" runat="server" Text="file(s) were uploaded:" Visible="False"></asp:Literal>
