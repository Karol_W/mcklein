<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.common.controls.UserResourceBlock" Codebehind="UserResourceBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<table width="100%" border="0">
	<tr>
		<td class="npbody"></td>
		<td class="npbody">
			<asp:Label id="sysError" runat="server" ForeColor="Red"></asp:Label></td>
		<td class="nplabel"><asp:label id="visiblelabel" text="Visible" Runat="server"></asp:label></td>
		<td class="npbody"><asp:checkbox id="syscommunityvisible" Runat="server"></asp:checkbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysc1label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:dropdownlist id="ddlc1" Runat="server" DataValueField="CodeValue" DataTextField="CodeName"></asp:dropdownlist></td>
		<td class="nplabel"><asp:label id="sysc2label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:dropdownlist id="ddlc2" Runat="server" DataValueField="CodeValue" DataTextField="CodeName"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysd1label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd1" Runat="server"></asp:textbox></td>
		<td class="nplabel"><asp:label id="sysd2label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd2" Runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysd3label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd3" Runat="server"></asp:textbox></td>
		<td class="nplabel"><asp:label id="sysd4label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd4" Runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysd5label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd5" Runat="server"></asp:textbox></td>
		<td class="nplabel"><asp:label id="sysd6label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd6" Runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysd7label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd7" Runat="server"></asp:textbox></td>
		<td class="nplabel"><asp:label id="sysd8label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd8" Runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysd9label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtd9" Runat="server"></asp:textbox></td>
		<td class="nplabel"><asp:label id="sysu1label" Runat="server"></asp:label></td>
		<td class="npbody"><asp:textbox id="txtu1" Runat="server"></asp:textbox></td>
	</tr>

	<tr>
		<td class="nplabel"><asp:label id="syst1label" Runat="server"></asp:label></td>
		<td colspan="3" class="npbody"><asp:textbox id="txtt1" Runat="server" Columns="40" Rows="5" TextMode="MultiLine"></asp:textbox></td>
	</tr>
	<tr>
		<td class="nplabel"><asp:label id="sysp1label" Runat="server"></asp:label></td>
		<td colspan="3" class="npbody"><asp:textbox id="txtp1" Runat="server"></asp:textbox></td>
	</tr>
	<tr>
		<td align="right" colspan="4" class="npbody"><asp:textbox id="CurrentResourceID" Runat="server" Visible="False"></asp:textbox><asp:imagebutton id="SaveButton" onclick="UpdateData" Runat="server" ImageUrl="~/assets/common/buttons/update.gif"></asp:imagebutton></td>
	</tr>
</table>
<asp:Literal id="hdnIsRequired" runat="server" Visible="False" Text="is required"></asp:Literal>
