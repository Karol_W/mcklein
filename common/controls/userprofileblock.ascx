<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.common.controls.UserProfileBlock" Codebehind="UserProfileBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Label ID="sysMessage" runat="server" EnableViewState="false" CssClass="npwarning"></asp:Label><br />
<table class="nptable">
	<tr>
		<th class="nplabel"><asp:literal ID="ltlTitle" Runat="server" Text="Title"></asp:literal></th>
	</tr>
	
	<tr>
		<th class="npbody">
			<asp:TextBox id="txtTitle" runat="server" class="form-control" MaxLength="10"></asp:TextBox></th>
	</tr>

	<tr>
		<th class="nplabel"><asp:literal ID="ltlFirstName" Runat="server" Text="First Name"></asp:literal></th>
	</tr>

    <tr>
		<th class="npbody">
			<asp:TextBox id="txtFirstName" runat="server" class="form-control"></asp:TextBox></th>
	</tr>

    <tr>
			<th class="nplabel"><asp:RequiredFieldValidator id="rfvFirstName" runat="server" ErrorMessage="*" ControlToValidate="txtFirstName">
				<img id="imgFirstName" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></th>
	</tr>

	<tr>
		<th class="nplabel"><asp:literal ID="ltlMiddleName" Runat="server" Text="Middle Name"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody">
			<asp:TextBox id="txtMiddleName" runat="server" class="form-control"></asp:TextBox></th>
	</tr>
	<tr>
		<th class="nplabel"><asp:literal ID="ltlLastName" Runat="server" Text="Last Name"></asp:literal></th>
	</tr>

	<tr>
		<th class="npbody">
			<asp:TextBox id="txtLastName" runat="server" class="form-control"></asp:TextBox>
			<asp:RequiredFieldValidator id="rrfvLastName" runat="server" ErrorMessage="*" ControlToValidate="txtLastName">
				<img id="Img1" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator></th>
	</tr>
	<tr>
		<th class="nplabel"><asp:literal ID="ltlSuffix" Runat="server" Text="Suffix"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody">
			<asp:TextBox id="txtSuffix" runat="server" class="form-control"></asp:TextBox></th>
	</tr>
	<tr>
		<th class="nplabel"><asp:literal ID="ltlEmail" Runat="server" Text="Email"></asp:literal></td>
	</tr>
	<tr>
	<th class="npbody"><asp:TextBox id="Email" runat="server" class="form-control" Columns="30" MaxLength="50"></asp:TextBox>
			<asp:RequiredFieldValidator id="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="Email">
				<img id="Img2" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator>
		    <asp:RegularExpressionValidator ID="regExEmail" runat="server" ErrorMessage="Valid email address required" CssClass="npwarning"
		        ControlToValidate="Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
		        <asp:Literal ID="ltlEmailErr" runat="server" Text="Valid email address required"></asp:Literal></asp:RegularExpressionValidator></th>
	</tr>

	<tr>
		<th class="nplabel"><asp:literal ID="ltlDayPhone" Runat="server" Text="Cell Phone"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody"><asp:TextBox id="txtDayPhone" runat="server" class="form-control"></asp:TextBox></th>
	</tr>
	<tr>
		<th class="nplabel"><asp:literal ID="ltlEveningPhone" runat="server" Text="Evening Phone"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody"><asp:TextBox id="txtEveningPhone" runat="server" class="form-control"></asp:TextBox></th>
	</tr>
	<tr>
		<th class="nplabel"><asp:literal ID="ltlMemberSince" Runat="server" Text="Member Since"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody"><asp:Label id="AddDate" runat="server" /></th>
	</tr>
	
	<tr>
		<th class="nplabel"><asp:literal Visible="false" ID="ltlProfile" Runat="server" Text="Profile"></asp:literal></th>
	</tr>

	<tr>
		<th class="npbody"><asp:TextBox  Visible="false" id="ProfileNotes" runat="server" Columns="30" TextMode="MultiLine" Rows="5"></asp:TextBox></th>
	</tr>
	<tr>
		<td class="nplabel"><asp:literal Visible="false" ID="ltlMemberPoints" Runat="server" Text="Member Points"></asp:literal></td>
		<td class="npbody"><asp:Label Visible="false" id="Points" runat="server" /></td>
	</tr>
    <tr>
		<th class="nplabel"><asp:literal ID="ltlLoginName" runat="server" Text="Login Name"></asp:literal></th>
	</tr>
	<tr>
		<th class="npbody"><asp:TextBox id="txtLoginName" runat="server" class="form-control"></asp:TextBox>
		<asp:RequiredFieldValidator id="rrfvLoginName" runat="server" ErrorMessage="*" ControlToValidate="txtLoginName">
				<img id="Img3" runat="server" src="~/assets/common/icons/warning.gif" alt="" /></asp:RequiredFieldValidator>
		</th>
	</tr>
	<tr>
		<td class="nplabel"></td>
		<!--td>Profile Picture</td-->
		<td class="npbody"><asp:Image id="ProfilePicture" runat="server" Width="80" Height="80" Visible="False" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><asp:ImageButton id="UpdateButton" ImageUrl="~/assets/common/buttons/update.gif" Runat="server" OnClick="UpdateData" /></td>
	</tr>	
</table>
<asp:Literal ID="ltlSaveMessage" runat="server" Visible="false" Text="Your profile was saved." />
<asp:Literal ID="errTitleExceedsMaximumLength" Visible="False" runat="server" Text="User's Title exceeds maximum length."></asp:Literal>
<asp:Literal ID="hdnLoginNameMustBeUnique" Visible="False" runat="server" Text="This login name is already in use.  Please try another."></asp:Literal>
