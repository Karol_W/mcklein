<%@ Page MasterPageFile="~/masters/default.master" Language="C#" AutoEventWireup="true" CodeBehind="SearchSetListPanel.aspx.cs" Inherits="netpoint.common.controls.SearchSetListPanel" %>

<asp:Content runat="server" ID="main" ContentPlaceHolderID="mainslot">
<br />
<asp:Label runat="server" ID="lblSaveAs" Text="Save Current Set As:"></asp:Label>
<asp:TextBox runat="server" ID="tbSaveName" Columns="10"></asp:TextBox>
<asp:ImageButton runat="server" ID="btnSave" ImageUrl="~/assets/common/icons/save.gif"
    OnClick="btnSave_Click" ToolTip="Save" />
    <br />
    <asp:Label runat="server" ID="lblRemoveSet" Text="Remove Set:"></asp:Label>
<asp:DropDownList runat="server" ID="ddlRemoveSet"></asp:DropDownList>
<asp:ImageButton runat="server" ID="btnRemove" ImageUrl="~/assets/common/icons/remove.gif"
    OnClick="btnRemove_Click" ToolTip="Remove" />
    <br /><br /><center>
    <asp:Button runat="Server" ID="btnClose" Text="Finished" OnClientClick="window.close();" />
    </center>
</asp:Content>
