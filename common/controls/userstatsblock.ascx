<%@ Control Language="c#" Inherits="netpoint.common.controls.UserStatsBlock" Codebehind="UserStatsBlock.ascx.cs" %>
<table>
	<tr>
		<td>Email</td>
		<td><asp:TextBox id="Email" runat="server" Columns="30" MaxLength="50"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Password</td>
		<td><asp:TextBox id="Password" runat="server" Columns="16" MaxLength="50" TextMode="Password"></asp:TextBox></td>
	</tr>
</table>
