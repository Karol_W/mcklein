<%@ Control Language="c#" Inherits="netpoint.common.controls.UserSubscriptionsBlock"
    Codebehind="UserSubscriptionsBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="SubscriptionBlock" Src="~/plugins/SubscriptionBlock.ascx" %>
<%@ Import Namespace="netpoint.api" %>
<table id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0" class="npbody">
    <tr>
        <td>
            <asp:GridView ID="UserSubscriptionsGrid" runat="server" Width="100%" ForeColor="Black"
                BackColor="White" AutoGenerateColumns="False" CellPadding="3" GridLines="None" EmptyDataText="No Subscriptions Found"
                CellSpacing="1" DataKeyNames="SubscriberID" OnRowDataBound="grid_RowDataBound">
                <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                <RowStyle CssClass="npbody" />
                <HeaderStyle CssClass="npsubheader"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkSelect" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="colListName|List Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Literal ID="sysListName" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Email" HeaderText="colEmail|Email" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField DataField="SubscribeDate" HeaderText="colAdded|Added" DataFormatString="{0:yyyy-MM-dd}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle Width="20%"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td><asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/assets/common/buttons/remove.gif" OnClick="btnRemove_Click" ToolTip="Remove" /><br /><br /></td>
    </tr>
    <tr>
        <td align="center"><np:SubscriptionBlock ID="ssb" runat="server"></np:SubscriptionBlock></td>
    </tr>
</table>
<asp:Literal runat="server" ID="hdnRemoveMessage" Text="This will remove you from all selected lists.\n Are you sure?" Visible="False" />
