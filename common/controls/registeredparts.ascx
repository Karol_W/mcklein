<%@ Control Language="c#" Inherits="netpoint.common.controls.RegisteredParts" Codebehind="RegisteredParts.ascx.cs" %>
<table class="npbody" id="table1" cellspacing="0" cellpadding="1" width="100%" border="0">
	<tbody>
		<tr>
			<td class="npsubheader"><asp:literal id="ltlHeader" runat="server" Text="Items Registered For"></asp:literal></td>
			<td class="npsubheader" align="right"><asp:hyperlink id="lnkRegister" runat="server" NavigateUrl="~/common/accounts/registerpart.aspx">Register Item</asp:hyperlink></td>
		</tr>
		<tr>
			<td colspan="2">
			    <asp:GridView 
			        ID="gvParts" 
			        runat="server" 
			        CellPadding="1" 
			        AllowPaging="True"
                    Width="100%" 
                    AutoGenerateColumns="False" 
                    PageSize="20" 
                    EmptyDataText="No Records Found" 
                    OnRowCommand="gvParts_RowCommand" 
                    OnRowDataBound="gvParts_RowDataBound" 
                    OnPageIndexChanging="gvParts_PageIndexChanging"
                    >
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />  
                    <Columns>
                        <asp:TemplateField Visible="false">
				            <ItemTemplate>
				                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/assets/common/icons/remove.gif" commandname="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UAPID") %>' />
				            </ItemTemplate>
				        </asp:TemplateField>
					    <asp:BoundField DataField="UserID" HeaderText="colUser|User"></asp:BoundField>
					    <asp:TemplateField HeaderText="colPart|Item">
						    <ItemTemplate>
							    <%# DataBinder.Eval(Container.DataItem, "PartNo") %>
							    &nbsp;-&nbsp;<%# DataBinder.Eval(Container.DataItem, "PartName") %>
						    </ItemTemplate>
					    </asp:TemplateField>
					    <asp:BoundField DataField="DatePurchased" HeaderText="colPurchaseDate|Purchase Date" HtmlEncode="false" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
					    <asp:BoundField DataField="DateRegistered" HeaderText="colRegisterDate|Register Date" HtmlEncode="false" DataFormatString="{0:yyyy-MMM-dd}"></asp:BoundField>
					    <asp:TemplateField HeaderText="colSupportContract|Support Contract">
						    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
						    <ItemStyle HorizontalAlign="Center"></ItemStyle>
						    <ItemTemplate>
							    <asp:ImageButton id="btnSupport" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SupportContractID") %>' CommandName="contract" ImageUrl="~/assets/common/icons/detail.gif">
							    </asp:ImageButton>
						    </ItemTemplate>
					    </asp:TemplateField>
				    </Columns>
				</asp:GridView>
				<asp:Literal ID="ltlDeleteText" runat="server" Text="This will delete this record permanently. Continue?" Visible="false"></asp:Literal>
			</td>
		</tr>
	</tbody>
</table>
