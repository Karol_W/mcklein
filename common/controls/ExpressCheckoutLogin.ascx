﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExpressCheckoutLogin.ascx.cs" Inherits="netpoint.common.controls.ExpressCheckoutLogin" %>
<asp:Panel ID="pnlExpressCheckout" runat="server">

    <asp:Literal ID="expressMessage" runat="server" Text="Checkout as a guest, without creating an account." />
    <asp:Literal ID="ltlEmail" runat="server" Text="Email Address: " />
    <asp:TextBox ID="txtEmail" runat="server" Width="300" MaxLength="255"></asp:TextBox>
    <asp:Image ID="imgEmailWarning" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif"></asp:Image>
    <asp:Panel ID="pnlError" runat="server" Visible="false" ForeColor="DarkRed" Font-Bold="true">
        <asp:Literal ID="hdnEmailRequired" runat="server" Text="Invalid Email Address."></asp:Literal>
    </asp:Panel>
    <asp:ImageButton ID="btnAutoCreateAccount" runat="server" ToolTip="Express Checkout" OnCommand="ExpressCheckout_Click" />
    <script type="text/javascript">
        function checkEnter(e, btn) {
            var characterCode 

            if (e && e.which) { 
                e = e
                characterCode = e.which 
            }
            else {
                e = event
                characterCode = e.keyCode 
            }

            if (characterCode == 13) { 
                __doPostBack(btn, '')
                return false
            }
            else {
                return true
            }
        }
    </script>
</asp:Panel>
