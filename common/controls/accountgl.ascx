<%@ Control Language="c#" Inherits="netpoint.common.controls.AccountGL" Codebehind="AccountGL.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:GridView ID="grid" runat="server" Width="100%" BorderColor="Black" BorderWidth="1px"
    PageSize="25" AutoGenerateColumns="False" AllowSorting="False" OnPageIndexChanging="grid_PageIndexChanging"
    OnRowDataBound="grid_RowDataBound" EmptyDataText="No Records Found">
    <RowStyle CssClass="npbody" />
    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="colPostDate|Post Date">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlPostDate" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDocumentType|Document Type">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlDocumentType" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colDebit|Debit">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <np:PriceDisplay ID="prcDebit" runat="server" Price='<%# Eval("Debit") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colCredit|Credit">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <np:PriceDisplay ID="prcCredit" runat="server" Price='<%# Eval("Credit") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="TopAndBottom" Mode="Numeric" />
    <PagerStyle HorizontalAlign="Right"></PagerStyle>
</asp:GridView>
<asp:Literal runat="server" ID="hdnInvoice" Text="Invoice" Visible="False" />
<asp:Literal runat="server" ID="hdnPayment" Text="Payment" Visible="False" />
<asp:Literal runat="server" ID="hdnCredit" Text="Credit" Visible="False" />
