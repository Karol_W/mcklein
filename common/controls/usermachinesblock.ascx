<%@ Control Language="c#" Inherits="netpoint.common.controls.UserMachinesBlock" Codebehind="UserMachinesBlock.ascx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="NPDatePicker.ascx" %>
<%@ Register TagPrefix="np" TagName="MachinesBlock" Src="../../catalog/controls/machinesblock.ascx" %>
<asp:panel id="pnlDetail" runat="server" Width="100%" Visible="False">
	<table cellspacing="0" cellpadding="1" width="100%" border="0">
		<tr>
			<td class="npsubheader" align="right" colspan="2"><asp:ImageButton id="btnBack" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Back" /></td>
		</tr>
		<tr>
			<td class="nplabel" style="width:20%;"><asp:literal id="colEquipmentName" Runat="server" Text="Equipment Name"></asp:literal></td>
			<td class="npbody"><asp:TextBox id="txtMachineName" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="nplabel"><asp:literal id="colEquipment" Runat="server" Text="Equipment"></asp:literal></td>
			<td class="npbody" style="white-space:nowrap;">
				<np:MachinesBlock id="machine" runat="server" ShowCatalog="false"></np:MachinesBlock>
				<asp:ImageButton id="imgError" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:ImageButton>
				<asp:Label id="lblMachineRequired" Visible="False" runat="server" CssClass="npwarning">You must select equipment</asp:Label></td>
		</tr>
		<tr>
			<td class="nplabel"><asp:literal id="colPurchaseDate" Runat="server" Text="Purchase Date"></asp:literal></td>
			<td class="npbody"><np:DatePicker id="calPurchaseDate" runat="server"></np:DatePicker></td>
		</tr>
		<tr>
			<td class="nplabel"><asp:literal id="colNotes" Runat="server" Text="Notes"></asp:literal></td>
			<td class="npbody">
				<asp:TextBox id="txtNotes" Width="350px" runat="server" TextMode="MultiLine" Height="75px" Columns="40"></asp:TextBox>&nbsp;
				<asp:ImageButton id="btnSave" runat="server" ImageUrl="~/assets/common/buttons/saveinfo.gif" ToolTip="Save"></asp:ImageButton></td>
		</tr>
	</table>
</asp:panel>
<br />
<asp:GridView ID="grid" runat="server" CssClass="nptable" AutoGenerateColumns="false" 
    DataKeyNames="UMID"
    OnRowDataBound="grid_RowDataBound" OnRowDeleting="grid_RowDeleting" OnRowEditing="grid_RowEditing">
    <RowStyle CssClass="npbody"></RowStyle>
	<HeaderStyle CssClass="npsubheader"></HeaderStyle>
	<Columns>
		<asp:BoundField DataField="MachineName" HeaderText="colName|Name"></asp:BoundField>
		<asp:BoundField DataField="Make" HeaderText="colMake|Make"></asp:BoundField>
		<asp:BoundField DataField="Model" HeaderText="colModel|Model"></asp:BoundField>
		<asp:BoundField DataField="MachineDates" HeaderText="colDates|Dates"></asp:BoundField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnEdit" runat="server" CommandName="edit" ImageUrl="~/assets/common/buttons/edit.gif" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField>
			<ItemStyle HorizontalAlign="Center"></ItemStyle>
			<ItemTemplate>
				<asp:ImageButton id="btnDelete" runat="server" CommandName="delete" ImageUrl="~/assets/common/buttons/remove.gif" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>
<p><asp:imagebutton id="btnAdd" runat="server" ImageUrl="~/assets/common/buttons/addinfo.gif" ToolTip="Add Equipment." /></p>
