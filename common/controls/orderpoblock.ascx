<%@ Control Language="c#" Inherits="netpoint.common.controls.OrderPOBlock" Codebehind="OrderPOBlock.ascx.cs" %>
<table>
	<tr>
		<td><asp:literal ID="ltlEmail" Runat="server" Text="Email"></asp:literal></td>
		<td><asp:TextBox id="Email" runat="server" Columns="30" MaxLength="50"></asp:TextBox></td>
	</tr>
	<tr>
		<td><asp:literal ID="ltlPassword" Runat="server" Text="Password"></asp:literal></td>
		<td><asp:TextBox id="Password" runat="server" Columns="16" MaxLength="50" TextMode="Password"></asp:TextBox></td>
	</tr>
</table>
