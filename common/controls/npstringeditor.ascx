<%@ Control Language="c#" Inherits="netpoint.common.controls.NPStringEditor" Codebehind="NPStringEditor.ascx.cs" %>
<style type="text/css">
.box { 
    BORDER-RIGHT: #000000 1px solid; 
    PADDING-RIGHT: 2px; 
    BORDER-TOP: #000000 1px solid; 
    PADDING-LEFT: 2px; 
    PADDING-BOTTOM: 2px; 
    BORDER-LEFT: #000000 1px solid; 
    COLOR: #ffffff; 
    PADDING-TOP: 2px; 
    BORDER-BOTTOM: #000000 1px solid; 
    BACKGROUND-COLOR: #333333 
}
.innerbox { 
    BORDER-RIGHT: #000000 0px solid; 
    PADDING-RIGHT: 0px; 
    BORDER-TOP: #000000 0px solid; 
    PADDING-LEFT: 0px; 
    PADDING-BOTTOM: 0px; 
    BORDER-LEFT: #000000 0px solid; 
    COLOR: #ffffff; PADDING-TOP: 0px; 
    BORDER-BOTTOM: #000000 0px solid; 
    BACKGROUND-COLOR: #555555 
}
</style>
<div class="box" id="boxOuter" style="LEFT: 600px; WIDTH: 400px; POSITION: absolute; TOP: 300px">
    <div id="innerWrapper" style="WIDTH: 400px; POSITION: relative;">
	    <div class="innerbox" id="boxHeader" style="WIDTH: 400px; POSITION: relative;" onmousedown="dragStart(event,'boxOuter','innerWrapper');" >
	    Translation<br /><br />
	    </div>
	<table class="npadminbody" width="100%" cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td class="npadminheader"><asp:label id="lblFormLabels" runat="server">Form Strings</asp:label></td>
			<td class="npadminheader" align="right">
                <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                    OnClick="btnCancel_Click" />
                &nbsp;
			    <asp:imagebutton id="btnSave" ImageUrl="~/assets/common/icons/save.gif" Runat="server" OnClick="btnSave_Click" ValidationGroup="npstringeditor" ></asp:imagebutton>
			 </td>
		</tr>
		<tr>
			<td class="npadminsubheader" colspan="2"><asp:literal id="ltlPage" Runat="server"></asp:literal></td>
		</tr>
		<tr>
			<td><asp:dropdownlist id="ddlDisplayEncoding" runat="server" AutoPostBack="True" onselectedindexchanged="ddlDisplayEncoding_SelectedIndexChanged"></asp:dropdownlist></td>
			<td align="right"><asp:literal id="ltlThemeText" Runat="server"></asp:literal></td>
		</tr>
    </table>
	<asp:GridView ID="gridView" runat="server" ShowHeader="false" CssClass="npadmintable" AutoGenerateColumns="false"
	    OnRowDataBound="gridView_RowDataBound">
	    <RowStyle CssClass="npadminbody"></RowStyle>
	    <Columns>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:Literal id="ltlBaseText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BaseText") %>'>
					</asp:Literal>
					<asp:Literal ID="ltlObjectType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ObjectType") %>' Visible="false" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemStyle HorizontalAlign="Center"></ItemStyle>
				<ItemTemplate>
					<asp:ImageButton id="btnControl" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImagePath") %>' tooltip='<%# DataBinder.Eval(Container.DataItem, "ToolTip") %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NameSpace") %>'>
					</asp:ImageButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:TextBox id="txtEncodedText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EncodedText") %>'>
					</asp:TextBox>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
	</div>
</div>
