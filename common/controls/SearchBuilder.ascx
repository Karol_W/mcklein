<%@ Control Language="C#" AutoEventWireup="true" Codebehind="SearchBuilder.ascx.cs" Inherits="netpoint.common.controls.SearchBuilder" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Panel runat="server" ID="pnlAddNew">
    <table class="npadmintable">
        <tr>
            <td style="width:175">
                <asp:DropDownList runat="server" ID="ddlFields" OnSelectedIndexChanged="ddlFields_SelectedIndexChanged" AutoPostBack="true" />
            </td>
            <td style="width:200">
                <asp:DropDownList runat="server" ID="ddlChoices" OnSelectedIndexChanged="ddlFields_SelectedIndexChanged" AutoPostBack="true" />
            </td>
            <td style="width:150">
                <asp:TextBox runat="server" ID="tbValue1" Columns="10" Visible="False"></asp:TextBox>
                <asp:TextBox Visible="false" ID="nbValue1" runat="server" MaxLength="9" Columns="4" />
                <np:DatePicker Visible="false" runat="server" ID="dpValue1" />
                <asp:DropDownList runat="server" ID="ddlBoolValue1" Visible="false">
                    <asp:ListItem Value="Y" Text="True"></asp:ListItem>
                    <asp:ListItem Value="N" Text="False"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlCode1" Visible="false" />
                <asp:Image ID="sysWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="false" />
            </td>
            <td style="width:150">
                <div class="npbutton">
                    <asp:ImageButton ID="btnImageAddFilter" runat="server" ImageUrl="~/assets/common/icons/itemadd.gif"
                        ImageAlign="left" OnClick="btnImageAddFilter_Click" ToolTip="Add Filter" />
                    <asp:LinkButton runat="server" ID="btnAddFilter" Text="Add Filter" OnClick="btnAddFilter_Click"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<asp:GridView runat="server" ID="gridSearchSet" ShowHeader="False" AutoGenerateColumns="False"
    OnRowCommand="gridSearchSet_RowCommand" CssClass="npadminsearchsettable"
    EmptyDataText="No Filters Specified"
    >
    <EmptyDataRowStyle HorizontalAlign="Center" Height="50px" CssClass="npadminempty" />
    <RowStyle CssClass="npadminbody" />
    <AlternatingRowStyle CssClass="npadminbodyalt" />
    <HeaderStyle CssClass="npadminsubheader"></HeaderStyle>
    <Columns>
        <asp:BoundField DataField="field">
            <ItemStyle Width="175px" />
        </asp:BoundField>
        <asp:BoundField DataField="filter">
            <ItemStyle Width="200px" />
        </asp:BoundField>
        <asp:BoundField DataField="value1">
            <ItemStyle Width="150px" />
        </asp:BoundField>
        <asp:TemplateField>
            <ItemStyle Width="150px" />
            <ItemTemplate>
                <div class="npbutton">
                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" CommandName="remove"
                        ImageUrl="~/assets/common/icons/itemremove.gif" ImageAlign="Left" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SearchSetDetailID") %>' />
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="remove"
                        Text="Remove Filter" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SearchSetDetailID") %>'></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:TextBox runat="server" Visible="false" ID="hdnEqualTo" Text="Equal To"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnNotEqualTo" Text="Not Equal To"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnStartsWith" Text="Starts With"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnEndsWith" Text="Ends With"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnContains" Text="Contains"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnGreaterThan" Text="Greater Than"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnGreaterThanEqualTo" Text="Greater Than or Equal To"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnLessThan" Text="Less Than"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnLessThanEqualTo" Text="Less Than or Equal To"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnIS" Text="IS NULL"></asp:TextBox>
<asp:TextBox runat="server" Visible="false" ID="hdnISNOT" Text="IS NOT NULL"></asp:TextBox>
<asp:Literal ID="hdnMustBeNumber" runat="server" Visible="false" Text="must be a number."></asp:Literal>