<%@ Control Language="c#" Inherits="netpoint.common.controls.AddressBlock" Codebehind="AddressBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:panel id="EditPanel" Runat="server">
    <asp:Label id="sysError" runat="server" cssclass="npwarning"></asp:Label>
	<table id="tblAddress" cellspacing="0" cellpadding="1" border="0">
	    <tr id="rowAddressType" runat="server">
			<td class="npadminlabel"><asp:literal id="ltlAddressType" Text="Address Type " runat="server"></asp:literal></td>
			<td class="npbody">
			    <asp:Label id="lblAddressType" runat="server" Text="" />
			</td>
		</tr>	
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlCountry" Text="Country " runat="server"></asp:literal></td>
			<td class="npbody">
				<asp:dropdownlist id="ddlCountry" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCountry_SelectedIndexChanged"></asp:dropdownlist></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlAttention" runat="server" text="Name "></asp:literal></td>
			<td class="npbody">
				<asp:textbox id="txtAttention" runat="server" Columns="25" MaxLength="150"></asp:textbox>
				<asp:Literal id="ltlAttentionExample" Runat="server" Text="i.e. John Doe"></asp:Literal></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlStreet1" Text="Street 1 " runat="server"></asp:literal></td>
			<td class="npbody" style="white-space:nowrap;">
				<asp:textbox id="txtStreet1" runat="server" Columns="45" MaxLength="150"></asp:textbox>
				<asp:image id="imgStreet1Warning" Runat="server" visible="False" imageurl="~/assets/common/icons/warning.gif" /></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlStreet2" Text="Street 2 " runat="server"></asp:literal></td>
			<td class="npbody"><asp:textbox id="txtStreet2" runat="server" Columns="45" MaxLength="150"></asp:textbox></td>
		</tr>
        <tr id="rowBlock" runat="server">
			<td class="npadminlabel"><asp:literal id="ltlBlock" Text="Block " runat="server"></asp:literal></td>
			<td class="npbody"><asp:textbox id="txtBlock" runat="server" Columns="45" MaxLength="150"></asp:textbox></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlCity" Text="City " runat="server"></asp:literal></td>
			<td class="npbody" style="white-space:nowrap;">
				<asp:textbox id="txtCity" runat="server" Columns="25" MaxLength="150"></asp:textbox>
				<asp:image id="imgCityWarning" Runat="server" visible="False" imageurl="~/assets/common/icons/warning.gif" /></td>
		</tr>
		<tr id="rowState" runat="server">
			<td class="npadminlabel"><asp:literal id="ltlState" Text="State " runat="server"></asp:literal></td>
			<td class="npbody"><asp:dropdownlist id="ddlState" runat="server"></asp:dropdownlist></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlZIP" Text="Zip " runat="server"></asp:literal></td>
			<td class="npbody" style="white-space:nowrap;">
				<asp:textbox id="txtPostalCode" runat="server" Columns="12" MaxLength="50"></asp:textbox>
				<asp:image id="imgZipWarning" runat="server" visible="false" imageurl="~/assets/common/icons/warning.gif" /></td>
		</tr>
        <tr id="rowCounty" runat="server">
			<td class="npadminlabel"><asp:literal id="ltlCounty" Text="County " runat="server" ></asp:literal></td>
			<td class="npbody"><asp:textbox id="txtCounty" runat="server" MaxLength="150"></asp:textbox></td>
        </tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlInCareOf" runat="server" text="In Care Of "></asp:literal></td>
			<td class="npbody">
				<asp:textbox id="txtInCareOf" runat="server" Columns="25" MaxLength="150"></asp:textbox>
				<asp:Literal id="Literal1" Runat="server" Text="i.e. Company Name"></asp:Literal></td>
		</tr>
		<tr>
			<td class="npadminlabel"><asp:literal id="ltlAddressName" Text="Address Label " runat="server"></asp:literal></td>
			<td class="npbody" style="white-space:nowrap;">
				<asp:textbox id="txtAddressName" runat="server" Columns="25" MaxLength="50"></asp:textbox>
				<asp:Image id="imgAddressNameWarning" Runat="server" imageurl="~/assets/common/icons/warning.gif" Visible="False" />
				<asp:Literal id="ltlAddressNameExample" Runat="server" Text="i.e. Work, Home"></asp:Literal></td>
		</tr>
		<tr>
			<td class="npbody" colspan="2"><asp:CheckBox id="sysShipDestination" Runat="server" Checked="True" Visible="false"></asp:CheckBox></td>
		</tr>
		<tr>
        	<td class="npbody" align="right" colspan="2">
			    <asp:imagebutton id="btnSave" runat="server" ImageUrl="~/assets/common/buttons/saveinfo.gif" ToolTip="Save" />			    
			</td>
		</tr>
	</table>	
</asp:panel>
<asp:panel id="ViewPanel" Runat="server" Visible="False" CssClass="npbody">
	<asp:Literal id="sysAddressHTML" runat="server"></asp:Literal>
</asp:panel>
<asp:Literal id="errVerifyAddress" runat="server" Text="Please verify that the address entered is correct." Visible="False"></asp:Literal>
<asp:Literal id="hdnShipping" runat="server" Text="Delivery" Visible="False"></asp:Literal>
<asp:Literal id="hdnBilling" runat="server" Text="Billing" Visible="False"></asp:Literal>
<asp:Literal id="hdnMyAddress" runat="server" Text="My Address" Visible="False"></asp:Literal>
<asp:Literal id="sysAddressID" runat="server" Text="0" Visible="False"></asp:Literal>
<asp:Literal id="errShip" runat="server" Text="No countries have been made valid for shipping." Visible="False"></asp:Literal>
<asp:Literal id="errBill" runat="server" Text="No countries have been made valid for billing." Visible="False"></asp:Literal>
