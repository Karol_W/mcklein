<%@ Import Namespace="netpoint.api" %>
<%@ Control Language="c#" Inherits="netpoint.common.controls.UserListBlock" Codebehind="UserListBlock.ascx.cs" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<asp:Label ID="sysError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="Larger"></asp:Label>
<asp:Panel ID="pnlDetail" runat="server" Visible="False">
    <table class="npbody" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlUserID" runat="server" Text="User ID"></asp:Literal></td>
            <td>
                <asp:Literal ID="sysUserID" runat="server"></asp:Literal>
                <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvUser" runat="server" ControlToValidate="txtUserID" ErrorMessage="RequiredFieldValidator">
					<img src='../../assets/common/icons/warning.gif' alt="" /></asp:RequiredFieldValidator></td>
        </tr>        
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlTitle" runat="server" Text="Title"></asp:Literal></td>
            <td><asp:TextBox ID="txtTitle" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlFirstName" runat="server" Text="First Name"></asp:Literal></td>
            <td><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlMiddleName" runat="server" Text="Middle Name"></asp:Literal></td>
            <td><asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlLastName" runat="server" Text="Last Name"></asp:Literal></td>
            <td><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlSuffix" runat="server" Text="Suffix"></asp:Literal></td>
            <td><asp:TextBox ID="txtSuffix" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
            <td><asp:TextBox ID="txtEmail" runat="server" Columns="50"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlApprovalLimit" runat="server" Text="Approval Limit"></asp:Literal></td>
            <td><asp:TextBox ID="nbApprovalLimit" runat="server" Columns="15" MaxLength="15"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regNum" runat="server" ControlToValidate="nbApprovalLimit" 
                    ErrorMessage="Approval Limit must be a number" CssClass="npwarning"  
                    ValidationExpression="(\d*\.{0,1}\d{1,4})|(\d+\.{0,1}\d{0,4})">
                    <asp:Literal ID="errNum" runat="server" Text="Approval Limit must be a number"></asp:Literal>
                    </asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="ltlActive" runat="server" Text="Active"></asp:Literal></td>
            <td><asp:CheckBox ID="sysActive" runat="server"></asp:CheckBox></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="hdnAdded" runat="server" Text="Date Added"></asp:Literal></td>
            <td><asp:Literal ID="sysAdded" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="nplabel" align="right"><asp:Literal ID="hdnLastVisit" runat="server" Text="Last Visit"></asp:Literal></td>
            <td><asp:Literal ID="sysLastVisit" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td align="center" colspan="2"><asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/assets/common/buttons/saveinfo.gif" ToolTip="Save" /></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Panel>
<asp:GridView ID="grid" CssClass="nptable" AutoGenerateColumns="false" runat="server" 
    DataKeyNames="UserID" OnRowDataBound="grid_RowDataBound" OnRowEditing="grid_RowEditing">
    <RowStyle CssClass="npbody"></RowStyle>
    <HeaderStyle CssClass="npsubheader"></HeaderStyle>
    <EmptyDataRowStyle CssClass="npempty" Height="50px" />
    <Columns>
        <asp:TemplateField HeaderText="colUserName|User Name">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "UserID") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colEmail|Email">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "Email") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colAdded|Added">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "AddDate", "{0:d}") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colLastVisit|Last Visit">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "LastVisitDate", "{0:g}") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colActive|Active">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:Image ID="imgActiveFlag" runat="server" Visible="false" ImageUrl="~/assets/common/icons/checked.gif" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/buttons/editinfo.gif" CommandName="edit" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<table class="npbody" cellspacing="0" cellpadding="1" width="100%" border="0">
    <tr>
        <td align="right" style="width:5%;"><asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/assets/common/buttons/createaccount.gif" Visible="False" ToolTip="New" /></td>
    </tr>
</table>
<asp:Literal ID="hdnDelete" runat="server" Text="Delete" Visible="False"></asp:Literal>
<asp:Literal ID="hdnSave" runat="server" Text="Save" Visible="False"></asp:Literal>
<asp:Literal ID="errMustBeNumber" runat="server" Text="Approval limit must be a number" Visible="false"></asp:Literal>
<asp:Literal ID="hdnDuplicateEmailText" runat="server" Text="The e-mail address that you entered is already in use. Please enter a different e-mail address." Visible="False"></asp:Literal>  
<asp:Literal ID="hdnDuplicateUsername" runat="server" Text="Username is already in use. Please enter a different username." Visible="False"></asp:Literal>  
<asp:Literal ID="hdnInvalidUsernameText" runat="server" Text="Username is invalid. Please ensure no special characters are being used." Visible="False"></asp:Literal>  
<asp:Literal ID="hdnUsernameRequired" runat="server" Text="Username is required." Visible="False"></asp:Literal>  
