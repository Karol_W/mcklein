<%@ Control Language="c#" Inherits="netpoint.common.controls.OrderHistoryBlock" Codebehind="OrderHistoryBlock.ascx.cs"
    AutoEventWireup="true" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Register TagPrefix="np" TagName="PartPrice" Src="~/commerce/controls/partprice.ascx" %>    
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Panel ID="pnlGrid" runat="server">
<style>
.dateRangeCell
{
	padding-right:5px;
	white-space:nowrap;
}
</style>
<table width="100%">
    <tr>
        <td class="dateRangeCell"><asp:Literal ID="Literal1" runat="server">Display orders created between</asp:Literal></td>
        <td class="dateRangeCell"><np:DatePicker ID="dtStartDate" runat="server"></np:DatePicker></td>
        <td class="dateRangeCell"><asp:Literal ID="Literal2" runat="server">and</asp:Literal></td>
        <td class="dateRangeCell"><np:DatePicker ID="dtEndDate" runat="server"></np:DatePicker></td>
        <td align="right" width="100%"><asp:ImageButton ID="btnSubmit" Runat="server" ImageUrl="~/assets/common/buttons/submit.gif" ToolTip="Search for orders between the specified date range." OnClick="btnFind_Click" /></td>
    </tr>
</table>
<asp:GridView ID="OrderHistoryListGrid" AllowSorting="False" AllowPaging="True" runat="server"
    Width="100%" AutoGenerateColumns="False" PageSize="25" EmptyDataText="No Orders Found"
    OnPageIndexChanging="OrderHistoryListGrid_PageIndexChanging" OnRowCommand="OrderHistoryListGrid_RowCommand"
    OnRowDataBound="OrderHistoryListGrid_RowDataBound">
    <RowStyle CssClass="npbody" />
    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
    <Columns>
        <asp:BoundField DataField="PurchaseDate" HtmlEncode="false" SortExpression="PurchaseDate DESC"
            HeaderText="colDate|Date" DataFormatString="{0:yyyy-MMM-dd}">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField SortExpression="CartType" HeaderText="colDocumentType|Document Type">
            <ItemTemplate>
                <asp:Literal ID="ltlDocumentType" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField SortExpression="OrderID DESC" HeaderText="colDocumentNumber|Document Number">
            <HeaderStyle Wrap="False"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:LinkButton ID="lnkOrderNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>'
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>' CommandName="detail"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colItems|Items">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField Visible="False" DataField="BillingAddress" SortExpression="BillingAddress"
            HeaderText="colBillingAddress|Billing Address" DataFormatString="&lt;pre&gt;{0}&lt;/pre&gt;"></asp:BoundField>
        <asp:BoundField DataField="ShippingMethodText" SortExpression="ShippingMethodText"
            HeaderText="colShipMethod|Delivery Method">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField Visible="False" DataField="ShippingAddress" SortExpression="ShippingAddress"
            HeaderText="colShippingAddress|Delivery Address" DataFormatString="&lt;pre&gt;{0}&lt;/pre&gt;"></asp:BoundField>
        <asp:TemplateField SortExpression="ShippingAddress" HeaderText="colShippingAddress|Delivery Address">
            <ItemTemplate>
                <asp:Literal ID="ltlShippingAddressName" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField SortExpression="ShippingStatus" HeaderText="colStatus|Status">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <asp:Literal ID="ltlShipStatus" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="UserID" SortExpression="UserID" HeaderText="colUser|User">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField SortExpression="GrandTotal" HeaderText="colTotal|Total">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
            <ItemTemplate>
                <np:PriceDisplay ID="prcGrandTotal" runat="server" PriceObject='<%# Eval("GrandTotal") %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="TopAndBottom" Mode="Numeric" />
    <PagerStyle HorizontalAlign="Right"></PagerStyle>
</asp:GridView>
</asp:Panel>
<asp:Literal ID="ltlNoRecords" runat="server" Text="No Orders Found" Visible="False"></asp:Literal>
<asp:Panel ID="pnlDetail" runat="server">
    <table id="tblDetail" cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td valign="top" colspan="2">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="npheader">
                            <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/assets/common/icons/cancel.gif"
                                OnClick="btnCancel_Click" ToolTip="Cancel" />&nbsp;
                            <asp:Literal ID="sysDocumentType" runat="server"></asp:Literal>
                            <asp:Literal ID="ltlDocumentNumber" runat="server" Text="Number"></asp:Literal>
                            <asp:Literal ID="sysDocNum" runat="server"></asp:Literal></td>
                        <td class="npheader" align="right">
                            <asp:ImageButton ID="btnOrderFromHistory" runat="server" ImageUrl="~/assets/common/buttons/addtocart.gif"
                                ToolTip="Create New Order Based on This Order" CommandName="orderfromhistory"
                                OnClick="btnOrderFromHistory_Click" />&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="npbody" valign="top">
                <asp:Literal ID="ltlPurchaseDate" runat="server" Text="Purchase Date:"></asp:Literal>&nbsp;
                <asp:Literal ID="sysPurchaseDate" runat="server"></asp:Literal><br />
                <asp:Literal ID="ltlRequestedShipDate" runat="server" Text="Requested Ship Date:"></asp:Literal>&nbsp;
                <asp:Literal ID="sysRequestedShipDate" runat="server"></asp:Literal></td>
            <td class="npbody" valign="top" align="right">
                <asp:Literal ID="ltlConfirmationNum" runat="server" Text="Confirmation:"></asp:Literal>&nbsp;
                <asp:Literal ID="sysConfirmationNum" runat="server" Text="Confirmation"></asp:Literal><br />
                <asp:Literal ID="ltlPONumber" runat="server" Visible="False" Text="PO No."></asp:Literal>&nbsp;
                <asp:Literal ID="sysPONum" runat="server"></asp:Literal>&nbsp;</td>
        </tr>
        <tr>
            <td class="npsubheader" valign="top">
                <asp:Literal ID="ltlBill" runat="server" Text="Bill To"></asp:Literal></td>
            <td class="npsubheader" valign="top">
                <asp:Literal ID="ltlShip" runat="server" Text="Deliver To"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npbody" valign="top">
                <asp:Literal ID="sysBillTo" runat="server"></asp:Literal>&nbsp;</td>
            <td class="npbody" valign="top">
                <asp:Literal ID="sysShipTo" runat="server"></asp:Literal>&nbsp;</td>
        </tr>
        <tr>
            <td class="npbody" valign="top" colspan="2">
                <asp:GridView ID="gridDetail" AutoGenerateColumns="False" runat="server" CellPadding="1"
                    Width="100%" EmptyDataText="No Details Found" OnRowDataBound="gridDetail_RowDataBound">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle HorizontalAlign="center" Height="50" VerticalAlign="middle" />
                    <Columns>
                        <asp:TemplateField HeaderText="colQty|Qty">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblQuantity"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="5%" />
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkDownloads" ImageUrl="~/assets/common/icons/downloads.png"
                                    Visible="false"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colItem|Item">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblPartNo"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colDescription|Description">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDescription"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="colPrice|Price" ItemStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="right">
                            <ItemTemplate>
								<np:PartPrice ID="partPrice" runat="server" ShowOriginalPrice="true" ShowOriginalPriceLabel="true" 
									ShowDiscount="true" ShowDiscountLabel="true" ShowFinalPriceLabel="false"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="npbody" valign="top">
                <table cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td class="npbody" style="height: 20px">
                            <asp:Literal ID="ltlVolume" runat="server" Text="Volume"></asp:Literal></td>
                        <td class="npbody" style="height: 20px">
                            <asp:Literal ID="sysVolumeTotal" runat="server" EnableViewState="False"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="npbody">
                            <asp:Literal ID="ltlWeight" runat="server" Text="Weight"></asp:Literal></td>
                        <td class="npbody">
                            <asp:Literal ID="sysWeightTotal" runat="server" EnableViewState="False"></asp:Literal></td>
                    </tr>
                </table>
            </td>
            <td class="npbody" valign="top" align="right">
				<np:OrderExpenses ID="expenses" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="npsubheader" valign="top" colspan="2">
                <asp:Literal ID="ltlPayments" runat="server" Text="Payments"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npbody" valign="top" colspan="2">
                <asp:GridView ID="gridPayments" AutoGenerateColumns="False" runat="server" EnableViewState="true"
                    CellPadding="1" Width="100%" EmptyDataText="No Payments Found" OnRowDataBound="gridPayments_RowDataBound">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle HorizontalAlign="center" Height="50" VerticalAlign="middle" />
                    <Columns>
                        <asp:BoundField DataField="PaymentName" HeaderText="colType|Type"></asp:BoundField>
                        <asp:BoundField DataField="MaskedNumber" HeaderText="Customer|Customer"></asp:BoundField>
                        <asp:BoundField DataField="ExpirationDate" HeaderText="colExpiration|Expiration"></asp:BoundField>
                        <asp:TemplateField HeaderText="colAmount|Amount" ItemStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="right">
                            <ItemTemplate>
                                <np:PriceDisplay ID="prcAmount" runat="server" Price='<%# Eval("PaymentAmount") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="npsubheader" valign="top" colspan="2">
                <asp:Literal ID="ltlShipments" runat="server" Text="Deliveries"></asp:Literal></td>
        </tr>
        <tr>
            <td class="npbody" valign="top" colspan="2">
                <asp:GridView ID="gridShipments" AutoGenerateColumns="False" runat="server" CellPadding="1"
                    Width="100%" EmptyDataText="No Shipments Found">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle HorizontalAlign="center" Height="50" VerticalAlign="middle" />
                    <Columns>
                        <asp:HyperLinkField Target="_blank" DataNavigateUrlFields="ShipmentID" DataNavigateUrlFormatString="~/commerce/shipment.aspx?shipmentid={0}"
                            DataTextField="ShipmentID" HeaderText="colID|ID"></asp:HyperLinkField>
                        <asp:BoundField DataField="ShipMethodText" HeaderText="colShippingVia|Shipping Via"></asp:BoundField>
                        <asp:BoundField DataField="EstShipDate" DataFormatString="{0:yyyy-MMM-dd}" HeaderText="colEstShipDate|Est. Ship Date">
                        </asp:BoundField>
                        <asp:BoundField DataField="ActualShipDate" DataFormatString="{0:yyyy-MMM-dd}" HeaderText="colShipDate|Ship Date">
                        </asp:BoundField>
                        <asp:BoundField DataField="TrackingNum" HeaderText="colTrackingNumber|Tracking Number"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="hdnOrder" runat="server" Visible="False" Text="Order"></asp:Literal>
<asp:Literal ID="hdnInvoice" runat="server" Visible="False" Text="Invoice"></asp:Literal>
<asp:Literal ID="hdnQuote" runat="server" Visible="False" Text="Quote"></asp:Literal>
<asp:Literal ID="hdnPending" runat="server" Text="Pending" visible="false"></asp:Literal>
<asp:Literal ID="hdnShipped" runat="server" Text="Delivered" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPicked" runat="server" Text="Picked" Visible="false"></asp:Literal>
<asp:Literal ID="hdnPartial" runat="server" Text="Partial" Visible="false"></asp:Literal>
<asp:Literal ID="hdnClosed" runat="server" Text="Closed" Visible="false"></asp:Literal>
<asp:Literal ID="hdnCancelled" runat="server" Text="Cancelled" Visible="false"></asp:Literal>
