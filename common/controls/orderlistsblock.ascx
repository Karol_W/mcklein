<%@ Control Language="c#" Inherits="netpoint.common.controls.OrderListsBlock" Codebehind="OrderListsBlock.ascx.cs" AutoEventWireup="true" %>
<%@ Register TagPrefix="np" TagName="PartDescription" src="~/commerce/controls/partdescription.ascx" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<NP:GridView ID="gridSavedLists" 
    Width="100%" 
    runat="server" 
    CellPadding="1" 
    AutoGenerateColumns="False"
    AllowPaging="True" 
    AllowSorting="False" 
    PageSize="20"
    EmptyDataText="No Lists Saved" 
    OnRowCommand="gridSavedLists_RowCommand" 
    OnRowDataBound="gridSavedLists_RowDataBound"
    OnPageIndexChanging="gridSavedLists_PageIndexChanging">
    <RowStyle CssClass="npbody" />
    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDeleteList" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/delete.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="CreateDate" HeaderText="colCreated|Created" DataFormatString="{0:d}"
            SortExpression="CreateDate"></asp:BoundField>
        <asp:TemplateField HeaderText="colListName|List Name" ItemStyle-HorizontalAlign="center" SortExpression="CartName">
            <ItemTemplate>
                <asp:LinkButton ID="lnkListName" runat="server"  CommandName="showdetail" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>'
                    Text='<%# DataBinder.Eval(Container, "DataItem.CartName") %>'>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="QuantityTotal" HeaderText="colQty|Qty" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" SortExpression="QuantityTotal">
        </asp:BoundField>
        <asp:TemplateField HeaderStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="right" HeaderText="colPrice|Price" SortExpression="SubTotal">
            <ItemTemplate>
                <np:PriceDisplay ID="prcSubTotal" runat="Server" Price='<%# DataBinder.Eval(Container.DataItem, "SubTotal") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnEmailList" runat="server" CommandName="email" ImageUrl="~/assets/common/buttons/sendtofriend.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colAdd|">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnAddListToCart" runat="server" CommandName="addtocart" ImageUrl="~/assets/common/icons/addtocart.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
        <PagerSettings Position="TopAndBottom" Mode="Numeric" />
        <PagerStyle HorizontalAlign="Right"></PagerStyle>
</NP:GridView>
<NP:GridView 
    ID="gridDetail" 
    Width="100%" 
    runat="server" 
    CellPadding="1" 
    AutoGenerateColumns="False"
    AllowPaging="False" 
    AllowSorting="False" 
    PageSize="20" 
    EmptyDataText="No Records Found" 
    OnRowCommand="gridDetail_RowCommand" 
    OnRowDataBound="gridDetail_RowDataBound">
    <RowStyle CssClass="npbody" />
    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
    <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnDeleteDetail" runat="server" CommandName="remove" ImageUrl="~/assets/common/icons/remove.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:HyperLink ID="lnkPartImage" runat="server" NavigateUrl="~/catalog/partdetail.aspx?partno="></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="PartNo" DataNavigateUrlFormatString="~/catalog/partdetail.aspx?PartNo={0}"
            DataTextField="PartNo" HeaderText="ItemNo|Item No."></asp:HyperLinkField>
        <asp:templatefield HeaderText="colDescription|Item Description">
			<ItemTemplate>
                <np:PartDescription ID="partDesc" runat="server" ShowPartDescription="false" ShowPartName="true" ShowPartNo="true" />
			</ItemTemplate>
		</asp:templatefield>
        <asp:BoundField DataField="Quantity" ItemStyle-HorizontalAlign="center" HeaderText="colQty|Qty"></asp:BoundField>
        <asp:TemplateField HeaderText="colPrice|Price" HeaderStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
                <np:PriceDisplay ID="prcPrice" runat="server" Price='<%# DataBinder.Eval(Container.DataItem, "PurchasePrice") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="colAdd|">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                <asp:ImageButton ID="btnAddDetailToCart" runat="server" CommandName="addtocart" ImageUrl="~/assets/common/icons/addtocart.gif"
                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.OrderDetailID") %>'></asp:ImageButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerSettings Position="TopAndBottom" Mode="Numeric" />
    <PagerStyle HorizontalAlign="Right"></PagerStyle>
</NP:GridView>
