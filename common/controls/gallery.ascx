<%@ Control Language="c#" Inherits="netpoint.common.controls.gallery" Codebehind="gallery.ascx.cs" %>
<%@ Register TagPrefix="ecn" TagName="uploader" Src="uploader.ascx" %>
<asp:Panel ID="PanelTabs" Visible="true" Runat="server">
	<table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr>
			<td style="width:72px;"><asp:linkbutton id="tabBrowse" onclick="showBrowse" Text="browse" CssClass="tableContent" runat="server"></asp:linkbutton>&nbsp;</td>
			<td style="width:72px;"><asp:linkbutton id="tabUpload" onclick="showUpload" Text="upload" CssClass="tableContent" runat="server"></asp:linkbutton>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width:72px;"><asp:linkbutton id="tabPreview" onclick="showPreview" Text="preview" CssClass="tableContent" runat="server" Visible="False"></asp:linkbutton>&nbsp;</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="PanelBrowse" Visible="true" Runat="server">
	<table cellspacing="0" cellpadding="0"  width="100%" border="1">
		<tr>
			<td align="center">
				<asp:TextBox id="imagepath" Runat="server" Visible="False"></asp:TextBox>
				<asp:TextBox id="imagesize" Runat="server" Visible="False">100</asp:TextBox>
				<asp:DataList id="imagerepeater" runat="server" CellSpacing="0" CellPadding="4" 
				    GridLines="Both" BorderWidth="1" BorderColor="black" 
				    RepeatColumns="3" RepeatLayout="Table">
				    <ItemTemplate>
					    <table class="tableContent" style="text-align:center;">
					        <tr>
					            <td valign="middle" align="center" style="height:<%=thumbnailSize%>;">
					                <asp:ImageButton runat="server" ID="BrowseImage" OnCommand="previewImage" 
					                    CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Path")%>' 
					                    ImageUrl='<%#DataBinder.Eval(Container.DataItem,"Image")%>' /></td>
					        </tr>
					        <tr>
					            <td valign="top" align="center">
					                <asp:LinkButton Runat="server" ID="BrowseImageLink" OnCommand="previewImage" 
					                    CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Path")%>' 
					                        Text='<%#DataBinder.Eval(Container.DataItem,"FileName")%>'></asp:LinkButton>
					                <br /><%#DataBinder.Eval(Container.DataItem,"Size")%> - <%#DataBinder.Eval(Container.DataItem,"Date")%>
					            </td>
					        </tr>
					    </table>
				    </ItemTemplate>
				</asp:DataList>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="PanelPreview" Visible="false" Runat="server">
	<table cellspacing="0" cellpadding="0"  width="100%" border="1">
		<tr>
			<td>
			    <table width="100%" cellpadding="10">
			        <tr>
			            <td colspan="2">
			                <table width="100%">
			                    <tr>
			                        <td style="width:33%;" class="tableContent" align="left">
			                            <asp:HyperLink runat="server" ID="fullsizeLink" Text="full size image" Target="_blank"></asp:HyperLink></td>
			                        <td style="width:33%;" class="tableContent" align="center">
			                            <asp:LinkButton Runat="server" ID="deleteLink" OnCommand="deleteImage" Text="delete image"></asp:LinkButton></td>
			                        <td style="width:33%;" class="tableContent" align="right"><a href="?imagefile=">back</a></td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			        <tr>
			            <td style="width:100px; height:300px;" align="center" valign="top" class="tableContent"><asp:Image ID="ImagePreview" Runat="server" /></td>
			            <td valign="top" class="tableContent">
			                <br />Name: <asp:Label ID="previewName" runat="server" Text="Name"></asp:Label>
			                <br />Size: <asp:Label ID="previewSize" runat="server" Text="Size"></asp:Label>
			                <br />Date: <asp:Label ID="previewDate" runat="server" Text="Date"></asp:Label>
			                <br />Height: <asp:Label ID="previewHeight" runat="server" Text="Height"></asp:Label>
			                <br />Width: <asp:Label ID="previewWidth" runat="server" Text="Width"></asp:Label>
			                <br />Resolution: <asp:Label ID="previewResolution" runat="server" Text="Width"></asp:Label>
			            </td>
			        </tr>
			    </table>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="PanelUpload" Visible="false" Runat="server">
	<table cellspacing="0" cellpadding="0"  width="100%" border="1">
		<tr>
			<td align="center">
			<br />
			<ecn:uploader id="pageuploader" runat="server" uploadDirectory="e:\\http\\dev\\ECNblaster\\assets\\eblaster\\customers\\1\\images\\"></ecn:uploader>
			<br /><br /></td>
		</tr>
	</table>
</asp:Panel>
