<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.pagedetail" Codebehind="pagedetail.aspx.cs" %>
<asp:Content ContentPlaceHolderID="headerslot"  runat="server" ID="header">
    <meta name="keywords" content="<%# Keywords %>" />
    <meta name="description" content="<%# Description %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table width="100%" cellpadding="0" cellspacing="0" class="npbody static-page">
        <tr>
            <td align="left" valign="top"><asp:PlaceHolder ID="sysPageText" runat="Server" /></td>
        </tr>
    </table>

    <style>
        aside {
            display: none;
        }
    </style>

</asp:Content>
<asp:Content ContentPlaceHolderID="menuslot" runat="server" ID="menu">
    <asp:Panel ID="PageAdminPanel" runat="server" Visible="false"><br />&nbsp;
        <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif" NavigateUrl="~/admin/common/pages/pageeditor.aspx?PageID=" ToolTip="Edit"></asp:HyperLink><b>
        <asp:Literal ID="ltlModified" runat="server" Text="Modified"></asp:Literal></b>
        <asp:Label ID="sysDateModifiedLabel" runat="Server"></asp:Label>&nbsp;<b>
        <asp:Literal ID="ltlViews" runat="server" Text="Views"></asp:Literal></b>
        <asp:Label ID="sysViewsLabel" runat="Server"></asp:Label>&nbsp;<b>
        <asp:Literal ID="ltlOwner" runat="server" Text="Owner"></asp:Literal></b>
        <asp:Label ID="sysUserIDLabel" runat="Server"></asp:Label>
    </asp:Panel>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="sysIgnoreLoginRequired" runat="server" Text="False" Visible="false" />
    <asp:Literal ID="errMissingPage" runat="server" Visible="False" Text="No page exists or none is currently active. <br><br>Check page configuration and make sure there is an active, visible, realeased but not expired page with a page code of "></asp:Literal>
</asp:Content>
