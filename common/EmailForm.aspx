﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.EmailForm" CodeBehind="EmailForm.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">


    <table width="100%" cellpadding="0" cellspacing="0" class="npbody email-wrapper">
        <tbody>
            <tr>
                <td align="left" valign="top">
                    <div id="page-wrap">

                        <h1>Warranty Repair Process</h1>
                        <br />
                        <p>Please visit the <a href="/common/pagedetail.aspx?PageCode=Policy">Policy</a> page for detailed warranty information.</p>
                        <br />
                        <p>If the damage resulted from a defect in materials or workmanship, McKlein will, at our expense, repair or replace the item if not repairable. You are responsible for the cost of shipping to our repair facility.</p>
                        <br />
                        <p>We will recommend the best course of action. For example, depending on the problem, it might be easier to DIY or have the work done by an authorized McKlein dealer or shoe repair shop close to home.</p>
                        <br />
                        <p><a href="https://mckleinusa.com/catalog/partlist.aspx?categoryid=190">Parts and accessories</a> are available to purchase. </p>
                        <br />
                        <p>McKlein's average repair time is 2 to 10 business days. Handle, wheels and locks usually take two days. Stitching takes longer.</p>
                        <br />
                        <ul>
                            <li>Step 1: Complete the form with a brief description of the issue. Once Customer Service has reviewed your request, you will receive a Return Authorization number, shipping instructions and a request for additional information if necessary.<br />
                                <br />
                            </li>
                            <li>Step 2: Pack and address Securely repackage your item. Affix the Return Authorization form to the outside of your package. We cannot process returns without a RA reference or accept responsibility for packages we do not ship. Please keep the tracking details for your records.<br />
                                <br />
                                We strongly recommend that you use a reputable insured carrier or recorded delivery service as the item remains your responsibility until received by us at our offices.
                                <br />
                                <br />
                                Shipping and handling charges are not refundable. Packages sent COD will be refused.<br />
                            </li>
                            <li>Step 3: Send Ship your package using a track-able, insured shipping method. McKlein Company Attn: Returns 4447 W. Cortland Street Chicago, IL 60639<br />
                                <br />
                            </li>

                            <li>Step 4: Follow Up For status, call us at 1-773-235-0600 x 230. You may also contact customer service at: <img class="img-responsive" src="../../../assets/common/themes/mcklein/assets/img/customerservice.jpg" alt="customerservice"> </br> Please include Repair Authorization number at time of inquiry.
                                <br />
                                <br />
                            </li>
                        </ul>

                        <!-- This is an example of an asp.net label. Text can be set to whatever you would like the label to display on the page. 
The id will need to start with lbl_ as shown below //-->

                        <p>
                            <strong style="font-size:20px; color:#F65;font-weight: bold !important;">
                                <asp:Label ID="sysSentMessage" runat="server" Visible="false">Thank you! Your message has been sent.</asp:Label></strong>
                        </p>

                        <br />
                        <div id="contact-area">


                            <asp:Panel runat="server" ID="contactForm" CssClass="container-fluid">
                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_ItemNumber" runat="server" Text="Item Number"></asp:Label></label>
                                <asp:TextBox ID="eml_ItemNumber" runat="server" class="form-control"></asp:TextBox>
                                <asp:CustomValidator ID="valemItemNumber" OnServerValidate="ItemNumberValidate"  Display="Dynamic" ControlToValidate="eml_ItemNumber" ErrorMessage="Item number must be a real item number" runat="server" />
                                </div>

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_ItemDes" runat="server" Text="Item Description"></asp:Label></label>
                                <asp:TextBox ID="eml_ItemDes" runat="server" class="form-control"></asp:TextBox>
                                </div>

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_dop" runat="server" Text="Date of Purchase"></asp:Label></label>
                                <asp:TextBox ID="eml_dop" runat="server" class="form-control" type="date"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valemdop" Display="Dynamic" ControlToValidate="eml_dop" ErrorMessage="Date of Purchase is required!" runat="server" />
                                </div>

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_pop" runat="server" Text="Place of Purchase"></asp:Label></label>
                                <asp:TextBox ID="eml_pop" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valempop" Display="Dynamic" ControlToValidate="eml_pop" ErrorMessage="Place of Purchase is required!" runat="server" />
                                </div>

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_location" runat="server" Text="Current State/Country"></asp:Label></label>
                                <asp:TextBox ID="eml_location" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valemlocation" Display="Dynamic" ControlToValidate="eml_location" ErrorMessage="Current State/Country is required!" runat="server" />
                                </div>
                                

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_category2" runat="server" Text="Nature of the issue:"></asp:Label>
                                </label>
                                <asp:DropDownList ID="eml_category2" runat="server"  class="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="Telescoping Handle" Value="D-Telescoping Handle" />
                                    <asp:ListItem Text="Wheels" Value="D-Wheels" />
                                    <asp:ListItem Text="Lock" Value="D-Lock" />
                                    <asp:ListItem Text="Magnet" Value="D-Magnet" />
                                    <asp:ListItem Text="Shoulder Strap Hardware" Value="D-Strap Hardware" />
                                    <asp:ListItem Text="Zipper" Value="D-Zipper" />
                                    <asp:ListItem Text="Leather" Value="D-Leather" />
                                    <asp:ListItem Text="Stitching/Lining" Value="D-Stitching/Lining" />
                                    <asp:ListItem Text="Other" Value="D-Other" />

                                </asp:DropDownList>
                                
                                </div>

                                <div class="form-group">
                                <label style="margin-bottom: 5px;">
                                    <asp:Label ID="lbl_Comments" runat="server" Text="Issue Type"></asp:Label>
                                </label>
                                <asp:TextBox ID="eml_Comments" runat="server" TextMode="Multiline" Columns="50" Rows="3" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valemComments" Display="Dynamic" ControlToValidate="eml_Comments" ErrorMessage="Comments is required!" runat="server" />

                                </div>

                                
                                <asp:Literal ID="sysEmailSubject" runat="server" Visible="false">McKlein Repair Form</asp:Literal>

                                <asp:Literal ID="sysEmailFrom" runat="server" Visible="false">mckleinwebsite@mckleincompany.com</asp:Literal>

                                <asp:Literal ID="sysEmailAddress" runat="server" Visible="false">customerservice@mckleincompany.com</asp:Literal>

                                <asp:Literal ID="sysEmailTemplate" runat="server" Visible="false">general</asp:Literal>
                                <asp:Literal ID="sysEmailBody" runat="server" Visible="false">
                                    <p>{0},</p>

                                    <p><br>
                                    </p>

                                    <p>Thank you for submitting a repair request.</p>

                                    <p>Your Ticket number is: {1}.</p>

                                    <p>We will respond to your request in 1 to 3 business days. </p>

                                    <p><br>
                                    </p>

                                    <p>Please note that our business hours are Monday through Friday from 8AM to 5PM central time.</p>

                                    <p><br>
                                    </p>

                                    <p>Thank you very much,</p>

                                    <p><br>
                                    </p>

                                    <p><span style="font-weight: bold;">Customer Service </span></p>
                                    <p><span style="font-weight: bold;">McKlein Company, L.L.C.</span></p>
                                    <p><span style="font-weight: bold;">4447 W. Cortland St. Chicago, IL 60639</span></p>
                                    <p><span style="font-weight: bold;">Phone:773:235:0600 ext 230</span></p>
                                </asp:Literal>
                                <div class="container">
                                    <h2 class="lead">Upload images of damage (max 3)</h2>
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn btn-success fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Add files...</span>
                                        <!-- The file input field used as target for the file upload widget -->
                                        <input id="fileupload" type="file" name="files[]" multiple>
                                    </span>
                                    <br>
                                    <br>
                                    <!-- The global progress bar -->
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <!-- The container for the uploaded files -->
                                    <div id="files" class="files"></div>
                                    <br>
                                </div>
                                <asp:HiddenField ID="UploadedFiles" runat="server" Value="[]"></asp:HiddenField>

                                <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
                                <script src="/assets/common/js/vendor/jquery.ui.widget.js"></script>
                                <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                                <script src="/assets/common/js/load-image.all.min.js"></script>
                                <!-- The Canvas to Blob plugin is included for image resizing functionality -->
                                <script src="/assets/common/js/canvas-to-blob.min.js"></script>
                                <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
                                <script src="/assets/common/js/jquery.iframe-transport.js"></script>
                                <!-- The basic File Upload plugin -->
                                <script src="/assets/common/js/jquery.fileupload.js"></script>
                                <!-- The File Upload processing plugin -->
                                <script src="/assets/common/js/jquery.fileupload-process.js"></script>
                                <!-- The File Upload image preview & resize plugin -->
                                <script src="/assets/common/js/jquery.fileupload-image.js"></script>
                                <!-- The File Upload audio preview plugin -->
                                <script src="/assets/common/js/jquery.fileupload-audio.js"></script>
                                <!-- The File Upload video preview plugin -->
                                <script src="/assets/common/js/jquery.fileupload-video.js"></script>
                                <!-- The File Upload validation plugin -->
                                <script src="/assets/common/js/jquery.fileupload-validate.js"></script>

                                <script src="/assets/common/js/mcklein-fileupload.js"></script>
                                <style>
                                    .fileinput-button {
                                        position: relative;
                                        overflow: hidden;
                                        display: inline-block;
                                    }

                                        .fileinput-button input {
                                            position: absolute;
                                            top: 0;
                                            right: 0;
                                            margin: 0;
                                            opacity: 0;
                                            -ms-filter: 'alpha(opacity=0)';
                                            font-size: 200px;
                                            direction: ltr;
                                            cursor: pointer;
                                        }

                                    /* Fixes for IE < 8 */
                                    @media screen\9 {
                                        .fileinput-button input {
                                            filter: alpha(opacity=0);
                                            font-size: 100%;
                                            height: 100%;
                                        }
                                    }
                                </style>
                                <br />

                                <asp:Button ID="btnSendmail" runat="server" CssClass="submit-button btn btn-default" Text="Send Email" OnClick="btnSendmail_Click" />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="requiredLogin">
                                Please
                                <asp:HyperLink runat="server" ID="loginLink">login or register</asp:HyperLink>
                                to submit a repair request.
                            </asp:Panel>

                        </div>

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</asp:Content>

