<%@ Page Language="C#" MasterPageFile="~/masters/common.master" AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs" Inherits="netpoint.common.accounts.forgotpassword" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<script type="text/javascript">
	var locate = "<%=locate %>";
    $(document).ready(function() {
    	$('input[type=image]').attr('src', '/assets/common/themes/<%=locate %>/assets/img/Button/Submit Button responsive.svg');
    });
</script>
<div class="f_password_container">
	<asp:Label ID="lblConfirm" runat="server" Visible="false" CssClass="npbody" Text="We have sent you a new password via email." />
<table cellpadding="0" cellspacing="0" border="0" id="tblRetrieve" runat="server">
    <tr>
        <td class="npsubheader" align="center" colspan="2">
            <asp:Literal ID="ltlHeader" runat="server" Text="Forgot your password?" />
        </td>
    </tr>
    <tr>
		<td class="npbody" align="center" colspan="2">
		    <asp:Literal ID="ltlInstructions" runat="server" Text="Enter your User Name to receive your password." />
		</td>
	</tr>
	<tr>
	    <td class="nplabel">
	        <asp:Literal ID="ltlUserName" runat="server" Text="User Name" />
	    </td>
	</tr>
	<tr>
		<td class="npbody">
	        <asp:TextBox ID="txtUserName" runat="server" />
	    </td>
	</tr>
	<tr id="trQA" runat="server" visible="false">
	    <td class="nplabel">
	        <asp:Literal ID="ltlQuestion" runat="server" />
	    </td>
	    <td class="npbody">
	        <asp:TextBox ID="txtAnswer" runat="server" />
	    </td>
	</tr>
	<tr id="trErr" runat="server" visible="false">
	    <td class="npbody" align="center" colspan="2">
	        <asp:Label ID="lblErr" runat="server" CssClass="npwarning" Text="Unable to retrieve your password" />
	    </td>
	</tr>
	 <tr>
		<td class="npbody" align="right" colspan="2">
		   <asp:ImageButton ID="btnSubmit" class="mcklein-submit" runat="server"  style="height: 30px; width:114px;"  OnClick="btnSubmit_Click" ToolTip="Submit" />
		   

		</td>
	</tr>
</table>
</div>

</asp:Content>
