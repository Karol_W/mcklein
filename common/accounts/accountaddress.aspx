<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.accountaddress" Codebehind="accountaddress.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="AccountAddressesBlock" Src="~/common/controls/AccountAddressesBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:Label ID="sysError" runat="server" font-bold="true" ForeColor="red"></asp:Label>
    
  
<div class="col-md-9 white-bg">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/addresses.gif" />
                <asp:HyperLink ID="lnkMyAddresses" runat="server" Text="My Addresses" Visible="false"
                    NavigateUrl="~/common/accounts/accountaddress.aspx">My Addresses</asp:HyperLink>
                <asp:Literal ID="sysGT" runat="server" Text=">" Visible="false"></asp:Literal>
                <asp:Label ID="lblMyAddress" runat="server">My Addresses</asp:Label>&nbsp;[<asp:Label
                    ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <np:AccountAddressesBlock ID="AccountAddresses" runat="server"></np:AccountAddressesBlock>
            </td>
        </tr>
    </table>
</div>    
    
  
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="hdnEditAddress" runat="server" Visible="false" Text="Edit Address"></asp:Literal>
    <asp:Literal ID="hdnAddAddress" runat="server" Visible="false" Text="Add a New Address"></asp:Literal>
    <asp:Literal ID="hdnDuplicationAddressNameShipping" runat="server" Text="The Business Partner already has a delivery address with this name." Visible="false"></asp:Literal>
    <asp:Literal ID="hdnDuplicationAddressNameBilling" runat="server" Text="The Business Partner already has a bill to address with this name." Visible="false"></asp:Literal>    
    <asp:Literal ID="hdnAddressNameRequired" runat="server" Text="Address name can not be blank."></asp:Literal>
    <asp:Literal ID="hdnStreet1Required" runat="server" Text="Street1 can not be blank."></asp:Literal>
    <asp:Literal ID="hdnCityIsRequired" runat="server" Text="City can not be blank."></asp:Literal>
    <asp:Literal ID="hdnPostalCodeRequired" runat="server" Text="Postal code is required."></asp:Literal>
</asp:Content>
