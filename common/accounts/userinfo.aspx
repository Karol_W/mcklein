<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.userinfo" Codebehind="userinfo.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="userresource" Src="~/common/controls/UserResourceBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="nptable">
        <tr>
            <td class="npheader" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/userslist.gif" />
                <asp:Label ID="PageView" runat="server"></asp:Label>[<asp:Label ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
    </table>
    <np:userresource ID="npuserresource" runat="server"></np:userresource>    
    <asp:GridView ID="gridView" runat="server" AutoGenerateColumns="false" CssClass="nptable" OnRowCommand="gridView_RowCommand" OnRowDataBound="gridView_RowDataBound">
        <RowStyle CssClass="npbody"></RowStyle>
        <HeaderStyle CssClass="npsubheader"></HeaderStyle>
         <Columns>
            <asp:BoundField DataField="Dimension1"></asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="lnkDimension1" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "Dimension1") %>' Target="_blank">
							<%# DataBinder.Eval(Container.DataItem, "Dimension1") %>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Dimension2"></asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="lnkDimension2" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "Dimension2") %>' Target="_blank">
							<%# DataBinder.Eval(Container.DataItem, "Dimension2") %>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Dimension3"></asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="lnkDimension3" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "Dimension3") %>'
                        Target="_blank">
							<%# DataBinder.Eval(Container.DataItem, "Dimension3") %>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/assets/common/icons/edit.gif"
                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "URID") %>' CommandName="edi">
                    </asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="5%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "URID") %>'
                        ImageUrl="~/assets/common/icons/delete.gif"></asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
     <table class="nptable">
        <tr>
            <td align="right"><asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/assets/common/icons/add.gif" OnClick="btnAdd_Click" ToolTip="Add" /></td>
        </tr>
    </table>
</asp:Content>
