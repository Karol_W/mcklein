<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.UserList" Codebehind="UserList.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="UserListBlock" Src="~/common/controls/UserListBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-9 white-bg">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/userslist.gif" />
                <asp:Label ID="lblUserList" runat="server">User List</asp:Label>
                [<asp:Label ID="sysUserID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <np:UserListBlock ID="Users" runat="server"></np:UserListBlock>
            </td>
        </tr>
    </table>
</div>    

</asp:Content>
