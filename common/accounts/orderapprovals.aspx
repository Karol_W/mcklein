<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.orderapprovals" AutoEventWireup="true"
    Codebehind="orderapprovals.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<section class="container">
<div class="col-md-9 white-bg pull-right">

    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/orderapprovals.png" />
                <asp:Label ID="lblOrderApprovals" runat="server">Orders Waiting Approval</asp:Label>[<asp:Label
                    ID="lblAccountID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="OrdersListGrid" AllowSorting="True" AllowPaging="True" runat="server"
                    Width="100%" AutoGenerateColumns="False" PageSize="20" OnRowDataBound="OrdersListGrid_RowDataBound"
                    EmptyDataText="No Orders Found">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                    <Columns>
                        <asp:BoundField DataField="ClosedDate" SortExpression="ClosedDate DESC" HeaderText="colDate|Date"
                            DataFormatString="{0:yyyy-MMM-dd}" HtmlEncode="false">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:HyperLinkField SortExpression="OrderID DESC" HeaderText="colDocumentNumber|Document Number"
                            DataTextField="OrderID" DataNavigateUrlFields="OrderID" DataNavigateUrlFormatString="ordercart.aspx?orderid={0}"
                            ItemStyle-HorizontalAlign="Center"></asp:HyperLinkField>
                        <asp:BoundField DataField="QuantityTotal" SortExpression="QuantityTotal" HeaderText="colItems|Items">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField Visible="False" DataField="BillingAddress" SortExpression="BillingAddress"
                            HeaderText="colBillingAddress|Billing Address" DataFormatString="&lt;pre&gt;{0}&lt;/pre&gt;"></asp:BoundField>
                        <asp:BoundField DataField="ShippingMethodText" SortExpression="ShippingMethodText"
                            HeaderText="colShipMethod|Ship Method">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField Visible="False" DataField="ShippingAddress" SortExpression="ShippingAddress"
                            HeaderText="colShippingAddress|Shipping Address" DataFormatString="&lt;pre&gt;{0}&lt;/pre&gt;"></asp:BoundField>
                        <asp:TemplateField SortExpression="ShippingAddress" HeaderText="colShippingAddress|Shipping Address">
                            <ItemTemplate>
                                <asp:Literal ID="ltlShippingAddressName" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="ShippingStatus" HeaderText="colStatus|Status">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Literal ID="ltlShipStatus" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UserID" SortExpression="UserID" HeaderText="colUser|User">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField SortExpression="GrandTotal" HeaderText="colTotal|Total">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            <ItemTemplate>
                                <np:PriceDisplay ID="prcGrandTotal" runat="server" Price='<%# Eval("GrandTotal") %>' DisplayCurrency="BaseCurrency" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Position="TopAndBottom" Mode="Numeric" />
                    <PagerStyle HorizontalAlign="Right"></PagerStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
    
</div>    
     </section>
</asp:Content>

<asp:Content ContentPlaceHolderID="hiddenslot" runat="Server" ID="hidden">
    <asp:Literal ID="hdnPending" runat="server" Text="Pending" visible="false"></asp:Literal>
    <asp:Literal ID="hdnShipped" runat="server" Text="Shipped" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnPicked" runat="server" Text="Picked" Visible="false"></asp:Literal>
    <asp:Literal ID="hdnPartial" runat="server" Text="Partial" Visible="false"></asp:Literal>
</asp:Content>


