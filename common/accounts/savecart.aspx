<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.savecart" Codebehind="savecart.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="OrderListsSaveBlock" Src="~/common/controls/OrderListsSaveBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npheader" align="left">
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
                <asp:Label ID="lblSaveThisList" runat="server">Save List</asp:Label>[<asp:Label
                    ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <np:OrderListsSaveBlock ID="OrderListsSaveBlock1" runat="server"></np:OrderListsSaveBlock>
            </td>
        </tr>
    </table>
</asp:Content>
