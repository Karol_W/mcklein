<%@ Register TagPrefix="np" TagName="cdlblock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>
<%@ Register TagPrefix="np" TagName="OrderExpenses" Src="~/commerce/controls/orderexpenses.ascx" %>
<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.ordercart" Codebehind="ordercart.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npheader" align="left" colspan="2">
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Label ID="lblOrderDetail" runat="server">Order Detail</asp:Label>[<asp:Label ID="lblOrderID" runat="server"></asp:Label>]</td>
        </tr>
        <tr>
            <td colspan="2"><br /><np:cdlblock ID="npcdlblock" runat="server" ShowReadOnlyQuantity="false" ShowPartType="True" BorderWidth="1"></np:cdlblock></td>
        </tr>
        <tr>
            <td valign="top">
                <br /><br />
                <asp:Panel runat="server" ID="pnlApproverTools">
                    <asp:Label ID="lblMessage" runat="server" Text="Message:"></asp:Label><br />
                    <asp:TextBox ID="txtMessage" runat="server" TextMode="multiline" Columns="50" Rows="4"></asp:TextBox><br />
                    <table>
                        <tr>
                            <td class="npbutton">
                                <asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/assets/common/icons/thumbsup.gif" />
                                <asp:LinkButton ID="lnkApprove" runat="server" Text="Approve" OnClick="lnkApprove_Click"></asp:LinkButton>&nbsp;</td>
                            <td class="npbutton">
                                <asp:ImageButton ID="imgReject" runat="server" ImageUrl="~/assets/common/icons/thumbsdown.gif" />
                                <asp:LinkButton ID="lnkReject" runat="server" Text="Reject" OnClick="lnkReject_Click"></asp:LinkButton>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td align="right" valign="top">
                <br />
                <np:OrderExpenses ID="expenses" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
