<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.orderdownloads" Codebehind="orderdownloads.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npheader" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/downloads.png" />
                <asp:Label ID="lblOrderDownloads" runat="server">Order Downloads</asp:Label>
                [<asp:Label ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:DataList runat="server" ID="dlDownloads" Width="100%" OnItemDataBound="dlDownloads_ItemDataBound">
                    <ItemTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="npbody" align="center" valign="top" style="width:5%;"><br />
                                    <asp:HyperLink runat="server" ID="lnkFileImage" ImageUrl="~/assets/common/icons/downloads.png"
                                        NavigateUrl="~/common/controls/partdownload.aspx"></asp:HyperLink></td>
                                <td class="npbody" valign="top"><br />
                                    <asp:HyperLink runat="server" ID="lnkFileName" Text="FileName" 
                                        NavigateUrl="~/common/controls/partdownload.aspx"></asp:HyperLink><br />
                                    <asp:Label runat="server" ID="lblFileDescription">Description</asp:Label></td>
                                <td class="npbody" align="right" valign="top"><br />
                                    <asp:Label runat="server" ID="lblVersion"></asp:Label><br />
                                    <asp:Label runat="server" ID="lblFileSize"></asp:Label></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>
