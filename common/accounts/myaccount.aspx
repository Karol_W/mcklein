<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.myaccount" Codebehind="myaccount.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-12">
<div class="full-width-wrapper">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Literal ID="ltlSeperator" runat="server" Text=" - "></asp:Literal>
                <asp:Label ID="lblUserID" runat="server"></asp:Label></td>
 
		 <!-- tab "panes" --> 
		  <div class="css-panes-admin"> 
			  <div id="AcctInfo">
			  
			  <h2><asp:Literal ID="ltlUserInformation" runat="server" Text="User Information"></asp:Literal></h2>
			  
			  <asp:Panel ID="pnlMyProfile" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgMyProfile" runat="server" NavigateUrl="~/common/user/profile.aspx"
                                    ImageUrl="~/assets/common/icons/profile.gif" ToolTip="My Profile"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkMyProfile" runat="server" Text="My Profile" NavigateUrl="~/common/user/profile.aspx"></asp:HyperLink></asp:Panel>                            
                            
                            <asp:Panel ID="pnlMySalesRep" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgMySalesRep" runat="server" NavigateUrl="~/common/user/salesrep.aspx"
                                    ImageUrl="~/assets/common/icons/salesperson.png" ToolTip="My Sales Rep"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkMySalesRep" runat="server" Text="My Sales Representative" NavigateUrl="~/common/user/salesrep.aspx"></asp:HyperLink></asp:Panel>
                                
                                 <asp:Panel ID="pnlMyRegisteredParts" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgMyRegisteredParts" runat="server" NavigateUrl="~/common/user/registeredparts.aspx"
                                    ImageUrl="~/assets/common/icons/registered.png" ToolTip="My RegisteredItems"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkMyRegisteredParts" runat="server" Text="My Registered Items"
                                    NavigateUrl="~/common/user/registeredparts.aspx"></asp:HyperLink></asp:Panel>

                            <asp:Panel ID="pnlMyDownloads" runat="server" CssClass="npbutton" Visible="false">
                                <asp:HyperLink ID="imgMyDownloads" runat="server" NavigateUrl="~/common/accounts/orderdownloads.aspx"
                                    ImageUrl="~/assets/common/icons/downloads.png" ToolTip="My Downloads"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkMyDownloads" runat="server" Text="My Order Downloads"
                                    NavigateUrl="~/common/accounts/orderdownloads.aspx"></asp:HyperLink></asp:Panel>
                            </div>
			  <div id="OrderInfo">
			  
			  	<h2><asp:Literal ID="ltlOrderInformation" runat="server" Text="Order Information"></asp:Literal></h2>
			  
                            <asp:Panel ID="pnlSavedLists" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgSavedLists" runat="server" NavigateUrl="savedlists.aspx" ImageUrl="~/assets/common/icons/savedlist.gif" ToolTip="Saved Lists"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkSavedLists" runat="server" Text="Saved Lists" NavigateUrl="savedlists.aspx"></asp:HyperLink></asp:Panel>
                            <asp:Panel ID="pnlOrderHistory" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgOrderHistory" runat="server" NavigateUrl="~/common/accounts/orderhistory.aspx"
                                    ImageUrl="~/assets/common/icons/orderhistory.png" ToolTip="Order History"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkOrderHistory" runat="server" Text="Order History" NavigateUrl="~/common/accounts/orderhistory.aspx"></asp:HyperLink></asp:Panel>
                           
                            <asp:Panel ID="pnlAccountTransactions" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgAccountTransactions" runat="server" NavigateUrl="~/common/accounts/accountbooks.aspx"
                                    ImageUrl="~/assets/common/icons/transactions.gif" ToolTip="Account Transactions"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkAccountTransactions" runat="server" Text="Account Transactions"
                                    NavigateUrl="~/common/accounts/accountbooks.aspx"></asp:HyperLink></asp:Panel>
                            <asp:Panel ID="pnlSpecialPricing" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgSpecialPricing" runat="server" NavigateUrl="~/common/accounts/SpecialPricing.aspx"
                                    ImageUrl="~/assets/common/icons/pricing_pricelistprice.gif" ToolTip="Special Pricing"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkSpecialPricing" runat="server" Text="Special Pricing"
                                    NavigateUrl="~/common/accounts/specialpricing.aspx"></asp:HyperLink></asp:Panel>                                     
                            <asp:Panel ID="pnlPreviouslyPurchased" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgPreviouslyPurchased" runat="server" NavigateUrl="~/common/accounts/PreviouslyPurchased.aspx"
                                    ImageUrl="~/assets/common/icons/cart.gif" ToolTip="Previously Purchased Items"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkPreviouslyPurchased" runat="server" Text="Previously Purchased Items"
                                    NavigateUrl="~/common/accounts/PreviouslyPurchased.aspx"></asp:HyperLink></asp:Panel></div>
			  
			  
			  <div id="userUDF"> 
			  
			  <h2><asp:Literal ID="ltlAccountInformation" runat="server" Text="Account Information"></asp:Literal></h2>
			  
			  <asp:Panel ID="pnlAcctInfo" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgAcctInfo" runat="server" NavigateUrl="~/common/accounts/accountinfo.aspx"
                                    ImageUrl="~/assets/common/icons/accountprofile.png" ToolTip="Account Info"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkAcctInfo" runat="server" Text="Account Information" NavigateUrl="~/common/accounts/accountinfo.aspx"></asp:HyperLink></asp:Panel>
                            <asp:Panel ID="pnlAccountAddresses" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgAccountAddresses" runat="server" NavigateUrl="~/common/accounts/accountaddress.aspx"
                                    ImageUrl="~/assets/common/icons/addresses.gif" ToolTip="Account Addresses"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkAccountAddresses" runat="server" Text="Account Addresses" NavigateUrl="~/common/accounts/accountaddress.aspx"></asp:HyperLink></asp:Panel>
                            <asp:Panel ID="pnlUserList" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgUserList" runat="server" NavigateUrl="~/common/accounts/userlist.aspx" ImageUrl="~/assets/common/icons/userslist.gif" ToolTip="User List"></asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="lnkUserList" runat="server" Text="Use List" NavigateUrl="~/common/accounts/userlist.aspx">User List</asp:HyperLink></asp:Panel>
			                <asp:DataList ID="ResourcesUserRepeater" runat="server" Width="100%" CssClass="npbody"
                                RepeatLayout="Table" RepeatColumns="1" BorderWidth="0" GridLines="None" CellPadding="0"
                                CellSpacing="0">
                                <ItemTemplate>
                                    <asp:Panel ID="Panel1" runat="server" CssClass="npbutton">
                                        <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="~/assets/common/icons/indicator.gif"></asp:HyperLink>
                                        &nbsp; <a href='userinfo.aspx?ResourceID=<%#DataBinder.Eval(Container.DataItem,"ResourceID")%>'>
                                            <%#DataBinder.Eval(Container.DataItem,"Name")%></a>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:DataList></div>
		  </div> 
        
                            <asp:Literal ID="ltlEventInformation" runat="server" Text="Event Information" Visible="False"></asp:Literal>
                            <asp:Panel ID="pnlAdmin" runat="server" CssClass="npbutton">
                                <asp:HyperLink ID="imgAdmin" runat="server" ImageUrl="~/assets/common/icons/administration.png" ToolTip="Administration"></asp:HyperLink>
                                <asp:HyperLink ID="lnkAdmin" NavigateUrl="~/admin/default.aspx" Text="Administration"
                                    runat="Server"></asp:HyperLink></asp:Panel>

                            <asp:LinkButton ID="lnkLogout" runat="server" OnCommand="Logout" ToolTip="Log Out"> 
			                    <asp:Image ID="imgLogout" runat="server" ImageUrl="~/assets/common/buttons/logoff.gif" />
			                </asp:LinkButton>           
			                
</div>
</div>                                 
</asp:Content>
