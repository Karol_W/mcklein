<%@ Register TagPrefix="np" TagName="accountgl" Src="../controls/AccountGL.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.accountbooks" Codebehind="accountbooks.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-9 white-bg pull-right">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left" style="height: 22px">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/transactions.gif" />
                <asp:Label ID="lblAccountTransactions" runat="server">Account Transactions</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <np:accountgl ID="accountgl" runat="server"></np:accountgl>
            </td>
        </tr>
    </table>
</div>

</asp:Content>
