<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.orderhistory" Codebehind="orderhistory.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="OrderHistoryBlock" Src="~/common/controls/OrderHistoryBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-9 white-bg">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/orderhistory.png" />
                <asp:Label ID="lblOrderHistory" runat="server">Order History</asp:Label>
                [<asp:Label ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
        <tr>
            <td>
                <np:OrderHistoryBlock ID="Orders" runat="server"></np:OrderHistoryBlock>
            </td>
        </tr>
    </table>
</div>

</asp:Content>
