﻿<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.specialpricing" Codebehind="specialpricing.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="pricetree" Src="~/admin/catalog/controls/pricelisttree.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="npheader" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="imgSpecialPricing" runat="server" ImageUrl="~/assets/common/icons/pricing_pricelistprice.gif" />
                <asp:Label ID="lblSpecialPricing" runat="server">Special Pricing</asp:Label>
                [<asp:Label ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
    </table>
    <np:pricetree id="specialPriceTree" runat="server" ></np:pricetree>
</asp:Content>
