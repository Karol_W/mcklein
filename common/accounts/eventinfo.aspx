<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.eventinfo" Codebehind="eventinfo.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="userresource" Src="~/common/controls/UserResourceBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table border="0" cellpadding="2" cellspacing="1" width="100%">
        <tr>
            <td class="npheader" align="left">
                <asp:HyperLink ID="LinkBack" runat="server" Text="My Account" NavigateUrl="myaccount.aspx" />&gt;
                <asp:Label ID="PageView" runat="server" />
                [<asp:Label ID="lblUserID" runat="server" />]</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="2" cellspacing="0" width="100%" border="1" style="border-color:Black;" class="npbody">
                    <tr>
                        <td valign="top"><np:userresource ID="npuserresource" runat="server" /></td>
                    </tr>
                </table>
                <br /><br />
            </td>
        </tr>
    </table>
</asp:Content>
