<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.savedlists" Codebehind="savedlists.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="OrderListsBlock" Src="~/common/controls/OrderListsBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-11 white-bg">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td align="left">
                <asp:Panel ID="pnlListsHeader" runat="server" CssClass="user-header-breadcrumb">
                    <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                    <asp:HyperLink ID="lnkBack" runat="server" Text="My Account" NavigateUrl="myaccount.aspx">My Account</asp:HyperLink>&gt;
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/savedlist.gif" />
                    <asp:Label ID="lblSavedLists" runat="server">Saved Lists</asp:Label>[<asp:Label ID="sysUserID" runat="server"></asp:Label>]
                </asp:Panel>
                <asp:Panel ID="pnlDetailHeader" runat="server" CssClass="user-header-breadcrumb" Visible="false">
                    <asp:Image ID="imgAcctDetail" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                    <asp:HyperLink ID="lnkBackDetail" runat="server" Text="My Account" NavigateUrl="myaccount.aspx">My Account</asp:HyperLink>&gt;
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/savedlist.gif" />
                    <asp:HyperLink ID="lnkLists" runat="server" Text="Saved Lists" NavigateUrl="~/common/accounts/savedlists.aspx"></asp:HyperLink>
                    [<asp:Label ID="sysUserDetail" runat="server"></asp:Label>]&gt;
                    <asp:Literal ID="ltlMyList" runat="server" Text="My List"></asp:Literal>
                    [<asp:Literal ID="sysListName" runat="server"></asp:Literal>]
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td><np:OrderListsBlock ID="SavedOrders" runat="server"></np:OrderListsBlock></td>
        </tr>
    </table>
</div>

</asp:Content>
