<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.accountsupport" Codebehind="accountsupport.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <table class="npbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="1">
        <tr>
            <td colspan="4"><asp:HyperLink ID="lnkBack" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Back"></asp:HyperLink></td>
        </tr>
        <tr>
            <td colspan="4" class="npheader"><asp:Literal ID="ltlContractCode" runat="server" Text="Contract Code"></asp:Literal></td>
        </tr>
        <tr>
            <td><br /><asp:Literal ID="ltlEffectiveDate" runat="server" Text="Effective Date"></asp:Literal></td>
            <td><br /><asp:Literal ID="ltlStartDate" runat="server"></asp:Literal></td>
            <td><br /><asp:Literal ID="ltlTerminationDate" runat="server" Text="Termination Date"></asp:Literal></td>
            <td><br /><asp:Literal ID="ltlEndDate" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td colspan="4"><br /><asp:Literal ID="ltlDescription" runat="server" Text="Description"></asp:Literal></td>
        </tr>
    </table>
</asp:Content>
