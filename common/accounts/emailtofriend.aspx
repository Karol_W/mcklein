<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.EmailToFriend" Codebehind="EmailToFriend.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/PriceDisplay.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
   <section class="container">
    <div class="email_list">
    <table class="npbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td class="npheader" colspan="2"><asp:Literal ID="ltlEmailYourShoppingList" runat="server" Text="Email Your Shopping List"></asp:Literal></td>
        </tr>
        <tr>
            <td style="width:10%; white-space:nowrap;"><asp:Literal ID="ltlMailFrom" runat="server" Text="Email From"></asp:Literal></td>
            <td><asp:Literal ID="ltlEmailFrom" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td style="width:10%; white-space:nowrap;">
                <asp:Literal ID="ltlEmail" runat="server" Text="Send To Email"></asp:Literal>
                <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
                </asp:RequiredFieldValidator></td>
            <td><asp:TextBox ID="txtEmail" runat="server" Width="100%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width:10%; white-space:nowrap;"><asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal></td>
            <td><asp:TextBox ID="txtSubject" runat="server" Text="My wish list" Width="100%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width:10%; white-space:nowrap;"><asp:Literal ID="ltlMessage" runat="server" Text="Message"></asp:Literal></td>
            <td><asp:TextBox ID="txtMessage" runat="server" Width="100%" Height="72px"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:ImageButton ID="btnGo" runat="server" ImageUrl="~/assets/common/buttons/Send_button.png" ToolTip="Go" /></td>
            <td>
                <asp:GridView 
                    ID="gvEmails" 
                    runat="server" 
                    CellPadding="0" 
                    Width="100%" 
                    AutoGenerateColumns="False" 
                    EmptyDataText="No Records Found" 
                    OnRowDataBound="gvEmails_RowDataBound">
                    <RowStyle CssClass="npbody" />
                    <HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />  
                    <Columns>
                        <asp:TemplateField>
                           
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="img" runat="server"></asp:Image>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                           
                            <ItemTemplate>
                        <div class="emailwish" style="padding-left: 65px;">   <b><asp:Literal ID="ltlDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PartName")%>'></asp:Literal></b><br /><br />
                              <asp:HyperLink ID="lnkPart" runat="server" Visible="true" ImageUrl="http://mckleinusa.com/assets/common/icons/WishList_Email_shop.jpg"
                NavigateUrl="~/catalog/partdetail.aspx?partno=">wishlist</asp:HyperLink></div>
                                </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errInvalidEmail" runat="server" Text="Invalid Email address" Visible="False"></asp:Literal>
    <asp:Literal ID="errFailedToSendEmail" runat="server" Text="Your email was not sent. \n Please try again later." Visible="False"></asp:Literal>
</asp:Content>
