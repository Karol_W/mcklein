<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.accounts.createaccount" CodeBehind="createaccount.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" OnCreatedUser="CreateUserWizard1_CreatedUser">
        <WizardSteps>
            <asp:CreateUserWizardStep runat="server" Title="Choose login credentials.">
                <ContentTemplate>

                    <div class="col-md-12 pull-right">
                        <div class="create-account-wrapper">
                            <h1>
                                <asp:Literal ID="ltlNewAccount" runat="server" Text="Register below."></asp:Literal></h1>


                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                            <div class="error">
                                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>


                                <table class="nptable">
                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:-</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" AutoComplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" AutoComplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                                ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                                ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question">Security Question:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="Question" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ControlToValidate="Question"
                                                ErrorMessage="Security question is required." ToolTip="Security question is required."
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                                ErrorMessage="Security answer is required." ToolTip="Security answer is required."
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblTitle" runat="server" AssociatedControlID="txtTitle">Title:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">First Name:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                                ErrorMessage="First Name is required." ToolTip="First Name is required."
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblMiddleName" runat="server" AssociatedControlID="txtMiddleName">Middle Name:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Last Name:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                                ErrorMessage="Last Name is required." ToolTip="Last Name is required."
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblSuffix" runat="server" AssociatedControlID="txtSuffix">Suffix:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtSuffix" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lblCompanyName" runat="server" AssociatedControlID="txtCompanyName">Company Name:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="nplabel">
                                            <asp:Label ID="lbltaxId" runat="server" AssociatedControlID="txtTaxId">Tax ID:</asp:Label>
                                        </td>
                                        <td class="npbody">
                                            <asp:TextBox ID="txtTaxId" runat="server" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me on this computer."></asp:CheckBox>
                                        </td>
                                    </tr>
                                </table>
                                <div>
                                    <asp:Label ID="ConditionID" runat="server">(Title, Middle Name, Suffix, Company Name, and Tax ID are optional fields.) </asp:Label>
                                </div>


                            </div>
                        </div>
                </ContentTemplate>
            </asp:CreateUserWizardStep>


            <asp:CompleteWizardStep runat="server">
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
    <asp:Literal ID="hdnPasswordFaiureText" runat="server" Text="Password length minimum: {0}. Numeric characters required: {1}. Non-Numeric characters required: 1" Visible="False"></asp:Literal>
    <asp:Literal ID="hdnDuplicateEmailText" runat="server" Text="The e-mail address that you entered is already in use. Please enter a different e-mail address." Visible="False"></asp:Literal>
    <asp:Literal ID="hdnInvalidUsernameText" runat="server" Text="Username is invalid. Please ensure no special characters are being used." Visible="False"></asp:Literal>
</asp:Content>

