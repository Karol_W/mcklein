﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/commonm.Master" CodeBehind="previouslypurchased.aspx.cs" Inherits="netpoint.common.accounts.purchaseditems" %>
<%@ Register TagPrefix="np" TagName="plblock" Src="~/catalog/controls/PartsListBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<div class="col-md-9 white-bg pull-right">
    <table cellspacing="1" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="user-header-breadcrumb" align="left">
                <asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
                <asp:HyperLink ID="LinkBack" runat="server" NavigateUrl="myaccount.aspx" Text="My Account"></asp:HyperLink>&gt;
                <asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/cart.gif" />
                <asp:Label ID="lblPreviouslyPurchased" runat="server">Previously Purchased Items</asp:Label>
                [<asp:Label ID="lblUserID" runat="server"></asp:Label>]
            </td>
        </tr>
    </table>
    
    <np:plblock ID="npplblock" runat="server" ListType="P" DisplayNumber="25" DisplayType="Q" BorderWidth="0" TypeVisible="False" CompareVisible="False" UnitVisible="False"/>
</div>    
 
</asp:Content>

