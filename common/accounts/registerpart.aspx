<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.accounts.registerpart" Codebehind="registerpart.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="DatePicker" Src="~/common/controls/NPDatePicker.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <section class="container">
    <div class="col-md-9 white-bg pull-right">
    <asp:Panel ID="pnl" runat="server">
        <table class="npbody" cellspacing="0" cellpadding="5" width="100%" border="1">
            <tr>
                <td class="npheader" colspan="2">
                    <asp:HyperLink ID="lnkBack" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Back"></asp:HyperLink>&nbsp;
                    <asp:Literal ID="sysHeader" runat="server">Product Registration</asp:Literal></td>
            </tr>
            <tr>
                <td class="nplabel" align="right" style="width:25%;">
                    <asp:Literal ID="ltlManufacturer" runat="server" Text="Manufacturer"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlMNFCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMNFCode_SelectedIndexChanged"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="nplabel" align="right"><asp:Literal ID="ltlPart" runat="server" Text="Item"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlPart" runat="server"></asp:DropDownList> &nbsp;
                    <asp:RequiredFieldValidator ID="rfvPart" runat="server" ControlToValidate="ddlPart"><asp:Image ID="sysPartReq" runat="server" ImageUrl="~/assets/common/icons/warning.gif" /></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="nplabel" align="right"><asp:Literal ID="ltlRegistrationID" runat="server" Text="Registration ID"></asp:Literal></td>
                <td><asp:TextBox ID="txtRegistrationNumber" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="nplabel" align="right">Purchase Date</td>
                <td>
                    <np:DatePicker ID="calPurchaseDate" runat="server"></np:DatePicker>
                    <asp:Image ID="imgPurchaseDateWarning" runat="server" ImageUrl="~/assets/common/icons/warning.gif" Visible="False"></asp:Image></td>
            </tr>
            <tr>
                <td class="nplabel" align="right"><asp:Literal ID="ltllocationPurchased" runat="server" Text="Location Purchased"></asp:Literal></td>
                <td><asp:DropDownList ID="ddlLocationPurchased" runat="server" AutoPostBack="True"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="nplabel" align="right"><asp:Literal ID="ltlPurchasePrice" runat="server" Text="Purchase Price"></asp:Literal></td>
                <td>
                    <asp:TextBox ID="txtPurchasePrice" runat="server" Text="0"></asp:TextBox>&nbsp;&nbsp;
                    <asp:Label ID="sysError" runat="server" class="npwarning"></asp:Label></td>
            </tr>
            <tr>
                <td class="nplabel" align="right" colspan="2"><asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/buttons/addinfo.gif" ToolTip="Submit" /></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlThankYou" runat="server" Visible="false">
        <table class="npbody" id="tblThankYou" cellspacing="0" cellpadding="5" width="100%"
            border="1">
            <tr>
                <td colspan="2"><asp:HyperLink ID="lnkBack2" runat="server" ImageUrl="~/assets/common/icons/cancel.gif" ToolTip="Back2"></asp:HyperLink></td>
            </tr>
            <tr>
                <td class="npheader" colspan="2"><asp:Literal ID="ltlProdReg" runat="server" Text="Product Registration"></asp:Literal></td>
            </tr>
            <tr>
                <td align="center"><br /><asp:Literal ID="ltlThankYouForRegistering" runat="server" Text="Thank you for registering."></asp:Literal></td>
            </tr>
        </table>
    </asp:Panel>
    </div>
      </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="errMustBeNumber" runat="server" Visible="False" Text="Purchase price must be a number."></asp:Literal>
</asp:Content>
