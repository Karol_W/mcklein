<%@ Page Language="c#" MasterPageFile="~/masters/commonlogin.master" Inherits="netpoint.common.accounts.login" Codebehind="login.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="loginblock" Src="~/common/controls/login.ascx" %>
<%@ Register TagPrefix="np" TagName="expressloginblock" Src="~/common/controls/ExpressCheckoutLogin.ascx" %>
<asp:Content ContentPlaceHolderID="messageslot" runat="server" ID="Content0">
    <asp:label id="sysMessage" runat="server" enableviewstate="False" CssClass="npwarning"></asp:label>
</asp:Content>
<asp:Content ContentPlaceHolderID="newuserslot" runat="server" ID="Content1">
    <asp:panel id="pnlCreateAccount" runat="server">
	    
	   <div class="create-account-area">
	   <h5><asp:Literal ID="ltlCreateNewAccount" runat="server" Text="Create a New Account"></asp:Literal></h5>
	   <p><asp:Literal ID="ltlCreateText" runat="server" Text="Take a moment to create an account, and you'll be able to: Save time on your next order, view order status and take advantage of a host of other extranet features with your own E-account. To get started, click 'Create Account' button below."></asp:Literal></p>

      <asp:HyperLink ID="CreateAccountButton" ImageUrl="~/assets/common/buttons/createaccount.gif" NavigateUrl="~/common/accounts/createaccount.aspx" runat="server" ToolTip="Create Account"></asp:HyperLink>
	   </div>

    </asp:panel>
</asp:Content>
<asp:Content ContentPlaceHolderID="loginslot" runat="server" ID="Content2">

		<div class="existing-accounts-area">
		<h5><asp:literal id="ltlExistingAccounts" runat="server" text="Existing Accounts"></asp:literal></h5>
		<np:loginblock ID="nploginblock" runat="server"></np:loginblock>
		</div>
		
</asp:Content>
<asp:Content ContentPlaceHolderID="expressslot" runat="server" ID="Content3">
    <asp:panel id="pnlExpressCheckout" runat="server" Visible="false">

	    <asp:Literal ID="ltlExpressCheckoutHeader" runat="server" Text="Express Checkout"></asp:Literal>
	    
	    <np:expressloginblock ID="npexpressloginblock" runat="server"></np:expressloginblock>
	    
    </asp:panel>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:literal id="errLoginIn" runat="server" text="Please log in before checking out" visible="False"></asp:literal>
    <asp:literal id="errTimeoutExpired" runat="server" text="Session has expired. Please log in again." visible="False"></asp:literal>
</asp:Content>
