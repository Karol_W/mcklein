﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.sendRMACart" CodeBehind="sendRMACart.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="cdlblock" Src="~/commerce/controls/CartDetailListBlock.ascx" %>

<%@ Register TagPrefix="np" TagName="PriceDisplay" Src="~/common/controls/pricedisplay.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">


    <table width="100%" cellpadding="0" cellspacing="0" class="npbody email-wrapper">
        <tbody>
            <tr>
                <td align="left" valign="top">
                    <div id="page-wrap">


                        <p>
                            <strong style="font-weight: bold !important;">
                                <asp:Label ID="sysSentMessage" runat="server" Visible="false">Thank you! Your message has been sent.</asp:Label></strong>
                        </p>

                        <br />
                        <div id="contact-area">


                            <asp:Panel runat="server" ID="contactForm" CssClass="container-fluid">
                                <div class="form-group">
                                    <label style="margin-bottom: 5px;">
                                        <asp:Label ID="lbl_RMANumber" runat="server" Text="RMA Number"></asp:Label></label>
                                    <asp:TextBox ID="eml_RMANumber" runat="server" class="form-control"></asp:TextBox>
                                </div>

                                <div class="form-group">
                                    <label style="margin-bottom: 5px;">
                                        <asp:Label ID="lbl_customer" runat="server" Text="Customer ID"></asp:Label></label>
                                    <asp:TextBox ID="customer" runat="server" class="form-control"></asp:TextBox>
                                </div>

                                <asp:GridView ID="gridProducts" runat="server" CssClass="nptable" AutoGenerateColumns="False" Width="100%" GridLines="None"
                                    EmptyDataText="Something Went Wrong">
                                    <RowStyle CssClass="npbody" />
                                    <HeaderStyle CssClass="npsubheader"></HeaderStyle>
                                    <AlternatingRowStyle CssClass="npbodyalt" />
                                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="nbQuantity" runat="server" Text='0' type="number" Style="width: 3em" min="0" />

                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                Quantity
                                            </HeaderTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="price" runat="server" Text='<%# new netpoint.api.catalog.NPPart((String)DataBinder.Eval(Container, "DataItem.PartNo")).BasePrice.ToString("C") %>'></asp:Label>

                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                Price
                                            </HeaderTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="number" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNo") %>'></asp:Label>

                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                Part Number
                                            </HeaderTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="item" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartName") %>'></asp:Label>

                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                Item
                                            </HeaderTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>

                                <div class="form-group">
                                    <label style="margin-bottom: 5px;">
                                        <asp:Label ID="lbl_discount" runat="server" Text="Discount Percentage"></asp:Label>
                                    </label>
                                    <asp:DropDownList ID="discount" runat="server" class="form-control">
                                        <asp:ListItem Text="0%" Value="0" />
                                        <asp:ListItem Text="10%" Value="10" />
                                        <asp:ListItem Text="20%" Value="20" />
                                        <asp:ListItem Text="30%" Value="30" />
                                        <asp:ListItem Text="40%" Value="40" />
                                        <asp:ListItem Text="50%" Value="50" />
                                        <asp:ListItem Text="60%" Value="60" />
                                        <asp:ListItem Text="70%" Value="70" />
                                        <asp:ListItem Text="80%" Value="80" />
                                        <asp:ListItem Text="90%" Value="90" />
                                        <asp:ListItem Text="100%" Value="100" />
                                    </asp:DropDownList>

                                </div>

                                <div class="form-group">
                                    <label style="margin-bottom: 5px;">
                                        <asp:Label ID="lbl_shipping" runat="server" Text="Shipping Type"></asp:Label>
                                    </label>
                                    <asp:DropDownList ID="shipping" runat="server" class="form-control">
                                        <asp:ListItem Value="6" Text="USPS Flat Rate ($7)" />
                                        <asp:ListItem Value="10" Text="UPS $20" />
                                        <asp:ListItem Value="15" Text="UPS $25" />
                                        <asp:ListItem Value="0" Text="Free" />
                                    </asp:DropDownList>

                                </div>





                                <asp:Literal ID="sysEmailSubject" runat="server" Visible="false">McKlein Repair Form</asp:Literal>

                                <asp:Literal ID="sysEmailFrom" runat="server" Visible="false">mckleinwebsite@mckleincompany.com</asp:Literal>

                                <asp:Literal ID="sysEmailAddress" runat="server" Visible="false">customerservice@mckleincompany.com</asp:Literal>

                                <asp:Literal ID="sysEmailTemplate" runat="server" Visible="false">general</asp:Literal>
                                <asp:Literal ID="sysEmailBody" runat="server" Visible="false">
                                    <p>{0},</p>

                                    <p><br>
                                    </p>

                                    <p>Thank you for contacting McKlein Company regarding your repair request #{1}.</p>

                                    <p>
                                        We have processed your request and are now ready to proceed with payment.
                                        The repair parts will cost {2}.
                                        To pay, please click the link below, and check out:
                                        <br />
                                        <a href="{3}">{3}</a>
                                    </p>

                                    <p><br>
                                    </p>

                                    <p>Thank you very much,</p>

                                    <p><br>
                                    </p>

                                    <p><span style="font-weight: bold;">Customer Service </span></p>
                                    <p><span style="font-weight: bold;">McKlein Company, L.L.C.</span></p>
                                    <p><span style="font-weight: bold;">4447 W. Cortland St. Chicago, IL 60639</span></p>
                                    <p><span style="font-weight: bold;">Phone:773:235:0600 ext 230</span></p>
                                </asp:Literal>
                                
                                <asp:Literal ID="orderNo" runat="server" Visible="false"></asp:Literal>
                                <asp:HyperLink ID="orderLink" runat="server" Visible="false"></asp:HyperLink>


                                <br />
                                <asp:Button ID="btn_processOrder" runat="server" CssClass="btn btn-default" Text="Process Order" OnClick="processOrder_Click" />

                                <asp:Panel ID="messageBlock" runat="server"  Visible="false">
                                    <asp:Label ID="totalPrice" runat="server"></asp:Label>
                                    <asp:Panel ID="email" class="form-group" runat="server">
                                        <label style="margin-bottom: 5px;">
                                            <asp:Label ID="lbl_message" runat="server" Text="Email Body"></asp:Label>
                                        </label>
                                        <asp:TextBox ID="message" runat="server" TextMode="Multiline" Rows="10" class="form-control"></asp:TextBox>

                                    </asp:Panel>

                                    <asp:Button ID="btnSendmail" runat="server" CssClass="submit-button btn btn-default" Text="Send Email" OnClick="btnSendmail_Click" />
                                    <script src="/assets/common/js/summernote.min.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        var link = document.createElement('link');
                                        link.setAttribute('rel', 'stylesheet');
                                        link.setAttribute('type', 'text/css');
                                        link.setAttribute('href', '/assets/common/css/font-awesome.min.css');
                                        document.getElementsByTagName('head')[0].appendChild(link);
                                        var link = document.createElement('link');
                                        link.setAttribute('rel', 'stylesheet');
                                        link.setAttribute('type', 'text/css');
                                        link.setAttribute('href', '/assets/common/css/summernote.css');
                                        document.getElementsByTagName('head')[0].appendChild(link);
                                        
                                        $(function () {
                                            var value = $("#ctl00_mainslot_message").val();
                                            $("#ctl00_mainslot_message").val(unescape(value));
                                            $('#ctl00_mainslot_message').summernote({
                                                onChange: function (e) {
                                                    $("#ctl00_mainslot_message").val($("#ctl00_mainslot_message").code());
                                                },
                                                height: 300,                 // set editor height

                                                minHeight: null,             // set minimum height of editor
                                                maxHeight: null             // set maximum height of editor

                                            });
                                            $("form").submit(function (event) {
                                                var value = $("#ctl00_mainslot_message").val();
                                                $("#ctl00_mainslot_message").val(escape(value));
                                            });
                                        });
                                    </script>
                                </asp:Panel>
                            </asp:Panel>

                        </div>

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</asp:Content>

