<%@ Register TagPrefix="np" TagName="requestblock" Src="~/plugins/RequestBlock.ascx" %>

<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.request" Codebehind="request.aspx.cs" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <np:requestblock ID="requestblock" runat="server" SubmitToProspect="True"></np:requestblock>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal ID="sysIgnoreLoginRequired" runat="server" Text="False" Visible="false" />
</asp:Content>