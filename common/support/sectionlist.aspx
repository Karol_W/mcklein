<%@ Page Language="c#" MasterPageFile="~/masters/support.master" Inherits="netpoint.common.support.sectionList" Codebehind="sectionList.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    &nbsp;
    <asp:Repeater ID="rpt" runat="server">
        <HeaderTemplate>
            <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td rowspan="2" style="width:25%;" align="center">
                    <asp:ImageButton ID="btnImage" runat="server" CommandName="go" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SectionID") %>'></asp:ImageButton></td>
                <td class="npheader">
                    <asp:Literal ID="ltlSectionName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SectionName") %>'></asp:Literal></td>
            </tr>
            <tr>
                <td class="npbody">
                    <asp:Literal ID="ltlDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ResourcePage.pageblurb") %>'></asp:Literal></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
