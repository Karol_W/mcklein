<%@ Page Language="C#" MasterPageFile="~/masters/support.master" AutoEventWireup="true"
    Codebehind="default.aspx.cs" Inherits="netpoint.common.support.search" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

<table cellpadding="5" cellspacing="10" width="100%">
    <tr>
        <td valign="top" align="left">
            <asp:Label runat="server" ID="lblSearch" Text="Search: "></asp:Label>
            <asp:TextBox runat="server" ID="txtSearch" Columns="50">
            </asp:TextBox>
            <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            <asp:GridView ID="gvResults" runat="server" Width="100%" PageSize="25" CssClass="npbody"
                OnPageIndexChanging="gvResults_PageIndexChanging" AllowPaging="True" ShowHeader="false"
                AutoGenerateColumns="False" BorderWidth="0" EmptyDataText="No Results Found">
                <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                <pagersettings mode="Numeric" position="Bottom" />
                <pagerstyle horizontalalign="right" />
                <columns>
                        <asp:TemplateField HeaderText="colSolution|Solution">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("SolutionName") %>' NavigateUrl='<%# Bind("SolutionID","SolutionDetail.aspx?SolutionID={0}") %>'></asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                </li>
                                <br />
                                <br />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </columns>
            </asp:GridView>
        </td>
    </tr>
</table>

</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
</asp:Content>
