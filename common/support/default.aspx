<%@ Page Language="C#" MasterPageFile="~/masters/support.master" AutoEventWireup="true" Codebehind="default.aspx.cs" Inherits="netpoint.common.support.landingpage" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<h1><asp:Literal runat="server" ID="ltlSolutionCenterHeader" Text="Knowledge Base"></asp:Literal></h1>
    <table cellpadding="5" cellspacing="10" width="100%">
        <tr>
            <td colspan="2" valign="top" align="left">
                <asp:Label runat="server" ID="lblSearch" Text="Search: "></asp:Label>
                <asp:TextBox runat="server" ID="txtSearch" Columns="50"></asp:TextBox>
                <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnSearch_Click" ToolTip="Search" /></td>
        </tr>
        <tr>
            <td style="width:25%;" class="npheader">
                <asp:Literal runat="server" ID="ltlSolutionTypes" Text="Topics"></asp:Literal></td>
            <td style="width:75%;" class="npheader">
                <asp:Literal runat="server" ID="ltlFeatured" Text="Featured Articles"></asp:Literal>
                <asp:Literal runat="server" ID="sysCodeType"></asp:Literal></td>
        </tr>
        <tr>
            <td style="width:25%;" valign="top">
                <asp:GridView ID="gvTypes" runat="server" Width="100%" CssClass="npbody" ShowHeader="false"
                    AutoGenerateColumns="False" BorderWidth="0" EmptyDataText="No Types Found">
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                    <Columns>
                        <asp:TemplateField HeaderText="colSolution|Entry">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <li><asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("CodeName") %>' 
                                    NavigateUrl='<%# Bind("CodeValue","default.aspx?type={0}") %>'></asp:HyperLink></li><br />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gvResults" runat="server" Width="100%" PageSize="5" CssClass="npbody"
                    OnPageIndexChanging="gvResults_PageIndexChanging" AllowPaging="True" ShowHeader="false"
                    AutoGenerateColumns="False" BorderWidth="0" EmptyDataText="No Results Found">
                    <EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
                    <PagerSettings Mode="Numeric" Position="Bottom" />
                    <PagerStyle HorizontalAlign="right" />
                    <Columns>
                        <asp:TemplateField HeaderText="colSolution|Entry">
                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <li><asp:HyperLink ID="lnkName" runat="server" Text='<%# Bind("SolutionName") %>' 
                                        NavigateUrl='<%# Bind("SolutionID","SolutionDetail.aspx?SolutionID={0}") %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label></li><br /><br />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
</asp:Content>
