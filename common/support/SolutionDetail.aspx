<%@ Page Language="C#" MasterPageFile="~/masters/support.master" AutoEventWireup="true"
    Codebehind="default.aspx.cs" Inherits="netpoint.common.support.SolutionDetail" %>
<%@ Register TagPrefix="np" TagName="solutionview" Src="~/common/support/controls/solutionview.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<np:solutionview runat="server" id="solutionview"></np:solutionview>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
</asp:Content>