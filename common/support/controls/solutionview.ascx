<%@ Control Language="C#" AutoEventWireup="true" Codebehind="solutionview.ascx.cs"
    Inherits="netpoint.common.support.controls.solutionview" %>
<style type="text/css">
.kbsubtitle {
	MARGIN: 15px 0px 5px;
}
.kbsubbody {
	MARGIN: 0px 0px 10px;
}
</style>
<table class="npbody" cellpadding="5" cellspacing="10" width="100%">
    <tr>
        <td>
            <h3 class="npheader">
                <asp:Literal runat="server" ID="sysSolutionName"></asp:Literal></h3>
            <asp:Panel runat="server" ID="pnlMoreInfo" Visible="false">
                <h4 class="kbsubtitle">
                    <asp:Label runat="Server" ID="ltlMoreInfoHeader" Text="MORE INFO"></asp:Label></h4>
                <asp:Label runat="Server" ID="sysMoreInfo" CssClass="kbsubbody" Text=""></asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSymptoms" Visible="false">
                <h4 class="kbsubtitle">
                    <asp:Label runat="Server" ID="ltlSymptomsHeaders" Text="SYMPTOMS"></asp:Label></h4>
                <asp:Label runat="Server" ID="sysSymptoms" CssClass="kbsubbody" Text=""></asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlCause" Visible="false">
                <h4 class="kbsubtitle">
                    <asp:Label runat="Server" ID="ltlCauseHeader" Text="CAUSE"></asp:Label></h4>
                <asp:Label runat="Server" ID="sysCause" CssClass="kbsubbody" Text=""></asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlResolution" Visible="false">
                <h4 class="kbsubtitle">
                    <asp:Label runat="Server" ID="ltlResolutionHeader" Text="RESOLUTION"></asp:Label></h4>
                <asp:Label runat="Server" ID="sysResolution" CssClass="kbsubbody" Text=""></asp:Label>
            </asp:Panel>
        </td>
    </tr>
</table>
