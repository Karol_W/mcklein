<%@ Page Language="c#" MasterPageFile="~/masters/support.master" Inherits="netpoint.common.support.supportpage" ValidateRequest="false" Codebehind="supportpage.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="ResourcePage" Src="../controls/ResourcePage.ascx" %>

<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<div class="col-md-9 pull-right white-bg">
	<table class="npbody" cellspacing="1" cellpadding="2" width="100%" border="0">
		<tbody>
			<tr>
				<td class="npbody" colspan="2">
					<np:ResourcePage ID="rsp" runat="server"></np:ResourcePage>
				</td>
			</tr>
			<tr>
				<td class="npheader" colspan="2">
					<asp:Literal ID="ltlPhone" runat="server" Text="Phone Support"></asp:Literal></td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:GridView ID="gvSupport" runat="server" CellPadding="1" AllowPaging="True"
						 Width="100%" AutoGenerateColumns="False" PageSize="20" EmptyDataText="No Records Found">
						<RowStyle CssClass="npbody" />
						<HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
						<EmptyDataRowStyle CssClass="npemtpy" Height="50px" />  
						<Columns>
							<asp:BoundField DataField="DepartmentName" HeaderText="colDepartment|Department"></asp:BoundField>
							<asp:BoundField DataField="DepartmentPhone" HeaderText="colPhone|Phone"></asp:BoundField>
							<asp:BoundField DataField="DepartmentEmail" HeaderText="colEmail|Email"></asp:BoundField>
						</Columns>
					</asp:GridView>            
				</td>
			</tr>
			<tr>
				<td class="npheader" colspan="2">
					<asp:Literal ID="ltlEmail" runat="server" Text="Email Support"></asp:Literal></td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlSubject" runat="server" Text="Subject"></asp:Literal></td>
				<td>
					<asp:TextBox ID="txtSubject" runat="server" Width="422" Columns="50"></asp:TextBox><asp:RequiredFieldValidator
						ID="rfvSubject" runat="server" ErrorMessage="Subject required" ControlToValidate="txtSubject">
						<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
					</asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlMessage" runat="server" Text="Message"></asp:Literal></td>
				<td>
					<asp:TextBox ID="txtMessage" runat="server" Columns="50" Rows="12" TextMode="MultiLine"></asp:TextBox><asp:RequiredFieldValidator
						ID="rfvMessage" runat="server" ErrorMessage="Message required." ControlToValidate="txtMessage">
						<asp:Image ID="Image2" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
					</asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlEmail1" runat="server" Text="Email"></asp:Literal></td>
				<td>
					<asp:TextBox ID="txtUserEmailAddress" runat="server" Columns="40"></asp:TextBox><asp:RequiredFieldValidator
						ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email address is required."
						ControlToValidate="txtUserEmailAddress">
						<asp:Image ID="WarningIcon" runat="server" ImageUrl="~/assets/common/icons/warning.gif" />
					</asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td class="nplabel">
				</td>
				<td>
					<asp:Image ID="imgEmail" runat="server" Visible="false" ImageUrl="~/assets/common/icons/warning.gif">
					</asp:Image><asp:Literal ID="ltlInvalidEmail" runat="server" Text="Invalid Email address"
						Visible="False"></asp:Literal></td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlSection" runat="server" Text="Queue"></asp:Literal></td>
				<td>
					<asp:DropDownList ID="ddlSectionID" runat="server"></asp:DropDownList>
					<asp:Label ID="sysSectionName" runat="server"></asp:Label>
					<asp:Literal ID="sysSectionID" runat="server" visible="false"></asp:Literal>
					</td>
			</tr>
							<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlType" runat="server" Text="Type"></asp:Literal></td>
				<td>
					<asp:DropDownList ID="ddlTicketType" runat="server"></asp:DropDownList>
					<asp:Label ID="sysTicketTypeName" runat="server"></asp:Label>
					<asp:Literal ID="sysTicketType" runat="server" visible="false"></asp:Literal>
					</td>
			</tr>
							<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlProject" runat="server" Text="Project"></asp:Literal></td>
				<td>
					<asp:DropDownList ID="ddlProjectID" runat="server"></asp:DropDownList>
					<asp:Label ID="sysProjectName" runat="server"></asp:Label>
					<asp:Literal ID="sysProjectID" runat="server" visible="false"></asp:Literal>
					</td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlUserID" runat="server" Text="User ID"></asp:Literal></td>
				<td>
					<asp:Label ID="lblUserID" runat="server"></asp:Label></td>
			</tr>
			<tr>
				<td class="nplabel">
					<asp:Literal ID="ltlAccountID" runat="server" Text="Business Partner ID"></asp:Literal></td>
				<td>
					<asp:Label ID="lblAccountID" runat="server"></asp:Label></td>
			</tr>
			<tr>
				<td colspan="2" align="Center">
					<asp:ImageButton id="btnSubmit" runat="server" ImageUrl="~/assets/common/buttons/submit.gif"
						CommandName="submit" ToolTip="Submit"></asp:ImageButton></td>
			</tr>
		</tbody>
	</table>
</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" runat="server" ID="hidden">
    <asp:Literal id="hdnSection" runat="server" text="Section" visible="False"></asp:Literal>
    <asp:Literal id="hdnEnterTicket" runat="server" text="Enter Ticket" visible="False"></asp:Literal>
</asp:Content>
