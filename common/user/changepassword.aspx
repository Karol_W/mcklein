<%@ Page MasterPageFile="~/masters/commonm.master" Language="C#" AutoEventWireup="true" CodeBehind="changepassword.aspx.cs" Inherits="netpoint.common.user.changepassword" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<div class="col-md-9 white-bg pull-right">
	<asp:Label ID="sysPasswordExpired" runat="server" EnableViewState="false" CssClass="npwarning" Visible="false">Your current password has expired and must be changed before continuing.</asp:Label><br />
	<asp:Label ID="sysMessage" runat="server" EnableViewState="false" CssClass="npwarning"></asp:Label><br />
	<table class="nptable">
		<tr>
			<td colspan="2" class="npheader">
			<asp:Literal ID="ltlHeader" runat="server" Text="Change Your Password" />
			</td>
		</tr>    
		<tr>
			<td class="nplabel" style="width:30%;">
				<asp:Literal ID="ltlOldPassword" runat="server" Text="Old Password"></asp:Literal>
			</td>
			<td class="npbody">
				<asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtOldPassword" ErrorMessage="*"></asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="nplabel">
				<asp:Literal ID="ltlNewPassword" runat="server" Text="New Password"></asp:Literal>
			</td>
			<td class="npbody">
				 <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
				 <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" EnableClientScript="false" ControlToValidate="txtNewPassword" ErrorMessage="" Display="None"></asp:RequiredFieldValidator>
			</td>
		</tr>
		<tr>
			<td class="nplabel">
				<asp:Literal ID="ltlConfirmNewPassword" runat="server" Text="Confirm New Password"></asp:Literal>
			</td>
			<td class="npbody">
				 <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
				 <asp:RequiredFieldValidator ID="rfvConfirmNewPassword" runat="server" EnableClientScript="false" ControlToValidate="txtConfirmNewPassword" ErrorMessage="" Display="None"></asp:RequiredFieldValidator>
			</td>
		</tr>
		<tr>
			<td class="nplabel">
				<asp:Literal ID="ltlQuestion" runat="server" Text="Security Question"></asp:Literal>
			</td>
			<td class="npbody">
				 <asp:TextBox ID="txtQuestion" runat="server" Width="400px" MaxLength="255"></asp:TextBox>
				 <asp:RequiredFieldValidator ID="rfvSecurityQuestion" runat="server" EnableClientScript="false" ControlToValidate="txtQuestion" ErrorMessage="" Display="None"></asp:RequiredFieldValidator>
			</td>
		</tr>
		<tr>
			<td class="nplabel">
				<asp:Literal ID="ltlAnswer" runat="server" Text="Security Answer"></asp:Literal>
			</td>
			<td class="npbody">
				 <asp:TextBox ID="txtAnswer" runat="server"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvSecurityAnswer" runat="server" EnableClientScript="false" ControlToValidate="txtAnswer" Display="None"></asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="npbody" colspan="2" align="center">
				<asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/assets/common/buttons/update.gif" OnClick="btnSubmit_Click" ToolTip="Submit"></asp:ImageButton>
			</td>
		</tr>
	</table>
</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="hiddenslot" ID="thehidden" runat="server">
<asp:Literal ID="ltlOldPassInvalid" runat="server" Visible="false" Text="Your old password is invalid."></asp:Literal>
<asp:Literal ID="ltlPasswordMatch" runat="server" Visible="false" Text="Your new password and it's confirmation must match."></asp:Literal>
<asp:Literal ID="ltlPasswordLength" runat="server" Visible="false" Text="Your password must be {0} characters long."></asp:Literal>
<asp:Literal ID="ltlPasswordNonAlphanumericCharacters" runat="server" Visible="false" Text="Your new password must contain at least {0} numeric character(s) and 1 non-numeric character."></asp:Literal>
<asp:Literal ID="ltlPasswordRegularExpression" runat="server" Visible="false" Text="Your password does not meet the complexity requirement."></asp:Literal>
<asp:Literal ID="hdnLoginNameMustBeUnique" runat="server" Text="This login name is already in use.  Please try another."></asp:Literal>
<asp:Literal ID="ltlSecurityQuestionAnswer" runat="server" Visible="false" Text="Both 'Security Question' and 'Security Answer' must be filled."></asp:Literal>
<asp:Literal ID="ltlFillPasswordOrQuestion" runat="server" Visible="false" Text="Fill in 'New Password' or 'Security Question'."></asp:Literal>
<asp:Literal ID="ltlPasswordUsed" runat="server" Visible="false" Text="Your new password must be different from the previous {0} passwords."></asp:Literal>
<asp:Literal ID="ltlEmailExists" runat="server" Visible="false" Text="Your email address is not unique."></asp:Literal>
</asp:Content>