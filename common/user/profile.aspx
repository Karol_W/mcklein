<%@ Register TagPrefix="np" TagName="UserProfileBlock" Src="../controls/UserProfileBlock.ascx" %>
<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.user.profile" Codebehind="profile.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
	<div class="white-bg">
		<table>
			<tr>
				<td class="user-header-breadcrumb">
					<asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
					<asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="../accounts/myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
					<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/profile.gif" />
					<asp:Label ID="ltlMyProfile" runat="server">My Profile</asp:Label>[<asp:Label ID="lblUserID" runat="server"></asp:Label>]</td>
				<td class="npheader" align="right">
					<asp:HyperLink ID="lnkChangePassword" runat="server" 
						NavigateUrl="~/common/user/changepassword.aspx" Text="Change Password"></asp:HyperLink></td>
			</tr>
			<tr>
				<td colspan="2"><np:UserProfileBlock ID="UserProfile" runat="server"></np:UserProfileBlock></td>
			</tr>
		</table>
	</div>
</asp:Content>
