<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.user.tickets" Codebehind="tickets.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<section class="container">
    <div class="col-md-9 white-bg pull-right">
		<table cellspacing="1" cellpadding="2" width="100%" border="0">
			<tr>
				<td class="user-header-breadcrumb" align="left">
					<asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
					<asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="../accounts/myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
					<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/tickets.gif" />
					<asp:Label ID="ltlMyTickets" runat="server">My Tickets</asp:Label>[<asp:Label ID="lblUserID" runat="server"></asp:Label>]</td>
			</tr>
		</table>
		<asp:GridView ID="TicketsListGrid" runat="server" PageSize="25" Width="100%" 
			AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true"
			OnSorting="TicketsListGrid_Sorting"
			OnRowDataBound="TicketsListGrid_RowDataBound" OnPageIndexChanging="TicketsListGrid_PageIndexChanging"
			EmptyDataText="No Records Found">
			<RowStyle CssClass="npbody" />
			<HeaderStyle HorizontalAlign="Center" CssClass="npsubheader" />
			<EmptyDataRowStyle CssClass="npemtpy" Height="50px" />
			<Columns>
				<asp:TemplateField SortExpression="SubmitDate" HeaderText="colDate|Date">
					<ItemTemplate>
						<asp:Literal ID="ltlDate" runat="server"></asp:Literal>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField SortExpression="TicketID" HeaderText="colTicketID|Ticket ID" DataField="TicketID" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
				<asp:BoundField DataField="TicketType" SortExpression="TicketType" HeaderText="TicketType|Ticket Type"></asp:BoundField>
				<asp:BoundField DataField="StatusName" SortExpression="Status" HeaderText="colStatus|Status"></asp:BoundField>
				<asp:TemplateField SortExpression="Subject" HeaderText="colMessage|Message">
					<ItemTemplate>
						<b><asp:Literal ID="ltlSubject" runat="server"></asp:Literal></b>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Position="TopAndBottom" Mode="Numeric" />
			<PagerStyle HorizontalAlign="Right"></PagerStyle>
		</asp:GridView>
	</div>
	</section>
</asp:Content>
