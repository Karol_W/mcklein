<%@ Page Language="c#" MasterPageFile="~/masters/commonm.master" Inherits="netpoint.common.user.salesrep" Codebehind="salesrep.aspx.cs" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">

	<div class="col-md-9 white-bg">
		<table class="npbody" id="Table1" cellspacing="0" cellpadding="1" width="100%" border="0">
			<tr>
				<td class="user-header-breadcrumb" align="left" colspan="2">
					<asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
					<asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="../accounts/myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
					<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/salesperson.png" />
					<asp:Literal ID="ltlSalesRep" runat="server" Text="My Sales Representative"></asp:Literal>&nbsp;
					[<asp:Label ID="lblUserID" runat="server"></asp:Label>]</td>
			</tr>
			<tr>
				<td class="nplabel" colspan="2"><asp:Literal ID="ltlYourSalesRep" runat="server" Text="Meet Your Sales Representative"></asp:Literal></td>
			</tr>
			<tr>
				<td><asp:Image ID="imgRep" runat="server" BorderStyle="Inset" /></td>
				<td>
					<asp:Literal ID="ltlRep" runat="server" Text="Rep"></asp:Literal><br />
					<asp:HyperLink ID="lnkEmail" runat="server" ImageUrl="~/assets/common/icons/send.gif" Visible="false" Text="Mail Me" />
				</td>
			</tr>
		</table>
	</div>

</asp:Content>
