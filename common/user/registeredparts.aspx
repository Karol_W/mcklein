<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.user.registeredparts" Codebehind="registeredparts.aspx.cs" %>
<%@ Register TagPrefix="np" TagName="RegisteredParts" Src="../controls/RegisteredParts.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <section class="container">
    <div class="col-md-9 white-bg pull-right">
		<table cellspacing="1" cellpadding="2" width="100%" border="0">
			<tr>
				<td class="user-header-breadcrumb" align="left">
					<asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
					<asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="../accounts/myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
					<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/registered.png" />
					<asp:Label ID="ltlMyRegisteredParts" runat="server">My Registered Items</asp:Label>[<asp:Label ID="lblUserID" runat="server"></asp:Label>]</td>
			</tr>
			<tr>
				<td><np:RegisteredParts ID="UserParts" runat="server"></np:RegisteredParts></td>
			</tr>
		</table>
	</div>
	</section>
</asp:Content>
