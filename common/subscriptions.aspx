<%@ Page Language="c#" MasterPageFile="~/masters/common.master" Inherits="netpoint.common.subscriptions" Codebehind="subscriptions.aspx.cs" %>

<%@ Register TagPrefix="np" TagName="UserSubscriptionsBlock" Src="~/common/controls/UserSubscriptionsBlock.ascx" %>
<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
<section class="container">
	<div class="col-md-9 white-bg pull-right">
		<table cellspacing="1" cellpadding="2" width="100%" border="0">
			<tr>
				<td class="user-header-breadcrumb">
					<asp:Image ID="imgMyAccount" runat="server" ImageUrl="~/assets/common/icons/user.gif" />
					<asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/common/accounts/myaccount.aspx" Text="My Account">My Account</asp:HyperLink>&gt;
					<asp:Image ID="Image1" runat="server" ImageUrl="~/assets/common/icons/campaigns.gif" />
					<asp:Label ID="ltlMySubscriptions" runat="server">My Subscriptions</asp:Label>[<asp:Label ID="lblUserID" runat="server"></asp:Label>]</td>            
			</tr>
			<tr>
				<td><np:UserSubscriptionsBlock ID="UserSubscriptions" runat="server"></np:UserSubscriptionsBlock></td>
			</tr>
		</table>
	</div>
	</section>
</asp:Content>
