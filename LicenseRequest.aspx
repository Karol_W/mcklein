<%@ Page Language="C#" AutoEventWireup="true" Codebehind="LicenseRequest.aspx.cs"
    ValidateRequest="false" Inherits="netpoint.LicenseRequest" %>

<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>zed eCommerce License Request</title>

<SCRIPT language="JavaScript">
function ClipBoard() {
    document.form1.tbDetails.select(); document.form1.tbDetails.focus();
    textRange = document.form1.tbDetails.createTextRange();
    textRange.execCommand("Copy");
}
</SCRIPT>

</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <asp:Panel runat="server" BackColor="LightGoldenrodYellow" BorderColor="Silver" HorizontalAlign="Left"
                BorderWidth="1" Width="550px" Visible="false" ID="pnlError">
                <asp:Image runat="server" ID="imgError" ImageUrl="~/assets/common/images/error.gif"
                    ImageAlign="Middle" />
                <asp:Label runat="server" ID="lblError"></asp:Label>
            </asp:Panel>
            <br />
            <table width="550">
                <tr>
                    <td width="73">
                        <asp:Image runat="server" ID="imgzedLogo" ImageUrl="~/assets/common/images/zedlogo_license.png" /></td>
                    <td valign="bottom">
                        <h3>
                            zed eCommerce License Request</h3>
                    </td>
                </tr>
            </table>
            <ComponentArt:TabStrip ID="TabStrip1" ImagesBaseUrl="~/assets/common/admin/" ClientScriptLocation="~/scripts"
                MultiPageId="MultiPage1" Width="550" runat="server">
                <Tabs>
                    <ComponentArt:TabStripTab runat="server" ID="TabStripTab1" Text="Request A License">
                    </ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="TabStripTab2" Text="License Request Details">
                    </ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab runat="server" ID="TabStripTab3" Text="Install Received License">
                    </ComponentArt:TabStripTab>
                </Tabs>
            </ComponentArt:TabStrip>
            <ComponentArt:MultiPage ID="MultiPage1" CssClass="MultiPage" runat="server" ClientScriptLocation="~/scripts">
                <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvRequest">
                    <table width="550">
                        <tr>
                            <td align="right" style='display:none; visibility:hidden;'>
                                Owner SAP Customer Number</td>
                            <td align="left">
                                <asp:TextBox ID="tbCustomerName" runat="server" Visible="false" Columns="30" text="0"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvtbCustomerName"  ControlToValidate="tbCustomerName" ValidationGroup="requestlicense" >
                                    <asp:Image runat="server" ID="rfvimgError1" ImageUrl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Owner Organization</td>
                            <td align="left">
                                <asp:TextBox ID="tbOwnerOrganization" runat="server" Columns="30"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvtbOwnerOrganization" ControlToValidate="tbOwnerOrganization" ValidationGroup="requestlicense">
                                    <asp:Image runat="server" ID="Image1" ImageUrl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Owner Contact Person</td>
                            <td align="left">
                                <asp:TextBox ID="tbOwnerContact" runat="server" Columns="30"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvtbOwnerContact" ControlToValidate="tbOwnerContact" ValidationGroup="requestlicense">
                                    <asp:Image runat="server" ID="Image2" ImageUrl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Technical Contact Person</td>
                            <td align="left">
                                <asp:TextBox ID="tbTechnicalContact" runat="server" Columns="30"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvtbTechnicalContact" ControlToValidate="tbTechnicalContact" ValidationGroup="requestlicense">
                                    <asp:Image runat="server" ID="Image3" ImageUrl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Technical Contact Email Address</td>
                            <td align="left">
                                <asp:TextBox ID="tbTechnicalEmail" runat="server" Columns="30"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvtbTechnicalEmail" ControlToValidate="tbTechnicalEmail" ValidationGroup="requestlicense">
                                    <asp:Image runat="server" ID="Image4" ImageUrl="~/assets/common/icons/warning.gif" />
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                License Type</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlType" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Machine Name</td>
                            <td align="left">
                                <asp:TextBox ID="tbMachineName" runat="server" ReadOnly="true" Columns="30"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Machine ID</td>
                            <td align="left">
                                <asp:TextBox ID="tbMachineID" runat="server" ReadOnly="true" Columns="30"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Requesting FQDN</td>
                            <td align="left">
                                <asp:TextBox ID="tbFQDN" runat="server" ReadOnly="true" Columns="30"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Version Number</td>
                            <td align="left">
                                <asp:TextBox ID="tbVersion" runat="server" Columns="12" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Number of Named Users</td>
                            <td align="left">
                                <asp:TextBox ID="tbNamedUsers" runat="server" Columns="4" Text="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                License zedSuite WebCRM</td>
                            <td align="left">
                                <asp:CheckBox ID="cbFocus" runat="server" Checked="true"></asp:CheckBox></td>
                        </tr>
                        <tr>
                            <td align="right">
                                License zed eCommerce</td>
                            <td align="left">
                                <asp:CheckBox ID="cbCommerce" runat="server" Checked="true"></asp:CheckBox></td>
                        </tr>
                    </table>
                    <table width="550">
                        <tr>
                            <td valign="bottom" align="center">
                                <asp:Button runat="server" ID="btnCreate" Text="Create License Request" OnClick="btnCreate_Click" ValidationGroup="requestlicense" /></td>
                        </tr>
                    </table>
                </ComponentArt:PageView>
                <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvDetails">
                    <asp:Label runat="server" ID="Label1">Copy and Paste request information below and send to <a href="mailto:support@zedsuite.com">zedSuite</a> to receive license information.</asp:Label>
                    <br />
                    <asp:TextBox runat="server" ID="tbDetails" Columns="65" Rows="25" TextMode="MultiLine"
                        Wrap="false"></asp:TextBox>
                    <br />
                    <asp:HyperLink runat="Server" ID="lnkCopy" NavigateUrl="javascript:ClipBoard()">Copy To Clipboard</asp:HyperLink>
                 </ComponentArt:PageView>
                <ComponentArt:PageView CssClass="npadminbody" runat="server" id="pvInstall">
                    <asp:Label runat="server" ID="lblInstructions">Cut and Paste License information you received from zedSuite in the textbox below.</asp:Label>
                    <br />
                    <asp:TextBox runat="server" ID="tbRequest" Columns="65" Rows="25" TextMode="MultiLine"
                        Wrap="false"></asp:TextBox>
                    <br />
                    <asp:Button runat="server" ID="btnInstallLicense" Text="Install License" CausesValidation=false OnClick="btnInstallLicense_Click" />
                </ComponentArt:PageView>
            </ComponentArt:MultiPage>
        </div>
    </form>
</body>
</html>
