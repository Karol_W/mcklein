﻿<%@ Control Language="C#" ClassName="ViewCartDetails" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.commerce" %>

<script runat="server">
    void Page_Load(object sender, System.EventArgs e){
        NPBasePage bp = (NPBasePage)Page;
        NPOrder o = new NPOrder(bp.UserID, bp.SessionID);
        txtGrandTotal.Text = o.GrandTotal.ToString("c", bp.AppCulture.NumberFormat);
        lnkCheckOut.NavigateUrl = "~/commerce/cart.aspx";
        lnkCheckOut.ImageUrl = bp.ResolveImage("icons", "cart.gif");
        lnkCheckOut.ToolTip = "Proceed to Checkout";
    }
</script>
<table>
<tr><td>Cart Total:</td>
<td><asp:Label ID="txtGrandTotal" runat="server" /></td>
</tr>
<tr>
<td colspan="2" align="right"><asp:hyperlink id="lnkCheckOut" runat="server" ></asp:hyperlink></td>
</tr>
</table>