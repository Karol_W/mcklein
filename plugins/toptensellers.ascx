<%@ Control Language="C#" ClassName="TopTenSellers" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.api.data" %>
<%@ Import Namespace="netpoint.api.framework" %>
<script runat="server">
    
    void Page_Load(object sender, System.EventArgs e) {
        NPBasePage bp = (NPBasePage)Page;
        DataParameters dp = new DataParameters(1);
        dp.Add(NPDataType.NPInt, "@catalogid", bp.Catalog.CatalogID);
        ArrayList theParts = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.Append("SELECT TOP 10 od.PartNo ");
        sql.Append("FROM OrderDetail od ");
        sql.Append("INNER JOIN OrderMaster o ON o.OrderID = od.OrderID ");
        sql.Append("INNER JOIN PartsCategory pc ON pc.PartNo = od.PartNo ");
        sql.Append("INNER JOIN CatalogsCategory cc ON cc.CategoryID= pc.CategoryID ");
        sql.Append("WHERE o.CartType = 'O' and cc.CatalogID= @catalogid ");
        sql.Append("GROUP BY od.PartNo, cc.CatalogID ");
        sql.Append("ORDER BY Count(od.PartNo) DESC ");

        using (IDataReader theReader = DataFunctions.ExecuteReader(CommandType.Text, sql.ToString(), dp.Parameters, bp.ConnectionString)) {
            while (theReader.Read()) {
                theParts.Add(new NPPart(theReader.GetString(0), bp.Connection));
            }
        }

        rptTopTen.DataSource = theParts;
        rptTopTen.DataBind();
    }

    protected void rptTopTen_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        NPPart part = (NPPart)e.Item.DataItem;
        HyperLink lnkPart = (HyperLink)e.Item.FindControl("lnkPart");
        if (lnkPart != null) {
            lnkPart.NavigateUrl = ((NPBasePage)Page).VirtualPath + "catalog/partdetail.aspx?partno=" + part.PartNo;
        }
    }
    
</script>

<div id="plugin_topten">
    <h2>Top Ten Sellers</h2>
    <ul id="plugin_topten_ul">
        <asp:Repeater ID="rptTopTen" runat="server" OnItemDataBound="rptTopTen_ItemDataBound">
            <ItemTemplate>
                <li>
                    <asp:HyperLink ID="lnkPart" runat="server">
                        <%# DataBinder.Eval(Container, "DataItem.PartName") %>
                    </asp:HyperLink>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>

