﻿<%@ Control Language="C#" ClassName="Plugin" AutoEventWireup="true" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<script runat="server">
    void Page_Load(object sender, System.EventArgs e) {
        NPBasePage bp = (NPBasePage)Page;
        if (!Page.IsPostBack) {
            ddlmnf.DataSource = bp.Catalog.Manufacturers;
            ddlmnf.DataTextField = "Name";
            ddlmnf.DataValueField = "ManufacturerCode";
            ddlmnf.DataBind();
        }
        if (Request.QueryString["MNFCode"] != null) {
            ddlmnf.SelectedIndex = ddlmnf.Items.IndexOf(ddlmnf.Items.FindByValue(Request.QueryString["MNFCode"]));
        }
    }

    protected void ddlmnf_SelectedIndexChanged(object sender, EventArgs e) {
        Response.Redirect("~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + ddlmnf.SelectedValue);
    }

    protected void btnfind_Click(object sender, ImageClickEventArgs e) {
        Response.Redirect("~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + ddlmnf.SelectedValue);
    }
</script>
<div id="manufacturersddl">
<ASP:DropDownList id="ddlmnf" runat="server" OnSelectedIndexChanged="ddlmnf_SelectedIndexChanged" AutoPostBack="true">
</ASP:DropDownList>
<asp:ImageButton runat="server" ID="btnfind" Visible="false" ImageUrl="~/assets/common/icons/search.gif" OnClick="btnfind_Click" />
</div>