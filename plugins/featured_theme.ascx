﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="featured_theme.ascx.cs" Inherits="netpoint.plugins.featured_theme" %>
<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>

<asp:Repeater id="FeaturedRepeater" runat="server"  OnItemDataBound="ItemDataBound">
 <ItemTemplate>
 
 <section class="row home-featured-products">
    <div class="col-md-2"><asp:HyperLink Runat="server" ID="PartImageLink" class="item-image"/></div>
    
    <div class="col-md-6">
	    <asp:HyperLink Runat="server" ID="PartNoLink" class="featured-name" />
	    <asp:Label ID="PartDescription" Runat="server" class="featured-desc" />
	    <p class="item-amount">Price: <np:PartPriceDisplay ID="SpecialPrice" runat="server"/></p>
    </div>
    
    <div class="col-md-4">
	    <div class="item-action-button">
	    <asp:HyperLink Runat="server" ID="MoreInfo" runat="server" />
	    <asp:HyperLink ID="AddToCart" runat="server" />
	    </div>
    </div>
 </section>   

</ItemTemplate>
</asp:Repeater>
<style> #myCarousel {display: block !important;} </style>