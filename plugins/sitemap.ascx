<%@ register tagprefix="np" TagName="treemenu" src="~/common/controls/treemenu.ascx"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="c#"  %>
<%@ Import Namespace="netpoint.classes" %>

<script language="C#" runat="server">
    void Page_Load(object sender, System.EventArgs e)
    {
	    System.Web.UI.HtmlControls.HtmlLink link = new System.Web.UI.HtmlControls.HtmlLink();
        link.Attributes.Add("type", "text/css");
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("href", "~/plugins/sitemap/siteMapStyle.css");
        this.Controls.Add(link);
    }
</script> 

   <ComponentArt:SiteMap id="SiteMap1" Width="547" Height="250" 
      SiteMapXmlFile="~/plugins/sitemap/siteMap.xml"
      SiteMapLayout="Directory"
      CssClass="SiteMap"
      RootNodeCssClass="RootNode"
      ParentNodeCssClass="ParentNode"
      LeafNodeCssClass="LeafNode"
      runat="server">
      <table>
        <ComponentArt:SiteMapTableRow>
          <ComponentArt:SiteMapTableCell valign="top">
          </ComponentArt:SiteMapTableCell>
        </ComponentArt:SiteMapTableRow>
      </table>      
    </ComponentArt:SiteMap>
