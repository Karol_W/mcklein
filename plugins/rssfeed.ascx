﻿<%@ Control Language="C#" ClassName="RssFeed" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<script language="C#" runat=server>
private void Page_Load(object sender, System.EventArgs e) {
    if (!Page.IsPostBack){
		myfeed.DataSource = "http://rss.slashdot.org/Slashdot/slashdot";
		myfeed.DataBind();
	}
}
</script>
<NP:rssfeed HorizontalAlign="center" width="100%" 
           id="myfeed" runat="server" 
           Font-Names="Helvetica" Font-Size="10pt" 
		   CssClass="blockbodymain"
           CellPadding="4">
  <AlternatingItemStyle BackColor="#EDEDED" CssClass="blockbodyheader" />
  <HeaderStyle Font-Size="14pt" HorizontalAlign="right" 
          Font-Bold="True" ForeColor="White" 
          BackColor="Silver"></HeaderStyle>
  <ItemTemplate>
    <strong><asp:Hyperlink runat="server"
        NavigateUrl='<%# Container.DataItem.Link %>'
        Text='<%# Container.DataItem.Title %>' ID="Hyperlink1"></asp:Hyperlink>
    </strong>
    <br />
    <i><%# Container.DataItem.PubDate %></i><br />
    <%# Container.DataItem.Description %>
  </ItemTemplate>
</NP:rssfeed>

