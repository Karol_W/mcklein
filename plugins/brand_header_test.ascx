<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>

<script language="C#" runat="server">
    private NPBasePage _bp;

    protected void Page_Load(object sender, System.EventArgs e){
        _bp = (NPBasePage)Page;

        var mckleinRegex = new Regex("^(www\\.)?mcklein[^.]*");
        var parindaRegex = new Regex("^(www\\.)?[^.]*parinda");
        var siamodRegex = new Regex("^(www\\.)?siamod");

        var baseAddressRegex = new Regex("(dev.|live.)?[^\\.]+\\.[^\\.]+(:[0-9]+)?$");
        var baseAddress = baseAddressRegex.Match(HttpContext.Current.Request.Url.Host).Value;

        var request = HttpContext.Current.Request;

        if(mckleinRegex.IsMatch(request.Url.Host)) {
            lnkMcklein.CssClass = "active-brand-bar brand-link";
            lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {            
            if(baseAddress.StartsWith("mckleinusa.com")) {
                lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + "mcklein." + baseAddress + "/";
            }
        }

        if(parindaRegex.IsMatch(request.Url.Host)) {
            lnkParinda.CssClass = "active-brand-bar brand-link";
            lnkParinda.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {            
            if(baseAddress.StartsWith("shopparinda.com")) {
                lnkParinda.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkParinda.NavigateUrl = request.Url.Scheme + "://" + "parinda." + baseAddress + "/";
            }
        }

        if(siamodRegex.IsMatch(request.Url.Host)) {
            lnkSiamod.CssClass = "active-brand-bar brand-link";
            lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {
            if(baseAddress.StartsWith("siamod.com")) {
                lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + "siamod." + baseAddress + "/";
            }
        }
         
    }

</script>
<div class="row my-row-no-margin">
    <div  Runat="server" class="col-xs-3 text-center global-brands-container">
        <asp:HyperLink id="lnkMcklein" class="brand-link" Runat="server">
        <img class="tester-img brand-header-mck retina-display" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/mcklein-barlogo3.png" />
        <img class="tester-img brand-header-mck normal-display" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/mcklein-barlogo2.png" />
    </asp:HyperLink>
    </div>
    <div class="col-xs-3 text-center global-brands-container">
        <asp:HyperLink id="lnkParinda" class="brand-link" Runat="server">
        <img class="tester-img brand-header-par retina-display" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/parinda-barlogo3.png" />
        <img class="tester-img brand-header-par normal-display" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/parinda-barlogo2.png" />
    </asp:HyperLink>
    </div>
    <div class="col-xs-3 text-center global-brands-container">
        <asp:HyperLink id="lnkSiamod" class="brand-link" Runat="server">
        <img class="tester-img brand-header-sia retina-display" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/siamod-barlogo3.png" />
        <img class="tester-img brand-header-sia normal-display" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/siamod-barlogo2.png" />
    </asp:HyperLink>
    </div>
    <div class="col-xs-3 text-center global-brands-container">
        <a class="brand-link" href="http://mckleincompany.com/mcklein360">
            <img class="tester-img2 brand-header-360 retina-display" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/mcklein360-barlogo3.png" />
            <img class="tester-img2 brand-header-360 normal-display" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/mcklein360-barlogo2.png" />
        </a>
    </div>
</div>

<%--<ul class="global-brands-bar text-center">
    <div class="row">
    <li class="col-md-3" id="liMcklein" Runat="server"><asp:HyperLink id="lnkMcklein" Runat="server"><img class="brand-header-mck" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/mcklein-barlogo2.png" /></asp:HyperLink></li>
    <li class="col-md-3" ><asp:HyperLink id="lnkParinda" Runat="server"><img class="brand-header-par" src="<%= _bp.ThemesPath %>/assets/img/Resized logos/parinda-barlogo2.png" /></asp:HyperLink></li>
    <li class="col-md-3" ><asp:HyperLink id="lnkSiamod" Runat="server"><img class="brand-header-sia" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/siamod-barlogo2.png" /></asp:HyperLink></li>
    <li class="col-md-3" id="m360"><a href="http://mckleincompany.com/mcklein360"><img class="brand-header-360" src="<%= _bp.ThemesPath %>/assets/img/Resized Logos/mcklein360-barlogo2.png" /></a></li>
</dv>--%>
</li>
</ul>
