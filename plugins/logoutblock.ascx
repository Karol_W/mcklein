<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Control Language="c#" %>
<script runat="server"> 
	private void Page_Load(object sender, System.EventArgs e) {
		if(!Page.IsPostBack){
			if(((NPBasePage)Page).UserID!=""){
				btnLogout.Visible=true;
			}else{
				btnLogout.Visible=false;	
			}
		}
	}
	
	public void Logout(object sender, System.Web.UI.ImageClickEventArgs e) {
		((NPBasePage)Page).Logout();
		Response.Redirect("~/common/accounts/login.aspx");
	}
</script> 
<asp:ImageButton ID="btnLogout" Runat="Server" ImageUrl="~/assets/common/buttons/logoff.gif" OnClick="Logout" />
