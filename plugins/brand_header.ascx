<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>

<script language="C#" runat="server">
    private NPBasePage _bp;

    protected void Page_Load(object sender, System.EventArgs e){
        _bp = (NPBasePage)Page;

        var mckleinRegex = new Regex("^(www\\.)?mcklein[^.]*");
        var parindaRegex = new Regex("^(www\\.)?[^.]*parinda");
        var siamodRegex = new Regex("^(www\\.)?siamod");

        var baseAddressRegex = new Regex("(dev.|live.)?[^\\.]+\\.[^\\.]+(:[0-9]+)?$");
        var baseAddress = baseAddressRegex.Match(HttpContext.Current.Request.Url.Host).Value;

        var request = HttpContext.Current.Request;

        if(mckleinRegex.IsMatch(request.Url.Host)) {
            lnkMcklein.CssClass = "active-brand-bar";
            lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {            
            if(baseAddress.StartsWith("mckleinusa.com")) {
                lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkMcklein.NavigateUrl = request.Url.Scheme + "://" + "mckleinusa.com" + "/";
            }
        }

        if(parindaRegex.IsMatch(request.Url.Host)) {
            lnkParinda.CssClass = "active-brand-bar";
            lnkParinda.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {            
            if(baseAddress.StartsWith("shopparinda.com")) {
                lnkParinda.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkParinda.NavigateUrl = request.Url.Scheme + "://" + "parinda." + "mckleinusa.com" + "/";
            }
        }

        if(siamodRegex.IsMatch(request.Url.Host)) {
            lnkSiamod.CssClass = "active-brand-bar";
            lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + request.Url.Host + "/";
        } else {
            if(baseAddress.StartsWith("siamod.com")) {
                lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + baseAddress + "/";
            } else {
                lnkSiamod.NavigateUrl = request.Url.Scheme + "://" + "siamod." + "mckleinusa.com" + "/";
            }
        }
         
    }

</script>


<ul class="global-brands-bar text-left">
    <li id="liMcklein" Runat="server"><asp:HyperLink id="lnkMcklein" Runat="server"><img src="<%= _bp.ThemesPath %>/assets/img/mcklein-barlogo.png" /></asp:HyperLink></li>
    <li><asp:HyperLink id="lnkParinda" Runat="server"><img src="<%= _bp.ThemesPath %>/assets/img/parinda-barlogo.png" /></asp:HyperLink></li>
    <li><asp:HyperLink id="lnkSiamod" Runat="server"><img src="<%= _bp.ThemesPath %>/assets/img/siamod-barlogo.png" /></asp:HyperLink></li>
    <li id="m360"><a href="http://mckleincompany.com/mcklein360"><img src="<%= _bp.ThemesPath %>/assets/img/mcklein360-barlogo.png" /></a></li>
    <li id="mckweekly"><i style="font-size: 15px;margin-left: -24px;" class="fa">&#xf0b1;</i><div id="weeklyNews">
<h2 id="firstS"><a href="https://mckleinusa.com/catalog/partlist.aspx?CategoryID=191" style="margin-top: -2%;margin-left: -10%;">One of a kind Web Exclusives!</a></h2>
<h2 id="secondS">International Shipping is now available </h2>
</div>
</li>
</ul>
