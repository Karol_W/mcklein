<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.account" %>
<%@ Import Namespace="netpoint.api.common" %>
<%@ Import Namespace="netpoint.api.prospecting" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
   <script language="C#" runat="server">
       NPBasePage bp;

       protected void Page_Load(object sender, System.EventArgs e) {
           bp = (NPBasePage)Page;
           if (!Page.IsPostBack) {
               NPAccount a = new NPAccount(bp.AccountID);
               NPUser u = new NPUser(bp.UserID);
               if (!Page.IsPostBack) {
                   ddlNewsletter.DataSource = NPContactList.FetchLists("email", bp.ServerID, a.AccountType, true, false);
                   ddlNewsletter.DataTextField = "ListName";
                   ddlNewsletter.DataValueField = "ListID";
                   ddlNewsletter.DataBind();
                   if (ddlNewsletter.Items.Count <= 0) {
                       btnSubscribe.Visible = false;
                   }
               }
               Email.Text = u.Email;
           }
       }

       protected void btnSubscribe_Click(object sender, System.EventArgs e)
       {
           Regex objPattern = new Regex("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,3}$");
           if (!objPattern.IsMatch(Email.Text))
           {
               Page.ClientScript.RegisterStartupScript(GetType(), "subscriptionalert", "<script>alert('" +Email.Text+" is not a valid E-Mail Address.'); <"+"/"+"script>");                              
           }
           else
           {
               if (!NPContactSubscriber.IsSubscribed(Email.Text, Convert.ToInt32(ddlNewsletter.SelectedValue)))
               {
                   NPContactSubscriber cs = new NPContactSubscriber();
                   cs.Email = Email.Text;
                   cs.ListID = Convert.ToInt32(ddlNewsletter.SelectedValue);
                   cs.ContactSubscribeType = SubscribeType.Subscribed;
                   cs.SubmitIPAddress = Request.UserHostAddress;
                   cs.SubscribeDate = System.DateTime.Now;
                   cs.UserID = bp.UserID;
                   cs.Save();
                   Page.ClientScript.RegisterStartupScript(GetType(), "subscriptionalert", "<script>alert('" + Email.Text + " subscribed.'); <" + "/" + "script>");                   
                   Email.Text = Email.Prompt;
                   this.RaiseBubbleEvent(this, new CommandEventArgs("subscribed", ddlNewsletter.SelectedValue));
               }
               else
               {
                   Page.ClientScript.RegisterStartupScript(GetType(), "subscriptionalert", "<script>alert('" + Email.Text + " already subscribed.'); <" + "/" + "script>");                   
                   Email.Text = Email.Prompt;
               }
           }
       }
   </script>
<asp:Panel ID="SubscribePanel" Runat="server" >
	<asp:Label id="lblNewsletterDescription" Runat="server" CssClass="npbody" Visible="True" Text="Sign up below to receive one of our email newsletters"></asp:Label>
	<br>
	<asp:DropDownList id="ddlNewsletter" runat="server"></asp:DropDownList>
	<NP:PromptTextBox id="Email" Runat="server" Prompt="E-Mail Address" Columns="25">e-Mail Address</NP:PromptTextBox>
	<asp:LinkButton id="btnSubscribe" Runat="server" Text="Subscribe" onclick="btnSubscribe_Click"></asp:LinkButton>
</asp:Panel>

