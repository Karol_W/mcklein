<%@ register tagprefix="np" TagName="treemenu" src="~/common/controls/treemenu.ascx"%>
<%@ Control Language="c#" %>

<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.catalog" %>

<script language="C#" runat="server">
    void Page_Load(object sender, System.EventArgs e)
    {
        NPBasePage bp = ((NPBasePage)Page);
        if (!Page.IsPostBack)
        {
            treemenu.LinkBase = bp.VirtualPath;
            treemenu.DataSource = NPCatalogCategory.GetCategoriesDataSet(bp.Catalog.CatalogID, bp.Config.SEO.EnableUrlRewrite);
            treemenu.DataBind();
        }
    }

</script>

<np:treemenu id="treemenu" runat="server"  />
