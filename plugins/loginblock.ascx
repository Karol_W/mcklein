<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>

<script language="C#" runat="server">
    private NPBasePage _bp;

    protected void Page_Load(object sender, System.EventArgs e){
        _bp = (NPBasePage)Page;
        if (_bp.UserID != ""){
            loginMain.Visible = false;
            MyAccountPanel.Visible = true;
            sysUserID.Text = _bp.UserID;
        }
        else{
            loginMain.Visible = true;
            MyAccountPanel.Visible = false;
        }
    }
    
    protected void btnLogOff_Click(object sender, System.EventArgs e){
        _bp.Logout();
        _bp.Exit();
    }

    protected void loginMain_LoggedIn(object sender, EventArgs e) {        
        ((NPBasePage)Page).Login(loginMain.UserName, loginMain.Password, false);
    }
    protected void loginMain_LoginError(object sender, EventArgs e) {
        Response.Redirect("~/common/accounts/login.aspx", true);
    }
</script>
<div id="plugin_myaccount">
<asp:Login ID="loginMain" runat="server" OnLoginError="loginMain_LoginError"
    OnLoggedIn="loginMain_LoggedIn" RememberMeText="" CssClass="nptable">
    <LayoutTemplate>
    <table class="nptable">
        <tr>
            <td class="npbody">
            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label><br/>
                <asp:TextBox ID="UserName" runat="server" Width="125px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="white-space:nowrap;" class="npbody">
            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label><br/>
            <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="90px" AutoComplete="off"></asp:TextBox>
            <asp:LinkButton ID="LoginButton" runat="server" CommandName="Login" Text="Log In"></asp:LinkButton>
            </td>
        </tr>
    </table>
    </LayoutTemplate>
</asp:Login>
<asp:panel id="MyAccountPanel" Runat="server" Visible="False" CssClass="npbody">
	<asp:Literal id="ltlWelcomeBack" Runat="server" Text="Welcome Back"></asp:Literal>
	<strong><asp:Literal id="sysUserID" Runat="server"></asp:Literal></strong>
	<br/>
	<asp:HyperLink id="lnkAccount" Runat="server" NavigateURL="~/common/accounts/myaccount.aspx">My Account</asp:HyperLink>
	<br/>
	<asp:LinkButton id="btnLogOff" Runat="server" onclick="btnLogOff_Click">Log Off</asp:LinkButton>
</asp:panel>
<asp:Literal id="hdnPasswordIncorrect" Text="Password is  incorrect, please re-enter." Visible="False"
	runat="server"></asp:Literal>
<asp:Literal id="hdnAccountLocked" Text="Account locked" Visible="False" runat="server"></asp:Literal>
<asp:Literal id="errUserNotInSystem" Text="User not in system" Visible="False" runat="server"></asp:Literal>
</div>
