<%@ Control Language="c#" %>

<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.catalog.controls" %>

<script language="C#" runat="server">
    NPBasePage bp;
    public string virtURL = "";
    public string assetsURL = "";
    public int DisplayNumber = 6;
    
    protected void Page_Load(object sender, System.EventArgs e)
    {
        bp = (NPBasePage)Page;

        FeaturedRepeater.DataSource = bp.Catalog.PartsListFeatured(DisplayNumber, bp.CatalogCode, bp.AccountID, bp.PriceList);
        FeaturedRepeater.DataBind();

        virtURL = bp.VirtualPath;
        assetsURL = bp.AssetsPath;

        this.featuredheader.ImageUrl = ((NPBasePage)this.Page).ResolveImage("images", "featuredheader.gif");
    }

    private void DetermineRowVars(object sender, DataListItemEventArgs e)
    {

        NPPart part = (NPPart)e.Item.DataItem;
        NPManufacturer manf = new NPManufacturer(part.ManufacturerCode, bp.Encoding);

        HyperLink PartImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
        PartImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
        if (part.StaticThumbNail.Length > 0)
        {
            PartImageLink.ImageUrl = bp.ProductsPath + part.StaticThumbNail;
        }
        else
        {
            PartImageLink.ImageUrl = bp.ProductThumbImage + part.ThumbNail;
        }

        HyperLink PartNoLink = ((HyperLink)e.Item.FindControl("PartNoLink"));
        PartNoLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
        PartNoLink.Text = part.PartName;

        HyperLink MNFLink = ((HyperLink)e.Item.FindControl("MNFLink"));
        if (part.ManufacturerCode == "")
        {
            MNFLink.Visible = false;
        }
        else
        {
            MNFLink.Visible = true;
            MNFLink.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + part.ManufacturerCode;
            MNFLink.Text = hdnMore.Text + " " + manf.MNFName + "...";
        }

        ((Label)e.Item.FindControl("PartDescription")).Text = part.Description;
        ((PartPriceDisplay)e.Item.FindControl("SpecialPrice")).Part = part;
    }
</script>

<table width="100%">
	<tr>
		<td>
			<asp:Image Runat="server" ID="featuredheader" ImageUrl="~/assets/common/images/featuredheader.gif"
				BorderWidth="0" /></td>
	</tr>
	<tr>
		<td>
			<ASP:DataList id="FeaturedRepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
				BorderWidth="0" BorderColor="White" RepeatColumns="2" RepeatLayout="Table" CssClass="npbody"
				ItemStyle-VerticalAlign="Top" Width="100%" OnItemDataBound="DetermineRowVars">
				<ItemTemplate>
					<table class="npbody" width="100%" cellspacing="0" cellpadding="5" border="0">
						<tr>
							<td valign="top" align="center">
								<asp:HyperLink Runat="server" ID="PartImageLink" />
							</td>
							<td valign="top" align="left">
								<b>
									<asp:HyperLink Runat="server" ID="PartNoLink" /></b>
								<br>
								<asp:Label ID="PartDescription" Runat="server" />
								<br />
								<np:PartPriceDisplay ID="SpecialPrice" runat="server" Style-Font-Bold="true" PriceStyle-ForeColor="red" TaxStyle-Font-Bold="false" />
								<br>
								<asp:HyperLink Runat="server" ID="MNFLink" /></td>
						</tr>
					</table>
				</ItemTemplate>
			</ASP:DataList>
		</td>
	</tr>
</table>
<asp:Literal id="hdnMore" runat="server" Text="More" Visible="False"></asp:Literal>
