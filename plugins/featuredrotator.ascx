<%@ Control Language="C#" ClassName="FeaturedByCategory" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<%@ Import Namespace="netpoint.api.utility" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.api.data" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<script runat="server">
    NPBasePage bp;

    protected void Page_Load(object sender, EventArgs e) {
        bp = (NPBasePage)Page;
        Rotator1.DataSource = bp.Catalog.PartsListFeatured(10, bp.CatalogCode, bp.AccountID, bp.PriceList);
        Rotator1.DataBind();
    }
</script>

<table cellspacing="0" cellpadding="0" border="0" width="340">
    <tr>
        <td class="RotatorFrame" onmouseover="this.className='RotatorFrameOver'" onmouseout="this.className='RotatorFrame'">
            <ComponentArt:Rotator ID="Rotator1" runat="server" CssClass="Rotator" Width="340"
                Height="77" ScrollInterval="10" RotationType="SmoothScroll" SlidePause="4000"
                PauseOnMouseOver="true">
                <SlideTemplate>
                    <table width="325" cellpadding="0" cellspacing="2" border="0">
                        <tr>
                            <td class="MainText" align="center">
                                <span class="ProductTitle">
                                    <%# DataBinder.Eval(Container.DataItem, "PartNo")%>
                                    <%# DataBinder.Eval(Container.DataItem, "PartName")%>
                                </span>
                                <br />
                                <span class="ProductText">
                                    <%# StringFunctions.Left(DataBinder.Eval(Container.DataItem, "Description").ToString(),100)%>
                                </span>
                                <br />
                                <span class="ProductPrice">
                                    <%# DataBinder.Eval(Container.DataItem, "BasePrice", "{0:c}") %>
                                </span>
                                <br />
                            </td>
                            <td style="width: 70px;">
                                <img src="<%#bp.ProductThumbImage %>/<%# DataBinder.Eval(Container.DataItem, "ThumbNail") %>"
                                    alt="" width="70" height="70" style="border: none;" /></td>
                        </tr>
                    </table>
                </SlideTemplate>
            </ComponentArt:Rotator>
        </td>
    </tr>
</table>
<style>
.Rotator 
{
  border:solid 1px black; 
  background-color:white;
  cursor:default; 
}

.RotatorFrame 
{
  border:solid 1px white; 
  width:450px; 
  height:25px; 
  font-family:verdana; 
  font-size:11px; 
  background-color:white; 
  cursor:default;
}

.RotatorFrameOver 
{
  border:solid 1px darkgray; 
  border-bottom-color:Black;
  border-right-color:Black;
  width:450px; 
  height:25px; 
  font-family:verdana; 
  font-size:11px; 
  background-color:white; 
  cursor:default;
}

.ProductTitle
{
  font-family:Verdana;
  font-size:12px;   
  font-weight:bold; 
}

.ProductText
{
  font-family:Verdana;
  font-size:11px;   
  color:darkslategray;
  width:220px;
}

.ProductPrice
{
  font-family:Verdana;
  font-size:12px;   
  font-weight:bold; 
  color:Red; 
}
</style>
