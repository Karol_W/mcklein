<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Control Language="c#"%>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>
<script language="C#" runat="server">
    public string AssetsPath;
    public string VirtualPath;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        NPBasePage bp = (NPBasePage)Page;
        AssetsPath = bp.AssetsPath;
        VirtualPath = bp.VirtualPath;
    }
    
    protected void SearchButton_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/catalog/search.aspx?keywords=" + Keywords.Text);
    }
</script>

<asp:Panel ID="SearchPanel" Runat="server" HorizontalAlign="Center">
	<NP:DefaultButtonTextBox id="Keywords" runat="server" columns="16" button="SearchButton"></NP:DefaultButtonTextBox> <asp:LinkButton id="SearchButton" Runat="server" Text="Search" OnClick="SearchButton_Click"></asp:LinkButton>
</asp:Panel>
