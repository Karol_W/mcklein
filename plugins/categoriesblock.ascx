<%@ Control Language="c#" %>

<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>

<script language="C#" runat="server">
    public string assetsURL;
    public string virtURL;
    public int ParentID = 0;
    public bool ShowName = false;
    public bool ShowImage = true;
    public bool ShowDescription = false;
    private NPBasePage bp;
    
    void Page_Load(object sender, System.EventArgs e)
    {

         bp = (NPBasePage)Page;

        assetsURL = bp.AssetsPath + "catalog/categories/";

        categoryrepeater.DataSource = (ParentID == 0 ? bp.Catalog.GetChildCategories(true) : new NPCatalogCategory(ParentID).GetChildCategories(true));
        categoryrepeater.DataBind();
    }


    public void DetermineRowVars(object sender, DataListItemEventArgs e)
    {
        NPCatalogCategory cat = (NPCatalogCategory)e.Item.DataItem;
        if (!ShowName)
        {
            e.Item.FindControl("CategoryName").Visible = false;
        }
        if (!ShowImage)
        {
            e.Item.FindControl("CategoryImage").Visible = false;
        }
        if (!ShowDescription)
        {
            e.Item.FindControl("CategoryDescription").Visible = false;
        }
        //overrides
        if (cat.ImageFile == null || cat.ImageFile.Length == 0)
        {
            e.Item.FindControl("CategoryImage").Visible = false;
            e.Item.FindControl("CategoryName").Visible = true;
        }
        if (cat.CategoryName == null || cat.CategoryName.Length == 0)
        {
            e.Item.FindControl("CategoryName").Visible = false;
        }
        if (cat.CategoryName == null || cat.CategoryName.Length == 0)
        {
            e.Item.FindControl("CategoryDescription").Visible = false;
        }
        
        //links 
        if (cat.FriendlyUrl != "" && bp.Config.SEO.EnableUrlRewrite)
        {
            ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/" + cat.FriendlyUrl.ToString();
            ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/" + cat.FriendlyUrl.ToString();
        }
        else
        {       
            ((HyperLink)(e.Item.FindControl("CategoryImageLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
            ((HyperLink)(e.Item.FindControl("CategoryNameLink"))).NavigateUrl = "~/catalog/partlist.aspx?CategoryID=" + cat.CategoryID.ToString();
        }
        
    }
</script>

<ASP:DataList id="categoryrepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None" RepeatColumns="0" RepeatLayout="Table" OnItemDataBound="DetermineRowVars" Width="100%">
	<ItemTemplate>

					<asp:HyperLink ID="CategoryImageLink" Runat="server">
						<asp:Image Runat="server" ID="CategoryImage" BorderWidth="0" ImageUrl='<%#assetsURL+DataBinder.Eval(Container.DataItem,"ImageFile")%>' />
					</asp:HyperLink>

					<asp:HyperLink ID="CategoryNameLink" Runat="server">
						<h1 class="category-name-heading white-gloss-bg">
						<asp:Label Runat="server" ID="CategoryName" Text='<%#DataBinder.Eval(Container.DataItem,"CategoryName")%>' />
						</h1>
					</asp:HyperLink>

					<asp:Label Runat="server" ID="CategoryDescription" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' Height="20" />

	</ItemTemplate>
</ASP:DataList>
