<%@ register tagprefix="np" TagName="treemenu" src="~/common/controls/treemenu.ascx"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="c#"  %>
<%@ Import Namespace="netpoint.classes" %>

<script language="C#" runat="server">
    void Page_Load(object sender, System.EventArgs e)
    {
	    System.Web.UI.HtmlControls.HtmlLink link = new System.Web.UI.HtmlControls.HtmlLink();
        link.Attributes.Add("type", "text/css");
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("href", "~/plugins/tabstrip/tabStyle.css");
        this.Controls.Add(link);
    }
</script> 
<ComponentArt:TabStrip id="TabStrip1" 
      CssClass="TopGroup"
      SiteMapXmlFile="~/plugins/tabstrip/tabData.xml"
      DefaultItemLookId="DefaultTabLook"
      DefaultSelectedItemLookId="SelectedTabLook"
      DefaultDisabledItemLookId="DisabledTabLook"
      DefaultGroupTabSpacing="0"
      ImagesBaseUrl="~/plugins/tabstrip/"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="3" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="5" LeftIconHeight="19" RightIconWidth="5" RightIconHeight="19" />
      <ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="3" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" LeftIconWidth="5" LeftIconHeight="19" RightIconWidth="5" RightIconHeight="19" />
      <ComponentArt:ItemLook LookId="DisabledTabLook" CssClass="DisabledTab" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="3" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" LeftIconWidth="5" LeftIconHeight="19" RightIconWidth="5" RightIconHeight="19" />
    </ItemLooks>
    </ComponentArt:TabStrip>
