<%@ Control Language="C#" %>
<%@ Import Namespace="netpoint.api.account" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.api.common" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="NetPoint.WebControls.Utilities" %>

<script language="C#" runat="server">
    
    NPBasePage _bp;
    
    void Page_Load(object sender, System.EventArgs e) {
        
        _bp = (NPBasePage)Page;

        if (!IsPostBack) {
            NPListBinder.BindCurrencies(ddlCurrencies, _bp.TheSession.CurrencyCode, false, _bp.Connection);
        }
    }

    void ddlCurrencies_SelectedIndexChanged(object sender, EventArgs e) {
        _bp.TheSession.CurrencyCode = ddlCurrencies.SelectedValue;
		if (!string.IsNullOrEmpty(_bp.UserID))
		{
			NPUser usr = new NPUser(_bp.UserID);
			if (usr.Initialized)
			{
				usr.CurrencyCode = ddlCurrencies.SelectedValue;
				usr.Save(false);
			}
		}
        Response.Redirect(Request.Url.PathAndQuery);
    }
    
</script>
<asp:DropDownList ID="ddlCurrencies" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrencies_SelectedIndexChanged" />
