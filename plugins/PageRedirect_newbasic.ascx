﻿
<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<script language="C#" runat="server">
    protected void Page_Init(object sender, System.EventArgs e)
    {

        NPBasePage bp = (NPBasePage)Page;
        int lastChar = Page.Request.RawUrl.IndexOf("aspx") + 4;
        if (lastChar > 4)
        {
            string pagePath = Page.Request.RawUrl.Substring(0, lastChar);
            int firstChar = pagePath.LastIndexOf("/") + 1;

            string pageName = pagePath.Substring(firstChar);
            string addPathInfo = "";


            if (lastChar < Page.Request.RawUrl.Length)
            {
                addPathInfo = Page.Request.RawUrl.Substring(lastChar);
            }

            if (System.IO.File.Exists(this.MapPath(bp.ThemesPath) + pageName))
            {
                if (!bp.AppRelativeVirtualPath.Contains(bp.ServerID))
                {
                    Server.Transfer("~/assets/common/themes/" + bp.ServerID + "/" + pageName + addPathInfo);
                }
            }

        }
    }
</script>

