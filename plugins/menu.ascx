<%@ register tagprefix="np" TagName="treemenu" src="~/common/controls/treemenu.ascx"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="c#"  %>
<%@ Import Namespace="netpoint.classes" %>

<script language="C#" runat="server">
    void Page_Load(object sender, System.EventArgs e)
    {
	    System.Web.UI.HtmlControls.HtmlLink link = new System.Web.UI.HtmlControls.HtmlLink();
        link.Attributes.Add("type", "text/css");
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("href", "~/plugins/menu/menuStyle.css");
        this.Controls.Add(link);
    }
</script>
    <ComponentArt:Menu id="Menu1" 
      Orientation="Horizontal"
      CssClass="TopGroup"
      DefaultGroupCssClass="MenuGroup"
      DefaultSubGroupExpandOffsetX="-10"
      DefaultSubGroupExpandOffsetY="-5"
      SiteMapXmlFile="~/plugins/menu/menuData.xml"
      DefaultItemLookID="DefaultItemLook"
      TopGroupItemSpacing="1"
      DefaultGroupItemSpacing="2"
      ImagesBaseUrl="~/plugins/menu/"
      EnableViewState="false"
      ExpandDelay="100"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="TopItemLook" CssClass="TopMenuItem" HoverCssClass="TopMenuItemHover" LabelPaddingLeft="15" LabelPaddingRight="15" LabelPaddingTop="4" LabelPaddingBottom="4" />
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LabelPaddingLeft="18" LabelPaddingRight="12" LabelPaddingTop="3" LabelPaddingBottom="4" />
      <ComponentArt:ItemLook LookID="BreakItem" ImageUrl="break.gif" CssClass="MenuBreak" ImageHeight="2" ImageWidth="100%" />
    </ItemLooks>
    </ComponentArt:Menu>
