<%@ Control Language="C#"%>
<script runat="server"> 
    private void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Page.Request.IsSecureConnection)
            {                
                Page.Response.Redirect(Page.Request.Url.ToString().ToLower().Replace("http://", "https://"));
                
            } 

                  
        }
    }
</script>
