<%@ Control Language="c#" %>
<%@ Register TagPrefix="NP" Namespace="NetPoint.WebControls" Assembly="NetPoint.WebControls" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.prospecting" %>
<%@ Import Namespace="netpoint.api.account" %>
<%@ Import Namespace="netpoint.api.common" %>
<%@ Import Namespace="netpoint.api.utility" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>

<script language="C#" runat="server">
    bool showMiddleName = false;


    NPBasePage bp;
    private bool _SubmitToProspect = true;

    public bool SubmitToProspect {
        set { _SubmitToProspect = value; }
    }

    protected void Page_Load(object sender, System.EventArgs e) {
        bp = (NPBasePage)Page;
        tbMiddleName.Visible = showMiddleName;
        if (!Page.IsPostBack) {
            NPUser u = new NPUser(bp.UserID);
            tbFirstName.Text = u.FirstName;
            tbMiddleName.Text = u.MiddleName;
            tbLastName.Text = u.LastName;
            tbPhoneOffice.Text = u.DayPhone;
            tbPhoneMobile.Text = u.MobilePhone;
            tbEmail.Text = u.Email;
            //tbCompany.Text = u.AccountName;
        }
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e) {
        // check email validity			
        if (NPValidator.IsEmail(tbEmailTo.Text)) {
            //Send The Request
            NPCommunicationLog cl = new NPCommunicationLog();
            cl.Subject = tbSubject.Text;
            cl.Email = tbEmailTo.Text;
            cl.EmailFrom = tbEmail.Text;
            cl.Message = bp.StyleSheetLink;
            cl.Message += tbBody.Text;
            cl.Message += bp.ControlToHTML(GetTable());
            cl.Incoming = false;
            cl.LogStatus = "O";
            cl.LogType = "email";
            cl.TimeStamp = DateTime.Now;
            cl.AccountID = bp.AccountID;
            cl.UserID = bp.UserID;
            cl.OwnerID = bp.UserID;
            cl.TemplateCode = "requestblock";
            cl.Save();

            //Send The Response
            NPCommunicationLog clr = new NPCommunicationLog();
            clr.Subject = tbResponseSubject.Text;
            clr.Email = tbEmail.Text;
            clr.EmailFrom = tbEmailTo.Text;
            clr.Message = tbResponseBody.Text;
            clr.Incoming = false;
            clr.LogStatus = "O";
            clr.LogType = "email";
            clr.TimeStamp = DateTime.Now;
            clr.AccountID = bp.AccountID;
            clr.UserID = bp.UserID;
            clr.OwnerID = bp.UserID;
            clr.TemplateCode = "requestblock";
            clr.Save();

            //prospect
            if (_SubmitToProspect) {
                NPProspect p = new NPProspect();
                p.FirstName = tbFirstName.Text;
                p.MiddleName = tbMiddleName.Text;
                p.LastName = tbLastName.Text;
                p.CompanyName = tbCompany.Text;
                p.PhoneOffice = tbPhoneOffice.Text;
                p.PhoneMobile = tbPhoneMobile.Text;
                p.Email = tbEmail.Text;
                p.Notes = tbNotes.Text;
                p.Source = tbSource.Text;
                p.Save();
            }
            //redirect
            if (tbRedirectPage.Text != "") {
                Response.Redirect(tbRedirectPage.Text);
            }
            else {
                Response.Redirect("~/");
            }
        }
        else {
            Response.Write("<script>alert('" + errInvalidEmail.Text + "'); <" + "/" + "script>");
            return;
        }
    }

    private Table GetTable() {
        Table t = new Table();
        t.CellPadding = 2;
        t.CellSpacing = 0;
        t.BorderWidth = 1;
        TableRow tr1 = new TableRow();
        TableCell tc11 = new TableCell();
        TableCell tc12 = new TableCell();
        tc11.Text = ltlName.Text;
        tc11.HorizontalAlign = HorizontalAlign.Right;
        tr1.Cells.Add(tc11);
        tc12.Text = tbFirstName.Text + " " + tbLastName.Text;
        tr1.Cells.Add(tc12);
        t.Rows.Add(tr1);
        //
        TableRow tr2 = new TableRow();
        TableCell tc21 = new TableCell();
        TableCell tc22 = new TableCell();
        tc21.Text = ltlCompany.Text;
        tc21.HorizontalAlign = HorizontalAlign.Right;
        tr2.Cells.Add(tc21);
        tc22.Text = tbCompany.Text;
        tr2.Cells.Add(tc22);
        t.Rows.Add(tr2);
        //
        TableRow tr3 = new TableRow();
        TableCell tc31 = new TableCell();
        TableCell tc32 = new TableCell();
        tc31.Text = ltlPhoneOffice.Text;
        tc31.HorizontalAlign = HorizontalAlign.Right;
        tr3.Cells.Add(tc31);
        tc32.Text = tbPhoneOffice.Text;
        tr3.Cells.Add(tc32);
        t.Rows.Add(tr3);
        //
        TableRow tr4 = new TableRow();
        TableCell tc41 = new TableCell();
        TableCell tc42 = new TableCell();
        tc41.Text = ltlPhoneMobile.Text;
        tc41.HorizontalAlign = HorizontalAlign.Right;
        tr4.Cells.Add(tc41);
        tc42.Text = tbPhoneMobile.Text;
        tr4.Cells.Add(tc42);
        t.Rows.Add(tr4);
        //
        TableRow tr5 = new TableRow();
        TableCell tc51 = new TableCell();
        TableCell tc52 = new TableCell();
        tc51.Text = ltlEmail.Text;
        tc51.HorizontalAlign = HorizontalAlign.Right;
        tr5.Cells.Add(tc51);
        tc52.Text = tbEmail.Text;
        tr5.Cells.Add(tc52);
        t.Rows.Add(tr5);
        //
        TableRow tr6 = new TableRow();
        TableCell tc61 = new TableCell();
        TableCell tc62 = new TableCell();
        tc61.Text = ltlNotes.Text;
        tc61.HorizontalAlign = HorizontalAlign.Right;
        tr6.Cells.Add(tc61);
        tc62.Text = tbNotes.Text;
        tr6.Cells.Add(tc62);
        t.Rows.Add(tr6);
        //  
        return t;
    }

</script>

<asp:Panel ID="SubscribePanel" runat="server">
    <table class="npbody" cellspacing="0" cellpadding="2" border="0">
        <tr>
            <td>
                <asp:Literal ID="ltlName" runat="server" Text="Name"></asp:Literal></td>
            <td>
                <asp:TextBox ID="tbFirstName" runat="server" Columns="15"></asp:TextBox>
                <asp:TextBox ID="tbMiddleName" runat="server" Columns="15"></asp:TextBox>
                <asp:TextBox ID="tbLastName" runat="server" Columns="25"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltlCompany" runat="server" Text="Company"></asp:Literal></td>
            <td>
                <asp:TextBox ID="tbCompany" runat="server" Columns="50"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltlPhoneOffice" runat="server" Text="Phone (Office)"></asp:Literal></td>
            <td>
                <asp:TextBox ID="tbPhoneOffice" runat="server" Columns="15"></asp:TextBox>&nbsp;
                <asp:Literal ID="ltlPhoneMobile" runat="server" Text="(Mobile)"></asp:Literal>
                <asp:TextBox ID="tbPhoneMobile" runat="server" Columns="15"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltlEmail" runat="server" Text="Email"></asp:Literal></td>
            <td>
                <asp:TextBox ID="tbEmail" runat="server" Columns="50"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltlNotes" runat="server" Text="Notes"></asp:Literal></td>
            <td>
                <asp:TextBox ID="tbNotes" runat="server" Columns="40" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:LinkButton ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"></asp:LinkButton></td>
        </tr>
    </table>
</asp:Panel>
<asp:TextBox ID="tbEmailTo" runat="server" Visible="False">info@praxissoft.net</asp:TextBox>
<asp:TextBox ID="tbSubject" runat="server" Visible="False">Request From Website</asp:TextBox>
<asp:TextBox ID="tbBody" runat="server" Visible="False">Request From Website</asp:TextBox>
<asp:TextBox ID="tbResponseSubject" runat="server" Visible="False">Response Message</asp:TextBox>
<asp:TextBox ID="tbResponseBody" runat="server" Visible="False">Response Message</asp:TextBox>
<asp:TextBox ID="tbRedirectPage" runat="server" Visible="False"></asp:TextBox>
<asp:TextBox ID="tbSource" runat="server" Visible="False" Text="RequestPage"></asp:TextBox>
<asp:Literal ID="errInvalidEmail" Text="Invalid Email address" Visible="False" runat="server"></asp:Literal>
