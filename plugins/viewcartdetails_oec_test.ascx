﻿<%@ Control Language="C#" ClassName="ViewCartDetails" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.commerce" %>

<script runat="server">
    void Page_Load(object sender, System.EventArgs e){
        NPBasePage bp = (NPBasePage)Page;
        NPOrder o = new NPOrder(bp.UserID, bp.SessionID);
        //c# for displaying price total commented below
        //txtGrandTotal.Text = o.GrandTotal.ToString("c", bp.AppCulture.NumberFormat);
        lnkCheckOut.NavigateUrl = "~/commerce/cart.aspx";
        //lnkCheckOut.ImageUrl = bp.ResolveImage("icons", "cart.png");
        lnkCheckOut.ToolTip = "Proceed to Checkout";

        lnkCart.NavigateUrl = "~/commerce/cart.aspx";
        lnkCart.ToolTip = "Proceed to Checkout";

        //o.OrderDetail.Count;
    }
</script>
<div class="cart-selector">
<asp:hyperlink id="lnkCheckOut" runat="server" >
    <img class="cart-img" src="/assets/common/icons/cart.png"></img>
    </asp:hyperlink>
</div>
<!--Displays cart total in header
<asp:hyperlink id="lnkCart" runat="server" >

    <span>Cart Total: <asp:Label ID="txtGrandTotal" runat="server" /></span>
</asp:hyperlink>
-->