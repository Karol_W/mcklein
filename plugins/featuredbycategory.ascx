<%@ Control Language="C#" ClassName="FeaturedByCategory" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.api.data" %>
<%@ Import Namespace="System.Data" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e) {
        Regex objIsAdminPage = new Regex("admin");
        string earl = Request.Url.ToString();
        if (Request.QueryString["categoryid"] != null && !objIsAdminPage.IsMatch(earl)) //categoryid has value and user not in admin
        {
            FeaturedRepeater.DataSource = FindParts(Request.QueryString["categoryid"]);
            FeaturedRepeater.DataBind();
            pnlFBC.Visible = true;
        }
        if (FeaturedRepeater.Items.Count == 0) {
            pnlFBC.Visible = false;
        }
    }

    protected void FeaturedRepeater_onItemDataBound(object sender, DataListItemEventArgs e) {
        NPBasePage bp = (NPBasePage)Page;
        NPPart part = (NPPart)e.Item.DataItem;
        HyperLink PartImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
        if (PartImageLink != null) {
            PartImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
            Image imgPart = ((Image)e.Item.FindControl("imgPart"));
            imgPart.ImageUrl = bp.ProductThumbImage + part.ThumbNail;
            HyperLink PartNoLink = ((HyperLink)e.Item.FindControl("PartNoLink"));
            PartNoLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
            PartNoLink.Text = part.PartName;
        }
    }

    private ArrayList FindParts(string catid) {
        NPBasePage bp = (NPBasePage)Page;
        string _globalDisplayOutOfStockFlag = "Y";
        if (!Convert.ToBoolean(NPConnection.GetConfigDB("Inventory", "DisplayOutOfStockFlag")))
        {
            _globalDisplayOutOfStockFlag = "N";
        }
        
        ArrayList thelist = new ArrayList();
        string SQL = "SELECT TOP 5 * FROM PartsMaster WHERE FeaturedFlag = 'Y' " +
            " AND PartNo IN (SELECT PartNo FROM PartsCategory WHERE CategoryID = " + catid + ")" +
            " AND AvailableFlag='Y' AND AvailableDate<=GetDate() AND (ExpireDate>=GetDate() OR ExpireDate IS NULL) " +
            "AND (CASE pm.InheritInventorySettingsFlag WHEN 'N' THEN pm.DisplayOutOfStockFlag ELSE '" + _globalDisplayOutOfStockFlag + "' END = 'Y' OR " +
            "(CASE pm.InheritInventorySettingsFlag WHEN 'N' THEN pm.DisplayOutOfStockFlag ELSE '" + _globalDisplayOutOfStockFlag + "' END = 'N' AND pm.InStockFlag = 'Y')) ";
        using (IDataReader TheReader = DataFunctions.ExecuteReader(CommandType.Text, SQL, bp.ConnectionString)) {
            while (TheReader.Read()) {
                NPPart part = new NPPart(TheReader.GetString(0));
                thelist.Add(part);
            }
        }
        return thelist;
    }
</script>

<asp:Panel runat="server" ID="pnlFBC" Visible="false">
    <asp:DataList ID="FeaturedRepeater" runat="server" CellPadding="0" CellSpacing="7"
        BorderWidth="0px" BorderColor="White" CssClass="npbody" OnItemDataBound="FeaturedRepeater_onItemDataBound"
        ShowFooter="False" ShowHeader="True" Font-Underline="False" HorizontalAlign="Center"
        RepeatDirection="Vertical" RepeatLayout="Flow" RepeatColumns="1" Width="100">
        <HeaderStyle CssClass="nplabel" />
        <HeaderTemplate>
        Featured Parts in this Category...
        </HeaderTemplate>
        <ItemStyle HorizontalAlign="center" VerticalAlign="top"  />
        <ItemTemplate>
            <br />
            <asp:HyperLink runat="server" ID="PartImageLink">
                <asp:Image ID="imgPart" runat="server" /></asp:HyperLink>
            <br />
            <asp:HyperLink runat="server" ID="PartNoLink" />
            <br />
        </ItemTemplate>
        <ItemStyle VerticalAlign="Top" />
        <SeparatorStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" />
    </asp:DataList>
</asp:Panel>
