﻿<%@ Control Language="C#" ClassName="Plugin" AutoEventWireup="true" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<script runat="server">
    void Page_Load(object sender, System.EventArgs e) {
        NPBasePage bp = (NPBasePage)Page;
        manufactrepeater.DataSource = bp.Catalog.Manufacturers;
        manufactrepeater.DataBind();
    }

    void DetermineRowVars(object sender, DataListItemEventArgs e) {
        NPCatalogManufacturer mnf = (NPCatalogManufacturer)e.Item.DataItem;
        HyperLink lnkMNFImage = (HyperLink)e.Item.FindControl("lnkMNFImage");
        HyperLink lnkName = (HyperLink)e.Item.FindControl("lnkName");
        if (lnkMNFImage!=null){
            if (mnf.Logo != ""){
                lnkMNFImage.ImageUrl = "~/assets/catalog/manufacturers/" + mnf.Logo;
                lnkMNFImage.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode="+ mnf.ManufacturerCode;
            } 
            else {
				lnkMNFImage.Visible=false;
                lnkName.Text = mnf.Name;
                lnkName.NavigateUrl = "~/catalog/partlist.aspx?ListType=S&DisplayType=L&DisplayNumber=200&MNFCode=" + mnf.ManufacturerCode;
            }
        }
    }
</script>
Browse by Manufacturer
<ASP:DataList id="manufactrepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
	BorderWidth="0" BorderColor="Black" RepeatColumns="0" RepeatLayout="Table" CssClass="npbody"
	OnItemDataBound="DetermineRowVars" Width="100%">
	<ItemTemplate>
		<table class="tableContent" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td valign="middle"><asp:HyperLink ID="lnkMNFImage" Runat="server" /></td></tr>
			<tr><td valign="top"><asp:HyperLink ID="lnkName" Runat="server" /></td></tr>
		</table>
	</ItemTemplate>
</ASP:DataList>