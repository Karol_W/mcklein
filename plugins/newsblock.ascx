<%@ Control Language="c#" %>

<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.common" %>

<script language="C#" runat="server">
    string dateFormat = "";
    protected void Page_Load(object sender, System.EventArgs e)
    {
        int _maxrecords=5;
        string _pagetype = "1"; //default news pagetypeid
        NPBasePage bp = (NPBasePage)Page;
        dateFormat = "{0:" + bp.TheTheme.ThemeCulture.DateTimeFormat.ShortDatePattern + "}";
        
        pagerepeater.DataSource = NPResourcesPages.Pages(_pagetype, "", _maxrecords, true, "");
        pagerepeater.DataBind();
    }
</script>

<ASP:DataList id="pagerepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
	BorderWidth="0" BorderColor="Black" RepeatColumns="0" RepeatLayout="Table" CssClass="npbody" Width="100%">
	<ItemTemplate>
		<table class="tableContent" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td valign="top">
					<b>
						<asp:Label Runat="server" ID="lblReleaseDate" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"ReleaseDate",dateFormat)%>'/></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<b>
						<asp:Label Runat="server" ID="lblDescription" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>'/></b>
					<br>
					<asp:Label Runat="server" ID="lblPageBlurb" CssClass="npbody" Text='<%#DataBinder.Eval(Container.DataItem,"PageBlurb")%>' />
				</td>
			</tr>
			<tr>
				<td valign="top">
					<asp:HyperLink Runat="server" ID="lnkMore" Text="Read More.." NavigateUrl='<%#DataBinder.Eval(Container.DataItem,"PageCode","~/common/pagedetail.aspx?pagecode={0}")%>'/>
					<br><br>
				</td>
			</tr>
		</table>
	</ItemTemplate>
</ASP:DataList>
