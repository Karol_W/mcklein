<%@ Control Language="c#" %>

<%@ Register TagPrefix="np" TagName="PartPriceDisplay" Src="~/catalog/controls/partpricedisplay.ascx" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.catalog" %>
<%@ Import Namespace="netpoint.api.commerce" %>
<%@ Import Namespace="netpoint.catalog.controls" %>

<script language="C#" runat="server">
    NPBasePage bp;
    public string virtURL = "";
    public string assetsURL = "";
    public int DisplayNumber = 6;
    
    protected void Page_Load(object sender, System.EventArgs e)
    {
        bp = (NPBasePage)Page;

        FeaturedRepeater.DataSource = bp.Catalog.PartsListFeatured(DisplayNumber, bp.CatalogCode, bp.AccountID, bp.PriceList);
        FeaturedRepeater.DataBind();

        virtURL = bp.VirtualPath;
        assetsURL = bp.AssetsPath;
    }

    private void DetermineRowVars(object sender, DataListItemEventArgs e)
    {

        NPPart part = (NPPart)e.Item.DataItem;
        NPManufacturer manf = new NPManufacturer(part.ManufacturerCode, bp.Encoding);

		//Show the image for the item. If there is a thumbnail specified use it instead of generating one
        HyperLink PartImageLink = ((HyperLink)e.Item.FindControl("PartImageLink"));
        PartImageLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
        if (part.StaticThumbNail.Length > 0)
        {
            PartImageLink.ImageUrl = bp.ProductsPath + part.StaticThumbNail;
        }
        else
        {
            PartImageLink.ImageUrl = bp.ProductThumbImage + part.ThumbNail;
        }
		//Show the description when hovering over the image
		PartImageLink.ToolTip = part.Description;
		
		//Item number link
        HyperLink PartNoLink = ((HyperLink)e.Item.FindControl("PartNoLink"));
        PartNoLink.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
        PartNoLink.Text = part.PartName;
		
		//Add to Cart button
		HyperLink AddToCart = ((HyperLink)e.Item.FindControl("AddToCart"));
		AddToCart.NavigateUrl = "~/commerce/cart.aspx?AddPartNo=" + part.PartNo;
		AddToCart.ImageUrl =  bp.ResolveImage("buttons", "addtocart.gif");
		
		//More Info button
		HyperLink MoreInfo = ((HyperLink)e.Item.FindControl("MoreInfo"));
        MoreInfo.NavigateUrl = "~/catalog/partdetail.aspx?PartNo=" + part.PartNo;
		MoreInfo.ImageUrl = bp.ResolveImage("buttons", "AddtoWishlist.gif");
		MoreInfo.ToolTip = "Learn More...";
		
		//Show the item description
        //((Label)e.Item.FindControl("PartDescription")).Text = part.Description;
		
		//Show the price
        ((PartPriceDisplay)e.Item.FindControl("SpecialPrice")).Part = part;
    }
</script>
<ul>
<ASP:DataList id="FeaturedRepeater" runat="server"  CellSpacing="0" CellPadding="0" RepeatColumns="3" RepeatLayout="Flow"  RepeatDirection="Horizontal" CssClass="" Width="560px" Height="363px" OnItemDataBound="DetermineRowVars">
  <ItemTemplate>
 <li>
  <div id="item_main">
    <a href="#"><asp:HyperLink Runat="server" ID="PartImageLink" /></a>
    <div id="name"><asp:HyperLink Runat="server" ID="PartNoLink" /></div>
    <div id="desc"><asp:Label ID="PartDescription" Runat="server" /></div>
    <div id="price"><span class="top11">price:</span> <np:PartPriceDisplay ID="SpecialPrice" runat="server" Style-Font-Bold="true" PriceStyle-ForeColor="red" TaxStyle-Font-Bold="false" /></div>
    <div style="padding-bottom: 5px"><asp:HyperLink Runat="server" ID="MoreInfo" runat="server" /><asp:HyperLink ID="AddToCart" runat="server" /></div>
  </div>
</li></ItemTemplate>
</ASP:DataList>
</ul>