<%@ Control Language="c#" %>

<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.api.common" %>
<%@ Import Namespace="netpoint.api.utility" %>

<script language="C#" runat="server">
    protected void Page_Load(object sender, System.EventArgs e)
    {
        NPBasePage bp = (NPBasePage)Page;
        int adid = NPResourcesAds.FindAd(Request.Url.ToString(), bp.ConnectionString);
        if (adid > 0)
        {
            NPResourcesAds ad = new NPResourcesAds(adid);
            NPTheme t = new NPTheme(bp.ServerID, bp.Connection);
            AdLink.ImageUrl = WebFunctions.ReplaceTags(ad.pictureurl, t.HomeUrl, bp.VirtualPath, bp.AssetsPath, Page, bp.UserID, bp.AccountID, bp.ServerID);
            if (ad.clickurl != null && ad.clickurl != "")
            {
                AdLink.NavigateUrl = "~/common/controls/clickthru.ashx?adid=" + ad.adid;
            }
            AdSource.Text = ad.sourcecode;
        }
    }
</script>

<asp:HyperLink ID="AdLink" Runat="server" />
<asp:Literal ID="AdSource" Runat="server" />
<asp:literal ID="hdnAdType" Text="banner" Visible="False" runat="server" />
