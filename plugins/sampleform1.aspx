<%@ Page Language="C#" MasterPageFile="~/masters/common.master" Inherits="netpoint.classes.NPBasePage" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>



<asp:Content ContentPlaceHolderID="mainslot" runat="server" ID="main">
    <asp:PlaceHolder ID="phMainSlot" runat="server">

        <script language="c#" runat="server">  
            protected void Page_Load(object sender, System.EventArgs e)
            {
                NPBasePage bp = (NPBasePage)Page;

                NPUser u = new NPUser(bp.UserID);

                txtBox.Text = u.FirstName + " " + u.LastName;
            }
        </script>

        <div>
            <asp:TextBox ID="txtBox" runat="server"></asp:TextBox>
        </div>
    </asp:PlaceHolder>
</asp:Content>
