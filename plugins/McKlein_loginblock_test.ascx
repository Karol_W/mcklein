<%@ Control Language="c#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.account" %>

<script language="C#" runat="server">
    private NPBasePage _bp;

    protected void Page_Load(object sender, System.EventArgs e){
        _bp = (NPBasePage)Page;
        if (_bp.UserID != ""){
            loginMain.Visible = false;
            MyAccountPanel.Visible = true;
            NPUser user = new NPUser(_bp.UserID);
            //sysUserID.Text = user.FirstName + " " + user.LastName;
            //_bp.UserID;
        }
        else{
            loginMain.Visible = true;
            MyAccountPanel.Visible = false;
        }
    }
    
    protected void btnLogOff_Click(object sender, System.EventArgs e){
        _bp.Logout();
        _bp.Exit();
    }

</script>
    <div class="accdropdown">
        <button type="button" onclick="accountDropDownFunction()" class="accdropbtn">
            Account &#9660;</button>
        <div id="accmyDropdown" class="accdropdown-content">
            <asp:Panel id="loginMain" runat="server">
               
                    <asp:HyperLink class="acclink" id="loginlink" runat="server" NavigateUrl="~/common/accounts/login.aspx" Text="Login"></asp:HyperLink>
                
            </asp:Panel> 
            <asp:panel id="MyAccountPanel" Runat="server" Visible="False">
    <%-- this section creates the hello, username
    <asp:Literal id="ltlWelcomeBack" Runat="server" Text="Hello, "></asp:Literal>
    <asp:Literal id="sysUserID" Runat="server"></asp:Literal>
    --%>
            <asp:HyperLink class="acclink" id="lnkAccount" Runat="server" NavigateURL="~/common/accounts/myaccount.aspx">Your Account</asp:HyperLink>
    
            <asp:LinkButton class="acclink" id="btnLogOff" Runat="server" onclick="btnLogOff_Click">Log Off</asp:LinkButton>
            </asp:panel>
        </div>
    </div>


<asp:Literal id="hdnPasswordIncorrect" Text="Password is  incorrect, please re-enter." Visible="False" runat="server"></asp:Literal>
<asp:Literal id="hdnAccountLocked" Text="Account locked" Visible="False" runat="server"></asp:Literal>
<asp:Literal id="errUserNotInSystem" Text="User not in system" Visible="False" runat="server"></asp:Literal>

<%--<asp:Panel id="loginMain" runat="server">
    <LayoutTemplate>
            <asp:HyperLink id="loginlink" runat="server" NavigateUrl="~/common/accounts/login.aspx" Text="Login"></asp:HyperLink>
    </LayoutTemplate>
</asp:Panel>
<asp:panel id="MyAccountPanel" Runat="server" Visible="False">
<asp:Literal id="ltlWelcomeBack" Runat="server" Text="Hello, "></asp:Literal><asp:Literal id="sysUserID" Runat="server"></asp:Literal>
<asp:HyperLink id="lnkAccount" Runat="server" NavigateURL="~/common/accounts/myaccount.aspx">Your Account</asp:HyperLink>&nbsp;|&nbsp;<asp:LinkButton id="btnLogOff" Runat="server" onclick="btnLogOff_Click">Log Off</asp:LinkButton>
</asp:panel>
<asp:Literal id="hdnPasswordIncorrect" Text="Password is  incorrect, please re-enter." Visible="False" runat="server"></asp:Literal>
<asp:Literal id="hdnAccountLocked" Text="Account locked" Visible="False" runat="server"></asp:Literal>
<asp:Literal id="errUserNotInSystem" Text="User not in system" Visible="False" runat="server"></asp:Literal>--%>
