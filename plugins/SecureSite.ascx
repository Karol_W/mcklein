<%@ Control Language="C#" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="System.Collections.Generic" %>
<script runat="server"> 
    private void Page_Init(object sender, System.EventArgs e)
    {
        string thismystring = HttpContext.Current.Request.Url.AbsoluteUri;
        if (!Page.IsPostBack)
        {
            bool pageRequiresSSL = PageRequiresSSL();
            if (pageRequiresSSL && !Page.Request.IsSecureConnection)
            {
                Page.Response.Redirect(Page.Request.Url.ToString().ToLower().Replace("http://", "https://"));

            }
          
        }
    }
    
    private bool PageRequiresSSL()
    {
        bool res = false;
        
        //add pages to secure to list
        List<string> securepages = new List<string>();
        securepages.Add("netpoint.common.accounts.login");
        securepages.Add("netpoint.common.accounts.createaccount");
        securepages.Add("netpoint.common.accounts.myaccount");
        securepages.Add("netpoint.common.accounts.accountinfo");
        securepages.Add("netpoint.common.accounts.accountaddress");
        securepages.Add("netpoint.common.accounts.userlist");
        securepages.Add("netpoint.common.accounts.savedlists");
        securepages.Add("netpoint.common.accounts.orderhistory");
        securepages.Add("netpoint.common.accounts.orderapprovals");
        securepages.Add("netpoint.common.accounts.accountbooks");
        securepages.Add("netpoint.common.accounts.purchaseditems");
        securepages.Add("netpoint.common.user.profile");
        securepages.Add("netpoint.common.user.changepassword");
        securepages.Add("netpoint.common.subscriptions");
        securepages.Add("netpoint.common.user.tickets");
        securepages.Add("netpoint.common.user.equipment");
        securepages.Add("netpoint.common.user.registeredparts");
        securepages.Add("netpoint.common.user.salesrep");
        
        string name = Page.GetType().BaseType.ToString();
        if (Page is NPBasePageCommerce || securepages.Contains(name))
        {
            res = true;
        }

        return res;
    }
</script>