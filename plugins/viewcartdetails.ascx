﻿<%@ Control Language="C#" ClassName="ViewCartDetails" %>
<%@ Import Namespace="netpoint.api" %>
<%@ Import Namespace="netpoint.classes" %>
<%@ Import Namespace="netpoint.api.commerce" %>

<script runat="server">
    void Page_Load(object sender, System.EventArgs e){
        NPBasePage bp = (NPBasePage)Page;
        NPOrder o = new NPOrder(bp.UserID, bp.SessionID);
        cartrepeater.DataSource = o.OrderDetail;
        cartrepeater.DataBind();
        txtGrandTotal.Text = "Total: "+o.GrandTotal.ToString("c", bp.AppCulture.NumberFormat);
        lnkCheckOut.NavigateUrl = "~/commerce/cart.aspx";
        lnkCheckOut.ImageUrl = bp.ResolveImage("icons", "cart.gif");
        string earl = Request.Url.ToString();
        Regex objIsCartPage = new Regex("commerce");        
        if (objIsCartPage.IsMatch(earl))
        {
            pnlCart.Visible = false;
        }
    }
    void DetermineRowVars(object sender, DataListItemEventArgs e) {
        NPOrderDetail npo = (NPOrderDetail)e.Item.DataItem;
        HyperLink lnkLineItem = (HyperLink)e.Item.FindControl("lnkLineItem");
        lnkLineItem.NavigateUrl = "~/catalog/partdetail.aspx?partno=" + npo.PartNo;
        string pname = npo.PartName;
        if (pname.Length > 12) {
            lnkLineItem.Text = npo.Quantity.ToString() + "&nbsp;x&nbsp;" + pname.Substring(0, 12) + "...";
        }
        else {
            lnkLineItem.Text = npo.Quantity.ToString() + "&nbsp;x&nbsp;" + pname;
        }
    }
</script>
<asp:Panel ID="pnlCart" runat="server" >
<table>
<tr><td style="white-space:nowrap;">Cart Contents:</td></tr>
<tr>
<td>
<ASP:DataList id="cartrepeater" runat="server" CellSpacing="0" CellPadding="0" GridLines="None"
	BorderWidth="0" BorderColor="Black" RepeatColumns="0" RepeatLayout="Table" CssClass="npbody"
	OnItemDataBound="DetermineRowVars" Width="100%" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
	<ItemTemplate>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td align="left" style="white-space:nowrap;"><asp:Hyperlink runat="server" ID="lnkLineItem" /></td></tr>
		</table>
	</ItemTemplate>
</ASP:DataList>
</td>
</tr>
<tr><td align="right"><strong><asp:Label runat="server" ID="txtGrandTotal" /></strong></td></tr>
<tr><td align="right"><asp:hyperlink id="lnkCheckOut" runat="server" NavigateUrl="~/commerce/cart.aspx" ></asp:hyperlink></td></tr>
<tr><td align="left"><asp:Label runat="server" ID="txtText"></asp:Label></td></tr>
</table>
</asp:Panel>