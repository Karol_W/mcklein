$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/dynamic/upload/';
    window.numfiles = 0;
    window.uploadedFiles = [];
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        //maxFileSize: 999000,
        maxNumberOfFiles: 3,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: false,
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        getNumberOfFiles: function () {
            return window.numfiles;
        }
    }).on('fileuploadadd', function (e, data) {
        $('[id$="btnSendmail"]').after("<div class=\"remove-soon\">Please wait for your attachment to finish uploading</div>");
        $('[id$="btnSendmail"]').attr("disabled","disabled");
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>');
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {

        window.numfiles += 1;
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
                $('.remove-soon').remove();
                $('[id$="btnSendmail"]').attr("disabled",false);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $('.remove-soon').remove();
        $('[id$="btnSendmail"]').attr("disabled",false);
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var name = data.files[index].name;
                var url = file.url;
                window.uploadedFiles.push({name: name, url: url});
                $("#ctl00_mainslot_UploadedFiles").val(JSON.stringify(window.uploadedFiles));
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $('.remove-soon').remove();
        $('[id$="btnSendmail"]').attr("disabled",false);
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});